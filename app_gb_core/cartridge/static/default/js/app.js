(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

var progress = require('./progress'),
	util = require('./util');

var currentRequests = [];

/**
 * @function
 * @description Ajax request to get json response
 * @param {Boolean} async  Asynchronous or not
 * @param {String} url URI for the request
 * @param {Object} data Name/Value pair data request
 * @param {Function} callback  Callback function to be called
 */
var getJson = function (options) {
	options.url = util.toAbsoluteUrl(options.url);
	// return if no url exists or url matches a current request
	if (!options.url || currentRequests[options.url]) {
		return;
	}

	currentRequests[options.url] = true;

	// make the server call
	$.ajax({
		dataType: 'json',
		url: options.url,
		async: (typeof options.async === 'undefined' || options.async === null) ? true : options.async,
		data: options.data || {},
		type :options.type|| 'GET' //PREVAIL-Added to handle security form issues. 
	})
	// success
	.done(function (response) {
		if (options.callback) {
			options.callback(response);
		}
	})
	// failed
	.fail(function (xhr, textStatus) {
		if (textStatus === 'parsererror') {
			window.alert(Resources.BAD_RESPONSE);
		}
		if (options.callback) {
			options.callback(null);
		}
	})
	// executed on success or fail
	.always(function () {
		// remove current request from hash
		if (currentRequests[options.url]) {
			delete currentRequests[options.url];
		}
	});
};
/**
 * @function
 * @description ajax request to load html response in a given container
 * @param {String} url URI for the request
 * @param {Object} data Name/Value pair data request
 * @param {Function} callback  Callback function to be called
 * @param {Object} target Selector or element that will receive content
 */
var load = function (options) {
	options.url = util.toAbsoluteUrl(options.url);
	// return if no url exists or url matches a current request
	if (!options.url || currentRequests[options.url]) {
		return;
	}

	currentRequests[options.url] = true;

	// make the server call
	$.ajax({
		dataType: 'html',
		url: util.appendParamToURL(options.url, 'format', 'ajax'),
		data: options.data,
		xhrFields: {
			withCredentials: true
		},
		type :options.type|| 'GET' //PREVAIL-Added to handle security form issues.
	})
	.done(function (response) {
		// success
		if (options.target) {
			$(options.target).empty().html(response);
		}
		if (options.callback) {
			options.callback(response);
		}
	})
	.fail(function (xhr, textStatus) {
		// failed
		if (textStatus === 'parsererror') {
			window.alert(Resources.BAD_RESPONSE);
		}
		options.callback(null, textStatus);
	})
	.always(function () {
		progress.hide();
		// remove current request from hash
		if (currentRequests[options.url]) {
			delete currentRequests[options.url];
		}
	});
};

exports.getJson = getJson;
exports.load = load;

},{"./progress":40,"./util":52}],2:[function(require,module,exports){
/**
 *    (c) 2009-2014 Demandware Inc.
 *    Subject to standard usage terms and conditions
 *    For all details and documentation:
 *    https://bitbucket.com/demandware/sitegenesis
 */

'use strict';

var countries = require('./countries'),
	cq = require('./cq'),
	dialog = require('./dialog'),
	minicart = require('./minicart'),
	page = require('./page'),
	searchplaceholder = require('./searchplaceholder'),
	searchsuggest = require('./searchsuggest'),
	searchsuggestbeta = require('./searchsuggest-beta'),
	tooltip = require('./tooltip'),
	util = require('./util'),
	validator = require('./validator');

// if jQuery has not been loaded, load from google cdn
if (!window.jQuery) {
	var s = document.createElement('script');
	s.setAttribute('src', 'https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js');
	s.setAttribute('type', 'text/javascript');
	document.getElementsByTagName('head')[0].appendChild(s);
}

require('./jquery-ext')();
require('./cookieprivacy')();


var SetMenuTimer = 150;
var MegaMenuTimer = {
    id: null,
    clear: function() {
        if (MegaMenuTimer.id) {
            window.clearTimeout(MegaMenuTimer.id); 
            delete MegaMenuTimer.id;
        }
    },
    start: function(duration, $curObj) {
        MegaMenuTimer.id = setTimeout(function() {
            if (!$curObj.hasClass('hovered-list')) {
                $curObj.siblings('li').removeClass('level-3-hovered');
                $curObj.addClass('level-3-hovered');
            }
        }, duration);
    }
};


function initializeEvents() {
	// Mega menu delay function
    $(document).off('mouseenter', '.menu-category .has-sub-menu').on('mouseenter', '.menu-category .has-sub-menu', function() {
    	var $curObj = $(this);
        if (window.matchMedia('(min-width: 1023px)').matches) {
            if (!$curObj.hasClass('hovered-list')) {
                $curObj.siblings('li').removeClass('hovered-list');
                $curObj.addClass('hovered-list');
                if( !$curObj.find('.level-2 .has-submenu').first().hasClass('level-3-hovered')) {
                	$curObj.find('.level-2 .has-submenu').siblings('li').removeClass('level-3-hovered');
                	$curObj.find('.level-2 .has-submenu').first().addClass('level-3-hovered');
                }
            }
        }
    }).off('mouseleave', '.menu-category .has-sub-menu').on('mouseleave', '.menu-category .has-sub-menu', function() {
    	var $curObj = $(this);
        if (window.matchMedia('(min-width: 1023px)').matches) {
        	$curObj.removeClass('hovered-list');
        	$curObj.find('.level-2 .has-submenu').first().removeClass('level-3-hovered');
        }
    });    
    if($(window).width() > 1023){
    	$(".top-banner .social-links-share").click(function(e){
    		$(".top-banner .social-links").toggle();
    	});
    }
    $(document).off('mouseenter', '.level-2 .has-submenu').on('mouseenter', '.level-2 .has-submenu', function() {
        if (window.matchMedia('(min-width: 1023px)').matches) {
            var $curObj = $(this);
            MegaMenuTimer.clear();
            MegaMenuTimer.start(SetMenuTimer, $curObj);
        }
    }).off('mouseleave', '.level-2 .has-submenu').on('mouseleave', '.level-2 .has-submenu', function() {
        if (window.matchMedia('(min-width: 1023px)').matches) {
            MegaMenuTimer.clear();
        }
    }); 
        
	var controlKeys = ['8', '13', '46', '45', '36', '35', '38', '37', '40', '39'];
	var slot = [];
	if (window.pageContext.type=="storefront" || window.pageContext.type=="search") {
		var promoId = "home-main";
		if(window.pageContext.type=="storefront")
			promoId = "home-main";
		else if(window.pageContext.type=="search")
			promoId = "globalslot";
		var homepageImp = null;
		if ($(window).width() > 1023) {
                homepageImp = $('#homepage-slider .banner1,.normal-btn-big:visible,.slot-below-usermenu.mobile .gtmeventsclick a img,.slot-below-usermenu.mobile .html-slot-container .top-promo,.html-slot-container .slot-banner-img,.category-banner-container.section .category-banner-cell.category-cell-1,.homepage-bottom-slots .section .banner5 .banner5-one a img,.homepage-bottom-slots .section .banner5 .banner5-two.hide-only-mobile a img,.homepage-bottom-slots .section .banner5 .banner5-three a img,#homepage-slider .banner4 div.show-only-desktop,.banner3 span.show-only-desktop:eq(1)');
	   	}else{
                homepageImp = $('#homepage-slider .banner1,.normal-btn-big:visible, .slot-below-usermenu.desktop .html-slot-container .top-promo,.slot-below-usermenu.mobile .gtmeventsclick a img, .homepage-bottom-slots .section .banner5 .banner5-two.show-only-mobile.slick-initialized.slick-slider a img,.html-slot-container .slot-banner-img,.category-banner-container.section .category-banner-cell.category-cell-1,.homepage-bottom-slots .section .banner2 ul li,.homepage-bottom-slots .section .banner2 .banner2-left .content-asset,.homepage-bottom-slots .section .banner4 .banner4-left');
	   	}
		$.each(homepageImp, function(index) {
			var url = '';
			if(window.pageContext.type == "storefront"){
				 url = $(this).find('a img').data('src') != undefined ? $(this).find('a img').data('src') : $(this).find('a img').attr('src') ;
                        url = ((url === ''||url == undefined) && $(this).data('src') != undefined) ? $(this).data('src') : $(this).attr('src')!=undefined && $(this).attr('src').length > 0 ? $(this).attr('src'): url;
				 url = url == undefined ? $(this).find('a').text() : url.lastIndexOf('?') > 0 ? url.split('?')[0] : url;
				 url = (url == undefined && $(this).find('a').text() && url.lastIndexOf('?') < 0 ) ? $(this).text() : url;
                        url = (url.length == 0) ? $(this).text().trim().replace('\n','') : url;
			}else{
                        url = $(this).is('img')?$(this).attr('src'):$(this).find('a img').attr('src');
			}
                 if($(this).is('img'))
                 {
                        $(this).length > 0 ? $(this).attr('index',index+1) : '';
                 }else
                 {
			$(this).find('a img').length > 0 ? $(this).find('a img').attr('index',index+1) : $(this).find('a').length > 0 ? $(this).find('a').attr('index',index+1) :$(this).attr('index',index+1);
                 }
                 
			var prodName = $(this).find('.name-link') != '' ? $(this).find('.name-link').text().trim() : $(this).find('a').text() != '' ? $(this).find('a').text().trim() : '';
			if(url != undefined){
				var count = url.lastIndexOf('/');
				var urlimg =url.slice(count+1);
				var imgjpj = urlimg.substr(urlimg.length-3,urlimg.length) == 'png' ? urlimg.replace(urlimg.substr(urlimg.length-3) ,'jpg') : urlimg;
				if(imgjpj == "lazy-placeholder.jpg" ||imgjpj == "blank_img.jpg" ||imgjpj == "blank_ampersand.jpg" ){
					// some unwanted images are loading that's why we are placing this.
				}else{
					var crea = urlimg.substr(urlimg.length-3,urlimg.length) == 'png' ? urlimg.replace(urlimg.substr(urlimg.length-3) ,'jpg') : urlimg;
                              crea =  crea.replace("?$staticlink$","");
					slot.push({
						 'id': $(this).find('#banner-ids').length > 0 ? $(this).find('#banner-ids').text() :$(this).closest('.banner2').find('#home-bottom-one').val() != null ? $(this).closest('.banner2').find('#home-bottom-one').val() : promoId,
					     'name': ($(this).find('a img').attr('gtmpname') != null) ? $(this).find('a img').attr('gtmpname') :($(this).attr('gtmpname') != null) ? $(this).attr('gtmpname'): ($(this).attr('gtmpname') != null) ? $(this).attr('gtmpname') : ($(this).find('a').attr('gtmpname') != null) ? $(this).find('a').attr('gtmpname') :  prodName ,
					     'creative': crea,
					     'position': index+1
				 });
				}	
			}
		});
		if(window.pageContext.type=="search"){
			var promoViewed = false;
			if($('.pt_plppage').length > 0){
				var plp = $('.col-lg-12.col-md-12.col-sm-12 .still-banner');
				$.each(plp, function(index) {
					var url = ($(this).find('img').attr('src') != null)?$(this).find('img').attr('src'):$('.sitewideBanner').parent().attr('href');
					var count = url.lastIndexOf('/');
					var prodName = $(this).find('.name-link') != '' ? $(this).find('.name-link').text().trim() : $(this).find('a').text() != '' ? $(this).find('a').text().trim() : '';
					var urlimg =url.slice(count+1);
					if(urlimg == "lazy-placeholder.jpg" ||urlimg == "blank_img.jpg" ||urlimg == "blank_ampersand.jpg" || urlimg == "shareyourwalls1.jpg" ){
						// some unwanted images are loading that's why we are placing this.
					}else{
						slot.push({
							 'id': $(this).find('#banner-ids').length > 0 ? $(this).find('#banner-ids').text() :'plpBodyContents',
						     'name': ($(this).find('img').attr('gtmpname') != null) ? $(this).find('img').attr('gtmpname') : prodName ,
						     'creative': $(this).find('a img').data('src') != undefined ? $(this).find('a img').data('src') : urlimg,
						     'position': index+1
					 });
					}	
				});
				$(".inner-content.right-text-title").find("a").each(function(index){
					$(this).find('span').length > 0 ? $(this).find('span').attr('index',1) : $(this).attr('index',1);
                    slot.push({
                            'id': $(this).closest('.inner-content').find('.category-tile h1').text(),
                            'name': $(this).find('span').length > 0 ? $(this).find('span').text() : $(this).text(),
                            'creative': $(this).find('span').length > 0 ? $(this).find('span').text() : $(this).text(),
                            'position': 1
                    });
                });
				
			}
		}
		if(window.pageContext.type != 'search'){
			dataLayer.push({
				'event' : 'promotions_loaded',
				'ecommerce' : {
					'promoView' : {
						'promotions' : slot 
						}}});
			//gtm reload for homepage GB-645
			var $homePageReload = $('#gtmReload');
			var url = Urls.gtmReload;
			//url = util.appendParamToURL(url, 'format', 'ajax');
			$homePageReload.load(url);
			//end
		}else{
                if($('.pt_plppage').length==0 && $('.clpdata').length == 0){
				if($(window).width() > 1023){
					dataLayer.push({
						'event' : 'promotions_loaded',
						'ecommerce' : {
							'promoView' : {
								'promotions' : slot 
								}}});
				}
				//gtm reload for clptwo GB-646
				var $clpTwo = $('#gtmReload');
				var url = Urls.gtmReload;
				//url = util.appendParamToURL(url, 'format', 'ajax');
				$clpTwo.load(url);
				//end
			}
			for(var i=0;i<dataLayer.length;i++){
				if(dataLayer[i].ecommerce!= null && !promoViewed){
					promoViewed = true;
					dataLayer[i].ecommerce.promoView = {
							promotions:slot
					}
				}
			}
		}
		
	}
	//Changes made for GB-620
	if(window.pageContext.type == "storefront"){
          $('#homepage-slider .banner1,.normal-btn-big:visible,.slot-below-usermenu.mobile .gtmeventsclick a,.slot-below-usermenu.mobile .html-slot-container .top-promo,.html-slot-container .slot-banner-img,.category-banner-container.section .category-banner-cell.category-cell-1,.homepage-bottom-slots .section .banner5 .banner5-one a img,.homepage-bottom-slots .section .banner5 .banner5-two.hide-only-mobile a img,.homepage-bottom-slots .section .banner5 .banner5-three a img,#homepage-slider .banner4 div.show-only-desktop,.banner3 span.show-only-desktop:eq(1)').on("click", function(e) {
			var url = '';
                        url = $(this).find('a img').data('src') != undefined ? $(this).find('a img').data('src') : $(this).find('a img').length>0 ? $(this).find('a img').attr('src'):$(this).find('img').attr('src');
				 url = (url == undefined && $(this).data('src') != undefined) ? $(this).data('src') : url;
				 url = url == undefined ? $(this).find('a').text() : url.lastIndexOf('?') > 0 ? url.split('?')[0] : url;
 
			var prodName = $(this).find('.name-link') != '' ? $(this).find('.name-link').text().trim() : $(this).find('a').text() != '' ? $(this).find('a').text().trim() : '';
			if(url != undefined){
				var count = url.lastIndexOf('/');
				var urlimg =url.slice(count+1);
				urlimg = urlimg == "" ? $(this).text().trim() : urlimg;
				var imgjpj = urlimg.substr(urlimg.length-3,urlimg.length) == 'png' ? urlimg.replace(urlimg.substr(urlimg.length-3) ,'jpg') : urlimg;
                        var position = $(this).find('a img').length > 0 ? $(this).find('a img').attr('index') : $(this).find('img').length>0 ? $(this).find('img').attr('index') : $(this).attr('index');
				position = position == undefined ? $(this).parent().parent().find('a img').attr('index') : position;
				if(imgjpj == "lazy-placeholder.jpg" ||imgjpj == "blank_img.jpg" ||imgjpj == "blank_ampersand.jpg"  ){
					// some unwanted images are loading that's why we are placing this.
				}else{
					slot.push({
						 'id': $(this).find('#banner-ids').length > 0 ? $(this).find('#banner-ids').text() :$(this).closest('.banner2').find('#home-bottom-one').val() != null ? $(this).closest('.banner2').find('#home-bottom-one').val() : "home-main",
					     'name': ($(this).find('a img').attr('gtmpname') != null) ? $(this).find('a img').attr('gtmpname') :($(this).attr('gtmpname') != null) ? $(this).attr('gtmpname'): ($(this).attr('gtmpname') != null) ? $(this).attr('gtmpname') : ($(this).find('a').attr('gtmpname') != null) ? $(this).find('a').attr('gtmpname') :  prodName ,
					     'creative': urlimg.substr(urlimg.length-3,urlimg.length) == 'png' ? urlimg.replace(urlimg.substr(urlimg.length-3) ,'jpg') : urlimg,
					     'position': position
				 });
				}	
			}
			
			if(window.pageContext.type=="storefront"){
				dataLayer.push({
					'event' : 'promotionClick',
					'ecommerce' : {
						'promoClick' : {
							'promotions' : slot
						}}});
			}
         });
	}
	//Changes made for GB-619
	var slot = [];
	var promoId = "globalslot";
	if(window.pageContext.type == "search"){
          $('#homepage-slider .banner1,.normal-btn-big:visible,.slot-below-usermenu.mobile .gtmeventsclick a,.html-slot-container .slot-banner-img,.slot-below-usermenu.mobile .html-slot-container .top-promo,.category-banner-container.section .category-banner-cell.category-cell-1,.homepage-bottom-slots .section .banner5 .banner5-one a img,.homepage-bottom-slots .section .banner5 .banner5-two.hide-only-mobile a img,.homepage-bottom-slots .section .banner5 .banner5-three a img,#homepage-slider .banner4 div.show-only-desktop,.banner3 span.show-only-desktop:eq(1)').on("click", function(e) {
                   var url = ($(this).find('a img').attr('src') != null)?$(this).find('a img').attr('src'):$(this).is('a')?$(this).find('img').attr('src'):$('.sitewideBanner').parent().attr('href');
			var count = url.lastIndexOf('/');
			var urlimg =url.slice(count+1);
                 var position = $(this).find('a img').length > 0 ? $(this).find('a img').attr('index') : $(this).find('img').length>0 ? $(this).find('img').attr('index') : $(this).attr('index');
			position = position == undefined ? $(this).parent().parent().find('a img').attr('index') : position;
			slot.push({
				'id': $(this).find('#banner-ids').length > 0 ? $(this).find('#banner-ids').text() :  promoId,
        	    'name': $(this).find('a img').attr('gtmpname') != null ? $(this).find('a img').attr('gtmpname') : "",
             'creative': urlimg.replace(urlimg.substr(urlimg.length-3) ,'jpg').replace('?$staticlijpg',''),
        	    'position': position
			});
			if(window.pageContext.type=="search"){
				dataLayer.push({
					'event' : 'promotionClick',
					'ecommerce' : {
						'promoClick' : {
							'promotions' : slot
						}}});
			}
	    }); 
			if($('.pt_plppage').length > 0){
				$('.col-lg-12.col-md-12.col-sm-12 .still-banner .inner-text a').on('click',function(){
					var url = ($(this).attr('href') != null)?$(this).attr('href'):$('.sitewideBanner').parent().attr('href');
						slot.push({
							 'id': $(this).closest('.inner-content').find('.category-tile h1').text() ,
						     'name': $(this).attr('gtmpname') != null ? $(this).attr('gtmpname') :$(this).find('span').length > 0 ? $(this).find('span').text() : $(this).text(),
						     'creative': $(this).text(),
						     'position': 1
					 });
						dataLayer.push({
							'event' : 'promotionClick',
							'ecommerce' : {
								'promoClick' : {
									'promotions' : slot
								}}});
				});
			}
	}
	////Changes made for GB-618 and GB-627
	var slot = [];
	var array = new Array();
	if(window.pageContext.type == "search"){
		$('body').on('click','.search-result-content .product-tile',function(){
			$( ".breadcrumb .breadcrumb-element" ).each(function( index ) {
				var arrayVals = $(this).text();
				array.push(arrayVals);
			});
			var breadCr = array.join('|')
			if($('#searchqueary').val() != "null"){
				var actionField={'list': 'Search Results'}
			}else if ($('#actionajax').val()=="ajax" && $('#searchqueary').val() == "null"){
				var actionField={'action':'click','list': breadCr+"- FILTERED"};
			}else{
				var actionField={'list': breadCr};
			}
			var variant = $('#searchqueary').val() != "null" ? $('#searchqueary').val() : '';
			slot.push({
				'name': $(this).find('.product-name').length > 0 ? $(this).find('.product-name').text().trim() : '' ,
				'id': $(this).attr('data-itemid').length > 0 ? $(this).attr('data-itemid') : '',
				'price':$(this).find('.product-sales-price').text() != "N/A" ? $(this).find('.product-sales-price').text().slice(1).trim() :'0.00', 
				'brand': $(this).attr('data-brand').length > 0 ? $(this).attr('data-brand') : '' ,
				'category': $(this).attr('data-primary-cgid').length > 0 ? $(this).attr('data-primary-cgid') : '',
				'variant': variant,
				'position': $(this).parent().attr('data-index') != undefined ? $(this).parent().attr('data-index').replace('.0','') : ""
			});
			dataLayer.push({
				'event' : 'productClick',
				'ecommerce' : {
					'click' : {
						'actionField': actionField,
							'products' : slot
					}}});
		});
	}
	////Changes made for GB-614
	var slot = [];
	var array = new Array();
	if(window.pageContext.type == "search" && $('.clpdata').length > 0){
		$('.product-listing .product-tile').on('click',function(){
			$( ".breadcrumb .breadcrumb-element" ).each(function( index ) {
				var arrayVals = $(this).text();
				array.push(arrayVals);
			});
			var breadCr = array.join('|');
			var actionField={'list': breadCr};
			slot.push({
				'name': $(this).find('.product-name').length > 0 ? $(this).find('.product-name').text().trim() : '' ,
				'id': $(this).attr('data-itemid').length > 0 ? $(this).attr('data-itemid') : '',
				'price':$(this).find('.product-sales-price').text() != "N/A" ? $(this).find('.product-sales-price').text().slice(1).trim() :'0.00', 
				'brand': $(this).attr('data-brand').length > 0 ? $(this).attr('data-brand') : '' ,
				'category': $(this).attr('data-primary-cgid').length > 0 ? $(this).attr('data-primary-cgid') : '',
				'variant': '',
				'position': $(this).parent().attr('data-prodcount') != undefined ? $(this).parent().attr('data-prodcount').replace('.0','') : ''
			});
			dataLayer.push({
				'event' : 'productClick',
				'ecommerce' : {
					'click' : {
						'actionField': actionField,
							'products' : slot
					}}});
		});
	}
	
	////Changes made for GB-623
	var slot = [];
	var array = new Array();
	if(window.pageContext.type == "product"){
		$('.product-tile').on('click',function(){
			slot.push({
				'name': $(this).find('.product-name').length > 0 ? $(this).find('.product-name').text().trim() : '' ,
				'id': $(this).attr('data-itemid').length > 0 ? $(this).attr('data-itemid') : '',
				'price':$(this).find('.product-sales-price').text() != "N/A" ? $(this).find('.product-sales-price').text().slice(1).trim() :'0.00', 
				'brand': $(this).attr('data-brand').length > 0 ? $(this).attr('data-brand') : '' ,
				'category': $(this).attr('data-primary-cgid').length > 0 ? $(this).attr('data-primary-cgid') : '',
				'variant': '',
				'position': $(this).closest('.grid-tile').attr('data-grid-tile-count') != undefined ? $(this).closest('.grid-tile').attr('data-grid-tile-count') : ''
			});
			var list = $(this).closest('.grid-tile').find("input[name=listName]").val();
			dataLayer.push({
				'event' : 'productClick',
				'ecommerce' : {
					'click' : {
						'actionField': {'list': list},
							'products' : slot
					}}});
		});
	}
	//GB-613 start
	window.onload = function() {
		var impressions;
		var youmaylikecount=1;
		for(var i=0;i<dataLayer.length;i++){
			if(dataLayer[i].ecommerce!= null){
				impressions = dataLayer[i].ecommerce.impressions;
			}
		}
		if(impressions != undefined){
			var youmaylike =  $('div.similar-products-content .recommendations #carousel-recommendations .slick-track .slick-slide').not('.slick-cloned');
			$.each(youmaylike, function(){
				impressions.push({
					'name': $(this).find('input[class="abtest-product-name"]').val(),
					'id':  $(this).find('input[class="abtest-pid"]').val(),
					'price':  $(this).find('input[class="abtest-price"]').val(),
					'brand':  $(this).find('input[class="abtest-brand"]').val(),
					'category':  $(this).find('input[class="abtest-category"]').val(),
					'variant': '',
					'list': 'You May Also Like',
					'position':	youmaylikecount++
				});
			});
		}
		var len = $('.product-col-1 #thumbnails ul li').length;
		if(len <= 4) {
			$('#thumbnails').addClass('tt');
		}
		if($('.bv-details-bar').length == 0)
		{
			$('#pdpMain').find('.product-col-2 #BVRRSummaryContainer .bv-action-bar').addClass('review-for-NZ');
		}
		var currentSite = $("#wrapper .country-select").find("select[id$='_destination'] option[selected=selected]").val();
		if(currentSite == "NL"){ 
			$('#pdpMain').find('.product-col-2 #BVRRSummaryContainer .bv-write-review-container').addClass('review-for-NL');
		}else if(currentSite == "FR"){ 
			$('#pdpMain').find('.product-col-2 #BVRRSummaryContainer .bv-write-review-container').addClass('review-for-FR');
		}else if(currentSite == "DE"){ 
			$('#pdpMain').find('.product-col-2 #BVRRSummaryContainer .bv-write-review-container').addClass('review-for-DE');
		}else if(currentSite == "CA"){ 
			$('#pdpMain').find('.product-col-2 #BVRRSummaryContainer .bv-write-review-container').addClass('review-for-CA');
		}	
	};
	//GB-613 end
	//PREVAIL-Added to handle form dialog boxes server side issues.
	$("body").on("click", ".dialogify, [data-dlg-options], [data-dlg-action]", require('./dialogify').setDialogify)
		.on('keydown', 'textarea[data-character-limit]', function (e) {
			var text = $.trim($(this).val()),
				charsLimit = $(this).data('character-limit'),
				charsUsed = text.length;

				if ((charsUsed >= charsLimit) && (controlKeys.indexOf(e.which.toString()) < 0)) {
					e.preventDefault();
				}
		})
		.on('change keyup mouseup', 'textarea[data-character-limit]', function () {
			var text = $.trim($(this).val()),
				charsLimit = $(this).data('character-limit'),
				charsUsed = text.length,
				charsRemain = charsLimit - charsUsed;

			if (charsRemain < 0) {
				$(this).val(text.slice(0, charsRemain));
				charsRemain = 0;
			}

			$(this).next('div.char-count').find('.char-remain-count').html(charsRemain);
		});

	/**
	 * initialize search suggestions, pending the value of the site preference(enhancedSearchSuggestions)
	 * this will either init the legacy(false) or the beta versions(true) of the the search suggest feature.
	 * */
	var $searchContainer = $('#navigation .header-search');
	if (SitePreferences.LISTING_SEARCHSUGGEST_LEGACY) {
		searchsuggest.init($searchContainer, Resources.SIMPLE_SEARCH);
	} else {
		searchsuggestbeta.init($searchContainer, Resources.SIMPLE_SEARCH);
	}

	// print handler
	$('.print-page').on('click', function () {
		window.print();
		return false;
	});

	// add show/hide navigation elements
	$('.secondary-navigation .toggle').click(function () {
		$(this).toggleClass('expanded').next('ul').toggle();
	});
	$(window).scroll(function() {
	    if ($(this).scrollTop()>50) 
	     {
	        $('.serviceMessages, .sitewideBanner, .slot-below-usermenu .html-slot-container').hide();
	     }
	    else
	     {
	      $('.serviceMessages, .sitewideBanner, .slot-below-usermenu .html-slot-container').show();
	     }
	});
	

	// add generic toggle functionality
	$('.toggle').next('.toggle-content').hide();
	$('.toggle').click(function (e) {
		e.preventDefault();//JIRA PREV-90 : When click on advance search from gift registry login page, focus is happening towards top of the page.
		$(this).toggleClass('expanded').next('.toggle-content').toggle();
	});

	// subscribe email box
	var $subscribeEmail = $('.subscribe-email');
	if ($subscribeEmail.length > 0)	{
		$subscribeEmail.focus(function () {
			var val = $(this.val());
			if (val.length > 0 && val !== Resources.SUBSCRIBE_EMAIL_DEFAULT) {
				return; // do not animate when contains non-default value
			}

			$(this).animate({color: '#999999'}, 500, 'linear', function () {
				$(this).val('').css('color', '#333333');
			});
		}).blur(function () {
			var val = $.trim($(this.val()));
			if (val.length > 0) {
				return; // do not animate when contains value
			}
			$(this).val(Resources.SUBSCRIBE_EMAIL_DEFAULT)
				.css('color', '#999999')
				.animate({color: '#333333'}, 500, 'linear');
		});
	}

	$('.privacy-policy').on('click', function (e) {
		e.preventDefault();
		dialog.open({
			url: $(e.target).attr('href'),
			options: {
				height: 600
			}
		});
	});

	// main menu toggle
	$('.menu-toggle').on('click', function () {
		$('#wrapper').toggleClass('menu-active');
	});
	$('.menu-category li .menu-item-toggle').on('click', function (e) {
		e.preventDefault();
		var $parentLi = $(e.target).closest('li');
		$parentLi.siblings('li').removeClass('active').find('.menu-item-toggle').removeClass('angle-down active').addClass('angle-right');
		$parentLi.toggleClass('active');
		$(e.target).toggleClass('angle-right angle-down active');
	});
	
	// Fix for tablet
	$('body').on('click', '.mobile_device .has-sub-menu.submenu_inactive', function(e) {
		e.preventDefault();
		$(this).removeClass('submenu_inactive');
		$(this).siblings().addClass('submenu_inactive');
	});
	
	$('.user-account').on('click', function (e) {
		e.preventDefault();
		$(this).parent('.user-info').toggleClass('active');
		$('.mini-cart-content').hide();
	});
	$('.user-panel').on('click', function (e) {
		$(this).toggleClass('active');
	});
	$(".header-search button[type='submit']").on('click', function () {
        $(".fix-head .header-search input").focus();
    });
	
	if ($(window).width() > 1024) {
		$(window).scroll(function() {  
		    var scroll = $(window).scrollTop();

		    if (scroll >= 201) {
		    	if (!$("#wrapper").hasClass("fix-head")) {
		    		$("#wrapper").addClass("fix-head");
		    		$("#header").fadeTo(0,0);
		    		$("#header").fadeTo(700,1);
		    	}
		    }
		    if (scroll <= 199) {
		        $("#wrapper").removeClass("fix-head");
		    }
		});
	}
		
	$('body').on('click','#navpanel .to-top', function(){
		$('html,body').animate({scrollTop: 0}, 500);
	});
	
	
	// fix for firefox issue in right side pagination
	$(window).scroll(function() {
	   if($(window).scrollTop() + $(window).height() > $(document).height() - 15) {
			var totalPage = $('.total-pages').text();
	       $('.page-number').text(totalPage);
	   }
	});
	$(window).scroll(function(){
		$(".backtoTop-button").addClass('backtoTop-button-show');
		if($(window).scrollTop() === 0) {
			$(".backtoTop-button").removeClass('backtoTop-button-show');
		}

		$(".backtoTop-button").click(function(){
				$(window).scrollTop(0);
		});
	});	
	
	$('body').on('click','.footer-left h3', function(){
		if ($(window).width() < 1024) {
			$(this).toggleClass("expand");
			$(this).next('.pipe').slideToggle();
		}
	});
	
	$(window).resize(function(){
		if ($(window).width() > 1023) {
			$(".footer-left h3").removeClass('expand');
			$(".footer-left .pipe").show();
		}
		$('.tiles-container .product-tile').css('height','auto');
		$('.tiles-container .product-tile').syncHeight();
	});
	
	//adding class for mobile device
	if(util.isMobile()) {
		 $('#wrapper').addClass('mobile_device');
	}
	
	//custom selectbox
	util.selectbox();
}
/**
 * @private
 * @function
 * @description Adds class ('js') to html for css targeting and loads js specific styles.
 */
function initializeDom() {
	// add class to html for css targeting
	$('html').addClass('js');
	if (SitePreferences.LISTING_INFINITE_SCROLL) {
		$('html').addClass('infinite-scroll');
	}
	// load js specific styles
	util.limitCharacters();
}

var pages = {
	account: require('./pages/account'),
	cart: require('./pages/cart'),
	checkout: require('./pages/checkout'),
	compare: require('./pages/compare'),
	product: require('./pages/product'),
	registry: require('./pages/registry'),
	search: require('./pages/search'),
	storefront: require('./pages/storefront'),
	wishlist: require('./pages/wishlist'),
	storelocator: require('./pages/storelocator')
};

var app = {
	init: function () {
		if (document.cookie.length === 0) {
			$('<div/>').addClass('browser-compatibility-alert').append($('<p/>').addClass('browser-error').html(Resources.COOKIES_DISABLED)).appendTo('#browser-check');
		}
		initializeDom();
		initializeEvents();

		// init specific global components
		countries.init();
		tooltip.init();
		minicart.init();
		validator.init();
		searchplaceholder.init();
		cq.init();
		// execute page specific initializations
		$.extend(page, window.pageContext);
		var ns = page.ns;
		if (ns && pages[ns] && pages[ns].init) {
			pages[ns].init();
		}
		
		//BISN for G&B
		require('./bisn').init();

		//SPC for G&B
		require('./spc').init();
		
		//GTM
		var gtmevents= require('./gtmevents');
		gtmevents.init();
		
		require('./browsera').init();
	}
};

// general extension functions
(function () {
	String.format = function () {
		var s = arguments[0];
		var i, len = arguments.length - 1;
		for (i = 0; i < len; i++) {
			var reg = new RegExp('\\{' + i + '\\}', 'gm');
			s = s.replace(reg, arguments[i + 1]);
		}
		return s;
	};
})();

// initialize app
$(document).ready(function () {
	app.init();
	
	
	$('button[name$="home-email"]').on('click',function(e) {
		e.preventDefault();
		emailsubscription($(this));
	});
	
	$('button[name$="content-email"]').on('click',function(e) {
		e.preventDefault();
		emailsubscription($(this));
	});
	
	function emailsubscription(currentname){
		var textemail = currentname.closest('#email-alert-signup').find('#email-alert-address').val();
		var txtemail=$.trim(textemail);
		var emailExp = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if(!txtemail.match(emailExp)){
			currentname.closest('form').find('.span-message').addClass('error').text(Resources.VALIDATE_EMAIL);
			return false;
		}
		else{
			currentname.closest('form').find('.span-message').addClass('error').text('');
			var url = Urls.emailUrl;
			url = util.appendParamToURL(url, 'email', txtemail);
		    dialog.open({
		    	url: url,
		    	options: {
					dialogClass : 'custom-popup',
					width : 390
		    	}
		    });
		  //SUB2 Tracking
			if(__s2tQ!=undefined || __s2tQ!="undefined" )
                __s2tQ.push(['storeData',{'Email' : txtemail , 'Optout1P' : '0' , 'Optout3P' : '1'}]);
		} 
	}
	
	//Added for email validation in header login section
	$('button[name$="account-email"]').on('click',function(e) {
		e.preventDefault();
		var txtemail =$('.user-panel form#mail-alert-signup').find('#mail-alert-address').val();
		var emailExp = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if(!txtemail.match(emailExp)){
			$('.spn-message').addClass('error').text(Resources.VALIDATE_EMAIL);
			return false;
		}
		else{
			$('.spn-message').addClass('error').text('');
			var url = Urls.emailUrl;
			url = util.appendParamToURL(url, 'email', txtemail);
			dialog.open({
				url: url,
				options: {
					dialogClass : 'custom-popup',
					width : 390
		    	}
		   });
			
			//SUB2 Tracking
			if(__s2tQ!=undefined || __s2tQ!="undefined" )
                __s2tQ.push(['storeData',{'Email' : txtemail , 'Optout1P' : '0' , 'Optout3P' : '1'}]);

		}    
	});

});

//Launch window popup

$(document).ready(function(){
	if(SitePreferences.LAUNCH_WINDOW_ENABLED){
		if(getCookie("launchwindow") && $("#header").find("select[id$='_destination']").val() != getCookie("launchwindow").toUpperCase()){
			setCookie("launchwindow", '', -1);
		}
		if(!getCookie("launchwindow") || getCookie("launchwindow") == 'true'){
			var url = Urls.launchWindow;
			dialog.open({
				url: url,
				options: {
					dialogClass: "launch-window",
				},
				callback: function () {
						var form = $("#LaunchWindowForm");
						if(form.find("select[id$='_destination']").val() != null && form.find("select[id$='_destination']").val() != ''){
							launchwindow.updateLanguageOptions(form.find("select[id$='_destination']"), form,false);
						}
						if(form.valid()){
							form.find("#submitBtn").removeAttr("disabled");
						}
						form.find("select[id$='_destination']").on('change',function(){
							launchwindow.updateLanguageOptions(form.find("select[id$='_destination']"), form, false);
						});
						form.find("select[id$='_destination'],select[id$='_language']").on('change',function(){
							if(form.valid()){
								form.find("#submitBtn").removeAttr("disabled");
							}else{
								form.find("#submitBtn").attr("disabled","disabled");
							}
						});
						form.find("#submitBtn").click(function(){
							if(form.valid()){
								launchwindow.changeURL(form);
							}
						});
				}
			});
		}else{
			var site = getCookie("launchwindow");
			if(window.location.href.replace(window.location.protocol+"//",'') == window.location.hostname || window.location.href.replace(window.location.protocol+"//",'') == window.location.hostname+"/"){
				window.location.href = "https://"+window.location.hostname+"/"+site;
			}
		}
	}
	$("#header").find("select[id$='_destination']").on('change',function(){
		launchwindow.updateLanguageOptions($("#header").find("select[id$='_destination']"), $("#header").find("#HeaderChangeFlag"),false);
		launchwindow.changeURL($(this).closest("form"));
	});
	$("#header").find("select[id$='_language']").on('change',function(){
		launchwindow.changeURL($(this).closest("form"));
	});
	if($("#header").find("select[id$='_destination'] option[selected=selected]").val() != null && $("#header").find("select[id$='_destination'] option[selected=selected]").val() != ''){
		launchwindow.updateLanguageOptions($("#header").find("select[id$='_destination'] option[selected=selected]"), $("#header").find("#HeaderChangeFlag"),true);
	}
	$('.last-visited .search-result-items, .featured-products .search-result-items').slick({
		  dots: false,
		  infinite: true,
		  speed: 300,
		  slidesToShow: 5,
		  slidesToScroll: 1,
		  responsive: [{
            breakpoint: 1023,
            settings: {
                slidesToShow: 3
            }
        }, {
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                dots: true
            }
        }]
	});
	$(window).on("load", function() {
		if ($("#wrapper").hasClass("pt_cart")) {
			$('.last-visited .grid-tile').syncHeight();
			$('.featured-products .grid-tile').syncHeight();
		}
	});
	$('.banner5-one, .banner5-two, .banner5-three').slick({
		infinite: true,
		speed: 300,
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: true,
		dotsClass: 'custom_paging',
		customPaging: function (slider, i) {
		    return  (i + 1) + '/' + slider.slideCount;
		}
	});
	$('.cat-land-row .product-listing .search-result-items').slick({
		  infinite: true,
		  speed: 300,
		  slidesToShow: 4,
		  slidesToScroll: 4,
		  dots: false,
		  responsive: [{
              breakpoint: 1700,
              settings: {
                  slidesToShow: 3,
                  slidesToScroll: 3
              }
          }, {
        	  breakpoint: 1280,
              settings: {
                  slidesToShow: 2,
                  slidesToScroll: 2
              }
          }, {
              breakpoint: 768,
              settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1,
                  dots: true,
                  dotsClass: 'custom_paging',
                  customPaging: function (slider, i) {
                      return  (i + 1) + '/' + slider.slideCount;
                  },
              }
          }]
	});
	util.countrySelectbox();
	//GB-716
	//Start GB-687
	$(window).on("load", function() {
		if($(window).width() >= 1024) {
			$('#secondary .refinement').find('ul').addClass('refinement-hide');
			$('.refinement.category-refinement').insertBefore('.bottom-apply');
		}
	    ACCORDION.init();
		if(window.pageContext.type != "checkout" && window.pageContext.type != "orderconfirmation" ){
			if($('input[id=logincustomer]').val() != undefined && $('input[id=logincustomer]').val() == 'true'){
				if($('input[id=newlogin]').val() != undefined  && $('input[id=newlogin]').val() == 'true'){
					if(dataLayer != undefined){
						dataLayer.push({
							'event' : 'CustomerState',
							'ecommerce' : {
									'CustomerState' : 'New' 
									}
						});
					}
				}else{
					if(dataLayer != undefined){
						dataLayer.push({
							'event' : 'CustomerState',
							'ecommerce' : {
									'CustomerState' : 'Returned' 
									}
						});
					}
				}			
			}else{
				if(getCookie('Graham&Brown') == ''){
					var cookievalue = parseInt(util.GetDateTime());
					setCookie('Graham&Brown',cookievalue,365);
					if(dataLayer != undefined){
						dataLayer.push({
							'event' : 'CustomerState',
							'ecommerce' : {
									'CustomerState' : 'New' 
									}
						});
					}
				}else{
					var cookietime = parseInt(getCookie('Graham&Brown'));
					var currenttime = parseInt(util.GetDateTime());
					var check = currenttime - cookietime;
					if(check <= SitePreferences.MaxTimePeriod){
						if(dataLayer != undefined){
							dataLayer.push({
								'event' : 'CustomerState',
								'ecommerce' : {
										'CustomerState' : 'NEW' 
										}
							});
						}
					}else{
						if(dataLayer != undefined){
							dataLayer.push({
								'event' : 'CustomerState',
								'ecommerce' : {
										'CustomerState' : 'Returned' 
										}
							});
						}
					}
				}
			}
		}
	});
	//End GB-687
});
//GB-703
var ACCORDION = {
		  full: null,
		  short: null,
		  init: function(){
		    
		    // define description
		    var description = $('.tabs .tab:first-child .inner-tab-content'),
		        firstParagraph = description.find('p, li').first(),
		        text = firstParagraph.text();
		    description[0]._inst = this;
		    // memorize full text
		    this.full = description.html();
		    if(description.find('p:empty').length > 0) {
		    	var firstParagraph = description.find('li').first();
		    	var	text = firstParagraph.text();
		    }

		    // memorize short text if there is need for
		    if(text.length > 350){
		      text = text.substring(0, 350) + '...';
		      firstParagraph.text(text);
		      this.short = firstParagraph[0].outerHTML + '<p class="description-read-more">'+ Resources.READ_MORE +'</p>';
		      
		    }else if(firstParagraph.next().length){
		      if(firstParagraph.next('li').length>0)
		      {
		    	  var newShortData='';
		    	  description.find('li').each(function(index){
		    		  if(index<3)
		    		  {
		    			  newShortData = newShortData + $(this)[0].outerHTML;  
		    		  }else if(index>=3)
		    		  {
		    			  return false;
		    		  }
		    	  });
		    	  this.short = newShortData + '<p class="description-read-more">'+ Resources.READ_MORE +'</p>';
		      }else
		      {
		    	  this.short = firstParagraph[0].outerHTML + '<p class="description-read-more">'+ Resources.READ_MORE +'</p>';  
		      }
		      
		    }else{
		      description.html(this.full + '<div style="font-size:0; height:9px; visibility:hidden;"></div>');
		    }
		    
		    // bind events
		    var labels =  $('.tabs .tab label');
		    for(var i = labels.length; i--;){
		      labels[i]._inst = this;
		    }
		    
		    $('.tabs .tab label').on('click', function(){
		    	if ($(this).closest('.tab').hasClass('checked')) {
		    		$(this).closest('.tab').removeClass('checked');
		    		$('.tabs .tab .tab-content').height(0);
		    	}
		    	else {
		    		$(this).closest('.tab').siblings('.tab').removeClass('checked');
		    		$(this).closest('.tab').addClass('checked');
		    		$('.tabs .tab .tab-content').height(0);
		    		this._inst.call(this);
		    	}
		    });
		    
		    // set short description
		    this.call( $('.tabs .tab:first-child label')[0] );
		  },
		  call: function(labelElement){
		    
		    var inner_tab = $(labelElement).parent().find('.inner-tab-content');
		    
		    // description tab only
		    if( $('.tabs .tab label').index(labelElement) == 0 && this.short){
		      
		      //set short text
		      inner_tab.html( this.short );
		      
		      //bind read-more event
		      $('.description-read-more').on('click', function(){
		        var inner_tab = $(this).parent();
		        inner_tab.html( inner_tab[0]._inst.full );
		        inner_tab.parent().height( inner_tab.height() );
		      });
		    }

		    // set tab adaptive height
		    $(labelElement).next().height( inner_tab.height() );
		  }
};
//getcookie
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

//setcookie
/************** Cookie Handling *****************/

function setCookie(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+""+expires+"; path=/";
	document.cookie = name+"="+value+expires+"; path=/";
}
//launch window
var launchwindow = {
	updateLanguageOptions: function (elem,form,keeplocale) {
		var site = window.countries[elem.val()],arrHtml = [];
		var lang;
		if(elem.val() == "ROW"){
			form.find(".shipmentnotification").html(Resources.DEFAULT_SHIPMENT_NOTIFICATION1+"<b>"+Resources.DEFAULT_SHIPMENT_NOTIFICATION2+"</b>"+Resources.DEFAULT_SHIPMENT_NOTIFICATION3);
		}else{
			form.find(".shipmentnotification").html("");
		}
		if(site != undefined && site != null){
			/*if(form.find(".currency").length > 0){
				form.find(".currency").html(site.currency);
			}else if(form.find("input[name='currency']").length > 0){
				form.find("select[id$='_language']").val(site.currency);
			}*/
			if(form.find(".selectedCountry").length > 0){
				form.find(".selectedCountry").addClass(elem.val());
			}
			var selected = form.find("select[id$='_language']").val();
			for (lang in site.language) {
				if(site.language[lang].ID == selected  && keeplocale){
					arrHtml.push('<option value="' + site.language[lang].ID + '" selected="selected">' + site.language[lang].value + "</option>");
				}else{
					arrHtml.push('<option value="' + site.language[lang].ID + '">' + site.language[lang].value + "</option>");
				}
			}
			form.find("select[id$='_language']").html(arrHtml.join(""));
		}
		//custom selectbox
		util.countrySelectbox();
	},
	changeURL: function (form) {
		var url = Urls.changeLocale;
		var dest = form.find("select[id$='_destination']").val();
		/*if(form.find("select[id$='_destination'] option[selected=selected]").val() != undefined && form.find("select[id$='_destination'] option[selected=selected]").val() != ''){
			dest = form.find("select[id$='_destination'] option[selected=selected]").val();
		}else{
			dest = form.find("select[id$='_destination']").val();
		} */
		var lang = form.find("select[id$='_language']").val();;
		/*if(form.find("select[id$='_language'] option[selected=selected]").val() != undefined && form.find("select[id$='_language'] option[selected=selected]").val() != ''){
			lang = form.find("select[id$='_language'] option[selected=selected]").val();
		}else{
			lang = form.find("select[id$='_language']").val();
		} */
		//var currency = "";
		url = util.appendParamToURL(url, 'destination', dest);
		url = util.appendParamToURL(url, 'language', lang);
		/*if(form.find("input[name='currency']").length > 0){
			currency = form.find("select[id$='_language']").val();
		}else if (form.find(".currency").length > 0) {
			currency = form.find(".currency").html();
		}
		url = util.appendParamToURL(url, 'currency', currency);*/
		url = util.appendParamToURL(url, 'format', 'ajax');
		$.ajax({
			type: 'GET',
			dataType: 'json',
			url: url,
			success: function (response) {
				if(response.success){
					setCookie("launchwindow", form.find("select[id$='_destination']").val().toLowerCase(), 60);
					dialog.close();
					window.location.href = response.redirectURL;
				}
			}
		});
	}
};
/*$(window).bind("pageshow", function(event) {
	if (event.originalEvent.persisted) {
	location.reload();
	}
	});*/
window.onpageshow = function(event) {
    if (event.persisted) {
        window.location.reload() 
    }
};
$(window).bind("pageshow", function(event) {
    if (event.originalEvent.persisted) {
      alert("From back / forward cache.");
    }
});
window.onpageshow = function(event) {
    if (event.persisted) {
        alert("From back / forward cache.");
    }
};
util.fancyScroll();

//Session timeout handling
$(document).ready(function () {
	var cookiecreation = new Date();
	var dateObj = new Date();
	dateObj.setTime(dateObj.getTime() + (1/24*24*30*60*1000));    
	document.cookie="sessionTime="+cookiecreation+"; expires="+dateObj.toUTCString()+"; path=/";
  	//(function($this) {
	window.setTimeout(
		function(){
			var currentDate = new Date();
			// var min = currentDate.getMinutes();
			// currentDate.setMinutes(min - 1);
			// var x =currentDate;
			var nameEQ = "sessionTime=";
			var ca = document.cookie.split(';');
			var final = null;
			for(var i=0;i < ca.length;i++) {
				var c = ca[i];
				while (c.charAt(0)==' ') c = c.substring(1,c.length);
				if (c.indexOf(nameEQ) == 0) { 
					final = c.substring(nameEQ.length,c.length);
		        }
			}					   
			var finalDate = new Date(final);
			var finalmin = finalDate.getMinutes();
			finalDate.setMinutes(finalmin + 28);			   
			   
			if(Date.parse(currentDate) >= Date.parse(finalDate)){
				window.setTimeout(function(){
					window.location.reload();						
				},60000);
			}
		}, 
	1680000);
});
//ipad touch color change
$('li a').on('touchstart', function(event){
	$("this").css("-webkit-tap-highlight-color","#63b0bb");
});


},{"./bisn":3,"./browsera":5,"./cookieprivacy":7,"./countries":8,"./cq":9,"./dialog":10,"./dialogify":11,"./gtmevents":14,"./jquery-ext":15,"./minicart":16,"./page":17,"./pages/account":18,"./pages/cart":19,"./pages/checkout":23,"./pages/compare":26,"./pages/product":30,"./pages/registry":34,"./pages/search":35,"./pages/storefront":36,"./pages/storelocator":37,"./pages/wishlist":38,"./searchplaceholder":44,"./searchsuggest":46,"./searchsuggest-beta":45,"./spc":47,"./tooltip":51,"./util":52,"./validator":53}],3:[function(require,module,exports){
'use strict';

var $cache = {};
var dialog = require('./dialog'),
    ajax = require('./ajax'),
    util = require('./util');

function initializeCache() {
    $cache.main = $('#main');
    $cache.bisnButton = $('.backInStockNotification .bisnButton');
}

function initializeEvents() {
    $cache.bisnButton.on("click", function(e) {
        e.preventDefault();
        var $this = $(this).closest('form');
        var invalidEmail = Resources.EMAILNOTIFICATIONFAIL;
        var pid = $this.find('#pid').val();
        var email = $this.find('.bisnemail').val();
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if(reg.test(email) == false || email == null) {
            $this.find('#bisnemailnstatus').text('');
            $this.find('.email').addClass('error');
            $this.find('span.error').text(invalidEmail);
        } else {
            $this.find('.email').removeClass('error');
            var url = util.appendParamsToUrl(Urls.emailNotification, {
                bisnpid: pid,
                bisnemail: email,
                format: "ajax"
            });
            ajax.getJson({
                url: url,
                dataType: 'json',
                callback: function(data) {
                    if(data.success) {
                        $this.find('.bisnemail').removeClass('error');
                        $this.find('#bisnemailnstatus').text('');
                        var data = "<div class='bisn'>"+ Resources.EMAILNOTIFICATIONSUCCESS +"</div>";
						dialog.open({
							html:data,
							options: {
								dialogClass : 'bisn custom-popup',
								width : 390
							},
							callback: function () {
							}
						});
                    } else {
                        $this.find('#bisnemailnstatus').text(Resources.EMAILNOTIFICATIONFAIL).css("color", "RED");
                    }
                }
            });
        }

    });
    $('.bisnemail').on("keypress", function(e) {
        var $this = $(this).closest('form');
        $this.find("#bisnemailnstatus").text('');
    });
}


exports.init = function() {
    initializeCache();
    initializeEvents();
}
},{"./ajax":1,"./dialog":10,"./util":52}],4:[function(require,module,exports){
'use strict';

var dialog = require('./dialog'),
	page = require('./page'),
	util = require('./util');

var selectedList = [];
var maxItems = 1;
var bliUUID = '';

/**
 * @private
 * @function
 * description Gets a list of bonus products related to a promoted product
 */
function getBonusProducts() {
	var o = {};
	o.bonusproducts = [];

	var i, len;
	for (i = 0, len = selectedList.length; i < len; i++) {
		var p = {
			pid: selectedList[i].pid,
			qty: selectedList[i].qty,
			options: {}
		};
		var a, alen, bp = selectedList[i];
		if (bp.options) {
			for (a = 0, alen = bp.options.length; a < alen; a++) {
				var opt = bp.options[a];
				p.options = {optionName:opt.name, optionValue:opt.value};
			}
		}
		o.bonusproducts.push({product:p});
	}
	return o;
}

var selectedItemTemplate = function (data) {
	var attributes = '';
	for (var attrID in data.attributes) {
		var attr = data.attributes[attrID];
		attributes += '<li data-attribute-id="' + attrID + '">\n';
		attributes += '<span class="display-name">' + attr.displayName + '</span>: ';
		attributes += '<span class="display-value">' + attr.displayValue + '</span>\n';
		attributes += '</li>';
	}
	attributes += '<li class="item-qty">\n';
	attributes += '<span class="display-name">Qty</span>: ';
	attributes += '<span class="display-value">' + data.qty + '</span>';
	return [
		'<li class="selected-bonus-item" data-uuid="' + data.uuid + '" data-pid="' + data.pid + '">',
		'<i class="remove-link fa fa-remove" title="Remove this product" href="#"></i>',
		'<div class="item-name">' + data.name + '</div>',
		'<ul class="item-attributes">',
		attributes,
		'<ul>',
		'<li>'
	].join('\n');
};

// hide swatches that are not selected or not part of a Product Variation Group
var hideSwatches = function () {
	$('.bonus-product-item .swatches li').not('.selected').not('.variation-group-value').hide();
	// prevent unselecting the selected variant
	$('.bonus-product-item .swatches .selected').on('click', function () {
		return false;
	});
};

/**
 * @private
 * @function
 * @description Updates the summary page with the selected bonus product
 */
function updateSummary() {
	var $bonusProductList = $('#bonus-product-list');
	// JIRA PREV-519 : Remove link issue in bonus selection overlay : calculating quantity of bonus product elligibility
	$bonusProductList.find('li.selected-bonus-item').remove();
	if (selectedList.length === 0) {
		$bonusProductList.find('li.selected-bonus-item').remove();
	} else {
		var ulList = $bonusProductList.find('ul.selected-bonus-items').first();
		var i, len,qty=0;//JIRA PREV-519 : Remove link issue in bonus selection overlay : calculating quantity of bonus product elligibility
		for (i = 0, len = selectedList.length; i < len; i++) {
			var item = selectedList[i];
			var li = selectedItemTemplate(item);
			$(li).appendTo(ulList);
			qty = qty + selectedList[i].qty;//JIRA PREV-519 : Remove link issue in bonus selection overlay : calculating quantity of bonus product elligibility
		}
	}

	// get remaining item count
	var remain = maxItems - selectedList.length;
	$bonusProductList.find('.bonus-items-available').text(remain);
	if (remain <= 0) {
		$bonusProductList.find('.select-bonus-item').attr('disabled', 'disabled');
	} else {
		$bonusProductList.find('.select-bonus-item').removeAttr('disabled');
	}
	/*Start JIRA PREV-519 : Remove link issue in bonus selection overlay */
	var remain = maxItems - qty;
	if(remain <= 0){
		$bonusProductList.find('.select-bonus-item').attr('disabled', 'disabled');
	}else{
		$bonusProductList.find('.select-bonus-item').removeAttr('disabled');
	}
	/*End JIRA PREV-519*/
}

function initializeGrid () {
	var $bonusProduct = $('#bonus-product-dialog'),
		$bonusProductList = $('#bonus-product-list'),
	bliData = $bonusProductList.data('line-item-detail');
	maxItems = bliData.maxItems;
	bliUUID = bliData.uuid;

	if (bliData.itemCount >= maxItems) {
		$bonusProductList.find('.select-bonus-item').attr('disabled', 'disabled');
	}

	var cartItems = $bonusProductList.find('.selected-bonus-item');
	cartItems.each(function () {
		var ci = $(this);
		var product = {
			uuid: ci.data('uuid'),
			pid: ci.data('pid'),
			qty: ci.find('.item-qty').text(),
			name: ci.find('.item-name').html(),
			attributes: {}
		};
		var attributes = ci.find('ul.item-attributes li');
		attributes.each(function () {
			var li = $(this);
			product.attributes[li.data('attributeId')] = {
				displayName:li.children('.display-name').html(),
				displayValue:li.children('.display-value').html()
			};
		});
		selectedList.push(product);
	});

	$bonusProductList.on('click', '.bonus-product-item a[href].swatchanchor', function (e) {
		e.preventDefault();
		var url = this.href,
			$this = $(this);
		url = util.appendParamsToUrl(url, {
			'source': 'bonus',
			'format': 'ajax'
		});
		$.ajax({
			url: url,
			success: function (response) {
				$this.closest('.bonus-product-item').empty().html(response);
				hideSwatches();
			}
		});
	})
	.on('change', '.input-text', function () {
		/* Start JIRA PREV-519 : Remove link issue in bonus selection overlay */
		updateSummary();
		//$bonusProductList.find('.select-bonus-item').removeAttr('disabled');
		/*End JIRA PREV-519*/
		$(this).closest('.bonus-product-form').find('.quantity-error').text('');
	})
	.on('click', '.select-bonus-item', function (e) {
		e.preventDefault();
		if (selectedList.length >= maxItems) {
			$bonusProductList.find('.select-bonus-item').attr('disabled', 'disabled');
			$bonusProductList.find('.bonus-items-available').text('0');
			return;
		}

		var form = $(this).closest('.bonus-product-form'),
			detail = $(this).closest('.product-detail'),
			uuid = form.find('input[name="productUUID"]').val(),
			qtyVal = form.find('input[name="Quantity"]').val(),
			qty = (isNaN(qtyVal)) ? 1 : (+qtyVal);

		if (qty > maxItems) {
			$bonusProductList.find('.select-bonus-item').attr('disabled', 'disabled');
			form.find('.quantity-error').text(Resources.BONUS_PRODUCT_TOOMANY);
			return;
		}

		var product = {
			uuid: uuid,
			pid: form.find('input[name="pid"]').val(),
			qty: qty,
			name: detail.find('.product-name').text(),
			attributes: detail.find('.product-variations').data('attributes'),
			options: []
		};

		var optionSelects = form.find('.product-option');

		optionSelects.each(function () {
			product.options.push({
				name: this.name,
				value: $(this).val(),
				display: $(this).children(':selected').first().html()
			});
		});
		selectedList.push(product);
		updateSummary();
	})
	.on('click', '.remove-link', function (e) {
		e.preventDefault();
		var container = $(this).closest('.selected-bonus-item');
		if (!container.data('uuid')) { return; }

		var uuid = container.data('uuid');
		var i, len = selectedList.length;
		for (i = 0; i < len; i++) {
			if (selectedList[i].uuid === uuid) {
				selectedList.splice(i, 1);
				break;
			}
		}
		updateSummary();
	})
	.on('click', '.add-to-cart-bonus', function (e) {
		e.preventDefault();
		var url = util.appendParamsToUrl(Urls.addBonusProduct, {bonusDiscountLineItemUUID: bliUUID});
		var bonusProducts = getBonusProducts();
		if (bonusProducts.bonusproducts[0].product.qty > maxItems) {
			bonusProducts.bonusproducts[0].product.qty = maxItems;
		}
		// make the server call
		$.ajax({
			type: 'POST',
			dataType: 'json',
			cache: false,
			contentType: 'application/json',
			url: url,
			data: JSON.stringify(bonusProducts)
		})
		.done(function () {
			// success
			page.refresh();
		})
		.fail(function (xhr, textStatus) {
			// failed
			if (textStatus === 'parsererror') {
				window.alert(Resources.BAD_RESPONSE);
			} else {
				window.alert(Resources.SERVER_CONNECTION_ERROR);
			}
		})
		.always(function () {
			$bonusProduct.dialog('close');
		});
	});
}

var bonusProductsView = {
	/**
	 * @function
	 * @description Open the list of bonus products selection dialog
	 */
	show: function (url) {
		var $bonusProduct = $('#bonus-product-dialog');
		// create the dialog
		dialog.open({
			target: $bonusProduct,
			url: url,
			options: {
				width: 795,
				title: Resources.BONUS_PRODUCTS
			},
			callback: function () {
				initializeGrid();
				hideSwatches();
			}
		});
	},
	/**
	 * @function
	 * @description Open bonus product promo prompt dialog
	 */
	loadBonusOption: function () {
		var	self = this,
			bonusDiscountContainer = document.querySelector('.bonus-discount-container');
		if (!bonusDiscountContainer) { return; }

		// get the html from minicart, then trash it
		var bonusDiscountContainerHtml = bonusDiscountContainer.outerHTML;
		bonusDiscountContainer.parentNode.removeChild(bonusDiscountContainer);

		dialog.open({
			html: bonusDiscountContainerHtml,
			options: {
				width: 400,
				title: Resources.BONUS_PRODUCT,
				buttons: [{
					text: Resources.SELECT_BONUS_PRODUCTS,
					click: function () {
						var uuid = $('.bonus-product-promo').data('lineitemid'),
							url = util.appendParamsToUrl(Urls.getBonusProducts, {
								bonusDiscountLineItemUUID: uuid,
								source: 'bonus'
							});
						$(this).dialog('close');
						self.show(url);
					}
				}, {
					text: Resources.NO_THANKS,
					click: function () {
						$(this).dialog('close');
					}
				}]
			},
			callback: function () {
				// show hide promo details
				$('.show-promo-details').on('click', function () {
					$('.promo-details').toggleClass('visible');
				});
			}
		});
	}
};

module.exports = bonusProductsView;

},{"./dialog":10,"./page":17,"./util":52}],5:[function(require,module,exports){
'use strict';

function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function runBrowseraTest(){
    
    var hover_selector = decodeURI(getUrlVars()['hover']);
    if(hover_selector != ''){
    	   hover_selector = hover_selector.replace(' eq ','=');
           if( $(hover_selector).length>0){
                  $(hover_selector).trigger('mouseenter');
           }
    }
    var click_selector = decodeURI(getUrlVars()['click']);
    if(click_selector != ''){
           click_selector = click_selector.replace(' eq ','=');
           if( $(click_selector).length>0){
                  $(click_selector).trigger('click');
           }
    }
}

exports.init = function(){
	runBrowseraTest();
};
},{}],6:[function(require,module,exports){
'use strict';

var page = require('./page'),
    util = require('./util'),
    TPromise = require('promise');

var _currentCategory = '',
    MAX_ACTIVE = 6;

/**
 * @private
 * @function
 * @description Verifies the number of elements in the compare container and updates it with sequential classes for ui targeting
 */
function refreshContainer() {
        var $compareContainer = $('.compare-items');
        var $compareItems = $compareContainer.find('.compare-item');
        var numActive = $compareItems.filter('.active').length;

        if(numActive < 2) {
            $('#compare-items-button').attr('disabled', 'disabled');
        } else {
            $('#compare-items-button').removeAttr('disabled');
        }
        /*Start JIRA PREV-234 : PLP - Not able to remove individual product from the compare grid from PLP.*/
        for(var i = 0; i < $compareItems.length; i++) {
            $($compareItems[i]).find('.compare-item-number').text(i + 1);
        }
        /*End JIRA PREV-234*/
        $compareContainer.toggle(numActive > 0);
    }
    /**
     * @private
     * @function
     * @description Adds an item to the compare container and refreshes it
     */
function addToList(data) {
        // get the first compare-item not currently active
        var $item = $('.compare-items .compare-item').not('.active').first(),
            $productTile = $('#' + data.uuid);

        if($item.length === 0) {
            if($productTile.length > 0) {
                $productTile.find('.compare-check')[0].checked = false;
            }
            window.alert(Resources.COMPARE_ADD_FAIL);
            return;
        }

        // if already added somehow, return
        if($('[data-uuid="' + data.uuid + '"]').length > 0) {
            return;
        }
        // set as active item
        $item.addClass('active')
            .attr('data-uuid', data.uuid)
            .attr('data-itemid', data.itemid)
            .data('uuid', data.uuid)
            .data('itemid', data.itemid)
            .append($(data.img).clone().addClass('compare-item-image'));
    }
    /**
     * @private
     * @function
     * description Removes an item from the compare container and refreshes it
     */
function removeFromList($item) {
    if($item.length === 0) {
        return;
    }
    // remove class, data and id from item
    $item.removeClass('active')
        .removeAttr('data-uuid')
        .removeAttr('data-itemid')
        .data('uuid', '')
        .data('itemid', '')
        // remove the image
        .find('.compare-item-image').remove();
    
	    /*Start JIRA PREV-234 : PLP - Not able to remove individual product from the compare grid from PLP.*/
	    var $cloneItem = $item.clone();
	    $item.remove();
	    $cloneItem.appendTo('.compare-items-panel');
	    refreshContainer();
	    /*End JIRA PREV-234*/
}

function addProductAjax(args) {
    var promise = new TPromise(function(resolve, reject) {
        $.ajax({
            url: Urls.compareAdd,
            data: {
                pid: args.itemid,
                category: _currentCategory
            },
            dataType: 'json'
        }).done(function(response) {
            if(!response || !response.success) {
                reject(new Error(Resources.COMPARE_ADD_FAIL));
            } else {
                resolve(response);
                initializeEvents(); // JIRA PREV-234 : PLP - Not able to remove individual product from the compare grid from PLP.
            }
        }).fail(function(jqxhr, status, err) {
            reject(new Error(err));
        });
    });
    return promise;
}

function removeProductAjax(args) {
    var promise = new TPromise(function(resolve, reject) {
        $.ajax({
            url: Urls.compareRemove,
            data: {
                pid: args.itemid,
                category: _currentCategory
            },
            dataType: 'json'
        }).done(function(response) {
            if(!response || !response.success) {
                reject(new Error(Resources.COMPARE_REMOVE_FAIL));
            } else {
                resolve(response);
            }
        }).fail(function(jqxhr, status, err) {
            reject(new Error(err));
        });
    });
    return promise;
}

function shiftImages() {
    return new TPromise(function(resolve) {
        var $items = $('.compare-items .compare-item');
        $items.each(function(i, item) {
            var $item = $(item);
            // last item
            if(i === $items.length - 1) {
                return removeFromList($item);
            }
            var $next = $items.eq(i);//JIRA PREV-261 : PLP: When more than 6 product compared then Comparesection not displaying all the product images. Removed +1
            if($next.hasClass('active')) {
                // remove its own image
                $next.find('.compare-item-image').detach().appendTo($item);
                $item.addClass('active')
                    .attr('data-uuid', $next.data('uuid'))
                    .attr('data-itemid', $next.data('itemid'))
                    .data('uuid', $next.data('uuid'))
                    .data('itemid', $next.data('itemid'));
            }
        });
        resolve();
    });
}

/**
 * @function
 * @description Adds product to the compare table
 */
function addProduct(args) {
    var promise;
    var $items = $('.compare-items .compare-item');
    var $cb = $(args.cb);
    var numActive = $items.filter('.active').length;
    if(numActive === MAX_ACTIVE) {
        if(!window.confirm(Resources.COMPARE_CONFIRMATION)) {
            $cb[0].checked = false;
            return;
        }

        // remove product using id
        var $firstItem = $items.first();
        promise = removeItem($firstItem).then(function() {
            return shiftImages();
        });
    } else {
        promise = TPromise.resolve(0);
    }
    return promise.then(function() {
        return addProductAjax(args).then(function() {
            addToList(args);
            if($cb && $cb.length > 0) {
                $cb[0].checked = true;
            }
            refreshContainer();
        });
    }).then(null, function() {
        if($cb && $cb.length > 0) {
            $cb[0].checked = false;
        }
    });
}

/**
 * @function
 * @description Removes product from the compare table
 * @param {object} args - the arguments object should have the following properties: itemid, uuid and cb (checkbox)
 */
function removeProduct(args) {
    var $cb = args.cb ? $(args.cb) : null;
    return removeProductAjax(args).then(function() {
        var $item = $('[data-uuid="' + args.uuid + '"]');
        removeFromList($item);
        if($cb && $cb.length > 0) {
            $cb[0].checked = false;
        }
        refreshContainer();
    }, function() {
        if($cb && $cb.length > 0) {
            $cb[0].checked = true;
        }
    });
}

function removeItem($item) {
    var uuid = $item.data('uuid'),
        $productTile = $('#' + uuid);
    return removeProduct({
        itemid: $item.data('itemid'),
        uuid: uuid,
        cb: ($productTile.length === 0) ? null : $productTile.find('.compare-check')
    });
}

/**
 * @private
 * @function
 * @description Initializes the DOM-Object of the compare container
 */
function initializeDom() {
    var $compareContainer = $('.compare-items');
    _currentCategory = $compareContainer.data('category') || '';
    var $active = $compareContainer.find('.compare-item').filter('.active');
    $active.each(function() {
        var $productTile = $('#' + $(this).data('uuid'));
        if($productTile.length === 0) {
            return;
        }
        $productTile.find('.compare-check')[0].checked = true;
    });
    // set container state
    refreshContainer();
}

/**
 * @private
 * @function
 * @description Initializes the events on the compare container
 */
function initializeEvents() {
    // add event to buttons to remove products
    $('.compare-item').on('click', '.compare-item-remove', function() {
        removeItem($(this).closest('.compare-item'));
    });

    // Button to go to compare page
    $('#compare-items-button').on('click', function() {
        page.redirect(util.appendParamToURL(Urls.compareShow, 'category', _currentCategory));
    });

    // Button to clear all compared items
    // rely on refreshContainer to take care of hiding the container
    $('#clear-compared-items').on('click', function() {
        $('.compare-items .active').each(function() {
            removeItem($(this));
        });
    });
}

exports.init = function() {
    initializeDom();
    initializeEvents();
};

exports.addProduct = addProduct;
exports.removeProduct = removeProduct;
},{"./page":17,"./util":52,"promise":59}],7:[function(require,module,exports){
'use strict';

var dialog = require('./dialog');

/**
 * @function cookieprivacy	Used to display/control the scrim containing the cookie privacy code
 **/
module.exports = function () {
	/**
	 * If we have not accepted cookies AND we're not on the Privacy Policy page, then show the notification
	 * NOTE: You will probably want to adjust the Privacy Page test to match your site's specific privacy / cookie page
	 */
	
	if (SitePreferences.COOKIE_HINT === true && document.cookie.indexOf('dw_cookies_accepted') < 0) {
		// check for privacy policy page
		if ($('.privacy-policy').length === 0) {
			dialog.open({
				url: Urls.cookieHint,
				options: {
					closeOnEscape: false,
					dialogClass: 'no-close',
					modal: false,
				},
				callback: function(){
					$('.privacypolicy-gb').on('click',function(){
						dialog.close();
						enableCookies();
					});
				}
			});
		}
	} else {
		// Otherwise, we don't need to show the asset, just enable the cookies
		enableCookies();
	}

	function enableCookies() {
		if (document.cookie.indexOf('dw=1') < 0) {
			document.cookie = 'dw=1; path=/';
		}
		if (document.cookie.indexOf('dw_cookies_accepted') < 0) {
			document.cookie = 'dw_cookies_accepted=1; path=/';
		}
	}
};

},{"./dialog":10}],8:[function(require,module,exports){
'use strict';

exports.init = function init () {
	$('.country-selector .current-country').on('click', function () {
		$('.country-selector .selector').toggleClass('active');
		$(this).toggleClass('selector-active');
	});
	// set currency first before reload
	$('.country-selector .selector .locale').on('click', function (e) {
		e.preventDefault();
		var url = this.href;
		var currency = this.getAttribute('data-currency');
		$.ajax({
			dataType: 'json',
			url: Urls.setSessionCurrency,
			data: {
				format: 'ajax',
				currencyMnemonic: currency
			}
		})
		.done(function (response) {
			if (!response.success) {
				throw new Error('Unable to set currency');
			}
			window.location.href = url;
		});
	});
};

},{}],9:[function(require,module,exports){
'use strict';
/* global CQuotient */

function clickThruAfter() {
	var recommenderName = localStorage.getItem('cq.recommenderName');
	var currentProductId = $('[itemprop="productID"]').data('masterid') || '';
	if (!recommenderName) {return;}
	var anchors;
	if (localStorage.getItem('cq.anchors')) {
		anchors = localStorage.getItem('cq.anchors');
		localStorage.removeItem('cq.anchors');
	}
	localStorage.removeItem('cq.recommenderName');
	if (window.CQuotient) {
		CQuotient.activities.push({
			activityType: 'clickReco',
			parameters: {
				cookieId: CQuotient.getCQCookieId(),
				userId: CQuotient.getCQUserId(),
				recommenderName: recommenderName,
				anchors: anchors || '',
				products: {
					id: currentProductId
				}
			}
		});
	}
}

exports.init = function () {
	// set cookie before click through from product tile
	$('body').on('click', '.product-tile[data-recommendername] a', function () {
		// if currently on a product page, send its productId as the anchor
		if (window.pageContext.type === 'product') {
			localStorage.setItem('cq.anchors', $('[itemprop="productID"]').data('masterid') || '');
		}
		var recommenderName = $(this).parents('.product-tile').data('recommendername');
		localStorage.setItem('cq.recommenderName', recommenderName);
	});

	clickThruAfter();
};

},{}],10:[function(require,module,exports){
'use strict';

var ajax = require('./ajax'),
	util = require('./util'),
	_ = require('lodash'),
	imagesLoaded = require('imagesloaded');

var dialog = {
	/**
	 * @function
	 * @description Appends a dialog to a given container (target)
	 * @param {Object} params  params.target can be an id selector or an jquery object
	 */
	create: function (params) {
		var $target, id;

		if (_.isString(params.target)) {
			if (params.target.charAt(0) === '#') {
				$target = $(params.target);
			} else {
				$target = $('#' + params.target);
			}
		} else if (params.target instanceof jQuery) {
			$target = params.target;
		} else {
			$target = $('#dialog-container');
		}

		// if no element found, create one
		if ($target.length === 0) {
			if ($target.selector && $target.selector.charAt(0) === '#') {
				id = $target.selector.substr(1);
				$target = $('<div>').attr('id', id).addClass('dialog-content').appendTo('body');
			}
		}

		// create the dialog
		this.$container = $target;
		this.$container.dialog(_.merge({}, this.settings, params.options || {}));
		return this.$container; // PREVAIL-Added Return value.
	},
	/**
	 * @function
	 * @description Opens a dialog using the given url (params.url) or html (params.html)
	 * @param {Object} params
	 * @param {Object} params.url should contain the url
	 * @param {String} params.html contains the html of the dialog content
	 */
	open: function (params) {
		// close any open dialog
		this.close();
		this.create(params);
		this.replace(params);
	},
	/**
	 * @description populate the dialog with html content, then open it
	 **/
	openWithContent: function (params) {
		var content, position, callback;

		if (!this.$container) { return; }
		content = params.content || params.html;
		if (!content) { return; }
		this.$container.empty().html(content);
		if (!this.$container.dialog('isOpen')) {
			this.$container.dialog('open');
		}
		
		if (params.options) {
			position = params.options.position;
		}
		if (!position) {
			position = this.settings.position;
		}
		imagesLoaded(this.$container).on('done', function () {
			this.$container.dialog('option', 'position', position);
		}.bind(this));

		callback = (typeof params.callback === 'function') ? params.callback : function () {};
		callback();
	},
	/**
	 * @description Replace the content of current dialog
	 * @param {object} params
	 * @param {string} params.url - If the url property is provided, an ajax call is performed to get the content to replace
	 * @param {string} params.html - If no url property is provided, use html provided to replace
	 */
	replace: function (params) {
		if (!this.$container) {
			return;
		}
		if (params.url) {
			params.url = util.appendParamToURL(params.url, 'format', 'ajax');
			ajax.load({
				url: params.url,
				data: params.data,
				callback: function (response) {
					params.content = response;
					this.openWithContent(params);
				}.bind(this)
			});
		} else if (params.html) {
			this.openWithContent(params);
		}
	},
	/**
	 * @function
	 * @description Closes the dialog
	 */
	close: function () {
		if (!this.$container) {
			return;
		}
		this.$container.dialog('close');
	},
	/**
	 * @function
	 * @description Submits the dialog form with the given action
	 * @param {String} The action which will be triggered upon form submit
	 */
	submit: function (action) {
		var $form = this.$container.find('form:first');
		// set the action
		$('<input/>').attr({
			name: action,
			type: 'hidden'
		}).appendTo($form);
		// serialize the form and get the post url
		var data = $form.serialize();
		var url = $form.attr('action');
		// make sure the server knows this is an ajax request
		if (data.indexOf('ajax') === -1) {
			data += '&format=ajax';
		}
		// post the data and replace current content with response content
		$.ajax({
			type: 'POST',
			url: url,
			data: data,
			dataType: 'html',
			success: function (html) {
				this.$container.html(html);
			}.bind(this),
			failure: function () {
				window.alert(Resources.SERVER_ERROR);
			}
		});
	},
	exists: function () {
		return this.$container && (this.$container.length > 0);
	},
	isActive: function () {
		return this.exists() && (this.$container.children.length > 0);
	},
	settings: {
		autoOpen: false,
		height: 'auto',
		modal: true,
		overlay: {
			opacity: 0.5,
			background: 'black'
		},
		resizable: false,
		title: '',
		width: '800',
		close: function () {
			$(this).dialog('close');
		},
		position: {
			my: 'center',
			at: 'center',
			of: window,
			collision: 'flipfit'
		}
	}
};

module.exports = dialog;

},{"./ajax":1,"./util":52,"imagesloaded":57,"lodash":58}],11:[function(require,module,exports){
'use strict';

var ajax = require('./ajax'),
	util = require('./util'),
	tooltip = require('./tooltip'),
	validator = require('./validator'),
	dialog = require('./dialog');

var setDialogify = function(e) {
    e.preventDefault();
    var actionSource = $(this),
        dlgAction = $(actionSource).data("dlg-action") || {}, // url, target, isForm
        dlgOptions = $.extend({}, dialog.settings, $(actionSource).data("dlg-options") || {});

    dlgOptions.title = dlgOptions.title || $(actionSource).attr("title") || "";

    var url = dlgAction.url // url from data
        || (dlgAction.isForm ? $(actionSource).closest("form").attr("action") : null) // or url from form action if isForm=true
        || $(actionSource).attr("href"); // or url from href

    if (!url) {
        return;
    }

    var form = jQuery(this).parents('form');
    var method = form.attr("method") || "POST";

    if (actionSource[0].tagName == "BUTTON" && !form.valid() || actionSource[0].tagName == "INPUT" && !form.valid()) {
        return false;
    }

    // if this is a content link, update url from Page-Show to Page-Include
    if ($(this).hasClass("attributecontentlink")) {
        var uri = util.getUri(url);
        url = Urls.pageInclude + uri.query;
    }
    if (method && method.toUpperCase() == "POST") {
        var postData = form.serialize() + "&" + jQuery(this).attr("name") + "=submit";
    } else {
        if (url.indexOf('?') == -1) {
            url += '?';
        } else {
            url += '&'
        }
        url += form.serialize();
        url = util.appendParamToURL(url, jQuery(this).attr('name'), "submit");
    }

    var dlg = dialog.create({
        target: dlgAction.target,
        options: dlgOptions
    });

    ajax.load({
        url: $(actionSource).attr("href") || $(actionSource).closest("form").attr("action"),
        target: dlg,
        callback: function() {
            dlg.dialog("open"); // open after load to ensure dialog is centered
            validator.init();
            tooltip.init(); 
        },
        data: !$(actionSource).attr("href") ? postData : null,
        type: method 

    });
}

exports.setDialogify = setDialogify;
},{"./ajax":1,"./dialog":10,"./tooltip":51,"./util":52,"./validator":53}],12:[function(require,module,exports){
'use strict';

var ajax = require('./ajax'),
	util = require('./util');
/**
 * @function
 * @description Load details to a given gift certificate
 * @param {String} id The ID of the gift certificate
 * @param {Function} callback A function to called
 */
//PREVAIL-Added pin to handle Custom GC.
exports.checkBalance = function (id, pin, callback) {
	// load gift certificate details
	var url = util.appendParamToURL(Urls.giftCardCheckBalance, 'giftCertificateID', id);
	var url = util.appendParamToURL(url, 'gcPin', pin); //PREVAIL-Added pin to handle Custom GC.
	ajax.getJson({
		url: url,
		callback: callback
	});
};

},{"./ajax":1,"./util":52}],13:[function(require,module,exports){
'use strict';

var ajax = require('./ajax'),
	minicart = require('./minicart'),
	util = require('./util');

var setAddToCartHandler = function (e) {
	e.preventDefault();
	var form = $(this).closest('form');

	var options = {
		url: util.ajaxUrl(form.attr('action')),
		method: 'POST',
		cache: false,
		data: form.serialize()
	};
	$.ajax(options).done(function (response) {
		if (response.success) {
			ajax.load({
				url: Urls.minicartGC,
				data: {lineItemId: response.result.lineItemId},
				callback: function (response) {
					minicart.show(response);
					form.find('input,textarea').val('');
				}
			});
		} else {
			form.find('span.error').hide();
			for (var id in response.errors.FormErrors) {
				var $errorEl = $('#' + id).addClass('error').removeClass('valid').next('.error');
				if (!$errorEl || $errorEl.length === 0) {
					$errorEl = $('<span for="' + id + '" generated="true" class="error" style=""></span>');
					$('#' + id).after($errorEl);
				}
				$errorEl.text(response.errors.FormErrors[id].replace(/\\'/g, '\'')).show();
			}
		}
	}).fail(function (xhr, textStatus) {
		// failed
		if (textStatus === 'parsererror') {
			window.alert(Resources.BAD_RESPONSE);
		} else {
			window.alert(Resources.SERVER_CONNECTION_ERROR);
		}
	});
};

exports.init = function () {
	$('#AddToBasketButton').on('click', setAddToCartHandler);
};

},{"./ajax":1,"./minicart":16,"./util":52}],14:[function(require,module,exports){
'use strict';

var _ = require('lodash'),
    util = require('./util');
//var dataLayer = [];
//Add To Cart Event
var addProduct = function(jsondata) {
    var productinfo = jsondata;
    var productjsonObj = JSON && JSON.parse(productinfo) || $.parseJSON(productinfo);
    var param = null;
    $.each(productjsonObj.ecommerce.add.products, function(e) {
        param = [{
            "id": this.id,
            "name": this.name,
            "price": this.price,
            "brand": this.brand,
            "category": this.category,
            'quantity': this.quantity,
            "variant": this.variant,
            'dimension6': this.dimension6,
            'group': this.group,
            'coupon': this.coupon,
            'metric3': this.metric3
        }];
    });
    dataLayer.push({
        'event': productjsonObj.event,
        'ecommerce': {
            'currencyCode': productjsonObj.ecommerce.currencyCode,
            'add': {
                'products': param
            }
        }
    });
};

//Checkout Steps
var checkoutProduct = function(jsondata) {
    var productinfo = jsondata;
    var productjsonObj = JSON && JSON.parse(productinfo) || $.parseJSON(productinfo);
    var param = [] ;
    $.each(productjsonObj.Products, function(e) {
        param.push({
            "id": this.id,
            "name": this.name,
            "price": this.price,
            "brand": this.brand,
            "category": this.category,
            'quantity': this.quantity,
            "variant": this.variant,
            'dimension6': this.dimension6
        });
    });
    dataLayer.push({
        'event': productjsonObj.event,
        'ecommerce': {
            'currencyCode': productjsonObj.currency,
            'checkout': {
                'actionField': {
                    'step': 1
                },
                'products': param
            }
        }
    });
};

//Event tracking for category sorting
var categoryPageSort = function(sortOption) {
    dataLayer.push({
        'event': 'categoryPageSort',
        'pageSort': sortOption,
    });
};

var categoryRefinement = function(){
	  var refType = "";
	  var refValue = "";
	  var refLocation = "";
	  if($(".refinements.new").length > 0){
		  refLocation = "top menu (A)";
	  }else{
		  refLocation = "side menu (B)";
	  }
	  $('.refinement').not('.category-refinement').each(function() {
			var ci = $(this);
			var ref = $(ci).find('.toggle').text();
			if(ref.length != 0){
				refType = (refType.length == 0) ? ref : refType+'/'+ref;
			}
				
	  });
	  $('.refinement ul li.selected').each(function() {
			var ci = $(this);
			var refV = $.trim($(ci).find('a').text());
			refValue= refValue +"/"+ refV;
	  });
	  
     dataLayer.push({
    	 'event': 'categoryPageRefinement',
    	 'refinementLocation' : refLocation,
    	 'refinementType': refType.replace(/(?:\r\n|\r|\n)/g, ''),
    	 'refinementValue' : refValue.replace(/(?:\r\n|\r|\n)/g, '')
     });
 };
 
var productShare = function(prodID,prodName,socialName){
	dataLayer.push({
		'event': socialName,
		'product_shared': prodName,
		'product_id_shared': prodID
	});
};

//Event tracking for adding product to wishlist
var addToWishlist = function(productID, productName) {
    dataLayer.push({
        'event': 'addToWishlist',
        'wishlistId': productID,
        'wishlistName': productName
    });
};

//Home Page main Carousel
var homeCarousel = function() {
    var count = 1;
    var slot = [];
    $('#homepage-slider ul li').each(function() {
        var ci = $(this);
        slot.push({
            'id': 'home-main',
            'creative': $(ci).find('a img').attr('src'),
            'position': 'slot' + count
        });
        count = count + 1;
    });

    dataLayer.push({
        'ecommerce': {
            'promoView': {
                'promotions': slot
            }
        }
    });
    
    $('#homepage-slider ul li').on("click", function() {
    	dataLayer.push({
	        'event': 'promotionClick',
	        'ecommerce': {
	        	'promoClick': {
        	      'promotions': [{
        	          'id': 'home-main',
        	          'name': '',
        	          'creative': $(this).find('a img').attr('src'),
        	          'position': 'slot'+$(this).index()
        	       }]
	        	}
	        }
	    });
    });
};

var homeRecommendation = function(){
	  var count=1;
	  var rec= {};
	  $('#vertical-carousel ul li').each(function() {
			rec[count] = {
				 'name': $(this).find('.product-tile .name-link').text(), 
     	         'id': $(this).find('.product-tile').attr('data-itemid'),
     	         'price': $(this).find('.product-tile .product-sales-price').text(),
     	         'brand': '',
     	         'category': '',
     	         'list': 'Homepage Recommendation Zone',
     	         'position':count
				}
				count = count+1;
	  });
	  
	  dataLayer.push({
			  'impressions': [rec]
	  });
};

//Coupon events
var applyCoupon = function(couponCode) {
    dataLayer.push({
        'event': 'promoCodeApplied',
        'promocode': couponCode,
    });
};

//Event tracking for signin and signup
var accountSignup = function() {
    dataLayer.push({
        'event': 'myAccountStart',
    });
};

//Event tracking for signin and signup
var login = function() {
    dataLayer.push({
        'event': 'myAccountSignIn',
    });
};
var signUp = function() {
    dataLayer.push({
        'event': 'myAccountComplete',
    });
};

//facebook
var facebookShare = function() {
    dataLayer.push({
        'event': 'facebookShare',
    });
};

//linkedIn
var linkedInShare = function() {
    dataLayer.push({
        'event': 'linkedInShare'
    });
};

//twitter
var twitterShare = function() {
    dataLayer.push({
        'event': 'twitterShare'
    });
};

//pinterest
var pinterestShare = function() {
    dataLayer.push({
        'event': 'pinterestFooter'
    });
};

//Footer email subscription
var footerEmailSignup = function() {
    dataLayer.push({
        'event': 'emailFooter',
    });
};

//Event tracking for AddProduct to Cart
var addToCart = function($form) {
    var pid = $form.find("input[name='pid']").val();
    var $qty = $form.find('input[name="Quantity"]');
    if ($qty.length === 0 || isNaN($qty.val()) || parseInt($qty.val(), 10) === 0) {
        $qty.val('1');
    }
    var param = {
        "pid": pid,
        'cartEvent': 'addtocart',
        'quantity': $qty.val()
    };
    $.ajax({
        type: 'GET',
        url: util.appendParamsToUrl(Urls.getGtmData, param),
        success: function(response) {
            try {
                var response = response;
                addProduct(response);
            } catch (e) {
            	//Do Nothing
            }

        }
    });

};

//Remove from cart
var removeProduct = function(pid, qty) {
    var param = {
        "pid": pid,
        'cartEvent': 'removeFromCart',
        'quantity': qty,
        'format':'ajax'
    };
    $.ajax({
        type: 'GET',
        url: util.appendParamsToUrl(Urls.getGtmData, param),
        success: function(response) {
            try {
                var response = response;
                addProduct(response);
            } catch (e) {
            	//Do Nothing
            }

        }
    });

};

//Checkout
var checkout = function(pid, qty) {
    var param = {
        'checkoutEvent': 'checkout'
    };
    $.ajax({
        type: 'GET',
        url: util.appendParamsToUrl(Urls.getGtmData, param),
        success: function(response) {
            try {
                var response = response;
                checkoutProduct(response);
            } catch (e) {
            	//Do Nothing
            }
        }
    });
};

var gtmevents = {
    init: function() {
        gtm_init();
    },
    removeProduct : removeProduct,
    addToCart : addToCart,
    footerEmailSignup : footerEmailSignup, 
    pinterestShare : pinterestShare,
    twitterShare : twitterShare,
    linkedInShare : linkedInShare,
    facebookShare : facebookShare,
    login : login,
    accountSignup : accountSignup,
    checkout : checkout,
    applyCoupon : applyCoupon,
    homeCarousel : homeCarousel,
    addToWishlist : addToWishlist,
    categoryPageSort : categoryPageSort,
    checkoutProduct : checkoutProduct,
    addProduct : addProduct,
    categoryRefinement : categoryRefinement,
    productShare : productShare,
    homeRecommendation : homeRecommendation,
    signUp :  signUp
    
};

var gtm_init = function() {
	
	var loginLink=$('.user-links a')[0];
	var registerLink=$('.user-links a')[1];
	
	//Linkedin 
    $('.footer-container .fa-linkedin-square').on('click', function(e) {
        linkedInShare();
    });
    
    //facebook
    $('.footer-container .fa-facebook-square').on('click', function(e) {
        facebookShare();
    });
    
    //twitter
    $('.footer-container .fa-twitter-square').on('click', function(e) {
        twitterShare();
    });
    
    //twitter
    $('#email-alert-signup button').on('click', function(e) {
    	footerEmailSignup();
    });
    //Home Carousel
    if ($('.pt_storefront').length > 0) {
    //   homeCarousel();
    //   homeRecommendation();
    }
	$(loginLink).on('click', function (e) {
		login();
	});
	$(registerLink).on('click', function (e) {
		accountSignup();
	});
	$('#RegistrationForm button').bind("click", function(e) {
		signUp();
    });
	$('.button-text[value="Remove"]').bind("click", function(e) {
        var productId = $(this).parents(".item-quantity-details").prevAll(".item-details").find(".sku span.value").text();
        var quantity = $(this).parents('.item-quantity-details').prev().children("input").val();
        removeProduct(productId,quantity);
    });
	$('.add-to-wishlist').bind("click", function(e) {
        var productId = $(this).parents(".item-quantity-details").prevAll(".item-details").find(".sku span.value").text();
        var name = $(this).parents(".item-quantity-details").prevAll(".item-details").find(".name a").text();
        addToWishlist(productId,name);
    });
	$('.cart-coupon-code #add-coupon').bind("click", function(e) {
        var couponCode = $('.cart-coupon-code input').val();
        applyCoupon(couponCode);
    });
	$('.button-fancy-large[value="Checkout"]').bind("click", function(e) {
        checkout();
    });

	
// Commenting for GB-618 and 627
//    $('#main').on('click', '.product-tile a:not("#quickviewbutton")', function () {
//    	dataLayer.push({
//	        'event': 'productClick',
//	        'productClicked': $(this).closest('.product-tile').attr('data-cgid'),
//	        'ecommerce': {
//	        	'click': {
//	        		 'actionField' : {
//	        			'list': $(this).closest('.product-tile').attr('data-cgid')
//	        		  },
//	        	     'products': [{
//	        	         'name': $(this).closest('.product-tile').find(' .name-link').text(), 
//	        	         'id': $(this).closest('.product-tile').attr('data-itemid'),
//	        	         'price': $(this).closest('.product-tile').find('.product-sales-price').text(),
//	        	         'brand': '',
//	        	         'category': $(this).closest('.product-tile').attr('data-cgid'),
//	        	         'position': $(this).parent().index()
//	        	      }]
//	        	}
//	        }
//	    });
//    });
};

module.exports = gtmevents;
},{"./util":52,"lodash":58}],15:[function(require,module,exports){
'use strict';
// jQuery extensions

module.exports = function () {
	// params
	// toggleClass - required
	// triggerSelector - optional. the selector for the element that triggers the event handler. defaults to the child elements of the list.
	// eventName - optional. defaults to 'click'
	$.fn.toggledList = function (options) {
		if (!options.toggleClass) { return this; }
		var list = this;
		return list.on(options.eventName || 'click', options.triggerSelector || list.children(), function (e) {
			e.preventDefault();
			var classTarget = options.triggerSelector ? $(this).parent() : $(this);
			classTarget.toggleClass(options.toggleClass);
			// execute callback if exists
			if (options.callback) {options.callback();}
		});
	};

	$.fn.syncHeight = function () {
		var arr = $.makeArray(this);
		arr.sort(function (a, b) {
			return $(a).height() - $(b).height();
		});
		return this.height($(arr[arr.length - 1]).height());
	};
};

},{}],16:[function(require,module,exports){
'use strict';

var util = require('./util'),
	bonusProductsView = require('./bonus-products-view');

var timer = {
	id: null,
	clear: function () {
		if (this.id) {
			window.clearTimeout(this.id);
			delete this.id;
		}
	},
	start: function (duration, callback) {
		this.id = setTimeout(callback, duration);
	}
};

var minicart = {
	init: function () {
		this.$el = $('#mini-cart');
		this.$content = this.$el.find('.mini-cart-content');

		$('.mini-cart-product').eq(0).find('.mini-cart-toggle').addClass('caret-down');
		$('.mini-cart-product').not(':first').addClass('collapsed')
			.find('.mini-cart-toggle').addClass('caret-right');

		$('.mini-cart-toggle').on('click', function () {
			$(this).toggleClass('caret-down caret-right');
			$(this).closest('.mini-cart-product').toggleClass('collapsed');
		});

		// events
		this.$el.find('.mini-cart-total').on('mouseenter click', function () {
			if (this.$content.not(':visible')) {
				this.slide();
			}
		}.bind(this));

		this.$content.on('mouseenter', function () {
			timer.clear();
		}).on('mouseleave', function () {
			timer.clear();
			timer.start(30, this.close.bind(this));
		}.bind(this));
		
		/*custom scroll
		$('.mini-cart-content .mini-cart-products').enscroll({
			verticalTrackClass: 'track1',
		    verticalHandleClass: 'handle1',
       });*/
	},
	/**
	 * @function
	 * @description Shows the given content in the mini cart
	 * @param {String} A HTML string with the content which will be shown
	 */
	show: function (html) {
		this.$el.html(html);
		// because of fixed header removing scrollTop in desktop 
		if($(window).width() < 1024 && $('.pt_product-search-result').length < 1) {
			util.scrollBrowser(0);
		}
		this.init();
		this.slide();
		bonusProductsView.loadBonusOption();
	},
	/**
	 * @function
	 * @description Slides down and show the contents of the mini cart
	 */
	slide: function () {
		timer.clear();
		// show the item
		this.$content.slideDown('slow');
		$('.user-info').removeClass('active');
		// after a time out automatically close it
		timer.start(6000, this.close.bind(this));
	},
	/**
	 * @function
	 * @description Closes the mini cart with given delay
	 * @param {Number} delay The delay in milliseconds
	 */
	close: function (delay) {
		timer.clear();
		this.$content.slideUp(delay);
	}
};

module.exports = minicart;

},{"./bonus-products-view":4,"./util":52}],17:[function(require,module,exports){
'use strict';

var util = require('./util');

var page = {
	title: '',
	type: '',
	params: util.getQueryStringParams(window.location.search.substr(1)),
	redirect: function (newURL) {
		setTimeout(function () {
			window.location.href = newURL;
		}, 0);
	},
	refresh: function () {
		setTimeout(function () {
			window.location.assign(window.location.href);
		}, 500);
	}
};

module.exports = page;

},{"./util":52}],18:[function(require,module,exports){
'use strict';

var giftcert = require('../giftcert'),
	tooltip = require('../tooltip'),
	util = require('../util'),
	dialog = require('../dialog'),
	page = require('../page'),
	formPrepare= require('../pages/checkout/formPrepare'),
	validator = require('../validator');

/**
 * @function
 * @description Initializes the events on the address form (apply, cancel, delete)
 * @param {Element} form The form which will be initialized
 */
function initializeAddressForm() {
	var $form = $('#edit-address-form');

	$form.find('input[name="format"]').remove();
	tooltip.init();
	//$("<input/>").attr({type:"hidden", name:"format", value:"ajax"}).appendTo(form);

	$form.on('click', '.apply-button', function (e) {
		e.preventDefault();
		if (!$form.valid()) {
			return false;
		}
		var url = util.appendParamToURL($form.attr('action'), 'format', 'ajax');
		var applyName = $form.find('.apply-button').attr('name');
		var options = {
			url: url,
			data: $form.serialize() + '&' + applyName + '=x',
			type: 'POST'
		};
		$.ajax(options).done(function (data) {
			if (typeof(data) !== 'string') {
				if (data.success) {
					dialog.close();
					page.refresh();
				} else {
					window.alert(data.message);
					return false;
				}
			} else {
				$('#dialog-container').html(data);
				account.init();
				tooltip.init();
				util.selectbox();
			}
		});
	})
	.on('click', '.cancel-button, .close-button', function (e) {
		e.preventDefault();
		dialog.close();
	})
	.on('click', '.delete-button', function (e) {
		e.preventDefault();
		if (window.confirm(String.format(Resources.CONFIRM_DELETE, Resources.TITLE_ADDRESS))) {
			var url = util.appendParamsToUrl(Urls.deleteAddress, {
				AddressID: $form.find('#addressid').val(),
				format: 'ajax'
			});
			$.ajax({
				url: url,
				method: 'POST',
				dataType: 'json'
			}).done(function (data) {
				if (data.status.toLowerCase() === 'ok') {
					dialog.close();
					page.refresh();
				} else if (data.message.length > 0) {
					window.alert(data.message);
					return false;
				} else {
					dialog.close();
					page.refresh();
				}
			});
		}
	});

	validator.init();
	if($form.find('select[id$="_country"]').length > 0 && $form.find('select[id$="_country"]').val() == 'GB' && $form.find('[name$="_state"][id$="_state"]').length > 0){
		$form.find('[name$="_state"][id$="_state"]').closest(".form-row").hide();
	}
	$('select[id$="_country"]', $form).on('change', function () {
        updateStateOptions($form);
    });

}
function getSite(value){
   switch(value){
      case "GB":
             return "";
      case "FR":
             return "INPUT";
      case "AU":
    	  	return "SELECT";
      case "NZ":
	  		return "INPUT";
      case "CA":
             return "SELECT";
      case "DE":
      return "INPUT";
      case "NL":
             return "INPUT";
      case "US":
             return "SELECT";
      default:
             return "INPUT";
   }
}
 function updateStateOptions (form) {
	 var $form = $(form);
	 if($form.find('[name$="_state"][id$="_state"]').length > 0 && !$form.find('[name$="_state"][id$="_state"]').is(":visible")){
	  $form.find('[name$="_state"][id$="_state"]').closest(".form-row").show();
	 }
	 $form.find('label[for$="_state"]').removeClass('error');
	 $form.find('input[name$="_state"]').removeClass('error');
	 $form.find('span[id$="_state-error"]').remove();
	  var $country = $form.find('select[id$="_country"]'),
	    site = $country.val(),
	       country = Countries[$country.val()];
	 var sitevalue = getSite(site);
	 var isstate = $form.find(".form-row .field-wrapper .state");
	 if(isstate.length > 0)
	 {
	     var id = isstate.attr("id");
	     var name = isstate.attr("name");
	 }else{
		 var id = "dwfrm_profile_address_states_state";
	     var name = "dwfrm_profile_address_states_state";
	 }
	  if ($country.length === 0 || !country) {
		  if($form.find('[name$="_state"][id$="_state"]').length > 0){
			  $form.find('[name$="_state"][id$="_state"]').closest("div.form-row").find("label span").first().html("State / Province / Region");
			  var fieldHTML = $('<input/>').attr({ type: 'text', class :'input-text state ', id:id, name:name });
		      if($form.find('select[name$="_state"]').length){
		            $form.find('select[name$="_state"]').replaceWith( fieldHTML);
		      }else{
		           $form.find('input[name$="_state"]').replaceWith( fieldHTML);
		      }
		  }
	      return;
	  }
	  if(sitevalue == "SELECT") 
	  { 
		  var  fieldHTML = $('<select/>').attr({ class :'input-select state valid', id:id, name:name});
	      if($form.find('select[name$="_state"]').length){
	            $form.find('select[name$="_state"]').replaceWith( fieldHTML);
	      } else{
	            $form.find('input[name$="_state"]').replaceWith( fieldHTML);
	      }
	  }else if(sitevalue == "INPUT"){  
	      var fieldHTML = $('<input/>').attr({ type: 'text', class :'input-text state ', id:id, name:name });
	      if($form.find('select[name$="_state"]').length){
	            $form.find('select[name$="_state"]').replaceWith( fieldHTML);
	      }else{
	           $form.find('input[name$="_state"]').replaceWith( fieldHTML);
	      }
	  }else{
		  if($form.find('[name$="_state"][id$="_state"]').length > 0){
			  $form.find('[name$="_state"][id$="_state"]').closest(".form-row").hide();
		  }
	  }
	  if(sitevalue == "SELECT") 
	  {
		  var arrHtml = [],
	     $stateField = $country.data('stateField') ? $country.data('stateField') : $form.find('select[name$="_state"]'),
	     $stateLabel = ($stateField.length > 0) ? $form.find('label[for="' + $stateField[0].id + '"] span').not('.required-indicator') : undefined;
	 }else if(sitevalue == "INPUT"){
	     var arrHtml = [],
	    $stateField = $country.data('stateField') ? $country.data('stateField') : $form.find('input[name$="_state"]'),
	    $stateLabel = ($stateField.length > 0) ? $stateField.closest("div.form-row").find("label span").first() : undefined;
	 }
	 if ($stateLabel) {
		 $stateLabel.html(country.regionLabel);
	 } else {
	     return;
	 }
	 if(sitevalue == "SELECT")      
	 {
	      var s;
	      for (s in country.regions) {
	            arrHtml.push('<option value="' + s + '">' + country.regions[s] + '</option>');
	       }
	     $form.find('select[name$="_state"]').html(arrHtml.join(""));     
	  } 
	 util.selectbox();
}

/**
 * @private
 * @function
 * @description Toggles the list of Orders
 */
function toggleFullOrder () {
	$('.order-items')
		.find('li.hidden:first')
		.prev('li')
		.append('<a class="toggle">View All</a>')
		.children('.toggle')
		.click(function () {
			$(this).parent().siblings('li.hidden').show();
			$(this).remove();
		});
}
/**
 * @private
 * @function
 * @description Binds the events on the address form (edit, create, delete)
 */
function initAddressEvents() {
	var addresses = $('#addresses');
	if (addresses.length === 0) { return; }

	addresses.on('click', '.address-edit, .address-create', function (e) {
		e.preventDefault();
		dialog.open({
			url: this.href,
			options: {
				open: initializeAddressForm,
				dialogClass : 'account-dialog'
			},
			callback: function () {
				var $form = $('#edit-address-form');
				 var currentSite = $("#wrapper .country-select").find("select[id$='_destination'] option[selected=selected]").val();
				 var currentCountryCode = $form.find('select[id$="_country"]').val();
				 if(currentSite != currentCountryCode){
					 updateStateOptions($form);
					 if($form.find("input[id='state']").length > 0 && $form.find("input[id='state']").val() != '' && $form.find("input[id='state']").val() != 'null'){
						 $form.find('[name$="_state"][id$="_state"]').val($form.find("input[id='state']").val());
						 $form.find('[name$="_state"][id$="_state"]').trigger('change');
					 }
				 }
				 $('.account-dialog select.input-select').trigger('change');
				 util.selectbox();
			}
		});
	}).on('click', '.delete', function (e) {
		e.preventDefault();
		if (window.confirm(String.format(Resources.CONFIRM_DELETE, Resources.TITLE_ADDRESS))) {
			$.ajax({
				url: util.appendParamToURL($(this).attr('href'), 'format', 'ajax'),
				dataType: 'json'
			}).done(function (data) {
				if (data.status.toLowerCase() === 'ok') {
					page.redirect(Urls.addressesList);
				} else if (data.message.length > 0) {
					window.alert(data.message);
				} else {
					page.refresh();
				}
			});
		}
	});
}
/**
 * @private
 * @function
 * @description Binds the events of the payment methods list (delete card)
 */
$(window).on("load", function() {
    if ($(".orderstatusposition").length > 0) {
        $('html, body').scrollTop($('.login-order-track legend').position().top + 120);
    }
});



function initPaymentEvents() {
	$('.add-card').on('click', function (e) {
		e.preventDefault();
		dialog.open({
			//PREVAIL-Added  to handle validation issues
			url: $(e.target).attr('href'),
			options: {
				open: function () {
					validator.init();
				}
			}
		});
	});

	var paymentList = $('.payment-list');
	if (paymentList.length === 0) { return; }

	util.setDeleteConfirmation(paymentList, String.format(Resources.CONFIRM_DELETE, Resources.TITLE_CREDITCARD));

	$('form[name="payment-remove"]').on('submit', function (e) {
		e.preventDefault();
		// override form submission in order to prevent refresh issues
		var button = $(this).find('.delete');
		$('<input/>').attr({
			type: 'hidden',
			name: button.attr('name'),
			value: button.attr('value') || 'delete card'
		}).appendTo($(this));
		var data = $(this).serialize();
		$.ajax({
			type: 'POST',
			url: $(this).attr('action'),
			data: data
		})
		.done(function () {
			page.redirect(Urls.paymentsList);
		});
	});
}

function initLoginPage() {
	//o-auth binding for which icon is clicked
	$('.oAuthIcon').bind('click', function () {
		$('#OAuthProvider').val(this.id);
	});

	//toggle the value of the rememberme checkbox
	$('#dwfrm_login_rememberme').bind('change', function () {
		if ($('#dwfrm_login_rememberme').attr('checked')) {
			$('#rememberme').val('true');
		} else {
			$('#rememberme').val('false');
		}
	});
	
	$("[id$='_customer_businesstype']").change(function() {
        if ($(this).val() == "other") {
            $(".tradepleasespecify").show();
            $(".tradepleasespecify .field-wrapper .input-text").addClass("required");
            $(".tradepleasespecify").find('label').append('<span class="required">*</span>');

        }else{
            $(".tradepleasespecify").hide();
            $(".tradepleasespecify .field-wrapper .input-text").removeClass("required");
        } 

    });
	$('#password-reset').on('click', function (e) {
		e.preventDefault();
		dialog.open({
			url: $(e.target).attr('href'),
			options: {
				//Start JIRA PREV-334 : Title is missing for the Forgot password overlay.
				dialogClass : 'password-reset-popup custom-popup',
				width : 390,
				open: function () {
					validator.init();
					var $requestPasswordForm = $('[name$="_requestpassword"]'),
						$submit = $requestPasswordForm.find('[name$="_requestpassword_send"]');
					$($submit).on('click', function (e) {
						if (!$requestPasswordForm.valid()) {
							return;
						}
						e.preventDefault();
						dialog.submit($submit.attr('name'));
					});
				}
			}
		});
	});

}

//Logout events open a pop up for logout confirmation

/*function initLogoutEvents() {
	$(".account-logoutconfirmation").on('click', function (e) {
		e.preventDefault();
		var url = Urls.logoutconfirmation;
		dialog.open({
			url : url,
			options : {
				dialogClass : 'account-logout-confirmation custom-popup',
				width : 390
			},
			callback : function(){
				//close the dialog and remain in the same page
				$(".account-logout-no").on('click', function (e) {
					e.preventDefault();
					dialog.close();
				});
			}
		});
	});
}*/


function contactUsEvents() {
    if ($('#Contactus').length>0) {
    	formPrepare.init({
                    continueSelector: '[name$="contactus_send"]',
                    formSelector:'[id$="Contactus"]'
            });
    }

}
$("div.contactuscontainerslot").each(function() {
    if($(this).children().length == 0){
        $(this).hide();
        }
});


//For cancel button in contactus page
$('#cancelBtn').on('click', function(e) {
	  e.preventDefault();
	  var prevurl= $("[name='prevurl']").val();
	  window.location.href = prevurl;

	})
$('.hpregister').on('click',function(e){
		e.preventDefault();
		var url = $(location).attr("href");
		var index = url.indexOf('callback_url=');
		var redirecturl = Urls.hpregister;
		if(index > 0){
			var callbackparam = url.substring(index+13);
			redirecturl = decodeURIComponent(util.appendParamToURL(redirecturl, 'callback_url',callbackparam ));
		}
		window.location.href = redirecturl;
	})

$('#digital_contact').on('click', function(e) {
    e.preventDefault();
    var txtemail =$("#EmailInput").val();
    var emailExp = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(!txtemail.match(emailExp)){
        $('#digital-erroremail').addClass('error').text(Resources.DIGITAL_ERROR);
        return false;
     }      
    var form = $(this).closest('form');
    var url = Urls.digitalContactus;
    var firstname = $("#FirstName_textbox").val();
    var lastname = $("#LastName_textbox").val();
    var email = $("#EmailInput").val();
    var phone = $("#PhoneID_textbox").val();
    var enquiry = $("#Enquiry_textbox").val();
    url = util.appendParamToURL(url, 'firstname', firstname);
    url = util.appendParamToURL(url, 'lastname', lastname);
    url = util.appendParamToURL(url, 'email', email);
    url = util.appendParamToURL(url, 'phone', phone);
    url = util.appendParamToURL(url, 'enquiry', enquiry);
    url = util.appendParamToURL(url, 'format', 'ajax');
     
    $.ajax({
	    type: 'GET',
	    url:url,
    	success: function (response) {
    		$("#primary").html(Resources.THANKYOU_DIGITAL);
        },
        failure: function () {
        	location.reload();
        }
   });
})

/**
 * @private
 * @function
 * @description Binds the events of the order, address and payment pages
 */
function initializeEvents() {
	initializeAddressForm();// JIRA PREV-63: 'Add Address' or 'Edit address' overlay changing to page after click on Apply button,when there is an error message on overlay.
	contactUsEvents();
	toggleFullOrder();
	initAddressEvents();
	initPaymentEvents();
	initLoginPage();
}

var account = {
	init: function () {
		initializeEvents();
		giftcert.init();
		//initLogoutEvents();
	},
	initCartLogin: function () {
		initLoginPage();
	}
};

module.exports = account;

},{"../dialog":10,"../giftcert":13,"../page":17,"../pages/checkout/formPrepare":22,"../tooltip":51,"../util":52,"../validator":53}],19:[function(require,module,exports){
'use strict';

var account = require('./account'),
	bonusProductsView = require('../bonus-products-view'),
	quickview = require('../quickview'),
	util = require('../util'),
	gtmevents= require('../gtmevents'),
	cartStoreInventory = require('../storeinventory/cart');

/**
 * @private
 * @function
 * @description Binds events to the cart page (edit item's details, bonus item's actions, coupon code entry)
 */
function initializeEvents() {
	$('#cart-table').on('click', '.item-edit-details a', function (e) {
		e.preventDefault();
		quickview.show({
			url: e.target.href,
			source: 'cart'
		});
		
	})
	.on('click', '.bonus-item-actions a, .item-details .bonusproducts a', function (e) {
		e.preventDefault();
		bonusProductsView.show(this.href);
	});

	// override enter key for coupon code entry
	$('form input[name$="_couponCode"]').on('keydown', function (e) {
		if (e.which === 13 && $(this).val().length === 0) { return false; }
		else if(e.which === 13){e.preventDefault();$("#add-coupon").click();} // JIRA PREV-30 : Cart page:  Coupon Code is not applying, when the user hit enter key.
	});
	
	//hide and show for coupon form
	if($("#cart-items-form .cart-footer .cart-coupon-code .cart-coupon-section1 a").length > 0){
		$("#cart-items-form .cart-footer .cart-coupon-code .cart-coupon-section1 a").on('click', function(){
			$(this).toggleClass('active');
			$("#cart-items-form .cart-footer .cart-coupon-code .cart-coupon-section2").slideToggle(150);
		});
	}
	if($('.cart-coupon-section2 div').hasClass('error')) {
		$("#cart-items-form .cart-footer .cart-coupon-code .cart-coupon-section1 a").addClass('active');
		$("#cart-items-form .cart-footer .cart-coupon-code .cart-coupon-section2").show();
	}
	
	//show tool tip on click of save for later for unauthenticated user
	if($("#cart-items-form #cart-table .cart-row .item-quantity-details .unauthenticated.add-to-wishlist").length > 0){
		$("#cart-items-form #cart-table .cart-row .item-quantity-details .unauthenticated.add-to-wishlist").on('click', function(e){
			$(this).parent().find(".errormsg").show();
		});
	}
	$('#primary').on('click', '.last-visited .grid-tile #add-to-cart,.product-listing .tiles-container #add-to-cart', function (e) {
		e.preventDefault();
		var pid = $(this).attr("productid");
		if(pid != '' && pid != null){
			addToCart(pid);
			gtmevents.addToCart($(this).closest('.grid-tile'));
		}
		$('html,body').animate({scrollTop: 0}, 500);
	});
	
	//For HP Integration: Navigating to respective PDP On click of customise button at recently viewed in cart page 
	$('#primary').on('click', '.cart-recommendations .product-listing .rec-customise', function (e) {
		e.preventDefault();
		var productid = $(this).closest('.tile_add_cart').find("input[name=pid]").val();
		var url = Urls.getProductUrl;
		url = util.appendParamToURL(url, 'pid', productid);
		window.location.href = url;
	});
	
	//updating the qty value of product 
	$(document).on("click", "#primary #cart-items-form .item-quantity a", function(e) {
	    e.preventDefault();
	    var curObj = $(this);
	    var inputfield = $(curObj).closest(".item-quantity-details").find("input.Quantity");
	    var inputValue = $(inputfield).val();
	    if ($(curObj).hasClass("prev-value")) {
	        if ($(inputfield).val() > 1) {
	            inputValue--;
	            $(inputfield).val(inputValue);
	            $('#update-cart').trigger('click');
	        }
	    } else if ($(curObj).hasClass("next-value")) {
	        if ($(inputfield).val() >= 0 && $(inputfield).val() < 99) {
	            inputValue++;
	            $(inputfield).val(inputValue);
	            $('#update-cart').trigger('click');
	        }
	    }
	    
	    //this code is commented for dynamically update bag page GB-564
	    
	    /*if($('#primary #cart-items-form .cart-footer .cart-order-totals').find('.updateQuantityError').length >0){
	    	$('#primary #cart-items-form .cart-footer .cart-order-totals').find('.updateQuantityError').remove();
	    }
	    var disable=false;
	    var error=false;
	    var basketQty=0;
	    $(this).closest("#primary #cart-items-form").find(".item-quantity-details .item-quantity").each(function(){
	    	if(parseInt($(this).find("input[name='hiddenQuantity']").val()) != parseInt($(this).find(".Quantity").val())){
	    		error=true;
	    	}
	    	if(parseInt($(this).find(".Quantity").val()) > 50){
	    		disable=true;
	    	}
	    	basketQty+=parseInt($(this).find(".Quantity").val());
	    });
	    if(error){
	    	$("#primary .continue-checkout").attr("disabled", true);
		    $('#primary #cart-items-form .cart-footer .cart-order-totals').find('button').after("<div class='updateQuantityError'>"+Resources.BASKETUPDATEQUANTITY +"</div>");
	    }else{
	    	if(!disable && basketQty < 100){
	    		$("#primary .continue-checkout").attr("disabled", false);
	    	}
	    }*/
	});
	
	//this code is commented for dynamically update bag page GB-564 
	
	/*$(document).find("#primary #cart-items-form .item-quantity").keyup(function(e) {
		e.preventDefault();
		if($('#primary #cart-items-form .cart-footer .cart-order-totals').find('.updateQuantityError').length >0){
			$('#primary #cart-items-form .cart-footer .cart-order-totals').find('.updateQuantityError').remove();
	    }
		var disable=false;
		var error=false;
		var basketQty=0;
		$(this).closest("#primary #cart-items-form").find(".item-quantity-details .item-quantity").each(function(){
	    	if(parseInt($(this).find("input[name='hiddenQuantity']").val()) != parseInt($(this).find(".Quantity").val())){
	    		error=true;
	    	}
	    	if(parseInt($(this).find(".Quantity").val()) > 50){
	    		disable=true;
	    	}
	    	basketQty+=parseInt($(this).find(".Quantity").val());
	    });
		if(error){
			$("#primary .continue-checkout").attr("disabled", true);
		    $('#primary #cart-items-form .cart-footer .cart-order-totals').find('button').after("<div class='updateQuantityError'>"+Resources.BASKETUPDATEQUANTITY +"</div>");
	    }else{
	    	if(!disable && basketQty < 100){
	    		$("#primary .continue-checkout").attr("disabled", false);
	    	}
	    }
	});*/
	//this is for dynamically update bag page GB-564 
	$(document).on('change','.Quantity',function(){
    	$('#update-cart').trigger('click');
    });
	$('#cart-items-form .item-quantity .Quantity').keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
             // Allow: Ctrl+C
            (e.keyCode == 67 && e.ctrlKey === true) ||
             // Allow: Ctrl+X
            (e.keyCode == 88 && e.ctrlKey === true) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

}

exports.init = function () {
	initializeEvents();
	if (SitePreferences.STORE_PICKUP) {
		cartStoreInventory.init();
	}
	account.initCartLogin();
};

//add to cart for featured products and recently viewed products
function addToCart(pid){
	var url = Urls.addProduct;
	var pid = pid;
	url = util.appendParamToURL(url, 'pid', pid);
	url = util.appendParamToURL(url, 'format', 'ajax');
	$.ajax({
		type: 'GET',
		dataType: 'html',
		url: url,
		success: function (response) {
			window.location = Urls.cartShow;
		},
		failure: function () {
			window.alert(Resources.SERVER_ERROR);
		}
	});
}

},{"../bonus-products-view":4,"../gtmevents":14,"../quickview":41,"../storeinventory/cart":48,"../util":52,"./account":18}],20:[function(require,module,exports){
'use strict';

var util = require('../../util');
var shipping = require('./shipping');
var formPrepare = require('../../pages/checkout/formPrepare');

/**
 * @function
 * @description Selects the first address from the list of addresses
 */
exports.init = function () {
	/*var $form = $('.address');
	// select address from list
	$('select[name$="_addressList"]', $form).on('change', function () {
		var selected = $(this).children(':selected').first();
		var selectedAddress = $(selected).data('address');
		if (!selectedAddress) { return; }
		util.fillAddressFields(selectedAddress, $form);
		shipping.updateShippingMethodList();
		// re-validate the form
		 JIRA PREV-95 : Highlighting the Credit card  details field, when the user select any saved address from the 'Select An Address' drop down.
		   (logged-in user) .Commented to prevent form from re-validation.
		
		//$form.validate().form();
	});*/
	var $shippingform = $('.address .shippingForm');
	// select address from list
	$('select[name$="_addressList"]', $shippingform).on('change', function () {
		var selected = $(this).children(':selected').first();
		var selectedAddress = $(selected).data('address');
		if (!selectedAddress) { return; }
		util.fillAddressFields(selectedAddress, $shippingform);
		shipping.updateShippingMethodList();
		// re-validate the form
		/* JIRA PREV-95 : Highlighting the Credit card  details field, when the user select any saved address from the 'Select An Address' drop down.
		   (logged-in user) .Commented to prevent form from re-validation.
		*/
		//$form.validate().form();
		formPrepare.init({
			continueSelector: '[name$="shippingAddress_save"]',
			formSelector:'[id$="singleshipping_shippingAddress"]'
		});
	});
	var $billingingform = $('.address .bilingForm');
	// select address from list
	$('select[name$="_addressList"]', $billingingform).on('change', function () {
		var selected = $(this).children(':selected').first();
		var selectedAddress = $(selected).data('address');
		if (!selectedAddress) { return; }
		util.fillAddressFields(selectedAddress, $billingingform);
		shipping.updateShippingMethodList();
		// re-validate the form
		/* JIRA PREV-95 : Highlighting the Credit card  details field, when the user select any saved address from the 'Select An Address' drop down.
		   (logged-in user) .Commented to prevent form from re-validation.
		*/
		//$form.validate().form();
		formPrepare.init({
			continueSelector: '[name$="shippingAddress_save"]',
			formSelector:'[id$="singleshipping_shippingAddress"]'
		});
	});
	
	//This change is for click ad collect 
	var $clickandcollectform = $(".click-and-collect-form .biling");
	// select address from list
	$('select[name$="_addressList"]', $clickandcollectform).on('change', function () {
		var selected = $(this).children(':selected').first();
		var selectedAddress = $(selected).data('address');
		if (!selectedAddress) { return; }
		util.fillAddressFields(selectedAddress, $clickandcollectform);
		shipping.updateShippingMethodList();
		formPrepare.init({
			continueSelector: '[name$="shippingAddress_save"]',
			formSelector:'[id$="singleshipping_shippingAddress"]'
		});
	});
	
};

},{"../../pages/checkout/formPrepare":22,"../../util":52,"./shipping":25}],21:[function(require,module,exports){
'use strict';

var ajax = require('../../ajax'),
	formPrepare = require('./formPrepare'),
	giftcard = require('../../giftcard'),
	util = require('../../util');

/**
 * @function
 * @description Fills the Credit Card form with the passed data-parameter and clears the former cvn input
 * @param {Object} data The Credit Card data (holder, type, masked number, expiration month/year)
 */
function setCCFields(data) {
	var $creditCard = $('[data-method="CREDIT_CARD"]');
	$creditCard.find('input[name$="creditCard_owner"]').val(data.holder).trigger('change');
	$creditCard.find('select[name$="_type"]').val(data.type).trigger('change');
	$creditCard.find('input[name*="_creditCard_number"]').val(data.maskedNumber).trigger('change');
	$creditCard.find('[name$="_month"]').val(data.expirationMonth).trigger('change');
	$creditCard.find('[name$="_year"]').val(data.expirationYear).trigger('change');
	$creditCard.find('input[name$="_cvn"]').val('').trigger('change');
}

/**
 * @function
 * @description Updates the credit card form with the attributes of a given card
 * @param {String} cardID the credit card ID of a given card
 */
function populateCreditCardForm(cardID) {
	// load card details
	var url = util.appendParamToURL(Urls.billingSelectCC, 'creditCardUUID', cardID);
	ajax.getJson({
		url: url,
		callback: function (data) {
			if (!data) {
				window.alert(Resources.CC_LOAD_ERROR);
				return false;
			}
			setCCFields(data);
		}
	});
}

/**
 * @function
 * @description Changes the payment method form depending on the passed paymentMethodID
 * @param {String} paymentMethodID the ID of the payment method, to which the payment method form should be changed to
 */
function updatePaymentMethod(paymentMethodID) {
	var $paymentMethods = $('.payment-method');
	$paymentMethods.removeClass('payment-method-expanded');

	var $selectedPaymentMethod = $paymentMethods.filter('[data-method="' + paymentMethodID + '"]');
	if ($selectedPaymentMethod.length === 0) {
		$selectedPaymentMethod = $('[data-method="Custom"]');
	}
	$selectedPaymentMethod.addClass('payment-method-expanded');

	// ensure checkbox of payment method is checked
	$('input[name$="_selectedPaymentMethodID"]').removeAttr('checked');
	$('input[value=' + paymentMethodID + ']').prop('checked', 'checked');

	formPrepare.validateForm();
}

/**
 * @function
 * @description loads billing address, Gift Certificates, Coupon and Payment methods
 */
exports.init = function () {
	var $checkoutForm = $('.checkout-billing');
	var $addGiftCert = $('#add-giftcert');
	var $giftCertCode = $('input[name$="_giftCertCode"]');
	var $addCoupon = $('#add-coupon');
	var $couponCode = $('input[name$="_couponCode"]');
	var $selectPaymentMethod = $('.payment-method-options');
	var selectedPaymentMethod = $selectPaymentMethod.find(':checked').val();
	
	
	//this is for Checkout - Highlighting missing fields GB-570
	/*formPrepare.init({
		formSelector: 'form[id$="billing"]',
		continueSelector: '[name$="billing_save"]'
	});*/

	/* JIRA PREV-135 : Disabled Credit Card in BM is displaying in the application. Changed default option from 'CREDIT_CARD' to first available payment method.*/
	updatePaymentMethod((selectedPaymentMethod) ? selectedPaymentMethod : $('.payment-method-options input').first().val());
	$selectPaymentMethod.on('click', 'input[type="radio"]', function () {
		updatePaymentMethod($(this).val());
	});

	// select credit card from list
	$('#creditCardList').on('change', function () {
		var cardUUID = $(this).val();
		if (!cardUUID) {return;}
		populateCreditCardForm(cardUUID);

		// remove server side error
		$('.required.error').removeClass('error');
		$('.error-message').remove();
	});

	$('#check-giftcert').on('click', function (e) {
		e.preventDefault();
		$checkoutForm.find('.giftcert-error').html('');//PREVAIL-Added to handle client side error messages and server side error messages together.
		var $balance = $('.balance');//PREVAIL-Added to handle client side error messages and server side error messages together.
		if ($giftCertCode.length === 0 || $giftCertCode.val().length === 0) {
			$balance.html('');
			var error = $balance.find('span.error');
			if (error.length === 0) {
				error = $('<span>').addClass('error').appendTo($balance);
			}
			error.html(Resources.GIFT_CERT_MISSING);
			return;
		}
		
		$balance.find('span.error').html(''); //PREVAIL-Added to handle client side error messages and server side error messages together.
		//PREVAIL-Added $("input[name$='_giftCertPin']").val() to handle custom GC.
		giftcard.checkBalance($giftCertCode.val(), $("input[name$='_giftCertPin']").val(), function (data) {
			if (!data || !data.giftCertificate) {
				$balance.html(Resources.GIFT_CERT_INVALID).removeClass('success').addClass('error');
				return;
			}
			$balance.html(Resources.GIFT_CERT_BALANCE + ' ' + data.giftCertificate.balance).removeClass('error').addClass('success');
		});
	});

	$addGiftCert.on('click', function (e) {
		e.preventDefault();
		$('.balance').html("").find('span.error').html("");
		var code = $giftCertCode.val(),
			pin  = $("input[name$='_giftCertPin']")?$("input[name$='_giftCertPin']").val():null, //PREVAIL-Added to handle custom GC
			$error = $checkoutForm.find('.giftcert-error');
		if (code.length === 0) {
			$error.html(Resources.GIFT_CERT_MISSING);
			return;
		}

		var url = util.appendParamsToUrl(Urls.redeemGiftCert, {giftCertCode: code, gcPin:pin, format: 'ajax'}); //PREVAIL-Added gcPin:pin to handle custom GC
		$.getJSON(url, function (data) {
			var fail = false;
			var msg = '';
			if (!data) {
				msg = Resources.BAD_RESPONSE;
				fail = true;
			} else if (!data.success) {
				msg = data.message.split('<').join('&lt;').split('>').join('&gt;');
				fail = true;
			}
			if (fail) {
				$error.html(msg);
				return;
			} else {
				if($(".spc-billing").length === 0){ //PREVAIL-Added to handle custom GC.
					window.location.assign(Urls.billing);
				}else{
					$(".spc-billing .checkout-tab-head.open[data-refreshurl]").trigger('click');
				}
			}
		});
	});

	$addCoupon.on('click', function (e) {
		e.preventDefault();
		var $error = $checkoutForm.find('.coupon-error'),
			code = $couponCode.val();
		if (code.length === 0) {
			$error.html(Resources.COUPON_CODE_MISSING);
			return;
		}

		var url = util.appendParamsToUrl(Urls.addCoupon, {couponCode: code, format: 'ajax'});
		$.getJSON(url, function (data) {
			var fail = false;
			var msg = '';
			if (!data) {
				msg = Resources.BAD_RESPONSE;
				fail = true;
			} else if (!data.success) {
				msg = data.message.split('<').join('&lt;').split('>').join('&gt;');
				fail = true;
			}
			/*JIRA PREV-96 : Billing page: Not updating the order summary and not displaying the success message, when the user applied any coupon. 
			  Added below block to display the coupon code apply message.*/
			else if(data.success){
				msg = data.message;
				$error.html(msg);
			}
			
			if (fail) {
				$error.html(msg);
				return;
			}

			//basket check for displaying the payment section, if the adjusted total of the basket is 0 after applying the coupon
			//this will force a page refresh to display the coupon message based on a parameter message
			if (data.success) {
				//PREVAIL-Added '$(".spc-billing").length === 0' to handle SPC.
				if($(".spc-billing").length === 0){
					window.location.assign(Urls.billing);
				}else{
					$(".spc-billing .checkout-tab-head.open[data-refreshurl]").trigger('click');
				}
			}
		});
	});

	// trigger events on enter
	$couponCode.on('keydown', function (e) {
		if (e.which === 13) {
			e.preventDefault();
			$addCoupon.click();
		}
	});
	$giftCertCode.on('keydown', function (e) {
		if (e.which === 13) {
			e.preventDefault();
			$addGiftCert.click();
		}
	});
	
	//PREVAIL - init Address Validation
	/*if(isAVSEnabled){
		require('../../addressvalidation').init();
	}*/
	
	//JIRA PREV-38 : Billing page_Credit Card Section: CVV number should not pre-populate.
	if($(".bypassDAV").length==0){
		$('.spc-billing .form-row.cvn input').val("");
	}
};

},{"../../ajax":1,"../../giftcard":12,"../../util":52,"./formPrepare":22}],22:[function(require,module,exports){
'use strict';

var _ = require('lodash');

var $form, $continue, $requiredInputs, validator;

var hasEmptyRequired = function () {
	// filter out only the visible fields
	var requiredValues = $requiredInputs.filter(':visible').map(function () {
		return $(this).val();
	});
	return _(requiredValues).contains('');
};

var validateForm = function () {
	// only validate form when all required fields are filled to avoid
	// throwing errors on empty form
	if (!validator) {
		return;
	}
	if (!hasEmptyRequired()) {
		if (validator.form()) {
			$continue.removeAttr('disabled');
		}
	} else {
		$continue.attr('disabled', 'disabled');
	}
};

var validateEl = function () {
	if ($(this).val() === '') {
		$continue.attr('disabled', 'disabled');
	} else {
		// enable continue button on last required field that is valid
		// only validate single field
		if (validator.element(this) && !hasEmptyRequired()) {
			$continue.removeAttr('disabled');
		} else {
			$continue.attr('disabled', 'disabled');
		}
	}
};

var init = function (opts) {
	if (!opts.formSelector || !opts.continueSelector) {
		throw new Error('Missing form and continue action selectors.');
	}
	$form = $(opts.formSelector);
	$continue = $(opts.continueSelector);
	validator = $form.validate();
	$requiredInputs = $('.required', $form).find(':input');
	validateForm();
	// start listening
	$requiredInputs.on('change', validateEl);
	$requiredInputs.filter('input').on('keyup', _.debounce(validateEl, 200));
	$requiredInputs.filter('textarea').on('keyup', _.debounce(validateEl, 200));
};

exports.init = init;
exports.validateForm = validateForm;
exports.validateEl = validateEl;

},{"lodash":58}],23:[function(require,module,exports){
'use strict';

var address = require('./address'),
	billing = require('./billing'),
	multiship = require('./multiship'),
	shipping = require('./shipping');

/**
 * @function Initializes the page events depending on the checkout stage (shipping/billing)
 */
exports.init = function () {
	address.init();
	if ($('.checkout-shipping').length > 0) {
		shipping.init();
	} else if ($('.checkout-multi-shipping').length > 0) {
		multiship.init();
	} else if ($('.checkout-billing').length > 0){ //PREVAIL-Added $('.checkout-billing').length > 0 to handle SPC 
		billing.init();
	}

	//if on the order review page and there are products that are not available diable the submit order button
	if ($('.order-summary-footer').length > 0) {
		if ($('.notavailable').length > 0) {
			$('.order-summary-footer .submit-order .button-fancy-large').attr('disabled', 'disabled');
		}
	}
};

},{"./address":20,"./billing":21,"./multiship":24,"./shipping":25}],24:[function(require,module,exports){
'use strict';

var address = require('./address'),
	formPrepare = require('./formPrepare'),
	dialog = require('../../dialog'),
	util = require('../../util'),
	shipping = require('./shipping'),//JIRA PREV-99 : shipping methods is not displayed for 2nd address in right nav.
	ajax = require('../../ajax');//JIRA PREV-99 : shipping methods is not displayed for 2nd address in right nav.

/**
 * @function
 * @description Initializes gift message box for multiship shipping, the message box starts off as hidden and this will display it if the radio button is checked to yes, also added event handler to listen for when a radio button is pressed to display the message box
 */
function initMultiGiftMessageBox() {
	$.each($('.item-list'), function () {
		var $this = $(this);
		var $giftMessage = $this.find('.gift-message-text');

		//handle initial load
		$giftMessage.toggleClass('hidden', $('input[name$="_isGift"]:checked', this).val() !== 'true');

		//set event listeners
		$this.on('change', function () {
			$giftMessage.toggleClass('hidden', $('input[name$="_isGift"]:checked', this).val() !== 'true');
		});
	});
}


/**
 * @function
 * @description capture add edit adddress form events
 */
function addEditAddress(target) {
	var $addressForm = $('form[name$="multishipping_editAddress"]'),
		$addressDropdown = $addressForm.find('select[name$=_addressList]'),
		$addressList = $addressForm.find('.address-list'),
		add = true,
		selectedAddressUUID = $(target).parent().siblings('.select-address').val();

	$addressDropdown.on('change', function (e) {
		e.preventDefault();
		var selectedAddress = $addressList.find('select').val();
		if (selectedAddress !== 'newAddress') {
			selectedAddress = $.grep($addressList.data('addresses'), function (add) {
				return add.UUID === selectedAddress;
			})[0];
			add = false;
			// proceed to fill the form with the selected address
			util.fillAddressFields(selectedAddress, $addressForm);
		} else {
			//reset the form if the value of the option is not a UUID
			$addressForm.find('.input-text, .input-select').val('');
		}
	});

	$addressForm.on('click', '.cancel', function (e) {
		e.preventDefault();
		dialog.close();
	});

	$addressForm.on('submit', function (e) {
		e.preventDefault();
		
		// PREV-98: validation error messages are not displayed when adding address in Multi-Shipping. Added the following IF block.
		if (!$addressForm.valid()) {
			return false;
		}
		$.getJSON(Urls.addEditAddress, $addressForm.serialize(), function (response) {
			if (!response.success) {
				// @TODO: figure out a way to handle error on the form
				return;
			}
			var address = response.address,
				$shippingAddress = $(target).closest('.shippingaddress'),
				$select = $shippingAddress.find('.select-address'),
				$selected = $select.find('option:selected'),
				newOption = '<option value="' + address.UUID + '">' +
					((address.ID) ? '(' + address.ID + ')' : address.firstName + ' ' + address.lastName) + ', ' +
					address.address1 + ', ' + address.city + ', ' + address.stateCode + ', ' + address.postalCode +
					'</option>';
			dialog.close();
			if (add) {
				$('.shippingaddress select').removeClass('no-option').append(newOption);
				$('.no-address').hide();
			} else {
				$('.shippingaddress select').find('option[value="' + address.UUID + '"]').html(newOption);
			}
			// if there's no previously selected option, select it
			if ($selected.length === 0 || $selected.val() === '') {
				$select.find('option[value="' + address.UUID + '"]').prop('selected', 'selected').trigger('change');
			}
		});
	});

	//preserve the uuid of the option for the hop up form
	if (selectedAddressUUID) {
		//update the form with selected address
		$addressList.find('option').each(function () {
			//check the values of the options
			if ($(this).attr('value') === selectedAddressUUID) {
				$(this).prop('selected', 'selected');
				$addressDropdown.trigger('change');
			}
		});
	}
}

/**
 * @function
 * @description shows gift message box in multiship, and if the page is the multi shipping address page it will call initmultishipshipaddress() to initialize the form
 */
exports.init = function () {
	initMultiGiftMessageBox();
	if ($('.cart-row .shippingaddress .select-address').length > 0) {
		formPrepare.init({
			continueSelector: '[name$="addressSelection_save"]',
			formSelector: '[id$="multishipping_addressSelection"]'
		});
	}
	$('.edit-address').on('click', 'a', function (e) {
		e.preventDefault(); //JIRA PREV-205-Checkout Multiple Shipping Page - In Add/Edit address page cancel button not responding
		dialog.open({url: this.href, options: {open: function () {
			address.init();
			addEditAddress(e.target);
			require('../../validator').init(); //JIRA PREV-98 : validation error messages are not displayed when adding  multiple address.re-init validator.
			require('../../tooltip').init(); // JIRA PREV-84 : Multiple shipping page: Not displaying Tool tip. re-init tooltips.
		}}});
	});
	
	/*
	  Start JIRA PREV-99 : shipping methods is not displayed for 2nd address in right nav
	  Start JIRA PREV-103 : Selected shipping method will not update automatically in right nav
	 */
	$(document).find(".checkoutmultishipping select[name$='_shippingMethodID']").on("change", function(e){
		var shipmentID = $(this).data('shipmentid');
		var shippingMethodId = $("option:selected", this).attr('value');
		ajax.getJson({
			url: Urls.multiShippingSelectSM,
			data : {"shipmentID" : shipmentID, "shippingMethodId" : shippingMethodId},
			callback: function (data) {
				shipping.updateSummary(); 
				if(!data || !data.success) {
					window.alert("Couldn't select shipping method.");
					return false;
				}
			}
		});
	});
	/*End JIRA PREV-103, PREV-99*/
};

},{"../../ajax":1,"../../dialog":10,"../../tooltip":51,"../../util":52,"../../validator":53,"./address":20,"./formPrepare":22,"./shipping":25}],25:[function(require,module,exports){
'use strict';

var ajax = require('../../ajax'),
	formPrepare = require('./formPrepare'),
	progress = require('../../progress'),
	tooltip = require('../../tooltip'),
	util = require('../../util');

var shippingMethods;
/**
 * @function
 * @description Initializes gift message box, if shipment is gift
 */
function giftMessageBox() {
	// show gift message box, if shipment is gift
	$('.gift-message-text').toggleClass('hidden', $('input[name$="_shippingAddress_isGift"]:checked').val() !== 'true');
}

/**
 * @function
 * @description updates the order summary based on a possibly recalculated basket after a shipping promotion has been applied
 */
function updateSummary() {
	var $summary = $('#secondary.summary');
	// indicate progress
	progress.show($summary);

	// load the updated summary area
	$summary.load(Urls.summaryRefreshURL, function () {
		// hide edit shipping method link
		$summary.fadeIn('fast');
		$summary.find('.checkout-mini-cart .minishipment .header a').hide();
		$summary.find('.order-totals-table .order-shipping .label a').hide();
		util.miniSummaryArrow();
	});
}

/**
 * @function
 * @description Helper method which constructs a URL for an AJAX request using the
 * entered address information as URL request parameters.
 */
function getShippingMethodURL(url, extraParams) {
	var $form = $('.address');
	var params = {
		address1: $form.find('input[name$="_address1"]').val(),
		address2: $form.find('input[name$="_address2"]').val(),
		countryCode: $form.find('select[id$="_country"]').val(),
		stateCode: $form.find('select[id$="_state"]').val(),
		postalCode: $form.find('input[name$="_postal"]').val(),
		city: $form.find('input[name$="_city"]').val()
	};
	return util.appendParamsToUrl(url, $.extend(params, extraParams));
}

/**
 * @function
 * @description selects a shipping method for the default shipment and updates the summary section on the right hand side
 * @param
 */
function selectShippingMethod(shippingMethodID) {
	// nothing entered
	if (!shippingMethodID) {
		return;
	}
	// attempt to set shipping method
	var url = getShippingMethodURL(Urls.selectShippingMethodsList, {shippingMethodID: shippingMethodID});
	ajax.getJson({
		url: url,
		callback: function (data) {
			updateSummary();
			if (!data || !data.shippingMethodID) {
				window.alert('Couldn\'t select shipping method.');
				return false;
			}
			// display promotion in UI and update the summary section,
			// if some promotions were applied
			$('.shippingpromotions').empty();


			// if (data.shippingPriceAdjustments && data.shippingPriceAdjustments.length > 0) {
			// 	var len = data.shippingPriceAdjustments.length;
			// 	for (var i=0; i < len; i++) {
			// 		var spa = data.shippingPriceAdjustments[i];
			// 	}
			// }
		}
	});
}

/**
 * @function
 * @description Make an AJAX request to the server to retrieve the list of applicable shipping methods
 * based on the merchandise in the cart and the currently entered shipping address
 * (the address may be only partially entered).  If the list of applicable shipping methods
 * has changed because new address information has been entered, then issue another AJAX
 * request which updates the currently selected shipping method (if needed) and also updates
 * the UI.
 */
function updateShippingMethodList() {
	var $shippingMethodList = $('#shipping-method-list');
	if (!$shippingMethodList || $shippingMethodList.length === 0) { return; }
	var url = getShippingMethodURL(Urls.shippingMethodsJSON);

	ajax.getJson({
		url: url,
		callback: function (data) {
			if (!data) {
				window.alert('Couldn\'t get list of applicable shipping methods.');
				return false;
			}
			if (false && shippingMethods && shippingMethods.toString() === data.toString()) { // PREVAIL-Added for 'false' to handle SPC.
				// No need to update the UI.  The list has not changed.
				return true;
			}

			// We need to update the UI.  The list has changed.
			// Cache the array of returned shipping methods.
			shippingMethods = data;
			// indicate progress
			//progress.show($shippingMethodList);

			// load the shipping method form
			var smlUrl = getShippingMethodURL(Urls.shippingMethodsList);
			$shippingMethodList.load(smlUrl, function () {
				$shippingMethodList.fadeIn('fast');
				// rebind the radio buttons onclick function to a handler.
				$shippingMethodList.find('[name$="_shippingMethodID"]').click(function () {
					selectShippingMethod($(this).val());
				});

				// update the summary
				updateSummary();
				progress.hide();
				tooltip.init();
				if($(".input-radio.ship-hide").length > 0){
		    		$(".input-radio.ship-hide").closest(".form-row.form-indent.label-inline").hide();
		    	}
				if(!$('input[data-default=true]').prop("checked") && $(".input-radio.ship-hide").prop("checked")){
					$('input[data-default=true]').prop("checked",true);
			    }
				//If click and collect shipping method is enable,Onload enabling the standerd delivary
				if(!$('input[id=shipping-method-standard]').prop("checked") && $(".input-radio.ship-hide").prop("checked")){
			    	$('input[id=shipping-method-standard]').prop("checked",true);
			    }
				//if nothing is selected in the shipping methods select the first one
				if ($shippingMethodList.find('.input-radio:checked').length === 0) {
					$shippingMethodList.find('.input-radio:first').prop('checked', 'checked');
				}
			});
		}
	});
}

exports.init = function () {
	/*formPrepare.init({
		continueSelector: '[name$="shippingAddress_save"]',
		formSelector:'[id$="singleshipping_shippingAddress"]'
	});*/
	$('input[name$="_shippingAddress_isGift"]').on('click', giftMessageBox);

   //Made the changes for GBSS-22
	$('.address').on('change input',
			'input[name$="_addressFields_address1"], input[name$="_addressFields_address2"], select[name$="_addressFields_states_state"], input[name$="_addressFields_city"], input[name$="_addressFields_postal"]', function(){ 
			 updateShippingMethodList();
	});
	$("body").on('click','.pcaitem',function(){
		//GB-187
		$('select[name$="_addressFields_states_state"]').val("");
		setTimeout(function(){ 
			$('select[name$="_addressFields_states_state"]').trigger("change"); 
			formPrepare.init({
				continueSelector: '[name$="shippingAddress_save"]',
				formSelector:'[id$="singleshipping_shippingAddress"]'
			});
		}, 1000);
	});
	giftMessageBox();
	updateShippingMethodList();
	
	//PREVAIL - init Address Validation
	/*if(isAVSEnabled){
		require('../../addressvalidation').init();
	}*/
};

exports.updateShippingMethodList = updateShippingMethodList;
exports.updateSummary = updateSummary; //JIRA PREV-99 : shipping methods is not displayed for 2nd address in right nav.

},{"../../ajax":1,"../../progress":40,"../../tooltip":51,"../../util":52,"./formPrepare":22}],26:[function(require,module,exports){
'use strict';

var addProductToCart = require('./product/addToCart'),
	ajax = require('../ajax'),
	page = require('../page'),
	productTile = require('../product-tile'),
	quickview = require('../quickview');

/**
 * @private
 * @function
 * @description Binds the click events to the remove-link and quick-view button
 */
function initializeEvents() {
	$('#compare-table').on('click', '.remove-link', function (e) {
		e.preventDefault();
		ajax.getJson({
			url: this.href,
			callback: function (response) {
				/* Start JIRA PREV-74 : Compare page: Not navigating to PLP, When user clicks the "Remove (X)" icon on the last product,present in the product compare page.
				   Added condition to check for the last product removal if so navigate back to previous PLP. 
				 */ 
					if(response.success && $("#compare-table .product-tile").length <= 1 && $("#compare-category-list").length == 0){
						window.location.href = $(".back").attr("href");
					}else if(response.success && $("#compare-table .product-tile").length <= 1 && $("#compare-category-list").length > 0){
						$("#compare-category-list option:selected").remove();
						$("#compare-category-list").trigger("change");
					}else{
						page.refresh();
					}
				/*End JIRA PREV-74 */
			}
		});
	})
	.on('click', '.open-quick-view', function (e) {
		e.preventDefault();
		var url = $(this).closest('.product').find('.thumb-link').attr('href');
		quickview.show({
			url: url,
			source: 'quickview'
		});
	});

	$('#compare-category-list').on('change', function () {
		$(this).closest('form').submit();
	});
}

exports.init = function () {
	productTile.init();
	initializeEvents();
	addProductToCart();
};

},{"../ajax":1,"../page":17,"../product-tile":39,"../quickview":41,"./product/addToCart":27}],27:[function(require,module,exports){
'use strict';

var dialog = require('../../dialog'),
	minicart = require('../../minicart'),
	page = require('../../page'),
	util = require('../../util'),
	TPromise = require('promise'),
	_ = require('lodash');

/**
 * @description Make the AJAX request to add an item to cart
 * @param {Element} form The form element that contains the item quantity and ID data
 * @returns {Promise}
 */
var addItemToCart = function (form) {
	var $form = $(form),
		$qty = $form.find('input[name="Quantity"]');
	if ($qty.length === 0 || isNaN($qty.val()) || parseInt($qty.val(), 10) === 0) {
		$qty.val('1');
	}
	
	//prevail- Added for GTM integration
	
	var gtmevents= require('../../gtmevents');
	gtmevents.addToCart($form);
	
	
	return TPromise.resolve($.ajax({
		type: 'POST',
		async: false,//JIRA PREV-305:SG issue: No of items in cart value not displayed as expected.Added async:false.
		url: util.ajaxUrl(Urls.addProduct),
		data: $form.serialize()
	}));
};

/**
 * @description Handler to handle the add to cart event
 */
var addToCart = function(e) {
    e.preventDefault();
    var cuobj = $(this);
    var value =$(this).closest("form").find("#Quantity").val();
    var maxOrderQty = 50;
    var currentForm = cuobj.closest("form[id]");
    if(value.length == 0){
        $(this).closest("form").find("#Quantity").val(1);
        value = 1;
    }
    if (value > maxOrderQty) {
        currentForm.find('.add-to-cart').attr("disabled", true);
        $('#add-all-to-cart').attr("disabled", true);
        $('.span-messg').text(Resources.MAXORDERABLEQUANTITYERROR);
        var col2Height = ($('.product-col-2.product-detail').outerHeight() - 533);
		$('.pdp-main .socialsharing.social-links').css('padding-bottom',col2Height);
    } else {
        currentForm.find('.add-to-cart').attr("disabled", false);
        $('#add-all-to-cart').attr("disabled", false);
        $('.span-messg').text('');
    }
    var $form = $(this).closest('form');
    checkQuantity($form).then(function(resp){
    	if(resp.success){
		    if (value <= maxOrderQty) {
		        addItemToCart($form).then(function(response) {
		            var $uuid = $form.find('input[name="uuid"]');
		            if ($uuid.length > 0 && $uuid.val().length > 0) {
		                page.refresh();
		            } else {
		                // do not close quickview if adding individual item that is part of product set
		                // @TODO should notify the user some other way that the add action has completed successfully
		                if (!$(this).hasClass('sub-product-item')) {
		                    dialog.close();
		                }
		                minicart.show(response);
		            }
		        }.bind(this));
		    }
    	}else{
    		$form.find('.add-to-cart').attr("disabled", true);
	        $('#add-all-to-cart').attr("disabled", true);
	        $('.span-messg').text(Resources.MAXORDERABLEQUANTITYERROR);
	        var col2Height = ($('.product-col-2.product-detail').outerHeight() - 533);
			$('.pdp-main .socialsharing.social-links').css('padding-bottom',col2Height);
    	}
    });
};

/**
 * @description Handler to handle the add all items to cart event
 */
var addAllToCart = function (e) {
	e.preventDefault();
	var cuobj = $(this);
	var value =$('.product-set-details #Quantity').val(); 
	if(value.length == 0){
	   value = 1;
       $('.product-set-details #Quantity').val(1)
	}
	
	var $productForms = $('#product-set-list').find('form').toArray();
	TPromise.all(_.map($productForms, addItemToCart))
	.then(function (responses) {
	dialog.close();
	// show the final response only, which would include all the other items
	minicart.show(responses[responses.length - 1]);
	});
	};


/**
 * @description Checks for the product quantity in the basket
 */
var checkQuantity = function (form) {
	var checkQuantityUrl = Urls.checkQuantity;
	checkQuantityUrl = util.appendParamToURL(checkQuantityUrl, 'pid', form.find("input[name='pid']").val());
	checkQuantityUrl = util.appendParamToURL(checkQuantityUrl, 'Quantity', form.find("input[name='Quantity']").val());
	checkQuantityUrl = util.ajaxUrl(checkQuantityUrl);
	return TPromise.resolve($.ajax({
		type: 'POST',
		async: false,//JIRA PREV-305:SG issue: No of items in cart value not displayed as expected.Added async:false.
		url: checkQuantityUrl
	}));
};

/**
 * @function
 * @description Binds the click event to a given target for the add-to-cart handling
 */
module.exports = function () {
	$('.add-to-cart[disabled]').attr('title', $('.availability-msg').text());
	//Start JIRA PREV-454, PREV-469 : Application navigation not consistent when click of add to cart button of the Product set page
	$('#pdpMain, .pt_wish-list').on('click', '.add-to-cart', addToCart);
	$('.product-add-to-cart #add-all-to-cart').on('click', addAllToCart);
};

},{"../../dialog":10,"../../gtmevents":14,"../../minicart":16,"../../page":17,"../../util":52,"lodash":58,"promise":59}],28:[function(require,module,exports){
'use strict';

var ajax =  require('../../ajax'),
	util = require('../../util');

var updateContainer = function (data, $this) {
	var currentForm = $this.closest("form[id]");
	var $availabilityMsgContainer = currentForm.find('.availability-msg'),
	$availabilityMsg;
	if (!data) {
		$availabilityMsgContainer.html(Resources.ITEM_STATUS_NOTAVAILABLE);
		return;
	}
	if(data.isMaster){
		return false;
	}
	$availabilityMsgContainer.empty();
	// Look through levels ... if msg is not empty, then create span el
	if (data.levels.IN_STOCK > 0) {
		$availabilityMsg = $availabilityMsgContainer.find('.in-stock-msg');
		if ($availabilityMsg.length === 0) {
			$availabilityMsg = $('<p/>').addClass('in-stock-msg').appendTo($availabilityMsgContainer);
		}
		if (data.levels.PREORDER === 0 && data.levels.BACKORDER === 0 && data.levels.NOT_AVAILABLE === 0) {
			// Just in stock
			$availabilityMsg.text(Resources.IN_STOCK);
		} else {
			// In stock with conditions ...
			$availabilityMsg.text(data.inStockMsg);
		}
		$availabilityMsgContainer.removeClass('not-available');
		currentForm.find('.add-to-cart').attr("disabled",false);
	}
	if (data.levels.PREORDER > 0) {
		$availabilityMsg = $availabilityMsgContainer.find('.preorder-msg');
		if ($availabilityMsg.length === 0) {
			$availabilityMsg = $('<p/>').addClass('preorder-msg');
		}
		if (data.levels.IN_STOCK === 0 && data.levels.BACKORDER === 0 && data.levels.NOT_AVAILABLE === 0) {
			// Just in stock
			$availabilityMsg.text(Resources.PREORDER);
		} else {
			$availabilityMsg.text(data.preOrderMsg);
		}
	}
	if (data.levels.BACKORDER > 0) {
		$availabilityMsg = $availabilityMsgContainer.find('.backorder-msg');
		if ($availabilityMsg.length === 0) {
			$availabilityMsg = $('<p/>').addClass('backorder-msg');
		}
		if (data.levels.IN_STOCK === 0 && data.levels.PREORDER === 0 && data.levels.NOT_AVAILABLE === 0) {
			// Just in stock
			$availabilityMsg.text(Resources.BACKORDER);
		} else {
			$availabilityMsg.text(data.backOrderMsg);
		}
	}
	if (data.inStockDate !== '') {
		$availabilityMsg = $availabilityMsgContainer.find('.in-stock-date-msg');
		if ($availabilityMsg.length === 0) {
			$availabilityMsg = $('<p/>').addClass('in-stock-date-msg');
		}
		$availabilityMsg.text(String.format(Resources.IN_STOCK_DATE, data.inStockDate));
	}
	if (data.levels.NOT_AVAILABLE > 0) {
		$availabilityMsg = $availabilityMsgContainer.find('.not-available-msg');
		if ($availabilityMsg.length === 0) {
			$availabilityMsg = $('<p/>').addClass('not-available-msg');
		}
		if (data.levels.PREORDER === 0 && data.levels.BACKORDER === 0 && data.levels.IN_STOCK === 0) {
			$availabilityMsg.text(Resources.NOT_AVAILABLE);
		} else {
			$availabilityMsg.text(Resources.REMAIN_NOT_AVAILABLE);
			$availabilityMsgContainer.addClass('not-available');
		}
		currentForm.find('.add-to-cart').attr("disabled",true);
		$('#add-all-to-cart').attr("disabled",true);
		
	}else{ 
		if($('.add-to-cart[disabled="disabled"]').length == 0) {
			$('#add-all-to-cart').attr("disabled",false);
		}else {
			$('#add-all-to-cart').attr("disabled",true);
		}
	}

	$availabilityMsgContainer.append($availabilityMsg);
};

var getAvailability = function () {
	var $this = $(this);
	if($this.val()!=""){
		ajax.getJson({
			url: util.appendParamsToUrl(Urls.getAvailability, {
				pid: $this.closest('form').find('#pid').val(), // JIRA PREV-55:Inventory message is not displaying for the individual product within the product set.
				Quantity: $(this).val(),
				format: "ajax"
			}),
			async : false, //JIRA PREV-45 : Able to proceed to checkout flow with more than instock qty by clicking on Go straight to checkout in the Mini cart.
			callback: function(data){
				updateContainer(data, $this);
			}
		});
	}
};

module.exports = function () {
	$('#pdpMain').on('change', '.pdpForm input[name="Quantity"]', getAvailability);
};

},{"../../ajax":1,"../../util":52}],29:[function(require,module,exports){
'use strict';
var dialog = require('../../dialog'),
	util = require('../../util');

var zoomMediaQuery = matchMedia('(min-width: 1024px)');

/**
 * @description Enables the zoom viewer on the product detail page
 * @param zmq {Media Query List}
 */
var loadZoom = function (zmq) {
	var $imgZoom = $('#pdpMain .main-image'),
		hiresUrl;
	
	if(util.isMobile()) {
		 $('.pinch_zoom').each(function () {
	           new RTP.PinchZoom($(this), {});
	      });
	}
	
};

//zoomMediaQuery.addListener(loadZoom);

/**
 * @description Sets the main image attributes and the href for the surrounding <a> tag
 * @param {Object} atts Object with url, alt, title and hires properties
 */
var setMainImage = function (atts) {
	$('#pdpMain .primary-image').attr({
		src: atts.url,
		alt: atts.alt,
		title: atts.title
	});
	if (!dialog.isActive() && !util.isMobile()) {
		$('#pdpMain .main-image').attr('href', atts.hires);
	}
	slickInit();
	util.selectbox();
	loadZoom();
};

var slickInit = function(){
	if($(window).width() < 767) {
		if($('.product-primary-image .mob-hide').length > 0) {
			$('.product-primary-image .mob-hide').remove();
		}
	}
	
	/*if($('.product-col-1 .product-primary-image').hasClass('slick-slider')){
		$('.product-col-1 .product-primary-image').slick('unslick');
	}*/
	

	
	$('.product-col-1 .product-primary-image').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  infinite: true,
	  arrows: true,
	  vertical: false,
	  asNavFor: '.product-col-1 #thumbnails ul'
	});
	
	
	/*if($('.product-col-1 #thumbnails ul').hasClass('slick-slider')){
		$('.product-col-1 #thumbnails ul').slick('unslick');
	}*/

	$('.product-col-1 #thumbnails ul').slick({
	  slidesToShow: 4,
      slidesToScroll: 1,
	  arrows: true,
	  infinite: true,
	  vertical: true,
	  focusOnSelect: true,
	  asNavFor: '.product-col-1 .product-primary-image'
	});
	
		
	/*only for 2 primary image start*/
	/*if($('.product-col-1 .product-primary-image.product-slider-twoimages').hasClass('slick-slider')){
		$('.product-col-1 .product-primary-image.product-slider-twoimages').slick('unslick');
	}
	$('.product-col-1 .product-primary-image.product-slider-twoimages').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  infinite: true,
	  arrows: true,
	  focusOnSelect: true,
	  asNavFor: '.product-col-1 #thumbnails ul',
		  responsive: [{
	          breakpoint: 1023,
	          settings: {
	        	  speed:100
	          }
	      }]
	});*/
	/*only for 2 primary image end*/
	$('.get-the-look-products').slick({
		  slidesToShow: 4,
		  slidesToScroll: 1,
		  infinite: true,
		  arrows: true
	});
	if($(window).width() < 768) {
		if($('.get-the-look-products').hasClass('slick-slider')){
			$('.get-the-look-products').slick('unslick');
		}
		
    	$('.get-the-look-products').slick({
    		responsive: [{
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                	slidesToScroll: 1,
                	dots: true
                    //dotsClass: 'custom_paging',
                    //customPaging: function (slider, i) {
                        //return  (i + 1) + '/' + slider.slideCount;
                    //},
          		  	//infinite: true,
          		  	//speed: 300
                }
            }]
    	});
    }
}
/**
 * @description Replaces the images in the image container, for eg. when a different color was clicked.
 */
var replaceImages = function () {
	var $newImages = $('#update-images'),
		$imageContainer = $('#pdpMain .product-image-container');
	if ($newImages.length === 0) { return; }

	$imageContainer.html($newImages.html());
	$newImages.remove();
	slickInit();
	util.selectbox();
	loadZoom();
};

/* @module image
 * @description this module handles the primary image viewer on PDP
 **/

/**
 * @description by default, this function sets up zoom and event handler for thumbnail click
 **/
module.exports = function () {
	if (dialog.isActive() || util.isMobile()) {
		$('#pdpMain .main-image').removeAttr('href');
	}
	slickInit();
	util.selectbox();
	loadZoom();
	// handle product thumbnail click event
	/*$('#pdpMain').on('click', '.productthumbnail', function () {
		// switch indicator
		$(this).closest('.product-thumbnails').find('.thumb.selected').removeClass('selected');
		$(this).closest('.thumb').addClass('selected');

		setMainImage($(this).data('lgimg'));
	});*/
};
module.exports.loadZoom = loadZoom;
module.exports.setMainImage = setMainImage;
module.exports.replaceImages = replaceImages;

},{"../../dialog":10,"../../util":52}],30:[function(require,module,exports){
'use strict';

var dialog = require('../../dialog'),
	productStoreInventory = require('../../storeinventory/product'),
	tooltip = require('../../tooltip'),
	util = require('../../util'),
	addToCart = require('./addToCart'),
	availability = require('./availability'),
	image = require('./image'),
	productSet = require('./productSet'),
	recommendations = require('./recommendations'),
	rating = require('../../rating'), //PREVAIL-Removed this from app.js and placed here to avoid issues during AJAX calls.
	variant = require('./variant');

/**
 * @description Initialize product detail page with reviews, recommendation and product navigation.
 */
function initializeDom() {
	tooltip.init();
}
/**
 * @description Initialize event handlers on product detail page
 */
function initializeEvents() {
	var $pdpMain = $('#pdpMain');

	addToCart();
	availability();
	variant();
	image();
	productSet();
	rating.init();//PREVAIL-Removed this from app.js and placed here to avoid issues during AJAX calls.
	if (SitePreferences.STORE_PICKUP) {
		productStoreInventory.init();
	}

	// Add to Wishlist and Add to Gift Registry links behaviors
	$pdpMain.on('click', '[data-action="wishlist"], [data-action="gift-registry"]', function () {
		var data = util.getQueryStringParams($('.pdpForm').serialize());
		if (data.cartAction) {
			delete data.cartAction;
		}
		var url = util.appendParamsToUrl(this.href, data);
		if(($(this).attr("data-action") == "wishlist")){
			var gtmevents= require('../../gtmevents');
			var id = $('.pdpForm').find(' input[name="pid"]').val();
			var name = $('.pdpForm').find(' input[name="name"]').val();
			gtmevents.addToWishlist(id,name);
		}
		this.setAttribute('href', url);
	});
	
	//custom scroll for PDP tab-content section
	if($(window).width() > 1024) {
		$('.tabs .tab').each(function() {
			$(this).find('.inner-tab-content').enscroll({
				verticalTrackClass: 'track1',
			    verticalHandleClass: 'handle1',
			    drawScrollButtons: true,
			    scrollUpButtonClass: 'scroll-up1',
			    scrollDownButtonClass: 'scroll-down1'
	       });
		});
	}

	//to aviod pdp product col2 overlapping in desktop
	$(window).load(function() {
		var col2Height = ($('.product-col-2.product-detail').outerHeight() - 533);
		$('.pdp-main .socialsharing.social-links').css('padding-bottom',col2Height);
	});

	// product options
	$pdpMain.on('change', '.product-options select', function () {
		/*Start JIRA PREV-66:Incorrect price displayed for the product set when options are selected for multiple individual products.
		Start JIRA PREV-67:Incorrect price displayed for the standard product when multiple options are selected.
		Added conditions for Product Set,Product Bundle and Normal Product*/
        var currentForm = $(this).closest("form");
        if (currentForm.hasClass("setProduct")) {
            var salesPrice = currentForm.find(".add-sub-product .price-sales");
            var selectedItem = $(this).children().filter(":selected").first();
            var combinedPrice = selectedItem.data("combined");
            salesPrice.text(combinedPrice);
            var setSalePrice = $("#pdpMain").find(".product-col-2.product-set").find(".product-add-to-cart").find(".product-price").find(".salesprice");
            var originalSetPrice = 0;
            $('.add-sub-product').find('.product-price').find('.price-sales').each(function(index) {
                var setlevelProductPrice = Number($(this).text().replace(/[^0-9\.]+/g, ""));
                originalSetPrice = originalSetPrice + setlevelProductPrice;
            });
            var currency = $('#currency').val();
            setSalePrice.text(currency + (parseFloat(originalSetPrice.toFixed(2))));
        } else {
            if (!($(this).closest('.product-set-item').hasClass('product-bundle-item')) || ($('.product-add-to-cart').find('form').hasClass('normalProduct'))) {
                var salesPrice = $pdpMain.find("div.product-add-to-cart .price-sales");
                var currency = $('#currency').val();
                if ($('.product-add-to-cart').find('form').find('.product-options').find('select option:selected').length > 1) {
                    var multiStandardProd = 0;
                    $('.product-add-to-cart').find('form').find('.product-options').find('select').each(function() {
                        multiStandardProd = multiStandardProd + Number(($(this).find('option:selected').data('setprodprice')).replace(/[^0-9\.]+/g, ""));
                    });
                    if ($('#product-content').find('.product-price').first().find('.price-sales').length > 0 &&
                        ($('#product-content').find('.product-price').first().find('.price-standard').length == 0)) {
                        var totalSalePrice = multiStandardProd + Number($('#product-content').find('.product-price').first().find('.price-sales').text().replace(/[^0-9\.]+/g, ""));
                    } else {
                        if ($('#product-content').find('.product-price').first().find('.price-standard').length > 0 &&
                            ($('#product-content').find('.product-price').first().find('.price-sales').length == 0)) {
                            var totalSalePrice = multiStandardProd + Number($('#product-content').find('.product-price').first().find('.price-standard').text().replace(/[^0-9\.]+/g, ""));
                        }
                    }

                    if (($('#product-content').find('.product-price').first().find('.price-standard').length > 0) &&
                        ($('#product-content').find('.product-price').first().find('.price-sales').length > 0)) {
                        var totalSalePrice = multiStandardProd + Number($('#product-content').find('.product-price').first().find('.price-sales').text().replace(/[^0-9\.]+/g, ""));
                    }

                    salesPrice.text(currency + (parseFloat(totalSalePrice.toFixed(2))));
                } else {
                    var selectedItem = $(this).children().filter(":selected").first();
                    var combinedPrice = selectedItem.data("combined");
                    salesPrice.text(combinedPrice);
                }

            }

        }
        if ($(this).closest('.product-set-item').hasClass('product-bundle-item')) {
            var bundleProduct = $(this).closest('.product-bundle-item');
            var bundleInitialPrice = $('.product-col-2').find('.product-price').first().find('span.price-sales').text();
            var numberPrice = Number(bundleInitialPrice.replace(/[^0-9\.]+/g, ""));
            var currency = $('#currency').val();
            var bundleLevelPriceForOption = 0;
            var price = '';
            $('.product-bundle-item').each(function() {
                if ($(this).find('.product-options').find('select option:selected').length > 1) {
                    var multiOptionsPrice = '';
                    var numMulti = 0;
                    $(this).find('.product-options').find('select').each(function() {
                        multiOptionsPrice = $(this).find('option:selected').data('setprodprice');
                        numMulti = numMulti + Number(multiOptionsPrice.replace(/[^0-9\.]+/g, ""));
                    });
                    price = currency + numMulti;
                } else {
                    price = $(this).find('.product-options').find('select option:selected').data('setprodprice');
                }

                var numPrice = 0;
                if (price != undefined) {
                    numPrice = Number(price.replace(/[^0-9\.]+/g, ""));
                }
                bundleLevelPriceForOption = bundleLevelPriceForOption + numPrice;
            });
            var totalBundlePrice = numberPrice + bundleLevelPriceForOption;
            var bundleLevelPriceTotal = $('.bundle').find('.product-add-to-cart').find('.product-price').find('.price-sales');
            bundleLevelPriceTotal.text(currency + (parseFloat(totalBundlePrice.toFixed(2))));
        }
        /*End JIRA PREV-66,PREV-67*/
	});

	// prevent default behavior of thumbnail link and add this Button
	$pdpMain.on('click', '.thumbnail-link, .unselectable a', function (e) {
		e.preventDefault();
	});

	//JIRA PREV-386 : On click of size chart link, the page displayed instead of overlay. Replaced $('.size-chart-link a').on('click', with $pdpMain.on('click', '.size-chart-link a',
	$pdpMain.on('click', '.size-chart-link a', function (e) {
		e.preventDefault();
		dialog.open({
			url: $(e.target).attr('href')
		});
	});
	//PDP zoom
	$("body").on('click','a.product-zoom', function(e){
		e.preventDefault();
		var pid = $("#pdp-zoom-prod").val();
		var url = Urls.PDPZoom;
		url = util.appendParamToURL(url, 'pid', pid);
		dialog.open({
			url : url,
			options : {
				dialogClass : 'PDP-product-zoom'
			},
			callback: function () {
				$('.ui-dialog .product-primary-image').slick({
				  slidesToShow: 1,
				  slidesToScroll: 1,
				  infinite: true,
				  arrows: true,
				  focusOnSelect: true,
				  asNavFor: '.ui-dialog #thumbnails ul',
				  initialSlide: parseInt($('#pdpMain .product-primary-image .slick-slide.slick-current:not(".slick-cloned")').attr('data-slick-index'))
				});
				$('.ui-dialog #thumbnails ul').slick({
				  asNavFor: '.ui-dialog .product-primary-image',
				  slidesToShow: 1,
				  slidesToScroll: 1,
				  arrows: false,
				  focusOnSelect: true,
				  initialSlide: parseInt($('#pdpMain .product-primary-image .slick-slide.slick-current:not(".slick-cloned")').attr('data-slick-index'))
				});
			}
		});
	});
	//PDP Video
	$(".thumb, .main-video, .slick-arrow").on('click', function(e){
		e.preventDefault();
		if($(this).hasClass('productVideos')) {
			$(".video-play-container").css("display", "block");
			$("#video-iframe").each(function(){
				$('.zoompad').addClass('zoom-tab');
				this.contentWindow.postMessage('{"event":"command","func":"' + 'playVideo' + '","args":""}', '*');
			});
		}
		else {
			$("#video-iframe").each(function(){
				$('.zoompad').removeClass('zoom-tab');
				this.contentWindow.postMessage('{"event":"command","func":"' + 'pauseVideo' + '","args":""}', '*');
			});
		}
		/*var pid = $("#pdp-zoom-prod").val();
		var url = Urls.PDPvideo;
		url = util.appendParamToURL(url, 'pid', pid);
		dialog.open({
			url : url,
			options : {
				dialogClass : 'PDP-product-video'
			},
			callback: function () {
				$(".ui-icon-closethick").on("click", function() {
					$('.video-container embed').remove();
				});
			}
		});*/
	});
	$(window).load(function(){
		var recomm = setInterval(function(){  recommend(); }, 5000);
		function recommend() {
			if($('.recommendations #carousel-recommendations ul').hasClass('slick-slider')) {
				clearInterval(recomm);
			}
			else {
				recommendations();
			}
		}
	});
}

var product = {
	initializeEvents: initializeEvents,
	init: function () {
		initializeDom();
		initializeEvents();
	}
};

module.exports = product;

},{"../../dialog":10,"../../gtmevents":14,"../../rating":42,"../../storeinventory/product":50,"../../tooltip":51,"../../util":52,"./addToCart":27,"./availability":28,"./image":29,"./productSet":31,"./recommendations":32,"./variant":33}],31:[function(require,module,exports){
'use strict';

var addToCart = require('./addToCart'),
	ajax = require('../../ajax'),
	tooltip = require('../../tooltip'),
	util = require('../../util');

module.exports = function () {
	var $addToCart = $('#add-to-cart'),
		$addAllToCart = $('#add-all-to-cart'),
		$productSetList = $('#product-set-list');

	var updateAddToCartButtons = function () {
		if ($productSetList.find('.add-to-cart[disabled]').length > 0) {
			$addAllToCart.attr('disabled', 'disabled');
			// product set does not have an add-to-cart button, but product bundle does
			$addToCart.attr('disabled', 'disabled');
		} else {
			$addAllToCart.removeAttr('disabled');
			$addToCart.removeAttr('disabled');
		}
	};

	if ($productSetList.length > 0) {
		updateAddToCartButtons();
	}
	// click on swatch for product set
	$productSetList.on('click', '.product-set-item .swatchanchor', function (e) {
		e.preventDefault();
		if ($(this).parents('li').hasClass('unselectable')) { return; }
		var url = Urls.getSetItem + this.search;
		var $container = $(this).closest('.product-set-item');
		var qty = $container.find('form input[name="Quantity"]').first().val();

		ajax.load({
			url: util.appendParamToURL(url, 'Quantity', isNaN(qty) ? '1' : qty),
			target: $container,
			callback: function () {
				updateAddToCartButtons();
				$('.product-set-item').find('form input[name="Quantity"]').trigger('change');//JIRA PREV-235:PD page: 'Not Available' message is not displaying when more than Available Qty entered in Qty field.
				tooltip.init();
			}
		});
	});
};

},{"../../ajax":1,"../../tooltip":51,"../../util":52,"./addToCart":27}],32:[function(require,module,exports){
'use strict';

/**
 * @description Creates product recommendation carousel using jQuery jcarousel plugin
 **/
module.exports = function () {

	$('.recommendations #carousel-recommendations ul').slick({
		  dots: false,
		  infinite: true,
		  speed: 300,
		  slidesToShow: 5,
		  slidesToScroll: 1,
		  responsive: [{
          breakpoint: 1023,
          settings: {
              slidesToShow: 3
          }
      }, {
          breakpoint: 768,
          settings: {
              slidesToShow: 1,
              dots: true
          }
      }]
	});	
};


},{}],33:[function(require,module,exports){
'use strict';

var addToCart = require('./addToCart'),
	ajax = require('../../ajax'),
	image = require('./image'),
	progress = require('../../progress'),
	productStoreInventory = require('../../storeinventory/product'),
	tooltip = require('../../tooltip'),
	util = require('../../util'),
	minicart = require('../../minicart'),
	dialog = require('../../dialog'),
	availability = require('./availability'),
	bisn = require('../../bisn'),
	redeye = require('../../redeyetracking'),
	gtmevents= require('../../gtmevents'),
	validator = require('../../validator'),
	TPromise = require('promise');


/**
 * @description update product content with new variant from href, load new content to #product-content panel
 * @param {String} href - url of the new product variant
 **/
//Commented for GB-674 start
/*var updateContent = function (href) {
	var $pdpForm = $('.pdpForm');
	var qty = $pdpForm.find('input[name="Quantity"]').first().val();
	var params = {
		Quantity: isNaN(qty) ? '1' : qty,
		format: 'ajax',
		productlistid: $pdpForm.find('input[name="productlistid"]').first().val()
	};

	progress.show($('#pdpMain'));

	ajax.load({
		url: util.appendParamsToUrl(href, params),
		target: $('#product-content'),
		callback: function () {
			if (SitePreferences.STORE_PICKUP) {
				productStoreInventory.init();
			}
			image.replaceImages();
			$('#Quantity').trigger('change'); //JIRA PREV-235:PD page: 'Not Available' message is not displaying when more than Available Qty entered in Qty field.
			tooltip.init();
			//$('#product-content').trigger('ready');  PREVAIL- Added for Gigya integration
			availability.getAvailability;
		}
	});
};*/
//Commented for GB-674 end

//Added for GB-674 start
var updateContent = function (href) {
	window.location = href.replace('Product-Variation','Product-Show');
};
//Added for GB-674 end


module.exports = function () {
	var $pdpMain = $('#pdpMain');
	// hover on swatch - should update main image with swatch image
	/*$pdpMain.on('mouseenter mouseleave', '.swatchanchor', function () {
		var largeImg = $(this).data('lgimg'),
			$imgZoom = $pdpMain.find('.main-image'),
			$mainImage = $pdpMain.find('.primary-image');

		if (!largeImg) { return; }
		// store the old data from main image for mouseleave handler
		$(this).data('lgimg', {
			hires: $imgZoom.attr('href'),
			url: $mainImage.attr('src'),
			alt: $mainImage.attr('alt'),
			title: $mainImage.attr('title')
		});
		// set the main image
		image.setMainImage(largeImg);
	});*/

	// click on swatch - should replace product content with new variant
	$pdpMain.on('click', '.product-detail .swatchanchor', function (e) {
		e.preventDefault();
		//if ($(this).parents('li').hasClass('unselectable')) { return; }
		updateContent(this.href);
	});
	// change drop down variation attribute - should replace product content with new variant
	$pdpMain.on('change', '.variation-select', function () {
		if ($(this).val().length === 0) { return; }
		updateContent($(this).val());
	});
	
	//keyup qty validation
	$(".pdp-main .pdpForm .quantity #Quantity").keyup(function(e) {
        e.preventDefault();
        var curObj = $(this);
        var maxOrderQty = 50 ;
        var currentForm = curObj.closest("form[id]");
        var inputfield = $(curObj).closest(".product-add-to-cart").find("input[name='Quantity']");
        var inputValue = $(inputfield).val();
        var $closestQty = $(curObj).closest(".quantity");
        if (inputValue > maxOrderQty) {
                        currentForm.find('.add-to-cart').attr("disabled", true);
                        $('#add-all-to-cart').attr("disabled", true);
                        $('.span-messg').text(Resources.MAXORDERABLEQUANTITYERROR);
                        var col2Height = ($('.product-col-2.product-detail').outerHeight() - 533);
                                      $('.pdp-main .socialsharing.social-links').css('padding-bottom',col2Height);
        } else {
              currentForm.find('.add-to-cart').attr("disabled", false);
              $('#add-all-to-cart').attr("disabled", false);
              $('.span-messg').text('');
          }
      });
	
	//updating the qty value of product 
	$(document).on("click", ".pdp-main .pdpForm .quantity a", function(e) {
	    e.preventDefault();
	    var curObj = $(this);
	    var maxOrderQty = 50 ;
	    var currentForm = curObj.closest("form[id]");
	    var inputfield = $(curObj).closest(".product-add-to-cart").find("input[name='Quantity']");
	    var inputValue = $(inputfield).val();
	    var $closestQty = $(curObj).closest(".quantity");
	    if ($(curObj).hasClass("prev-value")) {
	        if ($(inputfield).val() > 1) {
	            inputValue--;
	            $(inputfield).val(inputValue);
	        }
	    } else if ($(curObj).hasClass("next-value")) {
	        if ($(inputfield).val() >= 0 && $(inputfield).val() < 99) {
	            inputValue++;
	            $(inputfield).val(inputValue);
	        }
	    }
	    checkQuantity(currentForm).then(function(resp){
	    	if(resp.success){
			    if ($(inputfield).val() > maxOrderQty) {
			        currentForm.find('.add-to-cart').attr("disabled", true);
			        $('#add-all-to-cart').attr("disabled", true);
			        $('.span-messg').text(Resources.MAXORDERABLEQUANTITYERROR);
			        var col2Height = ($('.product-col-2.product-detail').outerHeight() - 533);
					$('.pdp-main .socialsharing.social-links').css('padding-bottom',col2Height);
			    } else {
			        currentForm.find('.add-to-cart').attr("disabled", false);
			        $('#add-all-to-cart').attr("disabled", false);
			        $('.span-messg').text('');
			        $(curObj).closest('.product-add-to-cart').find("input[name='Quantity']").val($(inputfield).val());
			        $(curObj).closest('.product-add-to-cart').find("input[name='Quantity']").trigger("change");
			    }
	    	}else{
	    		currentForm.find('.add-to-cart').attr("disabled", true);
		        $('#add-all-to-cart').attr("disabled", true);
		        $('.span-messg').text(Resources.MAXORDERABLEQUANTITYERROR);
		        var col2Height = ($('.product-col-2.product-detail').outerHeight() - 533);
				$('.pdp-main .socialsharing.social-links').css('padding-bottom',col2Height);
	    	}
	    });
	});
	//updating the qty value of productset 
	$(document).on("click"," .qty-block a",function(e){
		e.preventDefault();
		var curObj = $(this);
		var inputfield = $(this).closest(".block-add-to-cart").find("input[name='Quantity']");
		var inputValue = $(inputfield).val();
		var $closestQty = $(curObj).closest(".quantity");
		if($(curObj).hasClass("prev-value")) {
			if($(inputfield).val()  > 1) {
				inputValue --;
				$(inputfield).val(inputValue);
			}
		}
		
		else if($(curObj).hasClass("next-value")) {
			if($(inputfield).val()  >= 0 && $(inputfield).val() < 99) {
				inputValue ++;
				$(inputfield).val(inputValue);
			}
		}
		$(curObj).closest('.block-add-to-cart').find("input[name='Quantity']").val($(inputfield).val());
		$(curObj).closest('.block-add-to-cart').find("input[name='Quantity']").trigger("change");
	});
	
	//QTY field validation 
	$(".pdp-main .pdpForm .inventory .quantity").keyup(function(e){
		e.preventDefault();
		var  text = $(this).closest(".product-add-to-cart").find("input[name='Quantity']");
		var txt = text.val();
		var regex=/[^\d]/g;       
		if(txt != undefined && txt != '' && regex.test(txt)){
		   $(text).val(txt.replace(regex, ''));
		   return false;
		}else{
			return true;
		}
	});
	
	//productset qty field validation
	$('.product-set-details #Quantity').keydown(function (e) {
	   // Allow: backspace, delete, tab, escape, enter and .
	   if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
	        // Allow: Ctrl+A
	       (e.keyCode == 65 && e.ctrlKey === true) ||
	        // Allow: Ctrl+C
	       (e.keyCode == 67 && e.ctrlKey === true) ||
	        // Allow: Ctrl+X
	       (e.keyCode == 88 && e.ctrlKey === true) ||
	        // Allow: home, end, left, right
	       (e.keyCode >= 35 && e.keyCode <= 39)) {
	            // let it happen, don't do anything
	            return;
	   }
	   // Ensure that it is a number and stop the keypress
	   if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
	       e.preventDefault();
	   }
	});

	
	//save for later functionality
	$(document).on("click",".pdp-main .pdpForm .saveforlater button",function(e){
		e.preventDefault();
		var url = Urls.addToWishlist;
		var pid = $("input[name=pid]").val();
		var Quantity = $("input[name=Quantity]").val();
        url = util.appendParamToURL(url, 'pid', pid);
        url = util.appendParamToURL(url, 'Quantity', Quantity);
        url = util.appendParamToURL(url, 'format', 'ajax');

		$.ajax({
			type: 'GET',
			dataType: 'json',
			url: url,
			success: function (response) {
				if(response.success){
					if($(".pdp-main .pdpForm .saveforlater").length > 0){
						$(".pdp-main .pdpForm .saveforlater button").addClass("added");
					}
				}else{
					if($(".pdp-main .pdpForm .saveforlater .errormsg").length > 0){
						$(".pdp-main .pdpForm .saveforlater .errormsg").show();
						$('.pdp-main .pdpForm .saveforlater .errormsg .close-error-msg').on('click', function(){
							$('.pdp-main .pdpForm .saveforlater .errormsg').hide();
						});
						$(document).mouseup(function (e){
						    var container = $(".saveforlater .errormsg");
						    if (!container.is(e.target) && container.has(e.target).length === 0) {
						        container.hide();
						    }
						});
					}
				}
			}
		});
	});
	
	//order a sample product functionality
	$(document).on("click",".pdp-main .pdpForm .orderasample button",function(e){
		e.preventDefault();
		var pid = $(this).attr("pid");
		if(pid != '' && pid != null){
			addSampleToCart(pid);
		}
	});
	//order a sample product functionality when 1 or more products r there
	$(document).on("change",".pdp-main .pdpForm .orderasample #input-select",function(e){
		e.preventDefault();
		var pid = $(this).val();
		if(pid != '' && pid != null){
			addSampleToCart(pid);
		}
	});	
	
	$(document).on("click",".pdp-main .pdpForm .bisn .emailme",function(e){
		e.preventDefault();
		var url = Urls.emailMe;
		var pid = $(this).closest('form').find('#pid').val();
		url = util.appendParamToURL(url, 'pid', pid);
		url = util.appendParamToURL(url, 'format', 'ajax');
		$.ajax({
			type: 'GET',
			dataType: 'html',
			url: url,
			success: function (response) {
				$(".pdp-main .pdpForm .product-actions").html(response);
				bisn.init();
			}
		});
	});
	//roll calculator functionality
	$(document).on("click",".pdp-main .pdpForm .calculator a",function(e){
		e.preventDefault();
		var url = Urls.rollCalculator;
		var isProductSet = ($("#isProductSet").val() == "true") ? true : false;
		
		if(isProductSet){
			if($(this).closest("form").find("#salesprice").length > 0){
				var price = $(this).closest("form").find("#salesprice").val();
				url = util.appendParamToURL(url, 'price', price);
			}else{
				if($(this).closest("form").find("#standardprice").length > 0){
					var price = $(this).closest("form").find("#standardprice").val();
					url = util.appendParamToURL(url, 'price', price);
				}
			}
			if($(this).closest("form").find("#pid").length > 0){
				var pid = $(this).closest("form").find("#pid").val();
				url = util.appendParamToURL(url, 'pid', pid);
			}
		}else{
			if($("#pdpMain #product-content .product-price #salesprice").length > 0){
				var price = $("#pdpMain #product-content .product-price #salesprice").val();
				url = util.appendParamToURL(url, 'price', price);
			}else{
				if($("#pdpMain #product-content .product-price #standardprice").length > 0){
					var price = $("#pdpMain #product-content .product-price #standardprice").val();
					url = util.appendParamToURL(url, 'price', price);
				}
			}
			if($("#pdpMain #product-content .pdpForm #pid").length > 0){
				var pid = $("#pdpMain #product-content .pdpForm #pid").val();
				url = util.appendParamToURL(url, 'pid', pid);
			}
		}
		
		url = util.appendParamToURL(url, 'format', 'ajax');
		dialog.open({
			url: url,
			options: {
				height: 540,
				dialogClass: "roll-Calculator",
			},
			callback: function () {
				calculator.calculate();
				calculator.addSavedProductsToCart();
				calculator.removeRoom();
				roll_calculator();
				calculator.validate();
			}
		});
	});
	$pdpMain.on('click', '.social-links .share-icon', function (e) {
		var isProductSet = ($("#isProductSet").val() == "true") ? true : false;
		if(isProductSet){
			var socialName = $(this).attr("data-share");
			var prodId = $pdpMain.find(".product-add-to-cart input[name='pid']").val();
			var prodName = $pdpMain.find(".product-add-to-cart input[name='name']").val();
		}else{
			var socialName = $(this).attr("data-share");
			var prodId = $pdpMain.find("input[name='pid']").val();
			var prodName = $pdpMain.find("input[name='name']").val();
		}
		redeye.socialShare(socialName,prodId);
		gtmevents.productShare(prodId,prodName,socialName);
	});
	
	//colour card request
	$('#colourcardrequest').click(function(e) {
		e.preventDefault();
		var url = Urls.colourcard;
		dialog.open({
			url: url,
			options: {
			dialogClass : 'colorcard-popup',
			width : 510,
			open: function () {
			validator.init();
			$('#request-for-paintcard').click(function(e) {
					e.preventDefault(); 
					var $form = $(this).closest('form');
					if (!$form.valid()) {
						return false;
					}
					var url = $form.attr('action');
					   var name = $("#dwfrm_paintcard_customer_name").val();
					   var email = $("#dwfrm_paintcard_customer_email").val();
					   var address1 = $("#dwfrm_paintcard_customer_address1").val();
					   var address2 = $("#dwfrm_paintcard_customer_address2").val();
					   var city = $("#dwfrm_paintcard_customer_city").val();
					   var state = $("#dwfrm_paintcard_customer_state").val();
					   var postal = $("#dwfrm_paintcard_customer_postal").val();
					   var subscribe = $("#dwfrm_paintcard_customer_subscribe").val();
					   url = util.appendParamToURL(url, 'name', name);
					   url = util.appendParamToURL(url, 'email', email);
					   url = util.appendParamToURL(url, 'address1', address1);
					   url = util.appendParamToURL(url, 'address2', address2);
					   url = util.appendParamToURL(url, 'city', city);
					   url = util.appendParamToURL(url, 'state', state);
					   url = util.appendParamToURL(url, 'postal', postal);
					   url = util.appendParamToURL(url, 'subscribe', subscribe);
					   var action = $form.find('#request-for-paintcard').attr('name');
						url = util.appendParamToURL(url, action, "x");
						url = util.appendParamToURL(url, 'format', 'ajax');
					   $.ajax({
						   type: 'GET',
						   dataType: 'json',
						   url:url,
							success: function (response) {
								dialog.close();
								redeye.colourCard('colourcard',$form);
								var data = "<div class='paintcard'>"+ Resources.COLOURCARDCONFIRMATION +"</div>";
								dialog.open({
									html:data,
									options: {
										dialogClass : 'paintcard custom-popup',
										width : 390
									},
									callback: function () {
									}
								});
						    },
						  });
			});
			}
			  }
		});
	});
	$(".fa-pinterest-p").click(function(e) {
        e.preventDefault();
        var media = $(".product-primary-image div.slick-current a").find("img").attr("src");
        var href = $(this).attr('href');
        href = util.appendParamToURL(href, 'media', media);
        window.open(href,'_blank');
	});
	$(".fa-facebook").click(function(e) {
        e.preventDefault();
        var picture = $(".product-primary-image div.slick-current a").find("img").attr("src");
        var href = $(this).attr('href');
        href = util.appendParamToURL(href, 'picture', picture);
        window.open(href,'_blank');
	});

};

//Paint or Roll Calculator
var calculator = {
	calculate : function () {
		if($(".roll-Calculator").length > 0){
			$(".roll-Calculator").off('click', '.calculator-form .calculate');
			$(".roll-Calculator").on('click', '.calculator-form .calculate', function (e) {
				e.preventDefault();
				var $form = $("#calculator-form");
				if (!$form.valid()) {
					return false;
				}
				var url = $form.attr('action');
				if($(".roll-Calculator #calculator-form input[id$=height]").length > 0){
					var height = $(".roll-Calculator #calculator-form input[id$=height]").val();
					url = util.appendParamToURL(url, 'height', height);
				}
				if($(".roll-Calculator #calculator-form input[id$=width]").length > 0){
					var width = $(".roll-Calculator #calculator-form input[id$=width]").val();
					url = util.appendParamToURL(url, 'width', width);
				}
				var isProductSet = ($("#isProductSet").val() == "true") ? true : false;
				
				if(isProductSet){
					if($(this).closest("form").find("#salesprice").length > 0){
						var price = $(this).closest("form").find("#salesprice").val();
						url = util.appendParamToURL(url, 'price', price);
					}else{
						if($(this).closest("form").find("#standardprice").length > 0){
							var price = $(this).closest("form").find("#standardprice").val();
							url = util.appendParamToURL(url, 'price', price);
						}
					}
					if($(this).closest("form").find("#pid").length > 0){
						var pid = $(this).closest("form").find("#pid").val();
						url = util.appendParamToURL(url, 'pid', pid);
					}
				}else{
					if($("#pdpMain #product-content .product-price #salesprice").length > 0){
						var price = $("#pdpMain #product-content .product-price #salesprice").val();
						url = util.appendParamToURL(url, 'price', price);
					}else{
						if($("#pdpMain #product-content .product-price #standardprice").length > 0){
							var price = $("#pdpMain #product-content .product-price #standardprice").val();
							url = util.appendParamToURL(url, 'price', price);
						}
					}
					if($("#pdpMain #product-content .pdpForm #pid").length > 0){
						var pid = $("#pdpMain #product-content .pdpForm #pid").val();
						url = util.appendParamToURL(url, 'pid', pid);
					}
				}
				
				var calculate = $form.find('.calculate').attr('name');
				url = util.appendParamToURL(url, calculate, "x");
				url = util.appendParamToURL(url, 'format', 'ajax');
				$.ajax({
					type: 'GET',
					dataType: 'html',
					url: url,
					success: function (response) {
						$('#calculator').html(response);
						calculator.saveDimensions();
						calculator.addToCart();
						calculator.addSavedProductsToCart();
						calculator.removeRoom();
						roll_calculator();
						calculator.validate();
					}
				});
			});
		}
	},
	saveDimensions : function () {
		if($(".roll-Calculator .calculator-form .save").length > 0){
			$(".roll-Calculator").off('click', '.calculator-form .save');
			$(".roll-Calculator").on('click', '.calculator-form .save', function (e) {
				e.preventDefault();
				var $form = $("#calculator-form");
				if (!$form.valid()) {
					return false;
				}
				var url = $form.attr('action');
				if($(".roll-Calculator #calculator-form #height").length > 0){
					var height = $(".roll-Calculator #calculator-form #height").val();
					url = util.appendParamToURL(url, 'height', height);
				}
				if($(".roll-Calculator #calculator-form #width").length > 0){
					var width = $(".roll-Calculator #calculator-form #width").val();
					url = util.appendParamToURL(url, 'width', width);
				}
				var isProductSet = ($("#isProductSet").val() == "true") ? true : false;
				
				if(isProductSet){
					if($(this).closest("form").find("#salesprice").length > 0){
						var price = $(this).closest("form").find("#salesprice").val();
						url = util.appendParamToURL(url, 'price', price);
					}else{
						if($(this).closest("form").find("#standardprice").length > 0){
							var price = $(this).closest("form").find("#standardprice").val();
							url = util.appendParamToURL(url, 'price', price);
						}
					}
					if($(this).closest("form").find("#pid").length > 0){
						var pid = $(this).closest("form").find("#pid").val();
						url = util.appendParamToURL(url, 'pid', pid);
					}
				}else{
					if($("#pdpMain #product-content .product-price #salesprice").length > 0){
						var price = $("#pdpMain #product-content .product-price #salesprice").val();
						url = util.appendParamToURL(url, 'price', price);
					}else{
						if($("#pdpMain #product-content .product-price #standardprice").length > 0){
							var price = $("#pdpMain #product-content .product-price #standardprice").val();
							url = util.appendParamToURL(url, 'price', price);
						}
					}
					if($("#pdpMain #product-content .pdpForm #pid").length > 0){
						var pid = $("#pdpMain #product-content .pdpForm #pid").val();
						url = util.appendParamToURL(url, 'pid', pid);
					}
				}
				var save = $form.find('.save').attr('name');
				url = util.appendParamToURL(url, save, "x");
				url = util.appendParamToURL(url, 'format', 'ajax');
				$.ajax({
					type: 'GET',
					dataType: 'html',
					url: url,
					success: function (response) {
						$('#calculator').html(response);
						calculator.saveRoom();
						calculator.addToCart();
						calculator.addSavedProductsToCart();
						calculator.removeRoom();
						roll_calculator();
						calculator.validate();
					}
				});
			});
		}
	},
	addToCart : function () {
		if($(".roll-Calculator .calculator-form .addtocart").length > 0){
			$(".roll-Calculator").off('click', '.calculator-form .addtocart');
			$(".roll-Calculator").on('click', '.calculator-form .addtocart', function (e) {
				e.preventDefault();
				var $form = $("#calculator-form");
				if (!$form.valid()) {
					return false;
				}
				var url = Urls.addProduct;
				if($(".roll-Calculator .calculator-form #count").length > 0){
					var quantity = $(".roll-Calculator .calculator-form #count").val();
					url = util.appendParamToURL(url, 'Quantity', quantity);
				}
				var isProductSet = ($("#isProductSet").val() == "true") ? true : false;
				
				if(isProductSet){
					if($(this).closest("form").find("#pid").length > 0){
						var pid = $(this).closest("form").find("#pid").val();
						url = util.appendParamToURL(url, 'pid', pid);
					}
				}else{
					if($("#pdpMain #product-content .pdpForm #pid").length > 0){
						var pid = $("#pdpMain #product-content .pdpForm #pid").val();
						url = util.appendParamToURL(url, 'pid', pid);
					}
				}
				var url = util.appendParamToURL(url, 'format', 'ajax');
				$.ajax({
					type: 'GET',
					dataType: 'html',
					url: url,
					success: function (response) {
						minicart.show(response);
						if($(".roll-Calculator .calculator-form .addtocart").length > 0){
							$(".roll-Calculator .calculator-form .addtocart").hide();
						}
						calculator.removeRoom();
						roll_calculator();
						calculator.validate();
					},
					failure: function () {
						window.alert(Resources.SERVER_ERROR);
					}
				});
			});
		}
	},
	saveRoom : function () {
		if($(".roll-Calculator .calculator-form .saveroom").length > 0){
			$(".roll-Calculator").off('click', '.calculator-form .saveroom');
			$(".roll-Calculator").on('click', '.calculator-form .saveroom', function (e) {
				e.preventDefault();
				var $form = $("#calculator-form");
				if (!$form.valid()) {
					return false;
				}
				var url = $form.attr('action');
				if($(".roll-Calculator #calculator-form #height").length > 0){
					var height = $(".roll-Calculator #calculator-form #height").val();
					url = util.appendParamToURL(url, 'height', height);
				}
				if($(".roll-Calculator #calculator-form #width").length > 0){
					var width = $(".roll-Calculator #calculator-form #width").val();
					url = util.appendParamToURL(url, 'width', width);
				}
				var isProductSet = ($("#isProductSet").val() == "true") ? true : false;
				
				if(isProductSet){
					if($(this).closest("form").find("#salesprice").length > 0){
						var price = $(this).closest("form").find("#salesprice").val();
						url = util.appendParamToURL(url, 'price', price);
					}else{
						if($(this).closest("form").find("#standardprice").length > 0){
							var price = $(this).closest("form").find("#standardprice").val();
							url = util.appendParamToURL(url, 'price', price);
						}
					}
					if($(this).closest("form").find("#pid").length > 0){
						var pid = $(this).closest("form").find("#pid").val();
						url = util.appendParamToURL(url, 'pid', pid);
					}
				}else{
					if($("#pdpMain #product-content .product-price #salesprice").length > 0){
						var price = $("#pdpMain #product-content .product-price #salesprice").val();
						url = util.appendParamToURL(url, 'price', price);
					}else{
						if($("#pdpMain #product-content .product-price #standardprice").length > 0){
							var price = $("#pdpMain #product-content .product-price #standardprice").val();
							url = util.appendParamToURL(url, 'price', price);
						}
					}
					if($("#pdpMain #product-content .pdpForm #pid").length > 0){
						var pid = $("#pdpMain #product-content .pdpForm #pid").val();
						url = util.appendParamToURL(url, 'pid', pid);
					}
				}
				if($(".roll-Calculator #calculator-form #dwfrm_calculator_roomname").length > 0){
					var name = $(".roll-Calculator #calculator-form #dwfrm_calculator_roomname").val();
					url = util.appendParamToURL(url, 'name', name);
				}
				
				var saveroom = $form.find('.saveroom').attr('name');
				url = util.appendParamToURL(url, saveroom, "x");
				url = util.appendParamToURL(url, 'format', 'ajax');
				$.ajax({
					type: 'GET',
					dataType: 'html',
					url: url,
					success: function (response) {
						$('#calculator').html(response);
						calculator.calculate();
						calculator.addToCart();
						calculator.addSavedProductsToCart();
						calculator.removeRoom();
						roll_calculator();
						calculator.validate();
					}
				});
			});
		}
	},
	addSavedProductsToCart : function(){
		if($(".roll-Calculator #calculator:visible .savedrooms .parent_room .add-to-cart").length > 0){
			$(".roll-Calculator").off('click', '#calculator:visible .savedrooms .parent_room .add-to-cart');
			$(".roll-Calculator").on('click', '#calculator:visible .savedrooms .parent_room .add-to-cart', function (e) {
				e.preventDefault();
				var url = Urls.addProduct;
				if($(this).closest(".parent_room").find("#count").length > 0){
					var quantity = $(this).closest(".parent_room").find("#count").val();
					url = util.appendParamToURL(url, 'Quantity', quantity);
				}
				var isProductSet = ($("#isProductSet").val() == "true") ? true : false;
				
				if(isProductSet){
					if($(this).closest("form").find("#pid").length > 0){
						var pid = $(this).closest("form").find("#pid").val();
						url = util.appendParamToURL(url, 'pid', pid);
					}
				}else{
					if($("#pdpMain #product-content .pdpForm #pid").length > 0){
						var pid = $("#pdpMain #product-content .pdpForm #pid").val();
						url = util.appendParamToURL(url, 'pid', pid);
					}
				}
				var url = util.appendParamToURL(url, 'format', 'ajax');
				$.ajax({
					type: 'GET',
					dataType: 'html',
					url: url,
					success: function (response) {
						minicart.show(response);
						if($(".roll-Calculator .calculator-form .addtocart").length > 0){
							$(".roll-Calculator .calculator-form .addtocart").hide();
						}
						calculator.removeRoom();
						roll_calculator();
						calculator.validate();
					},
					failure: function () {
						window.alert(Resources.SERVER_ERROR);
					}
				});
			});
		}
	},
	removeRoom : function () {
		if($(".roll-Calculator #calculator:visible .savedrooms .remove").length > 0){
			$(".roll-Calculator").off('click', '#calculator:visible .savedrooms .remove');
			$(".roll-Calculator").on('click', '#calculator:visible .savedrooms .remove', function (e) {
				e.preventDefault();
				var $form = $("#calculator-form");
				var url = $form.attr('action');
				var isProductSet = ($("#isProductSet").val() == "true") ? true : false;
				
				if(isProductSet){
					if($(this).closest("form").find("#salesprice").length > 0){
						var price = $(this).closest("form").find("#salesprice").val();
						url = util.appendParamToURL(url, 'price', price);
					}else{
						if($(this).closest("form").find("#standardprice").length > 0){
							var price = $(this).closest("form").find("#standardprice").val();
							url = util.appendParamToURL(url, 'price', price);
						}
					}
					if($(this).closest("form").find("#pid").length > 0){
						var pid = $(this).closest("form").find("#pid").val();
						url = util.appendParamToURL(url, 'pid', pid);
					}
				}else{
					if($("#pdpMain #product-content .product-price #salesprice").length > 0){
						var price = $("#pdpMain #product-content .product-price #salesprice").val();
						url = util.appendParamToURL(url, 'price', price);
					}else{
						if($("#pdpMain #product-content .product-price #standardprice").length > 0){
							var price = $("#pdpMain #product-content .product-price #standardprice").val();
							url = util.appendParamToURL(url, 'price', price);
						}
					}
					if($("#pdpMain #product-content .pdpForm #pid").length > 0){
						var pid = $("#pdpMain #product-content .pdpForm #pid").val();
						url = util.appendParamToURL(url, 'pid', pid);
					}
				}
				var removeRoom = $('#calculator:visible .savedrooms .remove').attr('name');
				url = util.appendParamToURL(url, removeRoom, "x");
				var index = $(this).attr('index');
				url = util.appendParamToURL(url, "index", index-1);
				url = util.appendParamToURL(url, 'format', 'ajax');
				$.ajax({
					type: 'GET',
					dataType: 'html',
					url: url,
					success: function (response) {
						$('#calculator .savedrooms').empty();
						$('#calculator .savedrooms').replaceWith(response);
						calculator.saveRoom();
						calculator.addToCart();
						calculator.addSavedProductsToCart();
						calculator.calculate();
						roll_calculator();
						calculator.validate();
					},
					failure: function () {
						window.alert(Resources.SERVER_ERROR);
					}
				});
			});
		}
	},
	validate : function () {
		$(".roll-Calculator #calculator-form input[id$=height],.roll-Calculator #calculator-form input[id$=width]").keyup(function(e) {
			e.preventDefault();
			var  text = $(this).val();
		    var regex=/[^\d.]/g;       
		    if(regex.test(text)){
		           $(this).val(text.replace(regex, ''));
		           return false;
		    }else{
		    	return true;
			}
		});
	}
};
 function addSampleToCart(pid){
	var url = Urls.addProduct;
	var pid = pid;
	url = util.appendParamToURL(url, 'pid', pid);
	url = util.appendParamToURL(url, 'format', 'ajax');
	$.ajax({
		type: 'GET',
		dataType: 'html',
		url: url,
		success: function (response) {
			minicart.show(response);
			if($(".pdp-main .pdpForm .orderasample").length > 0){
				$(".pdp-main .pdpForm .orderasample").addClass("added");
			}
			if($(".pdp-main .pdpForm .orderasample #input-select").length > 0){
				$(".pdp-main .pdpForm .orderasample #input-select").val('');
				//custom selectbox
				$('.pdp-main .pdpForm .orderasample #input-select').data('selectBox-selectBoxIt').destroy();
                $('.pdp-main .pdpForm .orderasample #input-select').selectBoxIt();
			}
		},
		failure: function () {
			window.alert(Resources.SERVER_ERROR);
		}
	});
}

 //custom-scroll
 function roll_calculator() {
	 if ($('.calculator .enscroll-track').length < 1 ) {
		 $('.calculator .savedrooms').enscroll({
			verticalTrackClass: 'track1',
			verticalHandleClass: 'handle1',
			drawScrollButtons: true,
			addPaddingToPane: false,
			scrollUpButtonClass: 'scroll-up1',
			scrollDownButtonClass: 'scroll-down1'
		});
	 }
	 else {
		 $('.calculator .enscroll-track').parent().remove();
		 $('.calculator .savedrooms').enscroll({
			verticalTrackClass: 'track1',
			verticalHandleClass: 'handle1',
			drawScrollButtons: true,
			addPaddingToPane: false,
			scrollUpButtonClass: 'scroll-up1',
			scrollDownButtonClass: 'scroll-down1'
		});
	 }
	 
	 // fix for IE 
	 if ($('.savedrooms .parent_room').length < 4 ) {
		 $('.calculator .enscroll-track').parent().remove();
		 $('.calculator .savedrooms').css('padding-right', '0');
	 }
	 
	 if ($('.calculator .enscroll-track').parent().is(':visible')) {
		 $('.calculator .savedrooms').css('padding-right', '35px');
	 }
	 else {
		 $('.calculator .savedrooms').css('padding-right', '0');
	 }
}
 
function checkQuantity(form) {
	var checkQuantityUrl = Urls.checkQuantity;
	checkQuantityUrl = util.appendParamToURL(checkQuantityUrl, 'pid', form.find("input[name='pid']").val());
	checkQuantityUrl = util.appendParamToURL(checkQuantityUrl, 'Quantity', form.find("input[name='Quantity']").val());
	return TPromise.resolve($.ajax({
		type: 'POST',
		async: false,//JIRA PREV-305:SG issue: No of items in cart value not displayed as expected.Added async:false.
		url: checkQuantityUrl
	}));
}

//HPWallArt pop-up box functionality
$('.button-fancy-large.customise-your-wall.prm-btn').on('click',function(e){
	e.preventDefault();
	var url = Urls.HPWallArt;
	url = util.appendParamToURL(url, 'format', 'ajax');
	dialog.open({
        url: url,
        options: {
            width: 487,
            dialogClass : 'hpwallart-popup'
        },
        callback: function() {
            util.selectbox();
            hpwallart.caluclatePrice();
            hpwallart.launchWindow();
            hpwallart.validations();
        }
    });
});		

$('.get-the-look-content .get-look-tile .getthelook-customise').on('click', function(e){
	e.preventDefault();
	var productid = $(this).closest('.get-look-tile').find("input[name=pid]").val();
	var url = Urls.getProductUrl;
	url = util.appendParamToURL(url, 'pid', productid);
	window.location.href = url;
});

var hpwallart = {
		caluclatePrice : function(){
			$('#dwfrm_hpwallart_Price').attr('disabled',true);
            $('#CalculatePriceForHP').on('click',function(e){
            	e.preventDefault();
            	var $form = $("#HPWallArtFormOne");
            	var url = $form.attr('action');
            	if($('#HPWallArtFormOne #dwfrm_hpwallart_Width').length > 0){
            		var width = $('#HPWallArtFormOne #dwfrm_hpwallart_Width').val();
					url = util.appendParamToURL(url, 'width', width);
            	}
            	
            	if($('#HPWallArtFormOne #dwfrm_hpwallart_Hight').length > 0){
					var hight = $('#HPWallArtFormOne #dwfrm_hpwallart_Hight').val();
					url = util.appendParamToURL(url, 'hight', hight);
				}
            	
				if($("#pdpMain #product-content .product-price #salesprice").length > 0){
					var price = $("#pdpMain #product-content .product-price #salesprice").val();
					url = util.appendParamToURL(url, 'price', price);
				}else{
					if($("#pdpMain #product-content .product-price #standardprice").length > 0){
						var price = $("#pdpMain #product-content .product-price #standardprice").val();
						url = util.appendParamToURL(url, 'price', price);
					}
				}
				if($("#pdpMain #product-content .pdpForm #pid").length > 0){
					var pid = $("#pdpMain #product-content .pdpForm #pid").val();
					url = util.appendParamToURL(url, 'pid', pid);
				}
				
				var calculate = $form.find('#CalculatePriceForHP').attr('name');
				
				url = util.appendParamToURL(url, calculate, "x");
				url = util.appendParamToURL(url, 'format', 'ajax');
				var hpprodprice = $('input[id=salesprice]').val();
				
				$.ajax({
					url: url,
					success: function (response) {
						//Converting area from Sq Cms to Sq Mts
						var area = (hight*width) * 0.0001;
						var totalprice = (area * hpprodprice).toFixed(2);
						$('#dwfrm_hpwallart_Price').val(totalprice);
					}
				});
				
            	});
		},
		
		launchWindow : function(){
			$('#launchWindowForHP').on('click',function(e){
            	e.preventDefault();
            	var hpPID = $("#pdpMain #product-content .pdpForm #pid").val();
            	if(util.GetCookie("HPProductID") == ""){
            		util.SetCookie("HPProductID", hpPID , 1);
            	}else{
            		var hpcookieval = util.GetCookie("HPProductID");
            		if(hpPID != hpcookieval){
            			//Delete existing cookie and create new cookie if PDP product-ID & cookie value(productID) doesn't match 
            			util.DeleteCookie("HPProductID");
            			util.SetCookie("HPProductID", hpPID , 1);
            		}
            	}
                var url = "https://designer.hpwallart.com/grahambrown?web_link=true&sku=WA_WC";
            	
            	if($('input[id=customerno]').val() != undefined && $('input[id=customerno]').val() != 'null'){
            		var customernumber = $('input[id=customerno]').val();
            		customernumber = "GB"+customernumber;
            		url = util.appendParamToURL(url, 'auth_token',customernumber);
            		url = util.appendParamToURL(url, 'user_logged_in','true');
            	}else{
            		url = util.appendParamToURL(url, 'user_logged_in','false');
            	}
         	
            	if($('#HPWallArtFormOne #dwfrm_hpwallart_Hight').length > 0){
					var height = $('#HPWallArtFormOne #dwfrm_hpwallart_Hight').val();
				}
            	if($('#HPWallArtFormOne #dwfrm_hpwallart_Width').length > 0){
            		var width = $('#HPWallArtFormOne #dwfrm_hpwallart_Width').val();
            	}
            	if(height != undefined && width != undefined){
            		url = util.appendParamToURL(url, 'size',width+"x"+height);
            	}
            	
            	url = util.appendParamToURL(url, 'content_context_token', hpPID);
            	
            	window.location = url;
            	
            });
		},
		
		validations : function(){
			$("#dwfrm_hpwallart_Width,#dwfrm_hpwallart_Hight").keyup(function(e) {
				e.preventDefault();
				var  text = $(this).val();
			    var regex=/[^\d.]/g;       
			    if(regex.test(text)){
			           $(this).val(text.replace(regex, ''));
			           return false;
			    }else{
			    	return true;
				}
			});
		}
};
},{"../../ajax":1,"../../bisn":3,"../../dialog":10,"../../gtmevents":14,"../../minicart":16,"../../progress":40,"../../redeyetracking":43,"../../storeinventory/product":50,"../../tooltip":51,"../../util":52,"../../validator":53,"./addToCart":27,"./availability":28,"./image":29,"promise":59}],34:[function(require,module,exports){
'use strict';

var addProductToCart = require('./product/addToCart'),
	ajax = require('../ajax'),
	quickview = require('../quickview'),
	account = require('./account'),
	util = require('../util');

/**
 * @function
 * @description Loads address details to a given address and fills the address form
 * @param {String} addressID The ID of the address to which data will be loaded
 */
function populateForm(addressID, $form) {
	// load address details
	var url = Urls.giftRegAdd + addressID;
	ajax.getJson({
		url: url,
		callback: function (data) {
			if (!data || !data.address) {
				window.alert(Resources.REG_ADDR_ERROR);
				return false;
			}
			// fill the form
			$form.find('[name$="_addressid"]').val(data.address.ID);
			$form.find('[name$="_firstname"]').val(data.address.firstName);
			$form.find('[name$="_lastname"]').val(data.address.lastName);
			$form.find('[name$="_address1"]').val(data.address.address1);
			$form.find('[name$="_address2"]').val(data.address.address2);
			$form.find('[name$="_city"]').val(data.address.city);
			$form.find('[name$="_country"]').val(data.address.countryCode).trigger('change');
			$form.find('[name$="_postal"]').val(data.address.postalCode);
			$form.find('[name$="_state"]').val(data.address.stateCode);
			$form.find('[name$="_phone"]').val(data.address.phone);
			// $form.parent('form').validate().form();
		}
	});
}

/**
 * @private
 * @function
 * @description Initializes events for the gift registration
 */
function initializeEvents() {
	var $eventAddressForm = $('form[name$="_giftregistry"]'),
		$beforeAddress = $eventAddressForm.find('fieldset[name="address-before"]'),
		$afterAddress = $eventAddressForm.find('fieldset[name="address-after"]');

	$('.usepreevent').on('click', function () {
		// filter out storefront toolkit
		$(':input', $beforeAddress).not('[id^="ext"]').not('select[name$="_addressBeforeList"]').each(function () {
			var fieldName = $(this).attr('name'),
				$afterField = $afterAddress.find('[name="' + fieldName.replace('Before', 'After') + '"]');
			$afterField.val($(this).val()).trigger('change');
		});
	});
	$eventAddressForm.on('change', 'select[name$="_addressBeforeList"]', function () {
		var addressID = $(this).val();
		if (addressID.length === 0) { return; }
		populateForm(addressID, $beforeAddress);
	})
	.on('change', 'select[name$="_addressAfterList"]', function () {
		var addressID = $(this).val();
		if (addressID.length === 0) { return; }
		populateForm(addressID, $afterAddress);
	});

	$('form[name$="_giftregistry_items"]').on('click', '.item-details a', function (e) {
		e.preventDefault();
		var productListID = $('input[name=productListID]').val();
		quickview.show({
			url: e.target.href,
			source: 'giftregistry',
			productlistid: productListID
		});
	});
}

exports.init = function () {
	initializeEvents();
	addProductToCart();
	//Start JIRA PREV-333 : On click of Forgot Password link navigates to the page instead of overlay.
	account.initCartLogin();
	util.setDeleteConfirmation('.item-list', String.format(Resources.CONFIRM_DELETE, Resources.TITLE_GIFTREGISTRY));
};

},{"../ajax":1,"../quickview":41,"../util":52,"./account":18,"./product/addToCart":27}],35:[function(require,module,exports){
'use strict';

var compareWidget = require('../compare-widget'),
    productTile = require('../product-tile'),
    progress = require('../progress'),
    validator = require('../validator'),
    dialog = require('../dialog'),
    formPrepare = require('../pages/checkout/formPrepare'),
    util = require('../util');

function infiniteScroll() {
    // getting the hidden div, which is the placeholder for the next page
    var loadingPlaceHolder = $('.infinite-scroll-placeholder[data-loading-state="unloaded"]');
    // get url hidden in DOM
    var gridUrl = loadingPlaceHolder.attr('data-grid-url');

    if (loadingPlaceHolder.length === 1 && util.elementInViewport(loadingPlaceHolder.get(0), 250)) {
        // switch state to 'loading'
        // - switches state, so the above selector is only matching once
        // - shows loading indicator
        loadingPlaceHolder.attr('data-loading-state', 'loading');
        loadingPlaceHolder.addClass('infinite-scroll-loading');

        /**
         * named wrapper function, which can either be called, if cache is hit, or ajax repsonse is received
         */
        var fillEndlessScrollChunk = function(html) {
            loadingPlaceHolder.removeClass('infinite-scroll-loading');
            loadingPlaceHolder.attr('data-loading-state', 'loaded');
            $('div.search-result-content').append(html);
        };

        // old condition for caching was `'sessionStorage' in window && sessionStorage["scroll-cache_" + gridUrl]`
        // it was removed to temporarily address RAP-2649
        if (false) {
            // if we hit the cache
            fillEndlessScrollChunk(sessionStorage['scroll-cache_' + gridUrl]);
        } else {
            // else do query via ajax
            $.ajax({
                type: 'GET',
                dataType: 'html',
                url: gridUrl,
                success: function(response) {
                    // put response into cache
                    try {
                        sessionStorage['scroll-cache_' + gridUrl] = response;
                    } catch (e) {
                        // nothing to catch in case of out of memory of session storage
                        // it will fall back to load via ajax
                    }
                    // update UI
                    fillEndlessScrollChunk(response);
                    productTile.init();
                }
            });
        }
    }
}
/**
 * @private
 * @function
 * @description replaces breadcrumbs, lefthand nav and product listing with ajax and puts a loading indicator over the product listing
 */
function updateProductListing() {
    var hash = location.href.split('#')[1];
    if (hash === 'results-content' || hash === 'results-products') {
        return;
    }
    var refineUrl;

    if (hash.length > 0) {
        refineUrl = window.location.pathname + '?' + hash;
    } else {
        return;
    }
    progress.show($('.search-result-content'));
    $('#main').load(util.appendParamToURL(refineUrl, 'format', 'ajax'), function() {
        compareWidget.init();
        productTile.init();
        util.selectbox();
        refineBy();
        util.fancyScroll();
        refinementload();
        //code for mobile view in case of refinments showing
        var reminementExpand = false;
        $("#secondary").find("div.refinement").each(function() {
            var refinement = $(this);
            if ($(this).find("ul").length > 0 && $(this).find("ul li.selected").length > 0) {
                $(this).find("ul").css("display", "block");
                reminementExpand = true;
            }
        });
        if (reminementExpand) {
            //$("#secondary").css("display","block");
        }
        //end
        progress.hide();
        $('.last-visited .search-result-items, .featured-products .search-result-items').slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 5,
            slidesToScroll: 1,
            responsive: [{
                breakpoint: 1023,
                settings: {
                    slidesToShow: 3
                }
            }, {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    dots: true
                }
            }]
        });
        $(window).on("load", function() {
            if ($("#wrapper").hasClass("pt_cart")) {
                $('.last-visited .grid-tile').syncHeight();
                $('.featured-products .grid-tile').syncHeight();
            }
        });
        $('html, body').scrollTop(100);
    });
}
//this is for showing and hiding the clearall refinements (GB-571)
$(document).ajaxComplete(function() {
	refinementload();
    if ($('.clear-refinement').length >= 1) {
        $('.clearall').show();
    } else {
        $('.clearall').hide();
    }
    if($(window).width() >= 1024) {
    	$('#secondary .refinement').find('ul').addClass('refinement-hide');
    	$('.refinement.category-refinement').insertBefore('.bottom-apply');
	    $(".new .refinement").each(function(){
	        var $curObj = $(this);
	        if( $curObj.find("> ul li.selected").length > 0 ) {
	          $curObj.find("h3.toggle").trigger("click");
	        }
	    });
    }
});
/**
 * @private
 * @function
 * @description Initializes events for the following elements:<br/>
 * <p>refinement blocks</p>
 * <p>updating grid: refinements, pagination, breadcrumb</p>
 * <p>item click</p>
 * <p>sorting changes</p>
 */
function initializeEvents() {
    var $main = $('#main');
    refinementload();
    // compare checked
    $main.on('click', 'input[type="checkbox"].compare-check', function() {
        var cb = $(this);
        var tile = cb.closest('.product-tile');

        var func = this.checked ? compareWidget.addProduct : compareWidget.removeProduct;
        var itemImg = tile.find('.product-image a img').first();
        func({
            itemid: tile.data('itemid'),
            uuid: tile[0].id,
            img: itemImg,
            cb: cb
        });

    });

    //error page contact-us popup
    $('#error_contact').on('click', function(e) {
        e.preventDefault();
        var url = Urls.errorContactus;
        dialog.open({
            url: url,
            options: {
                dialogClass: 'colorcard-popup',
                width: 510,

            },
            callback: function() {
                util.selectbox();
                validator.init();
                contactUsEvents();
                //For cancel button in contactus page
                $('#cancelBtn').on('click', function(e) {
                    e.preventDefault();
                    var prevurl = $("[name='prevurl']").val();
                    window.location.href = prevurl;

                })

            }
        });

    });
    
    $(document).on('click', '.refine-by-mobile', function() {
    	$(this).toggleClass('active');
    });
    
    // handle toggle refinement blocks
    $main.on('click', '.refinement h3', function() {
        $(this).toggleClass('expanded');
        $(this).parent().siblings('ul').slideToggle();
        $(this).find('span').toggleClass('left-arrow');
    });
    

    //this is for clearall refinements (GB-571)
    $(document).on("click", ".clear-refinement", function(e) {
        e.preventDefault();
        var previousUrl = window.location.href;
        var peaces = previousUrl.split("prefn");
        var removeRef = $(this).closest('.refinement').attr('class').split(" ")[1];
        var clearurl = "";
        for (var peace = 1; peace < peaces.length; peace++) {
            var peaceStr = peaces[peace];
            if (peaceStr.indexOf(removeRef) > -1) {
                clearurl += peaces[peace].substring(2,peaces[peace].length);
            } else {
                clearurl += "prefn" + peaces[peace].substring(0, peaces[peace].length);
            }
        }
        var priceSelected = $(".pricefilter li").hasClass("selected");
        var productType = $('.refinements .ProductGroup li').hasClass('selected');
        var brandSelected = $('.refinements .brand li').hasClass('selected');
        var pattern = $('.refinements .DesignStyle li').hasClass('selected');
        var applicationMethod = $('.refinements .application li').hasClass('selected');
        var matchType = $('.refinements .DesignMatch li').hasClass('selected');
        if (priceSelected) {
            var clearurl = window.location.hash;
            var pmin = $(".pricefilter li.selected a").attr("pmin");
            var pmax = $(".pricefilter li.selected a").attr("pmax");
            clearurl = clearurl.replace("&pmin=" + pmin, "");
            clearurl = clearurl.replace("&pmax=" + pmax, "");
        }

        if (peaces.length == 2) {

            if (priceSelected || (productType != undefined && productType) || (brandSelected != undefined && brandSelected) || (pattern != undefined && pattern) || (applicationMethod != undefined && applicationMethod) || (matchType != undefined && matchType)) {
                window.location.hash = clearurl;
            } else {
                window.location.hash = window.location.search.replace('?', '') + "&refinement=clearall";
            }

        } else {
            window.location.hash = clearurl;
        }

    });

    //this is for clearall refinements (GB-571)
    $(document).on("click", ".clearall", function(e) {
        e.preventDefault();
        window.location.hash = window.location.search.replace('?', '') + "&refinement=clearall";
    });

    function commonRefinement(obj) {

        if ($(obj).parent().hasClass('unselectable')) {
            return;
        }
        var catparent = $(obj).parents('.category-refinement');
        var folderparent = $(obj).parents('.folder-refinement');

        //if the anchor tag is uunderneath a div with the class names & , prevent the double encoding of the url
        //else handle the encoding for the url
        if ((catparent != undefined && catparent.length > 0) || folderparent.length > 0) {
            return true;
        } else {
            var query = util.getQueryString(obj.context.href);
            if (query != undefined && query.length > 1) {
                window.location.hash = query;
            } else {
                window.location.href = obj.context.href;
            }

            //GTM
            var gtmevents = require('../gtmevents');
            gtmevents.categoryRefinement();

            return false;
        }
    }
    $main.on('click', '.apply', function(e) {
        if ($(window).width() < 1024) {
            var allParams = "";
            var allP = "";
            //#prefn1=colourgroup&prefv1=Multiprefn2=brand&prefv2=G&B Kidsprefn3=ProductGroup&prefv3=Children's Roomprefn4=SubBrand&prefv4=ZIZAZOU"
            var searchItems = 0;
            var pricecount = 0;
            var searchDiff = "";
            var qryStr = "&";
            var pmin = 0;
            var pmax = 0;
            var checkedStatus = false;
            var checkitem = '';
            $("li.selected a").each(function(i) {
                var searchItem = $(this).attr('refid0');
                var pricemin = $(this).attr('pmin');
                var pricemax = $(this).attr('pmax');
                checkedStatus = $(this).find("i").hasClass('fa-check-square-o');

                if (searchDiff != searchItem && searchItem != 'price' && searchItem != undefined && checkedStatus) {
                    searchDiff = searchItem;
                    searchItems++;
                    if (checkedStatus) {
                        if (searchItems != 1) {
                            qryStr += "&prefn" + searchItems + "=" + searchItem + "&prefv" + searchItems + "=";
                        } else {
                            qryStr += "prefn" + searchItems + "=" + searchItem + "&prefv" + searchItems + "=";
                        }
                    }
                }
                if (searchItem != 'price') {
                    var searchVal = $(this).attr('refid1');
                    var searchValue = $(this).attr('href');
                    if (searchVal != undefined && checkedStatus) {
                        //var filval = searchValue.substring(searchValue.indexOf("prefv1") + 7, searchValue.length);
                        //searchVal = searchVal.replace(new RegExp(" ", 'g'), "");
                    	if(checkitem != '' && searchItem == checkitem){
                    		searchVal = encodeURIComponent( "|" + searchVal );
                    	}else{
                    		searchVal = encodeURIComponent( searchVal );
                    	}
                        checkitem = searchItem;
                        qryStr += searchVal + ",";
                    }
                }
                qryStr = (qryStr.length - 1) == qryStr.indexOf(',') ? qryStr.substr(0, qryStr.length - 1) : qryStr;
            });

            var priceCheck = false;
            $("li.selected a").each(function(i) {
                var searchItem = $(this).attr('refid0');
                var pricemin = $(this).attr('pmin');
                var pricemax = $(this).attr('pmax');
                checkedStatus = $(this).find("i").hasClass('fa-check-square-o');
                if (searchItem == "price" && checkedStatus) {
                    priceCheck = true;
                    if (pricecount == 0) {
                        pmin = pricemin;
                        pmax = pricemax;
                    } else {
                        pmax = pricemax;
                    }
                    pricecount++;
                }
            });
            if (searchItems == 0) {
                qryStr = "&refinement=clearall&format=ajax";
            }
            if (priceCheck) {
                qryStr += "&pmin=" + pmin + "&pmax=" + pmax;
            } else {
                qryStr += "";
            }
            window.location.hash = window.location.search.replace('?', '') + qryStr;
            //GTM
            var gtmevents = require('../gtmevents');
            gtmevents.categoryRefinement();
        } else {
            commonRefinement($(this));
        }
	    
    });

    $main.on('click', 'div.refinements a', function(e) {    	
		e.preventDefault();
		if ($(this).hasClass('redirect-link')) {
			window.location = $(this).attr('href');
		} else {

	        if ($(window).width() < 1024) {
	            if ($(this).closest('ul').hasClass('pricefilter') && !$(this).closest('li').hasClass('selected')) {
	                $(this).closest('ul').find('li').removeClass("selected");
	                $(this).closest('ul').find('li i').removeClass('fa-check-square-o');
	                $(this).closest('ul').find('li i').addClass('fa-square-o');
	            }
	            $(this).closest('li').addClass("selected");
	            $(this).find('i').toggleClass('fa-check-square-o');
	            $(this).find('i').toggleClass('fa-square-o');
	        } else {
	            commonRefinement($(this));
	        }
		}
    });

    $main.on('click', '.pagination a, .breadcrumb-refinement-value a', function(e) {
        if ($(window).width() < 1024) {
            if ($(this).parent().hasClass('unselectable')) {
                return;
            }
            var catparent = $(this).parents('.category-refinement');
            var folderparent = $(this).parents('.folder-refinement');

            //if the anchor tag is uunderneath a div with the class names & , prevent the double encoding of the url
            //else handle the encoding for the url
            if ((catparent != undefined && catparent.length > 0) || folderparent.length > 0) {
                return true;
            } else {
                var query = util.getQueryString(this.href);
                if (query != undefined && query.length > 1) {
                    window.location.hash = query;
                } else {
                    window.location.href = this.href;
                }

                //GTM
                var gtmevents = require('../gtmevents');
                gtmevents.categoryRefinement();

                return false;
            }
        } else {
            commonRefinement($(this));
        }
    });


    // handle events item click. append params.
    $main.on('click', '.product-tile a:not("#quickviewbutton")', function() {
        var a = $(this);
        // get current page refinement values
        var wl = window.location;

        var qsParams = (wl.search.length > 1) ? util.getQueryStringParams(wl.search.substr(1)) : {};
        var hashParams = (wl.hash.length > 1) ? util.getQueryStringParams(wl.hash.substr(1)) : {};

        // merge hash params with querystring params
        var params = $.extend(hashParams, qsParams);
        if (!params.start) {
            params.start = 0;
        }
        // get the index of the selected item and save as start parameter
        var tile = a.closest('.product-tile');
        var idx = tile.data('idx') ? +tile.data('idx') : 0;

        /*Start JIRA PREV-50 : Next and Previous links will not be displayed on PDP if user navigate from Quick View.Added cgid to hash*/
        if (!params.cgid && tile.data("cgid") != null && tile.data("cgid") != "") {
            params.cgid = tile.data("cgid");
        }
        /*End JIRA PREV-50*/

        // convert params.start to integer and add index
        params.start = (+params.start) + (idx + 1);
        // set the hash and allow normal action to continue
        a[0].hash = $.param(params);
    });

    // handle sorting change
    $main.on('change', '.sort-by select', function() {
            var refineUrl = $(this).find('option:selected').val();
            var queryString = util.getQueryString(refineUrl);

            //GTM
            var gtmevents = require('../gtmevents');
            var selectedvalue = $('select#grid-sort-header option:selected').text();
            gtmevents.categoryPageSort(selectedvalue);

            window.location.hash = queryString;
            return false;
        })
        .on('change', '.items-per-page select', function() {
            var refineUrl = $(this).find('option:selected').val();
            var queryString = util.getQueryString(refineUrl);
            if (refineUrl === 'INFINITE_SCROLL') {
                $('html').addClass('infinite-scroll').removeClass('disable-infinite-scroll');
            } else {
                $('html').addClass('disable-infinite-scroll').removeClass('infinite-scroll');
                window.location.hash = queryString;
            }
            return false;
        });

    // handle hash change
    window.onhashchange = updateProductListing;

    refineBy();

    $(".category-banner-container .expand").click(function() {
        $(this).next().slideToggle();
        $(this).toggleClass("active");
    });
    $(".subcategories-mobile").click(function() {
        $(".subCat-mobile").slideToggle();
    });
    $('#home-recommendations').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 5,
        slidesToScroll: 5,
        responsive: [{
            breakpoint: 1023,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3
            }
        }, {
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true,
                dotsClass: 'custom_paging',
                customPaging: function(slider, i) {
                    return (i + 1) + '/' + slider.slideCount;
                },
            }
        }]
    });
    $(".search-result-bookmarks a#go-article").click(function() {
        $('html,body').animate({
            scrollTop: $(".search-results-content").offset().top - 200
        }, 'slow');
    });
}

//send button in contactus
function contactUsEvents() {
    if ($('#Contactus').length > 0) {
        formPrepare.init({
            continueSelector: '[name$="contactus_send"]',
            formSelector: '[id$="Contactus"]'
        });
    }

}


//refine by toggle for tab & mobile 
function refineBy() {
    $(".refine-by-mobile").click(function() {
        if ($(window).width() < 1024) {
            $("#secondary").slideToggle();
        }
    });

    $(".sort-by-mobile").click(function() {
        if ($(window).width() < 1024) {
            $(".sort-by, .items-per-page").slideToggle();
        }
    });

    $(window).resize(function() {
        if ($(window).width() > 1023) {
            $("#secondary, .sort-by, .items-per-page").show();
        }
    });
}

function refinementload() {
	if($(window).width() >= 1024) {
		$(document).on('click', '#secondary.new .refinement h3', function() {
		  $(this).toggleClass('active');
		  var currentcat = $(this).closest('.refinement').attr('class').split(' ')[1];
		  var addElement = false;
		  if(currentcat==="")
		  {
		    currentcat = $(this).closest('.refinement').find('ul').attr('class').split(' ')[1];
		  }
		  $(this).closest('.refinement').find('ul').addClass(currentcat);
		  var clickedElement = $(this);
		  if($('ul.horizontalSelector').length>0)
		  {
		    if(!clickedElement.hasClass('active'))
		    {
		      $('ul.horizontalSelector.'+currentcat).remove();
		    }else if($('ul.horizontalSelector.'+currentcat).length===0)
		    {
		      clickedElement.closest('.refinement').find('ul').clone().addClass('horizontalSelector').appendTo("#secondary");
		    }
		  }else
		  {
		    clickedElement.closest('.refinement').find('ul').clone().addClass('horizontalSelector').appendTo("#secondary");
		  }
		  $(this).closest('.refinement').find('ul').hide();
		});
	}
}

exports.init = function() {
    compareWidget.init();
    if (SitePreferences.LISTING_INFINITE_SCROLL) {
        $(window).on('scroll', infiniteScroll);
    }
    //changes for GBSS-9
    if(window.location.hash.length > 0){
        updateProductListing();
    }
    productTile.init();
    initializeEvents();
};
},{"../compare-widget":6,"../dialog":10,"../gtmevents":14,"../pages/checkout/formPrepare":22,"../product-tile":39,"../progress":40,"../util":52,"../validator":53}],36:[function(require,module,exports){
'use strict';
var rating = require('../rating');
exports.init = function () {
	$('#home-recommendations').slick({
		  dots: false,
		  infinite: true,
		  speed: 300,
		  slidesToShow: 5,
		  slidesToScroll: 5,
		  responsive: [{
              breakpoint: 1023,
              settings: {
                  slidesToShow: 3,
                  slidesToScroll: 3
              }
          }, {
              breakpoint: 768,
              settings: {
                  slidesToShow: 1,
                  slidesToScroll:1,
                  dots: true,
                  dotsClass: 'custom_paging',
                  customPaging: function (slider, i) {
                      return  (i + 1) + '/' + slider.slideCount;
                  },
              }
          }]
	});
	$('.banner2-slider .banner2-right').slick({
		  dots: false,
		  infinite: true,
		  speed: 300,
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  responsive: [{
              breakpoint: 767,
              settings: {
                  slidesToShow: 1,
                  dots: true,
                  dotsClass: 'custom_paging',
                  customPaging: function (slider, i) {
                      return  (i + 1) + '/' + slider.slideCount;
                  },
              }
          }]
	});
	$('#carousel-recommendations').slick({
		  dots: false,
		  infinite: true,
		  speed: 300,
		  slidesToShow: 2,
		  slidesToScroll: 1,
		  responsive: [{
            breakpoint: 767,
            settings: {
                slidesToShow: 1
            }
        }]
	});
	
	//for homepage first banner
	/*$('.banner1 span.show-only-desktop img').load(function(){
		$('.banner1').css('height',$(window).height() - 201);
		$('.banner1').css('max-height',$('.banner1 span.show-only-desktop img').height());
	});
	if ($('.banner1 span.show-only-desktop img').length > 0) {
		$('.banner1').css('height',$(window).height() - 201);
		$('.banner1').css('max-height',$('.banner1 span.show-only-desktop img').height());
	}
	$(window).resize(function() {
		$('.banner1').css('height',$(window).height() - 201);
		$('.banner1').css('max-height',$('.banner1 span.show-only-desktop img').height());
	});*/
	
	if($(document).find(".grid-tile").length > 0){
		rating.init();
	}
};

},{"../rating":42}],37:[function(require,module,exports){
'use strict';
var dialog = require('../dialog');

exports.init = function () {
	$('.store-details-link').on('click', function (e) {
		e.preventDefault();
		dialog.open({
			url: $(e.target).attr('href')
		});
	});
};

},{"../dialog":10}],38:[function(require,module,exports){
'use strict';

var page = require('../page'),
	account = require('./account'),
	minicart = require('../minicart'),
	gtmevents= require('../gtmevents'),
	util = require('../util');

exports.init = function () {
	//addProductToCart();
	//Start JIRA PREV-412 : SG Issue: Password reset overlay displayed as a page 
	account.initCartLogin();
	$('#editAddress').on('change', function () {
		page.redirect(util.appendParamToURL(Urls.wishlistAddress, 'AddressID', $(this).val()));
	});

	//add js logic to remove the , from the qty feild to pass regex expression on client side
	$('.option-quantity-desired input').on('focusout', function () {
		$(this).val($(this).val().replace(',', ''));
	});
	
	
	$(document).on("click",".option-add-to-cart ",function(e){

        var url = Urls.addWishlistProductToCart;
        var sku = $(this).closest("tr").find(".item-details .product-list-item .sku");
        var plid= $(this).find("input[name='plid']").val();
        var itemid=  $(this).find("input[name='itemid']").val();
        var pid = $(sku).find(".value").text();
        var wishlisturl= $("[name='wishlisturl']").val();
        var $form = $(this).closest('form');
        var Quantity=$(this).closest("form").find(".option-quantity-desired").find(".value").text();
        url = util.appendParamToURL(url, 'plid', plid);
        url = util.appendParamToURL(url, 'pid', pid);
        url = util.appendParamToURL(url, 'itemid', itemid);
        url = util.appendParamToURL(url, 'Quantity', Quantity);
        url = util.appendParamToURL(url, 'format', 'ajax');
        $.ajax({
                type: 'GET',
                dataType: 'html',
                        url: url,
                        success: function (response) {
                        	gtmevents.addToCart($form);
                             minicart.show(response);
                            window.location.href = wishlisturl;
                        },
                        failure: function () {
                                        window.alert(Resources.SERVER_ERROR);
                        }
        });
    })

	
	//For increasing and decreasing the quantity
	$(document).on("click",".option-quantity-desired a",function(e){
        e.preventDefault();
        var curObj = $(this);
        var inputfield = $(this).closest("form").find("input[type='number']");
        var inputValue = $(inputfield).val();
        var $closestQty = $(curObj).closest(".quantity-wishlist");
        if($(curObj).hasClass("prev-value")) {
	        if($(inputfield).val()  > 1) {
                inputValue --;
                $(inputfield).val(inputValue);
	        }
        }
        else if($(curObj).hasClass("next-value")) {
            if($(inputfield).val()  >= 1 && $(inputfield).val() < 99) {
                inputValue ++;
                $(inputfield).val(inputValue);
            }
        }
        $(curObj).closest('.block-add-to-cart').find("input[name='Quantity']").val($(inputfield).val());
        $(curObj).closest('.block-add-to-cart').find("input[name='Quantity']").trigger("change");
	});
};

},{"../gtmevents":14,"../minicart":16,"../page":17,"../util":52,"./account":18}],39:[function(require,module,exports){
'use strict';
var util = require('./util'),
	imagesLoaded = require('imagesloaded'),
	minicart = require('./minicart'),
	rating = require('./rating'); //PREVAIL-Added to handle ratings display in-case of ajax calls.
	
function gridViewToggle() {
	$('.toggle-grid').on('click', function () {
		$('.search-result-content').toggleClass('wide-tiles');
		$(this).toggleClass('wide');
	});
}

/**
 * @private
 * @function
 * @description Initializes events on the product-tile for the following elements:
 * - swatches
 * - thumbnails
 */
function initializeEvents() {
	gridViewToggle();
	rating.init(); //PREVAIL-Added to handle ratings display in-case of ajax calls.
	$('.swatch-list').on('mouseleave', function () {
		// Restore current thumb image
		var $tile = $(this).closest('.product-tile'),
			$thumb = $tile.find('.product-image .thumb-link img').eq(0),
			data = $thumb.data('current');

		$thumb.attr({
			src: data.src,
			alt: data.alt,
			title: data.title
		});
	});
	$('.swatch-list .swatch').on('click', function (e) {
		e.preventDefault();
		if ($(this).hasClass('selected')) { return; }

		var $tile = $(this).closest('.product-tile');
		$(this).closest('.swatch-list').find('.swatch.selected').removeClass('selected');
		$(this).addClass('selected');
		$tile.find('.thumb-link').attr('href', $(this).attr('href'));
		$tile.find('name-link').attr('href', $(this).attr('href'));

		var data = $(this).children('img').filter(':first').data('thumb');
		var $thumb = $tile.find('.product-image .thumb-link img').eq(0);
		var currentAttrs = {
			src: data.src,
			alt: data.alt,
			title: data.title
		};
		$thumb.attr(currentAttrs);
		$thumb.data('current', currentAttrs);
		
		/*Start JIRA PREV-466 : Product images are not updating in the compare section when the color swatches are changed in PLP.*/
		var pid = $(this).closest('.product-tile').attr( "data-itemid" );
		$( ".compare-items-panel .compare-item" ).each(function() {
			var compareid = $(this).attr( "data-itemid" );
			if(pid == compareid){
				var $compare = $(this).find('.compare-item-image').eq(0);
				$compare.attr(currentAttrs);
				$compare.data('current', currentAttrs);
			}
		});
		/*End JIRA PREV-466*/
	}).on('mouseenter', function () {
		// get current thumb details
		var $tile = $(this).closest('.product-tile'),
			$thumb = $tile.find('.product-image .thumb-link img').eq(0),
			data = $(this).children('img').filter(':first').data('thumb'),
			current = $thumb.data('current');

		// If this is the first time, then record the current img
		if (!current) {
			$thumb.data('current', {
				src: $thumb[0].src,
				alt: $thumb[0].alt,
				title: $thumb[0].title
			});
		}

		// Set the tile image to the values provided on the swatch data attributes
		$thumb.attr({
			src: data.src,
			alt: data.alt,
			title: data.title
		});
	});
	
	//save for later functionality
	$(document).on("click",".grid-tile .product-tile .saveforlater button",function(e){
		e.preventDefault();
		var $this = $(this);
		var url = Urls.addToWishlist;
		var pid = $(this).closest(".product-tile").find("input[name=pid]").val();
		url = util.appendParamToURL(url, 'pid', pid);
		url = util.appendParamToURL(url, 'format', 'ajax');
		$.ajax({
			type: 'GET',
			dataType: 'json',
			url: url,
			success: function (response) {
				if(response.success){
					if($this.closest(".product-tile").find(".saveforlater button").length > 0){
						$this.closest(".product-tile").find(".saveforlater button").addClass("added");
					}
				}else{
					if($this.closest(".product-tile").find(".saveforlater .errormsg").length > 0){
						$this.closest(".product-tile").find(".saveforlater .errormsg").show();
						$this.closest(".product-tile").find(".saveforlater .errormsg .close-error-msg").on('click', function(){
							$this.closest(".product-tile").find(".saveforlater .errormsg").hide();
						});
						$(document).mouseup(function (e){
						    var container = $(".saveforlater .errormsg");
						    if (!container.is(e.target) && container.has(e.target).length === 0) {
						        container.hide();
						    }
						});
					}
				}
			}
		});
	});
	//changes made for GB-581  start 
	$(document).on("click",".product-tile .orderasample-inner button",function(e){
		e.preventDefault();
		var pid = $(this).attr("pid");
		if(pid != '' && pid != null){
			addSampleToCart(pid,$(this));
		}
	});
	//order a sample product functionality when 1 or more products r there
	$(document).on("change",".product-tile  .orderasample-inner #input-select",function(e){
		e.preventDefault();
		var pid = $(this).val();
		if(pid != '' && pid != null){
			addSampleToCart(pid,$(this));
		}
	});	
	
}

function addSampleToCart(pid,$this){
	var url = Urls.addProduct;
	var pid = pid;
	url = util.appendParamToURL(url, 'pid', pid);
	url = util.appendParamToURL(url, 'format', 'ajax');
	$.ajax({
		type: 'GET',
		dataType: 'html',
		url: url,
		success: function (response) {
			minicart.show(response);
			if($(".product-tile .orderasample").length > 0){
				$this.closest(".product-tile .orderasample").addClass("added");
			}
			if($this.closest(".product-tile  .orderasample-inner #input-select").length > 0){
				$this.closest(".product-tile  .orderasample-inner #input-select").val('');
				//custom selectbox
				$this.closest('.product-tile  .orderasample-inner #input-select').data('selectBox-selectBoxIt').destroy();
				$this.closest('.product-tile  .orderasample-inner #input-select').selectBoxIt();
			}
		},
		failure: function () {
			window.alert(Resources.SERVER_ERROR);
		}
	});
	//end
}

exports.init = function () {
	var $tiles = $('.tiles-container .product-tile');
	if ($tiles.length === 0) { return; }
	imagesLoaded('.tiles-container').on('done', function () {
		$tiles.syncHeight()
			.each(function (idx) {
				$(this).data('idx', idx);
			});
	});
	initializeEvents();
};

},{"./minicart":16,"./rating":42,"./util":52,"imagesloaded":57}],40:[function(require,module,exports){
'use strict';

var $loader;

/**
 * @function
 * @description Shows an AJAX-loader on top of a given container
 * @param {Element} container The Element on top of which the AJAX-Loader will be shown
 */
var show = function (container) {
	var target = (!container || $(container).length === 0) ? $('body') : $(container);
	$loader = $loader || $('.loader');

	if ($loader.length === 0) {
		$loader = $('<div/>').addClass('loader')
			.append($('<div/>').addClass('loader-indicator'), $('<div/>').addClass('loader-bg'));
	}
	return $loader.appendTo(target).show();
};
/**
 * @function
 * @description Hides an AJAX-loader
 */
var hide = function () {
	if ($loader) {
		$loader.hide();
	}
};

exports.show = show;
exports.hide = hide;

},{}],41:[function(require,module,exports){
'use strict';

var dialog = require('./dialog'),
	product = require('./pages/product'),
	util = require('./util'),
	_ = require('lodash');


var makeUrl = function (url, source, productListID) {
	if (source) {
		url = util.appendParamToURL(url, 'source', source);
	}
	if (productListID) {
		url = util.appendParamToURL(url, 'productlistid', productListID);
	}
	return url;
};

var removeParam = function (url) {
	if (url.indexOf('?') !== -1) {
		return url.substring(0, url.indexOf('?'));
	} else {
		return url;
	}
};

var quickview = {
	init: function () {
		if (!this.exists()) {
			this.$container = $('<div/>').attr('id', 'QuickViewDialog').appendTo(document.body);
		}
		this.productLinks = $('#search-result-items .thumb-link').map(function (index, thumbLink) {
			return $(thumbLink).attr('href');
		});
	},

	setup: function (qvUrl) {
		var $btnNext = $('.quickview-next'),
			$btnPrev = $('.quickview-prev');

		product.initializeEvents();

		this.productLinkIndex = _(this.productLinks).findIndex(function (url) {
			return removeParam(url) === removeParam(qvUrl);
		});

		// hide the buttons on the compare page or when there are no other products
		if (this.productLinks.length <= 1 || $('.compareremovecell').length > 0) {
			$btnNext.hide();
			$btnPrev.hide();
			return;
		}else{
			/*  Start JIRA PREV-50: Next and Previous links will not be displayed on PDP if user navigate from Quick View.
				Added current URL parameters and index to viewfulldetails link
			*/				
			var a = $("#view-full-details");
			var wl = window.location;
			var qsParams = (wl.search.length > 1) ? util.getQueryStringParams(wl.search.substr(1)) : {};
			var hashParams = (wl.hash.length > 1) ? util.getQueryStringParams(wl.hash.substr(1)) : {};
			var params = $.extend(hashParams, qsParams);
			params.start = parseInt(this.productLinkIndex)+1;
			var tile = $('#search-result-items .product-tile').first();
			if(!params.cgid && tile.data("cgid") != null && tile.data("cgid") != ""){
				params.cgid=tile.data("cgid");
			}
			a.attr("href",a.attr("href")+"#"+$.param(params));
			/*End JIRA PREV-50*/
		}

		if (this.productLinkIndex === this.productLinks.length - 1) {
			$btnNext.attr('disabled', 'disabled');
		}
		if (this.productLinkIndex === 0) {
			$btnPrev.attr('disabled', 'disabled');
		}

		$btnNext.on('click', function (e) {
			e.preventDefault();
			this.navigateQuickview(1);
		}.bind(this));
		$btnPrev.on('click', function (e) {
			e.preventDefault();
			this.navigateQuickview(-1);
		}.bind(this));
	},

	/**
	 * @param {Number} step - How many products away from current product to navigate to. Negative number means navigate backward
	 */
	navigateQuickview: function (step) {
		// default step to 0
		this.productLinkIndex += (step ? step : 0);
		var url = makeUrl(this.productLinks[this.productLinkIndex], 'quickview');
		dialog.replace({
			url: url,
			callback: this.setup.bind(this, url)
		});
	},

	/**
	 * @description show quick view dialog
	 * @param {Object} options
	 * @param {String} options.url - url of the product details
	 * @param {String} options.source - source of the dialog to be appended to URL
	 * @param {String} options.productlistid - to be appended to URL
	 * @param {Function} options.callback - callback once the dialog is opened
	 */
	show: function (options) {
		var url;
		if (!this.exists()) {
			this.init();
		}
		url = makeUrl(options.url, options.source, options.productlistid);

		dialog.open({
			target: this.$container,
			url: url,
			options: {
				width: 920,
				title: Resources.QUICK_VIEW_POPUP,
				open: function () {
					this.setup(url);
					if (typeof options.callback === 'function') { options.callback(); }
				}.bind(this)
			}
		});
		
	},
	exists: function () {
		return this.$container && (this.$container.length > 0);
	}
};

module.exports = quickview;

},{"./dialog":10,"./pages/product":30,"./util":52,"lodash":58}],42:[function(require,module,exports){
'use strict';

/**
 * copied from https://github.com/darkskyapp/string-hash
 */
function hashFn(str) {
	var hash = 5381,
		i = str.length;

	while (i) {
		hash = (hash * 33) ^ str.charCodeAt(--i);
	}
	/* JavaScript does bitwise operations (like XOR, above) on 32-bit signed
	* integers. Since we want the results to be always positive, convert the
	* signed int to an unsigned by doing an unsigned bitshift. */
	return hash >>> 0;
}

/**
 * Create rating based on hash ranging from 2-5
 * @param pid
 */
function getRating(pid) {
	return hashFn(pid.toString()) % 30 / 10 + 2;
}

module.exports = {
	init: function () {
		$('.product-review').each(function (index, review) {
			var pid = $(review).data('pid');
			if (!pid) {
				return;
			}
			// rating range from 2 - 5
			var rating = getRating(pid);
			var baseRating = Math.floor(rating);
			var starsCount = 0;
			//JIRA PREV-474 & PREV-483 : Ratings stars are appended on each scroll action.
			$('.rating', review).empty();
			for (var i = 0; i < baseRating; i++) {
				$('.rating', review).append('<i class="fa fa-star"></i>');
				starsCount++;
			}
			// give half star for anything in between
			if (rating > baseRating) {
				$('.rating', review).append('<i class="fa fa-star-half-o"></i>');
				starsCount++;
			}
			if (starsCount < 5) {
				for (var j = 0; j < 5 - starsCount; j++) {
					$('.rating', review).append('<i class="fa fa-star-o"></i>');
				}
			}
		});
	}
};

},{}],43:[function(require,module,exports){
'use strict';

var ajax = require('./ajax'),
	util = require('./util');

var redeyetracking = {
		/**
		 * @function
		 * @description Function to trigger social share event in PDP
		 * @param {Object} params social share name, current product Id.
		 */
		socialShare: function (event,product) {
			var url = Urls.redEye;
			url = util.appendParamToURL(url, 'event', 'socialshare');
			if(event != undefined && event != null && event != ''){
				url = util.appendParamToURL(url, 'share', event);
			}
			if(product != undefined && product != null && product != ''){
				url = util.appendParamToURL(url, 'pid', product);
			}
			url = util.appendParamToURL(url, 'format', 'ajax');
			$.ajax({
				type: 'GET',
				dataType: 'html',
				url: url,
				success: function (response) {
					$("body").append(response);
				}
			});
		},
		colourCard: function (event,form){
			var url = Urls.redEye;
			url = util.appendParamToURL(url, 'event', event);
			url = util.appendParamToURL(url, 'format', 'ajax');
			var data = form.serialize();
			$.ajax({
				type: 'POST',
				url: url,
				data: data,
				dataType: 'html',
				success: function (response) {
					$("body").append(response);
				}
			});
		},
		emailPassed: function (email){
			var url = Urls.redEye;
			url = util.appendParamToURL(url, 'event', 'emailpassed');
			url = util.appendParamToURL(url, 'email', email);
			url = util.appendParamToURL(url, 'format', 'ajax');
			$.ajax({
				type: 'POST',
				url: url,
				dataType: 'html',
				success: function (response) {
					$("body").append(response);
				}
			});
		},
		emailSignUp: function (email){
			var url = Urls.redEye;
			url = util.appendParamToURL(url, 'event', 'emailsignup');
			url = util.appendParamToURL(url, 'email', email);
			url = util.appendParamToURL(url, 'format', 'ajax');
			$.ajax({
				type: 'POST',
				url: url,
				dataType: 'html',
				success: function (response) {
					$("body").append(response);
				}
			});
		}
};

module.exports = redeyetracking;

},{"./ajax":1,"./util":52}],44:[function(require,module,exports){
'use strict';

/**
 * @private
 * @function
 * @description Binds event to the place holder (.blur)
 */
function initializeEvents() {
	/* Start JIRA PREV-53:No search result page: When the search text field is 
 	empty,on clicking of "GO" button user is navigating to Home page.
 	Replaced #q with 'input[name=q]'*/
	$('input[name=q]').focus(function () {
		var input = $(this);
		if (input.val() === input.attr('placeholder')) {
			input.val('');
		}
	})
	.blur(function () {
		var input = $(this);
		/* Start JIRA PREV-53:No search result page: When the search text field is empty,on clicking of "GO"
		   button user is navigating to Home page.Added $.trim(input.val()) === ""*/
		if ($.trim(input.val()) === "" || input.val() === '' || input.val() === input.attr('placeholder')) {
			input.val(input.attr('placeholder'));
		}
	})
	.blur();
	
	/* Start JIRA-PREV-54:General Error page: When the new search field empty, on clicking of "GO" user is navigating to Home page.
	   Added condition for disabling search button in header and No search results page and error pages.
	   Start JIRA-PREV-53:No search result page: When the search text field is empty,on clicking of "GO" button user is navigating to Home page.*/
	$("input[name=q]").closest("form").submit(function(e){
		var input = $(this).find("input[name=q]");
		if($.trim(input.val()) === input.attr("placeholder") || $.trim(input.val()) === ""){
			e.preventDefault();
			return false;
		}
	});
	/*End JIRA PREV-53,PREV-54 */
}

exports.init = initializeEvents;

},{}],45:[function(require,module,exports){
'use strict';

var util = require('./util');

var currentQuery = null,
	lastQuery = null,
	runningQuery = null,
	listTotal = -1,
	listCurrent = -1,
	delay = 30,
	$resultsContainer;
/**
 * @function
 * @description Handles keyboard's arrow keys
 * @param keyCode Code of an arrow key to be handled
 */
function handleArrowKeys(keyCode) {
	switch (keyCode) {
		case 38:
			// keyUp
			listCurrent = (listCurrent <= 0) ? (listTotal - 1) : (listCurrent - 1);
			break;
		case 40:
			// keyDown
			listCurrent = (listCurrent >= listTotal - 1) ? 0 : listCurrent + 1;
			break;
		default:
			// reset
			listCurrent = -1;
			return false;
	}

	$resultsContainer.children().removeClass('selected').eq(listCurrent).addClass('selected');
	$('input[name="q"]').val($resultsContainer.find('.selected .suggestionterm').first().text());
	return true;
}

var searchsuggest = {
	/**
	 * @function
	 * @description Configures parameters and required object instances
	 */
	init: function (container, defaultValue) {
		var $searchContainer = $(container);
		var $searchForm = $searchContainer.find('form[name="simpleSearch"]');
		var $searchField = $searchForm.find('input[name="q"]');

		// disable browser auto complete
		$searchField.attr('autocomplete', 'off');

		// on focus listener (clear default value)
		$searchField.focus(function () {
			if (!$resultsContainer) {
				// create results container if needed
				$resultsContainer = $('<div/>').attr('id', 'search-suggestions').appendTo($searchContainer);
			}
			if ($searchField.val() === defaultValue) {
				$searchField.val('');
			}
		});
		// on blur listener
		$(document).on('click', function (e) {
			if (!$searchContainer.is(e.target)) {
				setTimeout(this.clearResults, 200);
			}
		}.bind(this));
		// on key up listener
		$searchField.keyup(function (e) {

			// get keyCode (window.event is for IE)
			var keyCode = e.keyCode || window.event.keyCode;

			// check and treat up and down arrows
			if (handleArrowKeys(keyCode)) {
				return;
			}
			// check for an ENTER or ESC
			if (keyCode === 13 || keyCode === 27) {
				this.clearResults();
				return;
			}

			currentQuery = $searchField.val().trim();

			// no query currently running, init a update
			if (runningQuery === null) {
				runningQuery = currentQuery;
				setTimeout(this.suggest.bind(this), delay);
			}
		}.bind(this));
	},

	/**
	 * @function
	 * @description trigger suggest action
	 */
	suggest: function () {
		// check whether query to execute (runningQuery) is still up to date and had not changed in the meanwhile
		// (we had a little delay)
		if (runningQuery !== currentQuery) {
			// update running query to the most recent search phrase
			runningQuery = currentQuery;
		}

		// if it's empty clear the results box and return
		if (runningQuery.length === 0) {
			this.clearResults();
			runningQuery = null;
			return;
		}

		// if the current search phrase is the same as for the last suggestion call, just return
		if (lastQuery === runningQuery) {
			runningQuery = null;
			return;
		}

		// build the request url
		var reqUrl = util.appendParamToURL(Urls.searchsuggest, 'q', runningQuery);
		reqUrl = util.appendParamToURL(reqUrl, 'legacy', 'false');

		// execute server call
		$.get(reqUrl, function (data) {
			var suggestionHTML = data,
				ansLength = suggestionHTML.trim().length;

			// if there are results populate the results div
			if (ansLength === 0) {
				this.clearResults();
			} else {
				// update the results div
				$resultsContainer.html(suggestionHTML).fadeIn(200);
			}

			// record the query that has been executed
			lastQuery = runningQuery;
			// reset currently running query
			runningQuery = null;

			// check for another required update (if current search phrase is different from just executed call)
			if (currentQuery !== lastQuery) {
				// ... and execute immediately if search has changed while this server call was in transit
				runningQuery = currentQuery;
				setTimeout(this.suggest.bind(this), delay);
			}
			this.hideLeftPanel();
		}.bind(this));
	},
	/**
	 * @function
	 * @description
	 */
	clearResults: function () {
		if (!$resultsContainer) { return; }
		$resultsContainer.fadeOut(200, function () {$resultsContainer.empty();});
	},
	/**
	 * @function
	 * @description
	 */
	hideLeftPanel: function () {
		//hide left panel if there is only a matching suggested custom phrase
		if ($('.search-suggestion-left-panel-hit').length === 1 && $('.search-phrase-suggestion a').text().replace(/(^[\s]+|[\s]+$)/g, '').toUpperCase() === $('.search-suggestion-left-panel-hit a').text().toUpperCase()) {
			$('.search-suggestion-left-panel').css('display', 'none');
			$('.search-suggestion-wrapper-full').addClass('search-suggestion-wrapper');
			$('.search-suggestion-wrapper').removeClass('search-suggestion-wrapper-full');
		}
	}
};

module.exports = searchsuggest;

},{"./util":52}],46:[function(require,module,exports){
'use strict';

var util = require('./util');

var qlen = 0,
	listTotal = -1,
	listCurrent = -1,
	delay = 300,
	fieldDefault = null,
	suggestionsJson = null,
	$searchForm,
	$searchField,
	$searchContainer,
	$resultsContainer;
/**
 * @function
 * @description Handles keyboard's arrow keys
 * @param keyCode Code of an arrow key to be handled
 */
function handleArrowKeys(keyCode) {
	switch (keyCode) {
		case 38:
			// keyUp
			listCurrent = (listCurrent <= 0) ? (listTotal - 1) : (listCurrent - 1);
			break;
		case 40:
			// keyDown
			listCurrent = (listCurrent >= listTotal - 1) ? 0 : listCurrent + 1;
			break;
		default:
			// reset
			listCurrent = -1;
			return false;
	}

	$resultsContainer.children().removeClass('selected').eq(listCurrent).addClass('selected');
	$searchField.val($resultsContainer.find('.selected .suggestionterm').first().text());
	return true;
}
var searchsuggest = {
	/**
	 * @function
	 * @description Configures parameters and required object instances
	 */
	init: function (container, defaultValue) {
		// initialize vars
		$searchContainer = $(container);
		$searchForm = $searchContainer.find('form[name="simpleSearch"]');
		$searchField = $searchForm.find('input[name="q"]');
		fieldDefault = defaultValue;

		// disable browser auto complete
		$searchField.attr('autocomplete', 'off');

		// on focus listener (clear default value)
		$searchField.focus(function () {
			if (!$resultsContainer) {
				// create results container if needed
				$resultsContainer = $('<div/>').attr('id', 'suggestions').appendTo($searchContainer).css({
					'top': $searchContainer[0].offsetHeight,
					'left': 0,
					'width': $searchField[0].offsetWidth
				});
			}
			if ($searchField.val() === fieldDefault) {
				$searchField.val('');
			}
		});
		// on blur listener
		$searchField.blur(function () {
			setTimeout(this.clearResults, 200);
		}.bind(this));
		// on key up listener
		$searchField.keyup(function (e) {

			// get keyCode (window.event is for IE)
			var keyCode = e.keyCode || window.event.keyCode;

			// check and treat up and down arrows
			if (handleArrowKeys(keyCode)) {
				return;
			}
			// check for an ENTER or ESC
			if (keyCode === 13 || keyCode === 27) {
				this.clearResults();
				return;
			}

			var lastVal = $searchField.val();

			// if is text, call with delay
			setTimeout(function () {
				this.suggest(lastVal);
			}.bind(this), delay);
		}.bind(this));
		// on submit we do not submit the form, but change the window location
		// in order to avoid https to http warnings in the browser
		// only if it's not the default value and it's not empty
		$searchForm.submit(function (e) {
			e.preventDefault();
			var searchTerm = $searchField.val();
			if (searchTerm === fieldDefault || searchTerm.length === 0) {
				return false;
			}
			window.location = util.appendParamToURL($(this).attr('action'), 'q', searchTerm);
		});
	},

	/**
	 * @function
	 * @description trigger suggest action
	 * @param lastValue
	 */
	suggest: function (lastValue) {
		// get the field value
		var part = $searchField.val();

		// if it's empty clear the resuts box and return
		if (part.length === 0) {
			this.clearResults();
			return;
		}

		// if part is not equal to the value from the initiated call,
		// or there were no results in the last call and the query length
		// is longer than the last query length, return
		// #TODO: improve this to look at the query value and length
		if ((lastValue !== part) || (listTotal === 0 && part.length > qlen)) {
			return;
		}
		qlen = part.length;

		// build the request url
		var reqUrl = util.appendParamToURL(Urls.searchsuggest, 'q', part);
		reqUrl = util.appendParamToURL(reqUrl, 'legacy', 'true');

		// get remote data as JSON
		$.getJSON(reqUrl, function (data) {
			// get the total of results
			var suggestions = data,
				ansLength = suggestions.length;

			// if there are results populate the results div
			if (ansLength === 0) {
				this.clearResults();
				return;
			}
			suggestionsJson = suggestions;
			var html = '';
			for (var i = 0; i < ansLength; i++) {
				html += '<div><div class="suggestionterm">' + suggestions[i].suggestion + '</div><span class="hits">' + suggestions[i].hits + '</span></div>';
			}

			// update the results div
			$resultsContainer.html(html).show().on('hover', 'div', function () {
				$(this).toggleClass = 'selected';
			}).on('click', 'div', function () {
				// on click copy suggestion to search field, hide the list and submit the search
				$searchField.val($(this).children('.suggestionterm').text());
				this.clearResults();
				$searchForm.trigger('submit');
			}.bind(this));
		}.bind(this));
	},
	/**
	 * @function
	 * @description
	 */
	clearResults: function () {
		if (!$resultsContainer) { return; }
		$resultsContainer.empty().hide();
	}
};

module.exports = searchsuggest;

},{"./util":52}],47:[function(require,module,exports){
'use strict';

var ajax = require('./ajax'),
	progress = require('./progress'),
	checkout =  require('./pages/checkout'),
	util = require('./util'),
	tooltip = require('./tooltip'),
	validator = require('./validator'),
	redeye = require('./redeyetracking'),
	formPrepare = require('./pages/checkout/formPrepare'),
	shipping = require('./pages/checkout/shipping'),
	dialog = require('./dialog');

var $cache = {},
	isSPCLogin = false,
	isSPCShipping = false,
	isSPCMultiShipping = false,
	isSPCShipToShip = false,
	isSPCBilling = false;

function initializeCache() {
    $cache = {
    	main: $('#main'),
        primary: $('#primary'),
        secondary: $('#secondary')
    };
}

function initializeDom() {
    isSPCLogin = $('.spc-login').length > 0;
    isSPCShipping = $('.spc-shipping').length > 0;
    isSPCMultiShipping = $('.spc-multi-shipping').length > 0;
    isSPCShipToShip = $('.clickandcollect') > 0;
    isSPCBilling = $('.spc-billing').length > 0;
}

function initializeEvents() {
	
	if (isSPCLogin) {
        loadSPCLogin();

    } else if (isSPCShipping) {
    	
        loadSPCShipping();

    } else if (isSPCMultiShipping) {
        loadSPCMultiShipping();

    } else if (isSPCBilling) {
        loadSPCBilling();

    }
    
    $('.pt_checkout').ajaxError(function() {
    	window.location.href = Urls.cartShow;
    });
    
	$("a.tooltip").click(function(e){
		e.preventDefault();
	});
    
    $('.privacy-policy').on('click', function (e) {
		e.preventDefault();
		dialog.open({
			url: $(e.target).attr('href'),
			options: {
				height: 600
			}
		});
	});
    
    $cache.primary.off('click', '.checkout-tab-head[data-href]');
    $cache.primary.on("click", '.checkout-tab-head[data-href]', function(e) {
        e.preventDefault();
        progress.show($cache.primary);
        ajax.load({
            url: $(this).data('href'),
            type: 'POST',
            callback: function(data) {
            	if(handleBasketError(data)){
	                $cache.primary.html(data);
	                validator.init();
	                initSPC();
	                if ($cache.primary.find('.spc-login').length == 0) {
	                    tooltip.init();
	                    util.limitCharacters();
	                    util.selectbox();
	                    checkout.init();
	                }
	                progress.hide();
	                updateAnchor();
	                //Code Changes for the GB-752
	                if($('#dwfrm_singleshipping_shippingAddress_addressFields_countrySelectBoxIt').text().toUpperCase()=="NORWAY"){
	                	$('.personalidentificationnumber').closest('.form-row').show();
	                }else{
	                	$('.personalidentificationnumber').closest('.form-row').hide();
	                }
	                
	                if(SitePreferences.ShippingMethod_ClickAndCollect.trim() == $(".minishipments-method .shippingMethodID").val().trim()){
	                	retainStoreDetails(data);
	                }else if( $(".minishipments-method .shippingMethodName").text().trim() != "" ){
	                	$('.delivery-tab-text').trigger("click");
	                }
            	}
            }
        });
    });
    
    $cache.primary.on("click", '.checkout-tab-head.open[data-refreshurl]', function(e) {
        e.preventDefault();
        progress.show($cache.primary);
        ajax.load({
            url: $(this).data('refreshurl'),
            type: 'POST',
            callback: function(data) {
            	if(handleBasketError(data)){
	                $cache.primary.html(data);
	                validator.init();
	                initSPC();
	                if ($cache.primary.find('.spc-login').length == 0) {
	                    tooltip.init();
	                    util.limitCharacters();
	                    util.selectbox();
	                    checkout.init();
	                }
	                progress.hide();
	                updateSummary();
	                //Code Changes for the GB-752
	                if($('#dwfrm_singleshipping_shippingAddress_addressFields_countrySelectBoxIt').text().toUpperCase()=="NORWAY"){
	                	$('.personalidentificationnumber').closest('.form-row').show();
	                }else{
	                	$('.personalidentificationnumber').closest('.form-row').hide();
	                }
	                
	                if(SitePreferences.ShippingMethod_ClickAndCollect.trim() == $(".minishipments-method .shippingMethodID").val().trim()){
	                	retainStoreDetails(data);
	                }else if( $(".minishipments-method .shippingMethodName").text().trim() != "" ){
	                	$('.delivery-tab-text').trigger("click");
	                }
            	}
            }
        });
    });

    $cache.secondary.on("click", '.order-component-block a, .order-totals-table .order-shipping a', function(e) {
        e.preventDefault();
        progress.show($cache.primary);
        ajax.load({
            url: $(this).attr('href'),
            type: 'POST',
            callback: function(data) {
            	if(handleBasketError(data)){
	                $cache.primary.html(data);
	                validator.init();
	                initSPC();
	                tooltip.init();
	                util.limitCharacters();
	                util.selectbox();
	                checkout.init();
	                progress.hide();
	                updateAnchor();
	                //Code Changes for the GB-752
	                if($('#dwfrm_singleshipping_shippingAddress_addressFields_countrySelectBoxIt').text().toUpperCase()=="NORWAY"){
	                	$('.personalidentificationnumber').closest('.form-row').show();
	                }else{
	                	$('.personalidentificationnumber').closest('.form-row').hide();
	                }
	                
	                if(SitePreferences.ShippingMethod_ClickAndCollect.trim() == $(".minishipments-method .shippingMethodID").val().trim()){
	                	retainStoreDetails(data);
	                }else if( $(".minishipments-method .shippingMethodName").text().trim() != "" ){
	                	$('.delivery-tab-text').trigger("click");
	                }
            	}
            }
        });
    });
    $cache.primary.on('click', '.delivery-tab-text.cc-js', function(e) {
    	$(this).closest('.checkout-tab').find('.cc-js .clickandcollect-text').removeClass('act');
		$(this).closest('.checkout-tab').find('.clickandcollect').removeClass('active1');
		$(this).closest('.checkout-tab').find('.delivery-details').addClass('active1');
		$(this).closest('.checkout-tab').find('.delivery-text').addClass('act');
		$(this).closest('.checkout-tab').find('.clickandcollect-text').removeClass('act');
		
		//Dialog box functionality
		var currentSite = $("#wrapper .country-select").find("select[id$='_destination'] option[selected=selected]").val();
		 var currentCountryCode = $('.checkout-shipping #dwfrm_singleshipping_shippingAddress_addressFields_country').val();
		 var countryCode;
		 if($('.checkout-shipping #dwfrm_singleshipping_shippingAddress_addressFields_country').val() == "GB"){
			 countryCode = "UK";
		 }else{
			 countryCode = $('.checkout-shipping #dwfrm_singleshipping_shippingAddress_addressFields_country').val();
		 }
		 if((countryCode != undefined) && (currentSite != getSite(countryCode))){
			 if(currentSite == "UK"){
				 currentCountryCode = "GB";
				 showDeliveryCountryChangePopup(countryCode,"GB");
			 }else if(currentSite == "ROW"){
				 currentCountryCode = "";
				 showDeliveryCountryChangePopup(countryCode,"");
			 }else{
				 currentCountryCode = currentSite;
				 showDeliveryCountryChangePopup(countryCode,currentSite);
			 }
		 }
    }); 
    $cache.primary.on('click', '.click-and-collect-tab-text', function(e) {
		$(this).closest('.checkout-tab').find('.cc-js .delivery-text').removeClass('act');
		$(this).closest('.checkout-tab').find('.delivery-details').removeClass('active1');
		$(this).closest('.checkout-tab').find('.clickandcollect').addClass('active1');
		$(this).closest('.checkout-tab').find('.clickandcollect-text').addClass('act');
		$(this).closest('.checkout-tab').find('.delivery-tab-text').removeClass('act');
});

    $cache.primary.on('submit', 'form.address', function(e) {
        if($(this).find('button[class$=-btn]').length > 0){
        	e.preventDefault();
        	$(this).find('button[class$=-btn]').click();
        }
    });
}
//Start GB-687
$(window).on("load", function() {
	if($('input[id=checklogincustomer]').val() != undefined && $('input[id=checklogincustomer]').val() == 'true'){
		if($('input[id=checknewlogin]').val() != undefined  && $('input[id=checknewlogin]').val() == 'true'){
			if(dataLayer != undefined){
				dataLayer.push({
					'event' : 'CustomerStateInCheckout',
					'ecommerce' : {
							'CustomerState' : 'New' 
							}
				});
			}
		}else{
			if(dataLayer != undefined){
				dataLayer.push({
					'event' : 'CustomerStateInCheckout',
					'ecommerce' : {
							'CustomerState' : 'Returned' 
							}
				});
			}
		}
		
	}
	//Start GB-689
	if($('input[id=sitename]').val() == 'GrahamBrownUS'){
		$('input[id^=dwfrm_login_addtolist]').prop('checked', 'checked');
	}
	//End GB-689
});
//End GB-687
function loadSPCLogin() {
    updateSummary();
    $cache.primary.find('.spc-login').trigger("ready");
    
    $cache.primary.off().on('click', '.spc-guest-btn:not(:disabled)', function(e) {
        e.preventDefault();
        var form = $(this).closest('form');
        $('<input type="hidden" name="' + $(this).attr('name') + '" value="true" />').appendTo(form);
        form.validate();
        if (!form.valid()) {
        	//this is for Checkout - Highlighting missing fields GB-570
        	if(form.find("input.error").length > 0){
        		form.find("input.error").first().focus();
        	}
            return false;
        }
        if($('input[id^=dwfrm_login_addtolist]').is(':checked')){
        	redeye.emailSignUp(form.find("input.email").val());
        }
        progress.show($cache.primary);
        ajax.load({
            url: util.ajaxUrl(form.attr('action')),
            data: form.serialize(),
            type: 'POST',
            callback: function(data) {
            	if(handleBasketError(data)){
	                $cache.primary.html(data);
	                validator.init();
	                initSPC();
	                if ($cache.primary.find('.spc-login').length == 0) {
	                    tooltip.init();
	                    util.limitCharacters();
	                    util.selectbox();
	                    checkout.init();
	                }
	                progress.hide();
	                updateAnchor();
	                //Code Changes for the GB-752
	            	if($('#dwfrm_singleshipping_shippingAddress_addressFields_countrySelectBoxIt').text().toUpperCase()=="NORWAY"){
	            	    $('.personalidentificationnumber').closest('.form-row').show();
	            	  }else{
	            		  $('.personalidentificationnumber').closest('.form-row').hide();
	            	  }
	            	if(SitePreferences.ShippingMethod_ClickAndCollect.trim() == $(".minishipments-method .shippingMethodID").val() != undefined ? $(".minishipments-method .shippingMethodID").val() : 'Not Avail'){
	                	retainStoreDetails(data);
	                }else if( $(".minishipments-method .shippingMethodName").text().trim() != "" ){
	                	$('.delivery-tab-text').trigger("click");
	                }
            	}
            	//Start GB-687
            	 if($('input[id=checklogincustomer]').val() != null && $('input[id=checklogincustomer]').val() == 'true'){
            		if($('input[id=checknewlogin]').val() != undefined  && $('input[id=checknewlogin]').val() == 'true'){
         	    		if(dataLayer != undefined){
         					dataLayer.push({
         						'event' : 'CustomerStateInCheckout',
         						'ecommerce' : {
         								'CustomerState' : 'New' 
         								}
         					});
         				}           			
            		}else{
         	    		if(dataLayer != undefined){
         					dataLayer.push({
         						'event' : 'CustomerStateInCheckout',
         						'ecommerce' : {
         								'CustomerState' : 'Returned' 
         								}
         					});
         				}
            		}

     	    	}
     	    	//End GB-687
            }
        });
        //Start GB-687
        var emailid = $('input[id^=dwfrm_login_username]').val();
        var cookieemail = util.GetCookie(emailid);
		if(cookieemail == ''){
			util.SetCookie(emailid, emailid ,365);
			if(dataLayer != undefined){
				dataLayer.push({
					'event' : 'CustomerStateInCheckout',
					'ecommerce' : {
							'CustomerState' : 'New' 
							}
				});
			}
		}else{
			if(dataLayer != undefined){
				dataLayer.push({
					'event' : 'CustomerStateInCheckout',
					'ecommerce' : {
							'CustomerState' : 'Returned' 
							}
				});
			}
		}
		
		//End GB-687
    });
    $cache.primary.on('click', '.checkoutlogin .col-1 .account-login', function(e) {
    	$cache.primary.find(".checkoutlogin .col-1").hide();
    	$cache.primary.find(".checkoutlogin .col-2").show();
    });
    $cache.primary.on('click', '.checkoutlogin .col-2 .guest-checkout', function(e) {
    	$cache.primary.find(".checkoutlogin .col-2").hide();
    	$cache.primary.find(".checkoutlogin .col-1").show();
    });
    //this is for Checkout - Highlighting missing fields GB-570 
    /*formPrepare.init({
		continueSelector: '[name$="dwfrm_login_unregistered"]',
		formSelector:'[id$="login_unregistered"]'
	});*/
	$('#password-reset').on('click', function (e) {
		e.preventDefault();
		dialog.open({
			url: $(e.target).attr('href'),
			options: {
				//Start JIRA PREV-334 : Title is missing for the Forgot password overlay.
				dialogClass : 'password-reset-popup custom-popup',
				width : 390,
				open: function () {
					validator.init();
					var $requestPasswordForm = $('[name$="_requestpassword"]'),
						$submit = $requestPasswordForm.find('[name$="_requestpassword_send"]');
					$($submit).on('click', function (e) {
						if (!$requestPasswordForm.valid()) {
							return;
						}
						e.preventDefault();
						dialog.submit($submit.attr('name'));
					});
				}
			}
		});
	});
	$cache.primary.on("click", ".spc-login-btn:not(:disabled)", function(e) {
        e.preventDefault();
        var form = $(this).closest('form');
        $('<input type="hidden" name="' + $(this).attr('name') + '" value="true" />').appendTo(form);
        form.validate();
        if (!form.valid()) {
            return false;
        }
        redeye.emailPassed(form.find("input.email").val());
        if(typeof __s2tQ !==undefined)
        {
        	__s2tQ.push(['storeData',{'Email':form.find("input.email").val()}]);
        }
        
        progress.show($cache.primary);
        ajax.load({
            url: util.ajaxUrl(form.attr('action')),
            data: form.serialize(),
            type: 'POST',
            callback: function(data) {
            	if($(data).find('#main #primary .reset_pwsd').length > 0){
            		window.location.href = Urls.resetPassword;
            		return;
            	}
            	if(handleBasketError(data)){
	                $cache.primary.html(data);
	                if(jQuery(data).find(".checkoutlogin").length > 0){
	                	$cache.primary.find(".checkoutlogin .col-1").hide();
	                	$cache.primary.find(".checkoutlogin .col-2").show();
	                }
	                validator.init();
	                initSPC();
	                if ($cache.primary.find('.spc-login').length == 0) {
	                    tooltip.init();
	                    util.limitCharacters();
	                    util.selectbox();
	                    checkout.init();
	                    updateCustomerInfo();
	                }
	                progress.hide();
	                updateAnchor();
            	}
            	//Start GB-687
            	 if($('input[id=logincheckout]').val() != null && $('input[id=logincheckout]').val() == 'true'){
            		 if(dataLayer != undefined){
       					dataLayer.push({
       						'event' : 'CustomerStateInCheckout',
       						'ecommerce' : {
       								'CustomerState' : 'Returned' 
       								}
       					});
       				}
            	}
            	//End GB-687
            }
        });
    });
}

function loadSPCShipping() {
	if($('#dwfrm_singleshipping_shippingAddress_addressFields_countrySelectBoxIt').text().toUpperCase()=="NORWAY"){
	    $('.personalidentificationnumber').closest('.form-row').show();
	  }else{
		  $('.personalidentificationnumber').closest('.form-row').hide();
	  }
    $cache.primary.on('click', '.spc-shipping-btn:not(:disabled)', function(e) {
        e.preventDefault();
        var stateVal = $("#dwfrm_singleshipping_shippingAddress_addressFields_states_stateSelectBoxItText").text();
        var countryVal = $('input[id=siteID]').val();
        var warningMsg = $('#warningMsg').val();
        var form = $(this).closest('form');
        $('<input type="hidden" name="' + $(this).attr('name') + '" value="true" />').appendTo(form);
        form.validate();
        if (!form.valid()) {
        	//this is for Checkout - Highlighting missing fields GB-570 
        	if($('.errorcheck').length == 0){
        		$('<span class="errorcheck">'+Resources.ERROR_MESSAGE+'</span>').insertBefore('.spc-shipping-btn');
        	} 
        	if(form.find("input.error").length > 0){
        		form.find("input.error").first().focus();
        	}
        	if($("#dwfrm_singleshipping_shippingAddress").find("select").hasClass('state') && $(".input-select.state").val() == ''){
    			$(".input-select.state").focus();
    		}
            return false;
        }
        
        if( warningMsg == 'true' && stateVal.toUpperCase() == 'CALIFORNIA' && countryVal == 'GrahamBrownUS' && sessionStorage.firstVisit != 'true'){
        	var data = "<div class='payment-alert-us'><span class='warning-headding'>"+ Resources.WARNING +"</span> <span>"+ Resources.NOTIFICATIONTEXT + "<a class='stores-toggle collapsed' style='white-space:pre;'target = '_blank' href='https://www.p65warnings.ca.gov/'>" + Resources.WARNINGSITE + "</a> </span></div>";
        	var homeleavingsitedialog = dialog.create({
        		html:data,
        		options: {
        			dialogClass : 'payment-alert-us custom-popup',
        			width : 390,
        			position: {
        				my: 'center',
        				at: 'center',
        				of: 'body',
        				collision: 'flipfit'
        			}
        		},
        	});
        	$('body').css('overflow','hidden');
        	if($(".payment-alert-us").length > 0){
        		$(".ui-button-icon-primary.ui-icon.ui-icon-closethick").click(function(e){
    				e.preventDefault();
    				dialog.close();
    				sessionStorage.setItem('firstVisit', 'true');
    				$('body').css('overflow','auto');
    			});
            	$('.payment-alert-us #dialog-container').empty();
        	}
            homeleavingsitedialog.append(data).dialog('open'); 
        	return;
        }
        
        
        if(form.find("#useshippingyes").prop('checked')){
        	copyShippindAddrToBillingAddr();
        }
        progress.show($cache.primary);
        ajax.load({
            url: util.ajaxUrl(form.attr('action')),
            data: form.serialize(),
            type: 'POST',
            callback: function(data) {
            	if(handleBasketError(data)){
	                $cache.primary.html(data);
	                updateSummary();
	                validator.init();
	                initSPC();
	                tooltip.init();
	                util.limitCharacters();
	                util.selectbox();
	                checkout.init();
	                progress.hide();
	                if ($('.payment-method-options').length > 0) {
	                	$('html, body').animate({scrollTop:$('.payment-method-options').position().top - 65 }, '300');
	                }
	                if ($cache.primary.find('.spc-shipping').length == 0) {
	                	updateAnchor();
	                }else{
	                	updateAnchor(true);
	                }
            	}
            }
        });
    });
    

    //click and collect triggering to payment page
    
    $cache.primary.on('click', '.spc-shipping .clickandcollect .spc-shipping-shiptoship-btn', function(e){
    	e.preventDefault();
    	var form = $(this).closest('form');
    	$('<input type="hidden" name="' + $(this).attr('name') + '" value="true" />').appendTo(form);
    	form.validate();
    	if (!form.valid()) {
    		if(form.find("input.error").length > 0){
        		form.find("input.error").first().focus();
        	}
    		return false;
        }
    	
    	if($('input[name="store"]:checked').length > 0){
    		$(".spcshipping").find("span").remove();
    	}else{
    		if($(".errorcheck").length == 0){
    			$('<span class="errorcheck">'+Resources.ERROR_MESSAGE+'</span>').insertBefore('.spc-shipping-shiptoship-btn');
    		}
    		return false;
    	}
    	
    	
    	progress.show($cache.primary);
    	
    	ajax.load({

            url: util.ajaxUrl(form.attr('action')),
            data: form.serialize(),
            type: 'POST',
            callback: function(data) {
            	if(handleBasketError(data)){
	                $cache.primary.html(data);
	                updateSummary();
	                validator.init();
	                initSPC();
	                tooltip.init();
	                util.limitCharacters();
	                util.selectbox();
	                checkout.init();
	                progress.hide();
	                if ($('.payment-method-options').length > 0) {
	                	$('html, body').animate({scrollTop:$('.payment-method-options').position().top - 65 }, '300');
	                }
	                if ($cache.primary.find('.spc-shipping').length == 0) {
	                	updateAnchor();
	                }else{
	                	updateAnchor(true);
	                }
            	}
            }
    		
    	});
    });
    
    //end
    
    
    $cache.primary.on('click', '.shiptomultiplebutton:not(:disabled)', function(e) {
        e.preventDefault();
        var form = $(this).closest('form');
        var url = util.appendParamToURL(form.attr('action'), $(this).attr('name'), true);
        url = util.appendParamToURL(url, form.find("input[name$=_securekey]").attr("name"), form.find("input[name$=_securekey]").attr("value"));
        progress.show($cache.primary);
        ajax.load({
            url: url,
            type: 'POST',
            callback: function(data) {
            	if(handleBasketError(data)){
	                $cache.primary.html(data);
	                updateSummary();
	                validator.init();
	                initSPC();
	                tooltip.init();
	                util.limitCharacters();
	                util.selectbox();
	                checkout.init();
	                progress.hide();
	                updateAnchor();
            	}
            }
        });
    });
    
    $cache.primary.off('click', '.checkout-shipping .coupon-head');
    $cache.primary.on('click', '.checkout-shipping .coupon-head', function(e) {
    	$cache.primary.find(".checkout-shipping .coupon-head").toggleClass('active');
    	$cache.primary.find(".checkout-shipping .billing-coupon-code").slideToggle(150);
    });
    
    $('#add-coupon').off('click');
    $('#add-coupon').on('click', function (e) {
		e.preventDefault();
		var $checkoutForm = $(".checkout-shipping"),
			$couponCode = $('input[name$="_couponCode"]'),
			$error = $checkoutForm.find('.coupon-error'),
			code = $couponCode.val();
		if (code.length === 0) {
			$error.html(Resources.COUPON_CODE_MISSING);
			return;
		}

		var url = util.appendParamsToUrl(Urls.addCoupon, {couponCode: code, format: 'ajax'});
		$.getJSON(url, function (data) {
			var fail = false;
			var msg = '';
			if (!data) {
				msg = Resources.BAD_RESPONSE;
				fail = true;
			} else if (!data.success) {
				msg = data.message.split('<').join('&lt;').split('>').join('&gt;');
				fail = true;
			}
			/*JIRA PREV-96 : Billing page: Not updating the order summary and not displaying the success message, when the user applied any coupon. 
			  Added below block to display the coupon code apply message.*/
			else if(data.success){
				msg = data.message;
				$error.html(msg);
			}
			
			if (fail) {
				$error.html(msg);
				return;
			}

			//basket check for displaying the payment section, if the adjusted total of the basket is 0 after applying the coupon
			//this will force a page refresh to display the coupon message based on a parameter message
			if (data.success) {
				//PREVAIL-Added '$(".spc-billing").length === 0' to handle SPC.
				updateSummary();
			}
		});
	});
    
    var currentSite = $("#wrapper .country-select").find("select[id$='_destination'] option[selected=selected]").val();
    if(currentSite != $('.checkout-shipping #dwfrm_singleshipping_shippingAddress_addressFields_country').val()){
    	updateStateOptionsshipping($('.spc-shipping .shippingForm'));
	}
    var useasshipping = $(".spc-shipping .useasshippingaddress input[name='useasshipping']").val();
    if (useasshipping !='false') {
    	jQuery(".spc-shipping .bilingForm").hide();
		jQuery("#useshippingyes").prop('checked', true);
		jQuery("#useshippingno").prop('checked', false);

	} else {
		jQuery(".spc-shipping .bilingForm").show();
		jQuery("#useshippingyes").prop('checked', false);
		jQuery("#useshippingno").prop('checked', true);
		initBilling();
		if(currentSite != $('.checkout-shipping #dwfrm_billing_billingAddress_addressFields_country').val()){
			updateStateOptionsbilling($('.spc-shipping .bilingForm'));
		}
	}
    
    jQuery("#useshippingyes").change(function () {
		changed_hide();
	});
	
	
	jQuery("#useshippingno").change(function () {
        changed_show();
        initBilling();
    });
	
	var $formshipping = $('.spc-shipping .shippingForm');
    if($formshipping.find('select[id$="_country"]').length > 0 && $formshipping.find('select[id$="_country"]').val() == 'GB' && $formshipping.find('[name$="_state"][id$="_state"]').length > 0){
       $formshipping.find('[name$="_state"][id$="_state"]').closest(".form-row").hide();
       }
    if($("#wrapper .country-select").find("select[id$='_destination'] option[selected=selected]").val() == 'US' || $("#wrapper .country-select").find("select[id$='_destination'] option[selected=selected]").val() == 'CA'){
       $cache.primary.on('change', '.checkout-shipping #dwfrm_singleshipping_shippingAddress_addressFields_country' ,function () {
            updateStateOptionsshipping($formshipping);
      });
    }
   
  
       
     var $form = $('.spc-shipping .bilingForm');
     $('select[id$="_country"]', $form).on('change', function () {
         updateStateOptionsbilling($form);
     });
       
     function getSiteFieldType(value){
         switch(value){
            case "GB":
                   return "";
            case "FR":
                   return "INPUT";
            case "AU":
                    return "INPUT";
            case "NZ":
                	return "INPUT";                    
            case "CA":
                   return "SELECT";
            case "DE":
            return "INPUT";
            case "NL":
                   return "INPUT";
            case "US":
                   return "SELECT";
            default:
                   return "INPUT";
           }
       }
            
       function updateStateOptionsbilling (form) {
            var $form = $(form);
            if($('.checkout-shipping #dwfrm_billing_billingAddress_addressFields_country').val() != "GB"){
	            if($form.find('[name$="_state"][id$="_state"]').length > 0 && !$form.find('[name$="_state"][id$="_state"]').is(":visible")){
	                   $form.find('[name$="_state"][id$="_state"]').closest(".form-row").show();
	            }
	            $form.find('label[for$="_state"]').removeClass('error');
	            $form.find('input[name$="_state"]').removeClass('error');
	            $form.find('span[id$="_state-error"]').remove();
	            var $country = $form.find('select[id$="_country"]'),
	                site = $country.val(),
	                country = Countries[$country.val()];
	            var sitevalue = getSiteFieldType(site);
	            var isstate = $form.find(".form-row .field-wrapper .state");
	            if(isstate.length > 0)
	            {
	               var id = isstate.attr("id");
	               var name = isstate.attr("name");
	            }else{
	                 var id = "dwfrm_profile_address_states_state";
	               var name = "dwfrm_profile_address_states_state";
	            }
	            if ($country.length === 0 || !country) {
	                if($form.find('[name$="_state"][id$="_state"]').length > 0){
	                      $form.find('[name$="_state"][id$="_state"]').closest("div.form-row").find("label span").first().html("State / Province / Region");
	                      var fieldHTML = $('<input/>').attr({ type: 'text', class :'input-text state ', id:id, name:name });
	                      if($form.find('select[name$="_state"]').length){
	                            $form.find('select[name$="_state"]').replaceWith( fieldHTML);
	                      }else{
	                            $form.find('input[name$="_state"]').replaceWith( fieldHTML);
	                      }
	                   }
	                return;
	            }
	            if(sitevalue == "SELECT") 
	            { 
	                var  fieldHTML = $('<select/>').attr({ class :'input-select state valid', id:id, name:name});
	                if($form.find('select[name$="_state"]').length){
	                      $form.find('select[name$="_state"]').replaceWith( fieldHTML);
	                } else{
	                      $form.find('input[name$="_state"]').replaceWith( fieldHTML);
	                }
	            }else if(sitevalue == "INPUT"){  
	                var fieldHTML = $('<input/>').attr({ type: 'text', class :'input-text state ', id:id, name:name });
	                if($form.find('select[name$="_state"]').length){
	                      $form.find('select[name$="_state"]').replaceWith( fieldHTML);
	                      if($form.find('#stateBillValue').val() != undefined && $form.find('#stateBillValue').val() != ''){
	    	            	  $form.find('input[name$="_state"]').val($form.find('#stateBillValue').val());
	    	              }
	                }else{
	                     $form.find('input[name$="_state"]').replaceWith( fieldHTML);
	                     if($form.find('#stateBillValue').val() != undefined && $form.find('#stateBillValue').val() != ''){
	   	            	  $form.find('select[name$="_state"]').val($form.find('#stateBillValue').val());
	   	              }
	                }
	            }else{
	                   if($form.find('[name$="_state"][id$="_state"]').length > 0){
	                         $form.find('[name$="_state"][id$="_state"]').closest(".form-row").hide();
	                   }
	            }   
	            if(sitevalue == "SELECT") 
	            {
	              var arrHtml = [],
	              $stateField = $country.data('stateField') ? $country.data('stateField') : $form.find('select[name$="_state"]');
	    		  //$stateLabel = ($stateField.length > 0) ? $form.find('label[for="' + $stateField[0].id + '"] span').not('.required-indicator') : undefined;
	            }else if(sitevalue == "INPUT"){
	            	var arrHtml = [],
	                $stateField = $country.data('stateField') ? $country.data('stateField') : $form.find('input[name$="_state"]');
	                //$stateLabel = ($stateField.length > 0) ? $stateField.closest("div.form-row").find("label span").first() : undefined;
	            }
	            /*if($stateLabel){
	            	$stateLabel.html(country.regionLabel);
	            }else{
	               return;
	            }*/
	            if(sitevalue == "SELECT")      
	            {
	        	  var s;
	        	  arrHtml.push('<option value="">Select</option>');
	              for (s in country.regions) {
	                  arrHtml.push('<option value="' + s + '">' + country.regions[s] + '</option>');
	              }
	              $form.find('select[name$="_state"]').html(arrHtml.join(""));
	              if($form.find('#stateBillValue').val() != undefined && $form.find('#stateBillValue').val() != ''){
	            	  $form.find('select[name$="_state"]').val($form.find('#stateBillValue').val());
	              }
	           }
	            util.selectbox();
    	   }else{
    		   $form.find('[name$="_state"][id$="_state"]').closest(".form-row").hide();
    	   }
       }
	
	function changed_hide(){
		
		jQuery(".spc-shipping .bilingForm").hide();
		jQuery(".checkout-shipping").find("#dwfrm_billing_billingAddress_addressFields_firstName").removeClass("required");
		jQuery(".checkout-shipping").find("#dwfrm_billing_billingAddress_addressFields_lastName").removeClass("required");
		jQuery(".checkout-shipping").find("#dwfrm_billing_billingAddress_addressFields_address1").removeClass("required");
		jQuery(".checkout-shipping").find("#dwfrm_billing_billingAddress_addressFields_address2").removeClass("required");
		jQuery(".checkout-shipping").find("#dwfrm_billing_billingAddress_addressFields_city").removeClass("required");
		jQuery(".checkout-shipping").find("#dwfrm_billing_billingAddress_addressFields_country").removeClass("required");
		jQuery(".checkout-shipping").find("#dwfrm_billing_billingAddress_addressFields_postal").removeClass("required");
		jQuery(".checkout-shipping").find("#dwfrm_billing_billingAddress_addressFields_phone").removeClass("required");
		copyShippindAddrToBillingAddr();
	}
	

	function changed_show(){
        jQuery(".spc-shipping .bilingForm").show();
        if($('.spc-shipping .bilingForm select[id$="_country"]').length > 0 && $('.spc-shipping .bilingForm select[id$="_country"]').val() == 'GB' && $('.spc-shipping .bilingForm [name$="_state"][id$="_state"]').length > 0){
        	$('.spc-shipping .bilingForm [name$="_state"][id$="_state"]').closest(".form-row").find(".required-indicator").remove();
        	$('.spc-shipping .bilingForm [name$="_state"][id$="_state"]').closest(".form-row").hide();
        }else{
        	if($('.spc-shipping .bilingForm [name$="_state"][id$="_state"]').closest(".form-row").find(".required-indicator").length > 0){
        		$('.spc-shipping .bilingForm [name$="_state"][id$="_state"]').closest(".form-row").find(".required-indicator").remove();
        	}
        }
        $('.spc-shipping .bilingForm [name$="_state"][id$="_state"]').closest(".form-row").removeClass("required");
		/*jQuery(".checkout-shipping").find("#dwfrm_billing_billingAddress_addressFields_firstName").addClass("required");
		jQuery(".checkout-shipping").find("#dwfrm_billing_billingAddress_addressFields_lastName").addClass("required");
		jQuery(".checkout-shipping").find("#dwfrm_billing_billingAddress_addressFields_address1").addClass("required");
		jQuery(".checkout-shipping").find("#dwfrm_billing_billingAddress_addressFields_city").addClass("required");
		jQuery(".checkout-shipping").find("#dwfrm_billing_billingAddress_addressFields_country").addClass("required");
		//jQuery(".checkout-shipping").find("#dwfrm_billing_billingAddress_addressFields_postal").addClass("required");
		jQuery(".checkout-shipping").find("#dwfrm_billing_billingAddress_addressFields_phone").addClass("required");*/
		//removing class "postal" for removing the validation of postal code
		//jQuery(".checkout-shipping").find("#dwfrm_billing_billingAddress_addressFields_postal").removeClass("postal");
	}
	
	function copyShippindAddrToBillingAddr(){
		//Copy shipping address data to billing address
		jQuery("input[name=dwfrm_billing_billingAddress_addressFields_firstName]")
			.val(jQuery("input[name=dwfrm_singleshipping_shippingAddress_addressFields_firstName]").val());
		jQuery("input[name=dwfrm_billing_billingAddress_addressFields_lastName]")
			.val(jQuery("input[name=dwfrm_singleshipping_shippingAddress_addressFields_lastName]").val());
		jQuery("input[name=dwfrm_billing_billingAddress_addressFields_address1]")
			.val(jQuery("input[name=dwfrm_singleshipping_shippingAddress_addressFields_address1]").val());
		jQuery("input[name=dwfrm_billing_billingAddress_addressFields_address2]")
			.val(jQuery("input[name=dwfrm_singleshipping_shippingAddress_addressFields_address2]").val());
		jQuery("input[name=dwfrm_billing_billingAddress_addressFields_city]")
			.val(jQuery("input[name=dwfrm_singleshipping_shippingAddress_addressFields_city]").val());
		jQuery("input[name=dwfrm_billing_billingAddress_addressFields_postal]")
			.val(jQuery("input[name=dwfrm_singleshipping_shippingAddress_addressFields_postal]").val());
		if(jQuery("select[name=dwfrm_singleshipping_shippingAddress_addressFields_states_state]").length > 0){
			jQuery("select[name=dwfrm_billing_billingAddress_addressFields_states_state]")
			.val(jQuery("select[name=dwfrm_singleshipping_shippingAddress_addressFields_states_state]").val());
		}else if(jQuery("input[name=dwfrm_singleshipping_shippingAddress_addressFields_state]").length > 0){
			jQuery("input[name=dwfrm_billing_billingAddress_addressFields_state]")
			.val(jQuery("input[name=dwfrm_singleshipping_shippingAddress_addressFields_state]").val());
		}
		jQuery("select[name=dwfrm_billing_billingAddress_addressFields_country]")
			.val(jQuery("select[name=dwfrm_singleshipping_shippingAddress_addressFields_country]").val());
		jQuery("input[name=dwfrm_billing_billingAddress_addressFields_phone]")
			.val(jQuery("input[name=dwfrm_singleshipping_shippingAddress_addressFields_phone]").val());
	}
	
	
	$cache.primary.on('click', '.checkout-shipping .shippingForm #previous-addresses-link', function(e) {
		$(this).hide();
		//$cache.primary.find(".checkout-shipping .shippingForm #enter-address-manually").hide();
		$cache.primary.find(".checkout-shipping .shippingForm .PCA-address-search").hide();
		$cache.primary.find(".checkout-shipping .shippingForm #search-for-addresses").show();
		$cache.primary.find(".checkout-shipping .shippingForm .reg-address-list").show();
		//enebleFields();
	});
	
	$cache.primary.on('click', '.checkout-shipping .shippingForm #search-for-addresses', function(e) {
		$(this).hide();
		$cache.primary.find(".checkout-shipping .shippingForm .reg-address-list").hide();
		//$cache.primary.find(".checkout-shipping .shippingForm #enter-address-manually").show();
		$cache.primary.find(".checkout-shipping .shippingForm .PCA-address-search").show();
		$cache.primary.find('.checkout-shipping .shippingForm #previous-addresses-link').show();
		//disableFields();
	});
	
	$cache.primary.on('click', '.clickandcollect .click-and-collect-form #previous-addresses-link', function(e) {
        $(this).hide();
        $cache.primary.find(".clickandcollect .click-and-collect-form .PCA-address-search").hide();
        $cache.primary.find(".clickandcollect .click-and-collect-form #search-for-addresses").show();
        $cache.primary.find(".clickandcollect .click-and-collect-form .select-address").show();
        //enebleBillingFields();
	});
	
	$cache.primary.on('click', '.clickandcollect .click-and-collect-form #search-for-addresses', function(e) {
        $(this).hide();
        $cache.primary.find(".clickandcollect .click-and-collect-form .select-address").hide();
        $cache.primary.find(".clickandcollect .click-and-collect-form #previous-addresses-link").show();
        $cache.primary.find(".clickandcollect .click-and-collect-form .PCA-address-search").show();
        //enebleBillingFields();
	});
	

	function initBilling(){
		$cache.primary.on('click', '.checkout-shipping .bilingForm #previous-addresses-link', function(e) {
			$(this).hide();
			//$cache.primary.find(".checkout-shipping .bilingForm #enter-address-manually").hide();
			$cache.primary.find(".checkout-shipping .bilingForm .PCA-address-search").hide();
			$cache.primary.find(".checkout-shipping .bilingForm #search-for-addresses").show();
			$cache.primary.find(".checkout-shipping .bilingForm .select-address").show();
			//enebleBillingFields();
		});
		
		$cache.primary.on('click', '.checkout-shipping .bilingForm #search-for-addresses', function(e) {
			$(this).hide();
			$cache.primary.find(".checkout-shipping .bilingForm .select-address").hide();
			//$cache.primary.find(".checkout-shipping .bilingForm #enter-address-manually").show();
			$cache.primary.find(".checkout-shipping .bilingForm .PCA-address-search").show();
			$cache.primary.find('.checkout-shipping .bilingForm #previous-addresses-link').show();
			//disableBillingFields();
		});
		
	}
	
	 var currentSite = $("#wrapper .country-select").find("select[id$='_destination'] option[selected=selected]").val();
	 var currentCountryCode = $('.checkout-shipping #dwfrm_singleshipping_shippingAddress_addressFields_country').val();
	 var BasketValue = $(".BasketValue").val();
	 var countryCode;
	 if($('.checkout-shipping #dwfrm_singleshipping_shippingAddress_addressFields_country').val() == "GB"){
		 countryCode = "UK";
	 }else{
		 countryCode = $('.checkout-shipping #dwfrm_singleshipping_shippingAddress_addressFields_country').val();
	 }
	 if((countryCode != undefined) && (currentSite != getSite(countryCode))){
		 if(currentSite == "UK"){
			 currentCountryCode = "GB";
			 if(BasketValue != "true"){
				 showDeliveryCountryChangePopup(countryCode,"GB");
			 }
		 }else if(currentSite == "ROW"){
			 currentCountryCode = "";
			 showDeliveryCountryChangePopup(countryCode,"");
		 }else{
			 currentCountryCode = currentSite;
			 showDeliveryCountryChangePopup(countryCode,currentSite);
		 }
	 }
	 $cache.primary.on('change', '.checkout-shipping #dwfrm_singleshipping_shippingAddress_addressFields_country', function(e) {
		 e.preventDefault();
		 var countryCode;
		 var currentSite = $("#wrapper .country-select").find("select[id$='_destination'] option[selected=selected]").val();
		 if($('#dwfrm_singleshipping_shippingAddress_addressFields_countrySelectBoxIt').text().toUpperCase()=="NORWAY"){
		    $('.personalidentificationnumber').closest('.form-row').show();
		 }else{
		    $('.personalidentificationnumber').closest('.form-row').hide();
		 }
		 if($(this).val() == "GB"){
			 countryCode = "UK";
		 }else{
			 countryCode = $(this).val();
		 }
		 if(currentSite != getSite(countryCode)){
			 showDeliveryCountryChangePopup(countryCode,currentCountryCode);
		 }
	 });
	//this is for Checkout - Highlighting missing fields GB-570 
	 /*disables continue button until all the mandatory fields are entered
	 formPrepare.init({
		continueSelector: '[name$="shippingAddress_save"]',
		formSelector:'[id$="singleshipping_shippingAddress"]'
	});*/
	 
	 if(SitePreferences.PCAENABLED){
		 var presentSite = $("#wrapper .country-select").find("select[id$='_destination'] option[selected=selected]").val();
		 $cache.primary.on('keyup', '.checkout-shipping #dwfrm_singleshipping_postalcodeshipping,.click-and-collect-form #dwfrm_singleshipping_postalcodebilling', function(e) {
			 if($(this).val().length > 1){
				 var pcaSearchCountryCode = SitePreferences.PCASEARCHCOUNTRYCODE;
				 if(presentSite == 'US' && SitePreferences.PCASEARCHCOUNTRYCODE.split('|') != undefined){
					 pcaSearchCountryCode = SitePreferences.PCASEARCHCOUNTRYCODE.split('|')[0];
				 }else if(presentSite == 'CA' && SitePreferences.PCASEARCHCOUNTRYCODE.split('|') != undefined){
					 pcaSearchCountryCode = SitePreferences.PCASEARCHCOUNTRYCODE.split('|')[1];
				 }
				 getSuggestions(SitePreferences.PCAKEY, $(this).val(), pcaSearchCountryCode, $(this));
			 }
		 });
		 $cache.primary.on('keyup', '.checkout-shipping #dwfrm_singleshipping_postalcodebilling,.click-and-collect-form #dwfrm_singleshipping_postalcodebilling', function(e) {
			 if($(this).val().length > 1){
				 var pcaSearchCountryCode = SitePreferences.PCASEARCHCOUNTRYCODE;
				 if(presentSite == 'US' && SitePreferences.PCASEARCHCOUNTRYCODE.split('|') != undefined){
					 pcaSearchCountryCode = SitePreferences.PCASEARCHCOUNTRYCODE.split('|')[0];
				 }else if(presentSite == 'CA' && SitePreferences.PCASEARCHCOUNTRYCODE.split('|') != undefined){
					 pcaSearchCountryCode = SitePreferences.PCASEARCHCOUNTRYCODE.split('|')[1];
				 }
				 getSuggestions(SitePreferences.PCAKEY, $(this).val(), pcaSearchCountryCode, $(this));
			 }
		 });
		 var getSuggestions = function(Key, SearchTerm, Country, currentElem) {
			    $.getJSON(SitePreferences.PCARETRIEVESUGGESTIONURL+"?callback=?",
			    {
			        Key: Key,
			        SearchTerm: SearchTerm,
			        Country: Country,
			    },
			    function (data) {
			    	$(currentElem).parent().find('.suggestions').remove();
			    	if(data.Items.length > 0){
			    		var suggestion = '';
			    		$(data.Items).each(function(i, item){
			    			if(item['Next'] == "Retrieve"){
			    				suggestion = suggestion + "<li><a addrID="+ item['Id'] +">"+ item['Text'] + "</a></li>";
			    			}
			    		});
			    		if(suggestion != ''){
				    		suggestion = "<ul class='suggestions'>" + suggestion + "</ul>";
				    		$(currentElem).after(suggestion);
			    		}
			    	}else if(data.Items.length == 0){
			    		var suggestion = "<ul class='suggestions'>Sorry, there were no results</ul>";
			    		$(currentElem).after(suggestion);
			    	}
			    	if($(currentElem).parent().find('.suggestions').length > 0 && $(currentElem).parent().find('.suggestions').find('li').length > 0){
			    		$(currentElem).parent().find('.suggestions').find('li a').on('click', function(e){
			    			var $this = $(this);
			    			var parent = $(currentElem).closest('fieldset');
			    			var addrId = $this.attr("addrid");
			    			$.getJSON(SitePreferences.PCARETRIEVEADDRESSURL+"?Key="+ SitePreferences.PCAKEY + "&Id=" +addrId,
	    				    function(data){
	    				    	var response = data;
	    				    	if(response.Items.length > 0 && response.Items[0] != undefined){
	    				    		var addr = response.Items[0];
	    				    		if(addr['Line1'] != undefined && addr['Line1'] != ''){
	    				    			if(parent.find("input[name$='_address1']").length > 0){
	    				    				var company = (addr['Company'] != undefined && addr['Company'] != '')? addr['Company']+"," : '';
		    				    				parent.find("input[name$='_address1']").val(company+addr['Line1']);
	    				    			}
	    				    		}else{
	    				    			if(parent.find("input[name$='_address1']").length > 0){
	    				    				parent.find("input[name$='_address1']").val('');
	    				    			}
	    				    		}
	    				    		if(addr['Line2'] != undefined && addr['Line2'] != ''){
	    				    			if(parent.find("input[name$='_address2']").length > 0){
	    				    				parent.find("input[name$='_address2']").val(addr['Line2']);
	    				    			}
	    				    		}else{
	    				    			if(parent.find("input[name$='_address2']").length > 0){
	    				    				parent.find("input[name$='_address2']").val('');
	    				    			}
	    				    		}
	    				    		if(addr['Province'] != undefined && addr['Province'] != ''){
	    				    			if(parent.find("input[name$='_city']").length > 0){
	    				    				parent.find("input[name$='_city']").val(addr['Province']);
	    				    			}
	    				    		}else{
	    				    			if(parent.find("input[name$='_city']").length > 0){
	    				    				parent.find("input[name$='_city']").val('');
	    				    			}
	    				    		}
	    				    		if(addr['PostalCode'] != undefined && addr['PostalCode'] != ''){
	    				    			if(parent.find("input[name$='_postal']").length > 0){
	    				    				parent.find("input[name$='_postal']").val(addr['PostalCode']);
	    				    			}
	    				    		}else{
	    				    			if(parent.find("input[name$='_postal']").length > 0){
	    				    				parent.find("input[name$='_postal']").val('');
	    				    			}
	    				    		}
 	    				    		shipping.updateShippingMethodList();//added for GBSS-22
	    				    		if(addr['CountryIso2'] != undefined && addr['CountryIso2'] != ''){
	    				    			if(parent.find("input[name$='_country']").length > 0){
	    				    				parent.find("input[name$='_country']").val(addr['CountryIso2']);
	    				    			}
	    				    		}
	    				    		if(addr['City'] != undefined && addr['City'] != ''){
	    				    			if(parent.find("input[name$='_city']").length > 0){
	    				    				parent.find("input[name$='_city']").val(addr['City']);
	    				    			}
	    				    		}else{
	    				    			if(parent.find("input[name$='_city']").length > 0){
	    				    				parent.find("input[name$='_city']").val('');
	    				    			}
	    				    		}
	    				    		if(addr['ProvinceCode'] != undefined && addr['ProvinceCode'] != ''){
	    				    			if(parent.find("select[name$='_state']").length > 0){
	    				    				parent.find("select[name$='_state']").val(addr['ProvinceCode']);
	    				    				parent.find("select[name$='_state']").trigger('change');
	    				    			}
	    				    		}else{
	    				    			if(parent.find("select[name$='_state']").length > 0){
	    				    				parent.find("select[name$='_state']").val('');
	    				    			}
	    				    		}
	    				    	}
	    				    	$(currentElem).parent().find('.suggestions').hide();
	    				    });
			    		});
			    	}
			    });
		};
	 }
	 //GB-467
	$('#dwfrm_singleshipping_postalcodeshipping').on('keyup input',function (){ 
        var value = $('#dwfrm_singleshipping_postalcodeshipping').val(); 
        if(value != "") { 
            $('.pca-field-reset').show(); 
        }else{ 
                $('.pca-field-reset').hide(); 
                $('.suggestions').hide(); 
        } 
    }); 
    $('.pca-field-reset').on('click',function (){ 
        $('#dwfrm_singleshipping_postalcodeshipping').val(''); 
        $('.pca-field-reset').hide(); 
        $('.suggestions').hide();
        shipping.updateShippingMethodList(); //added for GBSS-22
    });
    
       
    //Selecting geoloction while clicking the "or use current location"
    
    $(".checkout-delivery-lookup-link.js-collect-lookup-geolocate").on("click",function(e){
    	e.preventDefault();
    	var lat ;
    	var lng ;
    	
    	if(navigator.geolocation != null){
    		navigator.geolocation.getCurrentPosition(

        		    // Success callback
        		    function(position) {

        		    	lat = position.coords.latitude;
        		    	lng= position.coords.longitude;
        		        
        		    	var url = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+lat+","+lng+"&key="+SitePreferences.GOOGLE_API_KEY;
        		    	$.ajax({
        					type: 'GET',
        					dataType: 'json',
        					url: url,
        					success: function (response) {
        						if (response.status == "OK") {
        				            if (response.results[0]) {
        				                for (var i = 0; i < response.results[0].address_components.length; i++) {
        				                    var types = response.results[0].address_components[i].types;
        				                    
        				                    for (var typeIdx = 0; typeIdx < types.length; typeIdx++) {
        				                        if (types[typeIdx] == 'postal_code') {
        				                            $("input[name=dwfrm_shiptoshop_postal]").val(response.results[0].address_components[i].short_name);
        				                        }
        				                    }
        				                }
        				            } else {
        				               //do nothing
        				            }
        				        }
        					}
        				});
        		    }
        		);
    	}
    	
    });
    
    //end 
    var currentSiteID = $('input[id=siteID]').val();
    if(currentSiteID == 'GrahamBrownUK' || currentSiteID == 'GrahamBrownNZ' || currentSiteID == 'GrahamBrownFR' || currentSiteID == 'GrahamBrownNL' || currentSiteID == 'GrahamBrownDE'){
    	$cache.primary.off('click','.candc-find').on('click', '.candc-find', function(e){
        	e.preventDefault();
        	if($('#dwfrm_shiptoshop_postal').hasClass('error')){
                $(".responce-data-isml").addClass("empty-result");
        	}else{
                $(".responce-data-isml").removeClass("empty-result");
        	}
        	var form = $(this).closest('form');
        	var address = $('input[id=dwfrm_shiptoshop_address1]').val(),
        		postcode = $('input[id=dwfrm_shiptoshop_postal]').val(),
        		region = $("#dwfrm_shiptoshop_stateSelectBoxItText").attr('data-val'),
        		mobile = $('input[id=dwfrm_shiptoshop_mobile]').val(),
        		fname = $('input[id=fName]').val(),
        		lname = $('input[id=lName]').val(),
        		emailid = $('input[id=emailID]').val();
        	
        	//Whitespaces issue fix for postcode
        	if(currentSiteID == 'GrahamBrownUK' ){
	        	if(postcode.indexOf(' ') >= 0){
	        		postcode = $('input[id=dwfrm_shiptoshop_postal]').val();
	        	}else{
	        		postcode = $('input[id=dwfrm_shiptoshop_postal]').val().replace(/^(.*)(\d)/, "$1 $2");
	        	}
        	}
        	
        	if(!form.valid()){
                return false;
        	}
        	
        	/*if(address == '' || postcode == '' || region == '' || phone == '' ){
        		if($('.errorcheck').length == 0){
        			$('<span class="errorcheck">'+Resources.ERROR_MESSAGE+'</span>').insertAfter('#dwfrm_shiptoshop_postal');
        		}
            	return false;
        	}else{
        		$("span.errorcheck").remove()
        	}*/
        	var url = util.appendParamsToUrl(Urls.shiptoshop, {
                 postcode : postcode,
                 firstname : fname,
                 lastname : lname,
                 email : emailid,
                 address : address,
                 region : region,
                 mobile : mobile,
                 format: "ajax"
             });
        	$.ajax({
            	url: url,
            	type: 'POST',
            	cache : false,
            	success: function(data) {
                	$(".responce-data-isml").html("");
                    $(".responce-data-isml").html(data);
                }
            });
        });    	
    }
}

function loadSPCMultiShipping() {
    $cache.primary.on('click', '.spc-multi-shipping-btn:not(:disabled)', function(e) {
        e.preventDefault();
        var form = $(this).closest('form');
        $('<input type="hidden" name="' + $(this).attr('name') + '" value="true" />').appendTo(form);
        form.validate();
        if (!form.valid()) {
            return false;
        }
        progress.show($cache.primary);
        ajax.load({
            url: util.ajaxUrl(form.attr('action')),
            data: form.serialize(),
            type: 'POST',
            callback: function(data) {
            	if(handleBasketError(data)){
	                $cache.primary.html(data);
	                updateSummary();
	                validator.init();
	                initSPC();
	                tooltip.init();
	                util.limitCharacters();
	                util.selectbox();
	                checkout.init();
	                progress.hide();
	                updateAnchor();
            	}
            }
        });
    });
     
    $cache.primary.on('click', '.shiptosinglebutton:not(:disabled)', function(e) {
        e.preventDefault();
        var form = $(this).closest('form');
         
        var url = util.appendParamToURL(form.attr('action'), $(this).attr('name'), true);
        url = util.appendParamToURL(url, form.find("input[name$=_securekey]").attr("name"), form.find("input[name$=_securekey]").attr("value"));
        progress.show($cache.primary);
        ajax.load({
            url: url,	   
            type: 'POST',
            callback: function(data) {
            	if(handleBasketError(data)){
	                $cache.primary.html(data);
	                updateSummary();
	                validator.init();
	                initSPC();
	                tooltip.init();
	                util.limitCharacters();
	                util.selectbox();
	                checkout.init();
	                progress.hide();
	                updateAnchor();
            	}
            }
        });
    });
    
    $cache.primary.on("click", '.item-shipping-address a', function(e) {
        e.preventDefault();
        progress.show($cache.primary);
        ajax.load({
            url: $(this).attr('href'),
            type: 'POST',
            callback: function(data) {
            	if(handleBasketError(data)){
	                $cache.primary.html(data);
	                validator.init();
	                initSPC();
	                tooltip.init();
	                util.limitCharacters();
	                util.selectbox();
	                checkout.init();
	                progress.hide();
	                updateAnchor();
            	}
            }
        });
    });
    
}

function loadSPCBilling() {
	
	$cache.ccContainer = $("#PaymentMethod_CREDIT_CARD");
	$cache.ccList = $("#creditCardList");
	$cache.ccOwner = $cache.ccContainer.find("input[name$='creditCard_owner']");
	$cache.ccType = $cache.ccContainer.find("select[name$='_type']");
	$cache.ccNum = $cache.ccContainer.find("input[name$='_number']");
	$cache.ccMonth = $cache.ccContainer.find("[name$='_month']");
	$cache.ccYear = $cache.ccContainer.find("[name$='_year']");
	$cache.ccCcv = $cache.ccContainer.find("input[name$='_cvn']");
	
	if($cache.ccType != null && $cache.ccNum != null){
		cardtype($cache.ccNum, $cache.ccType);
	}
	
	$cache.primary.on('click', '.spc-billing-btn:not(:disabled)', function(e) {
        if($('input[name$=_selectedPaymentMethodID]:checked').val() == 'PayPal'){
        	$(this).removeClass('spc-billing-btn');
        	return true;
        }
    	e.preventDefault();
        var form = $(this).closest('form');
        form.validate();
        if (!form.valid()) {
            return false;
        }
        progress.show($cache.primary);
        ajax.load({
            url: util.ajaxUrl(form.attr('action')),
            data: form.serialize(),
            type: 'POST',
            callback: function(data) {
            	if(handleBasketError(data)){
	                $cache.primary.html(data);
	                updateSummary();
	                if ($cache.primary.find('.spc-summary').length == 0) {
	                    tooltip.init();
	                    util.limitCharacters();
	                    util.selectbox();
	                    validator.init();
	                    checkout.init();
	                }
	                initSPC();
	                progress.hide();
	                if ($cache.primary.find('.spc-billing').length == 0) {
	                	updateAnchor();
	                }else{
	                	updateAnchor(true);
	                }
            	}
            }
        });
    });
    
	$cache.primary.on('click', '.redemption.giftcert a.remove', function(e){
    	 e.preventDefault();
    	 progress.show($cache.primary);
    	 var url = $(this).attr("href");
         ajax.load({
             url: url,
             data: {},
             type: 'POST',
             callback: function(data) {
            	 if(handleBasketError(data)){
	                 $cache.primary.html(data);
	                 updateSummary();
	                 validator.init();
	                 initSPC();
	                 tooltip.init();
	                 util.limitCharacters();
	                 util.selectbox();
	                 checkout.init();
	                 progress.hide();
            	 }
             }
         });
    });
	
	$('#dwfrm_billing_paymentMethods_creditCard_number').keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
		// changes made for GBSS-16
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190 ,229]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
             // Allow: Ctrl+C
            (e.keyCode == 67 && e.ctrlKey === true) ||
             // Allow: Ctrl+X
            (e.keyCode == 88 && e.ctrlKey === true) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
	//changes made for GBSS-16
	$('#dwfrm_billing_paymentMethods_creditCard_number').keyup(function (e) {
		var text =$("#dwfrm_billing_paymentMethods_creditCard_number")
		var regex = /[^\d]/g;
		var txt = $(text).val();      
		if(regex.test(txt)){
		   $(text).val(txt.replace(regex, ''));
		   return false;
		}else{
			return true;
		}
	});
	$(document).ready(function(){
	    $("#dwfrm_billing_paymentMethods_creditCard_number").on("keyup keypress keydown paste", function(){   
	      var a =  $("#dwfrm_billing_paymentMethods_creditCard_number").val().length;
	        if(a > 16){
	           $("#dwfrm_billing_paymentMethods_creditCard_number").val($("#dwfrm_billing_paymentMethods_creditCard_number").val().substr(0,16));
	        }
	    });
	});
	
	if($(".spc-billing .checkout-billing").find("input[name='Error']").length > 0 && $(".spc-billing .checkout-billing").find("input[name='Error']").val() == "true"){
		var data = "<div class='paymentfail'><h1>"+ Resources.PAYNENTERRORNOTIFICATIONTITLE +"</h1> <p>"+ Resources.PAYNENTERRORNOTIFICATIONTEXT +"</p><button type='buttont' class='prm-btn' id='paymentfailagree'>"+ Resources.PAYNENTERRORNOTIFICATIONBUTTON +"</button></div>";
		dialog.open({
			html:data,
			options: {
				dialogClass : 'payment-fail custom-popup',
				width : 390
			},
			callback: function () {
				$(".payment-fail .paymentfail #paymentfailagree").click(function(e){
					e.preventDefault();
					dialog.close();
				});
			}
		});
	}
	
	//GB-466
	$(".checkout-notice-link").click(function(e){
		e.preventDefault();
		var url = $(this).attr("href");
		dialog.open({
			url: url,
			options: {
				dialogClass : 'checkout_popup_links'
			}
		});
	});
}
//this is for Checkout - Highlighting missing fields GB-570 
$(document).on('click','.payformcheck', function(){
	var form = $(this).closest('form');
	if(!form.valid()){
		if(form.find("input.error").length > 0){
    		form.find("input.error").first().focus();
    	}
		return false;
	}
});
//Payment method validation
function cardtype  ($cardinput, $typeselector) {
	var $con = $($cardinput).not(':hidden').closest('form');
	if ($($cardinput).length > 0 && $con != undefined) {
		$con.find($cardinput).focus(function(){
			if($(this).val() != "" && $(this).hasClass('tooltipstered')){
				$(this).tooltipster('destroy');
			}
		}).blur(function () {
			var val = $.trim($(this).val());
			var regex = /^[a-zA-Z]+$/;
			var errorspan = $(this).closest('.form-row').find('span.error');
			var $error = false;

			if ($(this).closest('.form-row').find('.cardtypeimg').length == 0) {
				$(this).closest('.form-row').append('<span class="cardtypeimg"><span style="display:none;"></span></span>');
			}

			if (val.length > 0 && val.indexOf('*') == -1) {

				var cardTypeval = validatecardtype(val);

				if (cardTypeval != "Error") {
					$($typeselector).find('option[label="' + cardTypeval + '"]').prop('selected', true);
					$(this).closest('.form-row').find('.cardtypeimg > span').attr('data-cardlabel', cardTypeval).show();

					if ((cardTypeval == "MasterCard") || (cardTypeval == "Visa") || (cardTypeval == "Discover") || (cardTypeval == "Maestro")) {
						if (val.length < 16 && cardTypeval != "Visa") {
							$error = true;
						}
						if (val.length != 13 && val.length != 16 && cardTypeval == "Visa") {
							$error = true;
						}

					} else if (cardTypeval == "Discover") {
						if (val.length < 15) {
							$error = true;
						}
					} else {
						$error = false;
					}
				} else {
					$error = true;
					$($typeselector).find('option').eq(0).prop('selected', true);
					$(this).closest('.form-row').find('.cardtypeimg > span').removeAttr('data-cardlabel').hide();
				}

			}else if(val.indexOf('*') == -1){
				$error = true;
			}

			if ($con.find($cardinput).val().length < 4) {
				$(this).closest('.form-row').find('.cardtypeimg > span').hide();
			}
		});

		$con.find($cardinput).each(function(){
			var $val = $(this).val();
			if ($(this).closest('.form-row').find('.cardtypeimg').length == 0) {
				$(this).closest('.form-row').append('<span class="cardtypeimg"><span style="display:none;"></span></span>');
			}

			if($(this).closest('form').attr('data-cardType') != undefined){
				$(this).closest('.form-row').find('.cardtypeimg > span').attr('data-cardlabel', $(this).closest('form').attr('data-cardType')).show();
			}else if (validatecardtype($val) != "Error" && $(this).val() != "") {
				$(this).closest('.form-row').find('.cardtypeimg > span').attr('data-cardlabel', validatecardtype($val)).show();
			}

			if($val != '' && $(this).hasClass('creditcard_masked')){
				$(this).attr('cardval', $val);
				$(this).val('************'+ $val.substr($val.length - 4, 4));
				$(this).removeClass('creditcard_masked');
			}
		});

		$con.on('click','#applyBtn', function(e){
			var $type = "Error";
			if($con.find($cardinput).attr('readonly') == undefined){
				if($con.find($cardinput).val() != '' && validatecardtype($con.find($cardinput).val().indexOf('*') != -1 ? $con.find($cardinput).attr('cardval') : $con.find($cardinput).val()) == "Error"){
					e.preventDefault();
					showcartooltip($con.find($cardinput));
					return false;
				}else if($con.find($cardinput).val().indexOf('*') != -1 && validatecardtype($con.find($cardinput).attr('cardval')) != "Error"){
					$con.find($cardinput).val($con.find($cardinput).attr('cardval'));
				}
			}else{
				if($con.find($cardinput).val().indexOf('*') != -1){
					$con.find($cardinput).val($con.find($cardinput).attr('cardval'));
				}
			}

		});

		$con.on('click','.cancel-button', function(e){
			if($con.find($cardinput).hasClass('tooltipstered')){
				$con.find($cardinput).tooltipster('hide');
			}
		});
	}
}
$(window).on("load", function() {
	if($('input[id=siteid]').val() == "GrahamBrownUS"){
		$('input[id^=dwfrm_login_addtolist]').prop('checked', 'checked');
		$('#confsubscribe').trigger("click");
	}
});

//GB-680
$('#confsubscribe').click(function() {
	if($('input[id^=dwfrm_login_addtolist]').is(':checked')){
		redeye.emailSignUp($('input[id=customer-email]').val());
		if($('input[id=siteid]').val() != "GrahamBrownUS"){
			$('#confsubscribe-div').replaceWith("<h3>"+Resources.THANK_YOU_SUBSCRIBE+"</h3>");
		}
	}
});

function validatecardtype(val) {
	var cardregex = {
		mastercard: /^5[1-5][0-9]{2,14}$/,
		visa: /^4[0-9]{3,15}$/,
		amex: /^3[47]([0-9]{2,13})$/,
		discover: /^6(?:011[0-9]{0,12}|5[0-9]{2,14})$/,
		maestro: /^(?:5[0678]\d\d|6304|6390|67\d\d)\d{8,15}$/

	}
	var result = " ",
		carNo = val;

	// first check for MasterCard
	if (cardregex.mastercard.test(carNo)) {
		result = "MasterCard";
	}
	// then check for Visa
	else if (cardregex.visa.test(carNo)) {
		result = "Visa";
	}
	// then check for AmEx
	else if (cardregex.amex.test(carNo)) {
		result = "American Express";
	} else if (cardregex.discover.test(carNo)) {
		result = "Discover";
	} 
	 else if (cardregex.maestro.test(carNo)) {
		result = "Maestro";
	}
	else {
		result = "Error"
	}
	return result;
}

function updateSummary() {
    var url = Urls.summaryRefreshURL;
    var summary = $('#secondary.summary');
    summary.load(url, function() {
        summary.find('.checkout-mini-cart .minishipment .header a').hide();
        summary.find('.order-totals-table .order-shipping .label a').hide();
        util.miniSummaryArrow();
    });
}

function updateAnchor(noDefault) {
    var erroredElements = $cache.primary.find('.form-row.error:visible').filter(function(i, ele) {
        return $.trim($(ele).html()).length > 0
    });

    if (erroredElements.length > 0) {
        jQuery('html, body').animate({
            scrollTop: erroredElements.first().position().top
        }, 500);

    } else if($('.billing-error').length > 0){
    	jQuery('html, body').animate({
            scrollTop: $('.billing-error').position().top
        }, 500);
    }
    else if(!noDefault){
    	 if($('#navigation').length > 0){
			 jQuery('html, body').animate({
		            scrollTop: $('#navigation').position().top
	         }, 500);
		 }
    }
}

//while click on edit in shippingpage reating the click and collect data
function retainStoreDetails(data){
	$('.clickandcollect-text').trigger("click");
    $('.candc-find').trigger("click");
    setTimeout(function(){ 
    	var storeName = $(".mini-shipment.order-component-block .details .address .shopAddress").html().trim();
    	var $storesList = $(".suggested-stores.custmradio");
    	var storeSelection;
    	$storesList.each(function () {
    		if($(this).find(".checkout-delivery-locations-name").text().trim() == storeName){
    			$(this).find('input[type="radio"]').prop("checked",true);
    			$(this).closest(".suggested-stores.custmradio").addClass("selected");
    			$('.pickup-stores').animate({scrollTop: 0},0);
    			var storesOffset = $('.suggested-stores.selected').offset().top - $('.pickup-stores').offset().top;
	               $('.pickup-stores').animate({
	                     scrollTop: storesOffset
	                 },0);
    		}
		});
	}, 7000);
}
function handleBasketError(data) {
    if ($(data).find('.cart-empty').length > 0 || $(data).find("#header").length > 0) {
        window.location.href = Urls.cartShow;
        return false;
    }
    return true;
}

function updateCustomerInfo() {
	ajax.load({
        url: Urls.customerInfo,
        type: 'POST',
        async: false,
        callback: function(data) {
        	$('#navigation').find(".menu-utility-user").find(".user-info").remove();
        	$(data).insertAfter("#navigation .menu-utility-user li:first");
        }
    });
}

function initSPC(){
	initializeCache();
	initializeDom();
	initializeEvents();
}

function showDeliveryCountryChangePopup(countryCode,currentCountryCode){
	 var destination = countryCode;
	 var locale;
	 if(window.countries[countryCode] != undefined && window.countries[countryCode] != null){
		 locale = window.countries[countryCode].language[0].ID;
	 }else{
		 locale = window.countries['ROW'].language[0].ID;
		 destination = "ROW";
	 }
	 var data = "<div class='changeDeliveryCountry'><h1>"+ Resources.CHANGEDELIVERYCOUNTRYTITLE +"</h1> <span>"+ Resources.CHANGEDELIVERYCOUNTRYDESC +"</span><p>"+ Resources.CHANGEDELIVERYCOUNTRYCONFIRM +"</p><button type='buttont' class='sec-btn' id='changeNo'>"+ Resources.CHANGEDELIVERYCOUNTRYNO +"</button><button type='buttont' class='sec-btn' id='changeYes'>"+ Resources.CHANGEDELIVERYCOUNTRYYES +"</button></div>";
	 var $formshipping = $('.spc-shipping .shippingForm');
	 dialog.open({
		html:data,
		options: {
			dialogClass : 'change-delivery custom-popup info',
			width : 390,
            beforeClose: function () {
                $('.checkout-shipping #dwfrm_singleshipping_shippingAddress_addressFields_country').val(currentCountryCode);
                 if($("#wrapper .country-select").find("select[id$='_destination'] option[selected=selected]").val() == 'US' || $("#wrapper .country-select").find("select[id$='_destination'] option[selected=selected]").val() == 'CA'){
                	 updateStateOptionsshipping($formshipping);
                 }
                 $('select.country').data('selectBox-selectBoxIt').destroy();
                 $('select.country').selectBoxIt();
            }
		},
		callback: function () {
			$(".changeDeliveryCountry #changeNo").click(function(e){
				e.preventDefault();
				dialog.close();
				$('.checkout-shipping #dwfrm_singleshipping_shippingAddress_addressFields_country').val(currentCountryCode);
				
				if($("#wrapper .country-select").find("select[id$='_destination'] option[selected=selected]").val() == 'US' || $("#wrapper .country-select").find("select[id$='_destination'] option[selected=selected]").val() == 'CA'){
					updateStateOptionsshipping($formshipping);
                }
				
				$('select.country').data('selectBox-selectBoxIt').destroy();
				$('select.country').selectBoxIt();
			});
			$(".changeDeliveryCountry #changeYes").click(function(e){
				e.preventDefault();
				var url = Urls.changeSite;
				url = util.appendParamToURL(url, 'destination', destination);
				url = util.appendParamToURL(url, 'language', locale);
				url = util.appendParamToURL(url, 'format', 'ajax');
				$.ajax({
					type: 'GET',
					dataType: 'json',
					url: url,
					success: function (response) {
						if(response.success){
							dialog.close();
							window.location.href = response.redirectURL;
						}
					}
				});
			});
		}
	});
	//Code Changes for the GB-752
	if($('#dwfrm_singleshipping_shippingAddress_addressFields_countrySelectBoxIt').text().toUpperCase()=="NORWAY"){
	    $('.personalidentificationnumber').closest('.form-row').show();
	  }else{
		  $('.personalidentificationnumber').closest('.form-row').hide();
	  }
}

function updateStateOptionsshipping (formshipping) {
    var $form = $(formshipping);
    if($form.find('[name$="_state"][id$="_state"]').length > 0 && !$form.find('[name$="_state"][id$="_state"]').is(":visible")){
           $form.find('[name$="_state"][id$="_state"]').closest(".form-row").show();
    }
    $form.find('label[for$="_state"]').removeClass('error');
    $form.find('input[name$="_state"]').removeClass('error');
    $form.find('span[id$="_state-error"]').remove();
    var $country = $form.find('select[id$="_country"]'),
        site = $country.val(),
        country = Countries[$country.val()];
   if(site == 'CA' || site == 'US')
       {
       var arrHtml = [],
       $stateField = $country.data('stateField') ? $country.data('stateField') : $form.find('select[name$="_state"]');
       //$stateLabel = ($stateField.length > 0) ? $form.find('label[for="' + $stateField[0].id + '"] span').not('.required-indicator') : undefined;
           /*if ($stateLabel) {
                $stateLabel.html(country.regionLabel);
           } else {
                return;
           }*/
           var s;
           arrHtml.push('<option value="">Select</option>');
           for (s in country.regions) {
               arrHtml.push('<option value="' + s + '">' + country.regions[s] + '</option>');
            }
           $form.find('select[name$="_state"]').html(arrHtml.join(""));
           if($form.find('#stateShipValue').val() != undefined && $form.find('#stateShipValue').val() != ''){
         	  $form.find('select[name$="_state"]').val($form.find('#stateShipValue').val());
           }
           $form.find('label[for$="_state"]').removeClass('error');
           $form.find('select[name$="_state"]').removeClass('error');
           $form.find('span[id$="_state-error"]').remove();
           setTimeout(function(){ 
        	   $form.find('select[name$="_state"]').data('selectBox-selectBoxIt').destroy();
               $form.find('select[name$="_state"]').selectBoxIt();
   			}, 1000);
       }else{
          return;
       }
   }


function getSite(value){
	switch(value){
	    case "UK":
			return "UK";
		case "FR":
			return "FR";
		case "AU":
	    	return "AU";
		case "NZ":
	    	return "NZ";	    	
		case "CA":
			return "CA";
		case "DE":
	    	return "DE";
		case "NL":
			return "NL";
		case "US":
			return "US";
		default:
			return "ROW";
	}
}

exports.init = function () {
    initializeCache();
    initializeDom();
    initializeEvents();
};

},{"./ajax":1,"./dialog":10,"./pages/checkout":23,"./pages/checkout/formPrepare":22,"./pages/checkout/shipping":25,"./progress":40,"./redeyetracking":43,"./tooltip":51,"./util":52,"./validator":53}],48:[function(require,module,exports){
'use strict';

var inventory = require('./');

var cartInventory = {
	setSelectedStore: function (storeId) {
		var $selectedStore = $('.store-tile.' + storeId),
			$lineItem = $('.cart-row[data-uuid="' + this.uuid + '"]'),
			storeAddress = $selectedStore.find('.store-address').html(),
			storeStatus = $selectedStore.find('.store-status').data('status'),
			storeStatusText = $selectedStore.find('.store-status').text();
		this.selectedStore = storeId;

		$lineItem.find('.instore-delivery .selected-store-address')
			.data('storeId', storeId)
			.attr('data-store-id', storeId)
			.html(storeAddress);
		$lineItem.find('.instore-delivery .selected-store-availability')
			.data('status', storeStatus)
			.attr('data-status', storeStatus)
			.text(storeStatusText);
		$lineItem.find('.instore-delivery .delivery-option').removeAttr('disabled').trigger('click');
	},
	cartSelectStore: function (selectedStore) {
		var self = this;
		inventory.getStoresInventory(this.uuid).then(function (stores) {
			inventory.selectStoreDialog({
				stores: stores,
				selectedStoreId: selectedStore,
				selectedStoreText: Resources.SELECTED_STORE,
				continueCallback: function () {},
				selectStoreCallback: self.setSelectedStore.bind(self)
			});
		}).done();
	},
	setDeliveryOption: function (value, storeId) {
		// set loading state
		$('.item-delivery-options')
			.addClass('loading')
			.children().hide();

		var data = {
			plid: this.uuid,
			storepickup: (value === 'store' ? true : false)
		};
		if (value === 'store') {
			data.storepickup = true;
			data.storeid = storeId;
		} else {
			data.storepickup = false;
		}
		$.ajax({
			url: Urls.setStorePickup,
			data: data,
			success: function () {
				// remove loading state
				$('.item-delivery-options')
					.removeClass('loading')
					.children().show();
			}
		});
	},
	init: function () {
		var self = this;
		$('.item-delivery-options .set-preferred-store').on('click', function (e) {
			e.preventDefault();
			self.uuid = $(this).data('uuid');
			var selectedStore = $(this).closest('.instore-delivery').find('.selected-store-address').data('storeId');
			if (!User.zip) {
				inventory.zipPrompt(function () {
					self.cartSelectStore(selectedStore);
				});
			} else {
				self.cartSelectStore(selectedStore);
			}
		});
		$('.item-delivery-options .delivery-option').on('click', function () {
			// reset the uuid
			var selectedStore = $(this).closest('.instore-delivery').find('.selected-store-address').data('storeId');
			self.uuid = $(this).closest('.cart-row').data('uuid');
			self.setDeliveryOption($(this).val(), selectedStore);
		});
	}
};

module.exports = cartInventory;

},{"./":49}],49:[function(require,module,exports){
'use strict';

var _ = require('lodash'),
	dialog = require('../dialog'),
	TPromise = require('promise'),
	util = require('../util');

var newLine = '\n';
var storeTemplate = function (store, selectedStoreId, selectedStoreText) {
	return [
		'<li class="store-tile ' + store.storeId + (store.storeId === selectedStoreId ? ' selected' : '') + '">',
		'	<p class="store-address">',
		'		' + store.address1 + '<br/>',
		'		' + store.city + ', ' + store.stateCode + ' ' + store.postalCode,
		'	</p>',
		'	<p class="store-status" data-status="' + store.statusclass + '">' + store.status + '</p>',
		'	<button class="select-store-button" data-store-id="' + store.storeId + '"' +
		(store.statusclass !== 'store-in-stock' ? 'disabled="disabled"' : '') + '>',
		'		' + (store.storeId === selectedStoreId ? selectedStoreText : Resources.SELECT_STORE),
		'	</button>',
		'</li>'
	].join(newLine);
};

var storeListTemplate = function (stores, selectedStoreId, selectedStoreText) {
	if (stores && stores.length) {
		return [
			'<div class="store-list-container">',
			'<ul class="store-list">',
			_.map(stores, function (store) {
				return storeTemplate(store, selectedStoreId, selectedStoreText);
			}).join(newLine),
			'</ul>',
			'</div>',
			'<div class="store-list-pagination">',
			'</div>'
		].join(newLine);
	} else {
		return '<div class="no-results">' + Resources.INVALID_ZIP + '</div>';
	}
};

var zipPromptTemplate = function () {
	return [
		'<div id="preferred-store-panel">',
		'	<input type="text" id="user-zip" placeholder="' + Resources.ENTER_ZIP + '" name="zipCode"/>',
		'</div>'
	].join(newLine);
};

/**
 * @description test whether zipcode is valid for either US or Canada
 * @return {Boolean} true if the zipcode is valid for either country, false if it's invalid for both
 **/
var validateZipCode = function (zipCode) {
	var regexes = {
		canada: /^[ABCEGHJKLMNPRSTVXY]\d[ABCEGHJKLMNPRSTVWXYZ]( )?\d[ABCEGHJKLMNPRSTVWXYZ]\d$/i,
		usa: /^\d{5}(-\d{4})?$/
	},
		valid = false;
	if (!zipCode) { return; }
	_.each(regexes, function (re) {
		var regexp = new RegExp(re);
		valid = regexp.test(zipCode);
	});
	return valid;
};

var storeinventory = {
	zipPrompt: function (callback) {
		var self = this;
		dialog.open({
			html: zipPromptTemplate(),
			options: {
				title: Resources.STORE_NEAR_YOU,
				width: 500,
				buttons: [{
					text: Resources.SEARCH,
					click: function () {
						var zipCode = $('#user-zip').val();
						if (validateZipCode(zipCode)) {
							self.setUserZip(zipCode);
							if (callback) {
								callback(zipCode);
							}
						}
					}
				}],
				open: function () {
					$('#user-zip').on('keypress', function (e) {
						if (e.which === 13) {
							// trigger the search button
							$('.ui-dialog-buttonset .ui-button').trigger('click');
						}
					});
				}
			}
		});
	},
	getStoresInventory: function (pid) {
		return TPromise.resolve($.ajax({
			url: util.appendParamsToUrl(Urls.storesInventory, {
				pid: pid,
				zipCode: User.zip
			}),
			dataType: 'json'
		}));
	},
	/**
	 * @description open the dialog to select store
	 * @param {Array} options.stores
	 * @param {String} options.selectedStoreId
	 * @param {String} options.selectedStoreText
	 * @param {Function} options.continueCallback
	 * @param {Function} options.selectStoreCallback
	 **/
	selectStoreDialog: function (options) {
		var self = this,
			stores = options.stores,
			selectedStoreId = options.selectedStoreId,
			selectedStoreText = options.selectedStoreText,
			storeList = storeListTemplate(stores, selectedStoreId, selectedStoreText);
		dialog.open({
			html: storeList,
			options: {
				title: Resources.SELECT_STORE + ' - ' + User.zip,
				buttons: [{
						text: Resources.CHANGE_LOCATION,
					click: function () {
						self.setUserZip(null);
						// trigger the event to start the process all over again
						$('.set-preferred-store').trigger('click');
					}.bind(this)
				}, {
					text: Resources.CONTINUE,
					click: function () {
						if (options.continueCallback) {
							options.continueCallback(stores);
						}
						dialog.close();
					}
				}],
				open: function () {
					$('.select-store-button').on('click', function (e) {
						e.preventDefault();
						var storeId = $(this).data('storeId');
						// if the store is already selected, don't select again
						if (storeId === selectedStoreId) { return; }
						$('.store-list .store-tile.selected').removeClass('selected')
							.find('.select-store-button').text(Resources.SELECT_STORE);
						$(this).text(selectedStoreText)
							.closest('.store-tile').addClass('selected');
						if (options.selectStoreCallback) {
							options.selectStoreCallback(storeId);
						}
					});
				}
			}
		});
	},
	setUserZip: function (zip) {
		User.zip = zip;
		$.ajax({
			type: 'POST',
			url: Urls.setZipCode,
			data: {
				zipCode: zip
			}
		});
	},
	shippingLoad: function () {
		var $checkoutForm = $('.address');
		$checkoutForm.off('click');
		$checkoutForm.on('click', 'input[name$="_shippingAddress_isGift"]', function () {
			$(this).parent().siblings('.gift-message-text').toggleClass('hidden', $('input[name$="_shippingAddress_isGift"]:checked').val());
		});
	}
};

module.exports = storeinventory;

},{"../dialog":10,"../util":52,"lodash":58,"promise":59}],50:[function(require,module,exports){
'use strict';

var _ = require('lodash'),
	inventory = require('./');

var newLine = '\n';
var pdpStoreTemplate = function (store) {
	return [
		'<li class="store-list-item ' + (store.storeId === User.storeId ? ' selected' : '') + '">',
		'	<div class="store-address">' + store.address1 + ', ' + store.city + ' ' + store.stateCode +
		' ' + store.postalCode + '</div>',
		'	<div class="store-status" data-status="' + store.statusclass + '">' + store.status + '</div>',
		'</li>'
	].join(newLine);
};
var pdpStoresListingTemplate = function (stores) {
	if (stores && stores.length) {
		return [
			'<div class="store-list-pdp-container">',
			(stores.length > 1 ? '	<a class="stores-toggle collapsed" href="#">' + Resources.SEE_MORE + '</a>' : ''),
			'	<ul class="store-list-pdp">',
			_.map(stores, pdpStoreTemplate).join(newLine),
			'	</ul>',
			'</div>'
		].join(newLine);
	}
};

var storesListing = function (stores) {
	// list all stores on PDP page
	if ($('.store-list-pdp-container').length) {
		$('.store-list-pdp-container').remove();
	}
	$('.availability-results').append(pdpStoresListingTemplate(stores));
};

var productInventory = {
	setPreferredStore: function (storeId) {
		User.storeId = storeId;
		$.ajax({
			url: Urls.setPreferredStore,
			type: 'POST',
			data: {storeId: storeId}
		});
	},
	productSelectStore: function () {
		var self = this;
		inventory.getStoresInventory(this.pid).then(function (stores) {
			inventory.selectStoreDialog({
				stores: stores,
				selectedStoreId: User.storeId,
				selectedStoreText: Resources.PREFERRED_STORE,
				continueCallback: storesListing,
				selectStoreCallback: self.setPreferredStore
			});
		}).done();
	},
	init: function () {
		var $availabilityContainer = $('.availability-results'),
			self = this;
		this.pid = $('input[name="pid"]').val();

		$('#product-content .set-preferred-store').on('click', function (e) {
			e.preventDefault();
			if (!User.zip) {
				inventory.zipPrompt(function () {
					self.productSelectStore();
				});
			} else {
				self.productSelectStore();
			}
		});

		if ($availabilityContainer.length) {
			if (User.storeId) {
				inventory.getStoresInventory(this.pid).then(storesListing);
			}

			// See more or less stores in the listing
			$availabilityContainer.on('click', '.stores-toggle', function (e) {
				e.preventDefault();
				$('.store-list-pdp .store-list-item').toggleClass('visible');
				if ($(this).hasClass('collapsed')) {
					$(this).text(Resources.SEE_LESS);
				} else {
					$(this).text(Resources.SEE_MORE);
				}
				$(this).toggleClass('collapsed');
			});
		}
	}
};

module.exports = productInventory;

},{"./":49,"lodash":58}],51:[function(require,module,exports){
'use strict';

/**
 * @function
 * @description Initializes the tooltip-content and layout
 */
exports.init = function () {
	$(document).tooltip({
		items: '.tooltip',
		track: true,
		position: { my: 'left+30 center', at: 'right center' },
		content: function () {
			return $(this).find('.tooltip-content').html();
		}
	});

	$('.share-link').on('click', function (e) {
		e.preventDefault();
		var target = $(this).data('target');
		if (!target) {
			return;
		}
		$(target).toggleClass('active');
	});
	
	/*JIRA PREV-282 : DEV-32: SG issue- 'What is this' link in checkout billing page should not be clickable. Added the folloiwng block.*/
	$("a.tooltip").click(function(e){
		e.preventDefault();
	});
};

},{}],52:[function(require,module,exports){
'use strict';

var _ = require('lodash');

var util = {
	/**
	 * @function
	 * @description appends the parameter with the given name and value to the given url and returns the changed url
	 * @param {String} url the url to which the parameter will be added
	 * @param {String} name the name of the parameter
	 * @param {String} value the value of the parameter
	 */
	appendParamToURL: function (url, name, value) {
		// quit if the param already exists
		if (url.indexOf(name + '=') !== -1) {
			return url;
		}
		var separator = url.indexOf('?') !== -1 ? '&' : '?';
		return url + separator + name + '=' + encodeURIComponent(value);
	},
	/**
	 * @function
	 * @description appends the parameters to the given url and returns the changed url
	 * @param {String} url the url to which the parameters will be added
	 * @param {Object} params
	 */
	appendParamsToUrl: function (url, params) {
		var _url = url;
		_.each(params, function (value, name) {
			_url = this.appendParamToURL(_url, name, value);
		}.bind(this));
		return _url;
	},

	/**
	 * @function
	 * @description remove the parameter and its value from the given url and returns the changed url
	 * @param {String} url the url from which the parameter will be removed
	 * @param {String} name the name of parameter that will be removed from url
	 */
	removeParamFromURL: function (url, name) {
		if (url.indexOf('?') === -1 || url.indexOf(name + '=') === -1) {
			return url;
		}
		var hash;
		var params;
		var domain = url.split('?')[0];
		var paramUrl = url.split('?')[1];
		var newParams = [];
		// if there is a hash at the end, store the hash
		if (paramUrl.indexOf('#') > -1) {
			hash = paramUrl.split('#')[1] || '';
			paramUrl = paramUrl.split('#')[0];
		}
		params = paramUrl.split('&');
		for (var i = 0; i < params.length; i++) {
			// put back param to newParams array if it is not the one to be removed
			if (params[i].split('=')[0] !== name) {
				newParams.push(params[i]);
			}
		}
		return domain + '?' + newParams.join('&') + (hash ? '#' + hash : '');
	},

	/**
	 * @function
	 * @description extract the query string from URL
	 * @param {String} url the url to extra query string from
	 **/
	getQueryString: function (url) {
		var qs;
		if (!_.isString(url)) { return; }
		var a = document.createElement('a');
		a.href = url;
		if (a.search) {
			qs = a.search.substr(1); // remove the leading ?
		}
		return qs;
	},
	/**
	 * @function
	 * @description
	 * @param {String}
	 * @param {String}
	 */
	elementInViewport: function (el, offsetToTop) {
		var top = el.offsetTop,
			left = el.offsetLeft,
			width = el.offsetWidth,
			height = el.offsetHeight;

		while (el.offsetParent) {
			el = el.offsetParent;
			top += el.offsetTop;
			left += el.offsetLeft;
		}

		if (typeof(offsetToTop) !== 'undefined') {
			top -= offsetToTop;
		}

		if (window.pageXOffset !== null) {
			return (
				top < (window.pageYOffset + window.innerHeight) &&
				left < (window.pageXOffset + window.innerWidth) &&
				(top + height) > window.pageYOffset &&
				(left + width) > window.pageXOffset
			);
		}

		if (document.compatMode === 'CSS1Compat') {
			return (
				top < (window.document.documentElement.scrollTop + window.document.documentElement.clientHeight) &&
				left < (window.document.documentElement.scrollLeft + window.document.documentElement.clientWidth) &&
				(top + height) > window.document.documentElement.scrollTop &&
				(left + width) > window.document.documentElement.scrollLeft
			);
		}
	},

	/**
	 * @function
	 * @description Appends the parameter 'format=ajax' to a given path
	 * @param {String} path the relative path
	 */
	ajaxUrl: function (path) {
		return this.appendParamToURL(path, 'format', 'ajax');
	},

	/**
	 * @function
	 * @description
	 * @param {String} url
	 */
	toAbsoluteUrl: function (url) {
		if (url.indexOf('http') !== 0 && url.charAt(0) !== '/') {
			url = '/' + url;
		}
		return url;
	},
	/**
	 * @function
	 * @description Loads css dynamically from given urls
	 * @param {Array} urls Array of urls from which css will be dynamically loaded.
	 */
	loadDynamicCss: function (urls) {
		var i, len = urls.length;
		for (i = 0; i < len; i++) {
			this.loadedCssFiles.push(this.loadCssFile(urls[i]));
		}
	},

	/**
	 * @function
	 * @description Loads css file dynamically from given url
	 * @param {String} url The url from which css file will be dynamically loaded.
	 */
	loadCssFile: function (url) {
		return $('<link/>').appendTo($('head')).attr({
			type: 'text/css',
			rel: 'stylesheet'
		}).attr('href', url); // for i.e. <9, href must be added after link has been appended to head
	},
	// array to keep track of the dynamically loaded CSS files
	loadedCssFiles: [],

	/**
	 * @function
	 * @description Removes all css files which were dynamically loaded
	 */
	clearDynamicCss: function () {
		var i = this.loadedCssFiles.length;
		while (0 > i--) {
			$(this.loadedCssFiles[i]).remove();
		}
		this.loadedCssFiles = [];
	},
	/**
	 * @function
	 * @description Extracts all parameters from a given query string into an object
	 * @param {String} qs The query string from which the parameters will be extracted
	 */
	getQueryStringParams: function (qs) {
		if (!qs || qs.length === 0) { return {}; }
		var params = {},
			unescapedQS = decodeURIComponent(qs);
		// Use the String::replace method to iterate over each
		// name-value pair in the string.
		unescapedQS.replace(new RegExp('([^?=&]+)(=([^&]*))?', 'g'),
			function ($0, $1, $2, $3) {
				params[$1] = $3;
			}
		);
		return params;
	},

	fillAddressFields: function (address, $form) {
		$form.find('[id$="_address2"]').val("");
		for (var field in address) {
			if (field === 'ID' || field === 'UUID' || field === 'key') {
				continue;
			}
			if(address[field] != null){
				// if the key in address object ends with 'Code', remove that suffix
				// keys that ends with 'Code' are postalCode, stateCode and countryCode
				$form.find('[name$="' + field.replace('Code', '') + '"]').val(address[field]);
				// update the state fields
				if (field === 'countryCode') {
					$form.find('[name$="country"]').trigger('change');
					// retrigger state selection after country has changed
					// this results in duplication of the state code, but is a necessary evil
					// for now because sometimes countryCode comes after stateCode
					$form.find('[name$="state"]').val(address.stateCode).trigger('change');
				} 
			}
		}
	},
	/**
	 * @function
	 * @description Updates the number of the remaining character
	 * based on the character limit in a text area
	 */
	limitCharacters: function () {
		$('form').find('textarea[data-character-limit]').each(function () {
			var characterLimit = $(this).data('character-limit');
			var charCountHtml = String.format(Resources.CHAR_LIMIT_MSG,
				'<span class="char-remain-count">' + characterLimit + '</span>',
				'<span class="char-allowed-count">' + characterLimit + '</span>');
			var charCountContainer = $(this).next('div.char-count');
			if (charCountContainer.length === 0) {
				charCountContainer = $('<div class="char-count"/>').insertAfter($(this));
			}
			charCountContainer.html(charCountHtml);
			// trigger the keydown event so that any existing character data is calculated
			$(this).change();
		});
	},
	/**
	 * @function
	 * @description Binds the onclick-event to a delete button on a given container,
	 * which opens a confirmation box with a given message
	 * @param {String} container The name of element to which the function will be bind
	 * @param {String} message The message the will be shown upon a click
	 */
	setDeleteConfirmation: function (container, message) {
		$(container).on('click', '.delete', function () {
			return window.confirm(message);
		});
	},
	/**
	 * @function
	 * @description Scrolls a browser window to a given x point
	 * @param {String} The x coordinate
	 */
	scrollBrowser: function (xLocation) {
		$('html, body').animate({scrollTop: xLocation}, 500);
	},

	isMobile: function () {
		var mobileAgentHash = ['mobile', 'tablet', 'phone', 'ipad', 'ipod', 'android', 'blackberry', 'windows ce', 'opera mini', 'palm'];
		var	idx = 0;
		var isMobile = false;
		var userAgent = (navigator.userAgent).toLowerCase();

		while (mobileAgentHash[idx] && !isMobile) {
			isMobile = (userAgent.indexOf(mobileAgentHash[idx]) >= 0);
			idx++;
		}
		return isMobile;
	},
	selectbox : function(){
		$('#main .input-select, .ui-dialog .input-select').each(function(){
			$(this).selectBoxIt();
		});
	},
	countrySelectbox : function(){
		$('#HeaderChangeFlag .input-select').each(function(){
			$(this).selectBoxIt();
		});
	},
	miniSummaryArrow : function(){
	    $('.mini-cart-product').eq(0).find('.mini-cart-toggle').addClass('caret-down');
		$('.mini-cart-product').not(':first').addClass('collapsed')
			.find('.mini-cart-toggle').addClass('caret-right');

		$('.mini-cart-toggle').on('click', function () {
			$(this).toggleClass('caret-down caret-right');
			$(this).closest('.mini-cart-product').toggleClass('collapsed');
		});
	},
	fancyScroll : function(){
	// for search result page
		$(".pt_product-search-result #search-result-items li.grid-tile:nth-child(6n+4)").addClass("section");
	
		//FancyScroll
		(function initFancyScroll($container) {
		    var 
				$scroller = $(window),
				topOffset = 60,
				$sections = $('.section', $container);
	
		    function throttle(fn, context) {
		        var lastInvocation = 0, threshold = 10;
		        return function () {
		            var now = new Date().getTime();
		            if (now - lastInvocation > threshold) {
		                return fn.apply(context, arguments);
		            }
		        }
		    }
	
		    function scrollTop() {
		        return $scroller.scrollTop();
		    }
	
		    function isAboveTop() {
		        return $(this)[0].offsetTop < $scroller.scrollTop();
		    }
	
		    function isBelowTop() {
		        return $(this)[0].offsetTop > $scroller.scrollTop();
		    }
	
		    function prevSection() {
		        return $sections.filter(isAboveTop).last();
		    }
	
		    function nextSection() {
		        return $sections.filter(isBelowTop).first();
		    }
	
		    function currentSectionNumber() {
		        var i, scrollTop = $scroller.scrollTop() + 10;
		        for (i = 0; $sections[i] && $($sections[i]).position().top <= scrollTop; i++) { }
		        return i;
		    }
	
		    function scrollTo($target) {
		        if ($target.length === 0) { return; }
		        if ($target[0].offsetTop != 0) { $('html,body').animate({ scrollTop: $target[0].offsetTop }, 500); }
		        if ($target != undefined && $target != null && $target.prevObject != undefined) {
		        	if ($target.prevObject.length === 1) { $('html,body').animate({ scrollTop: $target[0].offsetTop }, 500); }
		        }
		        //$('html,body').animate({ scrollTop: $target[0].offsetTop }, 500);
		        //console.debug('scrollTo: ' + $target.position().top);
		    }
	
		    function updatePageNumber() {
		        if ($(document).outerHeight() - $(document).scrollTop() <= $scroller.outerHeight()) {
		            $('.page-number').text($sections.length);
		        } else {
		            $('.page-number').text(currentSectionNumber());
		        }
		    }
	
		    function init() {
		        $('.up').click(function () {
		            scrollTo(prevSection());
		        });
		        $('.down').click(function () {
		            scrollTo(nextSection());
		        });
	
		        $('.total-pages').text($sections.length);
		        $scroller.scroll(throttle(updatePageNumber));
		        $scroller.trigger('scroll');
	
		        if ($('.up').length > 0 && $('#navpanel').is(':visible')) {
		            $(document).keydown(function (e) {
		                if (e.keyCode == 38) {
		                    scrollTo(prevSection());
		                    e.preventDefault();
		                }
		            });
		            $(document).keydown(function (e) {
		                if (e.keyCode == 40) {
		                    scrollTo(nextSection());
		                    e.preventDefault();
		                }
		            });
		        }
		    }
		    init();
		} ($('#main')));
	},
	//getcookie
	GetCookie : function (cname) {
	    var name = cname + "=";
	    var ca = document.cookie.split(';');
	    for(var i=0; i<ca.length; i++) {
	        var c = ca[i];
	        if (c.charAt(0)==' ') c = c.substring(1);
	        if (c.indexOf(name) == 0){
	        	return c.substring(name.length, c.length);
	        }
	    }
	    return "";
	},

	//setcookie	
	SetCookie : function (name,value,days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
		document.cookie = name+"="+value+expires+"; path=/";
		return '';
	},
	
	DeleteCookie : function (name){
		var expires = "Thu, 01 Jan 1970 00:00:00 UTC";
		document.cookie = name+"=; expires="+expires+"; path=/";
		return "";
	},
	
	GetDateTime : function getDateTime(){
		var dateobj = new Date();
		var cookieval = '';
		cookieval = dateobj.getFullYear().toString();
		((dateobj.getMonth()+1).toString().length == 1) ? (cookieval += "0"+(dateobj.getMonth()+1)) : cookieval += dateobj.getMonth()+1,
		(dateobj.getDate().toString().length == 1) ? (cookieval += "0"+dateobj.getDate()) :  cookieval += dateobj.getDate(),
		(dateobj.getHours().toString().length == 1) ? (cookieval += "0"+dateobj.getHours()) :  cookieval += dateobj.getHours(),
		(dateobj.getMinutes().toString().length == 1) ? (cookieval += "0"+dateobj.getMinutes()) :  cookieval += dateobj.getMinutes()
		return cookieval;
	}
	
};

module.exports = util;

},{"lodash":58}],53:[function(require,module,exports){
'use strict';

var naPhone = /^[0-9+ ]{6,20}$/;
var regex = {
	phone: {
		gb: naPhone,
		us: /^[0-9+ -]{6,20}$/,
		ca: /^[0-9+ -]{6,20}$/,
		nl: naPhone,
		de: naPhone,
		au: naPhone,
		nz: naPhone,
		fr: naPhone,
		row: naPhone
	},
	postal: {
		gb: /^[a-zA-Z]{1,2}[0-9][0-9A-Za-z]{0,1} {0,1}[0-9][A-Za-z]{2}$/,
		im: /^[a-zA-Z]{1,2}[0-9][0-9A-Za-z]{0,1} {0,1}[0-9][A-Za-z]{2}$/,
		je: /^[a-zA-Z]{1,2}[0-9][0-9A-Za-z]{0,1} {0,1}[0-9][A-Za-z]{2}$/,
		gg: /^[a-zA-Z]{1,2}[0-9][0-9A-Za-z]{0,1} {0,1}[0-9][A-Za-z]{2}$/,
		us: /^\d{5}(-\d{4})?$/,
		ca: /^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$/,
		nl: /^[1-9][0-9]{3}\s[a-zA-Z]{2}$/,
		de: /^[0-9]{4,5}$/,
		au: /^[0-9]{4}$/,
		nz: /^[0-9]{4}$/,
		fr: /^[0-9]{5}$/
	},
	notCC: /^(?!(([0-9 -]){13,19})).*$/
};
// global form validator settings
var settings = {
	errorClass: 'error',
	errorElement: 'span',
	onkeyup: false,
	onfocusout: function (element) {
		//if($(element).attr('id') != undefined && $(element).attr('id').split("_").length > 3 && $(element).attr('id').split("_").splice(0,4).join("_") == "dwfrm_billing_billingAddress_addressFields"){
		//	return true;
		//}
		if($(element).attr("type") != undefined){
			$(element).val($(element).val().trim());
		}
		if($(element).closest(".form-row").find("span.error").length > 0){
			$(element).closest(".form-row").find("span.error").hide();
		}
		if($(element).closest(".form-row").find("div.error-message").length > 0){
			$(element).closest(".form-row").find("div.error-message").hide();
		}
		if (!this.checkable(element)) {
			this.element(element);
			if(this.element(element)){
				$(element).closest(".form-row").find("label").removeClass("error");
	        }else{
	            $(element).closest(".form-row").find("label").addClass("error");
	        }
		}
	}
};
/**
 * @function
 * @description Validates a given phone number against the countries phone regex
 * @param {String} value The phone number which will be validated
 * @param {String} el The input field
 */
var validatePhone = function (value, el) {
    var country = $(el).closest('form').find('select.country');
    var country = $(el).closest('form').find('select.country');
    if( $(el).closest('form').hasClass("click-pos-form") && (country.length === 0 || country.val().length === 0) ){
    	country = $(".click-and-collect-form").find('select.country');
    }
    var rgx;
    if(regex.phone[country.val().toLowerCase()]){
           rgx = regex.phone[country.val().toLowerCase()];
    }else if(regex.phone['row']){
           rgx = regex.phone['row'];
    }else{
           return naPhone;
    }
    var isOptional = this.optional(el);
    var isValid = rgx.test($.trim(value));

    return isOptional || isValid;
};

/**
 * @function
 * @description Validates that a credit card owner is not a Credit card number
 * @param {String} value The owner field which will be validated
 * @param {String} el The input field
 */
var validateOwner = function (value) {
	var isValid = regex.notCC.test($.trim(value));
	return isValid;
};

/**
 * Add phone validation method to jQuery validation plugin.
 * Text fields must have 'phone' css class to be validated as phone
 */
$.validator.addMethod('phone', validatePhone, Resources.INVALID_PHONE);

$.validator.addMethod('mobile', validatePhone, Resources.INVALID_MOBILE);


/**
 * Add CCOwner validation method to jQuery validation plugin.
 * Text fields must have 'owner' css class to be validated as not a credit card
 */
$.validator.addMethod('owner', validateOwner, Resources.INVALID_OWNER);

/**
 * Add gift cert amount validation method to jQuery validation plugin.
 * Text fields must have 'gift-cert-amont' css class to be validated
 */
$.validator.addMethod('gift-cert-amount', function (value, el) {
	var isOptional = this.optional(el);
	var isValid = (!isNaN(value)) && (parseFloat(value) >= 5) && (parseFloat(value) <= 5000);
	return isOptional || isValid;
}, Resources.GIFT_CERT_AMOUNT_INVALID);

/**
 * Add positive number validation method to jQuery validation plugin.
 * Text fields must have 'positivenumber' css class to be validated as positivenumber
 */
$.validator.addMethod('positivenumber', function (value) {
	if ($.trim(value).length === 0) { return true; }
	return (!isNaN(value) && Number(value) >= 0);
}, ''); // '' should be replaced with error message if needed

/*Start JIRA PREV-77 : Zip code validation is not happening with respect to the State/Country.*/ 
function validateZip(value, el) {
	var country = $(el).closest("form").find("select.country");
	
	if(el.id == "dwfrm_shiptoshop_postal"){
		country = $(".click-and-collect-form").find("select.country");
	}
	//changes made for GBSS-86
	if(el.id == "dwfrm_billing_billingAddress_addressFields_postal"){
		country = $(el).closest('.bilingForm').find("select.country");
		if(country.val() == undefined || country.val() == null){
			country = $(el).closest('Form').find("select.country");
		}
	}
	if(country.length === 0 || country.val().length === 0 || !regex.postal[country.val().toLowerCase()]) {
		return true;
	}
	var isOptional = this.optional(el);
	var isValid = regex.postal[country.val().toLowerCase()].test($.trim(value));
	
	return isOptional || isValid;
}
$.validator.addMethod("postal", validateZip, Resources.INVALID_ZIP);
/*End JIRA PREV-77*/

//validation for first name
var validateFirstName = function (value, el) {
	var rgx = /[^A-Z a-z]/;
	var isValid = !rgx.test($.trim(value));
    return isValid;
};

//This is for shipping and billing form
$.validator.addMethod("firstName", validateFirstName, Resources.INVALID_FNAME);

//This is for registration and guest checkout user form
$.validator.addMethod("firstname", validateFirstName, Resources.INVALID_FNAME);


//validation for last name
var validateLastName = function (value, el) {
	var rgx = /[^A-Z a-z]/;
	var isValid = !rgx.test($.trim(value));
    return isValid;
};

//This is for shipping and billing form
$.validator.addMethod("lastName", validateLastName, Resources.INVALID_LNAME);
//This is for registration and guest checkout user form
$.validator.addMethod("lastname", validateLastName, Resources.INVALID_LNAME);



function validateExpirationDate(value, element) {
		if(element.form['dwfrm_billing_paymentMethods_creditCard_expiration_month'].value != ''){
			$(element.form).validate().element("#dwfrm_billing_paymentMethods_creditCard_expiration_month");
			$(element.form).find(".year span.error").hide();
		}
		return true;
}
function validateExpirationMonth(value, element) {
	if(element.form['dwfrm_billing_paymentMethods_creditCard_expiration_month'].value != '' && element.form['dwfrm_billing_paymentMethods_creditCard_expiration_year'].value != ''){
		var expiry = element.form['dwfrm_billing_paymentMethods_creditCard_expiration_year'].value + (element.form['dwfrm_billing_paymentMethods_creditCard_expiration_month'].value.length == 1 ? '0' + element.form['dwfrm_billing_paymentMethods_creditCard_expiration_month'].value : element.form['dwfrm_billing_paymentMethods_creditCard_expiration_month'].value),
	    date = new Date(),
	    month = date.getMonth() + 1,
	    month = month.toString();
	    if(month.length == 1){
	    	month = '0' + month;
	    }
	    var now = '' + date.getFullYear().toString() + month;
	    
	    var currentDate = Number(now);
	    var formData = Number(expiry);
	    
	    if(formData >= currentDate){
	    	return true;
	    }else{
	    	return false;
	    }
	}else{
		return true;
	}
}
$.validator.addMethod('year', validateExpirationDate,Resources.CREDITCARDEXPIRATIONERROR);

$.validator.addMethod('month', validateExpirationMonth,Resources.CREDITCARDEXPIRATIONERROR);
function validateCvnNo(value, element) {
	if(value != "***"){
		var rgx = /^[0-9]{1,4}$/;
		var isValid = rgx.test($.trim(value));
	    return isValid;
	}else{
		return true;
	}
}
$.validator.addMethod('cvn', validateCvnNo,Resources.CREDITCARDCVNERROR);

$.extend($.validator.messages, {
	required: Resources.VALIDATE_REQUIRED,
	remote: Resources.VALIDATE_REMOTE,
	email: Resources.VALIDATE_EMAIL,
	url: Resources.VALIDATE_URL,
	date: Resources.VALIDATE_DATE,
	dateISO: Resources.VALIDATE_DATEISO,
	number: Resources.VALIDATE_NUMBER,
	digits: Resources.VALIDATE_DIGITS,
	creditcard: Resources.VALIDATE_CREDITCARD,
	equalTo: Resources.VALIDATE_EQUALTO,
	maxlength: $.validator.format(Resources.VALIDATE_MAXLENGTH),
	minlength: $.validator.format(Resources.VALIDATE_MINLENGTH),
	rangelength: $.validator.format(Resources.VALIDATE_RANGELENGTH),
	range: $.validator.format(Resources.VALIDATE_RANGE),
	max: $.validator.format(Resources.VALIDATE_MAX),
	min: $.validator.format(Resources.VALIDATE_MIN)
});

var validator = {
	regex: regex,
	settings: settings,
	init: function () {
		var self = this;
		$('form:not(.suppress)').each(function () {
			$(this).validate(errorMessage(self.settings,$(this)));
		});
	},
	initForm: function (f) {
		$(f).validate(this.settings);
	}
};

function errorMessage(settings,form){
	settings.messages={};
	settings.rules={};
	form.find("[class^='input-'].required").each(function () {
		var key = $(this).attr("id");
		settings.messages[key]={};
		settings.rules[key]={};
		if($(this).is("[error]")){
			settings.messages[key]["required"]=$(this).attr("error");
			settings.rules[key]["required"]=true;
		}
		if($(this).is("[minlength]")){
			settings.rules[key]["minlength"]=parseInt($(this).attr("minlength"));
		}
		if($(this).is("[maxlength]")){
			settings.rules[key]["maxlength"]=parseInt($(this).attr("maxlength"));
		}
		if($(this).is("[minlengtherrormsg]")){
			settings.messages[key]["minlength"]=$(this).attr("minlengtherrormsg");
		}
		if($(this).is("[maxlengtherrormsg]")){
			settings.messages[key]["maxlength"]=$(this).attr("maxlengtherrormsg");
		}
	});
	return settings;
}

module.exports = validator;

},{}],54:[function(require,module,exports){
"use strict";

// rawAsap provides everything we need except exception management.
var rawAsap = require("./raw");
// RawTasks are recycled to reduce GC churn.
var freeTasks = [];
// We queue errors to ensure they are thrown in right order (FIFO).
// Array-as-queue is good enough here, since we are just dealing with exceptions.
var pendingErrors = [];
var requestErrorThrow = rawAsap.makeRequestCallFromTimer(throwFirstError);

function throwFirstError() {
    if (pendingErrors.length) {
        throw pendingErrors.shift();
    }
}

/**
 * Calls a task as soon as possible after returning, in its own event, with priority
 * over other events like animation, reflow, and repaint. An error thrown from an
 * event will not interrupt, nor even substantially slow down the processing of
 * other events, but will be rather postponed to a lower priority event.
 * @param {{call}} task A callable object, typically a function that takes no
 * arguments.
 */
module.exports = asap;
function asap(task) {
    var rawTask;
    if (freeTasks.length) {
        rawTask = freeTasks.pop();
    } else {
        rawTask = new RawTask();
    }
    rawTask.task = task;
    rawAsap(rawTask);
}

// We wrap tasks with recyclable task objects.  A task object implements
// `call`, just like a function.
function RawTask() {
    this.task = null;
}

// The sole purpose of wrapping the task is to catch the exception and recycle
// the task object after its single use.
RawTask.prototype.call = function () {
    try {
        this.task.call();
    } catch (error) {
        if (asap.onerror) {
            // This hook exists purely for testing purposes.
            // Its name will be periodically randomized to break any code that
            // depends on its existence.
            asap.onerror(error);
        } else {
            // In a web browser, exceptions are not fatal. However, to avoid
            // slowing down the queue of pending tasks, we rethrow the error in a
            // lower priority turn.
            pendingErrors.push(error);
            requestErrorThrow();
        }
    } finally {
        this.task = null;
        freeTasks[freeTasks.length] = this;
    }
};

},{"./raw":55}],55:[function(require,module,exports){
(function (global){
"use strict";

// Use the fastest means possible to execute a task in its own turn, with
// priority over other events including IO, animation, reflow, and redraw
// events in browsers.
//
// An exception thrown by a task will permanently interrupt the processing of
// subsequent tasks. The higher level `asap` function ensures that if an
// exception is thrown by a task, that the task queue will continue flushing as
// soon as possible, but if you use `rawAsap` directly, you are responsible to
// either ensure that no exceptions are thrown from your task, or to manually
// call `rawAsap.requestFlush` if an exception is thrown.
module.exports = rawAsap;
function rawAsap(task) {
    if (!queue.length) {
        requestFlush();
        flushing = true;
    }
    // Equivalent to push, but avoids a function call.
    queue[queue.length] = task;
}

var queue = [];
// Once a flush has been requested, no further calls to `requestFlush` are
// necessary until the next `flush` completes.
var flushing = false;
// `requestFlush` is an implementation-specific method that attempts to kick
// off a `flush` event as quickly as possible. `flush` will attempt to exhaust
// the event queue before yielding to the browser's own event loop.
var requestFlush;
// The position of the next task to execute in the task queue. This is
// preserved between calls to `flush` so that it can be resumed if
// a task throws an exception.
var index = 0;
// If a task schedules additional tasks recursively, the task queue can grow
// unbounded. To prevent memory exhaustion, the task queue will periodically
// truncate already-completed tasks.
var capacity = 1024;

// The flush function processes all tasks that have been scheduled with
// `rawAsap` unless and until one of those tasks throws an exception.
// If a task throws an exception, `flush` ensures that its state will remain
// consistent and will resume where it left off when called again.
// However, `flush` does not make any arrangements to be called again if an
// exception is thrown.
function flush() {
    while (index < queue.length) {
        var currentIndex = index;
        // Advance the index before calling the task. This ensures that we will
        // begin flushing on the next task the task throws an error.
        index = index + 1;
        queue[currentIndex].call();
        // Prevent leaking memory for long chains of recursive calls to `asap`.
        // If we call `asap` within tasks scheduled by `asap`, the queue will
        // grow, but to avoid an O(n) walk for every task we execute, we don't
        // shift tasks off the queue after they have been executed.
        // Instead, we periodically shift 1024 tasks off the queue.
        if (index > capacity) {
            // Manually shift all values starting at the index back to the
            // beginning of the queue.
            for (var scan = 0, newLength = queue.length - index; scan < newLength; scan++) {
                queue[scan] = queue[scan + index];
            }
            queue.length -= index;
            index = 0;
        }
    }
    queue.length = 0;
    index = 0;
    flushing = false;
}

// `requestFlush` is implemented using a strategy based on data collected from
// every available SauceLabs Selenium web driver worker at time of writing.
// https://docs.google.com/spreadsheets/d/1mG-5UYGup5qxGdEMWkhP6BWCz053NUb2E1QoUTU16uA/edit#gid=783724593

// Safari 6 and 6.1 for desktop, iPad, and iPhone are the only browsers that
// have WebKitMutationObserver but not un-prefixed MutationObserver.
// Must use `global` or `self` instead of `window` to work in both frames and web
// workers. `global` is a provision of Browserify, Mr, Mrs, or Mop.

/* globals self */
var scope = typeof global !== "undefined" ? global : self;
var BrowserMutationObserver = scope.MutationObserver || scope.WebKitMutationObserver;

// MutationObservers are desirable because they have high priority and work
// reliably everywhere they are implemented.
// They are implemented in all modern browsers.
//
// - Android 4-4.3
// - Chrome 26-34
// - Firefox 14-29
// - Internet Explorer 11
// - iPad Safari 6-7.1
// - iPhone Safari 7-7.1
// - Safari 6-7
if (typeof BrowserMutationObserver === "function") {
    requestFlush = makeRequestCallFromMutationObserver(flush);

// MessageChannels are desirable because they give direct access to the HTML
// task queue, are implemented in Internet Explorer 10, Safari 5.0-1, and Opera
// 11-12, and in web workers in many engines.
// Although message channels yield to any queued rendering and IO tasks, they
// would be better than imposing the 4ms delay of timers.
// However, they do not work reliably in Internet Explorer or Safari.

// Internet Explorer 10 is the only browser that has setImmediate but does
// not have MutationObservers.
// Although setImmediate yields to the browser's renderer, it would be
// preferrable to falling back to setTimeout since it does not have
// the minimum 4ms penalty.
// Unfortunately there appears to be a bug in Internet Explorer 10 Mobile (and
// Desktop to a lesser extent) that renders both setImmediate and
// MessageChannel useless for the purposes of ASAP.
// https://github.com/kriskowal/q/issues/396

// Timers are implemented universally.
// We fall back to timers in workers in most engines, and in foreground
// contexts in the following browsers.
// However, note that even this simple case requires nuances to operate in a
// broad spectrum of browsers.
//
// - Firefox 3-13
// - Internet Explorer 6-9
// - iPad Safari 4.3
// - Lynx 2.8.7
} else {
    requestFlush = makeRequestCallFromTimer(flush);
}

// `requestFlush` requests that the high priority event queue be flushed as
// soon as possible.
// This is useful to prevent an error thrown in a task from stalling the event
// queue if the exception handled by Node.js’s
// `process.on("uncaughtException")` or by a domain.
rawAsap.requestFlush = requestFlush;

// To request a high priority event, we induce a mutation observer by toggling
// the text of a text node between "1" and "-1".
function makeRequestCallFromMutationObserver(callback) {
    var toggle = 1;
    var observer = new BrowserMutationObserver(callback);
    var node = document.createTextNode("");
    observer.observe(node, {characterData: true});
    return function requestCall() {
        toggle = -toggle;
        node.data = toggle;
    };
}

// The message channel technique was discovered by Malte Ubl and was the
// original foundation for this library.
// http://www.nonblocking.io/2011/06/windownexttick.html

// Safari 6.0.5 (at least) intermittently fails to create message ports on a
// page's first load. Thankfully, this version of Safari supports
// MutationObservers, so we don't need to fall back in that case.

// function makeRequestCallFromMessageChannel(callback) {
//     var channel = new MessageChannel();
//     channel.port1.onmessage = callback;
//     return function requestCall() {
//         channel.port2.postMessage(0);
//     };
// }

// For reasons explained above, we are also unable to use `setImmediate`
// under any circumstances.
// Even if we were, there is another bug in Internet Explorer 10.
// It is not sufficient to assign `setImmediate` to `requestFlush` because
// `setImmediate` must be called *by name* and therefore must be wrapped in a
// closure.
// Never forget.

// function makeRequestCallFromSetImmediate(callback) {
//     return function requestCall() {
//         setImmediate(callback);
//     };
// }

// Safari 6.0 has a problem where timers will get lost while the user is
// scrolling. This problem does not impact ASAP because Safari 6.0 supports
// mutation observers, so that implementation is used instead.
// However, if we ever elect to use timers in Safari, the prevalent work-around
// is to add a scroll event listener that calls for a flush.

// `setTimeout` does not call the passed callback if the delay is less than
// approximately 7 in web workers in Firefox 8 through 18, and sometimes not
// even then.

function makeRequestCallFromTimer(callback) {
    return function requestCall() {
        // We dispatch a timeout with a specified delay of 0 for engines that
        // can reliably accommodate that request. This will usually be snapped
        // to a 4 milisecond delay, but once we're flushing, there's no delay
        // between events.
        var timeoutHandle = setTimeout(handleTimer, 0);
        // However, since this timer gets frequently dropped in Firefox
        // workers, we enlist an interval handle that will try to fire
        // an event 20 times per second until it succeeds.
        var intervalHandle = setInterval(handleTimer, 50);

        function handleTimer() {
            // Whichever timer succeeds will cancel both timers and
            // execute the callback.
            clearTimeout(timeoutHandle);
            clearInterval(intervalHandle);
            callback();
        }
    };
}

// This is for `asap.js` only.
// Its name will be periodically randomized to break any code that depends on
// its existence.
rawAsap.makeRequestCallFromTimer = makeRequestCallFromTimer;

// ASAP was originally a nextTick shim included in Q. This was factored out
// into this ASAP package. It was later adapted to RSVP which made further
// amendments. These decisions, particularly to marginalize MessageChannel and
// to capture the MutationObserver implementation in a closure, were integrated
// back into ASAP proper.
// https://github.com/tildeio/rsvp.js/blob/cddf7232546a9cf858524b75cde6f9edf72620a7/lib/rsvp/asap.js

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],56:[function(require,module,exports){
/*!
 * eventie v1.0.6
 * event binding helper
 *   eventie.bind( elem, 'click', myFn )
 *   eventie.unbind( elem, 'click', myFn )
 * MIT license
 */

/*jshint browser: true, undef: true, unused: true */
/*global define: false, module: false */

( function( window ) {

'use strict';

var docElem = document.documentElement;

var bind = function() {};

function getIEEvent( obj ) {
  var event = window.event;
  // add event.target
  event.target = event.target || event.srcElement || obj;
  return event;
}

if ( docElem.addEventListener ) {
  bind = function( obj, type, fn ) {
    obj.addEventListener( type, fn, false );
  };
} else if ( docElem.attachEvent ) {
  bind = function( obj, type, fn ) {
    obj[ type + fn ] = fn.handleEvent ?
      function() {
        var event = getIEEvent( obj );
        fn.handleEvent.call( fn, event );
      } :
      function() {
        var event = getIEEvent( obj );
        fn.call( obj, event );
      };
    obj.attachEvent( "on" + type, obj[ type + fn ] );
  };
}

var unbind = function() {};

if ( docElem.removeEventListener ) {
  unbind = function( obj, type, fn ) {
    obj.removeEventListener( type, fn, false );
  };
} else if ( docElem.detachEvent ) {
  unbind = function( obj, type, fn ) {
    obj.detachEvent( "on" + type, obj[ type + fn ] );
    try {
      delete obj[ type + fn ];
    } catch ( err ) {
      // can't delete window object properties
      obj[ type + fn ] = undefined;
    }
  };
}

var eventie = {
  bind: bind,
  unbind: unbind
};

// ----- module definition ----- //

if ( typeof define === 'function' && define.amd ) {
  // AMD
  define( eventie );
} else if ( typeof exports === 'object' ) {
  // CommonJS
  module.exports = eventie;
} else {
  // browser global
  window.eventie = eventie;
}

})( window );

},{}],57:[function(require,module,exports){
/*!
 * imagesLoaded v3.2.0
 * JavaScript is all like "You images are done yet or what?"
 * MIT License
 */

( function( window, factory ) { 'use strict';
  // universal module definition

  /*global define: false, module: false, require: false */

  if ( typeof define == 'function' && define.amd ) {
    // AMD
    define( [
      'eventEmitter/EventEmitter',
      'eventie/eventie'
    ], function( EventEmitter, eventie ) {
      return factory( window, EventEmitter, eventie );
    });
  } else if ( typeof module == 'object' && module.exports ) {
    // CommonJS
    module.exports = factory(
      window,
      require('wolfy87-eventemitter'),
      require('eventie')
    );
  } else {
    // browser global
    window.imagesLoaded = factory(
      window,
      window.EventEmitter,
      window.eventie
    );
  }

})( window,

// --------------------------  factory -------------------------- //

function factory( window, EventEmitter, eventie ) {

'use strict';

var $ = window.jQuery;
var console = window.console;

// -------------------------- helpers -------------------------- //

// extend objects
function extend( a, b ) {
  for ( var prop in b ) {
    a[ prop ] = b[ prop ];
  }
  return a;
}

var objToString = Object.prototype.toString;
function isArray( obj ) {
  return objToString.call( obj ) == '[object Array]';
}

// turn element or nodeList into an array
function makeArray( obj ) {
  var ary = [];
  if ( isArray( obj ) ) {
    // use object if already an array
    ary = obj;
  } else if ( typeof obj.length == 'number' ) {
    // convert nodeList to array
    for ( var i=0; i < obj.length; i++ ) {
      ary.push( obj[i] );
    }
  } else {
    // array of single index
    ary.push( obj );
  }
  return ary;
}

  // -------------------------- imagesLoaded -------------------------- //

  /**
   * @param {Array, Element, NodeList, String} elem
   * @param {Object or Function} options - if function, use as callback
   * @param {Function} onAlways - callback function
   */
  function ImagesLoaded( elem, options, onAlways ) {
    // coerce ImagesLoaded() without new, to be new ImagesLoaded()
    if ( !( this instanceof ImagesLoaded ) ) {
      return new ImagesLoaded( elem, options, onAlways );
    }
    // use elem as selector string
    if ( typeof elem == 'string' ) {
      elem = document.querySelectorAll( elem );
    }

    this.elements = makeArray( elem );
    this.options = extend( {}, this.options );

    if ( typeof options == 'function' ) {
      onAlways = options;
    } else {
      extend( this.options, options );
    }

    if ( onAlways ) {
      this.on( 'always', onAlways );
    }

    this.getImages();

    if ( $ ) {
      // add jQuery Deferred object
      this.jqDeferred = new $.Deferred();
    }

    // HACK check async to allow time to bind listeners
    var _this = this;
    setTimeout( function() {
      _this.check();
    });
  }

  ImagesLoaded.prototype = new EventEmitter();

  ImagesLoaded.prototype.options = {};

  ImagesLoaded.prototype.getImages = function() {
    this.images = [];

    // filter & find items if we have an item selector
    for ( var i=0; i < this.elements.length; i++ ) {
      var elem = this.elements[i];
      this.addElementImages( elem );
    }
  };

  /**
   * @param {Node} element
   */
  ImagesLoaded.prototype.addElementImages = function( elem ) {
    // filter siblings
    if ( elem.nodeName == 'IMG' ) {
      this.addImage( elem );
    }
    // get background image on element
    if ( this.options.background === true ) {
      this.addElementBackgroundImages( elem );
    }

    // find children
    // no non-element nodes, #143
    var nodeType = elem.nodeType;
    if ( !nodeType || !elementNodeTypes[ nodeType ] ) {
      return;
    }
    var childImgs = elem.querySelectorAll('img');
    // concat childElems to filterFound array
    for ( var i=0; i < childImgs.length; i++ ) {
      var img = childImgs[i];
      this.addImage( img );
    }

    // get child background images
    if ( typeof this.options.background == 'string' ) {
      var children = elem.querySelectorAll( this.options.background );
      for ( i=0; i < children.length; i++ ) {
        var child = children[i];
        this.addElementBackgroundImages( child );
      }
    }
  };

  var elementNodeTypes = {
    1: true,
    9: true,
    11: true
  };

  ImagesLoaded.prototype.addElementBackgroundImages = function( elem ) {
    var style = getStyle( elem );
    // get url inside url("...")
    var reURL = /url\(['"]*([^'"\)]+)['"]*\)/gi;
    var matches = reURL.exec( style.backgroundImage );
    while ( matches !== null ) {
      var url = matches && matches[1];
      if ( url ) {
        this.addBackground( url, elem );
      }
      matches = reURL.exec( style.backgroundImage );
    }
  };

  // IE8
  var getStyle = window.getComputedStyle || function( elem ) {
    return elem.currentStyle;
  };

  /**
   * @param {Image} img
   */
  ImagesLoaded.prototype.addImage = function( img ) {
    var loadingImage = new LoadingImage( img );
    this.images.push( loadingImage );
  };

  ImagesLoaded.prototype.addBackground = function( url, elem ) {
    var background = new Background( url, elem );
    this.images.push( background );
  };

  ImagesLoaded.prototype.check = function() {
    var _this = this;
    this.progressedCount = 0;
    this.hasAnyBroken = false;
    // complete if no images
    if ( !this.images.length ) {
      this.complete();
      return;
    }

    function onProgress( image, elem, message ) {
      // HACK - Chrome triggers event before object properties have changed. #83
      setTimeout( function() {
        _this.progress( image, elem, message );
      });
    }

    for ( var i=0; i < this.images.length; i++ ) {
      var loadingImage = this.images[i];
      loadingImage.once( 'progress', onProgress );
      loadingImage.check();
    }
  };

  ImagesLoaded.prototype.progress = function( image, elem, message ) {
    this.progressedCount++;
    this.hasAnyBroken = this.hasAnyBroken || !image.isLoaded;
    // progress event
    this.emit( 'progress', this, image, elem );
    if ( this.jqDeferred && this.jqDeferred.notify ) {
      this.jqDeferred.notify( this, image );
    }
    // check if completed
    if ( this.progressedCount == this.images.length ) {
      this.complete();
    }

    if ( this.options.debug && console ) {
      console.log( 'progress: ' + message, image, elem );
    }
  };

  ImagesLoaded.prototype.complete = function() {
    var eventName = this.hasAnyBroken ? 'fail' : 'done';
    this.isComplete = true;
    this.emit( eventName, this );
    this.emit( 'always', this );
    if ( this.jqDeferred ) {
      var jqMethod = this.hasAnyBroken ? 'reject' : 'resolve';
      this.jqDeferred[ jqMethod ]( this );
    }
  };

  // --------------------------  -------------------------- //

  function LoadingImage( img ) {
    this.img = img;
  }

  LoadingImage.prototype = new EventEmitter();

  LoadingImage.prototype.check = function() {
    // If complete is true and browser supports natural sizes,
    // try to check for image status manually.
    var isComplete = this.getIsImageComplete();
    if ( isComplete ) {
      // report based on naturalWidth
      this.confirm( this.img.naturalWidth !== 0, 'naturalWidth' );
      return;
    }

    // If none of the checks above matched, simulate loading on detached element.
    this.proxyImage = new Image();
    eventie.bind( this.proxyImage, 'load', this );
    eventie.bind( this.proxyImage, 'error', this );
    // bind to image as well for Firefox. #191
    eventie.bind( this.img, 'load', this );
    eventie.bind( this.img, 'error', this );
    this.proxyImage.src = this.img.src;
  };

  LoadingImage.prototype.getIsImageComplete = function() {
    return this.img.complete && this.img.naturalWidth !== undefined;
  };

  LoadingImage.prototype.confirm = function( isLoaded, message ) {
    this.isLoaded = isLoaded;
    this.emit( 'progress', this, this.img, message );
  };

  // ----- events ----- //

  // trigger specified handler for event type
  LoadingImage.prototype.handleEvent = function( event ) {
    var method = 'on' + event.type;
    if ( this[ method ] ) {
      this[ method ]( event );
    }
  };

  LoadingImage.prototype.onload = function() {
    this.confirm( true, 'onload' );
    this.unbindEvents();
  };

  LoadingImage.prototype.onerror = function() {
    this.confirm( false, 'onerror' );
    this.unbindEvents();
  };

  LoadingImage.prototype.unbindEvents = function() {
    eventie.unbind( this.proxyImage, 'load', this );
    eventie.unbind( this.proxyImage, 'error', this );
    eventie.unbind( this.img, 'load', this );
    eventie.unbind( this.img, 'error', this );
  };

  // -------------------------- Background -------------------------- //

  function Background( url, element ) {
    this.url = url;
    this.element = element;
    this.img = new Image();
  }

  // inherit LoadingImage prototype
  Background.prototype = new LoadingImage();

  Background.prototype.check = function() {
    eventie.bind( this.img, 'load', this );
    eventie.bind( this.img, 'error', this );
    this.img.src = this.url;
    // check if image is already complete
    var isComplete = this.getIsImageComplete();
    if ( isComplete ) {
      this.confirm( this.img.naturalWidth !== 0, 'naturalWidth' );
      this.unbindEvents();
    }
  };

  Background.prototype.unbindEvents = function() {
    eventie.unbind( this.img, 'load', this );
    eventie.unbind( this.img, 'error', this );
  };

  Background.prototype.confirm = function( isLoaded, message ) {
    this.isLoaded = isLoaded;
    this.emit( 'progress', this, this.element, message );
  };

  // -------------------------- jQuery -------------------------- //

  ImagesLoaded.makeJQueryPlugin = function( jQuery ) {
    jQuery = jQuery || window.jQuery;
    if ( !jQuery ) {
      return;
    }
    // set local variable
    $ = jQuery;
    // $().imagesLoaded()
    $.fn.imagesLoaded = function( options, callback ) {
      var instance = new ImagesLoaded( this, options, callback );
      return instance.jqDeferred.promise( $(this) );
    };
  };
  // try making plugin
  ImagesLoaded.makeJQueryPlugin();

  // --------------------------  -------------------------- //

  return ImagesLoaded;

});

},{"eventie":56,"wolfy87-eventemitter":67}],58:[function(require,module,exports){
(function (global){
/**
 * @license
 * lodash 3.10.1 (Custom Build) <https://lodash.com/>
 * Build: `lodash modern -d -o ./index.js`
 * Copyright 2012-2015 The Dojo Foundation <http://dojofoundation.org/>
 * Based on Underscore.js 1.8.3 <http://underscorejs.org/LICENSE>
 * Copyright 2009-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
 * Available under MIT license <https://lodash.com/license>
 */
;(function() {

  /** Used as a safe reference for `undefined` in pre-ES5 environments. */
  var undefined;

  /** Used as the semantic version number. */
  var VERSION = '3.10.1';

  /** Used to compose bitmasks for wrapper metadata. */
  var BIND_FLAG = 1,
      BIND_KEY_FLAG = 2,
      CURRY_BOUND_FLAG = 4,
      CURRY_FLAG = 8,
      CURRY_RIGHT_FLAG = 16,
      PARTIAL_FLAG = 32,
      PARTIAL_RIGHT_FLAG = 64,
      ARY_FLAG = 128,
      REARG_FLAG = 256;

  /** Used as default options for `_.trunc`. */
  var DEFAULT_TRUNC_LENGTH = 30,
      DEFAULT_TRUNC_OMISSION = '...';

  /** Used to detect when a function becomes hot. */
  var HOT_COUNT = 150,
      HOT_SPAN = 16;

  /** Used as the size to enable large array optimizations. */
  var LARGE_ARRAY_SIZE = 200;

  /** Used to indicate the type of lazy iteratees. */
  var LAZY_FILTER_FLAG = 1,
      LAZY_MAP_FLAG = 2;

  /** Used as the `TypeError` message for "Functions" methods. */
  var FUNC_ERROR_TEXT = 'Expected a function';

  /** Used as the internal argument placeholder. */
  var PLACEHOLDER = '__lodash_placeholder__';

  /** `Object#toString` result references. */
  var argsTag = '[object Arguments]',
      arrayTag = '[object Array]',
      boolTag = '[object Boolean]',
      dateTag = '[object Date]',
      errorTag = '[object Error]',
      funcTag = '[object Function]',
      mapTag = '[object Map]',
      numberTag = '[object Number]',
      objectTag = '[object Object]',
      regexpTag = '[object RegExp]',
      setTag = '[object Set]',
      stringTag = '[object String]',
      weakMapTag = '[object WeakMap]';

  var arrayBufferTag = '[object ArrayBuffer]',
      float32Tag = '[object Float32Array]',
      float64Tag = '[object Float64Array]',
      int8Tag = '[object Int8Array]',
      int16Tag = '[object Int16Array]',
      int32Tag = '[object Int32Array]',
      uint8Tag = '[object Uint8Array]',
      uint8ClampedTag = '[object Uint8ClampedArray]',
      uint16Tag = '[object Uint16Array]',
      uint32Tag = '[object Uint32Array]';

  /** Used to match empty string literals in compiled template source. */
  var reEmptyStringLeading = /\b__p \+= '';/g,
      reEmptyStringMiddle = /\b(__p \+=) '' \+/g,
      reEmptyStringTrailing = /(__e\(.*?\)|\b__t\)) \+\n'';/g;

  /** Used to match HTML entities and HTML characters. */
  var reEscapedHtml = /&(?:amp|lt|gt|quot|#39|#96);/g,
      reUnescapedHtml = /[&<>"'`]/g,
      reHasEscapedHtml = RegExp(reEscapedHtml.source),
      reHasUnescapedHtml = RegExp(reUnescapedHtml.source);

  /** Used to match template delimiters. */
  var reEscape = /<%-([\s\S]+?)%>/g,
      reEvaluate = /<%([\s\S]+?)%>/g,
      reInterpolate = /<%=([\s\S]+?)%>/g;

  /** Used to match property names within property paths. */
  var reIsDeepProp = /\.|\[(?:[^[\]]*|(["'])(?:(?!\1)[^\n\\]|\\.)*?\1)\]/,
      reIsPlainProp = /^\w*$/,
      rePropName = /[^.[\]]+|\[(?:(-?\d+(?:\.\d+)?)|(["'])((?:(?!\2)[^\n\\]|\\.)*?)\2)\]/g;

  /**
   * Used to match `RegExp` [syntax characters](http://ecma-international.org/ecma-262/6.0/#sec-patterns)
   * and those outlined by [`EscapeRegExpPattern`](http://ecma-international.org/ecma-262/6.0/#sec-escaperegexppattern).
   */
  var reRegExpChars = /^[:!,]|[\\^$.*+?()[\]{}|\/]|(^[0-9a-fA-Fnrtuvx])|([\n\r\u2028\u2029])/g,
      reHasRegExpChars = RegExp(reRegExpChars.source);

  /** Used to match [combining diacritical marks](https://en.wikipedia.org/wiki/Combining_Diacritical_Marks). */
  var reComboMark = /[\u0300-\u036f\ufe20-\ufe23]/g;

  /** Used to match backslashes in property paths. */
  var reEscapeChar = /\\(\\)?/g;

  /** Used to match [ES template delimiters](http://ecma-international.org/ecma-262/6.0/#sec-template-literal-lexical-components). */
  var reEsTemplate = /\$\{([^\\}]*(?:\\.[^\\}]*)*)\}/g;

  /** Used to match `RegExp` flags from their coerced string values. */
  var reFlags = /\w*$/;

  /** Used to detect hexadecimal string values. */
  var reHasHexPrefix = /^0[xX]/;

  /** Used to detect host constructors (Safari > 5). */
  var reIsHostCtor = /^\[object .+?Constructor\]$/;

  /** Used to detect unsigned integer values. */
  var reIsUint = /^\d+$/;

  /** Used to match latin-1 supplementary letters (excluding mathematical operators). */
  var reLatin1 = /[\xc0-\xd6\xd8-\xde\xdf-\xf6\xf8-\xff]/g;

  /** Used to ensure capturing order of template delimiters. */
  var reNoMatch = /($^)/;

  /** Used to match unescaped characters in compiled string literals. */
  var reUnescapedString = /['\n\r\u2028\u2029\\]/g;

  /** Used to match words to create compound words. */
  var reWords = (function() {
    var upper = '[A-Z\\xc0-\\xd6\\xd8-\\xde]',
        lower = '[a-z\\xdf-\\xf6\\xf8-\\xff]+';

    return RegExp(upper + '+(?=' + upper + lower + ')|' + upper + '?' + lower + '|' + upper + '+|[0-9]+', 'g');
  }());

  /** Used to assign default `context` object properties. */
  var contextProps = [
    'Array', 'ArrayBuffer', 'Date', 'Error', 'Float32Array', 'Float64Array',
    'Function', 'Int8Array', 'Int16Array', 'Int32Array', 'Math', 'Number',
    'Object', 'RegExp', 'Set', 'String', '_', 'clearTimeout', 'isFinite',
    'parseFloat', 'parseInt', 'setTimeout', 'TypeError', 'Uint8Array',
    'Uint8ClampedArray', 'Uint16Array', 'Uint32Array', 'WeakMap'
  ];

  /** Used to make template sourceURLs easier to identify. */
  var templateCounter = -1;

  /** Used to identify `toStringTag` values of typed arrays. */
  var typedArrayTags = {};
  typedArrayTags[float32Tag] = typedArrayTags[float64Tag] =
  typedArrayTags[int8Tag] = typedArrayTags[int16Tag] =
  typedArrayTags[int32Tag] = typedArrayTags[uint8Tag] =
  typedArrayTags[uint8ClampedTag] = typedArrayTags[uint16Tag] =
  typedArrayTags[uint32Tag] = true;
  typedArrayTags[argsTag] = typedArrayTags[arrayTag] =
  typedArrayTags[arrayBufferTag] = typedArrayTags[boolTag] =
  typedArrayTags[dateTag] = typedArrayTags[errorTag] =
  typedArrayTags[funcTag] = typedArrayTags[mapTag] =
  typedArrayTags[numberTag] = typedArrayTags[objectTag] =
  typedArrayTags[regexpTag] = typedArrayTags[setTag] =
  typedArrayTags[stringTag] = typedArrayTags[weakMapTag] = false;

  /** Used to identify `toStringTag` values supported by `_.clone`. */
  var cloneableTags = {};
  cloneableTags[argsTag] = cloneableTags[arrayTag] =
  cloneableTags[arrayBufferTag] = cloneableTags[boolTag] =
  cloneableTags[dateTag] = cloneableTags[float32Tag] =
  cloneableTags[float64Tag] = cloneableTags[int8Tag] =
  cloneableTags[int16Tag] = cloneableTags[int32Tag] =
  cloneableTags[numberTag] = cloneableTags[objectTag] =
  cloneableTags[regexpTag] = cloneableTags[stringTag] =
  cloneableTags[uint8Tag] = cloneableTags[uint8ClampedTag] =
  cloneableTags[uint16Tag] = cloneableTags[uint32Tag] = true;
  cloneableTags[errorTag] = cloneableTags[funcTag] =
  cloneableTags[mapTag] = cloneableTags[setTag] =
  cloneableTags[weakMapTag] = false;

  /** Used to map latin-1 supplementary letters to basic latin letters. */
  var deburredLetters = {
    '\xc0': 'A',  '\xc1': 'A', '\xc2': 'A', '\xc3': 'A', '\xc4': 'A', '\xc5': 'A',
    '\xe0': 'a',  '\xe1': 'a', '\xe2': 'a', '\xe3': 'a', '\xe4': 'a', '\xe5': 'a',
    '\xc7': 'C',  '\xe7': 'c',
    '\xd0': 'D',  '\xf0': 'd',
    '\xc8': 'E',  '\xc9': 'E', '\xca': 'E', '\xcb': 'E',
    '\xe8': 'e',  '\xe9': 'e', '\xea': 'e', '\xeb': 'e',
    '\xcC': 'I',  '\xcd': 'I', '\xce': 'I', '\xcf': 'I',
    '\xeC': 'i',  '\xed': 'i', '\xee': 'i', '\xef': 'i',
    '\xd1': 'N',  '\xf1': 'n',
    '\xd2': 'O',  '\xd3': 'O', '\xd4': 'O', '\xd5': 'O', '\xd6': 'O', '\xd8': 'O',
    '\xf2': 'o',  '\xf3': 'o', '\xf4': 'o', '\xf5': 'o', '\xf6': 'o', '\xf8': 'o',
    '\xd9': 'U',  '\xda': 'U', '\xdb': 'U', '\xdc': 'U',
    '\xf9': 'u',  '\xfa': 'u', '\xfb': 'u', '\xfc': 'u',
    '\xdd': 'Y',  '\xfd': 'y', '\xff': 'y',
    '\xc6': 'Ae', '\xe6': 'ae',
    '\xde': 'Th', '\xfe': 'th',
    '\xdf': 'ss'
  };

  /** Used to map characters to HTML entities. */
  var htmlEscapes = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#39;',
    '`': '&#96;'
  };

  /** Used to map HTML entities to characters. */
  var htmlUnescapes = {
    '&amp;': '&',
    '&lt;': '<',
    '&gt;': '>',
    '&quot;': '"',
    '&#39;': "'",
    '&#96;': '`'
  };

  /** Used to determine if values are of the language type `Object`. */
  var objectTypes = {
    'function': true,
    'object': true
  };

  /** Used to escape characters for inclusion in compiled regexes. */
  var regexpEscapes = {
    '0': 'x30', '1': 'x31', '2': 'x32', '3': 'x33', '4': 'x34',
    '5': 'x35', '6': 'x36', '7': 'x37', '8': 'x38', '9': 'x39',
    'A': 'x41', 'B': 'x42', 'C': 'x43', 'D': 'x44', 'E': 'x45', 'F': 'x46',
    'a': 'x61', 'b': 'x62', 'c': 'x63', 'd': 'x64', 'e': 'x65', 'f': 'x66',
    'n': 'x6e', 'r': 'x72', 't': 'x74', 'u': 'x75', 'v': 'x76', 'x': 'x78'
  };

  /** Used to escape characters for inclusion in compiled string literals. */
  var stringEscapes = {
    '\\': '\\',
    "'": "'",
    '\n': 'n',
    '\r': 'r',
    '\u2028': 'u2028',
    '\u2029': 'u2029'
  };

  /** Detect free variable `exports`. */
  var freeExports = objectTypes[typeof exports] && exports && !exports.nodeType && exports;

  /** Detect free variable `module`. */
  var freeModule = objectTypes[typeof module] && module && !module.nodeType && module;

  /** Detect free variable `global` from Node.js. */
  var freeGlobal = freeExports && freeModule && typeof global == 'object' && global && global.Object && global;

  /** Detect free variable `self`. */
  var freeSelf = objectTypes[typeof self] && self && self.Object && self;

  /** Detect free variable `window`. */
  var freeWindow = objectTypes[typeof window] && window && window.Object && window;

  /** Detect the popular CommonJS extension `module.exports`. */
  var moduleExports = freeModule && freeModule.exports === freeExports && freeExports;

  /**
   * Used as a reference to the global object.
   *
   * The `this` value is used if it's the global object to avoid Greasemonkey's
   * restricted `window` object, otherwise the `window` object is used.
   */
  var root = freeGlobal || ((freeWindow !== (this && this.window)) && freeWindow) || freeSelf || this;

  /*--------------------------------------------------------------------------*/

  /**
   * The base implementation of `compareAscending` which compares values and
   * sorts them in ascending order without guaranteeing a stable sort.
   *
   * @private
   * @param {*} value The value to compare.
   * @param {*} other The other value to compare.
   * @returns {number} Returns the sort order indicator for `value`.
   */
  function baseCompareAscending(value, other) {
    if (value !== other) {
      var valIsNull = value === null,
          valIsUndef = value === undefined,
          valIsReflexive = value === value;

      var othIsNull = other === null,
          othIsUndef = other === undefined,
          othIsReflexive = other === other;

      if ((value > other && !othIsNull) || !valIsReflexive ||
          (valIsNull && !othIsUndef && othIsReflexive) ||
          (valIsUndef && othIsReflexive)) {
        return 1;
      }
      if ((value < other && !valIsNull) || !othIsReflexive ||
          (othIsNull && !valIsUndef && valIsReflexive) ||
          (othIsUndef && valIsReflexive)) {
        return -1;
      }
    }
    return 0;
  }

  /**
   * The base implementation of `_.findIndex` and `_.findLastIndex` without
   * support for callback shorthands and `this` binding.
   *
   * @private
   * @param {Array} array The array to search.
   * @param {Function} predicate The function invoked per iteration.
   * @param {boolean} [fromRight] Specify iterating from right to left.
   * @returns {number} Returns the index of the matched value, else `-1`.
   */
  function baseFindIndex(array, predicate, fromRight) {
    var length = array.length,
        index = fromRight ? length : -1;

    while ((fromRight ? index-- : ++index < length)) {
      if (predicate(array[index], index, array)) {
        return index;
      }
    }
    return -1;
  }

  /**
   * The base implementation of `_.indexOf` without support for binary searches.
   *
   * @private
   * @param {Array} array The array to search.
   * @param {*} value The value to search for.
   * @param {number} fromIndex The index to search from.
   * @returns {number} Returns the index of the matched value, else `-1`.
   */
  function baseIndexOf(array, value, fromIndex) {
    if (value !== value) {
      return indexOfNaN(array, fromIndex);
    }
    var index = fromIndex - 1,
        length = array.length;

    while (++index < length) {
      if (array[index] === value) {
        return index;
      }
    }
    return -1;
  }

  /**
   * The base implementation of `_.isFunction` without support for environments
   * with incorrect `typeof` results.
   *
   * @private
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is correctly classified, else `false`.
   */
  function baseIsFunction(value) {
    // Avoid a Chakra JIT bug in compatibility modes of IE 11.
    // See https://github.com/jashkenas/underscore/issues/1621 for more details.
    return typeof value == 'function' || false;
  }

  /**
   * Converts `value` to a string if it's not one. An empty string is returned
   * for `null` or `undefined` values.
   *
   * @private
   * @param {*} value The value to process.
   * @returns {string} Returns the string.
   */
  function baseToString(value) {
    return value == null ? '' : (value + '');
  }

  /**
   * Used by `_.trim` and `_.trimLeft` to get the index of the first character
   * of `string` that is not found in `chars`.
   *
   * @private
   * @param {string} string The string to inspect.
   * @param {string} chars The characters to find.
   * @returns {number} Returns the index of the first character not found in `chars`.
   */
  function charsLeftIndex(string, chars) {
    var index = -1,
        length = string.length;

    while (++index < length && chars.indexOf(string.charAt(index)) > -1) {}
    return index;
  }

  /**
   * Used by `_.trim` and `_.trimRight` to get the index of the last character
   * of `string` that is not found in `chars`.
   *
   * @private
   * @param {string} string The string to inspect.
   * @param {string} chars The characters to find.
   * @returns {number} Returns the index of the last character not found in `chars`.
   */
  function charsRightIndex(string, chars) {
    var index = string.length;

    while (index-- && chars.indexOf(string.charAt(index)) > -1) {}
    return index;
  }

  /**
   * Used by `_.sortBy` to compare transformed elements of a collection and stable
   * sort them in ascending order.
   *
   * @private
   * @param {Object} object The object to compare.
   * @param {Object} other The other object to compare.
   * @returns {number} Returns the sort order indicator for `object`.
   */
  function compareAscending(object, other) {
    return baseCompareAscending(object.criteria, other.criteria) || (object.index - other.index);
  }

  /**
   * Used by `_.sortByOrder` to compare multiple properties of a value to another
   * and stable sort them.
   *
   * If `orders` is unspecified, all valuess are sorted in ascending order. Otherwise,
   * a value is sorted in ascending order if its corresponding order is "asc", and
   * descending if "desc".
   *
   * @private
   * @param {Object} object The object to compare.
   * @param {Object} other The other object to compare.
   * @param {boolean[]} orders The order to sort by for each property.
   * @returns {number} Returns the sort order indicator for `object`.
   */
  function compareMultiple(object, other, orders) {
    var index = -1,
        objCriteria = object.criteria,
        othCriteria = other.criteria,
        length = objCriteria.length,
        ordersLength = orders.length;

    while (++index < length) {
      var result = baseCompareAscending(objCriteria[index], othCriteria[index]);
      if (result) {
        if (index >= ordersLength) {
          return result;
        }
        var order = orders[index];
        return result * ((order === 'asc' || order === true) ? 1 : -1);
      }
    }
    // Fixes an `Array#sort` bug in the JS engine embedded in Adobe applications
    // that causes it, under certain circumstances, to provide the same value for
    // `object` and `other`. See https://github.com/jashkenas/underscore/pull/1247
    // for more details.
    //
    // This also ensures a stable sort in V8 and other engines.
    // See https://code.google.com/p/v8/issues/detail?id=90 for more details.
    return object.index - other.index;
  }

  /**
   * Used by `_.deburr` to convert latin-1 supplementary letters to basic latin letters.
   *
   * @private
   * @param {string} letter The matched letter to deburr.
   * @returns {string} Returns the deburred letter.
   */
  function deburrLetter(letter) {
    return deburredLetters[letter];
  }

  /**
   * Used by `_.escape` to convert characters to HTML entities.
   *
   * @private
   * @param {string} chr The matched character to escape.
   * @returns {string} Returns the escaped character.
   */
  function escapeHtmlChar(chr) {
    return htmlEscapes[chr];
  }

  /**
   * Used by `_.escapeRegExp` to escape characters for inclusion in compiled regexes.
   *
   * @private
   * @param {string} chr The matched character to escape.
   * @param {string} leadingChar The capture group for a leading character.
   * @param {string} whitespaceChar The capture group for a whitespace character.
   * @returns {string} Returns the escaped character.
   */
  function escapeRegExpChar(chr, leadingChar, whitespaceChar) {
    if (leadingChar) {
      chr = regexpEscapes[chr];
    } else if (whitespaceChar) {
      chr = stringEscapes[chr];
    }
    return '\\' + chr;
  }

  /**
   * Used by `_.template` to escape characters for inclusion in compiled string literals.
   *
   * @private
   * @param {string} chr The matched character to escape.
   * @returns {string} Returns the escaped character.
   */
  function escapeStringChar(chr) {
    return '\\' + stringEscapes[chr];
  }

  /**
   * Gets the index at which the first occurrence of `NaN` is found in `array`.
   *
   * @private
   * @param {Array} array The array to search.
   * @param {number} fromIndex The index to search from.
   * @param {boolean} [fromRight] Specify iterating from right to left.
   * @returns {number} Returns the index of the matched `NaN`, else `-1`.
   */
  function indexOfNaN(array, fromIndex, fromRight) {
    var length = array.length,
        index = fromIndex + (fromRight ? 0 : -1);

    while ((fromRight ? index-- : ++index < length)) {
      var other = array[index];
      if (other !== other) {
        return index;
      }
    }
    return -1;
  }

  /**
   * Checks if `value` is object-like.
   *
   * @private
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is object-like, else `false`.
   */
  function isObjectLike(value) {
    return !!value && typeof value == 'object';
  }

  /**
   * Used by `trimmedLeftIndex` and `trimmedRightIndex` to determine if a
   * character code is whitespace.
   *
   * @private
   * @param {number} charCode The character code to inspect.
   * @returns {boolean} Returns `true` if `charCode` is whitespace, else `false`.
   */
  function isSpace(charCode) {
    return ((charCode <= 160 && (charCode >= 9 && charCode <= 13) || charCode == 32 || charCode == 160) || charCode == 5760 || charCode == 6158 ||
      (charCode >= 8192 && (charCode <= 8202 || charCode == 8232 || charCode == 8233 || charCode == 8239 || charCode == 8287 || charCode == 12288 || charCode == 65279)));
  }

  /**
   * Replaces all `placeholder` elements in `array` with an internal placeholder
   * and returns an array of their indexes.
   *
   * @private
   * @param {Array} array The array to modify.
   * @param {*} placeholder The placeholder to replace.
   * @returns {Array} Returns the new array of placeholder indexes.
   */
  function replaceHolders(array, placeholder) {
    var index = -1,
        length = array.length,
        resIndex = -1,
        result = [];

    while (++index < length) {
      if (array[index] === placeholder) {
        array[index] = PLACEHOLDER;
        result[++resIndex] = index;
      }
    }
    return result;
  }

  /**
   * An implementation of `_.uniq` optimized for sorted arrays without support
   * for callback shorthands and `this` binding.
   *
   * @private
   * @param {Array} array The array to inspect.
   * @param {Function} [iteratee] The function invoked per iteration.
   * @returns {Array} Returns the new duplicate-value-free array.
   */
  function sortedUniq(array, iteratee) {
    var seen,
        index = -1,
        length = array.length,
        resIndex = -1,
        result = [];

    while (++index < length) {
      var value = array[index],
          computed = iteratee ? iteratee(value, index, array) : value;

      if (!index || seen !== computed) {
        seen = computed;
        result[++resIndex] = value;
      }
    }
    return result;
  }

  /**
   * Used by `_.trim` and `_.trimLeft` to get the index of the first non-whitespace
   * character of `string`.
   *
   * @private
   * @param {string} string The string to inspect.
   * @returns {number} Returns the index of the first non-whitespace character.
   */
  function trimmedLeftIndex(string) {
    var index = -1,
        length = string.length;

    while (++index < length && isSpace(string.charCodeAt(index))) {}
    return index;
  }

  /**
   * Used by `_.trim` and `_.trimRight` to get the index of the last non-whitespace
   * character of `string`.
   *
   * @private
   * @param {string} string The string to inspect.
   * @returns {number} Returns the index of the last non-whitespace character.
   */
  function trimmedRightIndex(string) {
    var index = string.length;

    while (index-- && isSpace(string.charCodeAt(index))) {}
    return index;
  }

  /**
   * Used by `_.unescape` to convert HTML entities to characters.
   *
   * @private
   * @param {string} chr The matched character to unescape.
   * @returns {string} Returns the unescaped character.
   */
  function unescapeHtmlChar(chr) {
    return htmlUnescapes[chr];
  }

  /*--------------------------------------------------------------------------*/

  /**
   * Create a new pristine `lodash` function using the given `context` object.
   *
   * @static
   * @memberOf _
   * @category Utility
   * @param {Object} [context=root] The context object.
   * @returns {Function} Returns a new `lodash` function.
   * @example
   *
   * _.mixin({ 'foo': _.constant('foo') });
   *
   * var lodash = _.runInContext();
   * lodash.mixin({ 'bar': lodash.constant('bar') });
   *
   * _.isFunction(_.foo);
   * // => true
   * _.isFunction(_.bar);
   * // => false
   *
   * lodash.isFunction(lodash.foo);
   * // => false
   * lodash.isFunction(lodash.bar);
   * // => true
   *
   * // using `context` to mock `Date#getTime` use in `_.now`
   * var mock = _.runInContext({
   *   'Date': function() {
   *     return { 'getTime': getTimeMock };
   *   }
   * });
   *
   * // or creating a suped-up `defer` in Node.js
   * var defer = _.runInContext({ 'setTimeout': setImmediate }).defer;
   */
  function runInContext(context) {
    // Avoid issues with some ES3 environments that attempt to use values, named
    // after built-in constructors like `Object`, for the creation of literals.
    // ES5 clears this up by stating that literals must use built-in constructors.
    // See https://es5.github.io/#x11.1.5 for more details.
    context = context ? _.defaults(root.Object(), context, _.pick(root, contextProps)) : root;

    /** Native constructor references. */
    var Array = context.Array,
        Date = context.Date,
        Error = context.Error,
        Function = context.Function,
        Math = context.Math,
        Number = context.Number,
        Object = context.Object,
        RegExp = context.RegExp,
        String = context.String,
        TypeError = context.TypeError;

    /** Used for native method references. */
    var arrayProto = Array.prototype,
        objectProto = Object.prototype,
        stringProto = String.prototype;

    /** Used to resolve the decompiled source of functions. */
    var fnToString = Function.prototype.toString;

    /** Used to check objects for own properties. */
    var hasOwnProperty = objectProto.hasOwnProperty;

    /** Used to generate unique IDs. */
    var idCounter = 0;

    /**
     * Used to resolve the [`toStringTag`](http://ecma-international.org/ecma-262/6.0/#sec-object.prototype.tostring)
     * of values.
     */
    var objToString = objectProto.toString;

    /** Used to restore the original `_` reference in `_.noConflict`. */
    var oldDash = root._;

    /** Used to detect if a method is native. */
    var reIsNative = RegExp('^' +
      fnToString.call(hasOwnProperty).replace(/[\\^$.*+?()[\]{}|]/g, '\\$&')
      .replace(/hasOwnProperty|(function).*?(?=\\\()| for .+?(?=\\\])/g, '$1.*?') + '$'
    );

    /** Native method references. */
    var ArrayBuffer = context.ArrayBuffer,
        clearTimeout = context.clearTimeout,
        parseFloat = context.parseFloat,
        pow = Math.pow,
        propertyIsEnumerable = objectProto.propertyIsEnumerable,
        Set = getNative(context, 'Set'),
        setTimeout = context.setTimeout,
        splice = arrayProto.splice,
        Uint8Array = context.Uint8Array,
        WeakMap = getNative(context, 'WeakMap');

    /* Native method references for those with the same name as other `lodash` methods. */
    var nativeCeil = Math.ceil,
        nativeCreate = getNative(Object, 'create'),
        nativeFloor = Math.floor,
        nativeIsArray = getNative(Array, 'isArray'),
        nativeIsFinite = context.isFinite,
        nativeKeys = getNative(Object, 'keys'),
        nativeMax = Math.max,
        nativeMin = Math.min,
        nativeNow = getNative(Date, 'now'),
        nativeParseInt = context.parseInt,
        nativeRandom = Math.random;

    /** Used as references for `-Infinity` and `Infinity`. */
    var NEGATIVE_INFINITY = Number.NEGATIVE_INFINITY,
        POSITIVE_INFINITY = Number.POSITIVE_INFINITY;

    /** Used as references for the maximum length and index of an array. */
    var MAX_ARRAY_LENGTH = 4294967295,
        MAX_ARRAY_INDEX = MAX_ARRAY_LENGTH - 1,
        HALF_MAX_ARRAY_LENGTH = MAX_ARRAY_LENGTH >>> 1;

    /**
     * Used as the [maximum length](http://ecma-international.org/ecma-262/6.0/#sec-number.max_safe_integer)
     * of an array-like value.
     */
    var MAX_SAFE_INTEGER = 9007199254740991;

    /** Used to store function metadata. */
    var metaMap = WeakMap && new WeakMap;

    /** Used to lookup unminified function names. */
    var realNames = {};

    /*------------------------------------------------------------------------*/

    /**
     * Creates a `lodash` object which wraps `value` to enable implicit chaining.
     * Methods that operate on and return arrays, collections, and functions can
     * be chained together. Methods that retrieve a single value or may return a
     * primitive value will automatically end the chain returning the unwrapped
     * value. Explicit chaining may be enabled using `_.chain`. The execution of
     * chained methods is lazy, that is, execution is deferred until `_#value`
     * is implicitly or explicitly called.
     *
     * Lazy evaluation allows several methods to support shortcut fusion. Shortcut
     * fusion is an optimization strategy which merge iteratee calls; this can help
     * to avoid the creation of intermediate data structures and greatly reduce the
     * number of iteratee executions.
     *
     * Chaining is supported in custom builds as long as the `_#value` method is
     * directly or indirectly included in the build.
     *
     * In addition to lodash methods, wrappers have `Array` and `String` methods.
     *
     * The wrapper `Array` methods are:
     * `concat`, `join`, `pop`, `push`, `reverse`, `shift`, `slice`, `sort`,
     * `splice`, and `unshift`
     *
     * The wrapper `String` methods are:
     * `replace` and `split`
     *
     * The wrapper methods that support shortcut fusion are:
     * `compact`, `drop`, `dropRight`, `dropRightWhile`, `dropWhile`, `filter`,
     * `first`, `initial`, `last`, `map`, `pluck`, `reject`, `rest`, `reverse`,
     * `slice`, `take`, `takeRight`, `takeRightWhile`, `takeWhile`, `toArray`,
     * and `where`
     *
     * The chainable wrapper methods are:
     * `after`, `ary`, `assign`, `at`, `before`, `bind`, `bindAll`, `bindKey`,
     * `callback`, `chain`, `chunk`, `commit`, `compact`, `concat`, `constant`,
     * `countBy`, `create`, `curry`, `debounce`, `defaults`, `defaultsDeep`,
     * `defer`, `delay`, `difference`, `drop`, `dropRight`, `dropRightWhile`,
     * `dropWhile`, `fill`, `filter`, `flatten`, `flattenDeep`, `flow`, `flowRight`,
     * `forEach`, `forEachRight`, `forIn`, `forInRight`, `forOwn`, `forOwnRight`,
     * `functions`, `groupBy`, `indexBy`, `initial`, `intersection`, `invert`,
     * `invoke`, `keys`, `keysIn`, `map`, `mapKeys`, `mapValues`, `matches`,
     * `matchesProperty`, `memoize`, `merge`, `method`, `methodOf`, `mixin`,
     * `modArgs`, `negate`, `omit`, `once`, `pairs`, `partial`, `partialRight`,
     * `partition`, `pick`, `plant`, `pluck`, `property`, `propertyOf`, `pull`,
     * `pullAt`, `push`, `range`, `rearg`, `reject`, `remove`, `rest`, `restParam`,
     * `reverse`, `set`, `shuffle`, `slice`, `sort`, `sortBy`, `sortByAll`,
     * `sortByOrder`, `splice`, `spread`, `take`, `takeRight`, `takeRightWhile`,
     * `takeWhile`, `tap`, `throttle`, `thru`, `times`, `toArray`, `toPlainObject`,
     * `transform`, `union`, `uniq`, `unshift`, `unzip`, `unzipWith`, `values`,
     * `valuesIn`, `where`, `without`, `wrap`, `xor`, `zip`, `zipObject`, `zipWith`
     *
     * The wrapper methods that are **not** chainable by default are:
     * `add`, `attempt`, `camelCase`, `capitalize`, `ceil`, `clone`, `cloneDeep`,
     * `deburr`, `endsWith`, `escape`, `escapeRegExp`, `every`, `find`, `findIndex`,
     * `findKey`, `findLast`, `findLastIndex`, `findLastKey`, `findWhere`, `first`,
     * `floor`, `get`, `gt`, `gte`, `has`, `identity`, `includes`, `indexOf`,
     * `inRange`, `isArguments`, `isArray`, `isBoolean`, `isDate`, `isElement`,
     * `isEmpty`, `isEqual`, `isError`, `isFinite` `isFunction`, `isMatch`,
     * `isNative`, `isNaN`, `isNull`, `isNumber`, `isObject`, `isPlainObject`,
     * `isRegExp`, `isString`, `isUndefined`, `isTypedArray`, `join`, `kebabCase`,
     * `last`, `lastIndexOf`, `lt`, `lte`, `max`, `min`, `noConflict`, `noop`,
     * `now`, `pad`, `padLeft`, `padRight`, `parseInt`, `pop`, `random`, `reduce`,
     * `reduceRight`, `repeat`, `result`, `round`, `runInContext`, `shift`, `size`,
     * `snakeCase`, `some`, `sortedIndex`, `sortedLastIndex`, `startCase`,
     * `startsWith`, `sum`, `template`, `trim`, `trimLeft`, `trimRight`, `trunc`,
     * `unescape`, `uniqueId`, `value`, and `words`
     *
     * The wrapper method `sample` will return a wrapped value when `n` is provided,
     * otherwise an unwrapped value is returned.
     *
     * @name _
     * @constructor
     * @category Chain
     * @param {*} value The value to wrap in a `lodash` instance.
     * @returns {Object} Returns the new `lodash` wrapper instance.
     * @example
     *
     * var wrapped = _([1, 2, 3]);
     *
     * // returns an unwrapped value
     * wrapped.reduce(function(total, n) {
     *   return total + n;
     * });
     * // => 6
     *
     * // returns a wrapped value
     * var squares = wrapped.map(function(n) {
     *   return n * n;
     * });
     *
     * _.isArray(squares);
     * // => false
     *
     * _.isArray(squares.value());
     * // => true
     */
    function lodash(value) {
      if (isObjectLike(value) && !isArray(value) && !(value instanceof LazyWrapper)) {
        if (value instanceof LodashWrapper) {
          return value;
        }
        if (hasOwnProperty.call(value, '__chain__') && hasOwnProperty.call(value, '__wrapped__')) {
          return wrapperClone(value);
        }
      }
      return new LodashWrapper(value);
    }

    /**
     * The function whose prototype all chaining wrappers inherit from.
     *
     * @private
     */
    function baseLodash() {
      // No operation performed.
    }

    /**
     * The base constructor for creating `lodash` wrapper objects.
     *
     * @private
     * @param {*} value The value to wrap.
     * @param {boolean} [chainAll] Enable chaining for all wrapper methods.
     * @param {Array} [actions=[]] Actions to peform to resolve the unwrapped value.
     */
    function LodashWrapper(value, chainAll, actions) {
      this.__wrapped__ = value;
      this.__actions__ = actions || [];
      this.__chain__ = !!chainAll;
    }

    /**
     * An object environment feature flags.
     *
     * @static
     * @memberOf _
     * @type Object
     */
    var support = lodash.support = {};

    /**
     * By default, the template delimiters used by lodash are like those in
     * embedded Ruby (ERB). Change the following template settings to use
     * alternative delimiters.
     *
     * @static
     * @memberOf _
     * @type Object
     */
    lodash.templateSettings = {

      /**
       * Used to detect `data` property values to be HTML-escaped.
       *
       * @memberOf _.templateSettings
       * @type RegExp
       */
      'escape': reEscape,

      /**
       * Used to detect code to be evaluated.
       *
       * @memberOf _.templateSettings
       * @type RegExp
       */
      'evaluate': reEvaluate,

      /**
       * Used to detect `data` property values to inject.
       *
       * @memberOf _.templateSettings
       * @type RegExp
       */
      'interpolate': reInterpolate,

      /**
       * Used to reference the data object in the template text.
       *
       * @memberOf _.templateSettings
       * @type string
       */
      'variable': '',

      /**
       * Used to import variables into the compiled template.
       *
       * @memberOf _.templateSettings
       * @type Object
       */
      'imports': {

        /**
         * A reference to the `lodash` function.
         *
         * @memberOf _.templateSettings.imports
         * @type Function
         */
        '_': lodash
      }
    };

    /*------------------------------------------------------------------------*/

    /**
     * Creates a lazy wrapper object which wraps `value` to enable lazy evaluation.
     *
     * @private
     * @param {*} value The value to wrap.
     */
    function LazyWrapper(value) {
      this.__wrapped__ = value;
      this.__actions__ = [];
      this.__dir__ = 1;
      this.__filtered__ = false;
      this.__iteratees__ = [];
      this.__takeCount__ = POSITIVE_INFINITY;
      this.__views__ = [];
    }

    /**
     * Creates a clone of the lazy wrapper object.
     *
     * @private
     * @name clone
     * @memberOf LazyWrapper
     * @returns {Object} Returns the cloned `LazyWrapper` object.
     */
    function lazyClone() {
      var result = new LazyWrapper(this.__wrapped__);
      result.__actions__ = arrayCopy(this.__actions__);
      result.__dir__ = this.__dir__;
      result.__filtered__ = this.__filtered__;
      result.__iteratees__ = arrayCopy(this.__iteratees__);
      result.__takeCount__ = this.__takeCount__;
      result.__views__ = arrayCopy(this.__views__);
      return result;
    }

    /**
     * Reverses the direction of lazy iteration.
     *
     * @private
     * @name reverse
     * @memberOf LazyWrapper
     * @returns {Object} Returns the new reversed `LazyWrapper` object.
     */
    function lazyReverse() {
      if (this.__filtered__) {
        var result = new LazyWrapper(this);
        result.__dir__ = -1;
        result.__filtered__ = true;
      } else {
        result = this.clone();
        result.__dir__ *= -1;
      }
      return result;
    }

    /**
     * Extracts the unwrapped value from its lazy wrapper.
     *
     * @private
     * @name value
     * @memberOf LazyWrapper
     * @returns {*} Returns the unwrapped value.
     */
    function lazyValue() {
      var array = this.__wrapped__.value(),
          dir = this.__dir__,
          isArr = isArray(array),
          isRight = dir < 0,
          arrLength = isArr ? array.length : 0,
          view = getView(0, arrLength, this.__views__),
          start = view.start,
          end = view.end,
          length = end - start,
          index = isRight ? end : (start - 1),
          iteratees = this.__iteratees__,
          iterLength = iteratees.length,
          resIndex = 0,
          takeCount = nativeMin(length, this.__takeCount__);

      if (!isArr || arrLength < LARGE_ARRAY_SIZE || (arrLength == length && takeCount == length)) {
        return baseWrapperValue((isRight && isArr) ? array.reverse() : array, this.__actions__);
      }
      var result = [];

      outer:
      while (length-- && resIndex < takeCount) {
        index += dir;

        var iterIndex = -1,
            value = array[index];

        while (++iterIndex < iterLength) {
          var data = iteratees[iterIndex],
              iteratee = data.iteratee,
              type = data.type,
              computed = iteratee(value);

          if (type == LAZY_MAP_FLAG) {
            value = computed;
          } else if (!computed) {
            if (type == LAZY_FILTER_FLAG) {
              continue outer;
            } else {
              break outer;
            }
          }
        }
        result[resIndex++] = value;
      }
      return result;
    }

    /*------------------------------------------------------------------------*/

    /**
     * Creates a cache object to store key/value pairs.
     *
     * @private
     * @static
     * @name Cache
     * @memberOf _.memoize
     */
    function MapCache() {
      this.__data__ = {};
    }

    /**
     * Removes `key` and its value from the cache.
     *
     * @private
     * @name delete
     * @memberOf _.memoize.Cache
     * @param {string} key The key of the value to remove.
     * @returns {boolean} Returns `true` if the entry was removed successfully, else `false`.
     */
    function mapDelete(key) {
      return this.has(key) && delete this.__data__[key];
    }

    /**
     * Gets the cached value for `key`.
     *
     * @private
     * @name get
     * @memberOf _.memoize.Cache
     * @param {string} key The key of the value to get.
     * @returns {*} Returns the cached value.
     */
    function mapGet(key) {
      return key == '__proto__' ? undefined : this.__data__[key];
    }

    /**
     * Checks if a cached value for `key` exists.
     *
     * @private
     * @name has
     * @memberOf _.memoize.Cache
     * @param {string} key The key of the entry to check.
     * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
     */
    function mapHas(key) {
      return key != '__proto__' && hasOwnProperty.call(this.__data__, key);
    }

    /**
     * Sets `value` to `key` of the cache.
     *
     * @private
     * @name set
     * @memberOf _.memoize.Cache
     * @param {string} key The key of the value to cache.
     * @param {*} value The value to cache.
     * @returns {Object} Returns the cache object.
     */
    function mapSet(key, value) {
      if (key != '__proto__') {
        this.__data__[key] = value;
      }
      return this;
    }

    /*------------------------------------------------------------------------*/

    /**
     *
     * Creates a cache object to store unique values.
     *
     * @private
     * @param {Array} [values] The values to cache.
     */
    function SetCache(values) {
      var length = values ? values.length : 0;

      this.data = { 'hash': nativeCreate(null), 'set': new Set };
      while (length--) {
        this.push(values[length]);
      }
    }

    /**
     * Checks if `value` is in `cache` mimicking the return signature of
     * `_.indexOf` by returning `0` if the value is found, else `-1`.
     *
     * @private
     * @param {Object} cache The cache to search.
     * @param {*} value The value to search for.
     * @returns {number} Returns `0` if `value` is found, else `-1`.
     */
    function cacheIndexOf(cache, value) {
      var data = cache.data,
          result = (typeof value == 'string' || isObject(value)) ? data.set.has(value) : data.hash[value];

      return result ? 0 : -1;
    }

    /**
     * Adds `value` to the cache.
     *
     * @private
     * @name push
     * @memberOf SetCache
     * @param {*} value The value to cache.
     */
    function cachePush(value) {
      var data = this.data;
      if (typeof value == 'string' || isObject(value)) {
        data.set.add(value);
      } else {
        data.hash[value] = true;
      }
    }

    /*------------------------------------------------------------------------*/

    /**
     * Creates a new array joining `array` with `other`.
     *
     * @private
     * @param {Array} array The array to join.
     * @param {Array} other The other array to join.
     * @returns {Array} Returns the new concatenated array.
     */
    function arrayConcat(array, other) {
      var index = -1,
          length = array.length,
          othIndex = -1,
          othLength = other.length,
          result = Array(length + othLength);

      while (++index < length) {
        result[index] = array[index];
      }
      while (++othIndex < othLength) {
        result[index++] = other[othIndex];
      }
      return result;
    }

    /**
     * Copies the values of `source` to `array`.
     *
     * @private
     * @param {Array} source The array to copy values from.
     * @param {Array} [array=[]] The array to copy values to.
     * @returns {Array} Returns `array`.
     */
    function arrayCopy(source, array) {
      var index = -1,
          length = source.length;

      array || (array = Array(length));
      while (++index < length) {
        array[index] = source[index];
      }
      return array;
    }

    /**
     * A specialized version of `_.forEach` for arrays without support for callback
     * shorthands and `this` binding.
     *
     * @private
     * @param {Array} array The array to iterate over.
     * @param {Function} iteratee The function invoked per iteration.
     * @returns {Array} Returns `array`.
     */
    function arrayEach(array, iteratee) {
      var index = -1,
          length = array.length;

      while (++index < length) {
        if (iteratee(array[index], index, array) === false) {
          break;
        }
      }
      return array;
    }

    /**
     * A specialized version of `_.forEachRight` for arrays without support for
     * callback shorthands and `this` binding.
     *
     * @private
     * @param {Array} array The array to iterate over.
     * @param {Function} iteratee The function invoked per iteration.
     * @returns {Array} Returns `array`.
     */
    function arrayEachRight(array, iteratee) {
      var length = array.length;

      while (length--) {
        if (iteratee(array[length], length, array) === false) {
          break;
        }
      }
      return array;
    }

    /**
     * A specialized version of `_.every` for arrays without support for callback
     * shorthands and `this` binding.
     *
     * @private
     * @param {Array} array The array to iterate over.
     * @param {Function} predicate The function invoked per iteration.
     * @returns {boolean} Returns `true` if all elements pass the predicate check,
     *  else `false`.
     */
    function arrayEvery(array, predicate) {
      var index = -1,
          length = array.length;

      while (++index < length) {
        if (!predicate(array[index], index, array)) {
          return false;
        }
      }
      return true;
    }

    /**
     * A specialized version of `baseExtremum` for arrays which invokes `iteratee`
     * with one argument: (value).
     *
     * @private
     * @param {Array} array The array to iterate over.
     * @param {Function} iteratee The function invoked per iteration.
     * @param {Function} comparator The function used to compare values.
     * @param {*} exValue The initial extremum value.
     * @returns {*} Returns the extremum value.
     */
    function arrayExtremum(array, iteratee, comparator, exValue) {
      var index = -1,
          length = array.length,
          computed = exValue,
          result = computed;

      while (++index < length) {
        var value = array[index],
            current = +iteratee(value);

        if (comparator(current, computed)) {
          computed = current;
          result = value;
        }
      }
      return result;
    }

    /**
     * A specialized version of `_.filter` for arrays without support for callback
     * shorthands and `this` binding.
     *
     * @private
     * @param {Array} array The array to iterate over.
     * @param {Function} predicate The function invoked per iteration.
     * @returns {Array} Returns the new filtered array.
     */
    function arrayFilter(array, predicate) {
      var index = -1,
          length = array.length,
          resIndex = -1,
          result = [];

      while (++index < length) {
        var value = array[index];
        if (predicate(value, index, array)) {
          result[++resIndex] = value;
        }
      }
      return result;
    }

    /**
     * A specialized version of `_.map` for arrays without support for callback
     * shorthands and `this` binding.
     *
     * @private
     * @param {Array} array The array to iterate over.
     * @param {Function} iteratee The function invoked per iteration.
     * @returns {Array} Returns the new mapped array.
     */
    function arrayMap(array, iteratee) {
      var index = -1,
          length = array.length,
          result = Array(length);

      while (++index < length) {
        result[index] = iteratee(array[index], index, array);
      }
      return result;
    }

    /**
     * Appends the elements of `values` to `array`.
     *
     * @private
     * @param {Array} array The array to modify.
     * @param {Array} values The values to append.
     * @returns {Array} Returns `array`.
     */
    function arrayPush(array, values) {
      var index = -1,
          length = values.length,
          offset = array.length;

      while (++index < length) {
        array[offset + index] = values[index];
      }
      return array;
    }

    /**
     * A specialized version of `_.reduce` for arrays without support for callback
     * shorthands and `this` binding.
     *
     * @private
     * @param {Array} array The array to iterate over.
     * @param {Function} iteratee The function invoked per iteration.
     * @param {*} [accumulator] The initial value.
     * @param {boolean} [initFromArray] Specify using the first element of `array`
     *  as the initial value.
     * @returns {*} Returns the accumulated value.
     */
    function arrayReduce(array, iteratee, accumulator, initFromArray) {
      var index = -1,
          length = array.length;

      if (initFromArray && length) {
        accumulator = array[++index];
      }
      while (++index < length) {
        accumulator = iteratee(accumulator, array[index], index, array);
      }
      return accumulator;
    }

    /**
     * A specialized version of `_.reduceRight` for arrays without support for
     * callback shorthands and `this` binding.
     *
     * @private
     * @param {Array} array The array to iterate over.
     * @param {Function} iteratee The function invoked per iteration.
     * @param {*} [accumulator] The initial value.
     * @param {boolean} [initFromArray] Specify using the last element of `array`
     *  as the initial value.
     * @returns {*} Returns the accumulated value.
     */
    function arrayReduceRight(array, iteratee, accumulator, initFromArray) {
      var length = array.length;
      if (initFromArray && length) {
        accumulator = array[--length];
      }
      while (length--) {
        accumulator = iteratee(accumulator, array[length], length, array);
      }
      return accumulator;
    }

    /**
     * A specialized version of `_.some` for arrays without support for callback
     * shorthands and `this` binding.
     *
     * @private
     * @param {Array} array The array to iterate over.
     * @param {Function} predicate The function invoked per iteration.
     * @returns {boolean} Returns `true` if any element passes the predicate check,
     *  else `false`.
     */
    function arraySome(array, predicate) {
      var index = -1,
          length = array.length;

      while (++index < length) {
        if (predicate(array[index], index, array)) {
          return true;
        }
      }
      return false;
    }

    /**
     * A specialized version of `_.sum` for arrays without support for callback
     * shorthands and `this` binding..
     *
     * @private
     * @param {Array} array The array to iterate over.
     * @param {Function} iteratee The function invoked per iteration.
     * @returns {number} Returns the sum.
     */
    function arraySum(array, iteratee) {
      var length = array.length,
          result = 0;

      while (length--) {
        result += +iteratee(array[length]) || 0;
      }
      return result;
    }

    /**
     * Used by `_.defaults` to customize its `_.assign` use.
     *
     * @private
     * @param {*} objectValue The destination object property value.
     * @param {*} sourceValue The source object property value.
     * @returns {*} Returns the value to assign to the destination object.
     */
    function assignDefaults(objectValue, sourceValue) {
      return objectValue === undefined ? sourceValue : objectValue;
    }

    /**
     * Used by `_.template` to customize its `_.assign` use.
     *
     * **Note:** This function is like `assignDefaults` except that it ignores
     * inherited property values when checking if a property is `undefined`.
     *
     * @private
     * @param {*} objectValue The destination object property value.
     * @param {*} sourceValue The source object property value.
     * @param {string} key The key associated with the object and source values.
     * @param {Object} object The destination object.
     * @returns {*} Returns the value to assign to the destination object.
     */
    function assignOwnDefaults(objectValue, sourceValue, key, object) {
      return (objectValue === undefined || !hasOwnProperty.call(object, key))
        ? sourceValue
        : objectValue;
    }

    /**
     * A specialized version of `_.assign` for customizing assigned values without
     * support for argument juggling, multiple sources, and `this` binding `customizer`
     * functions.
     *
     * @private
     * @param {Object} object The destination object.
     * @param {Object} source The source object.
     * @param {Function} customizer The function to customize assigned values.
     * @returns {Object} Returns `object`.
     */
    function assignWith(object, source, customizer) {
      var index = -1,
          props = keys(source),
          length = props.length;

      while (++index < length) {
        var key = props[index],
            value = object[key],
            result = customizer(value, source[key], key, object, source);

        if ((result === result ? (result !== value) : (value === value)) ||
            (value === undefined && !(key in object))) {
          object[key] = result;
        }
      }
      return object;
    }

    /**
     * The base implementation of `_.assign` without support for argument juggling,
     * multiple sources, and `customizer` functions.
     *
     * @private
     * @param {Object} object The destination object.
     * @param {Object} source The source object.
     * @returns {Object} Returns `object`.
     */
    function baseAssign(object, source) {
      return source == null
        ? object
        : baseCopy(source, keys(source), object);
    }

    /**
     * The base implementation of `_.at` without support for string collections
     * and individual key arguments.
     *
     * @private
     * @param {Array|Object} collection The collection to iterate over.
     * @param {number[]|string[]} props The property names or indexes of elements to pick.
     * @returns {Array} Returns the new array of picked elements.
     */
    function baseAt(collection, props) {
      var index = -1,
          isNil = collection == null,
          isArr = !isNil && isArrayLike(collection),
          length = isArr ? collection.length : 0,
          propsLength = props.length,
          result = Array(propsLength);

      while(++index < propsLength) {
        var key = props[index];
        if (isArr) {
          result[index] = isIndex(key, length) ? collection[key] : undefined;
        } else {
          result[index] = isNil ? undefined : collection[key];
        }
      }
      return result;
    }

    /**
     * Copies properties of `source` to `object`.
     *
     * @private
     * @param {Object} source The object to copy properties from.
     * @param {Array} props The property names to copy.
     * @param {Object} [object={}] The object to copy properties to.
     * @returns {Object} Returns `object`.
     */
    function baseCopy(source, props, object) {
      object || (object = {});

      var index = -1,
          length = props.length;

      while (++index < length) {
        var key = props[index];
        object[key] = source[key];
      }
      return object;
    }

    /**
     * The base implementation of `_.callback` which supports specifying the
     * number of arguments to provide to `func`.
     *
     * @private
     * @param {*} [func=_.identity] The value to convert to a callback.
     * @param {*} [thisArg] The `this` binding of `func`.
     * @param {number} [argCount] The number of arguments to provide to `func`.
     * @returns {Function} Returns the callback.
     */
    function baseCallback(func, thisArg, argCount) {
      var type = typeof func;
      if (type == 'function') {
        return thisArg === undefined
          ? func
          : bindCallback(func, thisArg, argCount);
      }
      if (func == null) {
        return identity;
      }
      if (type == 'object') {
        return baseMatches(func);
      }
      return thisArg === undefined
        ? property(func)
        : baseMatchesProperty(func, thisArg);
    }

    /**
     * The base implementation of `_.clone` without support for argument juggling
     * and `this` binding `customizer` functions.
     *
     * @private
     * @param {*} value The value to clone.
     * @param {boolean} [isDeep] Specify a deep clone.
     * @param {Function} [customizer] The function to customize cloning values.
     * @param {string} [key] The key of `value`.
     * @param {Object} [object] The object `value` belongs to.
     * @param {Array} [stackA=[]] Tracks traversed source objects.
     * @param {Array} [stackB=[]] Associates clones with source counterparts.
     * @returns {*} Returns the cloned value.
     */
    function baseClone(value, isDeep, customizer, key, object, stackA, stackB) {
      var result;
      if (customizer) {
        result = object ? customizer(value, key, object) : customizer(value);
      }
      if (result !== undefined) {
        return result;
      }
      if (!isObject(value)) {
        return value;
      }
      var isArr = isArray(value);
      if (isArr) {
        result = initCloneArray(value);
        if (!isDeep) {
          return arrayCopy(value, result);
        }
      } else {
        var tag = objToString.call(value),
            isFunc = tag == funcTag;

        if (tag == objectTag || tag == argsTag || (isFunc && !object)) {
          result = initCloneObject(isFunc ? {} : value);
          if (!isDeep) {
            return baseAssign(result, value);
          }
        } else {
          return cloneableTags[tag]
            ? initCloneByTag(value, tag, isDeep)
            : (object ? value : {});
        }
      }
      // Check for circular references and return its corresponding clone.
      stackA || (stackA = []);
      stackB || (stackB = []);

      var length = stackA.length;
      while (length--) {
        if (stackA[length] == value) {
          return stackB[length];
        }
      }
      // Add the source value to the stack of traversed objects and associate it with its clone.
      stackA.push(value);
      stackB.push(result);

      // Recursively populate clone (susceptible to call stack limits).
      (isArr ? arrayEach : baseForOwn)(value, function(subValue, key) {
        result[key] = baseClone(subValue, isDeep, customizer, key, value, stackA, stackB);
      });
      return result;
    }

    /**
     * The base implementation of `_.create` without support for assigning
     * properties to the created object.
     *
     * @private
     * @param {Object} prototype The object to inherit from.
     * @returns {Object} Returns the new object.
     */
    var baseCreate = (function() {
      function object() {}
      return function(prototype) {
        if (isObject(prototype)) {
          object.prototype = prototype;
          var result = new object;
          object.prototype = undefined;
        }
        return result || {};
      };
    }());

    /**
     * The base implementation of `_.delay` and `_.defer` which accepts an index
     * of where to slice the arguments to provide to `func`.
     *
     * @private
     * @param {Function} func The function to delay.
     * @param {number} wait The number of milliseconds to delay invocation.
     * @param {Object} args The arguments provide to `func`.
     * @returns {number} Returns the timer id.
     */
    function baseDelay(func, wait, args) {
      if (typeof func != 'function') {
        throw new TypeError(FUNC_ERROR_TEXT);
      }
      return setTimeout(function() { func.apply(undefined, args); }, wait);
    }

    /**
     * The base implementation of `_.difference` which accepts a single array
     * of values to exclude.
     *
     * @private
     * @param {Array} array The array to inspect.
     * @param {Array} values The values to exclude.
     * @returns {Array} Returns the new array of filtered values.
     */
    function baseDifference(array, values) {
      var length = array ? array.length : 0,
          result = [];

      if (!length) {
        return result;
      }
      var index = -1,
          indexOf = getIndexOf(),
          isCommon = indexOf == baseIndexOf,
          cache = (isCommon && values.length >= LARGE_ARRAY_SIZE) ? createCache(values) : null,
          valuesLength = values.length;

      if (cache) {
        indexOf = cacheIndexOf;
        isCommon = false;
        values = cache;
      }
      outer:
      while (++index < length) {
        var value = array[index];

        if (isCommon && value === value) {
          var valuesIndex = valuesLength;
          while (valuesIndex--) {
            if (values[valuesIndex] === value) {
              continue outer;
            }
          }
          result.push(value);
        }
        else if (indexOf(values, value, 0) < 0) {
          result.push(value);
        }
      }
      return result;
    }

    /**
     * The base implementation of `_.forEach` without support for callback
     * shorthands and `this` binding.
     *
     * @private
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function} iteratee The function invoked per iteration.
     * @returns {Array|Object|string} Returns `collection`.
     */
    var baseEach = createBaseEach(baseForOwn);

    /**
     * The base implementation of `_.forEachRight` without support for callback
     * shorthands and `this` binding.
     *
     * @private
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function} iteratee The function invoked per iteration.
     * @returns {Array|Object|string} Returns `collection`.
     */
    var baseEachRight = createBaseEach(baseForOwnRight, true);

    /**
     * The base implementation of `_.every` without support for callback
     * shorthands and `this` binding.
     *
     * @private
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function} predicate The function invoked per iteration.
     * @returns {boolean} Returns `true` if all elements pass the predicate check,
     *  else `false`
     */
    function baseEvery(collection, predicate) {
      var result = true;
      baseEach(collection, function(value, index, collection) {
        result = !!predicate(value, index, collection);
        return result;
      });
      return result;
    }

    /**
     * Gets the extremum value of `collection` invoking `iteratee` for each value
     * in `collection` to generate the criterion by which the value is ranked.
     * The `iteratee` is invoked with three arguments: (value, index|key, collection).
     *
     * @private
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function} iteratee The function invoked per iteration.
     * @param {Function} comparator The function used to compare values.
     * @param {*} exValue The initial extremum value.
     * @returns {*} Returns the extremum value.
     */
    function baseExtremum(collection, iteratee, comparator, exValue) {
      var computed = exValue,
          result = computed;

      baseEach(collection, function(value, index, collection) {
        var current = +iteratee(value, index, collection);
        if (comparator(current, computed) || (current === exValue && current === result)) {
          computed = current;
          result = value;
        }
      });
      return result;
    }

    /**
     * The base implementation of `_.fill` without an iteratee call guard.
     *
     * @private
     * @param {Array} array The array to fill.
     * @param {*} value The value to fill `array` with.
     * @param {number} [start=0] The start position.
     * @param {number} [end=array.length] The end position.
     * @returns {Array} Returns `array`.
     */
    function baseFill(array, value, start, end) {
      var length = array.length;

      start = start == null ? 0 : (+start || 0);
      if (start < 0) {
        start = -start > length ? 0 : (length + start);
      }
      end = (end === undefined || end > length) ? length : (+end || 0);
      if (end < 0) {
        end += length;
      }
      length = start > end ? 0 : (end >>> 0);
      start >>>= 0;

      while (start < length) {
        array[start++] = value;
      }
      return array;
    }

    /**
     * The base implementation of `_.filter` without support for callback
     * shorthands and `this` binding.
     *
     * @private
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function} predicate The function invoked per iteration.
     * @returns {Array} Returns the new filtered array.
     */
    function baseFilter(collection, predicate) {
      var result = [];
      baseEach(collection, function(value, index, collection) {
        if (predicate(value, index, collection)) {
          result.push(value);
        }
      });
      return result;
    }

    /**
     * The base implementation of `_.find`, `_.findLast`, `_.findKey`, and `_.findLastKey`,
     * without support for callback shorthands and `this` binding, which iterates
     * over `collection` using the provided `eachFunc`.
     *
     * @private
     * @param {Array|Object|string} collection The collection to search.
     * @param {Function} predicate The function invoked per iteration.
     * @param {Function} eachFunc The function to iterate over `collection`.
     * @param {boolean} [retKey] Specify returning the key of the found element
     *  instead of the element itself.
     * @returns {*} Returns the found element or its key, else `undefined`.
     */
    function baseFind(collection, predicate, eachFunc, retKey) {
      var result;
      eachFunc(collection, function(value, key, collection) {
        if (predicate(value, key, collection)) {
          result = retKey ? key : value;
          return false;
        }
      });
      return result;
    }

    /**
     * The base implementation of `_.flatten` with added support for restricting
     * flattening and specifying the start index.
     *
     * @private
     * @param {Array} array The array to flatten.
     * @param {boolean} [isDeep] Specify a deep flatten.
     * @param {boolean} [isStrict] Restrict flattening to arrays-like objects.
     * @param {Array} [result=[]] The initial result value.
     * @returns {Array} Returns the new flattened array.
     */
    function baseFlatten(array, isDeep, isStrict, result) {
      result || (result = []);

      var index = -1,
          length = array.length;

      while (++index < length) {
        var value = array[index];
        if (isObjectLike(value) && isArrayLike(value) &&
            (isStrict || isArray(value) || isArguments(value))) {
          if (isDeep) {
            // Recursively flatten arrays (susceptible to call stack limits).
            baseFlatten(value, isDeep, isStrict, result);
          } else {
            arrayPush(result, value);
          }
        } else if (!isStrict) {
          result[result.length] = value;
        }
      }
      return result;
    }

    /**
     * The base implementation of `baseForIn` and `baseForOwn` which iterates
     * over `object` properties returned by `keysFunc` invoking `iteratee` for
     * each property. Iteratee functions may exit iteration early by explicitly
     * returning `false`.
     *
     * @private
     * @param {Object} object The object to iterate over.
     * @param {Function} iteratee The function invoked per iteration.
     * @param {Function} keysFunc The function to get the keys of `object`.
     * @returns {Object} Returns `object`.
     */
    var baseFor = createBaseFor();

    /**
     * This function is like `baseFor` except that it iterates over properties
     * in the opposite order.
     *
     * @private
     * @param {Object} object The object to iterate over.
     * @param {Function} iteratee The function invoked per iteration.
     * @param {Function} keysFunc The function to get the keys of `object`.
     * @returns {Object} Returns `object`.
     */
    var baseForRight = createBaseFor(true);

    /**
     * The base implementation of `_.forIn` without support for callback
     * shorthands and `this` binding.
     *
     * @private
     * @param {Object} object The object to iterate over.
     * @param {Function} iteratee The function invoked per iteration.
     * @returns {Object} Returns `object`.
     */
    function baseForIn(object, iteratee) {
      return baseFor(object, iteratee, keysIn);
    }

    /**
     * The base implementation of `_.forOwn` without support for callback
     * shorthands and `this` binding.
     *
     * @private
     * @param {Object} object The object to iterate over.
     * @param {Function} iteratee The function invoked per iteration.
     * @returns {Object} Returns `object`.
     */
    function baseForOwn(object, iteratee) {
      return baseFor(object, iteratee, keys);
    }

    /**
     * The base implementation of `_.forOwnRight` without support for callback
     * shorthands and `this` binding.
     *
     * @private
     * @param {Object} object The object to iterate over.
     * @param {Function} iteratee The function invoked per iteration.
     * @returns {Object} Returns `object`.
     */
    function baseForOwnRight(object, iteratee) {
      return baseForRight(object, iteratee, keys);
    }

    /**
     * The base implementation of `_.functions` which creates an array of
     * `object` function property names filtered from those provided.
     *
     * @private
     * @param {Object} object The object to inspect.
     * @param {Array} props The property names to filter.
     * @returns {Array} Returns the new array of filtered property names.
     */
    function baseFunctions(object, props) {
      var index = -1,
          length = props.length,
          resIndex = -1,
          result = [];

      while (++index < length) {
        var key = props[index];
        if (isFunction(object[key])) {
          result[++resIndex] = key;
        }
      }
      return result;
    }

    /**
     * The base implementation of `get` without support for string paths
     * and default values.
     *
     * @private
     * @param {Object} object The object to query.
     * @param {Array} path The path of the property to get.
     * @param {string} [pathKey] The key representation of path.
     * @returns {*} Returns the resolved value.
     */
    function baseGet(object, path, pathKey) {
      if (object == null) {
        return;
      }
      if (pathKey !== undefined && pathKey in toObject(object)) {
        path = [pathKey];
      }
      var index = 0,
          length = path.length;

      while (object != null && index < length) {
        object = object[path[index++]];
      }
      return (index && index == length) ? object : undefined;
    }

    /**
     * The base implementation of `_.isEqual` without support for `this` binding
     * `customizer` functions.
     *
     * @private
     * @param {*} value The value to compare.
     * @param {*} other The other value to compare.
     * @param {Function} [customizer] The function to customize comparing values.
     * @param {boolean} [isLoose] Specify performing partial comparisons.
     * @param {Array} [stackA] Tracks traversed `value` objects.
     * @param {Array} [stackB] Tracks traversed `other` objects.
     * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
     */
    function baseIsEqual(value, other, customizer, isLoose, stackA, stackB) {
      if (value === other) {
        return true;
      }
      if (value == null || other == null || (!isObject(value) && !isObjectLike(other))) {
        return value !== value && other !== other;
      }
      return baseIsEqualDeep(value, other, baseIsEqual, customizer, isLoose, stackA, stackB);
    }

    /**
     * A specialized version of `baseIsEqual` for arrays and objects which performs
     * deep comparisons and tracks traversed objects enabling objects with circular
     * references to be compared.
     *
     * @private
     * @param {Object} object The object to compare.
     * @param {Object} other The other object to compare.
     * @param {Function} equalFunc The function to determine equivalents of values.
     * @param {Function} [customizer] The function to customize comparing objects.
     * @param {boolean} [isLoose] Specify performing partial comparisons.
     * @param {Array} [stackA=[]] Tracks traversed `value` objects.
     * @param {Array} [stackB=[]] Tracks traversed `other` objects.
     * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
     */
    function baseIsEqualDeep(object, other, equalFunc, customizer, isLoose, stackA, stackB) {
      var objIsArr = isArray(object),
          othIsArr = isArray(other),
          objTag = arrayTag,
          othTag = arrayTag;

      if (!objIsArr) {
        objTag = objToString.call(object);
        if (objTag == argsTag) {
          objTag = objectTag;
        } else if (objTag != objectTag) {
          objIsArr = isTypedArray(object);
        }
      }
      if (!othIsArr) {
        othTag = objToString.call(other);
        if (othTag == argsTag) {
          othTag = objectTag;
        } else if (othTag != objectTag) {
          othIsArr = isTypedArray(other);
        }
      }
      var objIsObj = objTag == objectTag,
          othIsObj = othTag == objectTag,
          isSameTag = objTag == othTag;

      if (isSameTag && !(objIsArr || objIsObj)) {
        return equalByTag(object, other, objTag);
      }
      if (!isLoose) {
        var objIsWrapped = objIsObj && hasOwnProperty.call(object, '__wrapped__'),
            othIsWrapped = othIsObj && hasOwnProperty.call(other, '__wrapped__');

        if (objIsWrapped || othIsWrapped) {
          return equalFunc(objIsWrapped ? object.value() : object, othIsWrapped ? other.value() : other, customizer, isLoose, stackA, stackB);
        }
      }
      if (!isSameTag) {
        return false;
      }
      // Assume cyclic values are equal.
      // For more information on detecting circular references see https://es5.github.io/#JO.
      stackA || (stackA = []);
      stackB || (stackB = []);

      var length = stackA.length;
      while (length--) {
        if (stackA[length] == object) {
          return stackB[length] == other;
        }
      }
      // Add `object` and `other` to the stack of traversed objects.
      stackA.push(object);
      stackB.push(other);

      var result = (objIsArr ? equalArrays : equalObjects)(object, other, equalFunc, customizer, isLoose, stackA, stackB);

      stackA.pop();
      stackB.pop();

      return result;
    }

    /**
     * The base implementation of `_.isMatch` without support for callback
     * shorthands and `this` binding.
     *
     * @private
     * @param {Object} object The object to inspect.
     * @param {Array} matchData The propery names, values, and compare flags to match.
     * @param {Function} [customizer] The function to customize comparing objects.
     * @returns {boolean} Returns `true` if `object` is a match, else `false`.
     */
    function baseIsMatch(object, matchData, customizer) {
      var index = matchData.length,
          length = index,
          noCustomizer = !customizer;

      if (object == null) {
        return !length;
      }
      object = toObject(object);
      while (index--) {
        var data = matchData[index];
        if ((noCustomizer && data[2])
              ? data[1] !== object[data[0]]
              : !(data[0] in object)
            ) {
          return false;
        }
      }
      while (++index < length) {
        data = matchData[index];
        var key = data[0],
            objValue = object[key],
            srcValue = data[1];

        if (noCustomizer && data[2]) {
          if (objValue === undefined && !(key in object)) {
            return false;
          }
        } else {
          var result = customizer ? customizer(objValue, srcValue, key) : undefined;
          if (!(result === undefined ? baseIsEqual(srcValue, objValue, customizer, true) : result)) {
            return false;
          }
        }
      }
      return true;
    }

    /**
     * The base implementation of `_.map` without support for callback shorthands
     * and `this` binding.
     *
     * @private
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function} iteratee The function invoked per iteration.
     * @returns {Array} Returns the new mapped array.
     */
    function baseMap(collection, iteratee) {
      var index = -1,
          result = isArrayLike(collection) ? Array(collection.length) : [];

      baseEach(collection, function(value, key, collection) {
        result[++index] = iteratee(value, key, collection);
      });
      return result;
    }

    /**
     * The base implementation of `_.matches` which does not clone `source`.
     *
     * @private
     * @param {Object} source The object of property values to match.
     * @returns {Function} Returns the new function.
     */
    function baseMatches(source) {
      var matchData = getMatchData(source);
      if (matchData.length == 1 && matchData[0][2]) {
        var key = matchData[0][0],
            value = matchData[0][1];

        return function(object) {
          if (object == null) {
            return false;
          }
          return object[key] === value && (value !== undefined || (key in toObject(object)));
        };
      }
      return function(object) {
        return baseIsMatch(object, matchData);
      };
    }

    /**
     * The base implementation of `_.matchesProperty` which does not clone `srcValue`.
     *
     * @private
     * @param {string} path The path of the property to get.
     * @param {*} srcValue The value to compare.
     * @returns {Function} Returns the new function.
     */
    function baseMatchesProperty(path, srcValue) {
      var isArr = isArray(path),
          isCommon = isKey(path) && isStrictComparable(srcValue),
          pathKey = (path + '');

      path = toPath(path);
      return function(object) {
        if (object == null) {
          return false;
        }
        var key = pathKey;
        object = toObject(object);
        if ((isArr || !isCommon) && !(key in object)) {
          object = path.length == 1 ? object : baseGet(object, baseSlice(path, 0, -1));
          if (object == null) {
            return false;
          }
          key = last(path);
          object = toObject(object);
        }
        return object[key] === srcValue
          ? (srcValue !== undefined || (key in object))
          : baseIsEqual(srcValue, object[key], undefined, true);
      };
    }

    /**
     * The base implementation of `_.merge` without support for argument juggling,
     * multiple sources, and `this` binding `customizer` functions.
     *
     * @private
     * @param {Object} object The destination object.
     * @param {Object} source The source object.
     * @param {Function} [customizer] The function to customize merged values.
     * @param {Array} [stackA=[]] Tracks traversed source objects.
     * @param {Array} [stackB=[]] Associates values with source counterparts.
     * @returns {Object} Returns `object`.
     */
    function baseMerge(object, source, customizer, stackA, stackB) {
      if (!isObject(object)) {
        return object;
      }
      var isSrcArr = isArrayLike(source) && (isArray(source) || isTypedArray(source)),
          props = isSrcArr ? undefined : keys(source);

      arrayEach(props || source, function(srcValue, key) {
        if (props) {
          key = srcValue;
          srcValue = source[key];
        }
        if (isObjectLike(srcValue)) {
          stackA || (stackA = []);
          stackB || (stackB = []);
          baseMergeDeep(object, source, key, baseMerge, customizer, stackA, stackB);
        }
        else {
          var value = object[key],
              result = customizer ? customizer(value, srcValue, key, object, source) : undefined,
              isCommon = result === undefined;

          if (isCommon) {
            result = srcValue;
          }
          if ((result !== undefined || (isSrcArr && !(key in object))) &&
              (isCommon || (result === result ? (result !== value) : (value === value)))) {
            object[key] = result;
          }
        }
      });
      return object;
    }

    /**
     * A specialized version of `baseMerge` for arrays and objects which performs
     * deep merges and tracks traversed objects enabling objects with circular
     * references to be merged.
     *
     * @private
     * @param {Object} object The destination object.
     * @param {Object} source The source object.
     * @param {string} key The key of the value to merge.
     * @param {Function} mergeFunc The function to merge values.
     * @param {Function} [customizer] The function to customize merged values.
     * @param {Array} [stackA=[]] Tracks traversed source objects.
     * @param {Array} [stackB=[]] Associates values with source counterparts.
     * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
     */
    function baseMergeDeep(object, source, key, mergeFunc, customizer, stackA, stackB) {
      var length = stackA.length,
          srcValue = source[key];

      while (length--) {
        if (stackA[length] == srcValue) {
          object[key] = stackB[length];
          return;
        }
      }
      var value = object[key],
          result = customizer ? customizer(value, srcValue, key, object, source) : undefined,
          isCommon = result === undefined;

      if (isCommon) {
        result = srcValue;
        if (isArrayLike(srcValue) && (isArray(srcValue) || isTypedArray(srcValue))) {
          result = isArray(value)
            ? value
            : (isArrayLike(value) ? arrayCopy(value) : []);
        }
        else if (isPlainObject(srcValue) || isArguments(srcValue)) {
          result = isArguments(value)
            ? toPlainObject(value)
            : (isPlainObject(value) ? value : {});
        }
        else {
          isCommon = false;
        }
      }
      // Add the source value to the stack of traversed objects and associate
      // it with its merged value.
      stackA.push(srcValue);
      stackB.push(result);

      if (isCommon) {
        // Recursively merge objects and arrays (susceptible to call stack limits).
        object[key] = mergeFunc(result, srcValue, customizer, stackA, stackB);
      } else if (result === result ? (result !== value) : (value === value)) {
        object[key] = result;
      }
    }

    /**
     * The base implementation of `_.property` without support for deep paths.
     *
     * @private
     * @param {string} key The key of the property to get.
     * @returns {Function} Returns the new function.
     */
    function baseProperty(key) {
      return function(object) {
        return object == null ? undefined : object[key];
      };
    }

    /**
     * A specialized version of `baseProperty` which supports deep paths.
     *
     * @private
     * @param {Array|string} path The path of the property to get.
     * @returns {Function} Returns the new function.
     */
    function basePropertyDeep(path) {
      var pathKey = (path + '');
      path = toPath(path);
      return function(object) {
        return baseGet(object, path, pathKey);
      };
    }

    /**
     * The base implementation of `_.pullAt` without support for individual
     * index arguments and capturing the removed elements.
     *
     * @private
     * @param {Array} array The array to modify.
     * @param {number[]} indexes The indexes of elements to remove.
     * @returns {Array} Returns `array`.
     */
    function basePullAt(array, indexes) {
      var length = array ? indexes.length : 0;
      while (length--) {
        var index = indexes[length];
        if (index != previous && isIndex(index)) {
          var previous = index;
          splice.call(array, index, 1);
        }
      }
      return array;
    }

    /**
     * The base implementation of `_.random` without support for argument juggling
     * and returning floating-point numbers.
     *
     * @private
     * @param {number} min The minimum possible value.
     * @param {number} max The maximum possible value.
     * @returns {number} Returns the random number.
     */
    function baseRandom(min, max) {
      return min + nativeFloor(nativeRandom() * (max - min + 1));
    }

    /**
     * The base implementation of `_.reduce` and `_.reduceRight` without support
     * for callback shorthands and `this` binding, which iterates over `collection`
     * using the provided `eachFunc`.
     *
     * @private
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function} iteratee The function invoked per iteration.
     * @param {*} accumulator The initial value.
     * @param {boolean} initFromCollection Specify using the first or last element
     *  of `collection` as the initial value.
     * @param {Function} eachFunc The function to iterate over `collection`.
     * @returns {*} Returns the accumulated value.
     */
    function baseReduce(collection, iteratee, accumulator, initFromCollection, eachFunc) {
      eachFunc(collection, function(value, index, collection) {
        accumulator = initFromCollection
          ? (initFromCollection = false, value)
          : iteratee(accumulator, value, index, collection);
      });
      return accumulator;
    }

    /**
     * The base implementation of `setData` without support for hot loop detection.
     *
     * @private
     * @param {Function} func The function to associate metadata with.
     * @param {*} data The metadata.
     * @returns {Function} Returns `func`.
     */
    var baseSetData = !metaMap ? identity : function(func, data) {
      metaMap.set(func, data);
      return func;
    };

    /**
     * The base implementation of `_.slice` without an iteratee call guard.
     *
     * @private
     * @param {Array} array The array to slice.
     * @param {number} [start=0] The start position.
     * @param {number} [end=array.length] The end position.
     * @returns {Array} Returns the slice of `array`.
     */
    function baseSlice(array, start, end) {
      var index = -1,
          length = array.length;

      start = start == null ? 0 : (+start || 0);
      if (start < 0) {
        start = -start > length ? 0 : (length + start);
      }
      end = (end === undefined || end > length) ? length : (+end || 0);
      if (end < 0) {
        end += length;
      }
      length = start > end ? 0 : ((end - start) >>> 0);
      start >>>= 0;

      var result = Array(length);
      while (++index < length) {
        result[index] = array[index + start];
      }
      return result;
    }

    /**
     * The base implementation of `_.some` without support for callback shorthands
     * and `this` binding.
     *
     * @private
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function} predicate The function invoked per iteration.
     * @returns {boolean} Returns `true` if any element passes the predicate check,
     *  else `false`.
     */
    function baseSome(collection, predicate) {
      var result;

      baseEach(collection, function(value, index, collection) {
        result = predicate(value, index, collection);
        return !result;
      });
      return !!result;
    }

    /**
     * The base implementation of `_.sortBy` which uses `comparer` to define
     * the sort order of `array` and replaces criteria objects with their
     * corresponding values.
     *
     * @private
     * @param {Array} array The array to sort.
     * @param {Function} comparer The function to define sort order.
     * @returns {Array} Returns `array`.
     */
    function baseSortBy(array, comparer) {
      var length = array.length;

      array.sort(comparer);
      while (length--) {
        array[length] = array[length].value;
      }
      return array;
    }

    /**
     * The base implementation of `_.sortByOrder` without param guards.
     *
     * @private
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function[]|Object[]|string[]} iteratees The iteratees to sort by.
     * @param {boolean[]} orders The sort orders of `iteratees`.
     * @returns {Array} Returns the new sorted array.
     */
    function baseSortByOrder(collection, iteratees, orders) {
      var callback = getCallback(),
          index = -1;

      iteratees = arrayMap(iteratees, function(iteratee) { return callback(iteratee); });

      var result = baseMap(collection, function(value) {
        var criteria = arrayMap(iteratees, function(iteratee) { return iteratee(value); });
        return { 'criteria': criteria, 'index': ++index, 'value': value };
      });

      return baseSortBy(result, function(object, other) {
        return compareMultiple(object, other, orders);
      });
    }

    /**
     * The base implementation of `_.sum` without support for callback shorthands
     * and `this` binding.
     *
     * @private
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function} iteratee The function invoked per iteration.
     * @returns {number} Returns the sum.
     */
    function baseSum(collection, iteratee) {
      var result = 0;
      baseEach(collection, function(value, index, collection) {
        result += +iteratee(value, index, collection) || 0;
      });
      return result;
    }

    /**
     * The base implementation of `_.uniq` without support for callback shorthands
     * and `this` binding.
     *
     * @private
     * @param {Array} array The array to inspect.
     * @param {Function} [iteratee] The function invoked per iteration.
     * @returns {Array} Returns the new duplicate-value-free array.
     */
    function baseUniq(array, iteratee) {
      var index = -1,
          indexOf = getIndexOf(),
          length = array.length,
          isCommon = indexOf == baseIndexOf,
          isLarge = isCommon && length >= LARGE_ARRAY_SIZE,
          seen = isLarge ? createCache() : null,
          result = [];

      if (seen) {
        indexOf = cacheIndexOf;
        isCommon = false;
      } else {
        isLarge = false;
        seen = iteratee ? [] : result;
      }
      outer:
      while (++index < length) {
        var value = array[index],
            computed = iteratee ? iteratee(value, index, array) : value;

        if (isCommon && value === value) {
          var seenIndex = seen.length;
          while (seenIndex--) {
            if (seen[seenIndex] === computed) {
              continue outer;
            }
          }
          if (iteratee) {
            seen.push(computed);
          }
          result.push(value);
        }
        else if (indexOf(seen, computed, 0) < 0) {
          if (iteratee || isLarge) {
            seen.push(computed);
          }
          result.push(value);
        }
      }
      return result;
    }

    /**
     * The base implementation of `_.values` and `_.valuesIn` which creates an
     * array of `object` property values corresponding to the property names
     * of `props`.
     *
     * @private
     * @param {Object} object The object to query.
     * @param {Array} props The property names to get values for.
     * @returns {Object} Returns the array of property values.
     */
    function baseValues(object, props) {
      var index = -1,
          length = props.length,
          result = Array(length);

      while (++index < length) {
        result[index] = object[props[index]];
      }
      return result;
    }

    /**
     * The base implementation of `_.dropRightWhile`, `_.dropWhile`, `_.takeRightWhile`,
     * and `_.takeWhile` without support for callback shorthands and `this` binding.
     *
     * @private
     * @param {Array} array The array to query.
     * @param {Function} predicate The function invoked per iteration.
     * @param {boolean} [isDrop] Specify dropping elements instead of taking them.
     * @param {boolean} [fromRight] Specify iterating from right to left.
     * @returns {Array} Returns the slice of `array`.
     */
    function baseWhile(array, predicate, isDrop, fromRight) {
      var length = array.length,
          index = fromRight ? length : -1;

      while ((fromRight ? index-- : ++index < length) && predicate(array[index], index, array)) {}
      return isDrop
        ? baseSlice(array, (fromRight ? 0 : index), (fromRight ? index + 1 : length))
        : baseSlice(array, (fromRight ? index + 1 : 0), (fromRight ? length : index));
    }

    /**
     * The base implementation of `wrapperValue` which returns the result of
     * performing a sequence of actions on the unwrapped `value`, where each
     * successive action is supplied the return value of the previous.
     *
     * @private
     * @param {*} value The unwrapped value.
     * @param {Array} actions Actions to peform to resolve the unwrapped value.
     * @returns {*} Returns the resolved value.
     */
    function baseWrapperValue(value, actions) {
      var result = value;
      if (result instanceof LazyWrapper) {
        result = result.value();
      }
      var index = -1,
          length = actions.length;

      while (++index < length) {
        var action = actions[index];
        result = action.func.apply(action.thisArg, arrayPush([result], action.args));
      }
      return result;
    }

    /**
     * Performs a binary search of `array` to determine the index at which `value`
     * should be inserted into `array` in order to maintain its sort order.
     *
     * @private
     * @param {Array} array The sorted array to inspect.
     * @param {*} value The value to evaluate.
     * @param {boolean} [retHighest] Specify returning the highest qualified index.
     * @returns {number} Returns the index at which `value` should be inserted
     *  into `array`.
     */
    function binaryIndex(array, value, retHighest) {
      var low = 0,
          high = array ? array.length : low;

      if (typeof value == 'number' && value === value && high <= HALF_MAX_ARRAY_LENGTH) {
        while (low < high) {
          var mid = (low + high) >>> 1,
              computed = array[mid];

          if ((retHighest ? (computed <= value) : (computed < value)) && computed !== null) {
            low = mid + 1;
          } else {
            high = mid;
          }
        }
        return high;
      }
      return binaryIndexBy(array, value, identity, retHighest);
    }

    /**
     * This function is like `binaryIndex` except that it invokes `iteratee` for
     * `value` and each element of `array` to compute their sort ranking. The
     * iteratee is invoked with one argument; (value).
     *
     * @private
     * @param {Array} array The sorted array to inspect.
     * @param {*} value The value to evaluate.
     * @param {Function} iteratee The function invoked per iteration.
     * @param {boolean} [retHighest] Specify returning the highest qualified index.
     * @returns {number} Returns the index at which `value` should be inserted
     *  into `array`.
     */
    function binaryIndexBy(array, value, iteratee, retHighest) {
      value = iteratee(value);

      var low = 0,
          high = array ? array.length : 0,
          valIsNaN = value !== value,
          valIsNull = value === null,
          valIsUndef = value === undefined;

      while (low < high) {
        var mid = nativeFloor((low + high) / 2),
            computed = iteratee(array[mid]),
            isDef = computed !== undefined,
            isReflexive = computed === computed;

        if (valIsNaN) {
          var setLow = isReflexive || retHighest;
        } else if (valIsNull) {
          setLow = isReflexive && isDef && (retHighest || computed != null);
        } else if (valIsUndef) {
          setLow = isReflexive && (retHighest || isDef);
        } else if (computed == null) {
          setLow = false;
        } else {
          setLow = retHighest ? (computed <= value) : (computed < value);
        }
        if (setLow) {
          low = mid + 1;
        } else {
          high = mid;
        }
      }
      return nativeMin(high, MAX_ARRAY_INDEX);
    }

    /**
     * A specialized version of `baseCallback` which only supports `this` binding
     * and specifying the number of arguments to provide to `func`.
     *
     * @private
     * @param {Function} func The function to bind.
     * @param {*} thisArg The `this` binding of `func`.
     * @param {number} [argCount] The number of arguments to provide to `func`.
     * @returns {Function} Returns the callback.
     */
    function bindCallback(func, thisArg, argCount) {
      if (typeof func != 'function') {
        return identity;
      }
      if (thisArg === undefined) {
        return func;
      }
      switch (argCount) {
        case 1: return function(value) {
          return func.call(thisArg, value);
        };
        case 3: return function(value, index, collection) {
          return func.call(thisArg, value, index, collection);
        };
        case 4: return function(accumulator, value, index, collection) {
          return func.call(thisArg, accumulator, value, index, collection);
        };
        case 5: return function(value, other, key, object, source) {
          return func.call(thisArg, value, other, key, object, source);
        };
      }
      return function() {
        return func.apply(thisArg, arguments);
      };
    }

    /**
     * Creates a clone of the given array buffer.
     *
     * @private
     * @param {ArrayBuffer} buffer The array buffer to clone.
     * @returns {ArrayBuffer} Returns the cloned array buffer.
     */
    function bufferClone(buffer) {
      var result = new ArrayBuffer(buffer.byteLength),
          view = new Uint8Array(result);

      view.set(new Uint8Array(buffer));
      return result;
    }

    /**
     * Creates an array that is the composition of partially applied arguments,
     * placeholders, and provided arguments into a single array of arguments.
     *
     * @private
     * @param {Array|Object} args The provided arguments.
     * @param {Array} partials The arguments to prepend to those provided.
     * @param {Array} holders The `partials` placeholder indexes.
     * @returns {Array} Returns the new array of composed arguments.
     */
    function composeArgs(args, partials, holders) {
      var holdersLength = holders.length,
          argsIndex = -1,
          argsLength = nativeMax(args.length - holdersLength, 0),
          leftIndex = -1,
          leftLength = partials.length,
          result = Array(leftLength + argsLength);

      while (++leftIndex < leftLength) {
        result[leftIndex] = partials[leftIndex];
      }
      while (++argsIndex < holdersLength) {
        result[holders[argsIndex]] = args[argsIndex];
      }
      while (argsLength--) {
        result[leftIndex++] = args[argsIndex++];
      }
      return result;
    }

    /**
     * This function is like `composeArgs` except that the arguments composition
     * is tailored for `_.partialRight`.
     *
     * @private
     * @param {Array|Object} args The provided arguments.
     * @param {Array} partials The arguments to append to those provided.
     * @param {Array} holders The `partials` placeholder indexes.
     * @returns {Array} Returns the new array of composed arguments.
     */
    function composeArgsRight(args, partials, holders) {
      var holdersIndex = -1,
          holdersLength = holders.length,
          argsIndex = -1,
          argsLength = nativeMax(args.length - holdersLength, 0),
          rightIndex = -1,
          rightLength = partials.length,
          result = Array(argsLength + rightLength);

      while (++argsIndex < argsLength) {
        result[argsIndex] = args[argsIndex];
      }
      var offset = argsIndex;
      while (++rightIndex < rightLength) {
        result[offset + rightIndex] = partials[rightIndex];
      }
      while (++holdersIndex < holdersLength) {
        result[offset + holders[holdersIndex]] = args[argsIndex++];
      }
      return result;
    }

    /**
     * Creates a `_.countBy`, `_.groupBy`, `_.indexBy`, or `_.partition` function.
     *
     * @private
     * @param {Function} setter The function to set keys and values of the accumulator object.
     * @param {Function} [initializer] The function to initialize the accumulator object.
     * @returns {Function} Returns the new aggregator function.
     */
    function createAggregator(setter, initializer) {
      return function(collection, iteratee, thisArg) {
        var result = initializer ? initializer() : {};
        iteratee = getCallback(iteratee, thisArg, 3);

        if (isArray(collection)) {
          var index = -1,
              length = collection.length;

          while (++index < length) {
            var value = collection[index];
            setter(result, value, iteratee(value, index, collection), collection);
          }
        } else {
          baseEach(collection, function(value, key, collection) {
            setter(result, value, iteratee(value, key, collection), collection);
          });
        }
        return result;
      };
    }

    /**
     * Creates a `_.assign`, `_.defaults`, or `_.merge` function.
     *
     * @private
     * @param {Function} assigner The function to assign values.
     * @returns {Function} Returns the new assigner function.
     */
    function createAssigner(assigner) {
      return restParam(function(object, sources) {
        var index = -1,
            length = object == null ? 0 : sources.length,
            customizer = length > 2 ? sources[length - 2] : undefined,
            guard = length > 2 ? sources[2] : undefined,
            thisArg = length > 1 ? sources[length - 1] : undefined;

        if (typeof customizer == 'function') {
          customizer = bindCallback(customizer, thisArg, 5);
          length -= 2;
        } else {
          customizer = typeof thisArg == 'function' ? thisArg : undefined;
          length -= (customizer ? 1 : 0);
        }
        if (guard && isIterateeCall(sources[0], sources[1], guard)) {
          customizer = length < 3 ? undefined : customizer;
          length = 1;
        }
        while (++index < length) {
          var source = sources[index];
          if (source) {
            assigner(object, source, customizer);
          }
        }
        return object;
      });
    }

    /**
     * Creates a `baseEach` or `baseEachRight` function.
     *
     * @private
     * @param {Function} eachFunc The function to iterate over a collection.
     * @param {boolean} [fromRight] Specify iterating from right to left.
     * @returns {Function} Returns the new base function.
     */
    function createBaseEach(eachFunc, fromRight) {
      return function(collection, iteratee) {
        var length = collection ? getLength(collection) : 0;
        if (!isLength(length)) {
          return eachFunc(collection, iteratee);
        }
        var index = fromRight ? length : -1,
            iterable = toObject(collection);

        while ((fromRight ? index-- : ++index < length)) {
          if (iteratee(iterable[index], index, iterable) === false) {
            break;
          }
        }
        return collection;
      };
    }

    /**
     * Creates a base function for `_.forIn` or `_.forInRight`.
     *
     * @private
     * @param {boolean} [fromRight] Specify iterating from right to left.
     * @returns {Function} Returns the new base function.
     */
    function createBaseFor(fromRight) {
      return function(object, iteratee, keysFunc) {
        var iterable = toObject(object),
            props = keysFunc(object),
            length = props.length,
            index = fromRight ? length : -1;

        while ((fromRight ? index-- : ++index < length)) {
          var key = props[index];
          if (iteratee(iterable[key], key, iterable) === false) {
            break;
          }
        }
        return object;
      };
    }

    /**
     * Creates a function that wraps `func` and invokes it with the `this`
     * binding of `thisArg`.
     *
     * @private
     * @param {Function} func The function to bind.
     * @param {*} [thisArg] The `this` binding of `func`.
     * @returns {Function} Returns the new bound function.
     */
    function createBindWrapper(func, thisArg) {
      var Ctor = createCtorWrapper(func);

      function wrapper() {
        var fn = (this && this !== root && this instanceof wrapper) ? Ctor : func;
        return fn.apply(thisArg, arguments);
      }
      return wrapper;
    }

    /**
     * Creates a `Set` cache object to optimize linear searches of large arrays.
     *
     * @private
     * @param {Array} [values] The values to cache.
     * @returns {null|Object} Returns the new cache object if `Set` is supported, else `null`.
     */
    function createCache(values) {
      return (nativeCreate && Set) ? new SetCache(values) : null;
    }

    /**
     * Creates a function that produces compound words out of the words in a
     * given string.
     *
     * @private
     * @param {Function} callback The function to combine each word.
     * @returns {Function} Returns the new compounder function.
     */
    function createCompounder(callback) {
      return function(string) {
        var index = -1,
            array = words(deburr(string)),
            length = array.length,
            result = '';

        while (++index < length) {
          result = callback(result, array[index], index);
        }
        return result;
      };
    }

    /**
     * Creates a function that produces an instance of `Ctor` regardless of
     * whether it was invoked as part of a `new` expression or by `call` or `apply`.
     *
     * @private
     * @param {Function} Ctor The constructor to wrap.
     * @returns {Function} Returns the new wrapped function.
     */
    function createCtorWrapper(Ctor) {
      return function() {
        // Use a `switch` statement to work with class constructors.
        // See http://ecma-international.org/ecma-262/6.0/#sec-ecmascript-function-objects-call-thisargument-argumentslist
        // for more details.
        var args = arguments;
        switch (args.length) {
          case 0: return new Ctor;
          case 1: return new Ctor(args[0]);
          case 2: return new Ctor(args[0], args[1]);
          case 3: return new Ctor(args[0], args[1], args[2]);
          case 4: return new Ctor(args[0], args[1], args[2], args[3]);
          case 5: return new Ctor(args[0], args[1], args[2], args[3], args[4]);
          case 6: return new Ctor(args[0], args[1], args[2], args[3], args[4], args[5]);
          case 7: return new Ctor(args[0], args[1], args[2], args[3], args[4], args[5], args[6]);
        }
        var thisBinding = baseCreate(Ctor.prototype),
            result = Ctor.apply(thisBinding, args);

        // Mimic the constructor's `return` behavior.
        // See https://es5.github.io/#x13.2.2 for more details.
        return isObject(result) ? result : thisBinding;
      };
    }

    /**
     * Creates a `_.curry` or `_.curryRight` function.
     *
     * @private
     * @param {boolean} flag The curry bit flag.
     * @returns {Function} Returns the new curry function.
     */
    function createCurry(flag) {
      function curryFunc(func, arity, guard) {
        if (guard && isIterateeCall(func, arity, guard)) {
          arity = undefined;
        }
        var result = createWrapper(func, flag, undefined, undefined, undefined, undefined, undefined, arity);
        result.placeholder = curryFunc.placeholder;
        return result;
      }
      return curryFunc;
    }

    /**
     * Creates a `_.defaults` or `_.defaultsDeep` function.
     *
     * @private
     * @param {Function} assigner The function to assign values.
     * @param {Function} customizer The function to customize assigned values.
     * @returns {Function} Returns the new defaults function.
     */
    function createDefaults(assigner, customizer) {
      return restParam(function(args) {
        var object = args[0];
        if (object == null) {
          return object;
        }
        args.push(customizer);
        return assigner.apply(undefined, args);
      });
    }

    /**
     * Creates a `_.max` or `_.min` function.
     *
     * @private
     * @param {Function} comparator The function used to compare values.
     * @param {*} exValue The initial extremum value.
     * @returns {Function} Returns the new extremum function.
     */
    function createExtremum(comparator, exValue) {
      return function(collection, iteratee, thisArg) {
        if (thisArg && isIterateeCall(collection, iteratee, thisArg)) {
          iteratee = undefined;
        }
        iteratee = getCallback(iteratee, thisArg, 3);
        if (iteratee.length == 1) {
          collection = isArray(collection) ? collection : toIterable(collection);
          var result = arrayExtremum(collection, iteratee, comparator, exValue);
          if (!(collection.length && result === exValue)) {
            return result;
          }
        }
        return baseExtremum(collection, iteratee, comparator, exValue);
      };
    }

    /**
     * Creates a `_.find` or `_.findLast` function.
     *
     * @private
     * @param {Function} eachFunc The function to iterate over a collection.
     * @param {boolean} [fromRight] Specify iterating from right to left.
     * @returns {Function} Returns the new find function.
     */
    function createFind(eachFunc, fromRight) {
      return function(collection, predicate, thisArg) {
        predicate = getCallback(predicate, thisArg, 3);
        if (isArray(collection)) {
          var index = baseFindIndex(collection, predicate, fromRight);
          return index > -1 ? collection[index] : undefined;
        }
        return baseFind(collection, predicate, eachFunc);
      };
    }

    /**
     * Creates a `_.findIndex` or `_.findLastIndex` function.
     *
     * @private
     * @param {boolean} [fromRight] Specify iterating from right to left.
     * @returns {Function} Returns the new find function.
     */
    function createFindIndex(fromRight) {
      return function(array, predicate, thisArg) {
        if (!(array && array.length)) {
          return -1;
        }
        predicate = getCallback(predicate, thisArg, 3);
        return baseFindIndex(array, predicate, fromRight);
      };
    }

    /**
     * Creates a `_.findKey` or `_.findLastKey` function.
     *
     * @private
     * @param {Function} objectFunc The function to iterate over an object.
     * @returns {Function} Returns the new find function.
     */
    function createFindKey(objectFunc) {
      return function(object, predicate, thisArg) {
        predicate = getCallback(predicate, thisArg, 3);
        return baseFind(object, predicate, objectFunc, true);
      };
    }

    /**
     * Creates a `_.flow` or `_.flowRight` function.
     *
     * @private
     * @param {boolean} [fromRight] Specify iterating from right to left.
     * @returns {Function} Returns the new flow function.
     */
    function createFlow(fromRight) {
      return function() {
        var wrapper,
            length = arguments.length,
            index = fromRight ? length : -1,
            leftIndex = 0,
            funcs = Array(length);

        while ((fromRight ? index-- : ++index < length)) {
          var func = funcs[leftIndex++] = arguments[index];
          if (typeof func != 'function') {
            throw new TypeError(FUNC_ERROR_TEXT);
          }
          if (!wrapper && LodashWrapper.prototype.thru && getFuncName(func) == 'wrapper') {
            wrapper = new LodashWrapper([], true);
          }
        }
        index = wrapper ? -1 : length;
        while (++index < length) {
          func = funcs[index];

          var funcName = getFuncName(func),
              data = funcName == 'wrapper' ? getData(func) : undefined;

          if (data && isLaziable(data[0]) && data[1] == (ARY_FLAG | CURRY_FLAG | PARTIAL_FLAG | REARG_FLAG) && !data[4].length && data[9] == 1) {
            wrapper = wrapper[getFuncName(data[0])].apply(wrapper, data[3]);
          } else {
            wrapper = (func.length == 1 && isLaziable(func)) ? wrapper[funcName]() : wrapper.thru(func);
          }
        }
        return function() {
          var args = arguments,
              value = args[0];

          if (wrapper && args.length == 1 && isArray(value) && value.length >= LARGE_ARRAY_SIZE) {
            return wrapper.plant(value).value();
          }
          var index = 0,
              result = length ? funcs[index].apply(this, args) : value;

          while (++index < length) {
            result = funcs[index].call(this, result);
          }
          return result;
        };
      };
    }

    /**
     * Creates a function for `_.forEach` or `_.forEachRight`.
     *
     * @private
     * @param {Function} arrayFunc The function to iterate over an array.
     * @param {Function} eachFunc The function to iterate over a collection.
     * @returns {Function} Returns the new each function.
     */
    function createForEach(arrayFunc, eachFunc) {
      return function(collection, iteratee, thisArg) {
        return (typeof iteratee == 'function' && thisArg === undefined && isArray(collection))
          ? arrayFunc(collection, iteratee)
          : eachFunc(collection, bindCallback(iteratee, thisArg, 3));
      };
    }

    /**
     * Creates a function for `_.forIn` or `_.forInRight`.
     *
     * @private
     * @param {Function} objectFunc The function to iterate over an object.
     * @returns {Function} Returns the new each function.
     */
    function createForIn(objectFunc) {
      return function(object, iteratee, thisArg) {
        if (typeof iteratee != 'function' || thisArg !== undefined) {
          iteratee = bindCallback(iteratee, thisArg, 3);
        }
        return objectFunc(object, iteratee, keysIn);
      };
    }

    /**
     * Creates a function for `_.forOwn` or `_.forOwnRight`.
     *
     * @private
     * @param {Function} objectFunc The function to iterate over an object.
     * @returns {Function} Returns the new each function.
     */
    function createForOwn(objectFunc) {
      return function(object, iteratee, thisArg) {
        if (typeof iteratee != 'function' || thisArg !== undefined) {
          iteratee = bindCallback(iteratee, thisArg, 3);
        }
        return objectFunc(object, iteratee);
      };
    }

    /**
     * Creates a function for `_.mapKeys` or `_.mapValues`.
     *
     * @private
     * @param {boolean} [isMapKeys] Specify mapping keys instead of values.
     * @returns {Function} Returns the new map function.
     */
    function createObjectMapper(isMapKeys) {
      return function(object, iteratee, thisArg) {
        var result = {};
        iteratee = getCallback(iteratee, thisArg, 3);

        baseForOwn(object, function(value, key, object) {
          var mapped = iteratee(value, key, object);
          key = isMapKeys ? mapped : key;
          value = isMapKeys ? value : mapped;
          result[key] = value;
        });
        return result;
      };
    }

    /**
     * Creates a function for `_.padLeft` or `_.padRight`.
     *
     * @private
     * @param {boolean} [fromRight] Specify padding from the right.
     * @returns {Function} Returns the new pad function.
     */
    function createPadDir(fromRight) {
      return function(string, length, chars) {
        string = baseToString(string);
        return (fromRight ? string : '') + createPadding(string, length, chars) + (fromRight ? '' : string);
      };
    }

    /**
     * Creates a `_.partial` or `_.partialRight` function.
     *
     * @private
     * @param {boolean} flag The partial bit flag.
     * @returns {Function} Returns the new partial function.
     */
    function createPartial(flag) {
      var partialFunc = restParam(function(func, partials) {
        var holders = replaceHolders(partials, partialFunc.placeholder);
        return createWrapper(func, flag, undefined, partials, holders);
      });
      return partialFunc;
    }

    /**
     * Creates a function for `_.reduce` or `_.reduceRight`.
     *
     * @private
     * @param {Function} arrayFunc The function to iterate over an array.
     * @param {Function} eachFunc The function to iterate over a collection.
     * @returns {Function} Returns the new each function.
     */
    function createReduce(arrayFunc, eachFunc) {
      return function(collection, iteratee, accumulator, thisArg) {
        var initFromArray = arguments.length < 3;
        return (typeof iteratee == 'function' && thisArg === undefined && isArray(collection))
          ? arrayFunc(collection, iteratee, accumulator, initFromArray)
          : baseReduce(collection, getCallback(iteratee, thisArg, 4), accumulator, initFromArray, eachFunc);
      };
    }

    /**
     * Creates a function that wraps `func` and invokes it with optional `this`
     * binding of, partial application, and currying.
     *
     * @private
     * @param {Function|string} func The function or method name to reference.
     * @param {number} bitmask The bitmask of flags. See `createWrapper` for more details.
     * @param {*} [thisArg] The `this` binding of `func`.
     * @param {Array} [partials] The arguments to prepend to those provided to the new function.
     * @param {Array} [holders] The `partials` placeholder indexes.
     * @param {Array} [partialsRight] The arguments to append to those provided to the new function.
     * @param {Array} [holdersRight] The `partialsRight` placeholder indexes.
     * @param {Array} [argPos] The argument positions of the new function.
     * @param {number} [ary] The arity cap of `func`.
     * @param {number} [arity] The arity of `func`.
     * @returns {Function} Returns the new wrapped function.
     */
    function createHybridWrapper(func, bitmask, thisArg, partials, holders, partialsRight, holdersRight, argPos, ary, arity) {
      var isAry = bitmask & ARY_FLAG,
          isBind = bitmask & BIND_FLAG,
          isBindKey = bitmask & BIND_KEY_FLAG,
          isCurry = bitmask & CURRY_FLAG,
          isCurryBound = bitmask & CURRY_BOUND_FLAG,
          isCurryRight = bitmask & CURRY_RIGHT_FLAG,
          Ctor = isBindKey ? undefined : createCtorWrapper(func);

      function wrapper() {
        // Avoid `arguments` object use disqualifying optimizations by
        // converting it to an array before providing it to other functions.
        var length = arguments.length,
            index = length,
            args = Array(length);

        while (index--) {
          args[index] = arguments[index];
        }
        if (partials) {
          args = composeArgs(args, partials, holders);
        }
        if (partialsRight) {
          args = composeArgsRight(args, partialsRight, holdersRight);
        }
        if (isCurry || isCurryRight) {
          var placeholder = wrapper.placeholder,
              argsHolders = replaceHolders(args, placeholder);

          length -= argsHolders.length;
          if (length < arity) {
            var newArgPos = argPos ? arrayCopy(argPos) : undefined,
                newArity = nativeMax(arity - length, 0),
                newsHolders = isCurry ? argsHolders : undefined,
                newHoldersRight = isCurry ? undefined : argsHolders,
                newPartials = isCurry ? args : undefined,
                newPartialsRight = isCurry ? undefined : args;

            bitmask |= (isCurry ? PARTIAL_FLAG : PARTIAL_RIGHT_FLAG);
            bitmask &= ~(isCurry ? PARTIAL_RIGHT_FLAG : PARTIAL_FLAG);

            if (!isCurryBound) {
              bitmask &= ~(BIND_FLAG | BIND_KEY_FLAG);
            }
            var newData = [func, bitmask, thisArg, newPartials, newsHolders, newPartialsRight, newHoldersRight, newArgPos, ary, newArity],
                result = createHybridWrapper.apply(undefined, newData);

            if (isLaziable(func)) {
              setData(result, newData);
            }
            result.placeholder = placeholder;
            return result;
          }
        }
        var thisBinding = isBind ? thisArg : this,
            fn = isBindKey ? thisBinding[func] : func;

        if (argPos) {
          args = reorder(args, argPos);
        }
        if (isAry && ary < args.length) {
          args.length = ary;
        }
        if (this && this !== root && this instanceof wrapper) {
          fn = Ctor || createCtorWrapper(func);
        }
        return fn.apply(thisBinding, args);
      }
      return wrapper;
    }

    /**
     * Creates the padding required for `string` based on the given `length`.
     * The `chars` string is truncated if the number of characters exceeds `length`.
     *
     * @private
     * @param {string} string The string to create padding for.
     * @param {number} [length=0] The padding length.
     * @param {string} [chars=' '] The string used as padding.
     * @returns {string} Returns the pad for `string`.
     */
    function createPadding(string, length, chars) {
      var strLength = string.length;
      length = +length;

      if (strLength >= length || !nativeIsFinite(length)) {
        return '';
      }
      var padLength = length - strLength;
      chars = chars == null ? ' ' : (chars + '');
      return repeat(chars, nativeCeil(padLength / chars.length)).slice(0, padLength);
    }

    /**
     * Creates a function that wraps `func` and invokes it with the optional `this`
     * binding of `thisArg` and the `partials` prepended to those provided to
     * the wrapper.
     *
     * @private
     * @param {Function} func The function to partially apply arguments to.
     * @param {number} bitmask The bitmask of flags. See `createWrapper` for more details.
     * @param {*} thisArg The `this` binding of `func`.
     * @param {Array} partials The arguments to prepend to those provided to the new function.
     * @returns {Function} Returns the new bound function.
     */
    function createPartialWrapper(func, bitmask, thisArg, partials) {
      var isBind = bitmask & BIND_FLAG,
          Ctor = createCtorWrapper(func);

      function wrapper() {
        // Avoid `arguments` object use disqualifying optimizations by
        // converting it to an array before providing it `func`.
        var argsIndex = -1,
            argsLength = arguments.length,
            leftIndex = -1,
            leftLength = partials.length,
            args = Array(leftLength + argsLength);

        while (++leftIndex < leftLength) {
          args[leftIndex] = partials[leftIndex];
        }
        while (argsLength--) {
          args[leftIndex++] = arguments[++argsIndex];
        }
        var fn = (this && this !== root && this instanceof wrapper) ? Ctor : func;
        return fn.apply(isBind ? thisArg : this, args);
      }
      return wrapper;
    }

    /**
     * Creates a `_.ceil`, `_.floor`, or `_.round` function.
     *
     * @private
     * @param {string} methodName The name of the `Math` method to use when rounding.
     * @returns {Function} Returns the new round function.
     */
    function createRound(methodName) {
      var func = Math[methodName];
      return function(number, precision) {
        precision = precision === undefined ? 0 : (+precision || 0);
        if (precision) {
          precision = pow(10, precision);
          return func(number * precision) / precision;
        }
        return func(number);
      };
    }

    /**
     * Creates a `_.sortedIndex` or `_.sortedLastIndex` function.
     *
     * @private
     * @param {boolean} [retHighest] Specify returning the highest qualified index.
     * @returns {Function} Returns the new index function.
     */
    function createSortedIndex(retHighest) {
      return function(array, value, iteratee, thisArg) {
        var callback = getCallback(iteratee);
        return (iteratee == null && callback === baseCallback)
          ? binaryIndex(array, value, retHighest)
          : binaryIndexBy(array, value, callback(iteratee, thisArg, 1), retHighest);
      };
    }

    /**
     * Creates a function that either curries or invokes `func` with optional
     * `this` binding and partially applied arguments.
     *
     * @private
     * @param {Function|string} func The function or method name to reference.
     * @param {number} bitmask The bitmask of flags.
     *  The bitmask may be composed of the following flags:
     *     1 - `_.bind`
     *     2 - `_.bindKey`
     *     4 - `_.curry` or `_.curryRight` of a bound function
     *     8 - `_.curry`
     *    16 - `_.curryRight`
     *    32 - `_.partial`
     *    64 - `_.partialRight`
     *   128 - `_.rearg`
     *   256 - `_.ary`
     * @param {*} [thisArg] The `this` binding of `func`.
     * @param {Array} [partials] The arguments to be partially applied.
     * @param {Array} [holders] The `partials` placeholder indexes.
     * @param {Array} [argPos] The argument positions of the new function.
     * @param {number} [ary] The arity cap of `func`.
     * @param {number} [arity] The arity of `func`.
     * @returns {Function} Returns the new wrapped function.
     */
    function createWrapper(func, bitmask, thisArg, partials, holders, argPos, ary, arity) {
      var isBindKey = bitmask & BIND_KEY_FLAG;
      if (!isBindKey && typeof func != 'function') {
        throw new TypeError(FUNC_ERROR_TEXT);
      }
      var length = partials ? partials.length : 0;
      if (!length) {
        bitmask &= ~(PARTIAL_FLAG | PARTIAL_RIGHT_FLAG);
        partials = holders = undefined;
      }
      length -= (holders ? holders.length : 0);
      if (bitmask & PARTIAL_RIGHT_FLAG) {
        var partialsRight = partials,
            holdersRight = holders;

        partials = holders = undefined;
      }
      var data = isBindKey ? undefined : getData(func),
          newData = [func, bitmask, thisArg, partials, holders, partialsRight, holdersRight, argPos, ary, arity];

      if (data) {
        mergeData(newData, data);
        bitmask = newData[1];
        arity = newData[9];
      }
      newData[9] = arity == null
        ? (isBindKey ? 0 : func.length)
        : (nativeMax(arity - length, 0) || 0);

      if (bitmask == BIND_FLAG) {
        var result = createBindWrapper(newData[0], newData[2]);
      } else if ((bitmask == PARTIAL_FLAG || bitmask == (BIND_FLAG | PARTIAL_FLAG)) && !newData[4].length) {
        result = createPartialWrapper.apply(undefined, newData);
      } else {
        result = createHybridWrapper.apply(undefined, newData);
      }
      var setter = data ? baseSetData : setData;
      return setter(result, newData);
    }

    /**
     * A specialized version of `baseIsEqualDeep` for arrays with support for
     * partial deep comparisons.
     *
     * @private
     * @param {Array} array The array to compare.
     * @param {Array} other The other array to compare.
     * @param {Function} equalFunc The function to determine equivalents of values.
     * @param {Function} [customizer] The function to customize comparing arrays.
     * @param {boolean} [isLoose] Specify performing partial comparisons.
     * @param {Array} [stackA] Tracks traversed `value` objects.
     * @param {Array} [stackB] Tracks traversed `other` objects.
     * @returns {boolean} Returns `true` if the arrays are equivalent, else `false`.
     */
    function equalArrays(array, other, equalFunc, customizer, isLoose, stackA, stackB) {
      var index = -1,
          arrLength = array.length,
          othLength = other.length;

      if (arrLength != othLength && !(isLoose && othLength > arrLength)) {
        return false;
      }
      // Ignore non-index properties.
      while (++index < arrLength) {
        var arrValue = array[index],
            othValue = other[index],
            result = customizer ? customizer(isLoose ? othValue : arrValue, isLoose ? arrValue : othValue, index) : undefined;

        if (result !== undefined) {
          if (result) {
            continue;
          }
          return false;
        }
        // Recursively compare arrays (susceptible to call stack limits).
        if (isLoose) {
          if (!arraySome(other, function(othValue) {
                return arrValue === othValue || equalFunc(arrValue, othValue, customizer, isLoose, stackA, stackB);
              })) {
            return false;
          }
        } else if (!(arrValue === othValue || equalFunc(arrValue, othValue, customizer, isLoose, stackA, stackB))) {
          return false;
        }
      }
      return true;
    }

    /**
     * A specialized version of `baseIsEqualDeep` for comparing objects of
     * the same `toStringTag`.
     *
     * **Note:** This function only supports comparing values with tags of
     * `Boolean`, `Date`, `Error`, `Number`, `RegExp`, or `String`.
     *
     * @private
     * @param {Object} object The object to compare.
     * @param {Object} other The other object to compare.
     * @param {string} tag The `toStringTag` of the objects to compare.
     * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
     */
    function equalByTag(object, other, tag) {
      switch (tag) {
        case boolTag:
        case dateTag:
          // Coerce dates and booleans to numbers, dates to milliseconds and booleans
          // to `1` or `0` treating invalid dates coerced to `NaN` as not equal.
          return +object == +other;

        case errorTag:
          return object.name == other.name && object.message == other.message;

        case numberTag:
          // Treat `NaN` vs. `NaN` as equal.
          return (object != +object)
            ? other != +other
            : object == +other;

        case regexpTag:
        case stringTag:
          // Coerce regexes to strings and treat strings primitives and string
          // objects as equal. See https://es5.github.io/#x15.10.6.4 for more details.
          return object == (other + '');
      }
      return false;
    }

    /**
     * A specialized version of `baseIsEqualDeep` for objects with support for
     * partial deep comparisons.
     *
     * @private
     * @param {Object} object The object to compare.
     * @param {Object} other The other object to compare.
     * @param {Function} equalFunc The function to determine equivalents of values.
     * @param {Function} [customizer] The function to customize comparing values.
     * @param {boolean} [isLoose] Specify performing partial comparisons.
     * @param {Array} [stackA] Tracks traversed `value` objects.
     * @param {Array} [stackB] Tracks traversed `other` objects.
     * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
     */
    function equalObjects(object, other, equalFunc, customizer, isLoose, stackA, stackB) {
      var objProps = keys(object),
          objLength = objProps.length,
          othProps = keys(other),
          othLength = othProps.length;

      if (objLength != othLength && !isLoose) {
        return false;
      }
      var index = objLength;
      while (index--) {
        var key = objProps[index];
        if (!(isLoose ? key in other : hasOwnProperty.call(other, key))) {
          return false;
        }
      }
      var skipCtor = isLoose;
      while (++index < objLength) {
        key = objProps[index];
        var objValue = object[key],
            othValue = other[key],
            result = customizer ? customizer(isLoose ? othValue : objValue, isLoose? objValue : othValue, key) : undefined;

        // Recursively compare objects (susceptible to call stack limits).
        if (!(result === undefined ? equalFunc(objValue, othValue, customizer, isLoose, stackA, stackB) : result)) {
          return false;
        }
        skipCtor || (skipCtor = key == 'constructor');
      }
      if (!skipCtor) {
        var objCtor = object.constructor,
            othCtor = other.constructor;

        // Non `Object` object instances with different constructors are not equal.
        if (objCtor != othCtor &&
            ('constructor' in object && 'constructor' in other) &&
            !(typeof objCtor == 'function' && objCtor instanceof objCtor &&
              typeof othCtor == 'function' && othCtor instanceof othCtor)) {
          return false;
        }
      }
      return true;
    }

    /**
     * Gets the appropriate "callback" function. If the `_.callback` method is
     * customized this function returns the custom method, otherwise it returns
     * the `baseCallback` function. If arguments are provided the chosen function
     * is invoked with them and its result is returned.
     *
     * @private
     * @returns {Function} Returns the chosen function or its result.
     */
    function getCallback(func, thisArg, argCount) {
      var result = lodash.callback || callback;
      result = result === callback ? baseCallback : result;
      return argCount ? result(func, thisArg, argCount) : result;
    }

    /**
     * Gets metadata for `func`.
     *
     * @private
     * @param {Function} func The function to query.
     * @returns {*} Returns the metadata for `func`.
     */
    var getData = !metaMap ? noop : function(func) {
      return metaMap.get(func);
    };

    /**
     * Gets the name of `func`.
     *
     * @private
     * @param {Function} func The function to query.
     * @returns {string} Returns the function name.
     */
    function getFuncName(func) {
      var result = func.name,
          array = realNames[result],
          length = array ? array.length : 0;

      while (length--) {
        var data = array[length],
            otherFunc = data.func;
        if (otherFunc == null || otherFunc == func) {
          return data.name;
        }
      }
      return result;
    }

    /**
     * Gets the appropriate "indexOf" function. If the `_.indexOf` method is
     * customized this function returns the custom method, otherwise it returns
     * the `baseIndexOf` function. If arguments are provided the chosen function
     * is invoked with them and its result is returned.
     *
     * @private
     * @returns {Function|number} Returns the chosen function or its result.
     */
    function getIndexOf(collection, target, fromIndex) {
      var result = lodash.indexOf || indexOf;
      result = result === indexOf ? baseIndexOf : result;
      return collection ? result(collection, target, fromIndex) : result;
    }

    /**
     * Gets the "length" property value of `object`.
     *
     * **Note:** This function is used to avoid a [JIT bug](https://bugs.webkit.org/show_bug.cgi?id=142792)
     * that affects Safari on at least iOS 8.1-8.3 ARM64.
     *
     * @private
     * @param {Object} object The object to query.
     * @returns {*} Returns the "length" value.
     */
    var getLength = baseProperty('length');

    /**
     * Gets the propery names, values, and compare flags of `object`.
     *
     * @private
     * @param {Object} object The object to query.
     * @returns {Array} Returns the match data of `object`.
     */
    function getMatchData(object) {
      var result = pairs(object),
          length = result.length;

      while (length--) {
        result[length][2] = isStrictComparable(result[length][1]);
      }
      return result;
    }

    /**
     * Gets the native function at `key` of `object`.
     *
     * @private
     * @param {Object} object The object to query.
     * @param {string} key The key of the method to get.
     * @returns {*} Returns the function if it's native, else `undefined`.
     */
    function getNative(object, key) {
      var value = object == null ? undefined : object[key];
      return isNative(value) ? value : undefined;
    }

    /**
     * Gets the view, applying any `transforms` to the `start` and `end` positions.
     *
     * @private
     * @param {number} start The start of the view.
     * @param {number} end The end of the view.
     * @param {Array} transforms The transformations to apply to the view.
     * @returns {Object} Returns an object containing the `start` and `end`
     *  positions of the view.
     */
    function getView(start, end, transforms) {
      var index = -1,
          length = transforms.length;

      while (++index < length) {
        var data = transforms[index],
            size = data.size;

        switch (data.type) {
          case 'drop':      start += size; break;
          case 'dropRight': end -= size; break;
          case 'take':      end = nativeMin(end, start + size); break;
          case 'takeRight': start = nativeMax(start, end - size); break;
        }
      }
      return { 'start': start, 'end': end };
    }

    /**
     * Initializes an array clone.
     *
     * @private
     * @param {Array} array The array to clone.
     * @returns {Array} Returns the initialized clone.
     */
    function initCloneArray(array) {
      var length = array.length,
          result = new array.constructor(length);

      // Add array properties assigned by `RegExp#exec`.
      if (length && typeof array[0] == 'string' && hasOwnProperty.call(array, 'index')) {
        result.index = array.index;
        result.input = array.input;
      }
      return result;
    }

    /**
     * Initializes an object clone.
     *
     * @private
     * @param {Object} object The object to clone.
     * @returns {Object} Returns the initialized clone.
     */
    function initCloneObject(object) {
      var Ctor = object.constructor;
      if (!(typeof Ctor == 'function' && Ctor instanceof Ctor)) {
        Ctor = Object;
      }
      return new Ctor;
    }

    /**
     * Initializes an object clone based on its `toStringTag`.
     *
     * **Note:** This function only supports cloning values with tags of
     * `Boolean`, `Date`, `Error`, `Number`, `RegExp`, or `String`.
     *
     * @private
     * @param {Object} object The object to clone.
     * @param {string} tag The `toStringTag` of the object to clone.
     * @param {boolean} [isDeep] Specify a deep clone.
     * @returns {Object} Returns the initialized clone.
     */
    function initCloneByTag(object, tag, isDeep) {
      var Ctor = object.constructor;
      switch (tag) {
        case arrayBufferTag:
          return bufferClone(object);

        case boolTag:
        case dateTag:
          return new Ctor(+object);

        case float32Tag: case float64Tag:
        case int8Tag: case int16Tag: case int32Tag:
        case uint8Tag: case uint8ClampedTag: case uint16Tag: case uint32Tag:
          var buffer = object.buffer;
          return new Ctor(isDeep ? bufferClone(buffer) : buffer, object.byteOffset, object.length);

        case numberTag:
        case stringTag:
          return new Ctor(object);

        case regexpTag:
          var result = new Ctor(object.source, reFlags.exec(object));
          result.lastIndex = object.lastIndex;
      }
      return result;
    }

    /**
     * Invokes the method at `path` on `object`.
     *
     * @private
     * @param {Object} object The object to query.
     * @param {Array|string} path The path of the method to invoke.
     * @param {Array} args The arguments to invoke the method with.
     * @returns {*} Returns the result of the invoked method.
     */
    function invokePath(object, path, args) {
      if (object != null && !isKey(path, object)) {
        path = toPath(path);
        object = path.length == 1 ? object : baseGet(object, baseSlice(path, 0, -1));
        path = last(path);
      }
      var func = object == null ? object : object[path];
      return func == null ? undefined : func.apply(object, args);
    }

    /**
     * Checks if `value` is array-like.
     *
     * @private
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is array-like, else `false`.
     */
    function isArrayLike(value) {
      return value != null && isLength(getLength(value));
    }

    /**
     * Checks if `value` is a valid array-like index.
     *
     * @private
     * @param {*} value The value to check.
     * @param {number} [length=MAX_SAFE_INTEGER] The upper bounds of a valid index.
     * @returns {boolean} Returns `true` if `value` is a valid index, else `false`.
     */
    function isIndex(value, length) {
      value = (typeof value == 'number' || reIsUint.test(value)) ? +value : -1;
      length = length == null ? MAX_SAFE_INTEGER : length;
      return value > -1 && value % 1 == 0 && value < length;
    }

    /**
     * Checks if the provided arguments are from an iteratee call.
     *
     * @private
     * @param {*} value The potential iteratee value argument.
     * @param {*} index The potential iteratee index or key argument.
     * @param {*} object The potential iteratee object argument.
     * @returns {boolean} Returns `true` if the arguments are from an iteratee call, else `false`.
     */
    function isIterateeCall(value, index, object) {
      if (!isObject(object)) {
        return false;
      }
      var type = typeof index;
      if (type == 'number'
          ? (isArrayLike(object) && isIndex(index, object.length))
          : (type == 'string' && index in object)) {
        var other = object[index];
        return value === value ? (value === other) : (other !== other);
      }
      return false;
    }

    /**
     * Checks if `value` is a property name and not a property path.
     *
     * @private
     * @param {*} value The value to check.
     * @param {Object} [object] The object to query keys on.
     * @returns {boolean} Returns `true` if `value` is a property name, else `false`.
     */
    function isKey(value, object) {
      var type = typeof value;
      if ((type == 'string' && reIsPlainProp.test(value)) || type == 'number') {
        return true;
      }
      if (isArray(value)) {
        return false;
      }
      var result = !reIsDeepProp.test(value);
      return result || (object != null && value in toObject(object));
    }

    /**
     * Checks if `func` has a lazy counterpart.
     *
     * @private
     * @param {Function} func The function to check.
     * @returns {boolean} Returns `true` if `func` has a lazy counterpart, else `false`.
     */
    function isLaziable(func) {
      var funcName = getFuncName(func);
      if (!(funcName in LazyWrapper.prototype)) {
        return false;
      }
      var other = lodash[funcName];
      if (func === other) {
        return true;
      }
      var data = getData(other);
      return !!data && func === data[0];
    }

    /**
     * Checks if `value` is a valid array-like length.
     *
     * **Note:** This function is based on [`ToLength`](http://ecma-international.org/ecma-262/6.0/#sec-tolength).
     *
     * @private
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is a valid length, else `false`.
     */
    function isLength(value) {
      return typeof value == 'number' && value > -1 && value % 1 == 0 && value <= MAX_SAFE_INTEGER;
    }

    /**
     * Checks if `value` is suitable for strict equality comparisons, i.e. `===`.
     *
     * @private
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` if suitable for strict
     *  equality comparisons, else `false`.
     */
    function isStrictComparable(value) {
      return value === value && !isObject(value);
    }

    /**
     * Merges the function metadata of `source` into `data`.
     *
     * Merging metadata reduces the number of wrappers required to invoke a function.
     * This is possible because methods like `_.bind`, `_.curry`, and `_.partial`
     * may be applied regardless of execution order. Methods like `_.ary` and `_.rearg`
     * augment function arguments, making the order in which they are executed important,
     * preventing the merging of metadata. However, we make an exception for a safe
     * common case where curried functions have `_.ary` and or `_.rearg` applied.
     *
     * @private
     * @param {Array} data The destination metadata.
     * @param {Array} source The source metadata.
     * @returns {Array} Returns `data`.
     */
    function mergeData(data, source) {
      var bitmask = data[1],
          srcBitmask = source[1],
          newBitmask = bitmask | srcBitmask,
          isCommon = newBitmask < ARY_FLAG;

      var isCombo =
        (srcBitmask == ARY_FLAG && bitmask == CURRY_FLAG) ||
        (srcBitmask == ARY_FLAG && bitmask == REARG_FLAG && data[7].length <= source[8]) ||
        (srcBitmask == (ARY_FLAG | REARG_FLAG) && bitmask == CURRY_FLAG);

      // Exit early if metadata can't be merged.
      if (!(isCommon || isCombo)) {
        return data;
      }
      // Use source `thisArg` if available.
      if (srcBitmask & BIND_FLAG) {
        data[2] = source[2];
        // Set when currying a bound function.
        newBitmask |= (bitmask & BIND_FLAG) ? 0 : CURRY_BOUND_FLAG;
      }
      // Compose partial arguments.
      var value = source[3];
      if (value) {
        var partials = data[3];
        data[3] = partials ? composeArgs(partials, value, source[4]) : arrayCopy(value);
        data[4] = partials ? replaceHolders(data[3], PLACEHOLDER) : arrayCopy(source[4]);
      }
      // Compose partial right arguments.
      value = source[5];
      if (value) {
        partials = data[5];
        data[5] = partials ? composeArgsRight(partials, value, source[6]) : arrayCopy(value);
        data[6] = partials ? replaceHolders(data[5], PLACEHOLDER) : arrayCopy(source[6]);
      }
      // Use source `argPos` if available.
      value = source[7];
      if (value) {
        data[7] = arrayCopy(value);
      }
      // Use source `ary` if it's smaller.
      if (srcBitmask & ARY_FLAG) {
        data[8] = data[8] == null ? source[8] : nativeMin(data[8], source[8]);
      }
      // Use source `arity` if one is not provided.
      if (data[9] == null) {
        data[9] = source[9];
      }
      // Use source `func` and merge bitmasks.
      data[0] = source[0];
      data[1] = newBitmask;

      return data;
    }

    /**
     * Used by `_.defaultsDeep` to customize its `_.merge` use.
     *
     * @private
     * @param {*} objectValue The destination object property value.
     * @param {*} sourceValue The source object property value.
     * @returns {*} Returns the value to assign to the destination object.
     */
    function mergeDefaults(objectValue, sourceValue) {
      return objectValue === undefined ? sourceValue : merge(objectValue, sourceValue, mergeDefaults);
    }

    /**
     * A specialized version of `_.pick` which picks `object` properties specified
     * by `props`.
     *
     * @private
     * @param {Object} object The source object.
     * @param {string[]} props The property names to pick.
     * @returns {Object} Returns the new object.
     */
    function pickByArray(object, props) {
      object = toObject(object);

      var index = -1,
          length = props.length,
          result = {};

      while (++index < length) {
        var key = props[index];
        if (key in object) {
          result[key] = object[key];
        }
      }
      return result;
    }

    /**
     * A specialized version of `_.pick` which picks `object` properties `predicate`
     * returns truthy for.
     *
     * @private
     * @param {Object} object The source object.
     * @param {Function} predicate The function invoked per iteration.
     * @returns {Object} Returns the new object.
     */
    function pickByCallback(object, predicate) {
      var result = {};
      baseForIn(object, function(value, key, object) {
        if (predicate(value, key, object)) {
          result[key] = value;
        }
      });
      return result;
    }

    /**
     * Reorder `array` according to the specified indexes where the element at
     * the first index is assigned as the first element, the element at
     * the second index is assigned as the second element, and so on.
     *
     * @private
     * @param {Array} array The array to reorder.
     * @param {Array} indexes The arranged array indexes.
     * @returns {Array} Returns `array`.
     */
    function reorder(array, indexes) {
      var arrLength = array.length,
          length = nativeMin(indexes.length, arrLength),
          oldArray = arrayCopy(array);

      while (length--) {
        var index = indexes[length];
        array[length] = isIndex(index, arrLength) ? oldArray[index] : undefined;
      }
      return array;
    }

    /**
     * Sets metadata for `func`.
     *
     * **Note:** If this function becomes hot, i.e. is invoked a lot in a short
     * period of time, it will trip its breaker and transition to an identity function
     * to avoid garbage collection pauses in V8. See [V8 issue 2070](https://code.google.com/p/v8/issues/detail?id=2070)
     * for more details.
     *
     * @private
     * @param {Function} func The function to associate metadata with.
     * @param {*} data The metadata.
     * @returns {Function} Returns `func`.
     */
    var setData = (function() {
      var count = 0,
          lastCalled = 0;

      return function(key, value) {
        var stamp = now(),
            remaining = HOT_SPAN - (stamp - lastCalled);

        lastCalled = stamp;
        if (remaining > 0) {
          if (++count >= HOT_COUNT) {
            return key;
          }
        } else {
          count = 0;
        }
        return baseSetData(key, value);
      };
    }());

    /**
     * A fallback implementation of `Object.keys` which creates an array of the
     * own enumerable property names of `object`.
     *
     * @private
     * @param {Object} object The object to query.
     * @returns {Array} Returns the array of property names.
     */
    function shimKeys(object) {
      var props = keysIn(object),
          propsLength = props.length,
          length = propsLength && object.length;

      var allowIndexes = !!length && isLength(length) &&
        (isArray(object) || isArguments(object));

      var index = -1,
          result = [];

      while (++index < propsLength) {
        var key = props[index];
        if ((allowIndexes && isIndex(key, length)) || hasOwnProperty.call(object, key)) {
          result.push(key);
        }
      }
      return result;
    }

    /**
     * Converts `value` to an array-like object if it's not one.
     *
     * @private
     * @param {*} value The value to process.
     * @returns {Array|Object} Returns the array-like object.
     */
    function toIterable(value) {
      if (value == null) {
        return [];
      }
      if (!isArrayLike(value)) {
        return values(value);
      }
      return isObject(value) ? value : Object(value);
    }

    /**
     * Converts `value` to an object if it's not one.
     *
     * @private
     * @param {*} value The value to process.
     * @returns {Object} Returns the object.
     */
    function toObject(value) {
      return isObject(value) ? value : Object(value);
    }

    /**
     * Converts `value` to property path array if it's not one.
     *
     * @private
     * @param {*} value The value to process.
     * @returns {Array} Returns the property path array.
     */
    function toPath(value) {
      if (isArray(value)) {
        return value;
      }
      var result = [];
      baseToString(value).replace(rePropName, function(match, number, quote, string) {
        result.push(quote ? string.replace(reEscapeChar, '$1') : (number || match));
      });
      return result;
    }

    /**
     * Creates a clone of `wrapper`.
     *
     * @private
     * @param {Object} wrapper The wrapper to clone.
     * @returns {Object} Returns the cloned wrapper.
     */
    function wrapperClone(wrapper) {
      return wrapper instanceof LazyWrapper
        ? wrapper.clone()
        : new LodashWrapper(wrapper.__wrapped__, wrapper.__chain__, arrayCopy(wrapper.__actions__));
    }

    /*------------------------------------------------------------------------*/

    /**
     * Creates an array of elements split into groups the length of `size`.
     * If `collection` can't be split evenly, the final chunk will be the remaining
     * elements.
     *
     * @static
     * @memberOf _
     * @category Array
     * @param {Array} array The array to process.
     * @param {number} [size=1] The length of each chunk.
     * @param- {Object} [guard] Enables use as a callback for functions like `_.map`.
     * @returns {Array} Returns the new array containing chunks.
     * @example
     *
     * _.chunk(['a', 'b', 'c', 'd'], 2);
     * // => [['a', 'b'], ['c', 'd']]
     *
     * _.chunk(['a', 'b', 'c', 'd'], 3);
     * // => [['a', 'b', 'c'], ['d']]
     */
    function chunk(array, size, guard) {
      if (guard ? isIterateeCall(array, size, guard) : size == null) {
        size = 1;
      } else {
        size = nativeMax(nativeFloor(size) || 1, 1);
      }
      var index = 0,
          length = array ? array.length : 0,
          resIndex = -1,
          result = Array(nativeCeil(length / size));

      while (index < length) {
        result[++resIndex] = baseSlice(array, index, (index += size));
      }
      return result;
    }

    /**
     * Creates an array with all falsey values removed. The values `false`, `null`,
     * `0`, `""`, `undefined`, and `NaN` are falsey.
     *
     * @static
     * @memberOf _
     * @category Array
     * @param {Array} array The array to compact.
     * @returns {Array} Returns the new array of filtered values.
     * @example
     *
     * _.compact([0, 1, false, 2, '', 3]);
     * // => [1, 2, 3]
     */
    function compact(array) {
      var index = -1,
          length = array ? array.length : 0,
          resIndex = -1,
          result = [];

      while (++index < length) {
        var value = array[index];
        if (value) {
          result[++resIndex] = value;
        }
      }
      return result;
    }

    /**
     * Creates an array of unique `array` values not included in the other
     * provided arrays using [`SameValueZero`](http://ecma-international.org/ecma-262/6.0/#sec-samevaluezero)
     * for equality comparisons.
     *
     * @static
     * @memberOf _
     * @category Array
     * @param {Array} array The array to inspect.
     * @param {...Array} [values] The arrays of values to exclude.
     * @returns {Array} Returns the new array of filtered values.
     * @example
     *
     * _.difference([1, 2, 3], [4, 2]);
     * // => [1, 3]
     */
    var difference = restParam(function(array, values) {
      return (isObjectLike(array) && isArrayLike(array))
        ? baseDifference(array, baseFlatten(values, false, true))
        : [];
    });

    /**
     * Creates a slice of `array` with `n` elements dropped from the beginning.
     *
     * @static
     * @memberOf _
     * @category Array
     * @param {Array} array The array to query.
     * @param {number} [n=1] The number of elements to drop.
     * @param- {Object} [guard] Enables use as a callback for functions like `_.map`.
     * @returns {Array} Returns the slice of `array`.
     * @example
     *
     * _.drop([1, 2, 3]);
     * // => [2, 3]
     *
     * _.drop([1, 2, 3], 2);
     * // => [3]
     *
     * _.drop([1, 2, 3], 5);
     * // => []
     *
     * _.drop([1, 2, 3], 0);
     * // => [1, 2, 3]
     */
    function drop(array, n, guard) {
      var length = array ? array.length : 0;
      if (!length) {
        return [];
      }
      if (guard ? isIterateeCall(array, n, guard) : n == null) {
        n = 1;
      }
      return baseSlice(array, n < 0 ? 0 : n);
    }

    /**
     * Creates a slice of `array` with `n` elements dropped from the end.
     *
     * @static
     * @memberOf _
     * @category Array
     * @param {Array} array The array to query.
     * @param {number} [n=1] The number of elements to drop.
     * @param- {Object} [guard] Enables use as a callback for functions like `_.map`.
     * @returns {Array} Returns the slice of `array`.
     * @example
     *
     * _.dropRight([1, 2, 3]);
     * // => [1, 2]
     *
     * _.dropRight([1, 2, 3], 2);
     * // => [1]
     *
     * _.dropRight([1, 2, 3], 5);
     * // => []
     *
     * _.dropRight([1, 2, 3], 0);
     * // => [1, 2, 3]
     */
    function dropRight(array, n, guard) {
      var length = array ? array.length : 0;
      if (!length) {
        return [];
      }
      if (guard ? isIterateeCall(array, n, guard) : n == null) {
        n = 1;
      }
      n = length - (+n || 0);
      return baseSlice(array, 0, n < 0 ? 0 : n);
    }

    /**
     * Creates a slice of `array` excluding elements dropped from the end.
     * Elements are dropped until `predicate` returns falsey. The predicate is
     * bound to `thisArg` and invoked with three arguments: (value, index, array).
     *
     * If a property name is provided for `predicate` the created `_.property`
     * style callback returns the property value of the given element.
     *
     * If a value is also provided for `thisArg` the created `_.matchesProperty`
     * style callback returns `true` for elements that have a matching property
     * value, else `false`.
     *
     * If an object is provided for `predicate` the created `_.matches` style
     * callback returns `true` for elements that match the properties of the given
     * object, else `false`.
     *
     * @static
     * @memberOf _
     * @category Array
     * @param {Array} array The array to query.
     * @param {Function|Object|string} [predicate=_.identity] The function invoked
     *  per iteration.
     * @param {*} [thisArg] The `this` binding of `predicate`.
     * @returns {Array} Returns the slice of `array`.
     * @example
     *
     * _.dropRightWhile([1, 2, 3], function(n) {
     *   return n > 1;
     * });
     * // => [1]
     *
     * var users = [
     *   { 'user': 'barney',  'active': true },
     *   { 'user': 'fred',    'active': false },
     *   { 'user': 'pebbles', 'active': false }
     * ];
     *
     * // using the `_.matches` callback shorthand
     * _.pluck(_.dropRightWhile(users, { 'user': 'pebbles', 'active': false }), 'user');
     * // => ['barney', 'fred']
     *
     * // using the `_.matchesProperty` callback shorthand
     * _.pluck(_.dropRightWhile(users, 'active', false), 'user');
     * // => ['barney']
     *
     * // using the `_.property` callback shorthand
     * _.pluck(_.dropRightWhile(users, 'active'), 'user');
     * // => ['barney', 'fred', 'pebbles']
     */
    function dropRightWhile(array, predicate, thisArg) {
      return (array && array.length)
        ? baseWhile(array, getCallback(predicate, thisArg, 3), true, true)
        : [];
    }

    /**
     * Creates a slice of `array` excluding elements dropped from the beginning.
     * Elements are dropped until `predicate` returns falsey. The predicate is
     * bound to `thisArg` and invoked with three arguments: (value, index, array).
     *
     * If a property name is provided for `predicate` the created `_.property`
     * style callback returns the property value of the given element.
     *
     * If a value is also provided for `thisArg` the created `_.matchesProperty`
     * style callback returns `true` for elements that have a matching property
     * value, else `false`.
     *
     * If an object is provided for `predicate` the created `_.matches` style
     * callback returns `true` for elements that have the properties of the given
     * object, else `false`.
     *
     * @static
     * @memberOf _
     * @category Array
     * @param {Array} array The array to query.
     * @param {Function|Object|string} [predicate=_.identity] The function invoked
     *  per iteration.
     * @param {*} [thisArg] The `this` binding of `predicate`.
     * @returns {Array} Returns the slice of `array`.
     * @example
     *
     * _.dropWhile([1, 2, 3], function(n) {
     *   return n < 3;
     * });
     * // => [3]
     *
     * var users = [
     *   { 'user': 'barney',  'active': false },
     *   { 'user': 'fred',    'active': false },
     *   { 'user': 'pebbles', 'active': true }
     * ];
     *
     * // using the `_.matches` callback shorthand
     * _.pluck(_.dropWhile(users, { 'user': 'barney', 'active': false }), 'user');
     * // => ['fred', 'pebbles']
     *
     * // using the `_.matchesProperty` callback shorthand
     * _.pluck(_.dropWhile(users, 'active', false), 'user');
     * // => ['pebbles']
     *
     * // using the `_.property` callback shorthand
     * _.pluck(_.dropWhile(users, 'active'), 'user');
     * // => ['barney', 'fred', 'pebbles']
     */
    function dropWhile(array, predicate, thisArg) {
      return (array && array.length)
        ? baseWhile(array, getCallback(predicate, thisArg, 3), true)
        : [];
    }

    /**
     * Fills elements of `array` with `value` from `start` up to, but not
     * including, `end`.
     *
     * **Note:** This method mutates `array`.
     *
     * @static
     * @memberOf _
     * @category Array
     * @param {Array} array The array to fill.
     * @param {*} value The value to fill `array` with.
     * @param {number} [start=0] The start position.
     * @param {number} [end=array.length] The end position.
     * @returns {Array} Returns `array`.
     * @example
     *
     * var array = [1, 2, 3];
     *
     * _.fill(array, 'a');
     * console.log(array);
     * // => ['a', 'a', 'a']
     *
     * _.fill(Array(3), 2);
     * // => [2, 2, 2]
     *
     * _.fill([4, 6, 8], '*', 1, 2);
     * // => [4, '*', 8]
     */
    function fill(array, value, start, end) {
      var length = array ? array.length : 0;
      if (!length) {
        return [];
      }
      if (start && typeof start != 'number' && isIterateeCall(array, value, start)) {
        start = 0;
        end = length;
      }
      return baseFill(array, value, start, end);
    }

    /**
     * This method is like `_.find` except that it returns the index of the first
     * element `predicate` returns truthy for instead of the element itself.
     *
     * If a property name is provided for `predicate` the created `_.property`
     * style callback returns the property value of the given element.
     *
     * If a value is also provided for `thisArg` the created `_.matchesProperty`
     * style callback returns `true` for elements that have a matching property
     * value, else `false`.
     *
     * If an object is provided for `predicate` the created `_.matches` style
     * callback returns `true` for elements that have the properties of the given
     * object, else `false`.
     *
     * @static
     * @memberOf _
     * @category Array
     * @param {Array} array The array to search.
     * @param {Function|Object|string} [predicate=_.identity] The function invoked
     *  per iteration.
     * @param {*} [thisArg] The `this` binding of `predicate`.
     * @returns {number} Returns the index of the found element, else `-1`.
     * @example
     *
     * var users = [
     *   { 'user': 'barney',  'active': false },
     *   { 'user': 'fred',    'active': false },
     *   { 'user': 'pebbles', 'active': true }
     * ];
     *
     * _.findIndex(users, function(chr) {
     *   return chr.user == 'barney';
     * });
     * // => 0
     *
     * // using the `_.matches` callback shorthand
     * _.findIndex(users, { 'user': 'fred', 'active': false });
     * // => 1
     *
     * // using the `_.matchesProperty` callback shorthand
     * _.findIndex(users, 'active', false);
     * // => 0
     *
     * // using the `_.property` callback shorthand
     * _.findIndex(users, 'active');
     * // => 2
     */
    var findIndex = createFindIndex();

    /**
     * This method is like `_.findIndex` except that it iterates over elements
     * of `collection` from right to left.
     *
     * If a property name is provided for `predicate` the created `_.property`
     * style callback returns the property value of the given element.
     *
     * If a value is also provided for `thisArg` the created `_.matchesProperty`
     * style callback returns `true` for elements that have a matching property
     * value, else `false`.
     *
     * If an object is provided for `predicate` the created `_.matches` style
     * callback returns `true` for elements that have the properties of the given
     * object, else `false`.
     *
     * @static
     * @memberOf _
     * @category Array
     * @param {Array} array The array to search.
     * @param {Function|Object|string} [predicate=_.identity] The function invoked
     *  per iteration.
     * @param {*} [thisArg] The `this` binding of `predicate`.
     * @returns {number} Returns the index of the found element, else `-1`.
     * @example
     *
     * var users = [
     *   { 'user': 'barney',  'active': true },
     *   { 'user': 'fred',    'active': false },
     *   { 'user': 'pebbles', 'active': false }
     * ];
     *
     * _.findLastIndex(users, function(chr) {
     *   return chr.user == 'pebbles';
     * });
     * // => 2
     *
     * // using the `_.matches` callback shorthand
     * _.findLastIndex(users, { 'user': 'barney', 'active': true });
     * // => 0
     *
     * // using the `_.matchesProperty` callback shorthand
     * _.findLastIndex(users, 'active', false);
     * // => 2
     *
     * // using the `_.property` callback shorthand
     * _.findLastIndex(users, 'active');
     * // => 0
     */
    var findLastIndex = createFindIndex(true);

    /**
     * Gets the first element of `array`.
     *
     * @static
     * @memberOf _
     * @alias head
     * @category Array
     * @param {Array} array The array to query.
     * @returns {*} Returns the first element of `array`.
     * @example
     *
     * _.first([1, 2, 3]);
     * // => 1
     *
     * _.first([]);
     * // => undefined
     */
    function first(array) {
      return array ? array[0] : undefined;
    }

    /**
     * Flattens a nested array. If `isDeep` is `true` the array is recursively
     * flattened, otherwise it is only flattened a single level.
     *
     * @static
     * @memberOf _
     * @category Array
     * @param {Array} array The array to flatten.
     * @param {boolean} [isDeep] Specify a deep flatten.
     * @param- {Object} [guard] Enables use as a callback for functions like `_.map`.
     * @returns {Array} Returns the new flattened array.
     * @example
     *
     * _.flatten([1, [2, 3, [4]]]);
     * // => [1, 2, 3, [4]]
     *
     * // using `isDeep`
     * _.flatten([1, [2, 3, [4]]], true);
     * // => [1, 2, 3, 4]
     */
    function flatten(array, isDeep, guard) {
      var length = array ? array.length : 0;
      if (guard && isIterateeCall(array, isDeep, guard)) {
        isDeep = false;
      }
      return length ? baseFlatten(array, isDeep) : [];
    }

    /**
     * Recursively flattens a nested array.
     *
     * @static
     * @memberOf _
     * @category Array
     * @param {Array} array The array to recursively flatten.
     * @returns {Array} Returns the new flattened array.
     * @example
     *
     * _.flattenDeep([1, [2, 3, [4]]]);
     * // => [1, 2, 3, 4]
     */
    function flattenDeep(array) {
      var length = array ? array.length : 0;
      return length ? baseFlatten(array, true) : [];
    }

    /**
     * Gets the index at which the first occurrence of `value` is found in `array`
     * using [`SameValueZero`](http://ecma-international.org/ecma-262/6.0/#sec-samevaluezero)
     * for equality comparisons. If `fromIndex` is negative, it is used as the offset
     * from the end of `array`. If `array` is sorted providing `true` for `fromIndex`
     * performs a faster binary search.
     *
     * @static
     * @memberOf _
     * @category Array
     * @param {Array} array The array to search.
     * @param {*} value The value to search for.
     * @param {boolean|number} [fromIndex=0] The index to search from or `true`
     *  to perform a binary search on a sorted array.
     * @returns {number} Returns the index of the matched value, else `-1`.
     * @example
     *
     * _.indexOf([1, 2, 1, 2], 2);
     * // => 1
     *
     * // using `fromIndex`
     * _.indexOf([1, 2, 1, 2], 2, 2);
     * // => 3
     *
     * // performing a binary search
     * _.indexOf([1, 1, 2, 2], 2, true);
     * // => 2
     */
    function indexOf(array, value, fromIndex) {
      var length = array ? array.length : 0;
      if (!length) {
        return -1;
      }
      if (typeof fromIndex == 'number') {
        fromIndex = fromIndex < 0 ? nativeMax(length + fromIndex, 0) : fromIndex;
      } else if (fromIndex) {
        var index = binaryIndex(array, value);
        if (index < length &&
            (value === value ? (value === array[index]) : (array[index] !== array[index]))) {
          return index;
        }
        return -1;
      }
      return baseIndexOf(array, value, fromIndex || 0);
    }

    /**
     * Gets all but the last element of `array`.
     *
     * @static
     * @memberOf _
     * @category Array
     * @param {Array} array The array to query.
     * @returns {Array} Returns the slice of `array`.
     * @example
     *
     * _.initial([1, 2, 3]);
     * // => [1, 2]
     */
    function initial(array) {
      return dropRight(array, 1);
    }

    /**
     * Creates an array of unique values that are included in all of the provided
     * arrays using [`SameValueZero`](http://ecma-international.org/ecma-262/6.0/#sec-samevaluezero)
     * for equality comparisons.
     *
     * @static
     * @memberOf _
     * @category Array
     * @param {...Array} [arrays] The arrays to inspect.
     * @returns {Array} Returns the new array of shared values.
     * @example
     * _.intersection([1, 2], [4, 2], [2, 1]);
     * // => [2]
     */
    var intersection = restParam(function(arrays) {
      var othLength = arrays.length,
          othIndex = othLength,
          caches = Array(length),
          indexOf = getIndexOf(),
          isCommon = indexOf == baseIndexOf,
          result = [];

      while (othIndex--) {
        var value = arrays[othIndex] = isArrayLike(value = arrays[othIndex]) ? value : [];
        caches[othIndex] = (isCommon && value.length >= 120) ? createCache(othIndex && value) : null;
      }
      var array = arrays[0],
          index = -1,
          length = array ? array.length : 0,
          seen = caches[0];

      outer:
      while (++index < length) {
        value = array[index];
        if ((seen ? cacheIndexOf(seen, value) : indexOf(result, value, 0)) < 0) {
          var othIndex = othLength;
          while (--othIndex) {
            var cache = caches[othIndex];
            if ((cache ? cacheIndexOf(cache, value) : indexOf(arrays[othIndex], value, 0)) < 0) {
              continue outer;
            }
          }
          if (seen) {
            seen.push(value);
          }
          result.push(value);
        }
      }
      return result;
    });

    /**
     * Gets the last element of `array`.
     *
     * @static
     * @memberOf _
     * @category Array
     * @param {Array} array The array to query.
     * @returns {*} Returns the last element of `array`.
     * @example
     *
     * _.last([1, 2, 3]);
     * // => 3
     */
    function last(array) {
      var length = array ? array.length : 0;
      return length ? array[length - 1] : undefined;
    }

    /**
     * This method is like `_.indexOf` except that it iterates over elements of
     * `array` from right to left.
     *
     * @static
     * @memberOf _
     * @category Array
     * @param {Array} array The array to search.
     * @param {*} value The value to search for.
     * @param {boolean|number} [fromIndex=array.length-1] The index to search from
     *  or `true` to perform a binary search on a sorted array.
     * @returns {number} Returns the index of the matched value, else `-1`.
     * @example
     *
     * _.lastIndexOf([1, 2, 1, 2], 2);
     * // => 3
     *
     * // using `fromIndex`
     * _.lastIndexOf([1, 2, 1, 2], 2, 2);
     * // => 1
     *
     * // performing a binary search
     * _.lastIndexOf([1, 1, 2, 2], 2, true);
     * // => 3
     */
    function lastIndexOf(array, value, fromIndex) {
      var length = array ? array.length : 0;
      if (!length) {
        return -1;
      }
      var index = length;
      if (typeof fromIndex == 'number') {
        index = (fromIndex < 0 ? nativeMax(length + fromIndex, 0) : nativeMin(fromIndex || 0, length - 1)) + 1;
      } else if (fromIndex) {
        index = binaryIndex(array, value, true) - 1;
        var other = array[index];
        if (value === value ? (value === other) : (other !== other)) {
          return index;
        }
        return -1;
      }
      if (value !== value) {
        return indexOfNaN(array, index, true);
      }
      while (index--) {
        if (array[index] === value) {
          return index;
        }
      }
      return -1;
    }

    /**
     * Removes all provided values from `array` using
     * [`SameValueZero`](http://ecma-international.org/ecma-262/6.0/#sec-samevaluezero)
     * for equality comparisons.
     *
     * **Note:** Unlike `_.without`, this method mutates `array`.
     *
     * @static
     * @memberOf _
     * @category Array
     * @param {Array} array The array to modify.
     * @param {...*} [values] The values to remove.
     * @returns {Array} Returns `array`.
     * @example
     *
     * var array = [1, 2, 3, 1, 2, 3];
     *
     * _.pull(array, 2, 3);
     * console.log(array);
     * // => [1, 1]
     */
    function pull() {
      var args = arguments,
          array = args[0];

      if (!(array && array.length)) {
        return array;
      }
      var index = 0,
          indexOf = getIndexOf(),
          length = args.length;

      while (++index < length) {
        var fromIndex = 0,
            value = args[index];

        while ((fromIndex = indexOf(array, value, fromIndex)) > -1) {
          splice.call(array, fromIndex, 1);
        }
      }
      return array;
    }

    /**
     * Removes elements from `array` corresponding to the given indexes and returns
     * an array of the removed elements. Indexes may be specified as an array of
     * indexes or as individual arguments.
     *
     * **Note:** Unlike `_.at`, this method mutates `array`.
     *
     * @static
     * @memberOf _
     * @category Array
     * @param {Array} array The array to modify.
     * @param {...(number|number[])} [indexes] The indexes of elements to remove,
     *  specified as individual indexes or arrays of indexes.
     * @returns {Array} Returns the new array of removed elements.
     * @example
     *
     * var array = [5, 10, 15, 20];
     * var evens = _.pullAt(array, 1, 3);
     *
     * console.log(array);
     * // => [5, 15]
     *
     * console.log(evens);
     * // => [10, 20]
     */
    var pullAt = restParam(function(array, indexes) {
      indexes = baseFlatten(indexes);

      var result = baseAt(array, indexes);
      basePullAt(array, indexes.sort(baseCompareAscending));
      return result;
    });

    /**
     * Removes all elements from `array` that `predicate` returns truthy for
     * and returns an array of the removed elements. The predicate is bound to
     * `thisArg` and invoked with three arguments: (value, index, array).
     *
     * If a property name is provided for `predicate` the created `_.property`
     * style callback returns the property value of the given element.
     *
     * If a value is also provided for `thisArg` the created `_.matchesProperty`
     * style callback returns `true` for elements that have a matching property
     * value, else `false`.
     *
     * If an object is provided for `predicate` the created `_.matches` style
     * callback returns `true` for elements that have the properties of the given
     * object, else `false`.
     *
     * **Note:** Unlike `_.filter`, this method mutates `array`.
     *
     * @static
     * @memberOf _
     * @category Array
     * @param {Array} array The array to modify.
     * @param {Function|Object|string} [predicate=_.identity] The function invoked
     *  per iteration.
     * @param {*} [thisArg] The `this` binding of `predicate`.
     * @returns {Array} Returns the new array of removed elements.
     * @example
     *
     * var array = [1, 2, 3, 4];
     * var evens = _.remove(array, function(n) {
     *   return n % 2 == 0;
     * });
     *
     * console.log(array);
     * // => [1, 3]
     *
     * console.log(evens);
     * // => [2, 4]
     */
    function remove(array, predicate, thisArg) {
      var result = [];
      if (!(array && array.length)) {
        return result;
      }
      var index = -1,
          indexes = [],
          length = array.length;

      predicate = getCallback(predicate, thisArg, 3);
      while (++index < length) {
        var value = array[index];
        if (predicate(value, index, array)) {
          result.push(value);
          indexes.push(index);
        }
      }
      basePullAt(array, indexes);
      return result;
    }

    /**
     * Gets all but the first element of `array`.
     *
     * @static
     * @memberOf _
     * @alias tail
     * @category Array
     * @param {Array} array The array to query.
     * @returns {Array} Returns the slice of `array`.
     * @example
     *
     * _.rest([1, 2, 3]);
     * // => [2, 3]
     */
    function rest(array) {
      return drop(array, 1);
    }

    /**
     * Creates a slice of `array` from `start` up to, but not including, `end`.
     *
     * **Note:** This method is used instead of `Array#slice` to support node
     * lists in IE < 9 and to ensure dense arrays are returned.
     *
     * @static
     * @memberOf _
     * @category Array
     * @param {Array} array The array to slice.
     * @param {number} [start=0] The start position.
     * @param {number} [end=array.length] The end position.
     * @returns {Array} Returns the slice of `array`.
     */
    function slice(array, start, end) {
      var length = array ? array.length : 0;
      if (!length) {
        return [];
      }
      if (end && typeof end != 'number' && isIterateeCall(array, start, end)) {
        start = 0;
        end = length;
      }
      return baseSlice(array, start, end);
    }

    /**
     * Uses a binary search to determine the lowest index at which `value` should
     * be inserted into `array` in order to maintain its sort order. If an iteratee
     * function is provided it is invoked for `value` and each element of `array`
     * to compute their sort ranking. The iteratee is bound to `thisArg` and
     * invoked with one argument; (value).
     *
     * If a property name is provided for `iteratee` the created `_.property`
     * style callback returns the property value of the given element.
     *
     * If a value is also provided for `thisArg` the created `_.matchesProperty`
     * style callback returns `true` for elements that have a matching property
     * value, else `false`.
     *
     * If an object is provided for `iteratee` the created `_.matches` style
     * callback returns `true` for elements that have the properties of the given
     * object, else `false`.
     *
     * @static
     * @memberOf _
     * @category Array
     * @param {Array} array The sorted array to inspect.
     * @param {*} value The value to evaluate.
     * @param {Function|Object|string} [iteratee=_.identity] The function invoked
     *  per iteration.
     * @param {*} [thisArg] The `this` binding of `iteratee`.
     * @returns {number} Returns the index at which `value` should be inserted
     *  into `array`.
     * @example
     *
     * _.sortedIndex([30, 50], 40);
     * // => 1
     *
     * _.sortedIndex([4, 4, 5, 5], 5);
     * // => 2
     *
     * var dict = { 'data': { 'thirty': 30, 'forty': 40, 'fifty': 50 } };
     *
     * // using an iteratee function
     * _.sortedIndex(['thirty', 'fifty'], 'forty', function(word) {
     *   return this.data[word];
     * }, dict);
     * // => 1
     *
     * // using the `_.property` callback shorthand
     * _.sortedIndex([{ 'x': 30 }, { 'x': 50 }], { 'x': 40 }, 'x');
     * // => 1
     */
    var sortedIndex = createSortedIndex();

    /**
     * This method is like `_.sortedIndex` except that it returns the highest
     * index at which `value` should be inserted into `array` in order to
     * maintain its sort order.
     *
     * @static
     * @memberOf _
     * @category Array
     * @param {Array} array The sorted array to inspect.
     * @param {*} value The value to evaluate.
     * @param {Function|Object|string} [iteratee=_.identity] The function invoked
     *  per iteration.
     * @param {*} [thisArg] The `this` binding of `iteratee`.
     * @returns {number} Returns the index at which `value` should be inserted
     *  into `array`.
     * @example
     *
     * _.sortedLastIndex([4, 4, 5, 5], 5);
     * // => 4
     */
    var sortedLastIndex = createSortedIndex(true);

    /**
     * Creates a slice of `array` with `n` elements taken from the beginning.
     *
     * @static
     * @memberOf _
     * @category Array
     * @param {Array} array The array to query.
     * @param {number} [n=1] The number of elements to take.
     * @param- {Object} [guard] Enables use as a callback for functions like `_.map`.
     * @returns {Array} Returns the slice of `array`.
     * @example
     *
     * _.take([1, 2, 3]);
     * // => [1]
     *
     * _.take([1, 2, 3], 2);
     * // => [1, 2]
     *
     * _.take([1, 2, 3], 5);
     * // => [1, 2, 3]
     *
     * _.take([1, 2, 3], 0);
     * // => []
     */
    function take(array, n, guard) {
      var length = array ? array.length : 0;
      if (!length) {
        return [];
      }
      if (guard ? isIterateeCall(array, n, guard) : n == null) {
        n = 1;
      }
      return baseSlice(array, 0, n < 0 ? 0 : n);
    }

    /**
     * Creates a slice of `array` with `n` elements taken from the end.
     *
     * @static
     * @memberOf _
     * @category Array
     * @param {Array} array The array to query.
     * @param {number} [n=1] The number of elements to take.
     * @param- {Object} [guard] Enables use as a callback for functions like `_.map`.
     * @returns {Array} Returns the slice of `array`.
     * @example
     *
     * _.takeRight([1, 2, 3]);
     * // => [3]
     *
     * _.takeRight([1, 2, 3], 2);
     * // => [2, 3]
     *
     * _.takeRight([1, 2, 3], 5);
     * // => [1, 2, 3]
     *
     * _.takeRight([1, 2, 3], 0);
     * // => []
     */
    function takeRight(array, n, guard) {
      var length = array ? array.length : 0;
      if (!length) {
        return [];
      }
      if (guard ? isIterateeCall(array, n, guard) : n == null) {
        n = 1;
      }
      n = length - (+n || 0);
      return baseSlice(array, n < 0 ? 0 : n);
    }

    /**
     * Creates a slice of `array` with elements taken from the end. Elements are
     * taken until `predicate` returns falsey. The predicate is bound to `thisArg`
     * and invoked with three arguments: (value, index, array).
     *
     * If a property name is provided for `predicate` the created `_.property`
     * style callback returns the property value of the given element.
     *
     * If a value is also provided for `thisArg` the created `_.matchesProperty`
     * style callback returns `true` for elements that have a matching property
     * value, else `false`.
     *
     * If an object is provided for `predicate` the created `_.matches` style
     * callback returns `true` for elements that have the properties of the given
     * object, else `false`.
     *
     * @static
     * @memberOf _
     * @category Array
     * @param {Array} array The array to query.
     * @param {Function|Object|string} [predicate=_.identity] The function invoked
     *  per iteration.
     * @param {*} [thisArg] The `this` binding of `predicate`.
     * @returns {Array} Returns the slice of `array`.
     * @example
     *
     * _.takeRightWhile([1, 2, 3], function(n) {
     *   return n > 1;
     * });
     * // => [2, 3]
     *
     * var users = [
     *   { 'user': 'barney',  'active': true },
     *   { 'user': 'fred',    'active': false },
     *   { 'user': 'pebbles', 'active': false }
     * ];
     *
     * // using the `_.matches` callback shorthand
     * _.pluck(_.takeRightWhile(users, { 'user': 'pebbles', 'active': false }), 'user');
     * // => ['pebbles']
     *
     * // using the `_.matchesProperty` callback shorthand
     * _.pluck(_.takeRightWhile(users, 'active', false), 'user');
     * // => ['fred', 'pebbles']
     *
     * // using the `_.property` callback shorthand
     * _.pluck(_.takeRightWhile(users, 'active'), 'user');
     * // => []
     */
    function takeRightWhile(array, predicate, thisArg) {
      return (array && array.length)
        ? baseWhile(array, getCallback(predicate, thisArg, 3), false, true)
        : [];
    }

    /**
     * Creates a slice of `array` with elements taken from the beginning. Elements
     * are taken until `predicate` returns falsey. The predicate is bound to
     * `thisArg` and invoked with three arguments: (value, index, array).
     *
     * If a property name is provided for `predicate` the created `_.property`
     * style callback returns the property value of the given element.
     *
     * If a value is also provided for `thisArg` the created `_.matchesProperty`
     * style callback returns `true` for elements that have a matching property
     * value, else `false`.
     *
     * If an object is provided for `predicate` the created `_.matches` style
     * callback returns `true` for elements that have the properties of the given
     * object, else `false`.
     *
     * @static
     * @memberOf _
     * @category Array
     * @param {Array} array The array to query.
     * @param {Function|Object|string} [predicate=_.identity] The function invoked
     *  per iteration.
     * @param {*} [thisArg] The `this` binding of `predicate`.
     * @returns {Array} Returns the slice of `array`.
     * @example
     *
     * _.takeWhile([1, 2, 3], function(n) {
     *   return n < 3;
     * });
     * // => [1, 2]
     *
     * var users = [
     *   { 'user': 'barney',  'active': false },
     *   { 'user': 'fred',    'active': false},
     *   { 'user': 'pebbles', 'active': true }
     * ];
     *
     * // using the `_.matches` callback shorthand
     * _.pluck(_.takeWhile(users, { 'user': 'barney', 'active': false }), 'user');
     * // => ['barney']
     *
     * // using the `_.matchesProperty` callback shorthand
     * _.pluck(_.takeWhile(users, 'active', false), 'user');
     * // => ['barney', 'fred']
     *
     * // using the `_.property` callback shorthand
     * _.pluck(_.takeWhile(users, 'active'), 'user');
     * // => []
     */
    function takeWhile(array, predicate, thisArg) {
      return (array && array.length)
        ? baseWhile(array, getCallback(predicate, thisArg, 3))
        : [];
    }

    /**
     * Creates an array of unique values, in order, from all of the provided arrays
     * using [`SameValueZero`](http://ecma-international.org/ecma-262/6.0/#sec-samevaluezero)
     * for equality comparisons.
     *
     * @static
     * @memberOf _
     * @category Array
     * @param {...Array} [arrays] The arrays to inspect.
     * @returns {Array} Returns the new array of combined values.
     * @example
     *
     * _.union([1, 2], [4, 2], [2, 1]);
     * // => [1, 2, 4]
     */
    var union = restParam(function(arrays) {
      return baseUniq(baseFlatten(arrays, false, true));
    });

    /**
     * Creates a duplicate-free version of an array, using
     * [`SameValueZero`](http://ecma-international.org/ecma-262/6.0/#sec-samevaluezero)
     * for equality comparisons, in which only the first occurence of each element
     * is kept. Providing `true` for `isSorted` performs a faster search algorithm
     * for sorted arrays. If an iteratee function is provided it is invoked for
     * each element in the array to generate the criterion by which uniqueness
     * is computed. The `iteratee` is bound to `thisArg` and invoked with three
     * arguments: (value, index, array).
     *
     * If a property name is provided for `iteratee` the created `_.property`
     * style callback returns the property value of the given element.
     *
     * If a value is also provided for `thisArg` the created `_.matchesProperty`
     * style callback returns `true` for elements that have a matching property
     * value, else `false`.
     *
     * If an object is provided for `iteratee` the created `_.matches` style
     * callback returns `true` for elements that have the properties of the given
     * object, else `false`.
     *
     * @static
     * @memberOf _
     * @alias unique
     * @category Array
     * @param {Array} array The array to inspect.
     * @param {boolean} [isSorted] Specify the array is sorted.
     * @param {Function|Object|string} [iteratee] The function invoked per iteration.
     * @param {*} [thisArg] The `this` binding of `iteratee`.
     * @returns {Array} Returns the new duplicate-value-free array.
     * @example
     *
     * _.uniq([2, 1, 2]);
     * // => [2, 1]
     *
     * // using `isSorted`
     * _.uniq([1, 1, 2], true);
     * // => [1, 2]
     *
     * // using an iteratee function
     * _.uniq([1, 2.5, 1.5, 2], function(n) {
     *   return this.floor(n);
     * }, Math);
     * // => [1, 2.5]
     *
     * // using the `_.property` callback shorthand
     * _.uniq([{ 'x': 1 }, { 'x': 2 }, { 'x': 1 }], 'x');
     * // => [{ 'x': 1 }, { 'x': 2 }]
     */
    function uniq(array, isSorted, iteratee, thisArg) {
      var length = array ? array.length : 0;
      if (!length) {
        return [];
      }
      if (isSorted != null && typeof isSorted != 'boolean') {
        thisArg = iteratee;
        iteratee = isIterateeCall(array, isSorted, thisArg) ? undefined : isSorted;
        isSorted = false;
      }
      var callback = getCallback();
      if (!(iteratee == null && callback === baseCallback)) {
        iteratee = callback(iteratee, thisArg, 3);
      }
      return (isSorted && getIndexOf() == baseIndexOf)
        ? sortedUniq(array, iteratee)
        : baseUniq(array, iteratee);
    }

    /**
     * This method is like `_.zip` except that it accepts an array of grouped
     * elements and creates an array regrouping the elements to their pre-zip
     * configuration.
     *
     * @static
     * @memberOf _
     * @category Array
     * @param {Array} array The array of grouped elements to process.
     * @returns {Array} Returns the new array of regrouped elements.
     * @example
     *
     * var zipped = _.zip(['fred', 'barney'], [30, 40], [true, false]);
     * // => [['fred', 30, true], ['barney', 40, false]]
     *
     * _.unzip(zipped);
     * // => [['fred', 'barney'], [30, 40], [true, false]]
     */
    function unzip(array) {
      if (!(array && array.length)) {
        return [];
      }
      var index = -1,
          length = 0;

      array = arrayFilter(array, function(group) {
        if (isArrayLike(group)) {
          length = nativeMax(group.length, length);
          return true;
        }
      });
      var result = Array(length);
      while (++index < length) {
        result[index] = arrayMap(array, baseProperty(index));
      }
      return result;
    }

    /**
     * This method is like `_.unzip` except that it accepts an iteratee to specify
     * how regrouped values should be combined. The `iteratee` is bound to `thisArg`
     * and invoked with four arguments: (accumulator, value, index, group).
     *
     * @static
     * @memberOf _
     * @category Array
     * @param {Array} array The array of grouped elements to process.
     * @param {Function} [iteratee] The function to combine regrouped values.
     * @param {*} [thisArg] The `this` binding of `iteratee`.
     * @returns {Array} Returns the new array of regrouped elements.
     * @example
     *
     * var zipped = _.zip([1, 2], [10, 20], [100, 200]);
     * // => [[1, 10, 100], [2, 20, 200]]
     *
     * _.unzipWith(zipped, _.add);
     * // => [3, 30, 300]
     */
    function unzipWith(array, iteratee, thisArg) {
      var length = array ? array.length : 0;
      if (!length) {
        return [];
      }
      var result = unzip(array);
      if (iteratee == null) {
        return result;
      }
      iteratee = bindCallback(iteratee, thisArg, 4);
      return arrayMap(result, function(group) {
        return arrayReduce(group, iteratee, undefined, true);
      });
    }

    /**
     * Creates an array excluding all provided values using
     * [`SameValueZero`](http://ecma-international.org/ecma-262/6.0/#sec-samevaluezero)
     * for equality comparisons.
     *
     * @static
     * @memberOf _
     * @category Array
     * @param {Array} array The array to filter.
     * @param {...*} [values] The values to exclude.
     * @returns {Array} Returns the new array of filtered values.
     * @example
     *
     * _.without([1, 2, 1, 3], 1, 2);
     * // => [3]
     */
    var without = restParam(function(array, values) {
      return isArrayLike(array)
        ? baseDifference(array, values)
        : [];
    });

    /**
     * Creates an array of unique values that is the [symmetric difference](https://en.wikipedia.org/wiki/Symmetric_difference)
     * of the provided arrays.
     *
     * @static
     * @memberOf _
     * @category Array
     * @param {...Array} [arrays] The arrays to inspect.
     * @returns {Array} Returns the new array of values.
     * @example
     *
     * _.xor([1, 2], [4, 2]);
     * // => [1, 4]
     */
    function xor() {
      var index = -1,
          length = arguments.length;

      while (++index < length) {
        var array = arguments[index];
        if (isArrayLike(array)) {
          var result = result
            ? arrayPush(baseDifference(result, array), baseDifference(array, result))
            : array;
        }
      }
      return result ? baseUniq(result) : [];
    }

    /**
     * Creates an array of grouped elements, the first of which contains the first
     * elements of the given arrays, the second of which contains the second elements
     * of the given arrays, and so on.
     *
     * @static
     * @memberOf _
     * @category Array
     * @param {...Array} [arrays] The arrays to process.
     * @returns {Array} Returns the new array of grouped elements.
     * @example
     *
     * _.zip(['fred', 'barney'], [30, 40], [true, false]);
     * // => [['fred', 30, true], ['barney', 40, false]]
     */
    var zip = restParam(unzip);

    /**
     * The inverse of `_.pairs`; this method returns an object composed from arrays
     * of property names and values. Provide either a single two dimensional array,
     * e.g. `[[key1, value1], [key2, value2]]` or two arrays, one of property names
     * and one of corresponding values.
     *
     * @static
     * @memberOf _
     * @alias object
     * @category Array
     * @param {Array} props The property names.
     * @param {Array} [values=[]] The property values.
     * @returns {Object} Returns the new object.
     * @example
     *
     * _.zipObject([['fred', 30], ['barney', 40]]);
     * // => { 'fred': 30, 'barney': 40 }
     *
     * _.zipObject(['fred', 'barney'], [30, 40]);
     * // => { 'fred': 30, 'barney': 40 }
     */
    function zipObject(props, values) {
      var index = -1,
          length = props ? props.length : 0,
          result = {};

      if (length && !values && !isArray(props[0])) {
        values = [];
      }
      while (++index < length) {
        var key = props[index];
        if (values) {
          result[key] = values[index];
        } else if (key) {
          result[key[0]] = key[1];
        }
      }
      return result;
    }

    /**
     * This method is like `_.zip` except that it accepts an iteratee to specify
     * how grouped values should be combined. The `iteratee` is bound to `thisArg`
     * and invoked with four arguments: (accumulator, value, index, group).
     *
     * @static
     * @memberOf _
     * @category Array
     * @param {...Array} [arrays] The arrays to process.
     * @param {Function} [iteratee] The function to combine grouped values.
     * @param {*} [thisArg] The `this` binding of `iteratee`.
     * @returns {Array} Returns the new array of grouped elements.
     * @example
     *
     * _.zipWith([1, 2], [10, 20], [100, 200], _.add);
     * // => [111, 222]
     */
    var zipWith = restParam(function(arrays) {
      var length = arrays.length,
          iteratee = length > 2 ? arrays[length - 2] : undefined,
          thisArg = length > 1 ? arrays[length - 1] : undefined;

      if (length > 2 && typeof iteratee == 'function') {
        length -= 2;
      } else {
        iteratee = (length > 1 && typeof thisArg == 'function') ? (--length, thisArg) : undefined;
        thisArg = undefined;
      }
      arrays.length = length;
      return unzipWith(arrays, iteratee, thisArg);
    });

    /*------------------------------------------------------------------------*/

    /**
     * Creates a `lodash` object that wraps `value` with explicit method
     * chaining enabled.
     *
     * @static
     * @memberOf _
     * @category Chain
     * @param {*} value The value to wrap.
     * @returns {Object} Returns the new `lodash` wrapper instance.
     * @example
     *
     * var users = [
     *   { 'user': 'barney',  'age': 36 },
     *   { 'user': 'fred',    'age': 40 },
     *   { 'user': 'pebbles', 'age': 1 }
     * ];
     *
     * var youngest = _.chain(users)
     *   .sortBy('age')
     *   .map(function(chr) {
     *     return chr.user + ' is ' + chr.age;
     *   })
     *   .first()
     *   .value();
     * // => 'pebbles is 1'
     */
    function chain(value) {
      var result = lodash(value);
      result.__chain__ = true;
      return result;
    }

    /**
     * This method invokes `interceptor` and returns `value`. The interceptor is
     * bound to `thisArg` and invoked with one argument; (value). The purpose of
     * this method is to "tap into" a method chain in order to perform operations
     * on intermediate results within the chain.
     *
     * @static
     * @memberOf _
     * @category Chain
     * @param {*} value The value to provide to `interceptor`.
     * @param {Function} interceptor The function to invoke.
     * @param {*} [thisArg] The `this` binding of `interceptor`.
     * @returns {*} Returns `value`.
     * @example
     *
     * _([1, 2, 3])
     *  .tap(function(array) {
     *    array.pop();
     *  })
     *  .reverse()
     *  .value();
     * // => [2, 1]
     */
    function tap(value, interceptor, thisArg) {
      interceptor.call(thisArg, value);
      return value;
    }

    /**
     * This method is like `_.tap` except that it returns the result of `interceptor`.
     *
     * @static
     * @memberOf _
     * @category Chain
     * @param {*} value The value to provide to `interceptor`.
     * @param {Function} interceptor The function to invoke.
     * @param {*} [thisArg] The `this` binding of `interceptor`.
     * @returns {*} Returns the result of `interceptor`.
     * @example
     *
     * _('  abc  ')
     *  .chain()
     *  .trim()
     *  .thru(function(value) {
     *    return [value];
     *  })
     *  .value();
     * // => ['abc']
     */
    function thru(value, interceptor, thisArg) {
      return interceptor.call(thisArg, value);
    }

    /**
     * Enables explicit method chaining on the wrapper object.
     *
     * @name chain
     * @memberOf _
     * @category Chain
     * @returns {Object} Returns the new `lodash` wrapper instance.
     * @example
     *
     * var users = [
     *   { 'user': 'barney', 'age': 36 },
     *   { 'user': 'fred',   'age': 40 }
     * ];
     *
     * // without explicit chaining
     * _(users).first();
     * // => { 'user': 'barney', 'age': 36 }
     *
     * // with explicit chaining
     * _(users).chain()
     *   .first()
     *   .pick('user')
     *   .value();
     * // => { 'user': 'barney' }
     */
    function wrapperChain() {
      return chain(this);
    }

    /**
     * Executes the chained sequence and returns the wrapped result.
     *
     * @name commit
     * @memberOf _
     * @category Chain
     * @returns {Object} Returns the new `lodash` wrapper instance.
     * @example
     *
     * var array = [1, 2];
     * var wrapped = _(array).push(3);
     *
     * console.log(array);
     * // => [1, 2]
     *
     * wrapped = wrapped.commit();
     * console.log(array);
     * // => [1, 2, 3]
     *
     * wrapped.last();
     * // => 3
     *
     * console.log(array);
     * // => [1, 2, 3]
     */
    function wrapperCommit() {
      return new LodashWrapper(this.value(), this.__chain__);
    }

    /**
     * Creates a new array joining a wrapped array with any additional arrays
     * and/or values.
     *
     * @name concat
     * @memberOf _
     * @category Chain
     * @param {...*} [values] The values to concatenate.
     * @returns {Array} Returns the new concatenated array.
     * @example
     *
     * var array = [1];
     * var wrapped = _(array).concat(2, [3], [[4]]);
     *
     * console.log(wrapped.value());
     * // => [1, 2, 3, [4]]
     *
     * console.log(array);
     * // => [1]
     */
    var wrapperConcat = restParam(function(values) {
      values = baseFlatten(values);
      return this.thru(function(array) {
        return arrayConcat(isArray(array) ? array : [toObject(array)], values);
      });
    });

    /**
     * Creates a clone of the chained sequence planting `value` as the wrapped value.
     *
     * @name plant
     * @memberOf _
     * @category Chain
     * @returns {Object} Returns the new `lodash` wrapper instance.
     * @example
     *
     * var array = [1, 2];
     * var wrapped = _(array).map(function(value) {
     *   return Math.pow(value, 2);
     * });
     *
     * var other = [3, 4];
     * var otherWrapped = wrapped.plant(other);
     *
     * otherWrapped.value();
     * // => [9, 16]
     *
     * wrapped.value();
     * // => [1, 4]
     */
    function wrapperPlant(value) {
      var result,
          parent = this;

      while (parent instanceof baseLodash) {
        var clone = wrapperClone(parent);
        if (result) {
          previous.__wrapped__ = clone;
        } else {
          result = clone;
        }
        var previous = clone;
        parent = parent.__wrapped__;
      }
      previous.__wrapped__ = value;
      return result;
    }

    /**
     * Reverses the wrapped array so the first element becomes the last, the
     * second element becomes the second to last, and so on.
     *
     * **Note:** This method mutates the wrapped array.
     *
     * @name reverse
     * @memberOf _
     * @category Chain
     * @returns {Object} Returns the new reversed `lodash` wrapper instance.
     * @example
     *
     * var array = [1, 2, 3];
     *
     * _(array).reverse().value()
     * // => [3, 2, 1]
     *
     * console.log(array);
     * // => [3, 2, 1]
     */
    function wrapperReverse() {
      var value = this.__wrapped__;

      var interceptor = function(value) {
        return (wrapped && wrapped.__dir__ < 0) ? value : value.reverse();
      };
      if (value instanceof LazyWrapper) {
        var wrapped = value;
        if (this.__actions__.length) {
          wrapped = new LazyWrapper(this);
        }
        wrapped = wrapped.reverse();
        wrapped.__actions__.push({ 'func': thru, 'args': [interceptor], 'thisArg': undefined });
        return new LodashWrapper(wrapped, this.__chain__);
      }
      return this.thru(interceptor);
    }

    /**
     * Produces the result of coercing the unwrapped value to a string.
     *
     * @name toString
     * @memberOf _
     * @category Chain
     * @returns {string} Returns the coerced string value.
     * @example
     *
     * _([1, 2, 3]).toString();
     * // => '1,2,3'
     */
    function wrapperToString() {
      return (this.value() + '');
    }

    /**
     * Executes the chained sequence to extract the unwrapped value.
     *
     * @name value
     * @memberOf _
     * @alias run, toJSON, valueOf
     * @category Chain
     * @returns {*} Returns the resolved unwrapped value.
     * @example
     *
     * _([1, 2, 3]).value();
     * // => [1, 2, 3]
     */
    function wrapperValue() {
      return baseWrapperValue(this.__wrapped__, this.__actions__);
    }

    /*------------------------------------------------------------------------*/

    /**
     * Creates an array of elements corresponding to the given keys, or indexes,
     * of `collection`. Keys may be specified as individual arguments or as arrays
     * of keys.
     *
     * @static
     * @memberOf _
     * @category Collection
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {...(number|number[]|string|string[])} [props] The property names
     *  or indexes of elements to pick, specified individually or in arrays.
     * @returns {Array} Returns the new array of picked elements.
     * @example
     *
     * _.at(['a', 'b', 'c'], [0, 2]);
     * // => ['a', 'c']
     *
     * _.at(['barney', 'fred', 'pebbles'], 0, 2);
     * // => ['barney', 'pebbles']
     */
    var at = restParam(function(collection, props) {
      return baseAt(collection, baseFlatten(props));
    });

    /**
     * Creates an object composed of keys generated from the results of running
     * each element of `collection` through `iteratee`. The corresponding value
     * of each key is the number of times the key was returned by `iteratee`.
     * The `iteratee` is bound to `thisArg` and invoked with three arguments:
     * (value, index|key, collection).
     *
     * If a property name is provided for `iteratee` the created `_.property`
     * style callback returns the property value of the given element.
     *
     * If a value is also provided for `thisArg` the created `_.matchesProperty`
     * style callback returns `true` for elements that have a matching property
     * value, else `false`.
     *
     * If an object is provided for `iteratee` the created `_.matches` style
     * callback returns `true` for elements that have the properties of the given
     * object, else `false`.
     *
     * @static
     * @memberOf _
     * @category Collection
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function|Object|string} [iteratee=_.identity] The function invoked
     *  per iteration.
     * @param {*} [thisArg] The `this` binding of `iteratee`.
     * @returns {Object} Returns the composed aggregate object.
     * @example
     *
     * _.countBy([4.3, 6.1, 6.4], function(n) {
     *   return Math.floor(n);
     * });
     * // => { '4': 1, '6': 2 }
     *
     * _.countBy([4.3, 6.1, 6.4], function(n) {
     *   return this.floor(n);
     * }, Math);
     * // => { '4': 1, '6': 2 }
     *
     * _.countBy(['one', 'two', 'three'], 'length');
     * // => { '3': 2, '5': 1 }
     */
    var countBy = createAggregator(function(result, value, key) {
      hasOwnProperty.call(result, key) ? ++result[key] : (result[key] = 1);
    });

    /**
     * Checks if `predicate` returns truthy for **all** elements of `collection`.
     * The predicate is bound to `thisArg` and invoked with three arguments:
     * (value, index|key, collection).
     *
     * If a property name is provided for `predicate` the created `_.property`
     * style callback returns the property value of the given element.
     *
     * If a value is also provided for `thisArg` the created `_.matchesProperty`
     * style callback returns `true` for elements that have a matching property
     * value, else `false`.
     *
     * If an object is provided for `predicate` the created `_.matches` style
     * callback returns `true` for elements that have the properties of the given
     * object, else `false`.
     *
     * @static
     * @memberOf _
     * @alias all
     * @category Collection
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function|Object|string} [predicate=_.identity] The function invoked
     *  per iteration.
     * @param {*} [thisArg] The `this` binding of `predicate`.
     * @returns {boolean} Returns `true` if all elements pass the predicate check,
     *  else `false`.
     * @example
     *
     * _.every([true, 1, null, 'yes'], Boolean);
     * // => false
     *
     * var users = [
     *   { 'user': 'barney', 'active': false },
     *   { 'user': 'fred',   'active': false }
     * ];
     *
     * // using the `_.matches` callback shorthand
     * _.every(users, { 'user': 'barney', 'active': false });
     * // => false
     *
     * // using the `_.matchesProperty` callback shorthand
     * _.every(users, 'active', false);
     * // => true
     *
     * // using the `_.property` callback shorthand
     * _.every(users, 'active');
     * // => false
     */
    function every(collection, predicate, thisArg) {
      var func = isArray(collection) ? arrayEvery : baseEvery;
      if (thisArg && isIterateeCall(collection, predicate, thisArg)) {
        predicate = undefined;
      }
      if (typeof predicate != 'function' || thisArg !== undefined) {
        predicate = getCallback(predicate, thisArg, 3);
      }
      return func(collection, predicate);
    }

    /**
     * Iterates over elements of `collection`, returning an array of all elements
     * `predicate` returns truthy for. The predicate is bound to `thisArg` and
     * invoked with three arguments: (value, index|key, collection).
     *
     * If a property name is provided for `predicate` the created `_.property`
     * style callback returns the property value of the given element.
     *
     * If a value is also provided for `thisArg` the created `_.matchesProperty`
     * style callback returns `true` for elements that have a matching property
     * value, else `false`.
     *
     * If an object is provided for `predicate` the created `_.matches` style
     * callback returns `true` for elements that have the properties of the given
     * object, else `false`.
     *
     * @static
     * @memberOf _
     * @alias select
     * @category Collection
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function|Object|string} [predicate=_.identity] The function invoked
     *  per iteration.
     * @param {*} [thisArg] The `this` binding of `predicate`.
     * @returns {Array} Returns the new filtered array.
     * @example
     *
     * _.filter([4, 5, 6], function(n) {
     *   return n % 2 == 0;
     * });
     * // => [4, 6]
     *
     * var users = [
     *   { 'user': 'barney', 'age': 36, 'active': true },
     *   { 'user': 'fred',   'age': 40, 'active': false }
     * ];
     *
     * // using the `_.matches` callback shorthand
     * _.pluck(_.filter(users, { 'age': 36, 'active': true }), 'user');
     * // => ['barney']
     *
     * // using the `_.matchesProperty` callback shorthand
     * _.pluck(_.filter(users, 'active', false), 'user');
     * // => ['fred']
     *
     * // using the `_.property` callback shorthand
     * _.pluck(_.filter(users, 'active'), 'user');
     * // => ['barney']
     */
    function filter(collection, predicate, thisArg) {
      var func = isArray(collection) ? arrayFilter : baseFilter;
      predicate = getCallback(predicate, thisArg, 3);
      return func(collection, predicate);
    }

    /**
     * Iterates over elements of `collection`, returning the first element
     * `predicate` returns truthy for. The predicate is bound to `thisArg` and
     * invoked with three arguments: (value, index|key, collection).
     *
     * If a property name is provided for `predicate` the created `_.property`
     * style callback returns the property value of the given element.
     *
     * If a value is also provided for `thisArg` the created `_.matchesProperty`
     * style callback returns `true` for elements that have a matching property
     * value, else `false`.
     *
     * If an object is provided for `predicate` the created `_.matches` style
     * callback returns `true` for elements that have the properties of the given
     * object, else `false`.
     *
     * @static
     * @memberOf _
     * @alias detect
     * @category Collection
     * @param {Array|Object|string} collection The collection to search.
     * @param {Function|Object|string} [predicate=_.identity] The function invoked
     *  per iteration.
     * @param {*} [thisArg] The `this` binding of `predicate`.
     * @returns {*} Returns the matched element, else `undefined`.
     * @example
     *
     * var users = [
     *   { 'user': 'barney',  'age': 36, 'active': true },
     *   { 'user': 'fred',    'age': 40, 'active': false },
     *   { 'user': 'pebbles', 'age': 1,  'active': true }
     * ];
     *
     * _.result(_.find(users, function(chr) {
     *   return chr.age < 40;
     * }), 'user');
     * // => 'barney'
     *
     * // using the `_.matches` callback shorthand
     * _.result(_.find(users, { 'age': 1, 'active': true }), 'user');
     * // => 'pebbles'
     *
     * // using the `_.matchesProperty` callback shorthand
     * _.result(_.find(users, 'active', false), 'user');
     * // => 'fred'
     *
     * // using the `_.property` callback shorthand
     * _.result(_.find(users, 'active'), 'user');
     * // => 'barney'
     */
    var find = createFind(baseEach);

    /**
     * This method is like `_.find` except that it iterates over elements of
     * `collection` from right to left.
     *
     * @static
     * @memberOf _
     * @category Collection
     * @param {Array|Object|string} collection The collection to search.
     * @param {Function|Object|string} [predicate=_.identity] The function invoked
     *  per iteration.
     * @param {*} [thisArg] The `this` binding of `predicate`.
     * @returns {*} Returns the matched element, else `undefined`.
     * @example
     *
     * _.findLast([1, 2, 3, 4], function(n) {
     *   return n % 2 == 1;
     * });
     * // => 3
     */
    var findLast = createFind(baseEachRight, true);

    /**
     * Performs a deep comparison between each element in `collection` and the
     * source object, returning the first element that has equivalent property
     * values.
     *
     * **Note:** This method supports comparing arrays, booleans, `Date` objects,
     * numbers, `Object` objects, regexes, and strings. Objects are compared by
     * their own, not inherited, enumerable properties. For comparing a single
     * own or inherited property value see `_.matchesProperty`.
     *
     * @static
     * @memberOf _
     * @category Collection
     * @param {Array|Object|string} collection The collection to search.
     * @param {Object} source The object of property values to match.
     * @returns {*} Returns the matched element, else `undefined`.
     * @example
     *
     * var users = [
     *   { 'user': 'barney', 'age': 36, 'active': true },
     *   { 'user': 'fred',   'age': 40, 'active': false }
     * ];
     *
     * _.result(_.findWhere(users, { 'age': 36, 'active': true }), 'user');
     * // => 'barney'
     *
     * _.result(_.findWhere(users, { 'age': 40, 'active': false }), 'user');
     * // => 'fred'
     */
    function findWhere(collection, source) {
      return find(collection, baseMatches(source));
    }

    /**
     * Iterates over elements of `collection` invoking `iteratee` for each element.
     * The `iteratee` is bound to `thisArg` and invoked with three arguments:
     * (value, index|key, collection). Iteratee functions may exit iteration early
     * by explicitly returning `false`.
     *
     * **Note:** As with other "Collections" methods, objects with a "length" property
     * are iterated like arrays. To avoid this behavior `_.forIn` or `_.forOwn`
     * may be used for object iteration.
     *
     * @static
     * @memberOf _
     * @alias each
     * @category Collection
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function} [iteratee=_.identity] The function invoked per iteration.
     * @param {*} [thisArg] The `this` binding of `iteratee`.
     * @returns {Array|Object|string} Returns `collection`.
     * @example
     *
     * _([1, 2]).forEach(function(n) {
     *   console.log(n);
     * }).value();
     * // => logs each value from left to right and returns the array
     *
     * _.forEach({ 'a': 1, 'b': 2 }, function(n, key) {
     *   console.log(n, key);
     * });
     * // => logs each value-key pair and returns the object (iteration order is not guaranteed)
     */
    var forEach = createForEach(arrayEach, baseEach);

    /**
     * This method is like `_.forEach` except that it iterates over elements of
     * `collection` from right to left.
     *
     * @static
     * @memberOf _
     * @alias eachRight
     * @category Collection
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function} [iteratee=_.identity] The function invoked per iteration.
     * @param {*} [thisArg] The `this` binding of `iteratee`.
     * @returns {Array|Object|string} Returns `collection`.
     * @example
     *
     * _([1, 2]).forEachRight(function(n) {
     *   console.log(n);
     * }).value();
     * // => logs each value from right to left and returns the array
     */
    var forEachRight = createForEach(arrayEachRight, baseEachRight);

    /**
     * Creates an object composed of keys generated from the results of running
     * each element of `collection` through `iteratee`. The corresponding value
     * of each key is an array of the elements responsible for generating the key.
     * The `iteratee` is bound to `thisArg` and invoked with three arguments:
     * (value, index|key, collection).
     *
     * If a property name is provided for `iteratee` the created `_.property`
     * style callback returns the property value of the given element.
     *
     * If a value is also provided for `thisArg` the created `_.matchesProperty`
     * style callback returns `true` for elements that have a matching property
     * value, else `false`.
     *
     * If an object is provided for `iteratee` the created `_.matches` style
     * callback returns `true` for elements that have the properties of the given
     * object, else `false`.
     *
     * @static
     * @memberOf _
     * @category Collection
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function|Object|string} [iteratee=_.identity] The function invoked
     *  per iteration.
     * @param {*} [thisArg] The `this` binding of `iteratee`.
     * @returns {Object} Returns the composed aggregate object.
     * @example
     *
     * _.groupBy([4.2, 6.1, 6.4], function(n) {
     *   return Math.floor(n);
     * });
     * // => { '4': [4.2], '6': [6.1, 6.4] }
     *
     * _.groupBy([4.2, 6.1, 6.4], function(n) {
     *   return this.floor(n);
     * }, Math);
     * // => { '4': [4.2], '6': [6.1, 6.4] }
     *
     * // using the `_.property` callback shorthand
     * _.groupBy(['one', 'two', 'three'], 'length');
     * // => { '3': ['one', 'two'], '5': ['three'] }
     */
    var groupBy = createAggregator(function(result, value, key) {
      if (hasOwnProperty.call(result, key)) {
        result[key].push(value);
      } else {
        result[key] = [value];
      }
    });

    /**
     * Checks if `value` is in `collection` using
     * [`SameValueZero`](http://ecma-international.org/ecma-262/6.0/#sec-samevaluezero)
     * for equality comparisons. If `fromIndex` is negative, it is used as the offset
     * from the end of `collection`.
     *
     * @static
     * @memberOf _
     * @alias contains, include
     * @category Collection
     * @param {Array|Object|string} collection The collection to search.
     * @param {*} target The value to search for.
     * @param {number} [fromIndex=0] The index to search from.
     * @param- {Object} [guard] Enables use as a callback for functions like `_.reduce`.
     * @returns {boolean} Returns `true` if a matching element is found, else `false`.
     * @example
     *
     * _.includes([1, 2, 3], 1);
     * // => true
     *
     * _.includes([1, 2, 3], 1, 2);
     * // => false
     *
     * _.includes({ 'user': 'fred', 'age': 40 }, 'fred');
     * // => true
     *
     * _.includes('pebbles', 'eb');
     * // => true
     */
    function includes(collection, target, fromIndex, guard) {
      var length = collection ? getLength(collection) : 0;
      if (!isLength(length)) {
        collection = values(collection);
        length = collection.length;
      }
      if (typeof fromIndex != 'number' || (guard && isIterateeCall(target, fromIndex, guard))) {
        fromIndex = 0;
      } else {
        fromIndex = fromIndex < 0 ? nativeMax(length + fromIndex, 0) : (fromIndex || 0);
      }
      return (typeof collection == 'string' || !isArray(collection) && isString(collection))
        ? (fromIndex <= length && collection.indexOf(target, fromIndex) > -1)
        : (!!length && getIndexOf(collection, target, fromIndex) > -1);
    }

    /**
     * Creates an object composed of keys generated from the results of running
     * each element of `collection` through `iteratee`. The corresponding value
     * of each key is the last element responsible for generating the key. The
     * iteratee function is bound to `thisArg` and invoked with three arguments:
     * (value, index|key, collection).
     *
     * If a property name is provided for `iteratee` the created `_.property`
     * style callback returns the property value of the given element.
     *
     * If a value is also provided for `thisArg` the created `_.matchesProperty`
     * style callback returns `true` for elements that have a matching property
     * value, else `false`.
     *
     * If an object is provided for `iteratee` the created `_.matches` style
     * callback returns `true` for elements that have the properties of the given
     * object, else `false`.
     *
     * @static
     * @memberOf _
     * @category Collection
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function|Object|string} [iteratee=_.identity] The function invoked
     *  per iteration.
     * @param {*} [thisArg] The `this` binding of `iteratee`.
     * @returns {Object} Returns the composed aggregate object.
     * @example
     *
     * var keyData = [
     *   { 'dir': 'left', 'code': 97 },
     *   { 'dir': 'right', 'code': 100 }
     * ];
     *
     * _.indexBy(keyData, 'dir');
     * // => { 'left': { 'dir': 'left', 'code': 97 }, 'right': { 'dir': 'right', 'code': 100 } }
     *
     * _.indexBy(keyData, function(object) {
     *   return String.fromCharCode(object.code);
     * });
     * // => { 'a': { 'dir': 'left', 'code': 97 }, 'd': { 'dir': 'right', 'code': 100 } }
     *
     * _.indexBy(keyData, function(object) {
     *   return this.fromCharCode(object.code);
     * }, String);
     * // => { 'a': { 'dir': 'left', 'code': 97 }, 'd': { 'dir': 'right', 'code': 100 } }
     */
    var indexBy = createAggregator(function(result, value, key) {
      result[key] = value;
    });

    /**
     * Invokes the method at `path` of each element in `collection`, returning
     * an array of the results of each invoked method. Any additional arguments
     * are provided to each invoked method. If `methodName` is a function it is
     * invoked for, and `this` bound to, each element in `collection`.
     *
     * @static
     * @memberOf _
     * @category Collection
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Array|Function|string} path The path of the method to invoke or
     *  the function invoked per iteration.
     * @param {...*} [args] The arguments to invoke the method with.
     * @returns {Array} Returns the array of results.
     * @example
     *
     * _.invoke([[5, 1, 7], [3, 2, 1]], 'sort');
     * // => [[1, 5, 7], [1, 2, 3]]
     *
     * _.invoke([123, 456], String.prototype.split, '');
     * // => [['1', '2', '3'], ['4', '5', '6']]
     */
    var invoke = restParam(function(collection, path, args) {
      var index = -1,
          isFunc = typeof path == 'function',
          isProp = isKey(path),
          result = isArrayLike(collection) ? Array(collection.length) : [];

      baseEach(collection, function(value) {
        var func = isFunc ? path : ((isProp && value != null) ? value[path] : undefined);
        result[++index] = func ? func.apply(value, args) : invokePath(value, path, args);
      });
      return result;
    });

    /**
     * Creates an array of values by running each element in `collection` through
     * `iteratee`. The `iteratee` is bound to `thisArg` and invoked with three
     * arguments: (value, index|key, collection).
     *
     * If a property name is provided for `iteratee` the created `_.property`
     * style callback returns the property value of the given element.
     *
     * If a value is also provided for `thisArg` the created `_.matchesProperty`
     * style callback returns `true` for elements that have a matching property
     * value, else `false`.
     *
     * If an object is provided for `iteratee` the created `_.matches` style
     * callback returns `true` for elements that have the properties of the given
     * object, else `false`.
     *
     * Many lodash methods are guarded to work as iteratees for methods like
     * `_.every`, `_.filter`, `_.map`, `_.mapValues`, `_.reject`, and `_.some`.
     *
     * The guarded methods are:
     * `ary`, `callback`, `chunk`, `clone`, `create`, `curry`, `curryRight`,
     * `drop`, `dropRight`, `every`, `fill`, `flatten`, `invert`, `max`, `min`,
     * `parseInt`, `slice`, `sortBy`, `take`, `takeRight`, `template`, `trim`,
     * `trimLeft`, `trimRight`, `trunc`, `random`, `range`, `sample`, `some`,
     * `sum`, `uniq`, and `words`
     *
     * @static
     * @memberOf _
     * @alias collect
     * @category Collection
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function|Object|string} [iteratee=_.identity] The function invoked
     *  per iteration.
     * @param {*} [thisArg] The `this` binding of `iteratee`.
     * @returns {Array} Returns the new mapped array.
     * @example
     *
     * function timesThree(n) {
     *   return n * 3;
     * }
     *
     * _.map([1, 2], timesThree);
     * // => [3, 6]
     *
     * _.map({ 'a': 1, 'b': 2 }, timesThree);
     * // => [3, 6] (iteration order is not guaranteed)
     *
     * var users = [
     *   { 'user': 'barney' },
     *   { 'user': 'fred' }
     * ];
     *
     * // using the `_.property` callback shorthand
     * _.map(users, 'user');
     * // => ['barney', 'fred']
     */
    function map(collection, iteratee, thisArg) {
      var func = isArray(collection) ? arrayMap : baseMap;
      iteratee = getCallback(iteratee, thisArg, 3);
      return func(collection, iteratee);
    }

    /**
     * Creates an array of elements split into two groups, the first of which
     * contains elements `predicate` returns truthy for, while the second of which
     * contains elements `predicate` returns falsey for. The predicate is bound
     * to `thisArg` and invoked with three arguments: (value, index|key, collection).
     *
     * If a property name is provided for `predicate` the created `_.property`
     * style callback returns the property value of the given element.
     *
     * If a value is also provided for `thisArg` the created `_.matchesProperty`
     * style callback returns `true` for elements that have a matching property
     * value, else `false`.
     *
     * If an object is provided for `predicate` the created `_.matches` style
     * callback returns `true` for elements that have the properties of the given
     * object, else `false`.
     *
     * @static
     * @memberOf _
     * @category Collection
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function|Object|string} [predicate=_.identity] The function invoked
     *  per iteration.
     * @param {*} [thisArg] The `this` binding of `predicate`.
     * @returns {Array} Returns the array of grouped elements.
     * @example
     *
     * _.partition([1, 2, 3], function(n) {
     *   return n % 2;
     * });
     * // => [[1, 3], [2]]
     *
     * _.partition([1.2, 2.3, 3.4], function(n) {
     *   return this.floor(n) % 2;
     * }, Math);
     * // => [[1.2, 3.4], [2.3]]
     *
     * var users = [
     *   { 'user': 'barney',  'age': 36, 'active': false },
     *   { 'user': 'fred',    'age': 40, 'active': true },
     *   { 'user': 'pebbles', 'age': 1,  'active': false }
     * ];
     *
     * var mapper = function(array) {
     *   return _.pluck(array, 'user');
     * };
     *
     * // using the `_.matches` callback shorthand
     * _.map(_.partition(users, { 'age': 1, 'active': false }), mapper);
     * // => [['pebbles'], ['barney', 'fred']]
     *
     * // using the `_.matchesProperty` callback shorthand
     * _.map(_.partition(users, 'active', false), mapper);
     * // => [['barney', 'pebbles'], ['fred']]
     *
     * // using the `_.property` callback shorthand
     * _.map(_.partition(users, 'active'), mapper);
     * // => [['fred'], ['barney', 'pebbles']]
     */
    var partition = createAggregator(function(result, value, key) {
      result[key ? 0 : 1].push(value);
    }, function() { return [[], []]; });

    /**
     * Gets the property value of `path` from all elements in `collection`.
     *
     * @static
     * @memberOf _
     * @category Collection
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Array|string} path The path of the property to pluck.
     * @returns {Array} Returns the property values.
     * @example
     *
     * var users = [
     *   { 'user': 'barney', 'age': 36 },
     *   { 'user': 'fred',   'age': 40 }
     * ];
     *
     * _.pluck(users, 'user');
     * // => ['barney', 'fred']
     *
     * var userIndex = _.indexBy(users, 'user');
     * _.pluck(userIndex, 'age');
     * // => [36, 40] (iteration order is not guaranteed)
     */
    function pluck(collection, path) {
      return map(collection, property(path));
    }

    /**
     * Reduces `collection` to a value which is the accumulated result of running
     * each element in `collection` through `iteratee`, where each successive
     * invocation is supplied the return value of the previous. If `accumulator`
     * is not provided the first element of `collection` is used as the initial
     * value. The `iteratee` is bound to `thisArg` and invoked with four arguments:
     * (accumulator, value, index|key, collection).
     *
     * Many lodash methods are guarded to work as iteratees for methods like
     * `_.reduce`, `_.reduceRight`, and `_.transform`.
     *
     * The guarded methods are:
     * `assign`, `defaults`, `defaultsDeep`, `includes`, `merge`, `sortByAll`,
     * and `sortByOrder`
     *
     * @static
     * @memberOf _
     * @alias foldl, inject
     * @category Collection
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function} [iteratee=_.identity] The function invoked per iteration.
     * @param {*} [accumulator] The initial value.
     * @param {*} [thisArg] The `this` binding of `iteratee`.
     * @returns {*} Returns the accumulated value.
     * @example
     *
     * _.reduce([1, 2], function(total, n) {
     *   return total + n;
     * });
     * // => 3
     *
     * _.reduce({ 'a': 1, 'b': 2 }, function(result, n, key) {
     *   result[key] = n * 3;
     *   return result;
     * }, {});
     * // => { 'a': 3, 'b': 6 } (iteration order is not guaranteed)
     */
    var reduce = createReduce(arrayReduce, baseEach);

    /**
     * This method is like `_.reduce` except that it iterates over elements of
     * `collection` from right to left.
     *
     * @static
     * @memberOf _
     * @alias foldr
     * @category Collection
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function} [iteratee=_.identity] The function invoked per iteration.
     * @param {*} [accumulator] The initial value.
     * @param {*} [thisArg] The `this` binding of `iteratee`.
     * @returns {*} Returns the accumulated value.
     * @example
     *
     * var array = [[0, 1], [2, 3], [4, 5]];
     *
     * _.reduceRight(array, function(flattened, other) {
     *   return flattened.concat(other);
     * }, []);
     * // => [4, 5, 2, 3, 0, 1]
     */
    var reduceRight = createReduce(arrayReduceRight, baseEachRight);

    /**
     * The opposite of `_.filter`; this method returns the elements of `collection`
     * that `predicate` does **not** return truthy for.
     *
     * @static
     * @memberOf _
     * @category Collection
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function|Object|string} [predicate=_.identity] The function invoked
     *  per iteration.
     * @param {*} [thisArg] The `this` binding of `predicate`.
     * @returns {Array} Returns the new filtered array.
     * @example
     *
     * _.reject([1, 2, 3, 4], function(n) {
     *   return n % 2 == 0;
     * });
     * // => [1, 3]
     *
     * var users = [
     *   { 'user': 'barney', 'age': 36, 'active': false },
     *   { 'user': 'fred',   'age': 40, 'active': true }
     * ];
     *
     * // using the `_.matches` callback shorthand
     * _.pluck(_.reject(users, { 'age': 40, 'active': true }), 'user');
     * // => ['barney']
     *
     * // using the `_.matchesProperty` callback shorthand
     * _.pluck(_.reject(users, 'active', false), 'user');
     * // => ['fred']
     *
     * // using the `_.property` callback shorthand
     * _.pluck(_.reject(users, 'active'), 'user');
     * // => ['barney']
     */
    function reject(collection, predicate, thisArg) {
      var func = isArray(collection) ? arrayFilter : baseFilter;
      predicate = getCallback(predicate, thisArg, 3);
      return func(collection, function(value, index, collection) {
        return !predicate(value, index, collection);
      });
    }

    /**
     * Gets a random element or `n` random elements from a collection.
     *
     * @static
     * @memberOf _
     * @category Collection
     * @param {Array|Object|string} collection The collection to sample.
     * @param {number} [n] The number of elements to sample.
     * @param- {Object} [guard] Enables use as a callback for functions like `_.map`.
     * @returns {*} Returns the random sample(s).
     * @example
     *
     * _.sample([1, 2, 3, 4]);
     * // => 2
     *
     * _.sample([1, 2, 3, 4], 2);
     * // => [3, 1]
     */
    function sample(collection, n, guard) {
      if (guard ? isIterateeCall(collection, n, guard) : n == null) {
        collection = toIterable(collection);
        var length = collection.length;
        return length > 0 ? collection[baseRandom(0, length - 1)] : undefined;
      }
      var index = -1,
          result = toArray(collection),
          length = result.length,
          lastIndex = length - 1;

      n = nativeMin(n < 0 ? 0 : (+n || 0), length);
      while (++index < n) {
        var rand = baseRandom(index, lastIndex),
            value = result[rand];

        result[rand] = result[index];
        result[index] = value;
      }
      result.length = n;
      return result;
    }

    /**
     * Creates an array of shuffled values, using a version of the
     * [Fisher-Yates shuffle](https://en.wikipedia.org/wiki/Fisher-Yates_shuffle).
     *
     * @static
     * @memberOf _
     * @category Collection
     * @param {Array|Object|string} collection The collection to shuffle.
     * @returns {Array} Returns the new shuffled array.
     * @example
     *
     * _.shuffle([1, 2, 3, 4]);
     * // => [4, 1, 3, 2]
     */
    function shuffle(collection) {
      return sample(collection, POSITIVE_INFINITY);
    }

    /**
     * Gets the size of `collection` by returning its length for array-like
     * values or the number of own enumerable properties for objects.
     *
     * @static
     * @memberOf _
     * @category Collection
     * @param {Array|Object|string} collection The collection to inspect.
     * @returns {number} Returns the size of `collection`.
     * @example
     *
     * _.size([1, 2, 3]);
     * // => 3
     *
     * _.size({ 'a': 1, 'b': 2 });
     * // => 2
     *
     * _.size('pebbles');
     * // => 7
     */
    function size(collection) {
      var length = collection ? getLength(collection) : 0;
      return isLength(length) ? length : keys(collection).length;
    }

    /**
     * Checks if `predicate` returns truthy for **any** element of `collection`.
     * The function returns as soon as it finds a passing value and does not iterate
     * over the entire collection. The predicate is bound to `thisArg` and invoked
     * with three arguments: (value, index|key, collection).
     *
     * If a property name is provided for `predicate` the created `_.property`
     * style callback returns the property value of the given element.
     *
     * If a value is also provided for `thisArg` the created `_.matchesProperty`
     * style callback returns `true` for elements that have a matching property
     * value, else `false`.
     *
     * If an object is provided for `predicate` the created `_.matches` style
     * callback returns `true` for elements that have the properties of the given
     * object, else `false`.
     *
     * @static
     * @memberOf _
     * @alias any
     * @category Collection
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function|Object|string} [predicate=_.identity] The function invoked
     *  per iteration.
     * @param {*} [thisArg] The `this` binding of `predicate`.
     * @returns {boolean} Returns `true` if any element passes the predicate check,
     *  else `false`.
     * @example
     *
     * _.some([null, 0, 'yes', false], Boolean);
     * // => true
     *
     * var users = [
     *   { 'user': 'barney', 'active': true },
     *   { 'user': 'fred',   'active': false }
     * ];
     *
     * // using the `_.matches` callback shorthand
     * _.some(users, { 'user': 'barney', 'active': false });
     * // => false
     *
     * // using the `_.matchesProperty` callback shorthand
     * _.some(users, 'active', false);
     * // => true
     *
     * // using the `_.property` callback shorthand
     * _.some(users, 'active');
     * // => true
     */
    function some(collection, predicate, thisArg) {
      var func = isArray(collection) ? arraySome : baseSome;
      if (thisArg && isIterateeCall(collection, predicate, thisArg)) {
        predicate = undefined;
      }
      if (typeof predicate != 'function' || thisArg !== undefined) {
        predicate = getCallback(predicate, thisArg, 3);
      }
      return func(collection, predicate);
    }

    /**
     * Creates an array of elements, sorted in ascending order by the results of
     * running each element in a collection through `iteratee`. This method performs
     * a stable sort, that is, it preserves the original sort order of equal elements.
     * The `iteratee` is bound to `thisArg` and invoked with three arguments:
     * (value, index|key, collection).
     *
     * If a property name is provided for `iteratee` the created `_.property`
     * style callback returns the property value of the given element.
     *
     * If a value is also provided for `thisArg` the created `_.matchesProperty`
     * style callback returns `true` for elements that have a matching property
     * value, else `false`.
     *
     * If an object is provided for `iteratee` the created `_.matches` style
     * callback returns `true` for elements that have the properties of the given
     * object, else `false`.
     *
     * @static
     * @memberOf _
     * @category Collection
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function|Object|string} [iteratee=_.identity] The function invoked
     *  per iteration.
     * @param {*} [thisArg] The `this` binding of `iteratee`.
     * @returns {Array} Returns the new sorted array.
     * @example
     *
     * _.sortBy([1, 2, 3], function(n) {
     *   return Math.sin(n);
     * });
     * // => [3, 1, 2]
     *
     * _.sortBy([1, 2, 3], function(n) {
     *   return this.sin(n);
     * }, Math);
     * // => [3, 1, 2]
     *
     * var users = [
     *   { 'user': 'fred' },
     *   { 'user': 'pebbles' },
     *   { 'user': 'barney' }
     * ];
     *
     * // using the `_.property` callback shorthand
     * _.pluck(_.sortBy(users, 'user'), 'user');
     * // => ['barney', 'fred', 'pebbles']
     */
    function sortBy(collection, iteratee, thisArg) {
      if (collection == null) {
        return [];
      }
      if (thisArg && isIterateeCall(collection, iteratee, thisArg)) {
        iteratee = undefined;
      }
      var index = -1;
      iteratee = getCallback(iteratee, thisArg, 3);

      var result = baseMap(collection, function(value, key, collection) {
        return { 'criteria': iteratee(value, key, collection), 'index': ++index, 'value': value };
      });
      return baseSortBy(result, compareAscending);
    }

    /**
     * This method is like `_.sortBy` except that it can sort by multiple iteratees
     * or property names.
     *
     * If a property name is provided for an iteratee the created `_.property`
     * style callback returns the property value of the given element.
     *
     * If an object is provided for an iteratee the created `_.matches` style
     * callback returns `true` for elements that have the properties of the given
     * object, else `false`.
     *
     * @static
     * @memberOf _
     * @category Collection
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {...(Function|Function[]|Object|Object[]|string|string[])} iteratees
     *  The iteratees to sort by, specified as individual values or arrays of values.
     * @returns {Array} Returns the new sorted array.
     * @example
     *
     * var users = [
     *   { 'user': 'fred',   'age': 48 },
     *   { 'user': 'barney', 'age': 36 },
     *   { 'user': 'fred',   'age': 42 },
     *   { 'user': 'barney', 'age': 34 }
     * ];
     *
     * _.map(_.sortByAll(users, ['user', 'age']), _.values);
     * // => [['barney', 34], ['barney', 36], ['fred', 42], ['fred', 48]]
     *
     * _.map(_.sortByAll(users, 'user', function(chr) {
     *   return Math.floor(chr.age / 10);
     * }), _.values);
     * // => [['barney', 36], ['barney', 34], ['fred', 48], ['fred', 42]]
     */
    var sortByAll = restParam(function(collection, iteratees) {
      if (collection == null) {
        return [];
      }
      var guard = iteratees[2];
      if (guard && isIterateeCall(iteratees[0], iteratees[1], guard)) {
        iteratees.length = 1;
      }
      return baseSortByOrder(collection, baseFlatten(iteratees), []);
    });

    /**
     * This method is like `_.sortByAll` except that it allows specifying the
     * sort orders of the iteratees to sort by. If `orders` is unspecified, all
     * values are sorted in ascending order. Otherwise, a value is sorted in
     * ascending order if its corresponding order is "asc", and descending if "desc".
     *
     * If a property name is provided for an iteratee the created `_.property`
     * style callback returns the property value of the given element.
     *
     * If an object is provided for an iteratee the created `_.matches` style
     * callback returns `true` for elements that have the properties of the given
     * object, else `false`.
     *
     * @static
     * @memberOf _
     * @category Collection
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function[]|Object[]|string[]} iteratees The iteratees to sort by.
     * @param {boolean[]} [orders] The sort orders of `iteratees`.
     * @param- {Object} [guard] Enables use as a callback for functions like `_.reduce`.
     * @returns {Array} Returns the new sorted array.
     * @example
     *
     * var users = [
     *   { 'user': 'fred',   'age': 48 },
     *   { 'user': 'barney', 'age': 34 },
     *   { 'user': 'fred',   'age': 42 },
     *   { 'user': 'barney', 'age': 36 }
     * ];
     *
     * // sort by `user` in ascending order and by `age` in descending order
     * _.map(_.sortByOrder(users, ['user', 'age'], ['asc', 'desc']), _.values);
     * // => [['barney', 36], ['barney', 34], ['fred', 48], ['fred', 42]]
     */
    function sortByOrder(collection, iteratees, orders, guard) {
      if (collection == null) {
        return [];
      }
      if (guard && isIterateeCall(iteratees, orders, guard)) {
        orders = undefined;
      }
      if (!isArray(iteratees)) {
        iteratees = iteratees == null ? [] : [iteratees];
      }
      if (!isArray(orders)) {
        orders = orders == null ? [] : [orders];
      }
      return baseSortByOrder(collection, iteratees, orders);
    }

    /**
     * Performs a deep comparison between each element in `collection` and the
     * source object, returning an array of all elements that have equivalent
     * property values.
     *
     * **Note:** This method supports comparing arrays, booleans, `Date` objects,
     * numbers, `Object` objects, regexes, and strings. Objects are compared by
     * their own, not inherited, enumerable properties. For comparing a single
     * own or inherited property value see `_.matchesProperty`.
     *
     * @static
     * @memberOf _
     * @category Collection
     * @param {Array|Object|string} collection The collection to search.
     * @param {Object} source The object of property values to match.
     * @returns {Array} Returns the new filtered array.
     * @example
     *
     * var users = [
     *   { 'user': 'barney', 'age': 36, 'active': false, 'pets': ['hoppy'] },
     *   { 'user': 'fred',   'age': 40, 'active': true, 'pets': ['baby puss', 'dino'] }
     * ];
     *
     * _.pluck(_.where(users, { 'age': 36, 'active': false }), 'user');
     * // => ['barney']
     *
     * _.pluck(_.where(users, { 'pets': ['dino'] }), 'user');
     * // => ['fred']
     */
    function where(collection, source) {
      return filter(collection, baseMatches(source));
    }

    /*------------------------------------------------------------------------*/

    /**
     * Gets the number of milliseconds that have elapsed since the Unix epoch
     * (1 January 1970 00:00:00 UTC).
     *
     * @static
     * @memberOf _
     * @category Date
     * @example
     *
     * _.defer(function(stamp) {
     *   console.log(_.now() - stamp);
     * }, _.now());
     * // => logs the number of milliseconds it took for the deferred function to be invoked
     */
    var now = nativeNow || function() {
      return new Date().getTime();
    };

    /*------------------------------------------------------------------------*/

    /**
     * The opposite of `_.before`; this method creates a function that invokes
     * `func` once it is called `n` or more times.
     *
     * @static
     * @memberOf _
     * @category Function
     * @param {number} n The number of calls before `func` is invoked.
     * @param {Function} func The function to restrict.
     * @returns {Function} Returns the new restricted function.
     * @example
     *
     * var saves = ['profile', 'settings'];
     *
     * var done = _.after(saves.length, function() {
     *   console.log('done saving!');
     * });
     *
     * _.forEach(saves, function(type) {
     *   asyncSave({ 'type': type, 'complete': done });
     * });
     * // => logs 'done saving!' after the two async saves have completed
     */
    function after(n, func) {
      if (typeof func != 'function') {
        if (typeof n == 'function') {
          var temp = n;
          n = func;
          func = temp;
        } else {
          throw new TypeError(FUNC_ERROR_TEXT);
        }
      }
      n = nativeIsFinite(n = +n) ? n : 0;
      return function() {
        if (--n < 1) {
          return func.apply(this, arguments);
        }
      };
    }

    /**
     * Creates a function that accepts up to `n` arguments ignoring any
     * additional arguments.
     *
     * @static
     * @memberOf _
     * @category Function
     * @param {Function} func The function to cap arguments for.
     * @param {number} [n=func.length] The arity cap.
     * @param- {Object} [guard] Enables use as a callback for functions like `_.map`.
     * @returns {Function} Returns the new function.
     * @example
     *
     * _.map(['6', '8', '10'], _.ary(parseInt, 1));
     * // => [6, 8, 10]
     */
    function ary(func, n, guard) {
      if (guard && isIterateeCall(func, n, guard)) {
        n = undefined;
      }
      n = (func && n == null) ? func.length : nativeMax(+n || 0, 0);
      return createWrapper(func, ARY_FLAG, undefined, undefined, undefined, undefined, n);
    }

    /**
     * Creates a function that invokes `func`, with the `this` binding and arguments
     * of the created function, while it is called less than `n` times. Subsequent
     * calls to the created function return the result of the last `func` invocation.
     *
     * @static
     * @memberOf _
     * @category Function
     * @param {number} n The number of calls at which `func` is no longer invoked.
     * @param {Function} func The function to restrict.
     * @returns {Function} Returns the new restricted function.
     * @example
     *
     * jQuery('#add').on('click', _.before(5, addContactToList));
     * // => allows adding up to 4 contacts to the list
     */
    function before(n, func) {
      var result;
      if (typeof func != 'function') {
        if (typeof n == 'function') {
          var temp = n;
          n = func;
          func = temp;
        } else {
          throw new TypeError(FUNC_ERROR_TEXT);
        }
      }
      return function() {
        if (--n > 0) {
          result = func.apply(this, arguments);
        }
        if (n <= 1) {
          func = undefined;
        }
        return result;
      };
    }

    /**
     * Creates a function that invokes `func` with the `this` binding of `thisArg`
     * and prepends any additional `_.bind` arguments to those provided to the
     * bound function.
     *
     * The `_.bind.placeholder` value, which defaults to `_` in monolithic builds,
     * may be used as a placeholder for partially applied arguments.
     *
     * **Note:** Unlike native `Function#bind` this method does not set the "length"
     * property of bound functions.
     *
     * @static
     * @memberOf _
     * @category Function
     * @param {Function} func The function to bind.
     * @param {*} thisArg The `this` binding of `func`.
     * @param {...*} [partials] The arguments to be partially applied.
     * @returns {Function} Returns the new bound function.
     * @example
     *
     * var greet = function(greeting, punctuation) {
     *   return greeting + ' ' + this.user + punctuation;
     * };
     *
     * var object = { 'user': 'fred' };
     *
     * var bound = _.bind(greet, object, 'hi');
     * bound('!');
     * // => 'hi fred!'
     *
     * // using placeholders
     * var bound = _.bind(greet, object, _, '!');
     * bound('hi');
     * // => 'hi fred!'
     */
    var bind = restParam(function(func, thisArg, partials) {
      var bitmask = BIND_FLAG;
      if (partials.length) {
        var holders = replaceHolders(partials, bind.placeholder);
        bitmask |= PARTIAL_FLAG;
      }
      return createWrapper(func, bitmask, thisArg, partials, holders);
    });

    /**
     * Binds methods of an object to the object itself, overwriting the existing
     * method. Method names may be specified as individual arguments or as arrays
     * of method names. If no method names are provided all enumerable function
     * properties, own and inherited, of `object` are bound.
     *
     * **Note:** This method does not set the "length" property of bound functions.
     *
     * @static
     * @memberOf _
     * @category Function
     * @param {Object} object The object to bind and assign the bound methods to.
     * @param {...(string|string[])} [methodNames] The object method names to bind,
     *  specified as individual method names or arrays of method names.
     * @returns {Object} Returns `object`.
     * @example
     *
     * var view = {
     *   'label': 'docs',
     *   'onClick': function() {
     *     console.log('clicked ' + this.label);
     *   }
     * };
     *
     * _.bindAll(view);
     * jQuery('#docs').on('click', view.onClick);
     * // => logs 'clicked docs' when the element is clicked
     */
    var bindAll = restParam(function(object, methodNames) {
      methodNames = methodNames.length ? baseFlatten(methodNames) : functions(object);

      var index = -1,
          length = methodNames.length;

      while (++index < length) {
        var key = methodNames[index];
        object[key] = createWrapper(object[key], BIND_FLAG, object);
      }
      return object;
    });

    /**
     * Creates a function that invokes the method at `object[key]` and prepends
     * any additional `_.bindKey` arguments to those provided to the bound function.
     *
     * This method differs from `_.bind` by allowing bound functions to reference
     * methods that may be redefined or don't yet exist.
     * See [Peter Michaux's article](http://peter.michaux.ca/articles/lazy-function-definition-pattern)
     * for more details.
     *
     * The `_.bindKey.placeholder` value, which defaults to `_` in monolithic
     * builds, may be used as a placeholder for partially applied arguments.
     *
     * @static
     * @memberOf _
     * @category Function
     * @param {Object} object The object the method belongs to.
     * @param {string} key The key of the method.
     * @param {...*} [partials] The arguments to be partially applied.
     * @returns {Function} Returns the new bound function.
     * @example
     *
     * var object = {
     *   'user': 'fred',
     *   'greet': function(greeting, punctuation) {
     *     return greeting + ' ' + this.user + punctuation;
     *   }
     * };
     *
     * var bound = _.bindKey(object, 'greet', 'hi');
     * bound('!');
     * // => 'hi fred!'
     *
     * object.greet = function(greeting, punctuation) {
     *   return greeting + 'ya ' + this.user + punctuation;
     * };
     *
     * bound('!');
     * // => 'hiya fred!'
     *
     * // using placeholders
     * var bound = _.bindKey(object, 'greet', _, '!');
     * bound('hi');
     * // => 'hiya fred!'
     */
    var bindKey = restParam(function(object, key, partials) {
      var bitmask = BIND_FLAG | BIND_KEY_FLAG;
      if (partials.length) {
        var holders = replaceHolders(partials, bindKey.placeholder);
        bitmask |= PARTIAL_FLAG;
      }
      return createWrapper(key, bitmask, object, partials, holders);
    });

    /**
     * Creates a function that accepts one or more arguments of `func` that when
     * called either invokes `func` returning its result, if all `func` arguments
     * have been provided, or returns a function that accepts one or more of the
     * remaining `func` arguments, and so on. The arity of `func` may be specified
     * if `func.length` is not sufficient.
     *
     * The `_.curry.placeholder` value, which defaults to `_` in monolithic builds,
     * may be used as a placeholder for provided arguments.
     *
     * **Note:** This method does not set the "length" property of curried functions.
     *
     * @static
     * @memberOf _
     * @category Function
     * @param {Function} func The function to curry.
     * @param {number} [arity=func.length] The arity of `func`.
     * @param- {Object} [guard] Enables use as a callback for functions like `_.map`.
     * @returns {Function} Returns the new curried function.
     * @example
     *
     * var abc = function(a, b, c) {
     *   return [a, b, c];
     * };
     *
     * var curried = _.curry(abc);
     *
     * curried(1)(2)(3);
     * // => [1, 2, 3]
     *
     * curried(1, 2)(3);
     * // => [1, 2, 3]
     *
     * curried(1, 2, 3);
     * // => [1, 2, 3]
     *
     * // using placeholders
     * curried(1)(_, 3)(2);
     * // => [1, 2, 3]
     */
    var curry = createCurry(CURRY_FLAG);

    /**
     * This method is like `_.curry` except that arguments are applied to `func`
     * in the manner of `_.partialRight` instead of `_.partial`.
     *
     * The `_.curryRight.placeholder` value, which defaults to `_` in monolithic
     * builds, may be used as a placeholder for provided arguments.
     *
     * **Note:** This method does not set the "length" property of curried functions.
     *
     * @static
     * @memberOf _
     * @category Function
     * @param {Function} func The function to curry.
     * @param {number} [arity=func.length] The arity of `func`.
     * @param- {Object} [guard] Enables use as a callback for functions like `_.map`.
     * @returns {Function} Returns the new curried function.
     * @example
     *
     * var abc = function(a, b, c) {
     *   return [a, b, c];
     * };
     *
     * var curried = _.curryRight(abc);
     *
     * curried(3)(2)(1);
     * // => [1, 2, 3]
     *
     * curried(2, 3)(1);
     * // => [1, 2, 3]
     *
     * curried(1, 2, 3);
     * // => [1, 2, 3]
     *
     * // using placeholders
     * curried(3)(1, _)(2);
     * // => [1, 2, 3]
     */
    var curryRight = createCurry(CURRY_RIGHT_FLAG);

    /**
     * Creates a debounced function that delays invoking `func` until after `wait`
     * milliseconds have elapsed since the last time the debounced function was
     * invoked. The debounced function comes with a `cancel` method to cancel
     * delayed invocations. Provide an options object to indicate that `func`
     * should be invoked on the leading and/or trailing edge of the `wait` timeout.
     * Subsequent calls to the debounced function return the result of the last
     * `func` invocation.
     *
     * **Note:** If `leading` and `trailing` options are `true`, `func` is invoked
     * on the trailing edge of the timeout only if the the debounced function is
     * invoked more than once during the `wait` timeout.
     *
     * See [David Corbacho's article](http://drupalmotion.com/article/debounce-and-throttle-visual-explanation)
     * for details over the differences between `_.debounce` and `_.throttle`.
     *
     * @static
     * @memberOf _
     * @category Function
     * @param {Function} func The function to debounce.
     * @param {number} [wait=0] The number of milliseconds to delay.
     * @param {Object} [options] The options object.
     * @param {boolean} [options.leading=false] Specify invoking on the leading
     *  edge of the timeout.
     * @param {number} [options.maxWait] The maximum time `func` is allowed to be
     *  delayed before it is invoked.
     * @param {boolean} [options.trailing=true] Specify invoking on the trailing
     *  edge of the timeout.
     * @returns {Function} Returns the new debounced function.
     * @example
     *
     * // avoid costly calculations while the window size is in flux
     * jQuery(window).on('resize', _.debounce(calculateLayout, 150));
     *
     * // invoke `sendMail` when the click event is fired, debouncing subsequent calls
     * jQuery('#postbox').on('click', _.debounce(sendMail, 300, {
     *   'leading': true,
     *   'trailing': false
     * }));
     *
     * // ensure `batchLog` is invoked once after 1 second of debounced calls
     * var source = new EventSource('/stream');
     * jQuery(source).on('message', _.debounce(batchLog, 250, {
     *   'maxWait': 1000
     * }));
     *
     * // cancel a debounced call
     * var todoChanges = _.debounce(batchLog, 1000);
     * Object.observe(models.todo, todoChanges);
     *
     * Object.observe(models, function(changes) {
     *   if (_.find(changes, { 'user': 'todo', 'type': 'delete'})) {
     *     todoChanges.cancel();
     *   }
     * }, ['delete']);
     *
     * // ...at some point `models.todo` is changed
     * models.todo.completed = true;
     *
     * // ...before 1 second has passed `models.todo` is deleted
     * // which cancels the debounced `todoChanges` call
     * delete models.todo;
     */
    function debounce(func, wait, options) {
      var args,
          maxTimeoutId,
          result,
          stamp,
          thisArg,
          timeoutId,
          trailingCall,
          lastCalled = 0,
          maxWait = false,
          trailing = true;

      if (typeof func != 'function') {
        throw new TypeError(FUNC_ERROR_TEXT);
      }
      wait = wait < 0 ? 0 : (+wait || 0);
      if (options === true) {
        var leading = true;
        trailing = false;
      } else if (isObject(options)) {
        leading = !!options.leading;
        maxWait = 'maxWait' in options && nativeMax(+options.maxWait || 0, wait);
        trailing = 'trailing' in options ? !!options.trailing : trailing;
      }

      function cancel() {
        if (timeoutId) {
          clearTimeout(timeoutId);
        }
        if (maxTimeoutId) {
          clearTimeout(maxTimeoutId);
        }
        lastCalled = 0;
        maxTimeoutId = timeoutId = trailingCall = undefined;
      }

      function complete(isCalled, id) {
        if (id) {
          clearTimeout(id);
        }
        maxTimeoutId = timeoutId = trailingCall = undefined;
        if (isCalled) {
          lastCalled = now();
          result = func.apply(thisArg, args);
          if (!timeoutId && !maxTimeoutId) {
            args = thisArg = undefined;
          }
        }
      }

      function delayed() {
        var remaining = wait - (now() - stamp);
        if (remaining <= 0 || remaining > wait) {
          complete(trailingCall, maxTimeoutId);
        } else {
          timeoutId = setTimeout(delayed, remaining);
        }
      }

      function maxDelayed() {
        complete(trailing, timeoutId);
      }

      function debounced() {
        args = arguments;
        stamp = now();
        thisArg = this;
        trailingCall = trailing && (timeoutId || !leading);

        if (maxWait === false) {
          var leadingCall = leading && !timeoutId;
        } else {
          if (!maxTimeoutId && !leading) {
            lastCalled = stamp;
          }
          var remaining = maxWait - (stamp - lastCalled),
              isCalled = remaining <= 0 || remaining > maxWait;

          if (isCalled) {
            if (maxTimeoutId) {
              maxTimeoutId = clearTimeout(maxTimeoutId);
            }
            lastCalled = stamp;
            result = func.apply(thisArg, args);
          }
          else if (!maxTimeoutId) {
            maxTimeoutId = setTimeout(maxDelayed, remaining);
          }
        }
        if (isCalled && timeoutId) {
          timeoutId = clearTimeout(timeoutId);
        }
        else if (!timeoutId && wait !== maxWait) {
          timeoutId = setTimeout(delayed, wait);
        }
        if (leadingCall) {
          isCalled = true;
          result = func.apply(thisArg, args);
        }
        if (isCalled && !timeoutId && !maxTimeoutId) {
          args = thisArg = undefined;
        }
        return result;
      }
      debounced.cancel = cancel;
      return debounced;
    }

    /**
     * Defers invoking the `func` until the current call stack has cleared. Any
     * additional arguments are provided to `func` when it is invoked.
     *
     * @static
     * @memberOf _
     * @category Function
     * @param {Function} func The function to defer.
     * @param {...*} [args] The arguments to invoke the function with.
     * @returns {number} Returns the timer id.
     * @example
     *
     * _.defer(function(text) {
     *   console.log(text);
     * }, 'deferred');
     * // logs 'deferred' after one or more milliseconds
     */
    var defer = restParam(function(func, args) {
      return baseDelay(func, 1, args);
    });

    /**
     * Invokes `func` after `wait` milliseconds. Any additional arguments are
     * provided to `func` when it is invoked.
     *
     * @static
     * @memberOf _
     * @category Function
     * @param {Function} func The function to delay.
     * @param {number} wait The number of milliseconds to delay invocation.
     * @param {...*} [args] The arguments to invoke the function with.
     * @returns {number} Returns the timer id.
     * @example
     *
     * _.delay(function(text) {
     *   console.log(text);
     * }, 1000, 'later');
     * // => logs 'later' after one second
     */
    var delay = restParam(function(func, wait, args) {
      return baseDelay(func, wait, args);
    });

    /**
     * Creates a function that returns the result of invoking the provided
     * functions with the `this` binding of the created function, where each
     * successive invocation is supplied the return value of the previous.
     *
     * @static
     * @memberOf _
     * @category Function
     * @param {...Function} [funcs] Functions to invoke.
     * @returns {Function} Returns the new function.
     * @example
     *
     * function square(n) {
     *   return n * n;
     * }
     *
     * var addSquare = _.flow(_.add, square);
     * addSquare(1, 2);
     * // => 9
     */
    var flow = createFlow();

    /**
     * This method is like `_.flow` except that it creates a function that
     * invokes the provided functions from right to left.
     *
     * @static
     * @memberOf _
     * @alias backflow, compose
     * @category Function
     * @param {...Function} [funcs] Functions to invoke.
     * @returns {Function} Returns the new function.
     * @example
     *
     * function square(n) {
     *   return n * n;
     * }
     *
     * var addSquare = _.flowRight(square, _.add);
     * addSquare(1, 2);
     * // => 9
     */
    var flowRight = createFlow(true);

    /**
     * Creates a function that memoizes the result of `func`. If `resolver` is
     * provided it determines the cache key for storing the result based on the
     * arguments provided to the memoized function. By default, the first argument
     * provided to the memoized function is coerced to a string and used as the
     * cache key. The `func` is invoked with the `this` binding of the memoized
     * function.
     *
     * **Note:** The cache is exposed as the `cache` property on the memoized
     * function. Its creation may be customized by replacing the `_.memoize.Cache`
     * constructor with one whose instances implement the [`Map`](http://ecma-international.org/ecma-262/6.0/#sec-properties-of-the-map-prototype-object)
     * method interface of `get`, `has`, and `set`.
     *
     * @static
     * @memberOf _
     * @category Function
     * @param {Function} func The function to have its output memoized.
     * @param {Function} [resolver] The function to resolve the cache key.
     * @returns {Function} Returns the new memoizing function.
     * @example
     *
     * var upperCase = _.memoize(function(string) {
     *   return string.toUpperCase();
     * });
     *
     * upperCase('fred');
     * // => 'FRED'
     *
     * // modifying the result cache
     * upperCase.cache.set('fred', 'BARNEY');
     * upperCase('fred');
     * // => 'BARNEY'
     *
     * // replacing `_.memoize.Cache`
     * var object = { 'user': 'fred' };
     * var other = { 'user': 'barney' };
     * var identity = _.memoize(_.identity);
     *
     * identity(object);
     * // => { 'user': 'fred' }
     * identity(other);
     * // => { 'user': 'fred' }
     *
     * _.memoize.Cache = WeakMap;
     * var identity = _.memoize(_.identity);
     *
     * identity(object);
     * // => { 'user': 'fred' }
     * identity(other);
     * // => { 'user': 'barney' }
     */
    function memoize(func, resolver) {
      if (typeof func != 'function' || (resolver && typeof resolver != 'function')) {
        throw new TypeError(FUNC_ERROR_TEXT);
      }
      var memoized = function() {
        var args = arguments,
            key = resolver ? resolver.apply(this, args) : args[0],
            cache = memoized.cache;

        if (cache.has(key)) {
          return cache.get(key);
        }
        var result = func.apply(this, args);
        memoized.cache = cache.set(key, result);
        return result;
      };
      memoized.cache = new memoize.Cache;
      return memoized;
    }

    /**
     * Creates a function that runs each argument through a corresponding
     * transform function.
     *
     * @static
     * @memberOf _
     * @category Function
     * @param {Function} func The function to wrap.
     * @param {...(Function|Function[])} [transforms] The functions to transform
     * arguments, specified as individual functions or arrays of functions.
     * @returns {Function} Returns the new function.
     * @example
     *
     * function doubled(n) {
     *   return n * 2;
     * }
     *
     * function square(n) {
     *   return n * n;
     * }
     *
     * var modded = _.modArgs(function(x, y) {
     *   return [x, y];
     * }, square, doubled);
     *
     * modded(1, 2);
     * // => [1, 4]
     *
     * modded(5, 10);
     * // => [25, 20]
     */
    var modArgs = restParam(function(func, transforms) {
      transforms = baseFlatten(transforms);
      if (typeof func != 'function' || !arrayEvery(transforms, baseIsFunction)) {
        throw new TypeError(FUNC_ERROR_TEXT);
      }
      var length = transforms.length;
      return restParam(function(args) {
        var index = nativeMin(args.length, length);
        while (index--) {
          args[index] = transforms[index](args[index]);
        }
        return func.apply(this, args);
      });
    });

    /**
     * Creates a function that negates the result of the predicate `func`. The
     * `func` predicate is invoked with the `this` binding and arguments of the
     * created function.
     *
     * @static
     * @memberOf _
     * @category Function
     * @param {Function} predicate The predicate to negate.
     * @returns {Function} Returns the new function.
     * @example
     *
     * function isEven(n) {
     *   return n % 2 == 0;
     * }
     *
     * _.filter([1, 2, 3, 4, 5, 6], _.negate(isEven));
     * // => [1, 3, 5]
     */
    function negate(predicate) {
      if (typeof predicate != 'function') {
        throw new TypeError(FUNC_ERROR_TEXT);
      }
      return function() {
        return !predicate.apply(this, arguments);
      };
    }

    /**
     * Creates a function that is restricted to invoking `func` once. Repeat calls
     * to the function return the value of the first call. The `func` is invoked
     * with the `this` binding and arguments of the created function.
     *
     * @static
     * @memberOf _
     * @category Function
     * @param {Function} func The function to restrict.
     * @returns {Function} Returns the new restricted function.
     * @example
     *
     * var initialize = _.once(createApplication);
     * initialize();
     * initialize();
     * // `initialize` invokes `createApplication` once
     */
    function once(func) {
      return before(2, func);
    }

    /**
     * Creates a function that invokes `func` with `partial` arguments prepended
     * to those provided to the new function. This method is like `_.bind` except
     * it does **not** alter the `this` binding.
     *
     * The `_.partial.placeholder` value, which defaults to `_` in monolithic
     * builds, may be used as a placeholder for partially applied arguments.
     *
     * **Note:** This method does not set the "length" property of partially
     * applied functions.
     *
     * @static
     * @memberOf _
     * @category Function
     * @param {Function} func The function to partially apply arguments to.
     * @param {...*} [partials] The arguments to be partially applied.
     * @returns {Function} Returns the new partially applied function.
     * @example
     *
     * var greet = function(greeting, name) {
     *   return greeting + ' ' + name;
     * };
     *
     * var sayHelloTo = _.partial(greet, 'hello');
     * sayHelloTo('fred');
     * // => 'hello fred'
     *
     * // using placeholders
     * var greetFred = _.partial(greet, _, 'fred');
     * greetFred('hi');
     * // => 'hi fred'
     */
    var partial = createPartial(PARTIAL_FLAG);

    /**
     * This method is like `_.partial` except that partially applied arguments
     * are appended to those provided to the new function.
     *
     * The `_.partialRight.placeholder` value, which defaults to `_` in monolithic
     * builds, may be used as a placeholder for partially applied arguments.
     *
     * **Note:** This method does not set the "length" property of partially
     * applied functions.
     *
     * @static
     * @memberOf _
     * @category Function
     * @param {Function} func The function to partially apply arguments to.
     * @param {...*} [partials] The arguments to be partially applied.
     * @returns {Function} Returns the new partially applied function.
     * @example
     *
     * var greet = function(greeting, name) {
     *   return greeting + ' ' + name;
     * };
     *
     * var greetFred = _.partialRight(greet, 'fred');
     * greetFred('hi');
     * // => 'hi fred'
     *
     * // using placeholders
     * var sayHelloTo = _.partialRight(greet, 'hello', _);
     * sayHelloTo('fred');
     * // => 'hello fred'
     */
    var partialRight = createPartial(PARTIAL_RIGHT_FLAG);

    /**
     * Creates a function that invokes `func` with arguments arranged according
     * to the specified indexes where the argument value at the first index is
     * provided as the first argument, the argument value at the second index is
     * provided as the second argument, and so on.
     *
     * @static
     * @memberOf _
     * @category Function
     * @param {Function} func The function to rearrange arguments for.
     * @param {...(number|number[])} indexes The arranged argument indexes,
     *  specified as individual indexes or arrays of indexes.
     * @returns {Function} Returns the new function.
     * @example
     *
     * var rearged = _.rearg(function(a, b, c) {
     *   return [a, b, c];
     * }, 2, 0, 1);
     *
     * rearged('b', 'c', 'a')
     * // => ['a', 'b', 'c']
     *
     * var map = _.rearg(_.map, [1, 0]);
     * map(function(n) {
     *   return n * 3;
     * }, [1, 2, 3]);
     * // => [3, 6, 9]
     */
    var rearg = restParam(function(func, indexes) {
      return createWrapper(func, REARG_FLAG, undefined, undefined, undefined, baseFlatten(indexes));
    });

    /**
     * Creates a function that invokes `func` with the `this` binding of the
     * created function and arguments from `start` and beyond provided as an array.
     *
     * **Note:** This method is based on the [rest parameter](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/rest_parameters).
     *
     * @static
     * @memberOf _
     * @category Function
     * @param {Function} func The function to apply a rest parameter to.
     * @param {number} [start=func.length-1] The start position of the rest parameter.
     * @returns {Function} Returns the new function.
     * @example
     *
     * var say = _.restParam(function(what, names) {
     *   return what + ' ' + _.initial(names).join(', ') +
     *     (_.size(names) > 1 ? ', & ' : '') + _.last(names);
     * });
     *
     * say('hello', 'fred', 'barney', 'pebbles');
     * // => 'hello fred, barney, & pebbles'
     */
    function restParam(func, start) {
      if (typeof func != 'function') {
        throw new TypeError(FUNC_ERROR_TEXT);
      }
      start = nativeMax(start === undefined ? (func.length - 1) : (+start || 0), 0);
      return function() {
        var args = arguments,
            index = -1,
            length = nativeMax(args.length - start, 0),
            rest = Array(length);

        while (++index < length) {
          rest[index] = args[start + index];
        }
        switch (start) {
          case 0: return func.call(this, rest);
          case 1: return func.call(this, args[0], rest);
          case 2: return func.call(this, args[0], args[1], rest);
        }
        var otherArgs = Array(start + 1);
        index = -1;
        while (++index < start) {
          otherArgs[index] = args[index];
        }
        otherArgs[start] = rest;
        return func.apply(this, otherArgs);
      };
    }

    /**
     * Creates a function that invokes `func` with the `this` binding of the created
     * function and an array of arguments much like [`Function#apply`](https://es5.github.io/#x15.3.4.3).
     *
     * **Note:** This method is based on the [spread operator](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_operator).
     *
     * @static
     * @memberOf _
     * @category Function
     * @param {Function} func The function to spread arguments over.
     * @returns {Function} Returns the new function.
     * @example
     *
     * var say = _.spread(function(who, what) {
     *   return who + ' says ' + what;
     * });
     *
     * say(['fred', 'hello']);
     * // => 'fred says hello'
     *
     * // with a Promise
     * var numbers = Promise.all([
     *   Promise.resolve(40),
     *   Promise.resolve(36)
     * ]);
     *
     * numbers.then(_.spread(function(x, y) {
     *   return x + y;
     * }));
     * // => a Promise of 76
     */
    function spread(func) {
      if (typeof func != 'function') {
        throw new TypeError(FUNC_ERROR_TEXT);
      }
      return function(array) {
        return func.apply(this, array);
      };
    }

    /**
     * Creates a throttled function that only invokes `func` at most once per
     * every `wait` milliseconds. The throttled function comes with a `cancel`
     * method to cancel delayed invocations. Provide an options object to indicate
     * that `func` should be invoked on the leading and/or trailing edge of the
     * `wait` timeout. Subsequent calls to the throttled function return the
     * result of the last `func` call.
     *
     * **Note:** If `leading` and `trailing` options are `true`, `func` is invoked
     * on the trailing edge of the timeout only if the the throttled function is
     * invoked more than once during the `wait` timeout.
     *
     * See [David Corbacho's article](http://drupalmotion.com/article/debounce-and-throttle-visual-explanation)
     * for details over the differences between `_.throttle` and `_.debounce`.
     *
     * @static
     * @memberOf _
     * @category Function
     * @param {Function} func The function to throttle.
     * @param {number} [wait=0] The number of milliseconds to throttle invocations to.
     * @param {Object} [options] The options object.
     * @param {boolean} [options.leading=true] Specify invoking on the leading
     *  edge of the timeout.
     * @param {boolean} [options.trailing=true] Specify invoking on the trailing
     *  edge of the timeout.
     * @returns {Function} Returns the new throttled function.
     * @example
     *
     * // avoid excessively updating the position while scrolling
     * jQuery(window).on('scroll', _.throttle(updatePosition, 100));
     *
     * // invoke `renewToken` when the click event is fired, but not more than once every 5 minutes
     * jQuery('.interactive').on('click', _.throttle(renewToken, 300000, {
     *   'trailing': false
     * }));
     *
     * // cancel a trailing throttled call
     * jQuery(window).on('popstate', throttled.cancel);
     */
    function throttle(func, wait, options) {
      var leading = true,
          trailing = true;

      if (typeof func != 'function') {
        throw new TypeError(FUNC_ERROR_TEXT);
      }
      if (options === false) {
        leading = false;
      } else if (isObject(options)) {
        leading = 'leading' in options ? !!options.leading : leading;
        trailing = 'trailing' in options ? !!options.trailing : trailing;
      }
      return debounce(func, wait, { 'leading': leading, 'maxWait': +wait, 'trailing': trailing });
    }

    /**
     * Creates a function that provides `value` to the wrapper function as its
     * first argument. Any additional arguments provided to the function are
     * appended to those provided to the wrapper function. The wrapper is invoked
     * with the `this` binding of the created function.
     *
     * @static
     * @memberOf _
     * @category Function
     * @param {*} value The value to wrap.
     * @param {Function} wrapper The wrapper function.
     * @returns {Function} Returns the new function.
     * @example
     *
     * var p = _.wrap(_.escape, function(func, text) {
     *   return '<p>' + func(text) + '</p>';
     * });
     *
     * p('fred, barney, & pebbles');
     * // => '<p>fred, barney, &amp; pebbles</p>'
     */
    function wrap(value, wrapper) {
      wrapper = wrapper == null ? identity : wrapper;
      return createWrapper(wrapper, PARTIAL_FLAG, undefined, [value], []);
    }

    /*------------------------------------------------------------------------*/

    /**
     * Creates a clone of `value`. If `isDeep` is `true` nested objects are cloned,
     * otherwise they are assigned by reference. If `customizer` is provided it is
     * invoked to produce the cloned values. If `customizer` returns `undefined`
     * cloning is handled by the method instead. The `customizer` is bound to
     * `thisArg` and invoked with two argument; (value [, index|key, object]).
     *
     * **Note:** This method is loosely based on the
     * [structured clone algorithm](http://www.w3.org/TR/html5/infrastructure.html#internal-structured-cloning-algorithm).
     * The enumerable properties of `arguments` objects and objects created by
     * constructors other than `Object` are cloned to plain `Object` objects. An
     * empty object is returned for uncloneable values such as functions, DOM nodes,
     * Maps, Sets, and WeakMaps.
     *
     * @static
     * @memberOf _
     * @category Lang
     * @param {*} value The value to clone.
     * @param {boolean} [isDeep] Specify a deep clone.
     * @param {Function} [customizer] The function to customize cloning values.
     * @param {*} [thisArg] The `this` binding of `customizer`.
     * @returns {*} Returns the cloned value.
     * @example
     *
     * var users = [
     *   { 'user': 'barney' },
     *   { 'user': 'fred' }
     * ];
     *
     * var shallow = _.clone(users);
     * shallow[0] === users[0];
     * // => true
     *
     * var deep = _.clone(users, true);
     * deep[0] === users[0];
     * // => false
     *
     * // using a customizer callback
     * var el = _.clone(document.body, function(value) {
     *   if (_.isElement(value)) {
     *     return value.cloneNode(false);
     *   }
     * });
     *
     * el === document.body
     * // => false
     * el.nodeName
     * // => BODY
     * el.childNodes.length;
     * // => 0
     */
    function clone(value, isDeep, customizer, thisArg) {
      if (isDeep && typeof isDeep != 'boolean' && isIterateeCall(value, isDeep, customizer)) {
        isDeep = false;
      }
      else if (typeof isDeep == 'function') {
        thisArg = customizer;
        customizer = isDeep;
        isDeep = false;
      }
      return typeof customizer == 'function'
        ? baseClone(value, isDeep, bindCallback(customizer, thisArg, 1))
        : baseClone(value, isDeep);
    }

    /**
     * Creates a deep clone of `value`. If `customizer` is provided it is invoked
     * to produce the cloned values. If `customizer` returns `undefined` cloning
     * is handled by the method instead. The `customizer` is bound to `thisArg`
     * and invoked with two argument; (value [, index|key, object]).
     *
     * **Note:** This method is loosely based on the
     * [structured clone algorithm](http://www.w3.org/TR/html5/infrastructure.html#internal-structured-cloning-algorithm).
     * The enumerable properties of `arguments` objects and objects created by
     * constructors other than `Object` are cloned to plain `Object` objects. An
     * empty object is returned for uncloneable values such as functions, DOM nodes,
     * Maps, Sets, and WeakMaps.
     *
     * @static
     * @memberOf _
     * @category Lang
     * @param {*} value The value to deep clone.
     * @param {Function} [customizer] The function to customize cloning values.
     * @param {*} [thisArg] The `this` binding of `customizer`.
     * @returns {*} Returns the deep cloned value.
     * @example
     *
     * var users = [
     *   { 'user': 'barney' },
     *   { 'user': 'fred' }
     * ];
     *
     * var deep = _.cloneDeep(users);
     * deep[0] === users[0];
     * // => false
     *
     * // using a customizer callback
     * var el = _.cloneDeep(document.body, function(value) {
     *   if (_.isElement(value)) {
     *     return value.cloneNode(true);
     *   }
     * });
     *
     * el === document.body
     * // => false
     * el.nodeName
     * // => BODY
     * el.childNodes.length;
     * // => 20
     */
    function cloneDeep(value, customizer, thisArg) {
      return typeof customizer == 'function'
        ? baseClone(value, true, bindCallback(customizer, thisArg, 1))
        : baseClone(value, true);
    }

    /**
     * Checks if `value` is greater than `other`.
     *
     * @static
     * @memberOf _
     * @category Lang
     * @param {*} value The value to compare.
     * @param {*} other The other value to compare.
     * @returns {boolean} Returns `true` if `value` is greater than `other`, else `false`.
     * @example
     *
     * _.gt(3, 1);
     * // => true
     *
     * _.gt(3, 3);
     * // => false
     *
     * _.gt(1, 3);
     * // => false
     */
    function gt(value, other) {
      return value > other;
    }

    /**
     * Checks if `value` is greater than or equal to `other`.
     *
     * @static
     * @memberOf _
     * @category Lang
     * @param {*} value The value to compare.
     * @param {*} other The other value to compare.
     * @returns {boolean} Returns `true` if `value` is greater than or equal to `other`, else `false`.
     * @example
     *
     * _.gte(3, 1);
     * // => true
     *
     * _.gte(3, 3);
     * // => true
     *
     * _.gte(1, 3);
     * // => false
     */
    function gte(value, other) {
      return value >= other;
    }

    /**
     * Checks if `value` is classified as an `arguments` object.
     *
     * @static
     * @memberOf _
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is correctly classified, else `false`.
     * @example
     *
     * _.isArguments(function() { return arguments; }());
     * // => true
     *
     * _.isArguments([1, 2, 3]);
     * // => false
     */
    function isArguments(value) {
      return isObjectLike(value) && isArrayLike(value) &&
        hasOwnProperty.call(value, 'callee') && !propertyIsEnumerable.call(value, 'callee');
    }

    /**
     * Checks if `value` is classified as an `Array` object.
     *
     * @static
     * @memberOf _
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is correctly classified, else `false`.
     * @example
     *
     * _.isArray([1, 2, 3]);
     * // => true
     *
     * _.isArray(function() { return arguments; }());
     * // => false
     */
    var isArray = nativeIsArray || function(value) {
      return isObjectLike(value) && isLength(value.length) && objToString.call(value) == arrayTag;
    };

    /**
     * Checks if `value` is classified as a boolean primitive or object.
     *
     * @static
     * @memberOf _
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is correctly classified, else `false`.
     * @example
     *
     * _.isBoolean(false);
     * // => true
     *
     * _.isBoolean(null);
     * // => false
     */
    function isBoolean(value) {
      return value === true || value === false || (isObjectLike(value) && objToString.call(value) == boolTag);
    }

    /**
     * Checks if `value` is classified as a `Date` object.
     *
     * @static
     * @memberOf _
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is correctly classified, else `false`.
     * @example
     *
     * _.isDate(new Date);
     * // => true
     *
     * _.isDate('Mon April 23 2012');
     * // => false
     */
    function isDate(value) {
      return isObjectLike(value) && objToString.call(value) == dateTag;
    }

    /**
     * Checks if `value` is a DOM element.
     *
     * @static
     * @memberOf _
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is a DOM element, else `false`.
     * @example
     *
     * _.isElement(document.body);
     * // => true
     *
     * _.isElement('<body>');
     * // => false
     */
    function isElement(value) {
      return !!value && value.nodeType === 1 && isObjectLike(value) && !isPlainObject(value);
    }

    /**
     * Checks if `value` is empty. A value is considered empty unless it is an
     * `arguments` object, array, string, or jQuery-like collection with a length
     * greater than `0` or an object with own enumerable properties.
     *
     * @static
     * @memberOf _
     * @category Lang
     * @param {Array|Object|string} value The value to inspect.
     * @returns {boolean} Returns `true` if `value` is empty, else `false`.
     * @example
     *
     * _.isEmpty(null);
     * // => true
     *
     * _.isEmpty(true);
     * // => true
     *
     * _.isEmpty(1);
     * // => true
     *
     * _.isEmpty([1, 2, 3]);
     * // => false
     *
     * _.isEmpty({ 'a': 1 });
     * // => false
     */
    function isEmpty(value) {
      if (value == null) {
        return true;
      }
      if (isArrayLike(value) && (isArray(value) || isString(value) || isArguments(value) ||
          (isObjectLike(value) && isFunction(value.splice)))) {
        return !value.length;
      }
      return !keys(value).length;
    }

    /**
     * Performs a deep comparison between two values to determine if they are
     * equivalent. If `customizer` is provided it is invoked to compare values.
     * If `customizer` returns `undefined` comparisons are handled by the method
     * instead. The `customizer` is bound to `thisArg` and invoked with three
     * arguments: (value, other [, index|key]).
     *
     * **Note:** This method supports comparing arrays, booleans, `Date` objects,
     * numbers, `Object` objects, regexes, and strings. Objects are compared by
     * their own, not inherited, enumerable properties. Functions and DOM nodes
     * are **not** supported. Provide a customizer function to extend support
     * for comparing other values.
     *
     * @static
     * @memberOf _
     * @alias eq
     * @category Lang
     * @param {*} value The value to compare.
     * @param {*} other The other value to compare.
     * @param {Function} [customizer] The function to customize value comparisons.
     * @param {*} [thisArg] The `this` binding of `customizer`.
     * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
     * @example
     *
     * var object = { 'user': 'fred' };
     * var other = { 'user': 'fred' };
     *
     * object == other;
     * // => false
     *
     * _.isEqual(object, other);
     * // => true
     *
     * // using a customizer callback
     * var array = ['hello', 'goodbye'];
     * var other = ['hi', 'goodbye'];
     *
     * _.isEqual(array, other, function(value, other) {
     *   if (_.every([value, other], RegExp.prototype.test, /^h(?:i|ello)$/)) {
     *     return true;
     *   }
     * });
     * // => true
     */
    function isEqual(value, other, customizer, thisArg) {
      customizer = typeof customizer == 'function' ? bindCallback(customizer, thisArg, 3) : undefined;
      var result = customizer ? customizer(value, other) : undefined;
      return  result === undefined ? baseIsEqual(value, other, customizer) : !!result;
    }

    /**
     * Checks if `value` is an `Error`, `EvalError`, `RangeError`, `ReferenceError`,
     * `SyntaxError`, `TypeError`, or `URIError` object.
     *
     * @static
     * @memberOf _
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is an error object, else `false`.
     * @example
     *
     * _.isError(new Error);
     * // => true
     *
     * _.isError(Error);
     * // => false
     */
    function isError(value) {
      return isObjectLike(value) && typeof value.message == 'string' && objToString.call(value) == errorTag;
    }

    /**
     * Checks if `value` is a finite primitive number.
     *
     * **Note:** This method is based on [`Number.isFinite`](http://ecma-international.org/ecma-262/6.0/#sec-number.isfinite).
     *
     * @static
     * @memberOf _
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is a finite number, else `false`.
     * @example
     *
     * _.isFinite(10);
     * // => true
     *
     * _.isFinite('10');
     * // => false
     *
     * _.isFinite(true);
     * // => false
     *
     * _.isFinite(Object(10));
     * // => false
     *
     * _.isFinite(Infinity);
     * // => false
     */
    function isFinite(value) {
      return typeof value == 'number' && nativeIsFinite(value);
    }

    /**
     * Checks if `value` is classified as a `Function` object.
     *
     * @static
     * @memberOf _
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is correctly classified, else `false`.
     * @example
     *
     * _.isFunction(_);
     * // => true
     *
     * _.isFunction(/abc/);
     * // => false
     */
    function isFunction(value) {
      // The use of `Object#toString` avoids issues with the `typeof` operator
      // in older versions of Chrome and Safari which return 'function' for regexes
      // and Safari 8 equivalents which return 'object' for typed array constructors.
      return isObject(value) && objToString.call(value) == funcTag;
    }

    /**
     * Checks if `value` is the [language type](https://es5.github.io/#x8) of `Object`.
     * (e.g. arrays, functions, objects, regexes, `new Number(0)`, and `new String('')`)
     *
     * @static
     * @memberOf _
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is an object, else `false`.
     * @example
     *
     * _.isObject({});
     * // => true
     *
     * _.isObject([1, 2, 3]);
     * // => true
     *
     * _.isObject(1);
     * // => false
     */
    function isObject(value) {
      // Avoid a V8 JIT bug in Chrome 19-20.
      // See https://code.google.com/p/v8/issues/detail?id=2291 for more details.
      var type = typeof value;
      return !!value && (type == 'object' || type == 'function');
    }

    /**
     * Performs a deep comparison between `object` and `source` to determine if
     * `object` contains equivalent property values. If `customizer` is provided
     * it is invoked to compare values. If `customizer` returns `undefined`
     * comparisons are handled by the method instead. The `customizer` is bound
     * to `thisArg` and invoked with three arguments: (value, other, index|key).
     *
     * **Note:** This method supports comparing properties of arrays, booleans,
     * `Date` objects, numbers, `Object` objects, regexes, and strings. Functions
     * and DOM nodes are **not** supported. Provide a customizer function to extend
     * support for comparing other values.
     *
     * @static
     * @memberOf _
     * @category Lang
     * @param {Object} object The object to inspect.
     * @param {Object} source The object of property values to match.
     * @param {Function} [customizer] The function to customize value comparisons.
     * @param {*} [thisArg] The `this` binding of `customizer`.
     * @returns {boolean} Returns `true` if `object` is a match, else `false`.
     * @example
     *
     * var object = { 'user': 'fred', 'age': 40 };
     *
     * _.isMatch(object, { 'age': 40 });
     * // => true
     *
     * _.isMatch(object, { 'age': 36 });
     * // => false
     *
     * // using a customizer callback
     * var object = { 'greeting': 'hello' };
     * var source = { 'greeting': 'hi' };
     *
     * _.isMatch(object, source, function(value, other) {
     *   return _.every([value, other], RegExp.prototype.test, /^h(?:i|ello)$/) || undefined;
     * });
     * // => true
     */
    function isMatch(object, source, customizer, thisArg) {
      customizer = typeof customizer == 'function' ? bindCallback(customizer, thisArg, 3) : undefined;
      return baseIsMatch(object, getMatchData(source), customizer);
    }

    /**
     * Checks if `value` is `NaN`.
     *
     * **Note:** This method is not the same as [`isNaN`](https://es5.github.io/#x15.1.2.4)
     * which returns `true` for `undefined` and other non-numeric values.
     *
     * @static
     * @memberOf _
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is `NaN`, else `false`.
     * @example
     *
     * _.isNaN(NaN);
     * // => true
     *
     * _.isNaN(new Number(NaN));
     * // => true
     *
     * isNaN(undefined);
     * // => true
     *
     * _.isNaN(undefined);
     * // => false
     */
    function isNaN(value) {
      // An `NaN` primitive is the only value that is not equal to itself.
      // Perform the `toStringTag` check first to avoid errors with some host objects in IE.
      return isNumber(value) && value != +value;
    }

    /**
     * Checks if `value` is a native function.
     *
     * @static
     * @memberOf _
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is a native function, else `false`.
     * @example
     *
     * _.isNative(Array.prototype.push);
     * // => true
     *
     * _.isNative(_);
     * // => false
     */
    function isNative(value) {
      if (value == null) {
        return false;
      }
      if (isFunction(value)) {
        return reIsNative.test(fnToString.call(value));
      }
      return isObjectLike(value) && reIsHostCtor.test(value);
    }

    /**
     * Checks if `value` is `null`.
     *
     * @static
     * @memberOf _
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is `null`, else `false`.
     * @example
     *
     * _.isNull(null);
     * // => true
     *
     * _.isNull(void 0);
     * // => false
     */
    function isNull(value) {
      return value === null;
    }

    /**
     * Checks if `value` is classified as a `Number` primitive or object.
     *
     * **Note:** To exclude `Infinity`, `-Infinity`, and `NaN`, which are classified
     * as numbers, use the `_.isFinite` method.
     *
     * @static
     * @memberOf _
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is correctly classified, else `false`.
     * @example
     *
     * _.isNumber(8.4);
     * // => true
     *
     * _.isNumber(NaN);
     * // => true
     *
     * _.isNumber('8.4');
     * // => false
     */
    function isNumber(value) {
      return typeof value == 'number' || (isObjectLike(value) && objToString.call(value) == numberTag);
    }

    /**
     * Checks if `value` is a plain object, that is, an object created by the
     * `Object` constructor or one with a `[[Prototype]]` of `null`.
     *
     * **Note:** This method assumes objects created by the `Object` constructor
     * have no inherited enumerable properties.
     *
     * @static
     * @memberOf _
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is a plain object, else `false`.
     * @example
     *
     * function Foo() {
     *   this.a = 1;
     * }
     *
     * _.isPlainObject(new Foo);
     * // => false
     *
     * _.isPlainObject([1, 2, 3]);
     * // => false
     *
     * _.isPlainObject({ 'x': 0, 'y': 0 });
     * // => true
     *
     * _.isPlainObject(Object.create(null));
     * // => true
     */
    function isPlainObject(value) {
      var Ctor;

      // Exit early for non `Object` objects.
      if (!(isObjectLike(value) && objToString.call(value) == objectTag && !isArguments(value)) ||
          (!hasOwnProperty.call(value, 'constructor') && (Ctor = value.constructor, typeof Ctor == 'function' && !(Ctor instanceof Ctor)))) {
        return false;
      }
      // IE < 9 iterates inherited properties before own properties. If the first
      // iterated property is an object's own property then there are no inherited
      // enumerable properties.
      var result;
      // In most environments an object's own properties are iterated before
      // its inherited properties. If the last iterated property is an object's
      // own property then there are no inherited enumerable properties.
      baseForIn(value, function(subValue, key) {
        result = key;
      });
      return result === undefined || hasOwnProperty.call(value, result);
    }

    /**
     * Checks if `value` is classified as a `RegExp` object.
     *
     * @static
     * @memberOf _
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is correctly classified, else `false`.
     * @example
     *
     * _.isRegExp(/abc/);
     * // => true
     *
     * _.isRegExp('/abc/');
     * // => false
     */
    function isRegExp(value) {
      return isObject(value) && objToString.call(value) == regexpTag;
    }

    /**
     * Checks if `value` is classified as a `String` primitive or object.
     *
     * @static
     * @memberOf _
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is correctly classified, else `false`.
     * @example
     *
     * _.isString('abc');
     * // => true
     *
     * _.isString(1);
     * // => false
     */
    function isString(value) {
      return typeof value == 'string' || (isObjectLike(value) && objToString.call(value) == stringTag);
    }

    /**
     * Checks if `value` is classified as a typed array.
     *
     * @static
     * @memberOf _
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is correctly classified, else `false`.
     * @example
     *
     * _.isTypedArray(new Uint8Array);
     * // => true
     *
     * _.isTypedArray([]);
     * // => false
     */
    function isTypedArray(value) {
      return isObjectLike(value) && isLength(value.length) && !!typedArrayTags[objToString.call(value)];
    }

    /**
     * Checks if `value` is `undefined`.
     *
     * @static
     * @memberOf _
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is `undefined`, else `false`.
     * @example
     *
     * _.isUndefined(void 0);
     * // => true
     *
     * _.isUndefined(null);
     * // => false
     */
    function isUndefined(value) {
      return value === undefined;
    }

    /**
     * Checks if `value` is less than `other`.
     *
     * @static
     * @memberOf _
     * @category Lang
     * @param {*} value The value to compare.
     * @param {*} other The other value to compare.
     * @returns {boolean} Returns `true` if `value` is less than `other`, else `false`.
     * @example
     *
     * _.lt(1, 3);
     * // => true
     *
     * _.lt(3, 3);
     * // => false
     *
     * _.lt(3, 1);
     * // => false
     */
    function lt(value, other) {
      return value < other;
    }

    /**
     * Checks if `value` is less than or equal to `other`.
     *
     * @static
     * @memberOf _
     * @category Lang
     * @param {*} value The value to compare.
     * @param {*} other The other value to compare.
     * @returns {boolean} Returns `true` if `value` is less than or equal to `other`, else `false`.
     * @example
     *
     * _.lte(1, 3);
     * // => true
     *
     * _.lte(3, 3);
     * // => true
     *
     * _.lte(3, 1);
     * // => false
     */
    function lte(value, other) {
      return value <= other;
    }

    /**
     * Converts `value` to an array.
     *
     * @static
     * @memberOf _
     * @category Lang
     * @param {*} value The value to convert.
     * @returns {Array} Returns the converted array.
     * @example
     *
     * (function() {
     *   return _.toArray(arguments).slice(1);
     * }(1, 2, 3));
     * // => [2, 3]
     */
    function toArray(value) {
      var length = value ? getLength(value) : 0;
      if (!isLength(length)) {
        return values(value);
      }
      if (!length) {
        return [];
      }
      return arrayCopy(value);
    }

    /**
     * Converts `value` to a plain object flattening inherited enumerable
     * properties of `value` to own properties of the plain object.
     *
     * @static
     * @memberOf _
     * @category Lang
     * @param {*} value The value to convert.
     * @returns {Object} Returns the converted plain object.
     * @example
     *
     * function Foo() {
     *   this.b = 2;
     * }
     *
     * Foo.prototype.c = 3;
     *
     * _.assign({ 'a': 1 }, new Foo);
     * // => { 'a': 1, 'b': 2 }
     *
     * _.assign({ 'a': 1 }, _.toPlainObject(new Foo));
     * // => { 'a': 1, 'b': 2, 'c': 3 }
     */
    function toPlainObject(value) {
      return baseCopy(value, keysIn(value));
    }

    /*------------------------------------------------------------------------*/

    /**
     * Recursively merges own enumerable properties of the source object(s), that
     * don't resolve to `undefined` into the destination object. Subsequent sources
     * overwrite property assignments of previous sources. If `customizer` is
     * provided it is invoked to produce the merged values of the destination and
     * source properties. If `customizer` returns `undefined` merging is handled
     * by the method instead. The `customizer` is bound to `thisArg` and invoked
     * with five arguments: (objectValue, sourceValue, key, object, source).
     *
     * @static
     * @memberOf _
     * @category Object
     * @param {Object} object The destination object.
     * @param {...Object} [sources] The source objects.
     * @param {Function} [customizer] The function to customize assigned values.
     * @param {*} [thisArg] The `this` binding of `customizer`.
     * @returns {Object} Returns `object`.
     * @example
     *
     * var users = {
     *   'data': [{ 'user': 'barney' }, { 'user': 'fred' }]
     * };
     *
     * var ages = {
     *   'data': [{ 'age': 36 }, { 'age': 40 }]
     * };
     *
     * _.merge(users, ages);
     * // => { 'data': [{ 'user': 'barney', 'age': 36 }, { 'user': 'fred', 'age': 40 }] }
     *
     * // using a customizer callback
     * var object = {
     *   'fruits': ['apple'],
     *   'vegetables': ['beet']
     * };
     *
     * var other = {
     *   'fruits': ['banana'],
     *   'vegetables': ['carrot']
     * };
     *
     * _.merge(object, other, function(a, b) {
     *   if (_.isArray(a)) {
     *     return a.concat(b);
     *   }
     * });
     * // => { 'fruits': ['apple', 'banana'], 'vegetables': ['beet', 'carrot'] }
     */
    var merge = createAssigner(baseMerge);

    /**
     * Assigns own enumerable properties of source object(s) to the destination
     * object. Subsequent sources overwrite property assignments of previous sources.
     * If `customizer` is provided it is invoked to produce the assigned values.
     * The `customizer` is bound to `thisArg` and invoked with five arguments:
     * (objectValue, sourceValue, key, object, source).
     *
     * **Note:** This method mutates `object` and is based on
     * [`Object.assign`](http://ecma-international.org/ecma-262/6.0/#sec-object.assign).
     *
     * @static
     * @memberOf _
     * @alias extend
     * @category Object
     * @param {Object} object The destination object.
     * @param {...Object} [sources] The source objects.
     * @param {Function} [customizer] The function to customize assigned values.
     * @param {*} [thisArg] The `this` binding of `customizer`.
     * @returns {Object} Returns `object`.
     * @example
     *
     * _.assign({ 'user': 'barney' }, { 'age': 40 }, { 'user': 'fred' });
     * // => { 'user': 'fred', 'age': 40 }
     *
     * // using a customizer callback
     * var defaults = _.partialRight(_.assign, function(value, other) {
     *   return _.isUndefined(value) ? other : value;
     * });
     *
     * defaults({ 'user': 'barney' }, { 'age': 36 }, { 'user': 'fred' });
     * // => { 'user': 'barney', 'age': 36 }
     */
    var assign = createAssigner(function(object, source, customizer) {
      return customizer
        ? assignWith(object, source, customizer)
        : baseAssign(object, source);
    });

    /**
     * Creates an object that inherits from the given `prototype` object. If a
     * `properties` object is provided its own enumerable properties are assigned
     * to the created object.
     *
     * @static
     * @memberOf _
     * @category Object
     * @param {Object} prototype The object to inherit from.
     * @param {Object} [properties] The properties to assign to the object.
     * @param- {Object} [guard] Enables use as a callback for functions like `_.map`.
     * @returns {Object} Returns the new object.
     * @example
     *
     * function Shape() {
     *   this.x = 0;
     *   this.y = 0;
     * }
     *
     * function Circle() {
     *   Shape.call(this);
     * }
     *
     * Circle.prototype = _.create(Shape.prototype, {
     *   'constructor': Circle
     * });
     *
     * var circle = new Circle;
     * circle instanceof Circle;
     * // => true
     *
     * circle instanceof Shape;
     * // => true
     */
    function create(prototype, properties, guard) {
      var result = baseCreate(prototype);
      if (guard && isIterateeCall(prototype, properties, guard)) {
        properties = undefined;
      }
      return properties ? baseAssign(result, properties) : result;
    }

    /**
     * Assigns own enumerable properties of source object(s) to the destination
     * object for all destination properties that resolve to `undefined`. Once a
     * property is set, additional values of the same property are ignored.
     *
     * **Note:** This method mutates `object`.
     *
     * @static
     * @memberOf _
     * @category Object
     * @param {Object} object The destination object.
     * @param {...Object} [sources] The source objects.
     * @returns {Object} Returns `object`.
     * @example
     *
     * _.defaults({ 'user': 'barney' }, { 'age': 36 }, { 'user': 'fred' });
     * // => { 'user': 'barney', 'age': 36 }
     */
    var defaults = createDefaults(assign, assignDefaults);

    /**
     * This method is like `_.defaults` except that it recursively assigns
     * default properties.
     *
     * **Note:** This method mutates `object`.
     *
     * @static
     * @memberOf _
     * @category Object
     * @param {Object} object The destination object.
     * @param {...Object} [sources] The source objects.
     * @returns {Object} Returns `object`.
     * @example
     *
     * _.defaultsDeep({ 'user': { 'name': 'barney' } }, { 'user': { 'name': 'fred', 'age': 36 } });
     * // => { 'user': { 'name': 'barney', 'age': 36 } }
     *
     */
    var defaultsDeep = createDefaults(merge, mergeDefaults);

    /**
     * This method is like `_.find` except that it returns the key of the first
     * element `predicate` returns truthy for instead of the element itself.
     *
     * If a property name is provided for `predicate` the created `_.property`
     * style callback returns the property value of the given element.
     *
     * If a value is also provided for `thisArg` the created `_.matchesProperty`
     * style callback returns `true` for elements that have a matching property
     * value, else `false`.
     *
     * If an object is provided for `predicate` the created `_.matches` style
     * callback returns `true` for elements that have the properties of the given
     * object, else `false`.
     *
     * @static
     * @memberOf _
     * @category Object
     * @param {Object} object The object to search.
     * @param {Function|Object|string} [predicate=_.identity] The function invoked
     *  per iteration.
     * @param {*} [thisArg] The `this` binding of `predicate`.
     * @returns {string|undefined} Returns the key of the matched element, else `undefined`.
     * @example
     *
     * var users = {
     *   'barney':  { 'age': 36, 'active': true },
     *   'fred':    { 'age': 40, 'active': false },
     *   'pebbles': { 'age': 1,  'active': true }
     * };
     *
     * _.findKey(users, function(chr) {
     *   return chr.age < 40;
     * });
     * // => 'barney' (iteration order is not guaranteed)
     *
     * // using the `_.matches` callback shorthand
     * _.findKey(users, { 'age': 1, 'active': true });
     * // => 'pebbles'
     *
     * // using the `_.matchesProperty` callback shorthand
     * _.findKey(users, 'active', false);
     * // => 'fred'
     *
     * // using the `_.property` callback shorthand
     * _.findKey(users, 'active');
     * // => 'barney'
     */
    var findKey = createFindKey(baseForOwn);

    /**
     * This method is like `_.findKey` except that it iterates over elements of
     * a collection in the opposite order.
     *
     * If a property name is provided for `predicate` the created `_.property`
     * style callback returns the property value of the given element.
     *
     * If a value is also provided for `thisArg` the created `_.matchesProperty`
     * style callback returns `true` for elements that have a matching property
     * value, else `false`.
     *
     * If an object is provided for `predicate` the created `_.matches` style
     * callback returns `true` for elements that have the properties of the given
     * object, else `false`.
     *
     * @static
     * @memberOf _
     * @category Object
     * @param {Object} object The object to search.
     * @param {Function|Object|string} [predicate=_.identity] The function invoked
     *  per iteration.
     * @param {*} [thisArg] The `this` binding of `predicate`.
     * @returns {string|undefined} Returns the key of the matched element, else `undefined`.
     * @example
     *
     * var users = {
     *   'barney':  { 'age': 36, 'active': true },
     *   'fred':    { 'age': 40, 'active': false },
     *   'pebbles': { 'age': 1,  'active': true }
     * };
     *
     * _.findLastKey(users, function(chr) {
     *   return chr.age < 40;
     * });
     * // => returns `pebbles` assuming `_.findKey` returns `barney`
     *
     * // using the `_.matches` callback shorthand
     * _.findLastKey(users, { 'age': 36, 'active': true });
     * // => 'barney'
     *
     * // using the `_.matchesProperty` callback shorthand
     * _.findLastKey(users, 'active', false);
     * // => 'fred'
     *
     * // using the `_.property` callback shorthand
     * _.findLastKey(users, 'active');
     * // => 'pebbles'
     */
    var findLastKey = createFindKey(baseForOwnRight);

    /**
     * Iterates over own and inherited enumerable properties of an object invoking
     * `iteratee` for each property. The `iteratee` is bound to `thisArg` and invoked
     * with three arguments: (value, key, object). Iteratee functions may exit
     * iteration early by explicitly returning `false`.
     *
     * @static
     * @memberOf _
     * @category Object
     * @param {Object} object The object to iterate over.
     * @param {Function} [iteratee=_.identity] The function invoked per iteration.
     * @param {*} [thisArg] The `this` binding of `iteratee`.
     * @returns {Object} Returns `object`.
     * @example
     *
     * function Foo() {
     *   this.a = 1;
     *   this.b = 2;
     * }
     *
     * Foo.prototype.c = 3;
     *
     * _.forIn(new Foo, function(value, key) {
     *   console.log(key);
     * });
     * // => logs 'a', 'b', and 'c' (iteration order is not guaranteed)
     */
    var forIn = createForIn(baseFor);

    /**
     * This method is like `_.forIn` except that it iterates over properties of
     * `object` in the opposite order.
     *
     * @static
     * @memberOf _
     * @category Object
     * @param {Object} object The object to iterate over.
     * @param {Function} [iteratee=_.identity] The function invoked per iteration.
     * @param {*} [thisArg] The `this` binding of `iteratee`.
     * @returns {Object} Returns `object`.
     * @example
     *
     * function Foo() {
     *   this.a = 1;
     *   this.b = 2;
     * }
     *
     * Foo.prototype.c = 3;
     *
     * _.forInRight(new Foo, function(value, key) {
     *   console.log(key);
     * });
     * // => logs 'c', 'b', and 'a' assuming `_.forIn ` logs 'a', 'b', and 'c'
     */
    var forInRight = createForIn(baseForRight);

    /**
     * Iterates over own enumerable properties of an object invoking `iteratee`
     * for each property. The `iteratee` is bound to `thisArg` and invoked with
     * three arguments: (value, key, object). Iteratee functions may exit iteration
     * early by explicitly returning `false`.
     *
     * @static
     * @memberOf _
     * @category Object
     * @param {Object} object The object to iterate over.
     * @param {Function} [iteratee=_.identity] The function invoked per iteration.
     * @param {*} [thisArg] The `this` binding of `iteratee`.
     * @returns {Object} Returns `object`.
     * @example
     *
     * function Foo() {
     *   this.a = 1;
     *   this.b = 2;
     * }
     *
     * Foo.prototype.c = 3;
     *
     * _.forOwn(new Foo, function(value, key) {
     *   console.log(key);
     * });
     * // => logs 'a' and 'b' (iteration order is not guaranteed)
     */
    var forOwn = createForOwn(baseForOwn);

    /**
     * This method is like `_.forOwn` except that it iterates over properties of
     * `object` in the opposite order.
     *
     * @static
     * @memberOf _
     * @category Object
     * @param {Object} object The object to iterate over.
     * @param {Function} [iteratee=_.identity] The function invoked per iteration.
     * @param {*} [thisArg] The `this` binding of `iteratee`.
     * @returns {Object} Returns `object`.
     * @example
     *
     * function Foo() {
     *   this.a = 1;
     *   this.b = 2;
     * }
     *
     * Foo.prototype.c = 3;
     *
     * _.forOwnRight(new Foo, function(value, key) {
     *   console.log(key);
     * });
     * // => logs 'b' and 'a' assuming `_.forOwn` logs 'a' and 'b'
     */
    var forOwnRight = createForOwn(baseForOwnRight);

    /**
     * Creates an array of function property names from all enumerable properties,
     * own and inherited, of `object`.
     *
     * @static
     * @memberOf _
     * @alias methods
     * @category Object
     * @param {Object} object The object to inspect.
     * @returns {Array} Returns the new array of property names.
     * @example
     *
     * _.functions(_);
     * // => ['after', 'ary', 'assign', ...]
     */
    function functions(object) {
      return baseFunctions(object, keysIn(object));
    }

    /**
     * Gets the property value at `path` of `object`. If the resolved value is
     * `undefined` the `defaultValue` is used in its place.
     *
     * @static
     * @memberOf _
     * @category Object
     * @param {Object} object The object to query.
     * @param {Array|string} path The path of the property to get.
     * @param {*} [defaultValue] The value returned if the resolved value is `undefined`.
     * @returns {*} Returns the resolved value.
     * @example
     *
     * var object = { 'a': [{ 'b': { 'c': 3 } }] };
     *
     * _.get(object, 'a[0].b.c');
     * // => 3
     *
     * _.get(object, ['a', '0', 'b', 'c']);
     * // => 3
     *
     * _.get(object, 'a.b.c', 'default');
     * // => 'default'
     */
    function get(object, path, defaultValue) {
      var result = object == null ? undefined : baseGet(object, toPath(path), path + '');
      return result === undefined ? defaultValue : result;
    }

    /**
     * Checks if `path` is a direct property.
     *
     * @static
     * @memberOf _
     * @category Object
     * @param {Object} object The object to query.
     * @param {Array|string} path The path to check.
     * @returns {boolean} Returns `true` if `path` is a direct property, else `false`.
     * @example
     *
     * var object = { 'a': { 'b': { 'c': 3 } } };
     *
     * _.has(object, 'a');
     * // => true
     *
     * _.has(object, 'a.b.c');
     * // => true
     *
     * _.has(object, ['a', 'b', 'c']);
     * // => true
     */
    function has(object, path) {
      if (object == null) {
        return false;
      }
      var result = hasOwnProperty.call(object, path);
      if (!result && !isKey(path)) {
        path = toPath(path);
        object = path.length == 1 ? object : baseGet(object, baseSlice(path, 0, -1));
        if (object == null) {
          return false;
        }
        path = last(path);
        result = hasOwnProperty.call(object, path);
      }
      return result || (isLength(object.length) && isIndex(path, object.length) &&
        (isArray(object) || isArguments(object)));
    }

    /**
     * Creates an object composed of the inverted keys and values of `object`.
     * If `object` contains duplicate values, subsequent values overwrite property
     * assignments of previous values unless `multiValue` is `true`.
     *
     * @static
     * @memberOf _
     * @category Object
     * @param {Object} object The object to invert.
     * @param {boolean} [multiValue] Allow multiple values per key.
     * @param- {Object} [guard] Enables use as a callback for functions like `_.map`.
     * @returns {Object} Returns the new inverted object.
     * @example
     *
     * var object = { 'a': 1, 'b': 2, 'c': 1 };
     *
     * _.invert(object);
     * // => { '1': 'c', '2': 'b' }
     *
     * // with `multiValue`
     * _.invert(object, true);
     * // => { '1': ['a', 'c'], '2': ['b'] }
     */
    function invert(object, multiValue, guard) {
      if (guard && isIterateeCall(object, multiValue, guard)) {
        multiValue = undefined;
      }
      var index = -1,
          props = keys(object),
          length = props.length,
          result = {};

      while (++index < length) {
        var key = props[index],
            value = object[key];

        if (multiValue) {
          if (hasOwnProperty.call(result, value)) {
            result[value].push(key);
          } else {
            result[value] = [key];
          }
        }
        else {
          result[value] = key;
        }
      }
      return result;
    }

    /**
     * Creates an array of the own enumerable property names of `object`.
     *
     * **Note:** Non-object values are coerced to objects. See the
     * [ES spec](http://ecma-international.org/ecma-262/6.0/#sec-object.keys)
     * for more details.
     *
     * @static
     * @memberOf _
     * @category Object
     * @param {Object} object The object to query.
     * @returns {Array} Returns the array of property names.
     * @example
     *
     * function Foo() {
     *   this.a = 1;
     *   this.b = 2;
     * }
     *
     * Foo.prototype.c = 3;
     *
     * _.keys(new Foo);
     * // => ['a', 'b'] (iteration order is not guaranteed)
     *
     * _.keys('hi');
     * // => ['0', '1']
     */
    var keys = !nativeKeys ? shimKeys : function(object) {
      var Ctor = object == null ? undefined : object.constructor;
      if ((typeof Ctor == 'function' && Ctor.prototype === object) ||
          (typeof object != 'function' && isArrayLike(object))) {
        return shimKeys(object);
      }
      return isObject(object) ? nativeKeys(object) : [];
    };

    /**
     * Creates an array of the own and inherited enumerable property names of `object`.
     *
     * **Note:** Non-object values are coerced to objects.
     *
     * @static
     * @memberOf _
     * @category Object
     * @param {Object} object The object to query.
     * @returns {Array} Returns the array of property names.
     * @example
     *
     * function Foo() {
     *   this.a = 1;
     *   this.b = 2;
     * }
     *
     * Foo.prototype.c = 3;
     *
     * _.keysIn(new Foo);
     * // => ['a', 'b', 'c'] (iteration order is not guaranteed)
     */
    function keysIn(object) {
      if (object == null) {
        return [];
      }
      if (!isObject(object)) {
        object = Object(object);
      }
      var length = object.length;
      length = (length && isLength(length) &&
        (isArray(object) || isArguments(object)) && length) || 0;

      var Ctor = object.constructor,
          index = -1,
          isProto = typeof Ctor == 'function' && Ctor.prototype === object,
          result = Array(length),
          skipIndexes = length > 0;

      while (++index < length) {
        result[index] = (index + '');
      }
      for (var key in object) {
        if (!(skipIndexes && isIndex(key, length)) &&
            !(key == 'constructor' && (isProto || !hasOwnProperty.call(object, key)))) {
          result.push(key);
        }
      }
      return result;
    }

    /**
     * The opposite of `_.mapValues`; this method creates an object with the
     * same values as `object` and keys generated by running each own enumerable
     * property of `object` through `iteratee`.
     *
     * @static
     * @memberOf _
     * @category Object
     * @param {Object} object The object to iterate over.
     * @param {Function|Object|string} [iteratee=_.identity] The function invoked
     *  per iteration.
     * @param {*} [thisArg] The `this` binding of `iteratee`.
     * @returns {Object} Returns the new mapped object.
     * @example
     *
     * _.mapKeys({ 'a': 1, 'b': 2 }, function(value, key) {
     *   return key + value;
     * });
     * // => { 'a1': 1, 'b2': 2 }
     */
    var mapKeys = createObjectMapper(true);

    /**
     * Creates an object with the same keys as `object` and values generated by
     * running each own enumerable property of `object` through `iteratee`. The
     * iteratee function is bound to `thisArg` and invoked with three arguments:
     * (value, key, object).
     *
     * If a property name is provided for `iteratee` the created `_.property`
     * style callback returns the property value of the given element.
     *
     * If a value is also provided for `thisArg` the created `_.matchesProperty`
     * style callback returns `true` for elements that have a matching property
     * value, else `false`.
     *
     * If an object is provided for `iteratee` the created `_.matches` style
     * callback returns `true` for elements that have the properties of the given
     * object, else `false`.
     *
     * @static
     * @memberOf _
     * @category Object
     * @param {Object} object The object to iterate over.
     * @param {Function|Object|string} [iteratee=_.identity] The function invoked
     *  per iteration.
     * @param {*} [thisArg] The `this` binding of `iteratee`.
     * @returns {Object} Returns the new mapped object.
     * @example
     *
     * _.mapValues({ 'a': 1, 'b': 2 }, function(n) {
     *   return n * 3;
     * });
     * // => { 'a': 3, 'b': 6 }
     *
     * var users = {
     *   'fred':    { 'user': 'fred',    'age': 40 },
     *   'pebbles': { 'user': 'pebbles', 'age': 1 }
     * };
     *
     * // using the `_.property` callback shorthand
     * _.mapValues(users, 'age');
     * // => { 'fred': 40, 'pebbles': 1 } (iteration order is not guaranteed)
     */
    var mapValues = createObjectMapper();

    /**
     * The opposite of `_.pick`; this method creates an object composed of the
     * own and inherited enumerable properties of `object` that are not omitted.
     *
     * @static
     * @memberOf _
     * @category Object
     * @param {Object} object The source object.
     * @param {Function|...(string|string[])} [predicate] The function invoked per
     *  iteration or property names to omit, specified as individual property
     *  names or arrays of property names.
     * @param {*} [thisArg] The `this` binding of `predicate`.
     * @returns {Object} Returns the new object.
     * @example
     *
     * var object = { 'user': 'fred', 'age': 40 };
     *
     * _.omit(object, 'age');
     * // => { 'user': 'fred' }
     *
     * _.omit(object, _.isNumber);
     * // => { 'user': 'fred' }
     */
    var omit = restParam(function(object, props) {
      if (object == null) {
        return {};
      }
      if (typeof props[0] != 'function') {
        var props = arrayMap(baseFlatten(props), String);
        return pickByArray(object, baseDifference(keysIn(object), props));
      }
      var predicate = bindCallback(props[0], props[1], 3);
      return pickByCallback(object, function(value, key, object) {
        return !predicate(value, key, object);
      });
    });

    /**
     * Creates a two dimensional array of the key-value pairs for `object`,
     * e.g. `[[key1, value1], [key2, value2]]`.
     *
     * @static
     * @memberOf _
     * @category Object
     * @param {Object} object The object to query.
     * @returns {Array} Returns the new array of key-value pairs.
     * @example
     *
     * _.pairs({ 'barney': 36, 'fred': 40 });
     * // => [['barney', 36], ['fred', 40]] (iteration order is not guaranteed)
     */
    function pairs(object) {
      object = toObject(object);

      var index = -1,
          props = keys(object),
          length = props.length,
          result = Array(length);

      while (++index < length) {
        var key = props[index];
        result[index] = [key, object[key]];
      }
      return result;
    }

    /**
     * Creates an object composed of the picked `object` properties. Property
     * names may be specified as individual arguments or as arrays of property
     * names. If `predicate` is provided it is invoked for each property of `object`
     * picking the properties `predicate` returns truthy for. The predicate is
     * bound to `thisArg` and invoked with three arguments: (value, key, object).
     *
     * @static
     * @memberOf _
     * @category Object
     * @param {Object} object The source object.
     * @param {Function|...(string|string[])} [predicate] The function invoked per
     *  iteration or property names to pick, specified as individual property
     *  names or arrays of property names.
     * @param {*} [thisArg] The `this` binding of `predicate`.
     * @returns {Object} Returns the new object.
     * @example
     *
     * var object = { 'user': 'fred', 'age': 40 };
     *
     * _.pick(object, 'user');
     * // => { 'user': 'fred' }
     *
     * _.pick(object, _.isString);
     * // => { 'user': 'fred' }
     */
    var pick = restParam(function(object, props) {
      if (object == null) {
        return {};
      }
      return typeof props[0] == 'function'
        ? pickByCallback(object, bindCallback(props[0], props[1], 3))
        : pickByArray(object, baseFlatten(props));
    });

    /**
     * This method is like `_.get` except that if the resolved value is a function
     * it is invoked with the `this` binding of its parent object and its result
     * is returned.
     *
     * @static
     * @memberOf _
     * @category Object
     * @param {Object} object The object to query.
     * @param {Array|string} path The path of the property to resolve.
     * @param {*} [defaultValue] The value returned if the resolved value is `undefined`.
     * @returns {*} Returns the resolved value.
     * @example
     *
     * var object = { 'a': [{ 'b': { 'c1': 3, 'c2': _.constant(4) } }] };
     *
     * _.result(object, 'a[0].b.c1');
     * // => 3
     *
     * _.result(object, 'a[0].b.c2');
     * // => 4
     *
     * _.result(object, 'a.b.c', 'default');
     * // => 'default'
     *
     * _.result(object, 'a.b.c', _.constant('default'));
     * // => 'default'
     */
    function result(object, path, defaultValue) {
      var result = object == null ? undefined : object[path];
      if (result === undefined) {
        if (object != null && !isKey(path, object)) {
          path = toPath(path);
          object = path.length == 1 ? object : baseGet(object, baseSlice(path, 0, -1));
          result = object == null ? undefined : object[last(path)];
        }
        result = result === undefined ? defaultValue : result;
      }
      return isFunction(result) ? result.call(object) : result;
    }

    /**
     * Sets the property value of `path` on `object`. If a portion of `path`
     * does not exist it is created.
     *
     * @static
     * @memberOf _
     * @category Object
     * @param {Object} object The object to augment.
     * @param {Array|string} path The path of the property to set.
     * @param {*} value The value to set.
     * @returns {Object} Returns `object`.
     * @example
     *
     * var object = { 'a': [{ 'b': { 'c': 3 } }] };
     *
     * _.set(object, 'a[0].b.c', 4);
     * console.log(object.a[0].b.c);
     * // => 4
     *
     * _.set(object, 'x[0].y.z', 5);
     * console.log(object.x[0].y.z);
     * // => 5
     */
    function set(object, path, value) {
      if (object == null) {
        return object;
      }
      var pathKey = (path + '');
      path = (object[pathKey] != null || isKey(path, object)) ? [pathKey] : toPath(path);

      var index = -1,
          length = path.length,
          lastIndex = length - 1,
          nested = object;

      while (nested != null && ++index < length) {
        var key = path[index];
        if (isObject(nested)) {
          if (index == lastIndex) {
            nested[key] = value;
          } else if (nested[key] == null) {
            nested[key] = isIndex(path[index + 1]) ? [] : {};
          }
        }
        nested = nested[key];
      }
      return object;
    }

    /**
     * An alternative to `_.reduce`; this method transforms `object` to a new
     * `accumulator` object which is the result of running each of its own enumerable
     * properties through `iteratee`, with each invocation potentially mutating
     * the `accumulator` object. The `iteratee` is bound to `thisArg` and invoked
     * with four arguments: (accumulator, value, key, object). Iteratee functions
     * may exit iteration early by explicitly returning `false`.
     *
     * @static
     * @memberOf _
     * @category Object
     * @param {Array|Object} object The object to iterate over.
     * @param {Function} [iteratee=_.identity] The function invoked per iteration.
     * @param {*} [accumulator] The custom accumulator value.
     * @param {*} [thisArg] The `this` binding of `iteratee`.
     * @returns {*} Returns the accumulated value.
     * @example
     *
     * _.transform([2, 3, 4], function(result, n) {
     *   result.push(n *= n);
     *   return n % 2 == 0;
     * });
     * // => [4, 9]
     *
     * _.transform({ 'a': 1, 'b': 2 }, function(result, n, key) {
     *   result[key] = n * 3;
     * });
     * // => { 'a': 3, 'b': 6 }
     */
    function transform(object, iteratee, accumulator, thisArg) {
      var isArr = isArray(object) || isTypedArray(object);
      iteratee = getCallback(iteratee, thisArg, 4);

      if (accumulator == null) {
        if (isArr || isObject(object)) {
          var Ctor = object.constructor;
          if (isArr) {
            accumulator = isArray(object) ? new Ctor : [];
          } else {
            accumulator = baseCreate(isFunction(Ctor) ? Ctor.prototype : undefined);
          }
        } else {
          accumulator = {};
        }
      }
      (isArr ? arrayEach : baseForOwn)(object, function(value, index, object) {
        return iteratee(accumulator, value, index, object);
      });
      return accumulator;
    }

    /**
     * Creates an array of the own enumerable property values of `object`.
     *
     * **Note:** Non-object values are coerced to objects.
     *
     * @static
     * @memberOf _
     * @category Object
     * @param {Object} object The object to query.
     * @returns {Array} Returns the array of property values.
     * @example
     *
     * function Foo() {
     *   this.a = 1;
     *   this.b = 2;
     * }
     *
     * Foo.prototype.c = 3;
     *
     * _.values(new Foo);
     * // => [1, 2] (iteration order is not guaranteed)
     *
     * _.values('hi');
     * // => ['h', 'i']
     */
    function values(object) {
      return baseValues(object, keys(object));
    }

    /**
     * Creates an array of the own and inherited enumerable property values
     * of `object`.
     *
     * **Note:** Non-object values are coerced to objects.
     *
     * @static
     * @memberOf _
     * @category Object
     * @param {Object} object The object to query.
     * @returns {Array} Returns the array of property values.
     * @example
     *
     * function Foo() {
     *   this.a = 1;
     *   this.b = 2;
     * }
     *
     * Foo.prototype.c = 3;
     *
     * _.valuesIn(new Foo);
     * // => [1, 2, 3] (iteration order is not guaranteed)
     */
    function valuesIn(object) {
      return baseValues(object, keysIn(object));
    }

    /*------------------------------------------------------------------------*/

    /**
     * Checks if `n` is between `start` and up to but not including, `end`. If
     * `end` is not specified it is set to `start` with `start` then set to `0`.
     *
     * @static
     * @memberOf _
     * @category Number
     * @param {number} n The number to check.
     * @param {number} [start=0] The start of the range.
     * @param {number} end The end of the range.
     * @returns {boolean} Returns `true` if `n` is in the range, else `false`.
     * @example
     *
     * _.inRange(3, 2, 4);
     * // => true
     *
     * _.inRange(4, 8);
     * // => true
     *
     * _.inRange(4, 2);
     * // => false
     *
     * _.inRange(2, 2);
     * // => false
     *
     * _.inRange(1.2, 2);
     * // => true
     *
     * _.inRange(5.2, 4);
     * // => false
     */
    function inRange(value, start, end) {
      start = +start || 0;
      if (end === undefined) {
        end = start;
        start = 0;
      } else {
        end = +end || 0;
      }
      return value >= nativeMin(start, end) && value < nativeMax(start, end);
    }

    /**
     * Produces a random number between `min` and `max` (inclusive). If only one
     * argument is provided a number between `0` and the given number is returned.
     * If `floating` is `true`, or either `min` or `max` are floats, a floating-point
     * number is returned instead of an integer.
     *
     * @static
     * @memberOf _
     * @category Number
     * @param {number} [min=0] The minimum possible value.
     * @param {number} [max=1] The maximum possible value.
     * @param {boolean} [floating] Specify returning a floating-point number.
     * @returns {number} Returns the random number.
     * @example
     *
     * _.random(0, 5);
     * // => an integer between 0 and 5
     *
     * _.random(5);
     * // => also an integer between 0 and 5
     *
     * _.random(5, true);
     * // => a floating-point number between 0 and 5
     *
     * _.random(1.2, 5.2);
     * // => a floating-point number between 1.2 and 5.2
     */
    function random(min, max, floating) {
      if (floating && isIterateeCall(min, max, floating)) {
        max = floating = undefined;
      }
      var noMin = min == null,
          noMax = max == null;

      if (floating == null) {
        if (noMax && typeof min == 'boolean') {
          floating = min;
          min = 1;
        }
        else if (typeof max == 'boolean') {
          floating = max;
          noMax = true;
        }
      }
      if (noMin && noMax) {
        max = 1;
        noMax = false;
      }
      min = +min || 0;
      if (noMax) {
        max = min;
        min = 0;
      } else {
        max = +max || 0;
      }
      if (floating || min % 1 || max % 1) {
        var rand = nativeRandom();
        return nativeMin(min + (rand * (max - min + parseFloat('1e-' + ((rand + '').length - 1)))), max);
      }
      return baseRandom(min, max);
    }

    /*------------------------------------------------------------------------*/

    /**
     * Converts `string` to [camel case](https://en.wikipedia.org/wiki/CamelCase).
     *
     * @static
     * @memberOf _
     * @category String
     * @param {string} [string=''] The string to convert.
     * @returns {string} Returns the camel cased string.
     * @example
     *
     * _.camelCase('Foo Bar');
     * // => 'fooBar'
     *
     * _.camelCase('--foo-bar');
     * // => 'fooBar'
     *
     * _.camelCase('__foo_bar__');
     * // => 'fooBar'
     */
    var camelCase = createCompounder(function(result, word, index) {
      word = word.toLowerCase();
      return result + (index ? (word.charAt(0).toUpperCase() + word.slice(1)) : word);
    });

    /**
     * Capitalizes the first character of `string`.
     *
     * @static
     * @memberOf _
     * @category String
     * @param {string} [string=''] The string to capitalize.
     * @returns {string} Returns the capitalized string.
     * @example
     *
     * _.capitalize('fred');
     * // => 'Fred'
     */
    function capitalize(string) {
      string = baseToString(string);
      return string && (string.charAt(0).toUpperCase() + string.slice(1));
    }

    /**
     * Deburrs `string` by converting [latin-1 supplementary letters](https://en.wikipedia.org/wiki/Latin-1_Supplement_(Unicode_block)#Character_table)
     * to basic latin letters and removing [combining diacritical marks](https://en.wikipedia.org/wiki/Combining_Diacritical_Marks).
     *
     * @static
     * @memberOf _
     * @category String
     * @param {string} [string=''] The string to deburr.
     * @returns {string} Returns the deburred string.
     * @example
     *
     * _.deburr('déjà vu');
     * // => 'deja vu'
     */
    function deburr(string) {
      string = baseToString(string);
      return string && string.replace(reLatin1, deburrLetter).replace(reComboMark, '');
    }

    /**
     * Checks if `string` ends with the given target string.
     *
     * @static
     * @memberOf _
     * @category String
     * @param {string} [string=''] The string to search.
     * @param {string} [target] The string to search for.
     * @param {number} [position=string.length] The position to search from.
     * @returns {boolean} Returns `true` if `string` ends with `target`, else `false`.
     * @example
     *
     * _.endsWith('abc', 'c');
     * // => true
     *
     * _.endsWith('abc', 'b');
     * // => false
     *
     * _.endsWith('abc', 'b', 2);
     * // => true
     */
    function endsWith(string, target, position) {
      string = baseToString(string);
      target = (target + '');

      var length = string.length;
      position = position === undefined
        ? length
        : nativeMin(position < 0 ? 0 : (+position || 0), length);

      position -= target.length;
      return position >= 0 && string.indexOf(target, position) == position;
    }

    /**
     * Converts the characters "&", "<", ">", '"', "'", and "\`", in `string` to
     * their corresponding HTML entities.
     *
     * **Note:** No other characters are escaped. To escape additional characters
     * use a third-party library like [_he_](https://mths.be/he).
     *
     * Though the ">" character is escaped for symmetry, characters like
     * ">" and "/" don't need escaping in HTML and have no special meaning
     * unless they're part of a tag or unquoted attribute value.
     * See [Mathias Bynens's article](https://mathiasbynens.be/notes/ambiguous-ampersands)
     * (under "semi-related fun fact") for more details.
     *
     * Backticks are escaped because in Internet Explorer < 9, they can break out
     * of attribute values or HTML comments. See [#59](https://html5sec.org/#59),
     * [#102](https://html5sec.org/#102), [#108](https://html5sec.org/#108), and
     * [#133](https://html5sec.org/#133) of the [HTML5 Security Cheatsheet](https://html5sec.org/)
     * for more details.
     *
     * When working with HTML you should always [quote attribute values](http://wonko.com/post/html-escaping)
     * to reduce XSS vectors.
     *
     * @static
     * @memberOf _
     * @category String
     * @param {string} [string=''] The string to escape.
     * @returns {string} Returns the escaped string.
     * @example
     *
     * _.escape('fred, barney, & pebbles');
     * // => 'fred, barney, &amp; pebbles'
     */
    function escape(string) {
      // Reset `lastIndex` because in IE < 9 `String#replace` does not.
      string = baseToString(string);
      return (string && reHasUnescapedHtml.test(string))
        ? string.replace(reUnescapedHtml, escapeHtmlChar)
        : string;
    }

    /**
     * Escapes the `RegExp` special characters "\", "/", "^", "$", ".", "|", "?",
     * "*", "+", "(", ")", "[", "]", "{" and "}" in `string`.
     *
     * @static
     * @memberOf _
     * @category String
     * @param {string} [string=''] The string to escape.
     * @returns {string} Returns the escaped string.
     * @example
     *
     * _.escapeRegExp('[lodash](https://lodash.com/)');
     * // => '\[lodash\]\(https:\/\/lodash\.com\/\)'
     */
    function escapeRegExp(string) {
      string = baseToString(string);
      return (string && reHasRegExpChars.test(string))
        ? string.replace(reRegExpChars, escapeRegExpChar)
        : (string || '(?:)');
    }

    /**
     * Converts `string` to [kebab case](https://en.wikipedia.org/wiki/Letter_case#Special_case_styles).
     *
     * @static
     * @memberOf _
     * @category String
     * @param {string} [string=''] The string to convert.
     * @returns {string} Returns the kebab cased string.
     * @example
     *
     * _.kebabCase('Foo Bar');
     * // => 'foo-bar'
     *
     * _.kebabCase('fooBar');
     * // => 'foo-bar'
     *
     * _.kebabCase('__foo_bar__');
     * // => 'foo-bar'
     */
    var kebabCase = createCompounder(function(result, word, index) {
      return result + (index ? '-' : '') + word.toLowerCase();
    });

    /**
     * Pads `string` on the left and right sides if it's shorter than `length`.
     * Padding characters are truncated if they can't be evenly divided by `length`.
     *
     * @static
     * @memberOf _
     * @category String
     * @param {string} [string=''] The string to pad.
     * @param {number} [length=0] The padding length.
     * @param {string} [chars=' '] The string used as padding.
     * @returns {string} Returns the padded string.
     * @example
     *
     * _.pad('abc', 8);
     * // => '  abc   '
     *
     * _.pad('abc', 8, '_-');
     * // => '_-abc_-_'
     *
     * _.pad('abc', 3);
     * // => 'abc'
     */
    function pad(string, length, chars) {
      string = baseToString(string);
      length = +length;

      var strLength = string.length;
      if (strLength >= length || !nativeIsFinite(length)) {
        return string;
      }
      var mid = (length - strLength) / 2,
          leftLength = nativeFloor(mid),
          rightLength = nativeCeil(mid);

      chars = createPadding('', rightLength, chars);
      return chars.slice(0, leftLength) + string + chars;
    }

    /**
     * Pads `string` on the left side if it's shorter than `length`. Padding
     * characters are truncated if they exceed `length`.
     *
     * @static
     * @memberOf _
     * @category String
     * @param {string} [string=''] The string to pad.
     * @param {number} [length=0] The padding length.
     * @param {string} [chars=' '] The string used as padding.
     * @returns {string} Returns the padded string.
     * @example
     *
     * _.padLeft('abc', 6);
     * // => '   abc'
     *
     * _.padLeft('abc', 6, '_-');
     * // => '_-_abc'
     *
     * _.padLeft('abc', 3);
     * // => 'abc'
     */
    var padLeft = createPadDir();

    /**
     * Pads `string` on the right side if it's shorter than `length`. Padding
     * characters are truncated if they exceed `length`.
     *
     * @static
     * @memberOf _
     * @category String
     * @param {string} [string=''] The string to pad.
     * @param {number} [length=0] The padding length.
     * @param {string} [chars=' '] The string used as padding.
     * @returns {string} Returns the padded string.
     * @example
     *
     * _.padRight('abc', 6);
     * // => 'abc   '
     *
     * _.padRight('abc', 6, '_-');
     * // => 'abc_-_'
     *
     * _.padRight('abc', 3);
     * // => 'abc'
     */
    var padRight = createPadDir(true);

    /**
     * Converts `string` to an integer of the specified radix. If `radix` is
     * `undefined` or `0`, a `radix` of `10` is used unless `value` is a hexadecimal,
     * in which case a `radix` of `16` is used.
     *
     * **Note:** This method aligns with the [ES5 implementation](https://es5.github.io/#E)
     * of `parseInt`.
     *
     * @static
     * @memberOf _
     * @category String
     * @param {string} string The string to convert.
     * @param {number} [radix] The radix to interpret `value` by.
     * @param- {Object} [guard] Enables use as a callback for functions like `_.map`.
     * @returns {number} Returns the converted integer.
     * @example
     *
     * _.parseInt('08');
     * // => 8
     *
     * _.map(['6', '08', '10'], _.parseInt);
     * // => [6, 8, 10]
     */
    function parseInt(string, radix, guard) {
      // Firefox < 21 and Opera < 15 follow ES3 for `parseInt`.
      // Chrome fails to trim leading <BOM> whitespace characters.
      // See https://code.google.com/p/v8/issues/detail?id=3109 for more details.
      if (guard ? isIterateeCall(string, radix, guard) : radix == null) {
        radix = 0;
      } else if (radix) {
        radix = +radix;
      }
      string = trim(string);
      return nativeParseInt(string, radix || (reHasHexPrefix.test(string) ? 16 : 10));
    }

    /**
     * Repeats the given string `n` times.
     *
     * @static
     * @memberOf _
     * @category String
     * @param {string} [string=''] The string to repeat.
     * @param {number} [n=0] The number of times to repeat the string.
     * @returns {string} Returns the repeated string.
     * @example
     *
     * _.repeat('*', 3);
     * // => '***'
     *
     * _.repeat('abc', 2);
     * // => 'abcabc'
     *
     * _.repeat('abc', 0);
     * // => ''
     */
    function repeat(string, n) {
      var result = '';
      string = baseToString(string);
      n = +n;
      if (n < 1 || !string || !nativeIsFinite(n)) {
        return result;
      }
      // Leverage the exponentiation by squaring algorithm for a faster repeat.
      // See https://en.wikipedia.org/wiki/Exponentiation_by_squaring for more details.
      do {
        if (n % 2) {
          result += string;
        }
        n = nativeFloor(n / 2);
        string += string;
      } while (n);

      return result;
    }

    /**
     * Converts `string` to [snake case](https://en.wikipedia.org/wiki/Snake_case).
     *
     * @static
     * @memberOf _
     * @category String
     * @param {string} [string=''] The string to convert.
     * @returns {string} Returns the snake cased string.
     * @example
     *
     * _.snakeCase('Foo Bar');
     * // => 'foo_bar'
     *
     * _.snakeCase('fooBar');
     * // => 'foo_bar'
     *
     * _.snakeCase('--foo-bar');
     * // => 'foo_bar'
     */
    var snakeCase = createCompounder(function(result, word, index) {
      return result + (index ? '_' : '') + word.toLowerCase();
    });

    /**
     * Converts `string` to [start case](https://en.wikipedia.org/wiki/Letter_case#Stylistic_or_specialised_usage).
     *
     * @static
     * @memberOf _
     * @category String
     * @param {string} [string=''] The string to convert.
     * @returns {string} Returns the start cased string.
     * @example
     *
     * _.startCase('--foo-bar');
     * // => 'Foo Bar'
     *
     * _.startCase('fooBar');
     * // => 'Foo Bar'
     *
     * _.startCase('__foo_bar__');
     * // => 'Foo Bar'
     */
    var startCase = createCompounder(function(result, word, index) {
      return result + (index ? ' ' : '') + (word.charAt(0).toUpperCase() + word.slice(1));
    });

    /**
     * Checks if `string` starts with the given target string.
     *
     * @static
     * @memberOf _
     * @category String
     * @param {string} [string=''] The string to search.
     * @param {string} [target] The string to search for.
     * @param {number} [position=0] The position to search from.
     * @returns {boolean} Returns `true` if `string` starts with `target`, else `false`.
     * @example
     *
     * _.startsWith('abc', 'a');
     * // => true
     *
     * _.startsWith('abc', 'b');
     * // => false
     *
     * _.startsWith('abc', 'b', 1);
     * // => true
     */
    function startsWith(string, target, position) {
      string = baseToString(string);
      position = position == null
        ? 0
        : nativeMin(position < 0 ? 0 : (+position || 0), string.length);

      return string.lastIndexOf(target, position) == position;
    }

    /**
     * Creates a compiled template function that can interpolate data properties
     * in "interpolate" delimiters, HTML-escape interpolated data properties in
     * "escape" delimiters, and execute JavaScript in "evaluate" delimiters. Data
     * properties may be accessed as free variables in the template. If a setting
     * object is provided it takes precedence over `_.templateSettings` values.
     *
     * **Note:** In the development build `_.template` utilizes
     * [sourceURLs](http://www.html5rocks.com/en/tutorials/developertools/sourcemaps/#toc-sourceurl)
     * for easier debugging.
     *
     * For more information on precompiling templates see
     * [lodash's custom builds documentation](https://lodash.com/custom-builds).
     *
     * For more information on Chrome extension sandboxes see
     * [Chrome's extensions documentation](https://developer.chrome.com/extensions/sandboxingEval).
     *
     * @static
     * @memberOf _
     * @category String
     * @param {string} [string=''] The template string.
     * @param {Object} [options] The options object.
     * @param {RegExp} [options.escape] The HTML "escape" delimiter.
     * @param {RegExp} [options.evaluate] The "evaluate" delimiter.
     * @param {Object} [options.imports] An object to import into the template as free variables.
     * @param {RegExp} [options.interpolate] The "interpolate" delimiter.
     * @param {string} [options.sourceURL] The sourceURL of the template's compiled source.
     * @param {string} [options.variable] The data object variable name.
     * @param- {Object} [otherOptions] Enables the legacy `options` param signature.
     * @returns {Function} Returns the compiled template function.
     * @example
     *
     * // using the "interpolate" delimiter to create a compiled template
     * var compiled = _.template('hello <%= user %>!');
     * compiled({ 'user': 'fred' });
     * // => 'hello fred!'
     *
     * // using the HTML "escape" delimiter to escape data property values
     * var compiled = _.template('<b><%- value %></b>');
     * compiled({ 'value': '<script>' });
     * // => '<b>&lt;script&gt;</b>'
     *
     * // using the "evaluate" delimiter to execute JavaScript and generate HTML
     * var compiled = _.template('<% _.forEach(users, function(user) { %><li><%- user %></li><% }); %>');
     * compiled({ 'users': ['fred', 'barney'] });
     * // => '<li>fred</li><li>barney</li>'
     *
     * // using the internal `print` function in "evaluate" delimiters
     * var compiled = _.template('<% print("hello " + user); %>!');
     * compiled({ 'user': 'barney' });
     * // => 'hello barney!'
     *
     * // using the ES delimiter as an alternative to the default "interpolate" delimiter
     * var compiled = _.template('hello ${ user }!');
     * compiled({ 'user': 'pebbles' });
     * // => 'hello pebbles!'
     *
     * // using custom template delimiters
     * _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;
     * var compiled = _.template('hello {{ user }}!');
     * compiled({ 'user': 'mustache' });
     * // => 'hello mustache!'
     *
     * // using backslashes to treat delimiters as plain text
     * var compiled = _.template('<%= "\\<%- value %\\>" %>');
     * compiled({ 'value': 'ignored' });
     * // => '<%- value %>'
     *
     * // using the `imports` option to import `jQuery` as `jq`
     * var text = '<% jq.each(users, function(user) { %><li><%- user %></li><% }); %>';
     * var compiled = _.template(text, { 'imports': { 'jq': jQuery } });
     * compiled({ 'users': ['fred', 'barney'] });
     * // => '<li>fred</li><li>barney</li>'
     *
     * // using the `sourceURL` option to specify a custom sourceURL for the template
     * var compiled = _.template('hello <%= user %>!', { 'sourceURL': '/basic/greeting.jst' });
     * compiled(data);
     * // => find the source of "greeting.jst" under the Sources tab or Resources panel of the web inspector
     *
     * // using the `variable` option to ensure a with-statement isn't used in the compiled template
     * var compiled = _.template('hi <%= data.user %>!', { 'variable': 'data' });
     * compiled.source;
     * // => function(data) {
     * //   var __t, __p = '';
     * //   __p += 'hi ' + ((__t = ( data.user )) == null ? '' : __t) + '!';
     * //   return __p;
     * // }
     *
     * // using the `source` property to inline compiled templates for meaningful
     * // line numbers in error messages and a stack trace
     * fs.writeFileSync(path.join(cwd, 'jst.js'), '\
     *   var JST = {\
     *     "main": ' + _.template(mainText).source + '\
     *   };\
     * ');
     */
    function template(string, options, otherOptions) {
      // Based on John Resig's `tmpl` implementation (http://ejohn.org/blog/javascript-micro-templating/)
      // and Laura Doktorova's doT.js (https://github.com/olado/doT).
      var settings = lodash.templateSettings;

      if (otherOptions && isIterateeCall(string, options, otherOptions)) {
        options = otherOptions = undefined;
      }
      string = baseToString(string);
      options = assignWith(baseAssign({}, otherOptions || options), settings, assignOwnDefaults);

      var imports = assignWith(baseAssign({}, options.imports), settings.imports, assignOwnDefaults),
          importsKeys = keys(imports),
          importsValues = baseValues(imports, importsKeys);

      var isEscaping,
          isEvaluating,
          index = 0,
          interpolate = options.interpolate || reNoMatch,
          source = "__p += '";

      // Compile the regexp to match each delimiter.
      var reDelimiters = RegExp(
        (options.escape || reNoMatch).source + '|' +
        interpolate.source + '|' +
        (interpolate === reInterpolate ? reEsTemplate : reNoMatch).source + '|' +
        (options.evaluate || reNoMatch).source + '|$'
      , 'g');

      // Use a sourceURL for easier debugging.
      var sourceURL = '//# sourceURL=' +
        ('sourceURL' in options
          ? options.sourceURL
          : ('lodash.templateSources[' + (++templateCounter) + ']')
        ) + '\n';

      string.replace(reDelimiters, function(match, escapeValue, interpolateValue, esTemplateValue, evaluateValue, offset) {
        interpolateValue || (interpolateValue = esTemplateValue);

        // Escape characters that can't be included in string literals.
        source += string.slice(index, offset).replace(reUnescapedString, escapeStringChar);

        // Replace delimiters with snippets.
        if (escapeValue) {
          isEscaping = true;
          source += "' +\n__e(" + escapeValue + ") +\n'";
        }
        if (evaluateValue) {
          isEvaluating = true;
          source += "';\n" + evaluateValue + ";\n__p += '";
        }
        if (interpolateValue) {
          source += "' +\n((__t = (" + interpolateValue + ")) == null ? '' : __t) +\n'";
        }
        index = offset + match.length;

        // The JS engine embedded in Adobe products requires returning the `match`
        // string in order to produce the correct `offset` value.
        return match;
      });

      source += "';\n";

      // If `variable` is not specified wrap a with-statement around the generated
      // code to add the data object to the top of the scope chain.
      var variable = options.variable;
      if (!variable) {
        source = 'with (obj) {\n' + source + '\n}\n';
      }
      // Cleanup code by stripping empty strings.
      source = (isEvaluating ? source.replace(reEmptyStringLeading, '') : source)
        .replace(reEmptyStringMiddle, '$1')
        .replace(reEmptyStringTrailing, '$1;');

      // Frame code as the function body.
      source = 'function(' + (variable || 'obj') + ') {\n' +
        (variable
          ? ''
          : 'obj || (obj = {});\n'
        ) +
        "var __t, __p = ''" +
        (isEscaping
           ? ', __e = _.escape'
           : ''
        ) +
        (isEvaluating
          ? ', __j = Array.prototype.join;\n' +
            "function print() { __p += __j.call(arguments, '') }\n"
          : ';\n'
        ) +
        source +
        'return __p\n}';

      var result = attempt(function() {
        return Function(importsKeys, sourceURL + 'return ' + source).apply(undefined, importsValues);
      });

      // Provide the compiled function's source by its `toString` method or
      // the `source` property as a convenience for inlining compiled templates.
      result.source = source;
      if (isError(result)) {
        throw result;
      }
      return result;
    }

    /**
     * Removes leading and trailing whitespace or specified characters from `string`.
     *
     * @static
     * @memberOf _
     * @category String
     * @param {string} [string=''] The string to trim.
     * @param {string} [chars=whitespace] The characters to trim.
     * @param- {Object} [guard] Enables use as a callback for functions like `_.map`.
     * @returns {string} Returns the trimmed string.
     * @example
     *
     * _.trim('  abc  ');
     * // => 'abc'
     *
     * _.trim('-_-abc-_-', '_-');
     * // => 'abc'
     *
     * _.map(['  foo  ', '  bar  '], _.trim);
     * // => ['foo', 'bar']
     */
    function trim(string, chars, guard) {
      var value = string;
      string = baseToString(string);
      if (!string) {
        return string;
      }
      if (guard ? isIterateeCall(value, chars, guard) : chars == null) {
        return string.slice(trimmedLeftIndex(string), trimmedRightIndex(string) + 1);
      }
      chars = (chars + '');
      return string.slice(charsLeftIndex(string, chars), charsRightIndex(string, chars) + 1);
    }

    /**
     * Removes leading whitespace or specified characters from `string`.
     *
     * @static
     * @memberOf _
     * @category String
     * @param {string} [string=''] The string to trim.
     * @param {string} [chars=whitespace] The characters to trim.
     * @param- {Object} [guard] Enables use as a callback for functions like `_.map`.
     * @returns {string} Returns the trimmed string.
     * @example
     *
     * _.trimLeft('  abc  ');
     * // => 'abc  '
     *
     * _.trimLeft('-_-abc-_-', '_-');
     * // => 'abc-_-'
     */
    function trimLeft(string, chars, guard) {
      var value = string;
      string = baseToString(string);
      if (!string) {
        return string;
      }
      if (guard ? isIterateeCall(value, chars, guard) : chars == null) {
        return string.slice(trimmedLeftIndex(string));
      }
      return string.slice(charsLeftIndex(string, (chars + '')));
    }

    /**
     * Removes trailing whitespace or specified characters from `string`.
     *
     * @static
     * @memberOf _
     * @category String
     * @param {string} [string=''] The string to trim.
     * @param {string} [chars=whitespace] The characters to trim.
     * @param- {Object} [guard] Enables use as a callback for functions like `_.map`.
     * @returns {string} Returns the trimmed string.
     * @example
     *
     * _.trimRight('  abc  ');
     * // => '  abc'
     *
     * _.trimRight('-_-abc-_-', '_-');
     * // => '-_-abc'
     */
    function trimRight(string, chars, guard) {
      var value = string;
      string = baseToString(string);
      if (!string) {
        return string;
      }
      if (guard ? isIterateeCall(value, chars, guard) : chars == null) {
        return string.slice(0, trimmedRightIndex(string) + 1);
      }
      return string.slice(0, charsRightIndex(string, (chars + '')) + 1);
    }

    /**
     * Truncates `string` if it's longer than the given maximum string length.
     * The last characters of the truncated string are replaced with the omission
     * string which defaults to "...".
     *
     * @static
     * @memberOf _
     * @category String
     * @param {string} [string=''] The string to truncate.
     * @param {Object|number} [options] The options object or maximum string length.
     * @param {number} [options.length=30] The maximum string length.
     * @param {string} [options.omission='...'] The string to indicate text is omitted.
     * @param {RegExp|string} [options.separator] The separator pattern to truncate to.
     * @param- {Object} [guard] Enables use as a callback for functions like `_.map`.
     * @returns {string} Returns the truncated string.
     * @example
     *
     * _.trunc('hi-diddly-ho there, neighborino');
     * // => 'hi-diddly-ho there, neighbo...'
     *
     * _.trunc('hi-diddly-ho there, neighborino', 24);
     * // => 'hi-diddly-ho there, n...'
     *
     * _.trunc('hi-diddly-ho there, neighborino', {
     *   'length': 24,
     *   'separator': ' '
     * });
     * // => 'hi-diddly-ho there,...'
     *
     * _.trunc('hi-diddly-ho there, neighborino', {
     *   'length': 24,
     *   'separator': /,? +/
     * });
     * // => 'hi-diddly-ho there...'
     *
     * _.trunc('hi-diddly-ho there, neighborino', {
     *   'omission': ' [...]'
     * });
     * // => 'hi-diddly-ho there, neig [...]'
     */
    function trunc(string, options, guard) {
      if (guard && isIterateeCall(string, options, guard)) {
        options = undefined;
      }
      var length = DEFAULT_TRUNC_LENGTH,
          omission = DEFAULT_TRUNC_OMISSION;

      if (options != null) {
        if (isObject(options)) {
          var separator = 'separator' in options ? options.separator : separator;
          length = 'length' in options ? (+options.length || 0) : length;
          omission = 'omission' in options ? baseToString(options.omission) : omission;
        } else {
          length = +options || 0;
        }
      }
      string = baseToString(string);
      if (length >= string.length) {
        return string;
      }
      var end = length - omission.length;
      if (end < 1) {
        return omission;
      }
      var result = string.slice(0, end);
      if (separator == null) {
        return result + omission;
      }
      if (isRegExp(separator)) {
        if (string.slice(end).search(separator)) {
          var match,
              newEnd,
              substring = string.slice(0, end);

          if (!separator.global) {
            separator = RegExp(separator.source, (reFlags.exec(separator) || '') + 'g');
          }
          separator.lastIndex = 0;
          while ((match = separator.exec(substring))) {
            newEnd = match.index;
          }
          result = result.slice(0, newEnd == null ? end : newEnd);
        }
      } else if (string.indexOf(separator, end) != end) {
        var index = result.lastIndexOf(separator);
        if (index > -1) {
          result = result.slice(0, index);
        }
      }
      return result + omission;
    }

    /**
     * The inverse of `_.escape`; this method converts the HTML entities
     * `&amp;`, `&lt;`, `&gt;`, `&quot;`, `&#39;`, and `&#96;` in `string` to their
     * corresponding characters.
     *
     * **Note:** No other HTML entities are unescaped. To unescape additional HTML
     * entities use a third-party library like [_he_](https://mths.be/he).
     *
     * @static
     * @memberOf _
     * @category String
     * @param {string} [string=''] The string to unescape.
     * @returns {string} Returns the unescaped string.
     * @example
     *
     * _.unescape('fred, barney, &amp; pebbles');
     * // => 'fred, barney, & pebbles'
     */
    function unescape(string) {
      string = baseToString(string);
      return (string && reHasEscapedHtml.test(string))
        ? string.replace(reEscapedHtml, unescapeHtmlChar)
        : string;
    }

    /**
     * Splits `string` into an array of its words.
     *
     * @static
     * @memberOf _
     * @category String
     * @param {string} [string=''] The string to inspect.
     * @param {RegExp|string} [pattern] The pattern to match words.
     * @param- {Object} [guard] Enables use as a callback for functions like `_.map`.
     * @returns {Array} Returns the words of `string`.
     * @example
     *
     * _.words('fred, barney, & pebbles');
     * // => ['fred', 'barney', 'pebbles']
     *
     * _.words('fred, barney, & pebbles', /[^, ]+/g);
     * // => ['fred', 'barney', '&', 'pebbles']
     */
    function words(string, pattern, guard) {
      if (guard && isIterateeCall(string, pattern, guard)) {
        pattern = undefined;
      }
      string = baseToString(string);
      return string.match(pattern || reWords) || [];
    }

    /*------------------------------------------------------------------------*/

    /**
     * Attempts to invoke `func`, returning either the result or the caught error
     * object. Any additional arguments are provided to `func` when it is invoked.
     *
     * @static
     * @memberOf _
     * @category Utility
     * @param {Function} func The function to attempt.
     * @returns {*} Returns the `func` result or error object.
     * @example
     *
     * // avoid throwing errors for invalid selectors
     * var elements = _.attempt(function(selector) {
     *   return document.querySelectorAll(selector);
     * }, '>_>');
     *
     * if (_.isError(elements)) {
     *   elements = [];
     * }
     */
    var attempt = restParam(function(func, args) {
      try {
        return func.apply(undefined, args);
      } catch(e) {
        return isError(e) ? e : new Error(e);
      }
    });

    /**
     * Creates a function that invokes `func` with the `this` binding of `thisArg`
     * and arguments of the created function. If `func` is a property name the
     * created callback returns the property value for a given element. If `func`
     * is an object the created callback returns `true` for elements that contain
     * the equivalent object properties, otherwise it returns `false`.
     *
     * @static
     * @memberOf _
     * @alias iteratee
     * @category Utility
     * @param {*} [func=_.identity] The value to convert to a callback.
     * @param {*} [thisArg] The `this` binding of `func`.
     * @param- {Object} [guard] Enables use as a callback for functions like `_.map`.
     * @returns {Function} Returns the callback.
     * @example
     *
     * var users = [
     *   { 'user': 'barney', 'age': 36 },
     *   { 'user': 'fred',   'age': 40 }
     * ];
     *
     * // wrap to create custom callback shorthands
     * _.callback = _.wrap(_.callback, function(callback, func, thisArg) {
     *   var match = /^(.+?)__([gl]t)(.+)$/.exec(func);
     *   if (!match) {
     *     return callback(func, thisArg);
     *   }
     *   return function(object) {
     *     return match[2] == 'gt'
     *       ? object[match[1]] > match[3]
     *       : object[match[1]] < match[3];
     *   };
     * });
     *
     * _.filter(users, 'age__gt36');
     * // => [{ 'user': 'fred', 'age': 40 }]
     */
    function callback(func, thisArg, guard) {
      if (guard && isIterateeCall(func, thisArg, guard)) {
        thisArg = undefined;
      }
      return isObjectLike(func)
        ? matches(func)
        : baseCallback(func, thisArg);
    }

    /**
     * Creates a function that returns `value`.
     *
     * @static
     * @memberOf _
     * @category Utility
     * @param {*} value The value to return from the new function.
     * @returns {Function} Returns the new function.
     * @example
     *
     * var object = { 'user': 'fred' };
     * var getter = _.constant(object);
     *
     * getter() === object;
     * // => true
     */
    function constant(value) {
      return function() {
        return value;
      };
    }

    /**
     * This method returns the first argument provided to it.
     *
     * @static
     * @memberOf _
     * @category Utility
     * @param {*} value Any value.
     * @returns {*} Returns `value`.
     * @example
     *
     * var object = { 'user': 'fred' };
     *
     * _.identity(object) === object;
     * // => true
     */
    function identity(value) {
      return value;
    }

    /**
     * Creates a function that performs a deep comparison between a given object
     * and `source`, returning `true` if the given object has equivalent property
     * values, else `false`.
     *
     * **Note:** This method supports comparing arrays, booleans, `Date` objects,
     * numbers, `Object` objects, regexes, and strings. Objects are compared by
     * their own, not inherited, enumerable properties. For comparing a single
     * own or inherited property value see `_.matchesProperty`.
     *
     * @static
     * @memberOf _
     * @category Utility
     * @param {Object} source The object of property values to match.
     * @returns {Function} Returns the new function.
     * @example
     *
     * var users = [
     *   { 'user': 'barney', 'age': 36, 'active': true },
     *   { 'user': 'fred',   'age': 40, 'active': false }
     * ];
     *
     * _.filter(users, _.matches({ 'age': 40, 'active': false }));
     * // => [{ 'user': 'fred', 'age': 40, 'active': false }]
     */
    function matches(source) {
      return baseMatches(baseClone(source, true));
    }

    /**
     * Creates a function that compares the property value of `path` on a given
     * object to `value`.
     *
     * **Note:** This method supports comparing arrays, booleans, `Date` objects,
     * numbers, `Object` objects, regexes, and strings. Objects are compared by
     * their own, not inherited, enumerable properties.
     *
     * @static
     * @memberOf _
     * @category Utility
     * @param {Array|string} path The path of the property to get.
     * @param {*} srcValue The value to match.
     * @returns {Function} Returns the new function.
     * @example
     *
     * var users = [
     *   { 'user': 'barney' },
     *   { 'user': 'fred' }
     * ];
     *
     * _.find(users, _.matchesProperty('user', 'fred'));
     * // => { 'user': 'fred' }
     */
    function matchesProperty(path, srcValue) {
      return baseMatchesProperty(path, baseClone(srcValue, true));
    }

    /**
     * Creates a function that invokes the method at `path` on a given object.
     * Any additional arguments are provided to the invoked method.
     *
     * @static
     * @memberOf _
     * @category Utility
     * @param {Array|string} path The path of the method to invoke.
     * @param {...*} [args] The arguments to invoke the method with.
     * @returns {Function} Returns the new function.
     * @example
     *
     * var objects = [
     *   { 'a': { 'b': { 'c': _.constant(2) } } },
     *   { 'a': { 'b': { 'c': _.constant(1) } } }
     * ];
     *
     * _.map(objects, _.method('a.b.c'));
     * // => [2, 1]
     *
     * _.invoke(_.sortBy(objects, _.method(['a', 'b', 'c'])), 'a.b.c');
     * // => [1, 2]
     */
    var method = restParam(function(path, args) {
      return function(object) {
        return invokePath(object, path, args);
      };
    });

    /**
     * The opposite of `_.method`; this method creates a function that invokes
     * the method at a given path on `object`. Any additional arguments are
     * provided to the invoked method.
     *
     * @static
     * @memberOf _
     * @category Utility
     * @param {Object} object The object to query.
     * @param {...*} [args] The arguments to invoke the method with.
     * @returns {Function} Returns the new function.
     * @example
     *
     * var array = _.times(3, _.constant),
     *     object = { 'a': array, 'b': array, 'c': array };
     *
     * _.map(['a[2]', 'c[0]'], _.methodOf(object));
     * // => [2, 0]
     *
     * _.map([['a', '2'], ['c', '0']], _.methodOf(object));
     * // => [2, 0]
     */
    var methodOf = restParam(function(object, args) {
      return function(path) {
        return invokePath(object, path, args);
      };
    });

    /**
     * Adds all own enumerable function properties of a source object to the
     * destination object. If `object` is a function then methods are added to
     * its prototype as well.
     *
     * **Note:** Use `_.runInContext` to create a pristine `lodash` function to
     * avoid conflicts caused by modifying the original.
     *
     * @static
     * @memberOf _
     * @category Utility
     * @param {Function|Object} [object=lodash] The destination object.
     * @param {Object} source The object of functions to add.
     * @param {Object} [options] The options object.
     * @param {boolean} [options.chain=true] Specify whether the functions added
     *  are chainable.
     * @returns {Function|Object} Returns `object`.
     * @example
     *
     * function vowels(string) {
     *   return _.filter(string, function(v) {
     *     return /[aeiou]/i.test(v);
     *   });
     * }
     *
     * _.mixin({ 'vowels': vowels });
     * _.vowels('fred');
     * // => ['e']
     *
     * _('fred').vowels().value();
     * // => ['e']
     *
     * _.mixin({ 'vowels': vowels }, { 'chain': false });
     * _('fred').vowels();
     * // => ['e']
     */
    function mixin(object, source, options) {
      if (options == null) {
        var isObj = isObject(source),
            props = isObj ? keys(source) : undefined,
            methodNames = (props && props.length) ? baseFunctions(source, props) : undefined;

        if (!(methodNames ? methodNames.length : isObj)) {
          methodNames = false;
          options = source;
          source = object;
          object = this;
        }
      }
      if (!methodNames) {
        methodNames = baseFunctions(source, keys(source));
      }
      var chain = true,
          index = -1,
          isFunc = isFunction(object),
          length = methodNames.length;

      if (options === false) {
        chain = false;
      } else if (isObject(options) && 'chain' in options) {
        chain = options.chain;
      }
      while (++index < length) {
        var methodName = methodNames[index],
            func = source[methodName];

        object[methodName] = func;
        if (isFunc) {
          object.prototype[methodName] = (function(func) {
            return function() {
              var chainAll = this.__chain__;
              if (chain || chainAll) {
                var result = object(this.__wrapped__),
                    actions = result.__actions__ = arrayCopy(this.__actions__);

                actions.push({ 'func': func, 'args': arguments, 'thisArg': object });
                result.__chain__ = chainAll;
                return result;
              }
              return func.apply(object, arrayPush([this.value()], arguments));
            };
          }(func));
        }
      }
      return object;
    }

    /**
     * Reverts the `_` variable to its previous value and returns a reference to
     * the `lodash` function.
     *
     * @static
     * @memberOf _
     * @category Utility
     * @returns {Function} Returns the `lodash` function.
     * @example
     *
     * var lodash = _.noConflict();
     */
    function noConflict() {
      root._ = oldDash;
      return this;
    }

    /**
     * A no-operation function that returns `undefined` regardless of the
     * arguments it receives.
     *
     * @static
     * @memberOf _
     * @category Utility
     * @example
     *
     * var object = { 'user': 'fred' };
     *
     * _.noop(object) === undefined;
     * // => true
     */
    function noop() {
      // No operation performed.
    }

    /**
     * Creates a function that returns the property value at `path` on a
     * given object.
     *
     * @static
     * @memberOf _
     * @category Utility
     * @param {Array|string} path The path of the property to get.
     * @returns {Function} Returns the new function.
     * @example
     *
     * var objects = [
     *   { 'a': { 'b': { 'c': 2 } } },
     *   { 'a': { 'b': { 'c': 1 } } }
     * ];
     *
     * _.map(objects, _.property('a.b.c'));
     * // => [2, 1]
     *
     * _.pluck(_.sortBy(objects, _.property(['a', 'b', 'c'])), 'a.b.c');
     * // => [1, 2]
     */
    function property(path) {
      return isKey(path) ? baseProperty(path) : basePropertyDeep(path);
    }

    /**
     * The opposite of `_.property`; this method creates a function that returns
     * the property value at a given path on `object`.
     *
     * @static
     * @memberOf _
     * @category Utility
     * @param {Object} object The object to query.
     * @returns {Function} Returns the new function.
     * @example
     *
     * var array = [0, 1, 2],
     *     object = { 'a': array, 'b': array, 'c': array };
     *
     * _.map(['a[2]', 'c[0]'], _.propertyOf(object));
     * // => [2, 0]
     *
     * _.map([['a', '2'], ['c', '0']], _.propertyOf(object));
     * // => [2, 0]
     */
    function propertyOf(object) {
      return function(path) {
        return baseGet(object, toPath(path), path + '');
      };
    }

    /**
     * Creates an array of numbers (positive and/or negative) progressing from
     * `start` up to, but not including, `end`. If `end` is not specified it is
     * set to `start` with `start` then set to `0`. If `end` is less than `start`
     * a zero-length range is created unless a negative `step` is specified.
     *
     * @static
     * @memberOf _
     * @category Utility
     * @param {number} [start=0] The start of the range.
     * @param {number} end The end of the range.
     * @param {number} [step=1] The value to increment or decrement by.
     * @returns {Array} Returns the new array of numbers.
     * @example
     *
     * _.range(4);
     * // => [0, 1, 2, 3]
     *
     * _.range(1, 5);
     * // => [1, 2, 3, 4]
     *
     * _.range(0, 20, 5);
     * // => [0, 5, 10, 15]
     *
     * _.range(0, -4, -1);
     * // => [0, -1, -2, -3]
     *
     * _.range(1, 4, 0);
     * // => [1, 1, 1]
     *
     * _.range(0);
     * // => []
     */
    function range(start, end, step) {
      if (step && isIterateeCall(start, end, step)) {
        end = step = undefined;
      }
      start = +start || 0;
      step = step == null ? 1 : (+step || 0);

      if (end == null) {
        end = start;
        start = 0;
      } else {
        end = +end || 0;
      }
      // Use `Array(length)` so engines like Chakra and V8 avoid slower modes.
      // See https://youtu.be/XAqIpGU8ZZk#t=17m25s for more details.
      var index = -1,
          length = nativeMax(nativeCeil((end - start) / (step || 1)), 0),
          result = Array(length);

      while (++index < length) {
        result[index] = start;
        start += step;
      }
      return result;
    }

    /**
     * Invokes the iteratee function `n` times, returning an array of the results
     * of each invocation. The `iteratee` is bound to `thisArg` and invoked with
     * one argument; (index).
     *
     * @static
     * @memberOf _
     * @category Utility
     * @param {number} n The number of times to invoke `iteratee`.
     * @param {Function} [iteratee=_.identity] The function invoked per iteration.
     * @param {*} [thisArg] The `this` binding of `iteratee`.
     * @returns {Array} Returns the array of results.
     * @example
     *
     * var diceRolls = _.times(3, _.partial(_.random, 1, 6, false));
     * // => [3, 6, 4]
     *
     * _.times(3, function(n) {
     *   mage.castSpell(n);
     * });
     * // => invokes `mage.castSpell(n)` three times with `n` of `0`, `1`, and `2`
     *
     * _.times(3, function(n) {
     *   this.cast(n);
     * }, mage);
     * // => also invokes `mage.castSpell(n)` three times
     */
    function times(n, iteratee, thisArg) {
      n = nativeFloor(n);

      // Exit early to avoid a JSC JIT bug in Safari 8
      // where `Array(0)` is treated as `Array(1)`.
      if (n < 1 || !nativeIsFinite(n)) {
        return [];
      }
      var index = -1,
          result = Array(nativeMin(n, MAX_ARRAY_LENGTH));

      iteratee = bindCallback(iteratee, thisArg, 1);
      while (++index < n) {
        if (index < MAX_ARRAY_LENGTH) {
          result[index] = iteratee(index);
        } else {
          iteratee(index);
        }
      }
      return result;
    }

    /**
     * Generates a unique ID. If `prefix` is provided the ID is appended to it.
     *
     * @static
     * @memberOf _
     * @category Utility
     * @param {string} [prefix] The value to prefix the ID with.
     * @returns {string} Returns the unique ID.
     * @example
     *
     * _.uniqueId('contact_');
     * // => 'contact_104'
     *
     * _.uniqueId();
     * // => '105'
     */
    function uniqueId(prefix) {
      var id = ++idCounter;
      return baseToString(prefix) + id;
    }

    /*------------------------------------------------------------------------*/

    /**
     * Adds two numbers.
     *
     * @static
     * @memberOf _
     * @category Math
     * @param {number} augend The first number to add.
     * @param {number} addend The second number to add.
     * @returns {number} Returns the sum.
     * @example
     *
     * _.add(6, 4);
     * // => 10
     */
    function add(augend, addend) {
      return (+augend || 0) + (+addend || 0);
    }

    /**
     * Calculates `n` rounded up to `precision`.
     *
     * @static
     * @memberOf _
     * @category Math
     * @param {number} n The number to round up.
     * @param {number} [precision=0] The precision to round up to.
     * @returns {number} Returns the rounded up number.
     * @example
     *
     * _.ceil(4.006);
     * // => 5
     *
     * _.ceil(6.004, 2);
     * // => 6.01
     *
     * _.ceil(6040, -2);
     * // => 6100
     */
    var ceil = createRound('ceil');

    /**
     * Calculates `n` rounded down to `precision`.
     *
     * @static
     * @memberOf _
     * @category Math
     * @param {number} n The number to round down.
     * @param {number} [precision=0] The precision to round down to.
     * @returns {number} Returns the rounded down number.
     * @example
     *
     * _.floor(4.006);
     * // => 4
     *
     * _.floor(0.046, 2);
     * // => 0.04
     *
     * _.floor(4060, -2);
     * // => 4000
     */
    var floor = createRound('floor');

    /**
     * Gets the maximum value of `collection`. If `collection` is empty or falsey
     * `-Infinity` is returned. If an iteratee function is provided it is invoked
     * for each value in `collection` to generate the criterion by which the value
     * is ranked. The `iteratee` is bound to `thisArg` and invoked with three
     * arguments: (value, index, collection).
     *
     * If a property name is provided for `iteratee` the created `_.property`
     * style callback returns the property value of the given element.
     *
     * If a value is also provided for `thisArg` the created `_.matchesProperty`
     * style callback returns `true` for elements that have a matching property
     * value, else `false`.
     *
     * If an object is provided for `iteratee` the created `_.matches` style
     * callback returns `true` for elements that have the properties of the given
     * object, else `false`.
     *
     * @static
     * @memberOf _
     * @category Math
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function|Object|string} [iteratee] The function invoked per iteration.
     * @param {*} [thisArg] The `this` binding of `iteratee`.
     * @returns {*} Returns the maximum value.
     * @example
     *
     * _.max([4, 2, 8, 6]);
     * // => 8
     *
     * _.max([]);
     * // => -Infinity
     *
     * var users = [
     *   { 'user': 'barney', 'age': 36 },
     *   { 'user': 'fred',   'age': 40 }
     * ];
     *
     * _.max(users, function(chr) {
     *   return chr.age;
     * });
     * // => { 'user': 'fred', 'age': 40 }
     *
     * // using the `_.property` callback shorthand
     * _.max(users, 'age');
     * // => { 'user': 'fred', 'age': 40 }
     */
    var max = createExtremum(gt, NEGATIVE_INFINITY);

    /**
     * Gets the minimum value of `collection`. If `collection` is empty or falsey
     * `Infinity` is returned. If an iteratee function is provided it is invoked
     * for each value in `collection` to generate the criterion by which the value
     * is ranked. The `iteratee` is bound to `thisArg` and invoked with three
     * arguments: (value, index, collection).
     *
     * If a property name is provided for `iteratee` the created `_.property`
     * style callback returns the property value of the given element.
     *
     * If a value is also provided for `thisArg` the created `_.matchesProperty`
     * style callback returns `true` for elements that have a matching property
     * value, else `false`.
     *
     * If an object is provided for `iteratee` the created `_.matches` style
     * callback returns `true` for elements that have the properties of the given
     * object, else `false`.
     *
     * @static
     * @memberOf _
     * @category Math
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function|Object|string} [iteratee] The function invoked per iteration.
     * @param {*} [thisArg] The `this` binding of `iteratee`.
     * @returns {*} Returns the minimum value.
     * @example
     *
     * _.min([4, 2, 8, 6]);
     * // => 2
     *
     * _.min([]);
     * // => Infinity
     *
     * var users = [
     *   { 'user': 'barney', 'age': 36 },
     *   { 'user': 'fred',   'age': 40 }
     * ];
     *
     * _.min(users, function(chr) {
     *   return chr.age;
     * });
     * // => { 'user': 'barney', 'age': 36 }
     *
     * // using the `_.property` callback shorthand
     * _.min(users, 'age');
     * // => { 'user': 'barney', 'age': 36 }
     */
    var min = createExtremum(lt, POSITIVE_INFINITY);

    /**
     * Calculates `n` rounded to `precision`.
     *
     * @static
     * @memberOf _
     * @category Math
     * @param {number} n The number to round.
     * @param {number} [precision=0] The precision to round to.
     * @returns {number} Returns the rounded number.
     * @example
     *
     * _.round(4.006);
     * // => 4
     *
     * _.round(4.006, 2);
     * // => 4.01
     *
     * _.round(4060, -2);
     * // => 4100
     */
    var round = createRound('round');

    /**
     * Gets the sum of the values in `collection`.
     *
     * @static
     * @memberOf _
     * @category Math
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function|Object|string} [iteratee] The function invoked per iteration.
     * @param {*} [thisArg] The `this` binding of `iteratee`.
     * @returns {number} Returns the sum.
     * @example
     *
     * _.sum([4, 6]);
     * // => 10
     *
     * _.sum({ 'a': 4, 'b': 6 });
     * // => 10
     *
     * var objects = [
     *   { 'n': 4 },
     *   { 'n': 6 }
     * ];
     *
     * _.sum(objects, function(object) {
     *   return object.n;
     * });
     * // => 10
     *
     * // using the `_.property` callback shorthand
     * _.sum(objects, 'n');
     * // => 10
     */
    function sum(collection, iteratee, thisArg) {
      if (thisArg && isIterateeCall(collection, iteratee, thisArg)) {
        iteratee = undefined;
      }
      iteratee = getCallback(iteratee, thisArg, 3);
      return iteratee.length == 1
        ? arraySum(isArray(collection) ? collection : toIterable(collection), iteratee)
        : baseSum(collection, iteratee);
    }

    /*------------------------------------------------------------------------*/

    // Ensure wrappers are instances of `baseLodash`.
    lodash.prototype = baseLodash.prototype;

    LodashWrapper.prototype = baseCreate(baseLodash.prototype);
    LodashWrapper.prototype.constructor = LodashWrapper;

    LazyWrapper.prototype = baseCreate(baseLodash.prototype);
    LazyWrapper.prototype.constructor = LazyWrapper;

    // Add functions to the `Map` cache.
    MapCache.prototype['delete'] = mapDelete;
    MapCache.prototype.get = mapGet;
    MapCache.prototype.has = mapHas;
    MapCache.prototype.set = mapSet;

    // Add functions to the `Set` cache.
    SetCache.prototype.push = cachePush;

    // Assign cache to `_.memoize`.
    memoize.Cache = MapCache;

    // Add functions that return wrapped values when chaining.
    lodash.after = after;
    lodash.ary = ary;
    lodash.assign = assign;
    lodash.at = at;
    lodash.before = before;
    lodash.bind = bind;
    lodash.bindAll = bindAll;
    lodash.bindKey = bindKey;
    lodash.callback = callback;
    lodash.chain = chain;
    lodash.chunk = chunk;
    lodash.compact = compact;
    lodash.constant = constant;
    lodash.countBy = countBy;
    lodash.create = create;
    lodash.curry = curry;
    lodash.curryRight = curryRight;
    lodash.debounce = debounce;
    lodash.defaults = defaults;
    lodash.defaultsDeep = defaultsDeep;
    lodash.defer = defer;
    lodash.delay = delay;
    lodash.difference = difference;
    lodash.drop = drop;
    lodash.dropRight = dropRight;
    lodash.dropRightWhile = dropRightWhile;
    lodash.dropWhile = dropWhile;
    lodash.fill = fill;
    lodash.filter = filter;
    lodash.flatten = flatten;
    lodash.flattenDeep = flattenDeep;
    lodash.flow = flow;
    lodash.flowRight = flowRight;
    lodash.forEach = forEach;
    lodash.forEachRight = forEachRight;
    lodash.forIn = forIn;
    lodash.forInRight = forInRight;
    lodash.forOwn = forOwn;
    lodash.forOwnRight = forOwnRight;
    lodash.functions = functions;
    lodash.groupBy = groupBy;
    lodash.indexBy = indexBy;
    lodash.initial = initial;
    lodash.intersection = intersection;
    lodash.invert = invert;
    lodash.invoke = invoke;
    lodash.keys = keys;
    lodash.keysIn = keysIn;
    lodash.map = map;
    lodash.mapKeys = mapKeys;
    lodash.mapValues = mapValues;
    lodash.matches = matches;
    lodash.matchesProperty = matchesProperty;
    lodash.memoize = memoize;
    lodash.merge = merge;
    lodash.method = method;
    lodash.methodOf = methodOf;
    lodash.mixin = mixin;
    lodash.modArgs = modArgs;
    lodash.negate = negate;
    lodash.omit = omit;
    lodash.once = once;
    lodash.pairs = pairs;
    lodash.partial = partial;
    lodash.partialRight = partialRight;
    lodash.partition = partition;
    lodash.pick = pick;
    lodash.pluck = pluck;
    lodash.property = property;
    lodash.propertyOf = propertyOf;
    lodash.pull = pull;
    lodash.pullAt = pullAt;
    lodash.range = range;
    lodash.rearg = rearg;
    lodash.reject = reject;
    lodash.remove = remove;
    lodash.rest = rest;
    lodash.restParam = restParam;
    lodash.set = set;
    lodash.shuffle = shuffle;
    lodash.slice = slice;
    lodash.sortBy = sortBy;
    lodash.sortByAll = sortByAll;
    lodash.sortByOrder = sortByOrder;
    lodash.spread = spread;
    lodash.take = take;
    lodash.takeRight = takeRight;
    lodash.takeRightWhile = takeRightWhile;
    lodash.takeWhile = takeWhile;
    lodash.tap = tap;
    lodash.throttle = throttle;
    lodash.thru = thru;
    lodash.times = times;
    lodash.toArray = toArray;
    lodash.toPlainObject = toPlainObject;
    lodash.transform = transform;
    lodash.union = union;
    lodash.uniq = uniq;
    lodash.unzip = unzip;
    lodash.unzipWith = unzipWith;
    lodash.values = values;
    lodash.valuesIn = valuesIn;
    lodash.where = where;
    lodash.without = without;
    lodash.wrap = wrap;
    lodash.xor = xor;
    lodash.zip = zip;
    lodash.zipObject = zipObject;
    lodash.zipWith = zipWith;

    // Add aliases.
    lodash.backflow = flowRight;
    lodash.collect = map;
    lodash.compose = flowRight;
    lodash.each = forEach;
    lodash.eachRight = forEachRight;
    lodash.extend = assign;
    lodash.iteratee = callback;
    lodash.methods = functions;
    lodash.object = zipObject;
    lodash.select = filter;
    lodash.tail = rest;
    lodash.unique = uniq;

    // Add functions to `lodash.prototype`.
    mixin(lodash, lodash);

    /*------------------------------------------------------------------------*/

    // Add functions that return unwrapped values when chaining.
    lodash.add = add;
    lodash.attempt = attempt;
    lodash.camelCase = camelCase;
    lodash.capitalize = capitalize;
    lodash.ceil = ceil;
    lodash.clone = clone;
    lodash.cloneDeep = cloneDeep;
    lodash.deburr = deburr;
    lodash.endsWith = endsWith;
    lodash.escape = escape;
    lodash.escapeRegExp = escapeRegExp;
    lodash.every = every;
    lodash.find = find;
    lodash.findIndex = findIndex;
    lodash.findKey = findKey;
    lodash.findLast = findLast;
    lodash.findLastIndex = findLastIndex;
    lodash.findLastKey = findLastKey;
    lodash.findWhere = findWhere;
    lodash.first = first;
    lodash.floor = floor;
    lodash.get = get;
    lodash.gt = gt;
    lodash.gte = gte;
    lodash.has = has;
    lodash.identity = identity;
    lodash.includes = includes;
    lodash.indexOf = indexOf;
    lodash.inRange = inRange;
    lodash.isArguments = isArguments;
    lodash.isArray = isArray;
    lodash.isBoolean = isBoolean;
    lodash.isDate = isDate;
    lodash.isElement = isElement;
    lodash.isEmpty = isEmpty;
    lodash.isEqual = isEqual;
    lodash.isError = isError;
    lodash.isFinite = isFinite;
    lodash.isFunction = isFunction;
    lodash.isMatch = isMatch;
    lodash.isNaN = isNaN;
    lodash.isNative = isNative;
    lodash.isNull = isNull;
    lodash.isNumber = isNumber;
    lodash.isObject = isObject;
    lodash.isPlainObject = isPlainObject;
    lodash.isRegExp = isRegExp;
    lodash.isString = isString;
    lodash.isTypedArray = isTypedArray;
    lodash.isUndefined = isUndefined;
    lodash.kebabCase = kebabCase;
    lodash.last = last;
    lodash.lastIndexOf = lastIndexOf;
    lodash.lt = lt;
    lodash.lte = lte;
    lodash.max = max;
    lodash.min = min;
    lodash.noConflict = noConflict;
    lodash.noop = noop;
    lodash.now = now;
    lodash.pad = pad;
    lodash.padLeft = padLeft;
    lodash.padRight = padRight;
    lodash.parseInt = parseInt;
    lodash.random = random;
    lodash.reduce = reduce;
    lodash.reduceRight = reduceRight;
    lodash.repeat = repeat;
    lodash.result = result;
    lodash.round = round;
    lodash.runInContext = runInContext;
    lodash.size = size;
    lodash.snakeCase = snakeCase;
    lodash.some = some;
    lodash.sortedIndex = sortedIndex;
    lodash.sortedLastIndex = sortedLastIndex;
    lodash.startCase = startCase;
    lodash.startsWith = startsWith;
    lodash.sum = sum;
    lodash.template = template;
    lodash.trim = trim;
    lodash.trimLeft = trimLeft;
    lodash.trimRight = trimRight;
    lodash.trunc = trunc;
    lodash.unescape = unescape;
    lodash.uniqueId = uniqueId;
    lodash.words = words;

    // Add aliases.
    lodash.all = every;
    lodash.any = some;
    lodash.contains = includes;
    lodash.eq = isEqual;
    lodash.detect = find;
    lodash.foldl = reduce;
    lodash.foldr = reduceRight;
    lodash.head = first;
    lodash.include = includes;
    lodash.inject = reduce;

    mixin(lodash, (function() {
      var source = {};
      baseForOwn(lodash, function(func, methodName) {
        if (!lodash.prototype[methodName]) {
          source[methodName] = func;
        }
      });
      return source;
    }()), false);

    /*------------------------------------------------------------------------*/

    // Add functions capable of returning wrapped and unwrapped values when chaining.
    lodash.sample = sample;

    lodash.prototype.sample = function(n) {
      if (!this.__chain__ && n == null) {
        return sample(this.value());
      }
      return this.thru(function(value) {
        return sample(value, n);
      });
    };

    /*------------------------------------------------------------------------*/

    /**
     * The semantic version number.
     *
     * @static
     * @memberOf _
     * @type string
     */
    lodash.VERSION = VERSION;

    // Assign default placeholders.
    arrayEach(['bind', 'bindKey', 'curry', 'curryRight', 'partial', 'partialRight'], function(methodName) {
      lodash[methodName].placeholder = lodash;
    });

    // Add `LazyWrapper` methods for `_.drop` and `_.take` variants.
    arrayEach(['drop', 'take'], function(methodName, index) {
      LazyWrapper.prototype[methodName] = function(n) {
        var filtered = this.__filtered__;
        if (filtered && !index) {
          return new LazyWrapper(this);
        }
        n = n == null ? 1 : nativeMax(nativeFloor(n) || 0, 0);

        var result = this.clone();
        if (filtered) {
          result.__takeCount__ = nativeMin(result.__takeCount__, n);
        } else {
          result.__views__.push({ 'size': n, 'type': methodName + (result.__dir__ < 0 ? 'Right' : '') });
        }
        return result;
      };

      LazyWrapper.prototype[methodName + 'Right'] = function(n) {
        return this.reverse()[methodName](n).reverse();
      };
    });

    // Add `LazyWrapper` methods that accept an `iteratee` value.
    arrayEach(['filter', 'map', 'takeWhile'], function(methodName, index) {
      var type = index + 1,
          isFilter = type != LAZY_MAP_FLAG;

      LazyWrapper.prototype[methodName] = function(iteratee, thisArg) {
        var result = this.clone();
        result.__iteratees__.push({ 'iteratee': getCallback(iteratee, thisArg, 1), 'type': type });
        result.__filtered__ = result.__filtered__ || isFilter;
        return result;
      };
    });

    // Add `LazyWrapper` methods for `_.first` and `_.last`.
    arrayEach(['first', 'last'], function(methodName, index) {
      var takeName = 'take' + (index ? 'Right' : '');

      LazyWrapper.prototype[methodName] = function() {
        return this[takeName](1).value()[0];
      };
    });

    // Add `LazyWrapper` methods for `_.initial` and `_.rest`.
    arrayEach(['initial', 'rest'], function(methodName, index) {
      var dropName = 'drop' + (index ? '' : 'Right');

      LazyWrapper.prototype[methodName] = function() {
        return this.__filtered__ ? new LazyWrapper(this) : this[dropName](1);
      };
    });

    // Add `LazyWrapper` methods for `_.pluck` and `_.where`.
    arrayEach(['pluck', 'where'], function(methodName, index) {
      var operationName = index ? 'filter' : 'map',
          createCallback = index ? baseMatches : property;

      LazyWrapper.prototype[methodName] = function(value) {
        return this[operationName](createCallback(value));
      };
    });

    LazyWrapper.prototype.compact = function() {
      return this.filter(identity);
    };

    LazyWrapper.prototype.reject = function(predicate, thisArg) {
      predicate = getCallback(predicate, thisArg, 1);
      return this.filter(function(value) {
        return !predicate(value);
      });
    };

    LazyWrapper.prototype.slice = function(start, end) {
      start = start == null ? 0 : (+start || 0);

      var result = this;
      if (result.__filtered__ && (start > 0 || end < 0)) {
        return new LazyWrapper(result);
      }
      if (start < 0) {
        result = result.takeRight(-start);
      } else if (start) {
        result = result.drop(start);
      }
      if (end !== undefined) {
        end = (+end || 0);
        result = end < 0 ? result.dropRight(-end) : result.take(end - start);
      }
      return result;
    };

    LazyWrapper.prototype.takeRightWhile = function(predicate, thisArg) {
      return this.reverse().takeWhile(predicate, thisArg).reverse();
    };

    LazyWrapper.prototype.toArray = function() {
      return this.take(POSITIVE_INFINITY);
    };

    // Add `LazyWrapper` methods to `lodash.prototype`.
    baseForOwn(LazyWrapper.prototype, function(func, methodName) {
      var checkIteratee = /^(?:filter|map|reject)|While$/.test(methodName),
          retUnwrapped = /^(?:first|last)$/.test(methodName),
          lodashFunc = lodash[retUnwrapped ? ('take' + (methodName == 'last' ? 'Right' : '')) : methodName];

      if (!lodashFunc) {
        return;
      }
      lodash.prototype[methodName] = function() {
        var args = retUnwrapped ? [1] : arguments,
            chainAll = this.__chain__,
            value = this.__wrapped__,
            isHybrid = !!this.__actions__.length,
            isLazy = value instanceof LazyWrapper,
            iteratee = args[0],
            useLazy = isLazy || isArray(value);

        if (useLazy && checkIteratee && typeof iteratee == 'function' && iteratee.length != 1) {
          // Avoid lazy use if the iteratee has a "length" value other than `1`.
          isLazy = useLazy = false;
        }
        var interceptor = function(value) {
          return (retUnwrapped && chainAll)
            ? lodashFunc(value, 1)[0]
            : lodashFunc.apply(undefined, arrayPush([value], args));
        };

        var action = { 'func': thru, 'args': [interceptor], 'thisArg': undefined },
            onlyLazy = isLazy && !isHybrid;

        if (retUnwrapped && !chainAll) {
          if (onlyLazy) {
            value = value.clone();
            value.__actions__.push(action);
            return func.call(value);
          }
          return lodashFunc.call(undefined, this.value())[0];
        }
        if (!retUnwrapped && useLazy) {
          value = onlyLazy ? value : new LazyWrapper(this);
          var result = func.apply(value, args);
          result.__actions__.push(action);
          return new LodashWrapper(result, chainAll);
        }
        return this.thru(interceptor);
      };
    });

    // Add `Array` and `String` methods to `lodash.prototype`.
    arrayEach(['join', 'pop', 'push', 'replace', 'shift', 'sort', 'splice', 'split', 'unshift'], function(methodName) {
      var func = (/^(?:replace|split)$/.test(methodName) ? stringProto : arrayProto)[methodName],
          chainName = /^(?:push|sort|unshift)$/.test(methodName) ? 'tap' : 'thru',
          retUnwrapped = /^(?:join|pop|replace|shift)$/.test(methodName);

      lodash.prototype[methodName] = function() {
        var args = arguments;
        if (retUnwrapped && !this.__chain__) {
          return func.apply(this.value(), args);
        }
        return this[chainName](function(value) {
          return func.apply(value, args);
        });
      };
    });

    // Map minified function names to their real names.
    baseForOwn(LazyWrapper.prototype, function(func, methodName) {
      var lodashFunc = lodash[methodName];
      if (lodashFunc) {
        var key = lodashFunc.name,
            names = realNames[key] || (realNames[key] = []);

        names.push({ 'name': methodName, 'func': lodashFunc });
      }
    });

    realNames[createHybridWrapper(undefined, BIND_KEY_FLAG).name] = [{ 'name': 'wrapper', 'func': undefined }];

    // Add functions to the lazy wrapper.
    LazyWrapper.prototype.clone = lazyClone;
    LazyWrapper.prototype.reverse = lazyReverse;
    LazyWrapper.prototype.value = lazyValue;

    // Add chaining functions to the `lodash` wrapper.
    lodash.prototype.chain = wrapperChain;
    lodash.prototype.commit = wrapperCommit;
    lodash.prototype.concat = wrapperConcat;
    lodash.prototype.plant = wrapperPlant;
    lodash.prototype.reverse = wrapperReverse;
    lodash.prototype.toString = wrapperToString;
    lodash.prototype.run = lodash.prototype.toJSON = lodash.prototype.valueOf = lodash.prototype.value = wrapperValue;

    // Add function aliases to the `lodash` wrapper.
    lodash.prototype.collect = lodash.prototype.map;
    lodash.prototype.head = lodash.prototype.first;
    lodash.prototype.select = lodash.prototype.filter;
    lodash.prototype.tail = lodash.prototype.rest;

    return lodash;
  }

  /*--------------------------------------------------------------------------*/

  // Export lodash.
  var _ = runInContext();

  // Some AMD build optimizers like r.js check for condition patterns like the following:
  if (typeof define == 'function' && typeof define.amd == 'object' && define.amd) {
    // Expose lodash to the global object when an AMD loader is present to avoid
    // errors in cases where lodash is loaded by a script tag and not intended
    // as an AMD module. See http://requirejs.org/docs/errors.html#mismatch for
    // more details.
    root._ = _;

    // Define as an anonymous module so, through path mapping, it can be
    // referenced as the "underscore" module.
    define(function() {
      return _;
    });
  }
  // Check for `exports` after `define` in case a build optimizer adds an `exports` object.
  else if (freeExports && freeModule) {
    // Export for Node.js or RingoJS.
    if (moduleExports) {
      (freeModule.exports = _)._ = _;
    }
    // Export for Rhino with CommonJS support.
    else {
      freeExports._ = _;
    }
  }
  else {
    // Export for a browser or Rhino.
    root._ = _;
  }
}.call(this));

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],59:[function(require,module,exports){
'use strict';

module.exports = require('./lib')

},{"./lib":64}],60:[function(require,module,exports){
'use strict';

var asap = require('asap/raw');

function noop() {}

// States:
//
// 0 - pending
// 1 - fulfilled with _value
// 2 - rejected with _value
// 3 - adopted the state of another promise, _value
//
// once the state is no longer pending (0) it is immutable

// All `_` prefixed properties will be reduced to `_{random number}`
// at build time to obfuscate them and discourage their use.
// We don't use symbols or Object.defineProperty to fully hide them
// because the performance isn't good enough.


// to avoid using try/catch inside critical functions, we
// extract them to here.
var LAST_ERROR = null;
var IS_ERROR = {};
function getThen(obj) {
  try {
    return obj.then;
  } catch (ex) {
    LAST_ERROR = ex;
    return IS_ERROR;
  }
}

function tryCallOne(fn, a) {
  try {
    return fn(a);
  } catch (ex) {
    LAST_ERROR = ex;
    return IS_ERROR;
  }
}
function tryCallTwo(fn, a, b) {
  try {
    fn(a, b);
  } catch (ex) {
    LAST_ERROR = ex;
    return IS_ERROR;
  }
}

module.exports = Promise;

function Promise(fn) {
  if (typeof this !== 'object') {
    throw new TypeError('Promises must be constructed via new');
  }
  if (typeof fn !== 'function') {
    throw new TypeError('Promise constructor\'s argument is not a function');
  }
  this._40 = 0;
  this._65 = 0;
  this._55 = null;
  this._72 = null;
  if (fn === noop) return;
  doResolve(fn, this);
}
Promise._37 = null;
Promise._87 = null;
Promise._61 = noop;

Promise.prototype.then = function(onFulfilled, onRejected) {
  if (this.constructor !== Promise) {
    return safeThen(this, onFulfilled, onRejected);
  }
  var res = new Promise(noop);
  handle(this, new Handler(onFulfilled, onRejected, res));
  return res;
};

function safeThen(self, onFulfilled, onRejected) {
  return new self.constructor(function (resolve, reject) {
    var res = new Promise(noop);
    res.then(resolve, reject);
    handle(self, new Handler(onFulfilled, onRejected, res));
  });
}
function handle(self, deferred) {
  while (self._65 === 3) {
    self = self._55;
  }
  if (Promise._37) {
    Promise._37(self);
  }
  if (self._65 === 0) {
    if (self._40 === 0) {
      self._40 = 1;
      self._72 = deferred;
      return;
    }
    if (self._40 === 1) {
      self._40 = 2;
      self._72 = [self._72, deferred];
      return;
    }
    self._72.push(deferred);
    return;
  }
  handleResolved(self, deferred);
}

function handleResolved(self, deferred) {
  asap(function() {
    var cb = self._65 === 1 ? deferred.onFulfilled : deferred.onRejected;
    if (cb === null) {
      if (self._65 === 1) {
        resolve(deferred.promise, self._55);
      } else {
        reject(deferred.promise, self._55);
      }
      return;
    }
    var ret = tryCallOne(cb, self._55);
    if (ret === IS_ERROR) {
      reject(deferred.promise, LAST_ERROR);
    } else {
      resolve(deferred.promise, ret);
    }
  });
}
function resolve(self, newValue) {
  // Promise Resolution Procedure: https://github.com/promises-aplus/promises-spec#the-promise-resolution-procedure
  if (newValue === self) {
    return reject(
      self,
      new TypeError('A promise cannot be resolved with itself.')
    );
  }
  if (
    newValue &&
    (typeof newValue === 'object' || typeof newValue === 'function')
  ) {
    var then = getThen(newValue);
    if (then === IS_ERROR) {
      return reject(self, LAST_ERROR);
    }
    if (
      then === self.then &&
      newValue instanceof Promise
    ) {
      self._65 = 3;
      self._55 = newValue;
      finale(self);
      return;
    } else if (typeof then === 'function') {
      doResolve(then.bind(newValue), self);
      return;
    }
  }
  self._65 = 1;
  self._55 = newValue;
  finale(self);
}

function reject(self, newValue) {
  self._65 = 2;
  self._55 = newValue;
  if (Promise._87) {
    Promise._87(self, newValue);
  }
  finale(self);
}
function finale(self) {
  if (self._40 === 1) {
    handle(self, self._72);
    self._72 = null;
  }
  if (self._40 === 2) {
    for (var i = 0; i < self._72.length; i++) {
      handle(self, self._72[i]);
    }
    self._72 = null;
  }
}

function Handler(onFulfilled, onRejected, promise){
  this.onFulfilled = typeof onFulfilled === 'function' ? onFulfilled : null;
  this.onRejected = typeof onRejected === 'function' ? onRejected : null;
  this.promise = promise;
}

/**
 * Take a potentially misbehaving resolver function and make sure
 * onFulfilled and onRejected are only called once.
 *
 * Makes no guarantees about asynchrony.
 */
function doResolve(fn, promise) {
  var done = false;
  var res = tryCallTwo(fn, function (value) {
    if (done) return;
    done = true;
    resolve(promise, value);
  }, function (reason) {
    if (done) return;
    done = true;
    reject(promise, reason);
  });
  if (!done && res === IS_ERROR) {
    done = true;
    reject(promise, LAST_ERROR);
  }
}

},{"asap/raw":55}],61:[function(require,module,exports){
'use strict';

var Promise = require('./core.js');

module.exports = Promise;
Promise.prototype.done = function (onFulfilled, onRejected) {
  var self = arguments.length ? this.then.apply(this, arguments) : this;
  self.then(null, function (err) {
    setTimeout(function () {
      throw err;
    }, 0);
  });
};

},{"./core.js":60}],62:[function(require,module,exports){
'use strict';

//This file contains the ES6 extensions to the core Promises/A+ API

var Promise = require('./core.js');

module.exports = Promise;

/* Static Functions */

var TRUE = valuePromise(true);
var FALSE = valuePromise(false);
var NULL = valuePromise(null);
var UNDEFINED = valuePromise(undefined);
var ZERO = valuePromise(0);
var EMPTYSTRING = valuePromise('');

function valuePromise(value) {
  var p = new Promise(Promise._61);
  p._65 = 1;
  p._55 = value;
  return p;
}
Promise.resolve = function (value) {
  if (value instanceof Promise) return value;

  if (value === null) return NULL;
  if (value === undefined) return UNDEFINED;
  if (value === true) return TRUE;
  if (value === false) return FALSE;
  if (value === 0) return ZERO;
  if (value === '') return EMPTYSTRING;

  if (typeof value === 'object' || typeof value === 'function') {
    try {
      var then = value.then;
      if (typeof then === 'function') {
        return new Promise(then.bind(value));
      }
    } catch (ex) {
      return new Promise(function (resolve, reject) {
        reject(ex);
      });
    }
  }
  return valuePromise(value);
};

Promise.all = function (arr) {
  var args = Array.prototype.slice.call(arr);

  return new Promise(function (resolve, reject) {
    if (args.length === 0) return resolve([]);
    var remaining = args.length;
    function res(i, val) {
      if (val && (typeof val === 'object' || typeof val === 'function')) {
        if (val instanceof Promise && val.then === Promise.prototype.then) {
          while (val._65 === 3) {
            val = val._55;
          }
          if (val._65 === 1) return res(i, val._55);
          if (val._65 === 2) reject(val._55);
          val.then(function (val) {
            res(i, val);
          }, reject);
          return;
        } else {
          var then = val.then;
          if (typeof then === 'function') {
            var p = new Promise(then.bind(val));
            p.then(function (val) {
              res(i, val);
            }, reject);
            return;
          }
        }
      }
      args[i] = val;
      if (--remaining === 0) {
        resolve(args);
      }
    }
    for (var i = 0; i < args.length; i++) {
      res(i, args[i]);
    }
  });
};

Promise.reject = function (value) {
  return new Promise(function (resolve, reject) {
    reject(value);
  });
};

Promise.race = function (values) {
  return new Promise(function (resolve, reject) {
    values.forEach(function(value){
      Promise.resolve(value).then(resolve, reject);
    });
  });
};

/* Prototype Methods */

Promise.prototype['catch'] = function (onRejected) {
  return this.then(null, onRejected);
};

},{"./core.js":60}],63:[function(require,module,exports){
'use strict';

var Promise = require('./core.js');

module.exports = Promise;
Promise.prototype['finally'] = function (f) {
  return this.then(function (value) {
    return Promise.resolve(f()).then(function () {
      return value;
    });
  }, function (err) {
    return Promise.resolve(f()).then(function () {
      throw err;
    });
  });
};

},{"./core.js":60}],64:[function(require,module,exports){
'use strict';

module.exports = require('./core.js');
require('./done.js');
require('./finally.js');
require('./es6-extensions.js');
require('./node-extensions.js');
require('./synchronous.js');

},{"./core.js":60,"./done.js":61,"./es6-extensions.js":62,"./finally.js":63,"./node-extensions.js":65,"./synchronous.js":66}],65:[function(require,module,exports){
'use strict';

// This file contains then/promise specific extensions that are only useful
// for node.js interop

var Promise = require('./core.js');
var asap = require('asap');

module.exports = Promise;

/* Static Functions */

Promise.denodeify = function (fn, argumentCount) {
  if (
    typeof argumentCount === 'number' && argumentCount !== Infinity
  ) {
    return denodeifyWithCount(fn, argumentCount);
  } else {
    return denodeifyWithoutCount(fn);
  }
};

var callbackFn = (
  'function (err, res) {' +
  'if (err) { rj(err); } else { rs(res); }' +
  '}'
);
function denodeifyWithCount(fn, argumentCount) {
  var args = [];
  for (var i = 0; i < argumentCount; i++) {
    args.push('a' + i);
  }
  var body = [
    'return function (' + args.join(',') + ') {',
    'var self = this;',
    'return new Promise(function (rs, rj) {',
    'var res = fn.call(',
    ['self'].concat(args).concat([callbackFn]).join(','),
    ');',
    'if (res &&',
    '(typeof res === "object" || typeof res === "function") &&',
    'typeof res.then === "function"',
    ') {rs(res);}',
    '});',
    '};'
  ].join('');
  return Function(['Promise', 'fn'], body)(Promise, fn);
}
function denodeifyWithoutCount(fn) {
  var fnLength = Math.max(fn.length - 1, 3);
  var args = [];
  for (var i = 0; i < fnLength; i++) {
    args.push('a' + i);
  }
  var body = [
    'return function (' + args.join(',') + ') {',
    'var self = this;',
    'var args;',
    'var argLength = arguments.length;',
    'if (arguments.length > ' + fnLength + ') {',
    'args = new Array(arguments.length + 1);',
    'for (var i = 0; i < arguments.length; i++) {',
    'args[i] = arguments[i];',
    '}',
    '}',
    'return new Promise(function (rs, rj) {',
    'var cb = ' + callbackFn + ';',
    'var res;',
    'switch (argLength) {',
    args.concat(['extra']).map(function (_, index) {
      return (
        'case ' + (index) + ':' +
        'res = fn.call(' + ['self'].concat(args.slice(0, index)).concat('cb').join(',') + ');' +
        'break;'
      );
    }).join(''),
    'default:',
    'args[argLength] = cb;',
    'res = fn.apply(self, args);',
    '}',
    
    'if (res &&',
    '(typeof res === "object" || typeof res === "function") &&',
    'typeof res.then === "function"',
    ') {rs(res);}',
    '});',
    '};'
  ].join('');

  return Function(
    ['Promise', 'fn'],
    body
  )(Promise, fn);
}

Promise.nodeify = function (fn) {
  return function () {
    var args = Array.prototype.slice.call(arguments);
    var callback =
      typeof args[args.length - 1] === 'function' ? args.pop() : null;
    var ctx = this;
    try {
      return fn.apply(this, arguments).nodeify(callback, ctx);
    } catch (ex) {
      if (callback === null || typeof callback == 'undefined') {
        return new Promise(function (resolve, reject) {
          reject(ex);
        });
      } else {
        asap(function () {
          callback.call(ctx, ex);
        })
      }
    }
  }
};

Promise.prototype.nodeify = function (callback, ctx) {
  if (typeof callback != 'function') return this;

  this.then(function (value) {
    asap(function () {
      callback.call(ctx, null, value);
    });
  }, function (err) {
    asap(function () {
      callback.call(ctx, err);
    });
  });
};

},{"./core.js":60,"asap":54}],66:[function(require,module,exports){
'use strict';

var Promise = require('./core.js');

module.exports = Promise;
Promise.enableSynchronous = function () {
  Promise.prototype.isPending = function() {
    return this.getState() == 0;
  };

  Promise.prototype.isFulfilled = function() {
    return this.getState() == 1;
  };

  Promise.prototype.isRejected = function() {
    return this.getState() == 2;
  };

  Promise.prototype.getValue = function () {
    if (this._65 === 3) {
      return this._55.getValue();
    }

    if (!this.isFulfilled()) {
      throw new Error('Cannot get a value of an unfulfilled promise.');
    }

    return this._55;
  };

  Promise.prototype.getReason = function () {
    if (this._65 === 3) {
      return this._55.getReason();
    }

    if (!this.isRejected()) {
      throw new Error('Cannot get a rejection reason of a non-rejected promise.');
    }

    return this._55;
  };

  Promise.prototype.getState = function () {
    if (this._65 === 3) {
      return this._55.getState();
    }
    if (this._65 === -1 || this._65 === -2) {
      return 0;
    }

    return this._65;
  };
};

Promise.disableSynchronous = function() {
  Promise.prototype.isPending = undefined;
  Promise.prototype.isFulfilled = undefined;
  Promise.prototype.isRejected = undefined;
  Promise.prototype.getValue = undefined;
  Promise.prototype.getReason = undefined;
  Promise.prototype.getState = undefined;
};

},{"./core.js":60}],67:[function(require,module,exports){
/*!
 * EventEmitter v4.2.11 - git.io/ee
 * Unlicense - http://unlicense.org/
 * Oliver Caldwell - http://oli.me.uk/
 * @preserve
 */

;(function () {
    'use strict';

    /**
     * Class for managing events.
     * Can be extended to provide event functionality in other classes.
     *
     * @class EventEmitter Manages event registering and emitting.
     */
    function EventEmitter() {}

    // Shortcuts to improve speed and size
    var proto = EventEmitter.prototype;
    var exports = this;
    var originalGlobalValue = exports.EventEmitter;

    /**
     * Finds the index of the listener for the event in its storage array.
     *
     * @param {Function[]} listeners Array of listeners to search through.
     * @param {Function} listener Method to look for.
     * @return {Number} Index of the specified listener, -1 if not found
     * @api private
     */
    function indexOfListener(listeners, listener) {
        var i = listeners.length;
        while (i--) {
            if (listeners[i].listener === listener) {
                return i;
            }
        }

        return -1;
    }

    /**
     * Alias a method while keeping the context correct, to allow for overwriting of target method.
     *
     * @param {String} name The name of the target method.
     * @return {Function} The aliased method
     * @api private
     */
    function alias(name) {
        return function aliasClosure() {
            return this[name].apply(this, arguments);
        };
    }

    /**
     * Returns the listener array for the specified event.
     * Will initialise the event object and listener arrays if required.
     * Will return an object if you use a regex search. The object contains keys for each matched event. So /ba[rz]/ might return an object containing bar and baz. But only if you have either defined them with defineEvent or added some listeners to them.
     * Each property in the object response is an array of listener functions.
     *
     * @param {String|RegExp} evt Name of the event to return the listeners from.
     * @return {Function[]|Object} All listener functions for the event.
     */
    proto.getListeners = function getListeners(evt) {
        var events = this._getEvents();
        var response;
        var key;

        // Return a concatenated array of all matching events if
        // the selector is a regular expression.
        if (evt instanceof RegExp) {
            response = {};
            for (key in events) {
                if (events.hasOwnProperty(key) && evt.test(key)) {
                    response[key] = events[key];
                }
            }
        }
        else {
            response = events[evt] || (events[evt] = []);
        }

        return response;
    };

    /**
     * Takes a list of listener objects and flattens it into a list of listener functions.
     *
     * @param {Object[]} listeners Raw listener objects.
     * @return {Function[]} Just the listener functions.
     */
    proto.flattenListeners = function flattenListeners(listeners) {
        var flatListeners = [];
        var i;

        for (i = 0; i < listeners.length; i += 1) {
            flatListeners.push(listeners[i].listener);
        }

        return flatListeners;
    };

    /**
     * Fetches the requested listeners via getListeners but will always return the results inside an object. This is mainly for internal use but others may find it useful.
     *
     * @param {String|RegExp} evt Name of the event to return the listeners from.
     * @return {Object} All listener functions for an event in an object.
     */
    proto.getListenersAsObject = function getListenersAsObject(evt) {
        var listeners = this.getListeners(evt);
        var response;

        if (listeners instanceof Array) {
            response = {};
            response[evt] = listeners;
        }

        return response || listeners;
    };

    /**
     * Adds a listener function to the specified event.
     * The listener will not be added if it is a duplicate.
     * If the listener returns true then it will be removed after it is called.
     * If you pass a regular expression as the event name then the listener will be added to all events that match it.
     *
     * @param {String|RegExp} evt Name of the event to attach the listener to.
     * @param {Function} listener Method to be called when the event is emitted. If the function returns true then it will be removed after calling.
     * @return {Object} Current instance of EventEmitter for chaining.
     */
    proto.addListener = function addListener(evt, listener) {
        var listeners = this.getListenersAsObject(evt);
        var listenerIsWrapped = typeof listener === 'object';
        var key;

        for (key in listeners) {
            if (listeners.hasOwnProperty(key) && indexOfListener(listeners[key], listener) === -1) {
                listeners[key].push(listenerIsWrapped ? listener : {
                    listener: listener,
                    once: false
                });
            }
        }

        return this;
    };

    /**
     * Alias of addListener
     */
    proto.on = alias('addListener');

    /**
     * Semi-alias of addListener. It will add a listener that will be
     * automatically removed after its first execution.
     *
     * @param {String|RegExp} evt Name of the event to attach the listener to.
     * @param {Function} listener Method to be called when the event is emitted. If the function returns true then it will be removed after calling.
     * @return {Object} Current instance of EventEmitter for chaining.
     */
    proto.addOnceListener = function addOnceListener(evt, listener) {
        return this.addListener(evt, {
            listener: listener,
            once: true
        });
    };

    /**
     * Alias of addOnceListener.
     */
    proto.once = alias('addOnceListener');

    /**
     * Defines an event name. This is required if you want to use a regex to add a listener to multiple events at once. If you don't do this then how do you expect it to know what event to add to? Should it just add to every possible match for a regex? No. That is scary and bad.
     * You need to tell it what event names should be matched by a regex.
     *
     * @param {String} evt Name of the event to create.
     * @return {Object} Current instance of EventEmitter for chaining.
     */
    proto.defineEvent = function defineEvent(evt) {
        this.getListeners(evt);
        return this;
    };

    /**
     * Uses defineEvent to define multiple events.
     *
     * @param {String[]} evts An array of event names to define.
     * @return {Object} Current instance of EventEmitter for chaining.
     */
    proto.defineEvents = function defineEvents(evts) {
        for (var i = 0; i < evts.length; i += 1) {
            this.defineEvent(evts[i]);
        }
        return this;
    };

    /**
     * Removes a listener function from the specified event.
     * When passed a regular expression as the event name, it will remove the listener from all events that match it.
     *
     * @param {String|RegExp} evt Name of the event to remove the listener from.
     * @param {Function} listener Method to remove from the event.
     * @return {Object} Current instance of EventEmitter for chaining.
     */
    proto.removeListener = function removeListener(evt, listener) {
        var listeners = this.getListenersAsObject(evt);
        var index;
        var key;

        for (key in listeners) {
            if (listeners.hasOwnProperty(key)) {
                index = indexOfListener(listeners[key], listener);

                if (index !== -1) {
                    listeners[key].splice(index, 1);
                }
            }
        }

        return this;
    };

    /**
     * Alias of removeListener
     */
    proto.off = alias('removeListener');

    /**
     * Adds listeners in bulk using the manipulateListeners method.
     * If you pass an object as the second argument you can add to multiple events at once. The object should contain key value pairs of events and listeners or listener arrays. You can also pass it an event name and an array of listeners to be added.
     * You can also pass it a regular expression to add the array of listeners to all events that match it.
     * Yeah, this function does quite a bit. That's probably a bad thing.
     *
     * @param {String|Object|RegExp} evt An event name if you will pass an array of listeners next. An object if you wish to add to multiple events at once.
     * @param {Function[]} [listeners] An optional array of listener functions to add.
     * @return {Object} Current instance of EventEmitter for chaining.
     */
    proto.addListeners = function addListeners(evt, listeners) {
        // Pass through to manipulateListeners
        return this.manipulateListeners(false, evt, listeners);
    };

    /**
     * Removes listeners in bulk using the manipulateListeners method.
     * If you pass an object as the second argument you can remove from multiple events at once. The object should contain key value pairs of events and listeners or listener arrays.
     * You can also pass it an event name and an array of listeners to be removed.
     * You can also pass it a regular expression to remove the listeners from all events that match it.
     *
     * @param {String|Object|RegExp} evt An event name if you will pass an array of listeners next. An object if you wish to remove from multiple events at once.
     * @param {Function[]} [listeners] An optional array of listener functions to remove.
     * @return {Object} Current instance of EventEmitter for chaining.
     */
    proto.removeListeners = function removeListeners(evt, listeners) {
        // Pass through to manipulateListeners
        return this.manipulateListeners(true, evt, listeners);
    };

    /**
     * Edits listeners in bulk. The addListeners and removeListeners methods both use this to do their job. You should really use those instead, this is a little lower level.
     * The first argument will determine if the listeners are removed (true) or added (false).
     * If you pass an object as the second argument you can add/remove from multiple events at once. The object should contain key value pairs of events and listeners or listener arrays.
     * You can also pass it an event name and an array of listeners to be added/removed.
     * You can also pass it a regular expression to manipulate the listeners of all events that match it.
     *
     * @param {Boolean} remove True if you want to remove listeners, false if you want to add.
     * @param {String|Object|RegExp} evt An event name if you will pass an array of listeners next. An object if you wish to add/remove from multiple events at once.
     * @param {Function[]} [listeners] An optional array of listener functions to add/remove.
     * @return {Object} Current instance of EventEmitter for chaining.
     */
    proto.manipulateListeners = function manipulateListeners(remove, evt, listeners) {
        var i;
        var value;
        var single = remove ? this.removeListener : this.addListener;
        var multiple = remove ? this.removeListeners : this.addListeners;

        // If evt is an object then pass each of its properties to this method
        if (typeof evt === 'object' && !(evt instanceof RegExp)) {
            for (i in evt) {
                if (evt.hasOwnProperty(i) && (value = evt[i])) {
                    // Pass the single listener straight through to the singular method
                    if (typeof value === 'function') {
                        single.call(this, i, value);
                    }
                    else {
                        // Otherwise pass back to the multiple function
                        multiple.call(this, i, value);
                    }
                }
            }
        }
        else {
            // So evt must be a string
            // And listeners must be an array of listeners
            // Loop over it and pass each one to the multiple method
            i = listeners.length;
            while (i--) {
                single.call(this, evt, listeners[i]);
            }
        }

        return this;
    };

    /**
     * Removes all listeners from a specified event.
     * If you do not specify an event then all listeners will be removed.
     * That means every event will be emptied.
     * You can also pass a regex to remove all events that match it.
     *
     * @param {String|RegExp} [evt] Optional name of the event to remove all listeners for. Will remove from every event if not passed.
     * @return {Object} Current instance of EventEmitter for chaining.
     */
    proto.removeEvent = function removeEvent(evt) {
        var type = typeof evt;
        var events = this._getEvents();
        var key;

        // Remove different things depending on the state of evt
        if (type === 'string') {
            // Remove all listeners for the specified event
            delete events[evt];
        }
        else if (evt instanceof RegExp) {
            // Remove all events matching the regex.
            for (key in events) {
                if (events.hasOwnProperty(key) && evt.test(key)) {
                    delete events[key];
                }
            }
        }
        else {
            // Remove all listeners in all events
            delete this._events;
        }

        return this;
    };

    /**
     * Alias of removeEvent.
     *
     * Added to mirror the node API.
     */
    proto.removeAllListeners = alias('removeEvent');

    /**
     * Emits an event of your choice.
     * When emitted, every listener attached to that event will be executed.
     * If you pass the optional argument array then those arguments will be passed to every listener upon execution.
     * Because it uses `apply`, your array of arguments will be passed as if you wrote them out separately.
     * So they will not arrive within the array on the other side, they will be separate.
     * You can also pass a regular expression to emit to all events that match it.
     *
     * @param {String|RegExp} evt Name of the event to emit and execute listeners for.
     * @param {Array} [args] Optional array of arguments to be passed to each listener.
     * @return {Object} Current instance of EventEmitter for chaining.
     */
    proto.emitEvent = function emitEvent(evt, args) {
        var listenersMap = this.getListenersAsObject(evt);
        var listeners;
        var listener;
        var i;
        var key;
        var response;

        for (key in listenersMap) {
            if (listenersMap.hasOwnProperty(key)) {
                listeners = listenersMap[key].slice(0);
                i = listeners.length;

                while (i--) {
                    // If the listener returns true then it shall be removed from the event
                    // The function is executed either with a basic call or an apply if there is an args array
                    listener = listeners[i];

                    if (listener.once === true) {
                        this.removeListener(evt, listener.listener);
                    }

                    response = listener.listener.apply(this, args || []);

                    if (response === this._getOnceReturnValue()) {
                        this.removeListener(evt, listener.listener);
                    }
                }
            }
        }

        return this;
    };

    /**
     * Alias of emitEvent
     */
    proto.trigger = alias('emitEvent');

    /**
     * Subtly different from emitEvent in that it will pass its arguments on to the listeners, as opposed to taking a single array of arguments to pass on.
     * As with emitEvent, you can pass a regex in place of the event name to emit to all events that match it.
     *
     * @param {String|RegExp} evt Name of the event to emit and execute listeners for.
     * @param {...*} Optional additional arguments to be passed to each listener.
     * @return {Object} Current instance of EventEmitter for chaining.
     */
    proto.emit = function emit(evt) {
        var args = Array.prototype.slice.call(arguments, 1);
        return this.emitEvent(evt, args);
    };

    /**
     * Sets the current value to check against when executing listeners. If a
     * listeners return value matches the one set here then it will be removed
     * after execution. This value defaults to true.
     *
     * @param {*} value The new value to check for when executing listeners.
     * @return {Object} Current instance of EventEmitter for chaining.
     */
    proto.setOnceReturnValue = function setOnceReturnValue(value) {
        this._onceReturnValue = value;
        return this;
    };

    /**
     * Fetches the current value to check against when executing listeners. If
     * the listeners return value matches this one then it should be removed
     * automatically. It will return true by default.
     *
     * @return {*|Boolean} The current value to check for or the default, true.
     * @api private
     */
    proto._getOnceReturnValue = function _getOnceReturnValue() {
        if (this.hasOwnProperty('_onceReturnValue')) {
            return this._onceReturnValue;
        }
        else {
            return true;
        }
    };

    /**
     * Fetches the events object and creates one if required.
     *
     * @return {Object} The events storage object.
     * @api private
     */
    proto._getEvents = function _getEvents() {
        return this._events || (this._events = {});
    };

    /**
     * Reverts the global {@link EventEmitter} to its previous value and returns a reference to this version.
     *
     * @return {Function} Non conflicting EventEmitter class.
     */
    EventEmitter.noConflict = function noConflict() {
        exports.EventEmitter = originalGlobalValue;
        return EventEmitter;
    };

    // Expose the class either via AMD, CommonJS or the global object
    if (typeof define === 'function' && define.amd) {
        define(function () {
            return EventEmitter;
        });
    }
    else if (typeof module === 'object' && module.exports){
        module.exports = EventEmitter;
    }
    else {
        exports.EventEmitter = EventEmitter;
    }
}.call(this));

},{}]},{},[2]);

//# sourceMappingURL=app.js.map
