'use strict';

/**
 * Controller that performs basket actions for calls from IOS Application
 *
 * @module controllers/AppWeb
 */

var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');  

var Logger = require('dw/system/Logger');
var Transaction = require('dw/system/Transaction');
var URLUtils = require('dw/web/URLUtils');
var ArrayList = require('dw/util/ArrayList');
var HashMap = require('dw/util/HashMap');
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var Money = require('dw/value/Money');
var regex = {
    numeric : /[\~\`\!\@\#\$\%\^\&\*\(\)\-\_\+\.\=\[\]\{\}\\\|\;\:\'\"\,\<\>\?\/A-Za-z]/g
};

function basketRequest() {
	
	var Product = app.getModel('Product');
	var basket = app.getModel('Cart').goc();
	var basketObj = basket.object;
	var maxOrderQty = 50;
	
	// Validate Request Body
	var params = request.httpParameterMap;
	var requestBody = params.requestBodyAsString;
	var requestBodyJSON = validateRequest(requestBody);
	
	// Request Variables
	var requestJSON;
	var basketRequest, reqActionType, reqBasketId, reqSessionId, requestItems;
	var requestItemSKUs, reqSkuQtyMap;
	var isRemoveBasket = true;
	if(requestBodyJSON.valid){
		requestJSON = requestBodyJSON.requestJSON;
		basketRequest = requestJSON.BasketRequest;
		reqActionType = basketRequest.ActionType;
		reqBasketId = basketRequest.BasketId;
		reqSessionId = basketRequest.SessionId;
		requestItems = basketRequest.RequestItems;
		requestItemSKUs = new ArrayList();
		reqSkuQtyMap = new HashMap();
		for each (var requestItem in requestItems){
			requestItemSKUs.add(requestItem.SKU);
			reqSkuQtyMap.put(requestItem.SKU,parseInt(requestItem.Quantity));
		}
	} else {
		let r = require('~/cartridge/scripts/util/Response');
        return r.renderJSON({
        	success: false,
            message: requestBodyJSON.message
        });
	}
	
	// Response Variables
	var responseJSON;
	var basketResponse, resActionType, resBasketId, resSessionId, basketItems, basketTotal;
	
	// Create a basket with existing basket data from custom object
	var storedBasketObject = null;
	var storedBasketData = null;
	if(reqBasketId != ""){
		try {
			storedBasketObject = CustomObjectMgr.getCustomObject('AppBasketData', reqBasketId);
			if(!storedBasketObject){
				let r = require('~/cartridge/scripts/util/Response');
		        return r.renderJSON({
		            success: false,
		            message: 'Basket does not exists for the BasketId sent in the request'
		        });
			} else if (storedBasketObject != null && storedBasketObject.custom.SessionId == reqSessionId) {
				storedBasketData = storedBasketObject.custom.storedBasketData;
	        }
	    } catch (e) {
	        Logger.error('Error while fetching custom object in AppWeb.js controller : ' + e);
	    }
	    
	    if(storedBasketData != null){
	    	requestItemsEx = JSON.parse(storedBasketData).BasketResponse.BasketItems;
	    	for each (var requestItem in requestItemsEx){
					try {
						var pid = requestItem.SKU;
						var quantity = parseInt(requestItem.Quantity);
						var productModel = Product.get(pid);
						var productOptionModel;
						productOptionModel = productModel.updateOptionSelection(params);
						var isAddproduct = true;
						if(reqActionType=='RemoveFromBasket' && requestItemSKUs.contains(requestItem.SKU)){
			    			isRemoveBasket = false;
			    			resActionType = "RemovedFromBasket";
			    			var reqQuantity = reqSkuQtyMap.get(requestItem.SKU);
			    			if(quantity > reqQuantity){
			    				quantity = quantity - reqQuantity;
			    			} else {
			    				isAddproduct = false;
			    			}
						} 
						if(isAddproduct){
							var repData = basket.addProductItem(productModel.object, quantity, productOptionModel);
							if(repData && !repData.success){
								let r = require('~/cartridge/scripts/util/Response');
						        return r.renderJSON({
						            success: false,
						            message: repData.message
						        });
							}
						}
					} catch (e) {
				        Logger.error('Error while fetching product in AppWeb.js controller : ' + e);
				        let r = require('~/cartridge/scripts/util/Response');
				        return r.renderJSON({
				            success: false,
				            message: e.message
				        });
				    }
			}
	    }
	    
	}
	
	
	// Perform the requested action - AddToBasket/RemoveFromBasket/GetBasket
	if(reqActionType=='AddToBasket'){
		resActionType = "AddedToBasket";
		for each (var requestItem in requestItems){
			var pid = requestItem.SKU;
			var quantity = parseInt(requestItem.Quantity);
			if(quantity > maxOrderQty){
				let r = require('~/cartridge/scripts/util/Response');
		        return r.renderJSON({
		            success: false,
		            message: 'Maximum of 50 per item can be ordered. Please adjust your quantity to continue: ' + pid
		        });
			}
			try {
				var productModel = Product.get(pid);
				var productOptionModel;
				productOptionModel = productModel.updateOptionSelection(params);
				var repData = basket.addProductItem(productModel.object, quantity, productOptionModel);
				if(repData && !repData.success){
					let r = require('~/cartridge/scripts/util/Response');
			        return r.renderJSON({
			            success: false,
			            message: repData.message
			        });
				}
			} catch (e) {
		        Logger.error('Error while fetching product in AppWeb.js controller : ' + e);
		        let r = require('~/cartridge/scripts/util/Response');
		        return r.renderJSON({
		            success: false,
		            message: e.message
		        });
		    }
		}
	} else if(reqActionType=='RemoveFromBasket' && isRemoveBasket){
		resActionType = "RemovedFromBasket";
		for each (var requestItem in requestItems){
			var pid = requestItem.SKU;
			var quantity = parseInt(requestItem.Quantity);
			var productListItems = basketObj.productLineItems;
			if(productListItems.length > 0){
				for (var q = 0; q < productListItems.length; q++) {
	            	if (productListItems[q].productID === pid) {
	                    var productInCart = productListItems[q];
	                    if (productInCart) {
	                    	Transaction.wrap(function () {
		                    	basketObj.removeProductLineItem(productInCart);
		                    	basket.calculate();
	                        });
	                    }
	                    break;
	                } else if(q == (productListItems.length -1)){
	                	let r = require('~/cartridge/scripts/util/Response');
	    		        return r.renderJSON({
	    		            success: false,
	    		            message: 'Basket does not contains requested item'
	    		        });
	                }
	            }
			}else{
				let r = require('~/cartridge/scripts/util/Response');
		        return r.renderJSON({
		            success: false,
		            message: 'Basket is empty'
		        });
			}
		}
	} else if(reqActionType=='UpdateBasket'){
		resActionType = "UpdatedBasket";
		for each (var requestItem in requestItems){
			var pid = requestItem.SKU;
			var quantity = parseInt(requestItem.Quantity);
			if(quantity > maxOrderQty){
				let r = require('~/cartridge/scripts/util/Response');
		        return r.renderJSON({
		            success: false,
		            message: 'Maximum of 50 per item can be ordered. Please adjust your quantity to continue: ' + pid
		        });
			}
			try {
				var productListItems = basketObj.productLineItems;
	            for (var q = 0; q < productListItems.length; q++) {
	            	if (productListItems[q].productID === pid) {
	                    var productInCart = productListItems[q];
	                    if (productInCart) {
	                    	Transaction.wrap(function () {
	                    		productInCart.setQuantityValue(quantity);
	                    		basket.calculate();
	                    	});
	                    }
	                    break;
	            	} else if(q == (productListItems.length -1)){
	                	let r = require('~/cartridge/scripts/util/Response');
	    		        return r.renderJSON({
	    		            success: false,
	    		            message: 'Basket does not contains requested item'
	    		        });
	                }
	            }
			} catch (e) {
		        Logger.error('Error while fetching product in AppWeb.js controller : ' + e);
		        let r = require('~/cartridge/scripts/util/Response');
		        return r.renderJSON({
		            success: false,
		            message: e.message
		        });
		    }
		}
	} else if(reqActionType=='GetBasket'){
		resActionType = "GetBasketResponse";
	}
	
	// Response JSON Formation
	basketObj = basket.object;
	responseJSON = new Object();
	basketResponse = new Object();
	basketResponse.ActionType = resActionType;
	basketResponse.BasketId = reqBasketId != "" ? reqBasketId : basketObj.UUID;
	basketResponse.SessionId = reqSessionId;
	
	basketItems = new Array();
	for each(var productLineItem in basketObj.productLineItems){
		try {
			var basketItem = new Object();
			basketItem.SKU = productLineItem.productID;
			basketItem.Quantity = productLineItem.quantityValue.toString();
			if(productLineItem.adjustedGrossPrice != Money.NOT_AVAILABLE){
				basketItem.Price = productLineItem.adjustedGrossPrice.value.toFixed(2).toString();
			} else {
				basketItem.Price = 'N/A';
			}
			var productModel;
			productModel = productLineItem.product;
			if(productModel.availabilityModel.inventoryRecord && productModel.availabilityModel.inventoryRecord.perpetual){
				basketItem.InStock = true;
			}else{
				var atsValue = productModel.availabilityModel.inventoryRecord ? productModel.availabilityModel.inventoryRecord.ATS.value : 0;
				if(productLineItem.quantityValue <= atsValue){
					basketItem.InStock = true;
				}else{
					basketItem.InStock = false;
				}
			}
			basketItems.push(basketItem);
		 } catch (e) {
			 Logger.error('Error while creating custom object in AppWeb.js controller : ' + e);
		 }
	}
	if(basketObj.adjustedMerchandizeTotalGrossPrice != Money.NOT_AVAILABLE){
		basketTotal = basketObj.adjustedMerchandizeTotalGrossPrice.value.toFixed(2).toString();
	} else {
		basketTotal = 'N/A';
	}
	
	basketResponse.BasketItems = basketItems;
	basketResponse.BasketTotal = basketTotal;
	responseJSON.BasketResponse = basketResponse;
	
	var responseString = JSON.stringify(responseJSON);
	
	try {
		Transaction.wrap(function() {
			if (storedBasketObject != null) {
				storedBasketObject.custom.storedBasketData = responseString;
				storedBasketObject.custom.SessionId = reqSessionId;
			} else if (reqBasketId == ""){
	        	storedBasketObject = CustomObjectMgr.createCustomObject('AppBasketData', basketObj.UUID);
	        	storedBasketObject.custom.storedBasketData = responseString;
	        	storedBasketObject.custom.SessionId = reqSessionId;
	        }
		});
    } catch (e) {
        Logger.error('Error while creating custom object in AppWeb.js controller : ' + e);
    }
	
	return require('~/cartridge/scripts/util/Response').renderJSON(responseJSON || {});
	
}

function webBasket() {
	var Product = app.getModel('Product');
	var basket = app.getModel('Cart').goc();
	var basketObj = basket.object;
	
	// Request Variables
	var params = request.httpParameterMap;
	var reqBasketId = params.BasketId;
	var reqSessionId = params.SessionId;
	
	if(!reqBasketId || !reqSessionId){
		response.redirect(URLUtils.url('Cart-Show'));
	}else{
		// Create a basket with existing basket data from custom object
		var storedBasketObject = null;
		var storedBasketData = null;
		if(reqBasketId != ""){
			try {
				storedBasketObject = CustomObjectMgr.getCustomObject('AppBasketData', reqBasketId);
				if (storedBasketObject != null && storedBasketObject.custom.SessionId == reqSessionId) {
					storedBasketData = storedBasketObject.custom.storedBasketData;
		        }
		    } catch (e) {
		        Logger.error('Error while fetching custom object in AppWeb.js controller : ' + e);
		    }
		    
		    if(storedBasketData != null){
		    	requestItemsEx = JSON.parse(storedBasketData).BasketResponse.BasketItems;
		    	for each (var requestItem in requestItemsEx){
					try {
						var pid = requestItem.SKU;
						var quantity = parseInt(requestItem.Quantity);
						var productModel = Product.get(pid);
						var productOptionModel;
						productOptionModel = productModel.updateOptionSelection(params);
						basket.addProductItem(productModel.object, quantity, productOptionModel);
					} catch (e) {
				        Logger.error('Error while fetching product in AppWeb.js controller : ' + e);
				    }
				}
		    }
		}
		response.redirect(URLUtils.https('Cart-Show','BasketId',reqBasketId,'SessionId',reqSessionId));
	}
}


function getBasket() {
	var Product = app.getModel('Product');
	var basket = app.getModel('Cart').goc();
	var basketObj = basket.object;
	
	// Request Variables
	var params = request.httpParameterMap;
	var reqBasketId = params.BasketId.stringValue;
	var reqSessionId = params.SessionId.stringValue;
	var resActionType = "GetBasketResponse";
	
	if(!reqBasketId || !reqSessionId){
		let r = require('~/cartridge/scripts/util/Response');
        return r.renderJSON({
            success: false,
            message: 'Basket does not exists for the BasketId/SessionId sent in the request'
        });
	}else{
		// Create a basket with existing basket data from custom object
		var storedBasketObject = null;
		var storedBasketData = null;
		if(reqBasketId != ""){
			try {
				storedBasketObject = CustomObjectMgr.getCustomObject('AppBasketData', reqBasketId);
				if(!storedBasketObject){
					let r = require('~/cartridge/scripts/util/Response');
			        return r.renderJSON({
			            success: false,
			            message: 'Basket does not exists for the BasketId sent in the request'
			        });
				} else if(storedBasketObject != null && storedBasketObject.custom.SessionId != reqSessionId){
					let r = require('~/cartridge/scripts/util/Response');
			        return r.renderJSON({
			            success: false,
			            message: 'Basket does not exists for the SessionId sent in the request'
			        });
				} else if (storedBasketObject != null && storedBasketObject.custom.SessionId == reqSessionId) {
					storedBasketData = storedBasketObject.custom.storedBasketData;
		        }
		    } catch (e) {
		        Logger.error('Error while fetching custom object in AppWeb.js controller : ' + e);
		    }
		    
		    if(storedBasketData != null){
		    	requestItemsEx = JSON.parse(storedBasketData).BasketResponse.BasketItems;
		    	for each (var requestItem in requestItemsEx){
					try {
						var pid = requestItem.SKU;
						var quantity = parseInt(requestItem.Quantity);
						var productModel = Product.get(pid);
						var productOptionModel;
						productOptionModel = productModel.updateOptionSelection(params);
						var repData = basket.addProductItem(productModel.object, quantity, productOptionModel);
						if(repData && !repData.success){
							let r = require('~/cartridge/scripts/util/Response');
					        return r.renderJSON({
					            success: false,
					            message: repData.message
					        });
						}
					} catch (e) {
				        Logger.error('Error while fetching product in AppWeb.js controller : ' + e);
				    }
				}
		    }
		}
		
		basketObj = basket.object // updated basketObj
		var responseJSON = prepareResopnseJSON(basketObj,reqBasketId,reqSessionId,resActionType);
		var responseString = JSON.stringify(responseJSON);
		
		return require('~/cartridge/scripts/util/Response').renderJSON(responseJSON || {});
		
	}
}

function prepareResopnseJSON(basketObj,reqBasketId,reqSessionId,resActionType) {
	
	var responseJSON;
	var basketResponse, basketItems, basketTotal;
	
	// Response JSON Formation
	responseJSON = new Object();
	basketResponse = new Object();
	basketResponse.ActionType = resActionType;
	basketResponse.BasketId = reqBasketId != "" ? reqBasketId : basketObj.UUID;
	basketResponse.SessionId = reqSessionId;
	
	basketItems = new Array();
	for each(var productLineItem in basketObj.productLineItems){
		try {
			var basketItem = new Object();
			basketItem.SKU = productLineItem.productID;
			basketItem.Quantity = productLineItem.quantityValue.toString();
			if(productLineItem.adjustedGrossPrice != Money.NOT_AVAILABLE){
				basketItem.Price = productLineItem.adjustedGrossPrice.value.toFixed(2).toString();
			} else {
				basketItem.Price = 'N/A';
			}
			var productModel = productLineItem.product;
			if(productModel.availabilityModel.inventoryRecord && productModel.availabilityModel.inventoryRecord.perpetual){
				basketItem.InStock = true;
			} else {
				var atsValue = productModel.availabilityModel.inventoryRecord ? productModel.availabilityModel.inventoryRecord.ATS.value : 0;
				if(productLineItem.quantityValue <= atsValue){
					basketItem.InStock = true;
				} else {
					basketItem.InStock = false;
				}
			}
			basketItems.push(basketItem);
		 } catch (e) {
			 Logger.error('TODO - Error while creating custom object in AppWeb.js controller : ' + e);
		 }
	}
	if(basketObj.adjustedMerchandizeTotalGrossPrice != Money.NOT_AVAILABLE){
		basketTotal = basketObj.adjustedMerchandizeTotalGrossPrice.value.toFixed(2).toString();
	} else {
		basketTotal = 'N/A';
	}
	
	basketResponse.BasketItems = basketItems;
	basketResponse.BasketTotal = basketTotal;
	responseJSON.BasketResponse = basketResponse;
	
	return responseJSON;
	
}

function validateRequest(requestBody) {
	
	var requestJSON;
	var basketRequest, reqActionType, reqBasketId, reqSessionId, requestItems;
	try{
		requestJSON = JSON.parse(requestBody);
		basketRequest = requestJSON.BasketRequest;
		reqActionType = basketRequest.ActionType;
		reqBasketId = basketRequest.BasketId;
		reqSessionId = basketRequest.SessionId;
		requestItems = basketRequest.RequestItems;
	} catch (e) {
		return {
			valid: false,
            message: e.message
	    };
    }
	
	// Validate requestJSON for all required field
	if(!reqActionType || (reqActionType!='AddToBasket' && reqActionType!='RemoveFromBasket' && reqActionType!='GetBasket' && reqActionType!='UpdateBasket')){
		return {
			valid: false,
			message: 'Invalid ActionType'
	    };
	}
	
	if(reqActionType && (reqActionType=='AddToBasket' || reqActionType=='RemoveFromBasket' || reqActionType=='UpdateBasket')){
		if(!requestItems || !requestItems[0].SKU || !requestItems[0].Quantity || regex.numeric.test(requestItems[0].Quantity)){
			return {
				valid: false,
				message: 'Invalid RequestItems'
		    };
		} else if(typeof reqBasketId === 'undefined'){
			return {
				valid: false,
				message: 'Invalid BasketId'
		    };
		}
	}
	
	if(reqActionType && (reqActionType=='GetBasket' || reqActionType=='RemoveFromBasket' || reqActionType=='UpdateBasket')){
		if(!reqBasketId){
			return {
				valid: false,
				message: 'Invalid BasketId'
		    };
		} else if(reqActionType=='GetBasket' && requestItems){
			return {
				valid: false,
				message: 'Invalid RequestItems'
		    };
		}
	}
	
	if(!reqSessionId){
		return {
			valid: false,
			message: 'Invalid SessionId'
	    };
	}
	
	return {
		valid: true,
		requestJSON: requestJSON
    };
}

exports.BasketRequest = guard.ensure(['post'], basketRequest);
exports.WebBasket = guard.ensure(['get'], webBasket);
exports.GetBasket = guard.ensure(['get'], getBasket);