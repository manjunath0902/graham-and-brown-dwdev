/**
* Copies files to a remote FTP-Location
*/

var System = require('dw/system');
var File = require('dw/io/File');

/**
 * 
 * @param HostURL {String}
 * @param UserID {String}
 * @param Password {String}
 * @param FilePattern {String}
 * @param SourceFolder {String}
 * @param TargetFolder {String}
 * @param SecureFtp {String}
 * @param ArchiveFile {String}
 * @param Timeout {Number}
 * @returns true
 */

function execute(parameters, stepExecution)
{
    var ftpClient = {};
	var copyResult = true;
	var HostURL = parameters.HostURL;
	var UserID = parameters.UserID;
	var Password = parameters.Password;
	var FilePattern = parameters.FilePattern;
	var SourceFolder = parameters.SourceFolder;
	var TargetFolder = parameters.TargetFolder
	var SecureFtp = parameters.SecureFtp;
	var ArchiveFile = parameters.ArchiveFile;
	var Timeout = parameters.Timeout;
	var noFileFoundStatus = parameters.noFileFoundStatus;
	var portNumber = parameters.PortNo;
	
	//Test mandatory paramater
	if (!empty(HostURL) && !empty(SecureFtp) && !empty(ArchiveFile) && !empty(SourceFolder) && !empty(TargetFolder)) {
		var sftp = false;
		if ("FTP".equals(SecureFtp)) {
			var FTPClient = require('dw/net/FTPClient');
			ftpClient = new FTPClient();
		} else {
			var SFTPClient = require('dw/net/SFTPClient');
			ftpClient = new SFTPClient();
			sftp = true;
		}
		
		//set connection timeout
		Timeout = (Timeout != null?new Number(Timeout):Timeout);
		if(ftpClient != null && Timeout != null && Timeout > 0) {
			ftpClient.setTimeout(Timeout);
		}
		
		// Try to connect
		if(!empty(UserID) && !empty(Password)) {
			if(!empty(portNumber))
			{
				ftpClient.connect(HostURL, portNumber, UserID, Password);
			}
			else
			{
				ftpClient.connect(HostURL, UserID, Password);
			}
		} else {
			if (sftp) {
				return new System.Status(System.Status.ERROR, 'FTPUpload.js: User-ID and Password are manadatory for SFTP-Connection.');
			} else {
				ftpClient.connect(HostURL);
			}
		}
		if (ftpClient.connected) {
			var filePattern;
			if (!empty(FilePattern)) {
				filePattern = FilePattern;
			} else {
				filePattern = "^[\\w\-]{1,}\\.xml$";
			}
		
			//Archive flag
			var archiveFile = true;
			if("Keep".equals(ArchiveFile)) {
				archiveFile = false;
			}
	   	 	//Copy (and archive) files
	   	 	var copyResult = copyFilesToTarget(ftpClient, SourceFolder, filePattern, TargetFolder, archiveFile);
		} else {
			return new System.Status(System.Status.ERROR, 'FTPUpload.js: The connection couldn\'t be established.');
		}
	} else {
		return new System.Status(System.Status.ERROR, 'FTPUpload.js: One or more mandatory parameters are missing.');
	}
	
	if (ftpClient != null && ftpClient.connected) {
		ftpClient.disconnect();
	}
	
	if (!copyResult.done) {
		if (copyResult.noFilesFound) {
			switch (noFileFoundStatus) {
				case 'OK':
					return new System.Status(System.Status.OK, 'FILE_NOT_FOUND', copyResult.message);
					break;
				case 'WARN':
					return new System.Status(System.Status.OK, 'FILE_NOT_FOUND', copyResult.message);
					break;
				case 'ERROR':
					return new System.Status(System.Status.ERROR, 'FILE_NOT_FOUND', copyResult.message);
					break;
			}
		}
		return new System.Status(System.Status.ERROR, 'ERROR', copyResult.message);
	}
	return new System.Status(System.Status.OK);
}
/**
*	Copy (and archive locally) files to the remote FTP-Target-Folder
*	@param ftpClient	{Object} 	FTP Client used
*	@param sourceFolder {String} 	source Folder
*	@param filePattern 	{String} 	The pattern for the filenames
*	@param targetFolder	{String} 	target FTP Folder
*	@param archiveFile 	{Boolean} 	Flag if filess should be archived after successful copying 
*
*	@returns Boolean If files were found at the specified location.
**/
function copyFilesToTarget(ftpClient, sourceFolder, filePattern, targetFolder, archiveFile) 
{
	
	var targetFolderStr = targetFolder.charAt(0) == '/' ? targetFolder.substring(1) : targetFolder;
	var sourceDirStr = sourceFolder.charAt(0).equals("/") ? sourceFolder + "/"  : "/" + sourceFolder;
	
	var fileList = getFileListingFromSource(sourceDirStr, filePattern);
	
	if(fileList != null && fileList.length > 0) {
		
		var dirExists = ftpClient.cd(targetFolderStr);
	
		if(!dirExists) {
			ftpClient.mkdir(targetFolderStr);
			ftpClient.cd(targetFolderStr);
		}
		
		for(var i = 0, len = fileList.length; i < len; i++) {
			var fileCopyResult = copyFileToTarget(ftpClient, fileList[i], archiveFile);
			if (!fileCopyResult.done) {
				return {'done': false, 'message' : 'error while transfering file : '+fileCopyResult.message};
			}
		}
		
		return {'done': true, 'message' : ''};
	}
	
	return {'done': false, 'message' : 'File-List was empty.', 'noFilesFound': true};
}
/**
*	get list of files which matches the file pattern
*	@param sourceFolder	{String} 	source Folder
*	@param filePattern 	{String} 	The pattern for the filenames
*
*	@returns Collection List of files which match the given pattern
**/
function getFileListingFromSource(sourceFolder, filePattern)
{
	var fileList = new (require('dw/util/ArrayList'))();
	var theDir = new File("IMPEX/" + sourceFolder);
	
	var regExp = new RegExp(filePattern);
	
	fileList.addAll(theDir.listFiles(function(file){			
		if (!empty(filePattern)) {
			return regExp.test(file.name);
		}
		
		return true;
	}));
	
	return fileList;
}
/**
*	Copy (and archive locally) a file to the remote FTP-Target-Folder
*	@param ftpClient 	{Object} 	FTP Client used
*	@param file 		{File} 		The file to copy
*	@param archiveFile 	{Boolean} 	Flag if files should be archived after successful copying 
*
**/
function copyFileToTarget(ftpClient, file, archiveFile) 
{
	
	var fileCopied = ftpClient.putBinary(file.name, file);
	if (!fileCopied) {
		let msg = ftpClient.errorMessage || ftpClient.replyMessage || 'unknown error';
		return {'done' : false, 'message' : msg};
	}
	if (archiveFile) {
		var archiveDirStr = file.fullPath.substring( 0, file.fullPath.lastIndexOf("/") ) + "/archive";
		var archiveDir = new File(archiveDirStr);
		if(!archiveDir.exists()) {
			archiveDir.mkdir();
		}
		var theArchiveFile = new File(archiveDirStr + "/" + file.name);
		copyFileToArchive(file, theArchiveFile);
		file.remove();
	}
	
	return {'done' : true, 'message' : ''};
}
/**
*	Archive a file locally
*	@param srFile {File} The file to archive
*	@param dtFile {File} The archiv file
*
**/
function copyFileToArchive(srFile, dtFile)
{
	var FileReader = require('dw/io/FileReader');
	var FileWriter = require('dw/io/FileWriter');
	var fReader = new FileReader(srFile);
	var fWriter = new FileWriter(dtFile);
	var nextLine;
	while((nextLine = fReader.readLine()) != null) {
		fWriter.writeLine(nextLine);
	}
	fReader.close();
	fWriter.close();
	dtFile.createNewFile();
}

module.exports = {
		execute: execute
};