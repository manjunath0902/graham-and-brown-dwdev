'use strict';

/**
 * @module util/Browsing
 */
var URLUtils = require('dw/web/URLUtils');

/**
 * Recovers the last url from the click stream
 * @return {dw.web.URL} the last called URL
 */
exports.lastUrl  = function lastUrl() {
    var location = URLUtils.url('Home-Show');
    var click = session.clickStream.last;
    if (click) {
        location = URLUtils.url(click.pipelineName);
        if (!empty(click.queryString) && click.queryString.indexOf('=') !== -1) {
            var params = click.queryString.split('&');
            params.forEach(function (param) {
                location.append.apply(location,param.split('='));
            });
        }
    }
    return location;
};

/**
 * Returns the last catalog URL or homepage URL if non found
 * @return {String} The last browsed catalog URL
 */
exports.lastCatalogURL = function lastCatalogURL() {
    var clicks = session.getClickStream().getClicks();

	for (var i = clicks.size() - 1; i >= 0; i--) {
		var click = clicks[i];
		switch (click.getPipelineName()) {
			case 'Product-Show':
			case 'Search-Show':
				// catalog related click
				// replace well-known http parameter names 'source' and 'format' to avoid loading partial page markup only
				return 'http://' + click.host + click.url.replace(/source=/g, 'src=').replace(/format=/g, 'frmt=');
		}
	}

    return URLUtils.httpHome().toString();
};

/**
 * Returns homepage canonical url
 * @return {String}
 */
exports.getHomePageCanonicalURL = function getHomePageCanonicalURL() {
	var home = require('../common/BrowsingUtils').pndHome();
	if (home.indexOf('http://') === 0 || home.indexOf('https://') === 0) {
		return home;
	}
	return 'http://' + request.httpHost + home;
};

/**
 * Returns product page canonical url
 * @param  {dw.catalog.Product} product
 * @return {String}
 */
exports.getProductPageCanonicalURL = function getProductPageCanonicalURL(product) {
	if (!empty(product)) {
		var productID = product.variant ? product.getVariationModel().master.ID : product.ID;
		return URLUtils.http('Product-Show', 'pid', productID).toString();
	}
};

/**
 * Returns category page canonical url
 * @param  {dw.catalog.ProductSearchModel} productSearchResult
 * @return {String}
 */
exports.getCategoryPageCanonicalURL = function getCategoryPageCanonicalURL(productSearchResult) {
	if (!empty(productSearchResult) && productSearchResult.categorySearch) {
		return URLUtils.http('Search-Show', 'cgid', productSearchResult.category.ID).toString();
	}
};

/**
 * Returns content page canonical url
 * @param  {dw.content.Content} content
 * @return {String}
 */
exports.getContentPageCanonicalURL = function getContentPageCanonicalURL(content) {
	if (!empty(content)) {
		return URLUtils.http('Page-Show', 'cid', content.ID).toString();
	}
};


/**
 * @method appendParametersToUrl
 * @param {String} url
 * @param {Map} params
 * Returns URL with appended parameters
 * @return {String} URL with apended parameters
 */
exports.appendParametersToUrl = function appendParametersToUrl(url, params) {
	var urlParams = '';
	var result = '';
	var rx = /(^dwfrm)/;

	for (var param in params) {
		var val = params[param];

		if (!rx.test(param)) {
			urlParams += param + '=' + encodeURIComponent(val) + '&';
		}
	}

	if (url.toString().indexOf('?') < 0) {
		result = url + '?' + urlParams;
	} else {
		result = url + '&' + urlParams;
	}

	result = result.slice(0, -1);

	return result;
}

//add url parser (libUrl)
//add continue shopping here