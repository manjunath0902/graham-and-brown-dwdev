/**
* CSVOrderFeed.ds 
* To generate a order export CSV file with '|' as delimiter and values are enclosed within ""
* 
*
* @input CurrentOrders : dw.util.Iterator The orders being prepared for export.
*/
var Logger = require("dw/system/Logger");
var File = require("dw/io/File");
var CSVStreamWriter = require("dw/io/CSVStreamWriter");
var FileWriter = require("dw/io/FileWriter");
var Calendar = require("dw/util/Calendar");
var StringUtils = require("dw/util/StringUtils");
var OrderMgr = require("dw/order/OrderMgr");
var Order = require("dw/order/Order");
var Iterator = require("dw/util/Iterator");
var Site = require("dw/system/Site");
var StringUtils = require("dw/util/StringUtils");
function execute( args : PipelineDictionary ) : Number
{
    var orderExportFileWriter : FileWriter = null;
    var orderItemsExportFileWriter : FileWriter = null;
	var calendar = new Calendar();
	var currentOrders : Iterator = args.CurrentOrders;
	var dateString = StringUtils.formatCalendar(calendar, "yyyyMMdd");
	var orderDetailFileName = Site.current.getCustomPreferenceValue("csvOrderDetailFileName");
	var orderItemsFileName = Site.current.getCustomPreferenceValue("csvOrderItemsFileName");
	var siteid = Site.current.getCustomPreferenceValue("csvSiteID");
	var sequenceNo = pad(Site.current.getCustomPreferenceValue("csvOrderExportSequenceNo").toString());
	try{
		var exportDirectory = new File(File.IMPEX + File.SEPARATOR + "upload" + File.SEPARATOR +"CSVFeeds" + File.SEPARATOR + "OrderFeeds");
		if( !exportDirectory.exists() )
		{
			exportDirectory.mkdir();
		}
		var orderExportFile = new File(File.IMPEX + File.SEPARATOR + "upload" + File.SEPARATOR +"CSVFeeds"+ File.SEPARATOR + "OrderFeeds" + File.SEPARATOR + dateString + "_GAB_DW_" + orderDetailFileName +"_"+ siteid +"_"+ sequenceNo + '.csv');
		var orderItemsExportFile = new File(File.IMPEX + File.SEPARATOR + "upload" + File.SEPARATOR +"CSVFeeds"+ File.SEPARATOR + "OrderFeeds" + File.SEPARATOR + dateString + "_GAB_DW_" + orderItemsFileName +"_"+ siteid +"_"+ sequenceNo + '.csv');
		if( !orderExportFile.exists() ){
			orderExportFile.createNewFile();
		}
		if( !orderItemsExportFile.exists() ){
			orderItemsExportFile.createNewFile();
		}
		orderExportFileWriter= new FileWriter(orderExportFile);
		orderItemsExportFileWriter = new FileWriter(orderItemsExportFile);
		var orderCSVWriter =  new CSVStreamWriter(orderExportFileWriter, "|"," ");
		var orderItemsCSVWriter = new CSVStreamWriter(orderItemsExportFileWriter, "|"," ");
		
		var csvArray = new Array();
		var itemsCSVArray = new Array();
		var plItemsArr = new Array();
		var plItemArr = new Array();
		var shLItemsArr = new Array();
		var shLItemArr = new Array();
		var shipmentArr = new Array();
		var shipmentsArr = new Array();
		var payInstr = new Array();
		var payInstrs = new Array();
		var orderCustomAttrs = new Array();
		//var currOrder : Order=OrderMgr.getOrder("00001205");
		var pli, custmAttr, currShipment, currShipLineitem, orderPaymentInstrument;
		var ccCalendar, orderCustAttr;
		csvArray=[];
		itemsCSVArray = [];
		var orderCustEmail, orderNumber, orderDate;
		//order details header
		csvArray.push( 'Order-no');
		csvArray.push( 'order-date');
		csvArray.push( 'created-by');
		csvArray.push( 'currency');
		csvArray.push( 'customer-locale');
		csvArray.push( 'taxation');
		csvArray.push( 'invoice-no');
		csvArray.push( 'customer-no');
		csvArray.push( 'customer-name');
		csvArray.push( 'customer-email');
		csvArray.push( 'first-name');
		csvArray.push( 'last-name');
		csvArray.push( 'address1');
		csvArray.push( 'address2');
		csvArray.push( 'city');
		csvArray.push( 'postal-code');
		csvArray.push( 'state-code');
		csvArray.push( 'country-code');
		csvArray.push( 'order-status');
		csvArray.push( 'shipping-status');
		csvArray.push( 'confirmation-status');
		csvArray.push( 'payment-status');
		csvArray.push( 'shipping-shippingMethodID');
		csvArray.push( 'Shipping-method');
		csvArray.push( 'shipping-addressFirstName');
		csvArray.push( 'shipping-addressLastName');
		csvArray.push( 'shipping-address1');
		csvArray.push( 'shipping-address2');
		csvArray.push( 'shipping-city');
		csvArray.push( 'shipping-postalCode');
		csvArray.push( 'shipping-stateCode');
		csvArray.push( 'shipping-countryCode');
		csvArray.push( 'shipping-gift');
		csvArray.push( 'merchandize-total-net-price');
		csvArray.push( 'merchandize-total-tax-price');
		csvArray.push( 'merchandize-total-gross-price')
		csvArray.push( 'adjusted-merchandize-total-net-price');
		csvArray.push( 'adjusted-merchandize-total-tax-price');
		csvArray.push( 'adjusted-merchandize-total-gross-price');
		csvArray.push( 'shipping-total-net-price');
		csvArray.push( 'shipping-total-tax-price');
		csvArray.push( 'shipping-total-gross-price');
		csvArray.push( 'adjusted-shipping-total-net-price');
		csvArray.push( 'adjusted-shipping-total-tax-price');
		csvArray.push( 'adjusted-shipping-total-gross-price');
		csvArray.push( 'total-net-price');
		csvArray.push( 'total-tax-price');
		csvArray.push( 'total-gross-price');
		csvArray.push( 'paymentInstruments');
		csvArray.push( 'Order-level-Discount-amount');
		csvArray.push( 'Coupon-ID');
		csvArray.push( 'Campaign-reference');
		orderCSVWriter.writeNext(csvArray);
		
		//orderItems header
		itemsCSVArray.push('CustomerEmail');
		itemsCSVArray.push("OrderItemID");
		itemsCSVArray.push("OrderID");
		itemsCSVArray.push("SKUName");
		itemsCSVArray.push("OrderItemSKUID");
		itemsCSVArray.push("Units");
		itemsCSVArray.push("GrossPrice");
		itemsCSVArray.push("Date Order");
		itemsCSVArray.push("Sku");
		itemsCSVArray.push("OrderShippingNetPrice");
		itemsCSVArray.push("OrderShippingGrossPrice");
		itemsCSVArray.push("OrderShippingTax");
		itemsCSVArray.push("OrderItemNetPrice");
		itemsCSVArray.push("OrderItemTax");
		itemsCSVArray.push("OrderItemBasePrice");
		itemsCSVArray.push("OrderItemTaxBasis");
		itemsCSVArray.push("OrderItemTaxRate");
		itemsCSVArray.push("OrderItemShipmentID");
		itemsCSVArray.push("OrderItem-EAN");
		itemsCSVArray.push("OrderItem-UPC");
		itemsCSVArray.push('OrderItem-Sample');
		itemsCSVArray.push('Discount-amount');
		orderItemsCSVWriter.writeNext(itemsCSVArray);
		
		//Start order iterations
		while (currentOrders.hasNext()) 
		{
			currOrder = currentOrders.next();
			csvArray=[];
			itemsCSVArray=[];
			orderCustEmail =currOrder.customerEmail; 
			orderNumber=currOrder.orderNo;
			orderDate = currOrder.getCreationDate().toISOString();
			csvArray.push( orderNumber);
			csvArray.push( orderDate);
			csvArray.push( currOrder.createdBy);
			csvArray.push( currOrder.currencyCode);
			csvArray.push( currOrder.customerLocaleID);
			csvArray.push( 'gross');
			csvArray.push( currOrder.invoiceNo);
			csvArray.push( currOrder.customerNo);
			csvArray.push( currOrder.customerName);
			csvArray.push( orderCustEmail);
			csvArray.push( currOrder.billingAddress.firstName);
			csvArray.push( currOrder.billingAddress.lastName);
			csvArray.push( currOrder.billingAddress.address1);
			csvArray.push( currOrder.billingAddress.address2||'');
			csvArray.push( currOrder.billingAddress.city);
			csvArray.push( currOrder.billingAddress.postalCode||'');
			csvArray.push( currOrder.billingAddress.stateCode||'');
			csvArray.push( currOrder.billingAddress.countryCode);
			csvArray.push( currOrder.status.displayValue);
			csvArray.push( currOrder.shippingStatus.displayValue);
			csvArray.push( currOrder.confirmationStatus.displayValue);
			csvArray.push( currOrder.paymentStatus.displayValue);
			//writing prduct line items into new file
			for each(pli in currOrder.allProductLineItems)
			{
				plItemArr =[];
				plItemArr.push(orderCustEmail);
				plItemArr.push(!empty(pli.product)?pli.product.ID:'');
				plItemArr.push(orderNumber);
				plItemArr.push(!empty(pli.product)?pli.product.name:'');
				plItemArr.push(pli.orderItem.itemID);
				plItemArr.push(pli.quantity.value.toFixed(1));
				plItemArr.push(pli.adjustedGrossPrice.value.toFixed(2));
				plItemArr.push(orderDate);
				plItemArr.push(!empty(pli.product)?pli.product.manufacturerSKU:'');
				plItemArr.push( pli.shipment.shippingTotalNetPrice.value.toFixed(2));
				plItemArr.push( pli.shipment.shippingTotalGrossPrice.value.toFixed(2));
				plItemArr.push( pli.shipment.shippingTotalTax.value.toFixed(2));				
				plItemArr.push(pli.adjustedNetPrice.value.toFixed(2));
				plItemArr.push(pli.adjustedTax.value.toFixed(2));
				plItemArr.push(pli.basePrice.value.toFixed(2));
				plItemArr.push(pli.taxBasis.value.toFixed(2));
				plItemArr.push(pli.taxRate.toFixed(2));
				plItemArr.push(pli.shipment.ID);
				if(!empty(pli.custom.EAN)){
					plItemArr.push(pli.custom.EAN.toString());
				}else{
					plItemArr.push('');
				}
				if(!empty(pli.custom.UPC)){
					plItemArr.push(pli.custom.UPC.toString());
				}else{
					plItemArr.push('');
				}
				if(!empty(pli.custom.isSample)){
					plItemArr.push(pli.custom.isSample.toString());
				}else{
					plItemArr.push('');
				}
				plItemArr.push(pli.getPrice().value.toFixed(2)-pli.getAdjustedPrice().value.toFixed(2));
				orderItemsCSVWriter.writeNext(plItemArr);
			}
			//ship Litems and shipments
			shLItemsArr = [];
			shipmentsArr = [];
			for each(currShipment in currOrder.shipments)
			{
				csvArray.push( currShipment.shippingMethodID);
				csvArray.push( !empty(currShipment.shippingMethod)?currShipment.shippingMethod.displayName:'');
				csvArray.push( currShipment.shippingAddress.firstName);
				csvArray.push( currShipment.shippingAddress.lastName);
				csvArray.push( currShipment.shippingAddress.address1);
				csvArray.push( currShipment.shippingAddress.address2);
				csvArray.push( currShipment.shippingAddress.city);
				csvArray.push( currShipment.shippingAddress.postalCode);
				csvArray.push( currShipment.shippingAddress.stateCode);
				csvArray.push( currShipment.shippingAddress.countryCode.displayValue);
				csvArray.push( currShipment.gift.toString());
				break;
			}
			csvArray.push( currOrder.merchandizeTotalNetPrice.value.toFixed(2));
			csvArray.push( currOrder.merchandizeTotalTax.value.toFixed(2));
			csvArray.push( currOrder.merchandizeTotalGrossPrice.value.toFixed(2));
			csvArray.push( currOrder.adjustedMerchandizeTotalNetPrice.value.toFixed(2));
			csvArray.push( currOrder.adjustedMerchandizeTotalTax.value.toFixed(2));
			csvArray.push( currOrder.adjustedMerchandizeTotalGrossPrice.value.toFixed(2));
			csvArray.push( currOrder.shippingTotalNetPrice.value.toFixed(2));
			csvArray.push( currOrder.shippingTotalTax.value.toFixed(2));
			csvArray.push( currOrder.shippingTotalGrossPrice.value.toFixed(2));
			csvArray.push( currOrder.adjustedShippingTotalNetPrice.value.toFixed(2));
			csvArray.push( currOrder.adjustedShippingTotalTax.value.toFixed(2));
			csvArray.push( currOrder.adjustedShippingTotalGrossPrice.value.toFixed(2));
			csvArray.push( currOrder.totalNetPrice.value.toFixed(2));
			csvArray.push( currOrder.totalTax.value.toFixed(2));
			csvArray.push( currOrder.totalGrossPrice.value.toFixed(2));
			//payment Instruments
			payInstrs = [];
			for each(orderPaymentInstrument in currOrder.paymentInstruments)
			{
				payInstr = [];
				if(orderPaymentInstrument.paymentMethod==orderPaymentInstrument.METHOD_CREDIT_CARD)
				{
					payInstr.push( orderPaymentInstrument.creditCardType);
					payInstr.push( orderPaymentInstrument.creditCardNumber);
					payInstr.push( orderPaymentInstrument.creditCardHolder);
					payInstr.push( orderPaymentInstrument.creditCardExpirationMonth);
					payInstr.push( orderPaymentInstrument.creditCardExpirationYear);
					payInstr.push( orderPaymentInstrument.custom.paymentMethod);
					//ccCalendar = new Calendar(orderPaymentInstrument.custom.payment_last3DSauthorization);
					//payInstr.push( StringUtils.formatCalendar(ccCalendar, "yyyy-MM-dd"));
				}
				payInstr.push( orderPaymentInstrument.paymentTransaction.amount.value.toFixed(2));
				if(!empty(orderPaymentInstrument.paymentTransaction.paymentProcessor))
					payInstr.push( orderPaymentInstrument.paymentTransaction.paymentProcessor.ID);
				payInstr.push( orderPaymentInstrument.paymentTransaction.transactionID);
				payInstr.push( orderPaymentInstrument.paymentTransaction.custom.paymentStatus);
				payInstrs.push(payInstr.toString());
			}
			csvArray.push(payInstrs.join(";"));
			var ordertotalextax = currOrder.merchandizeTotalPrice.subtract(currOrder.adjustedMerchandizeTotalTax);
			var ordersavedTotalValue = ordertotalextax.subtract(currOrder.adjustedMerchandizeTotalNetPrice);
			var shippingDiscount = currOrder.shippingTotalPrice.subtract(currOrder.adjustedShippingTotalPrice);
			if(currOrder.adjustedMerchandizeTotalPrice > 0 || shippingDiscount > 0){
				if(shippingDiscount != null && shippingDiscount > 0){
					csvArray.push(currOrder.merchandizeTotalPrice.subtract(currOrder.adjustedMerchandizeTotalPrice).add(shippingDiscount).value.toFixed(2));
				}else{
					csvArray.push(currOrder.merchandizeTotalPrice.subtract(currOrder.adjustedMerchandizeTotalPrice).value.toFixed(2));
				}
			}
			if(!empty(currOrder.getCouponLineItems())){
				var coupons = [];
				var campaigns = [];
                for each(var couponApplied : CouponLineItem in currOrder.getCouponLineItems()){
                	coupons.push(couponApplied.couponCode);
                	var coupon : dw.campaign.Coupon = dw.campaign.CouponMgr.getCouponByCode(couponApplied.couponCode);
                	if(!empty(coupon) && !empty(coupon.promotions)){
                		var promotions : dw.util.Iterator = coupon.promotions.iterator();
	                	if(coupon.promotions.length > 0){
	                		var campaignID = promotions.next().campaign.ID;
	                		campaigns.push(campaignID);
	                	}
                	}
                }
                csvArray.push(coupons.join(','));
                csvArray.push(campaigns.join(','));
			}else{
				csvArray.push("");
				csvArray.push("");
			}
			orderCSVWriter.writeNext(csvArray);
		}
		orderExportFileWriter.flush();
		orderCSVWriter.close();
		orderExportFileWriter.close();
		orderItemsExportFileWriter.flush();
		orderItemsCSVWriter.close();
		orderItemsExportFileWriter.close();
		Site.current.preferences.custom.csvOrderExportSequenceNo = Site.current.getCustomPreferenceValue("csvOrderExportSequenceNo")+1;
	}catch(e)
	{
		Logger.error("Error occured in CSVOrderFeed.ds "+e.message);
		return PIPELET_ERROR;
	}
	return PIPELET_NEXT;
}
function appendQuoteChar(appendVal)
{
	return '"'+appendVal+'"';
}

function pad (str) {
  var max =5;
  return str.length < max ? pad("0" + str, max) : str;
}