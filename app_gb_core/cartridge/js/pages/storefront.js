'use strict';
var rating = require('../rating');
exports.init = function () {
	$('#home-recommendations').slick({
		  dots: false,
		  infinite: true,
		  speed: 300,
		  slidesToShow: 5,
		  slidesToScroll: 5,
		  responsive: [{
              breakpoint: 1023,
              settings: {
                  slidesToShow: 3,
                  slidesToScroll: 3
              }
          }, {
              breakpoint: 768,
              settings: {
                  slidesToShow: 1,
                  slidesToScroll:1,
                  dots: true,
                  dotsClass: 'custom_paging',
                  customPaging: function (slider, i) {
                      return  (i + 1) + '/' + slider.slideCount;
                  },
              }
          }]
	});
	$('.banner2-slider .banner2-right').slick({
		  dots: false,
		  infinite: true,
		  speed: 300,
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  responsive: [{
              breakpoint: 767,
              settings: {
                  slidesToShow: 1,
                  dots: true,
                  dotsClass: 'custom_paging',
                  customPaging: function (slider, i) {
                      return  (i + 1) + '/' + slider.slideCount;
                  },
              }
          }]
	});
	$('#carousel-recommendations').slick({
		  dots: false,
		  infinite: true,
		  speed: 300,
		  slidesToShow: 2,
		  slidesToScroll: 1,
		  responsive: [{
            breakpoint: 767,
            settings: {
                slidesToShow: 1
            }
        }]
	});
	
	//for homepage first banner
	/*$('.banner1 span.show-only-desktop img').load(function(){
		$('.banner1').css('height',$(window).height() - 201);
		$('.banner1').css('max-height',$('.banner1 span.show-only-desktop img').height());
	});
	if ($('.banner1 span.show-only-desktop img').length > 0) {
		$('.banner1').css('height',$(window).height() - 201);
		$('.banner1').css('max-height',$('.banner1 span.show-only-desktop img').height());
	}
	$(window).resize(function() {
		$('.banner1').css('height',$(window).height() - 201);
		$('.banner1').css('max-height',$('.banner1 span.show-only-desktop img').height());
	});*/
	
	if($(document).find(".grid-tile").length > 0){
		rating.init();
	}
};
