'use strict';

var giftcert = require('../giftcert'),
	tooltip = require('../tooltip'),
	util = require('../util'),
	dialog = require('../dialog'),
	page = require('../page'),
	formPrepare= require('../pages/checkout/formPrepare'),
	validator = require('../validator');

/**
 * @function
 * @description Initializes the events on the address form (apply, cancel, delete)
 * @param {Element} form The form which will be initialized
 */
function initializeAddressForm() {
	var $form = $('#edit-address-form');

	$form.find('input[name="format"]').remove();
	tooltip.init();
	//$("<input/>").attr({type:"hidden", name:"format", value:"ajax"}).appendTo(form);

	$form.on('click', '.apply-button', function (e) {
		e.preventDefault();
		if (!$form.valid()) {
			return false;
		}
		var url = util.appendParamToURL($form.attr('action'), 'format', 'ajax');
		var applyName = $form.find('.apply-button').attr('name');
		var options = {
			url: url,
			data: $form.serialize() + '&' + applyName + '=x',
			type: 'POST'
		};
		$.ajax(options).done(function (data) {
			if (typeof(data) !== 'string') {
				if (data.success) {
					dialog.close();
					page.refresh();
				} else {
					window.alert(data.message);
					return false;
				}
			} else {
				$('#dialog-container').html(data);
				account.init();
				tooltip.init();
				util.selectbox();
			}
		});
	})
	.on('click', '.cancel-button, .close-button', function (e) {
		e.preventDefault();
		dialog.close();
	})
	.on('click', '.delete-button', function (e) {
		e.preventDefault();
		if (window.confirm(String.format(Resources.CONFIRM_DELETE, Resources.TITLE_ADDRESS))) {
			var url = util.appendParamsToUrl(Urls.deleteAddress, {
				AddressID: $form.find('#addressid').val(),
				format: 'ajax'
			});
			$.ajax({
				url: url,
				method: 'POST',
				dataType: 'json'
			}).done(function (data) {
				if (data.status.toLowerCase() === 'ok') {
					dialog.close();
					page.refresh();
				} else if (data.message.length > 0) {
					window.alert(data.message);
					return false;
				} else {
					dialog.close();
					page.refresh();
				}
			});
		}
	});

	validator.init();
	if($form.find('select[id$="_country"]').length > 0 && $form.find('select[id$="_country"]').val() == 'GB' && $form.find('[name$="_state"][id$="_state"]').length > 0){
		$form.find('[name$="_state"][id$="_state"]').closest(".form-row").hide();
	}
	$('select[id$="_country"]', $form).on('change', function () {
        updateStateOptions($form);
    });

}
function getSite(value){
   switch(value){
      case "GB":
             return "";
      case "FR":
             return "INPUT";
      case "AU":
    	  	return "SELECT";
      case "NZ":
	  		return "INPUT";
      case "CA":
             return "SELECT";
      case "DE":
      return "INPUT";
      case "NL":
             return "INPUT";
      case "US":
             return "SELECT";
      default:
             return "INPUT";
   }
}
 function updateStateOptions (form) {
	 var $form = $(form);
	 if($form.find('[name$="_state"][id$="_state"]').length > 0 && !$form.find('[name$="_state"][id$="_state"]').is(":visible")){
	  $form.find('[name$="_state"][id$="_state"]').closest(".form-row").show();
	 }
	 $form.find('label[for$="_state"]').removeClass('error');
	 $form.find('input[name$="_state"]').removeClass('error');
	 $form.find('span[id$="_state-error"]').remove();
	  var $country = $form.find('select[id$="_country"]'),
	    site = $country.val(),
	       country = Countries[$country.val()];
	 var sitevalue = getSite(site);
	 var isstate = $form.find(".form-row .field-wrapper .state");
	 if(isstate.length > 0)
	 {
	     var id = isstate.attr("id");
	     var name = isstate.attr("name");
	 }else{
		 var id = "dwfrm_profile_address_states_state";
	     var name = "dwfrm_profile_address_states_state";
	 }
	  if ($country.length === 0 || !country) {
		  if($form.find('[name$="_state"][id$="_state"]').length > 0){
			  $form.find('[name$="_state"][id$="_state"]').closest("div.form-row").find("label span").first().html("State / Province / Region");
			  var fieldHTML = $('<input/>').attr({ type: 'text', class :'input-text state ', id:id, name:name });
		      if($form.find('select[name$="_state"]').length){
		            $form.find('select[name$="_state"]').replaceWith( fieldHTML);
		      }else{
		           $form.find('input[name$="_state"]').replaceWith( fieldHTML);
		      }
		  }
	      return;
	  }
	  if(sitevalue == "SELECT") 
	  { 
		  var  fieldHTML = $('<select/>').attr({ class :'input-select state valid', id:id, name:name});
	      if($form.find('select[name$="_state"]').length){
	            $form.find('select[name$="_state"]').replaceWith( fieldHTML);
	      } else{
	            $form.find('input[name$="_state"]').replaceWith( fieldHTML);
	      }
	  }else if(sitevalue == "INPUT"){  
	      var fieldHTML = $('<input/>').attr({ type: 'text', class :'input-text state ', id:id, name:name });
	      if($form.find('select[name$="_state"]').length){
	            $form.find('select[name$="_state"]').replaceWith( fieldHTML);
	      }else{
	           $form.find('input[name$="_state"]').replaceWith( fieldHTML);
	      }
	  }else{
		  if($form.find('[name$="_state"][id$="_state"]').length > 0){
			  $form.find('[name$="_state"][id$="_state"]').closest(".form-row").hide();
		  }
	  }
	  if(sitevalue == "SELECT") 
	  {
		  var arrHtml = [],
	     $stateField = $country.data('stateField') ? $country.data('stateField') : $form.find('select[name$="_state"]'),
	     $stateLabel = ($stateField.length > 0) ? $form.find('label[for="' + $stateField[0].id + '"] span').not('.required-indicator') : undefined;
	 }else if(sitevalue == "INPUT"){
	     var arrHtml = [],
	    $stateField = $country.data('stateField') ? $country.data('stateField') : $form.find('input[name$="_state"]'),
	    $stateLabel = ($stateField.length > 0) ? $stateField.closest("div.form-row").find("label span").first() : undefined;
	 }
	 if ($stateLabel) {
		 $stateLabel.html(country.regionLabel);
	 } else {
	     return;
	 }
	 if(sitevalue == "SELECT")      
	 {
	      var s;
	      for (s in country.regions) {
	            arrHtml.push('<option value="' + s + '">' + country.regions[s] + '</option>');
	       }
	     $form.find('select[name$="_state"]').html(arrHtml.join(""));     
	  } 
	 util.selectbox();
}

/**
 * @private
 * @function
 * @description Toggles the list of Orders
 */
function toggleFullOrder () {
	$('.order-items')
		.find('li.hidden:first')
		.prev('li')
		.append('<a class="toggle">View All</a>')
		.children('.toggle')
		.click(function () {
			$(this).parent().siblings('li.hidden').show();
			$(this).remove();
		});
}
/**
 * @private
 * @function
 * @description Binds the events on the address form (edit, create, delete)
 */
function initAddressEvents() {
	var addresses = $('#addresses');
	if (addresses.length === 0) { return; }

	addresses.on('click', '.address-edit, .address-create', function (e) {
		e.preventDefault();
		dialog.open({
			url: this.href,
			options: {
				open: initializeAddressForm,
				dialogClass : 'account-dialog'
			},
			callback: function () {
				var $form = $('#edit-address-form');
				 var currentSite = $("#wrapper .country-select").find("select[id$='_destination'] option[selected=selected]").val();
				 var currentCountryCode = $form.find('select[id$="_country"]').val();
				 if(currentSite != currentCountryCode){
					 updateStateOptions($form);
					 if($form.find("input[id='state']").length > 0 && $form.find("input[id='state']").val() != '' && $form.find("input[id='state']").val() != 'null'){
						 $form.find('[name$="_state"][id$="_state"]').val($form.find("input[id='state']").val());
						 $form.find('[name$="_state"][id$="_state"]').trigger('change');
					 }
				 }
				 $('.account-dialog select.input-select').trigger('change');
				 util.selectbox();
			}
		});
	}).on('click', '.delete', function (e) {
		e.preventDefault();
		if (window.confirm(String.format(Resources.CONFIRM_DELETE, Resources.TITLE_ADDRESS))) {
			$.ajax({
				url: util.appendParamToURL($(this).attr('href'), 'format', 'ajax'),
				dataType: 'json'
			}).done(function (data) {
				if (data.status.toLowerCase() === 'ok') {
					page.redirect(Urls.addressesList);
				} else if (data.message.length > 0) {
					window.alert(data.message);
				} else {
					page.refresh();
				}
			});
		}
	});
}
/**
 * @private
 * @function
 * @description Binds the events of the payment methods list (delete card)
 */
$(window).on("load", function() {
    if ($(".orderstatusposition").length > 0) {
        $('html, body').scrollTop($('.login-order-track legend').position().top + 120);
    }
});



function initPaymentEvents() {
	$('.add-card').on('click', function (e) {
		e.preventDefault();
		dialog.open({
			//PREVAIL-Added  to handle validation issues
			url: $(e.target).attr('href'),
			options: {
				open: function () {
					validator.init();
				}
			}
		});
	});

	var paymentList = $('.payment-list');
	if (paymentList.length === 0) { return; }

	util.setDeleteConfirmation(paymentList, String.format(Resources.CONFIRM_DELETE, Resources.TITLE_CREDITCARD));

	$('form[name="payment-remove"]').on('submit', function (e) {
		e.preventDefault();
		// override form submission in order to prevent refresh issues
		var button = $(this).find('.delete');
		$('<input/>').attr({
			type: 'hidden',
			name: button.attr('name'),
			value: button.attr('value') || 'delete card'
		}).appendTo($(this));
		var data = $(this).serialize();
		$.ajax({
			type: 'POST',
			url: $(this).attr('action'),
			data: data
		})
		.done(function () {
			page.redirect(Urls.paymentsList);
		});
	});
}

function initLoginPage() {
	//o-auth binding for which icon is clicked
	$('.oAuthIcon').bind('click', function () {
		$('#OAuthProvider').val(this.id);
	});

	//toggle the value of the rememberme checkbox
	$('#dwfrm_login_rememberme').bind('change', function () {
		if ($('#dwfrm_login_rememberme').attr('checked')) {
			$('#rememberme').val('true');
		} else {
			$('#rememberme').val('false');
		}
	});
	
	$("[id$='_customer_businesstype']").change(function() {
        if ($(this).val() == "other") {
            $(".tradepleasespecify").show();
            $(".tradepleasespecify .field-wrapper .input-text").addClass("required");
            $(".tradepleasespecify").find('label').append('<span class="required">*</span>');

        }else{
            $(".tradepleasespecify").hide();
            $(".tradepleasespecify .field-wrapper .input-text").removeClass("required");
        } 

    });
	$('#password-reset').on('click', function (e) {
		e.preventDefault();
		dialog.open({
			url: $(e.target).attr('href'),
			options: {
				//Start JIRA PREV-334 : Title is missing for the Forgot password overlay.
				dialogClass : 'password-reset-popup custom-popup',
				width : 390,
				open: function () {
					validator.init();
					var $requestPasswordForm = $('[name$="_requestpassword"]'),
						$submit = $requestPasswordForm.find('[name$="_requestpassword_send"]');
					$($submit).on('click', function (e) {
						if (!$requestPasswordForm.valid()) {
							return;
						}
						e.preventDefault();
						dialog.submit($submit.attr('name'));
					});
				}
			}
		});
	});

}

//Logout events open a pop up for logout confirmation

/*function initLogoutEvents() {
	$(".account-logoutconfirmation").on('click', function (e) {
		e.preventDefault();
		var url = Urls.logoutconfirmation;
		dialog.open({
			url : url,
			options : {
				dialogClass : 'account-logout-confirmation custom-popup',
				width : 390
			},
			callback : function(){
				//close the dialog and remain in the same page
				$(".account-logout-no").on('click', function (e) {
					e.preventDefault();
					dialog.close();
				});
			}
		});
	});
}*/


function contactUsEvents() {
    if ($('#Contactus').length>0) {
    	formPrepare.init({
                    continueSelector: '[name$="contactus_send"]',
                    formSelector:'[id$="Contactus"]'
            });
    }

}
$("div.contactuscontainerslot").each(function() {
    if($(this).children().length == 0){
        $(this).hide();
        }
});


//For cancel button in contactus page
$('#cancelBtn').on('click', function(e) {
	  e.preventDefault();
	  var prevurl= $("[name='prevurl']").val();
	  window.location.href = prevurl;

	})
$('.hpregister').on('click',function(e){
		e.preventDefault();
		var url = $(location).attr("href");
		var index = url.indexOf('callback_url=');
		var redirecturl = Urls.hpregister;
		if(index > 0){
			var callbackparam = url.substring(index+13);
			redirecturl = decodeURIComponent(util.appendParamToURL(redirecturl, 'callback_url',callbackparam ));
		}
		window.location.href = redirecturl;
	})

$('#digital_contact').on('click', function(e) {
    e.preventDefault();
    var txtemail =$("#EmailInput").val();
    var emailExp = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(!txtemail.match(emailExp)){
        $('#digital-erroremail').addClass('error').text(Resources.DIGITAL_ERROR);
        return false;
     }      
    var form = $(this).closest('form');
    var url = Urls.digitalContactus;
    var firstname = $("#FirstName_textbox").val();
    var lastname = $("#LastName_textbox").val();
    var email = $("#EmailInput").val();
    var phone = $("#PhoneID_textbox").val();
    var enquiry = $("#Enquiry_textbox").val();
    url = util.appendParamToURL(url, 'firstname', firstname);
    url = util.appendParamToURL(url, 'lastname', lastname);
    url = util.appendParamToURL(url, 'email', email);
    url = util.appendParamToURL(url, 'phone', phone);
    url = util.appendParamToURL(url, 'enquiry', enquiry);
    url = util.appendParamToURL(url, 'format', 'ajax');
     
    $.ajax({
	    type: 'GET',
	    url:url,
    	success: function (response) {
    		$("#primary").html(Resources.THANKYOU_DIGITAL);
        },
        failure: function () {
        	location.reload();
        }
   });
})

/**
 * @private
 * @function
 * @description Binds the events of the order, address and payment pages
 */
function initializeEvents() {
	initializeAddressForm();// JIRA PREV-63: 'Add Address' or 'Edit address' overlay changing to page after click on Apply button,when there is an error message on overlay.
	contactUsEvents();
	toggleFullOrder();
	initAddressEvents();
	initPaymentEvents();
	initLoginPage();
}

var account = {
	init: function () {
		initializeEvents();
		giftcert.init();
		//initLogoutEvents();
	},
	initCartLogin: function () {
		initLoginPage();
	}
};

module.exports = account;
