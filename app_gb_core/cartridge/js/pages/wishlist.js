'use strict';

var page = require('../page'),
	account = require('./account'),
	minicart = require('../minicart'),
	gtmevents= require('../gtmevents'),
	util = require('../util');

exports.init = function () {
	//addProductToCart();
	//Start JIRA PREV-412 : SG Issue: Password reset overlay displayed as a page 
	account.initCartLogin();
	$('#editAddress').on('change', function () {
		page.redirect(util.appendParamToURL(Urls.wishlistAddress, 'AddressID', $(this).val()));
	});

	//add js logic to remove the , from the qty feild to pass regex expression on client side
	$('.option-quantity-desired input').on('focusout', function () {
		$(this).val($(this).val().replace(',', ''));
	});
	
	
	$(document).on("click",".option-add-to-cart ",function(e){

        var url = Urls.addWishlistProductToCart;
        var sku = $(this).closest("tr").find(".item-details .product-list-item .sku");
        var plid= $(this).find("input[name='plid']").val();
        var itemid=  $(this).find("input[name='itemid']").val();
        var pid = $(sku).find(".value").text();
        var wishlisturl= $("[name='wishlisturl']").val();
        var $form = $(this).closest('form');
        var Quantity=$(this).closest("form").find(".option-quantity-desired").find(".value").text();
        url = util.appendParamToURL(url, 'plid', plid);
        url = util.appendParamToURL(url, 'pid', pid);
        url = util.appendParamToURL(url, 'itemid', itemid);
        url = util.appendParamToURL(url, 'Quantity', Quantity);
        url = util.appendParamToURL(url, 'format', 'ajax');
        $.ajax({
                type: 'GET',
                dataType: 'html',
                        url: url,
                        success: function (response) {
                        	gtmevents.addToCart($form);
                             minicart.show(response);
                            window.location.href = wishlisturl;
                        },
                        failure: function () {
                                        window.alert(Resources.SERVER_ERROR);
                        }
        });
    })

	
	//For increasing and decreasing the quantity
	$(document).on("click",".option-quantity-desired a",function(e){
        e.preventDefault();
        var curObj = $(this);
        var inputfield = $(this).closest("form").find("input[type='number']");
        var inputValue = $(inputfield).val();
        var $closestQty = $(curObj).closest(".quantity-wishlist");
        if($(curObj).hasClass("prev-value")) {
	        if($(inputfield).val()  > 1) {
                inputValue --;
                $(inputfield).val(inputValue);
	        }
        }
        else if($(curObj).hasClass("next-value")) {
            if($(inputfield).val()  >= 1 && $(inputfield).val() < 99) {
                inputValue ++;
                $(inputfield).val(inputValue);
            }
        }
        $(curObj).closest('.block-add-to-cart').find("input[name='Quantity']").val($(inputfield).val());
        $(curObj).closest('.block-add-to-cart').find("input[name='Quantity']").trigger("change");
	});
};
