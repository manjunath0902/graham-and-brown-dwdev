'use strict';

var account = require('./account'),
	bonusProductsView = require('../bonus-products-view'),
	quickview = require('../quickview'),
	util = require('../util'),
	gtmevents= require('../gtmevents'),
	cartStoreInventory = require('../storeinventory/cart');

/**
 * @private
 * @function
 * @description Binds events to the cart page (edit item's details, bonus item's actions, coupon code entry)
 */
function initializeEvents() {
	$('#cart-table').on('click', '.item-edit-details a', function (e) {
		e.preventDefault();
		quickview.show({
			url: e.target.href,
			source: 'cart'
		});
		
	})
	.on('click', '.bonus-item-actions a, .item-details .bonusproducts a', function (e) {
		e.preventDefault();
		bonusProductsView.show(this.href);
	});

	// override enter key for coupon code entry
	$('form input[name$="_couponCode"]').on('keydown', function (e) {
		if (e.which === 13 && $(this).val().length === 0) { return false; }
		else if(e.which === 13){e.preventDefault();$("#add-coupon").click();} // JIRA PREV-30 : Cart page:  Coupon Code is not applying, when the user hit enter key.
	});
	
	//hide and show for coupon form
	if($("#cart-items-form .cart-footer .cart-coupon-code .cart-coupon-section1 a").length > 0){
		$("#cart-items-form .cart-footer .cart-coupon-code .cart-coupon-section1 a").on('click', function(){
			$(this).toggleClass('active');
			$("#cart-items-form .cart-footer .cart-coupon-code .cart-coupon-section2").slideToggle(150);
		});
	}
	if($('.cart-coupon-section2 div').hasClass('error')) {
		$("#cart-items-form .cart-footer .cart-coupon-code .cart-coupon-section1 a").addClass('active');
		$("#cart-items-form .cart-footer .cart-coupon-code .cart-coupon-section2").show();
	}
	
	//show tool tip on click of save for later for unauthenticated user
	if($("#cart-items-form #cart-table .cart-row .item-quantity-details .unauthenticated.add-to-wishlist").length > 0){
		$("#cart-items-form #cart-table .cart-row .item-quantity-details .unauthenticated.add-to-wishlist").on('click', function(e){
			$(this).parent().find(".errormsg").show();
		});
	}
	$('#primary').on('click', '.last-visited .grid-tile #add-to-cart,.product-listing .tiles-container #add-to-cart', function (e) {
		e.preventDefault();
		var pid = $(this).attr("productid");
		if(pid != '' && pid != null){
			addToCart(pid);
			gtmevents.addToCart($(this).closest('.grid-tile'));
		}
		$('html,body').animate({scrollTop: 0}, 500);
	});
	
	//For HP Integration: Navigating to respective PDP On click of customise button at recently viewed in cart page 
	$('#primary').on('click', '.cart-recommendations .product-listing .rec-customise', function (e) {
		e.preventDefault();
		var productid = $(this).closest('.tile_add_cart').find("input[name=pid]").val();
		var url = Urls.getProductUrl;
		url = util.appendParamToURL(url, 'pid', productid);
		window.location.href = url;
	});
	
	//updating the qty value of product 
	$(document).on("click", "#primary #cart-items-form .item-quantity a", function(e) {
	    e.preventDefault();
	    var curObj = $(this);
	    var inputfield = $(curObj).closest(".item-quantity-details").find("input.Quantity");
	    var inputValue = $(inputfield).val();
	    if ($(curObj).hasClass("prev-value")) {
	        if ($(inputfield).val() > 1) {
	            inputValue--;
	            $(inputfield).val(inputValue);
	            $('#update-cart').trigger('click');
	        }
	    } else if ($(curObj).hasClass("next-value")) {
	        if ($(inputfield).val() >= 0 && $(inputfield).val() < 99) {
	            inputValue++;
	            $(inputfield).val(inputValue);
	            $('#update-cart').trigger('click');
	        }
	    }
	    
	    //this code is commented for dynamically update bag page GB-564
	    
	    /*if($('#primary #cart-items-form .cart-footer .cart-order-totals').find('.updateQuantityError').length >0){
	    	$('#primary #cart-items-form .cart-footer .cart-order-totals').find('.updateQuantityError').remove();
	    }
	    var disable=false;
	    var error=false;
	    var basketQty=0;
	    $(this).closest("#primary #cart-items-form").find(".item-quantity-details .item-quantity").each(function(){
	    	if(parseInt($(this).find("input[name='hiddenQuantity']").val()) != parseInt($(this).find(".Quantity").val())){
	    		error=true;
	    	}
	    	if(parseInt($(this).find(".Quantity").val()) > 50){
	    		disable=true;
	    	}
	    	basketQty+=parseInt($(this).find(".Quantity").val());
	    });
	    if(error){
	    	$("#primary .continue-checkout").attr("disabled", true);
		    $('#primary #cart-items-form .cart-footer .cart-order-totals').find('button').after("<div class='updateQuantityError'>"+Resources.BASKETUPDATEQUANTITY +"</div>");
	    }else{
	    	if(!disable && basketQty < 100){
	    		$("#primary .continue-checkout").attr("disabled", false);
	    	}
	    }*/
	});
	
	//this code is commented for dynamically update bag page GB-564 
	
	/*$(document).find("#primary #cart-items-form .item-quantity").keyup(function(e) {
		e.preventDefault();
		if($('#primary #cart-items-form .cart-footer .cart-order-totals').find('.updateQuantityError').length >0){
			$('#primary #cart-items-form .cart-footer .cart-order-totals').find('.updateQuantityError').remove();
	    }
		var disable=false;
		var error=false;
		var basketQty=0;
		$(this).closest("#primary #cart-items-form").find(".item-quantity-details .item-quantity").each(function(){
	    	if(parseInt($(this).find("input[name='hiddenQuantity']").val()) != parseInt($(this).find(".Quantity").val())){
	    		error=true;
	    	}
	    	if(parseInt($(this).find(".Quantity").val()) > 50){
	    		disable=true;
	    	}
	    	basketQty+=parseInt($(this).find(".Quantity").val());
	    });
		if(error){
			$("#primary .continue-checkout").attr("disabled", true);
		    $('#primary #cart-items-form .cart-footer .cart-order-totals').find('button').after("<div class='updateQuantityError'>"+Resources.BASKETUPDATEQUANTITY +"</div>");
	    }else{
	    	if(!disable && basketQty < 100){
	    		$("#primary .continue-checkout").attr("disabled", false);
	    	}
	    }
	});*/
	//this is for dynamically update bag page GB-564 
	$(document).on('change','.Quantity',function(){
    	$('#update-cart').trigger('click');
    });
	$('#cart-items-form .item-quantity .Quantity').keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
             // Allow: Ctrl+C
            (e.keyCode == 67 && e.ctrlKey === true) ||
             // Allow: Ctrl+X
            (e.keyCode == 88 && e.ctrlKey === true) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

}

exports.init = function () {
	initializeEvents();
	if (SitePreferences.STORE_PICKUP) {
		cartStoreInventory.init();
	}
	account.initCartLogin();
};

//add to cart for featured products and recently viewed products
function addToCart(pid){
	var url = Urls.addProduct;
	var pid = pid;
	url = util.appendParamToURL(url, 'pid', pid);
	url = util.appendParamToURL(url, 'format', 'ajax');
	$.ajax({
		type: 'GET',
		dataType: 'html',
		url: url,
		success: function (response) {
			window.location = Urls.cartShow;
		},
		failure: function () {
			window.alert(Resources.SERVER_ERROR);
		}
	});
}
