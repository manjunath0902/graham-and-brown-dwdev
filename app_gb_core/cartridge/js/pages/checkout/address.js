'use strict';

var util = require('../../util');
var shipping = require('./shipping');
var formPrepare = require('../../pages/checkout/formPrepare');

/**
 * @function
 * @description Selects the first address from the list of addresses
 */
exports.init = function () {
	/*var $form = $('.address');
	// select address from list
	$('select[name$="_addressList"]', $form).on('change', function () {
		var selected = $(this).children(':selected').first();
		var selectedAddress = $(selected).data('address');
		if (!selectedAddress) { return; }
		util.fillAddressFields(selectedAddress, $form);
		shipping.updateShippingMethodList();
		// re-validate the form
		 JIRA PREV-95 : Highlighting the Credit card  details field, when the user select any saved address from the 'Select An Address' drop down.
		   (logged-in user) .Commented to prevent form from re-validation.
		
		//$form.validate().form();
	});*/
	var $shippingform = $('.address .shippingForm');
	// select address from list
	$('select[name$="_addressList"]', $shippingform).on('change', function () {
		var selected = $(this).children(':selected').first();
		var selectedAddress = $(selected).data('address');
		if (!selectedAddress) { return; }
		util.fillAddressFields(selectedAddress, $shippingform);
		shipping.updateShippingMethodList();
		// re-validate the form
		/* JIRA PREV-95 : Highlighting the Credit card  details field, when the user select any saved address from the 'Select An Address' drop down.
		   (logged-in user) .Commented to prevent form from re-validation.
		*/
		//$form.validate().form();
		formPrepare.init({
			continueSelector: '[name$="shippingAddress_save"]',
			formSelector:'[id$="singleshipping_shippingAddress"]'
		});
	});
	var $billingingform = $('.address .bilingForm');
	// select address from list
	$('select[name$="_addressList"]', $billingingform).on('change', function () {
		var selected = $(this).children(':selected').first();
		var selectedAddress = $(selected).data('address');
		if (!selectedAddress) { return; }
		util.fillAddressFields(selectedAddress, $billingingform);
		shipping.updateShippingMethodList();
		// re-validate the form
		/* JIRA PREV-95 : Highlighting the Credit card  details field, when the user select any saved address from the 'Select An Address' drop down.
		   (logged-in user) .Commented to prevent form from re-validation.
		*/
		//$form.validate().form();
		formPrepare.init({
			continueSelector: '[name$="shippingAddress_save"]',
			formSelector:'[id$="singleshipping_shippingAddress"]'
		});
	});
	
	//This change is for click ad collect 
	var $clickandcollectform = $(".click-and-collect-form .biling");
	// select address from list
	$('select[name$="_addressList"]', $clickandcollectform).on('change', function () {
		var selected = $(this).children(':selected').first();
		var selectedAddress = $(selected).data('address');
		if (!selectedAddress) { return; }
		util.fillAddressFields(selectedAddress, $clickandcollectform);
		shipping.updateShippingMethodList();
		formPrepare.init({
			continueSelector: '[name$="shippingAddress_save"]',
			formSelector:'[id$="singleshipping_shippingAddress"]'
		});
	});
	
};
