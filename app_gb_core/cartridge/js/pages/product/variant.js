'use strict';

var addToCart = require('./addToCart'),
	ajax = require('../../ajax'),
	image = require('./image'),
	progress = require('../../progress'),
	productStoreInventory = require('../../storeinventory/product'),
	tooltip = require('../../tooltip'),
	util = require('../../util'),
	minicart = require('../../minicart'),
	dialog = require('../../dialog'),
	availability = require('./availability'),
	bisn = require('../../bisn'),
	redeye = require('../../redeyetracking'),
	gtmevents= require('../../gtmevents'),
	validator = require('../../validator'),
	TPromise = require('promise');


/**
 * @description update product content with new variant from href, load new content to #product-content panel
 * @param {String} href - url of the new product variant
 **/
//Commented for GB-674 start
/*var updateContent = function (href) {
	var $pdpForm = $('.pdpForm');
	var qty = $pdpForm.find('input[name="Quantity"]').first().val();
	var params = {
		Quantity: isNaN(qty) ? '1' : qty,
		format: 'ajax',
		productlistid: $pdpForm.find('input[name="productlistid"]').first().val()
	};

	progress.show($('#pdpMain'));

	ajax.load({
		url: util.appendParamsToUrl(href, params),
		target: $('#product-content'),
		callback: function () {
			if (SitePreferences.STORE_PICKUP) {
				productStoreInventory.init();
			}
			image.replaceImages();
			$('#Quantity').trigger('change'); //JIRA PREV-235:PD page: 'Not Available' message is not displaying when more than Available Qty entered in Qty field.
			tooltip.init();
			//$('#product-content').trigger('ready');  PREVAIL- Added for Gigya integration
			availability.getAvailability;
		}
	});
};*/
//Commented for GB-674 end

//Added for GB-674 start
var updateContent = function (href) {
	window.location = href.replace('Product-Variation','Product-Show');
};
//Added for GB-674 end


module.exports = function () {
	var $pdpMain = $('#pdpMain');
	// hover on swatch - should update main image with swatch image
	/*$pdpMain.on('mouseenter mouseleave', '.swatchanchor', function () {
		var largeImg = $(this).data('lgimg'),
			$imgZoom = $pdpMain.find('.main-image'),
			$mainImage = $pdpMain.find('.primary-image');

		if (!largeImg) { return; }
		// store the old data from main image for mouseleave handler
		$(this).data('lgimg', {
			hires: $imgZoom.attr('href'),
			url: $mainImage.attr('src'),
			alt: $mainImage.attr('alt'),
			title: $mainImage.attr('title')
		});
		// set the main image
		image.setMainImage(largeImg);
	});*/

	// click on swatch - should replace product content with new variant
	$pdpMain.on('click', '.product-detail .swatchanchor', function (e) {
		e.preventDefault();
		//if ($(this).parents('li').hasClass('unselectable')) { return; }
		updateContent(this.href);
	});
	// change drop down variation attribute - should replace product content with new variant
	$pdpMain.on('change', '.variation-select', function () {
		if ($(this).val().length === 0) { return; }
		updateContent($(this).val());
	});
	
	//keyup qty validation
	$(".pdp-main .pdpForm .quantity #Quantity").keyup(function(e) {
        e.preventDefault();
        var curObj = $(this);
        var maxOrderQty = 50 ;
        var currentForm = curObj.closest("form[id]");
        var inputfield = $(curObj).closest(".product-add-to-cart").find("input[name='Quantity']");
        var inputValue = $(inputfield).val();
        var $closestQty = $(curObj).closest(".quantity");
        if (inputValue > maxOrderQty) {
                        currentForm.find('.add-to-cart').attr("disabled", true);
                        $('#add-all-to-cart').attr("disabled", true);
                        $('.span-messg').text(Resources.MAXORDERABLEQUANTITYERROR);
                        var col2Height = ($('.product-col-2.product-detail').outerHeight() - 533);
                                      $('.pdp-main .socialsharing.social-links').css('padding-bottom',col2Height);
        } else {
              currentForm.find('.add-to-cart').attr("disabled", false);
              $('#add-all-to-cart').attr("disabled", false);
              $('.span-messg').text('');
          }
      });
	
	//updating the qty value of product 
	$(document).on("click", ".pdp-main .pdpForm .quantity a", function(e) {
	    e.preventDefault();
	    var curObj = $(this);
	    var maxOrderQty = 50 ;
	    var currentForm = curObj.closest("form[id]");
	    var inputfield = $(curObj).closest(".product-add-to-cart").find("input[name='Quantity']");
	    var inputValue = $(inputfield).val();
	    var $closestQty = $(curObj).closest(".quantity");
	    if ($(curObj).hasClass("prev-value")) {
	        if ($(inputfield).val() > 1) {
	            inputValue--;
	            $(inputfield).val(inputValue);
	        }
	    } else if ($(curObj).hasClass("next-value")) {
	        if ($(inputfield).val() >= 0 && $(inputfield).val() < 99) {
	            inputValue++;
	            $(inputfield).val(inputValue);
	        }
	    }
	    checkQuantity(currentForm).then(function(resp){
	    	if(resp.success){
			    if ($(inputfield).val() > maxOrderQty) {
			        currentForm.find('.add-to-cart').attr("disabled", true);
			        $('#add-all-to-cart').attr("disabled", true);
			        $('.span-messg').text(Resources.MAXORDERABLEQUANTITYERROR);
			        var col2Height = ($('.product-col-2.product-detail').outerHeight() - 533);
					$('.pdp-main .socialsharing.social-links').css('padding-bottom',col2Height);
			    } else {
			        currentForm.find('.add-to-cart').attr("disabled", false);
			        $('#add-all-to-cart').attr("disabled", false);
			        $('.span-messg').text('');
			        $(curObj).closest('.product-add-to-cart').find("input[name='Quantity']").val($(inputfield).val());
			        $(curObj).closest('.product-add-to-cart').find("input[name='Quantity']").trigger("change");
			    }
	    	}else{
	    		currentForm.find('.add-to-cart').attr("disabled", true);
		        $('#add-all-to-cart').attr("disabled", true);
		        $('.span-messg').text(Resources.MAXORDERABLEQUANTITYERROR);
		        var col2Height = ($('.product-col-2.product-detail').outerHeight() - 533);
				$('.pdp-main .socialsharing.social-links').css('padding-bottom',col2Height);
	    	}
	    });
	});
	//updating the qty value of productset 
	$(document).on("click"," .qty-block a",function(e){
		e.preventDefault();
		var curObj = $(this);
		var inputfield = $(this).closest(".block-add-to-cart").find("input[name='Quantity']");
		var inputValue = $(inputfield).val();
		var $closestQty = $(curObj).closest(".quantity");
		if($(curObj).hasClass("prev-value")) {
			if($(inputfield).val()  > 1) {
				inputValue --;
				$(inputfield).val(inputValue);
			}
		}
		
		else if($(curObj).hasClass("next-value")) {
			if($(inputfield).val()  >= 0 && $(inputfield).val() < 99) {
				inputValue ++;
				$(inputfield).val(inputValue);
			}
		}
		$(curObj).closest('.block-add-to-cart').find("input[name='Quantity']").val($(inputfield).val());
		$(curObj).closest('.block-add-to-cart').find("input[name='Quantity']").trigger("change");
	});
	
	//QTY field validation 
	$(".pdp-main .pdpForm .inventory .quantity").keyup(function(e){
		e.preventDefault();
		var  text = $(this).closest(".product-add-to-cart").find("input[name='Quantity']");
		var txt = text.val();
		var regex=/[^\d]/g;       
		if(txt != undefined && txt != '' && regex.test(txt)){
		   $(text).val(txt.replace(regex, ''));
		   return false;
		}else{
			return true;
		}
	});
	
	//productset qty field validation
	$('.product-set-details #Quantity').keydown(function (e) {
	   // Allow: backspace, delete, tab, escape, enter and .
	   if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
	        // Allow: Ctrl+A
	       (e.keyCode == 65 && e.ctrlKey === true) ||
	        // Allow: Ctrl+C
	       (e.keyCode == 67 && e.ctrlKey === true) ||
	        // Allow: Ctrl+X
	       (e.keyCode == 88 && e.ctrlKey === true) ||
	        // Allow: home, end, left, right
	       (e.keyCode >= 35 && e.keyCode <= 39)) {
	            // let it happen, don't do anything
	            return;
	   }
	   // Ensure that it is a number and stop the keypress
	   if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
	       e.preventDefault();
	   }
	});

	
	//save for later functionality
	$(document).on("click",".pdp-main .pdpForm .saveforlater button",function(e){
		e.preventDefault();
		var url = Urls.addToWishlist;
		var pid = $("input[name=pid]").val();
		var Quantity = $("input[name=Quantity]").val();
        url = util.appendParamToURL(url, 'pid', pid);
        url = util.appendParamToURL(url, 'Quantity', Quantity);
        url = util.appendParamToURL(url, 'format', 'ajax');

		$.ajax({
			type: 'GET',
			dataType: 'json',
			url: url,
			success: function (response) {
				if(response.success){
					if($(".pdp-main .pdpForm .saveforlater").length > 0){
						$(".pdp-main .pdpForm .saveforlater button").addClass("added");
					}
				}else{
					if($(".pdp-main .pdpForm .saveforlater .errormsg").length > 0){
						$(".pdp-main .pdpForm .saveforlater .errormsg").show();
						$('.pdp-main .pdpForm .saveforlater .errormsg .close-error-msg').on('click', function(){
							$('.pdp-main .pdpForm .saveforlater .errormsg').hide();
						});
						$(document).mouseup(function (e){
						    var container = $(".saveforlater .errormsg");
						    if (!container.is(e.target) && container.has(e.target).length === 0) {
						        container.hide();
						    }
						});
					}
				}
			}
		});
	});
	
	//order a sample product functionality
	$(document).on("click",".pdp-main .pdpForm .orderasample button",function(e){
		e.preventDefault();
		var pid = $(this).attr("pid");
		if(pid != '' && pid != null){
			addSampleToCart(pid);
		}
	});
	//order a sample product functionality when 1 or more products r there
	$(document).on("change",".pdp-main .pdpForm .orderasample #input-select",function(e){
		e.preventDefault();
		var pid = $(this).val();
		if(pid != '' && pid != null){
			addSampleToCart(pid);
		}
	});	
	
	$(document).on("click",".pdp-main .pdpForm .bisn .emailme",function(e){
		e.preventDefault();
		var url = Urls.emailMe;
		var pid = $(this).closest('form').find('#pid').val();
		url = util.appendParamToURL(url, 'pid', pid);
		url = util.appendParamToURL(url, 'format', 'ajax');
		$.ajax({
			type: 'GET',
			dataType: 'html',
			url: url,
			success: function (response) {
				$(".pdp-main .pdpForm .product-actions").html(response);
				bisn.init();
			}
		});
	});
	//roll calculator functionality
	$(document).on("click",".pdp-main .pdpForm .calculator a",function(e){
		e.preventDefault();
		var url = Urls.rollCalculator;
		var isProductSet = ($("#isProductSet").val() == "true") ? true : false;
		
		if(isProductSet){
			if($(this).closest("form").find("#salesprice").length > 0){
				var price = $(this).closest("form").find("#salesprice").val();
				url = util.appendParamToURL(url, 'price', price);
			}else{
				if($(this).closest("form").find("#standardprice").length > 0){
					var price = $(this).closest("form").find("#standardprice").val();
					url = util.appendParamToURL(url, 'price', price);
				}
			}
			if($(this).closest("form").find("#pid").length > 0){
				var pid = $(this).closest("form").find("#pid").val();
				url = util.appendParamToURL(url, 'pid', pid);
			}
		}else{
			if($("#pdpMain #product-content .product-price #salesprice").length > 0){
				var price = $("#pdpMain #product-content .product-price #salesprice").val();
				url = util.appendParamToURL(url, 'price', price);
			}else{
				if($("#pdpMain #product-content .product-price #standardprice").length > 0){
					var price = $("#pdpMain #product-content .product-price #standardprice").val();
					url = util.appendParamToURL(url, 'price', price);
				}
			}
			if($("#pdpMain #product-content .pdpForm #pid").length > 0){
				var pid = $("#pdpMain #product-content .pdpForm #pid").val();
				url = util.appendParamToURL(url, 'pid', pid);
			}
		}
		
		url = util.appendParamToURL(url, 'format', 'ajax');
		dialog.open({
			url: url,
			options: {
				height: 540,
				dialogClass: "roll-Calculator",
			},
			callback: function () {
				calculator.calculate();
				calculator.addSavedProductsToCart();
				calculator.removeRoom();
				roll_calculator();
				calculator.validate();
			}
		});
	});
	$pdpMain.on('click', '.social-links .share-icon', function (e) {
		var isProductSet = ($("#isProductSet").val() == "true") ? true : false;
		if(isProductSet){
			var socialName = $(this).attr("data-share");
			var prodId = $pdpMain.find(".product-add-to-cart input[name='pid']").val();
			var prodName = $pdpMain.find(".product-add-to-cart input[name='name']").val();
		}else{
			var socialName = $(this).attr("data-share");
			var prodId = $pdpMain.find("input[name='pid']").val();
			var prodName = $pdpMain.find("input[name='name']").val();
		}
		redeye.socialShare(socialName,prodId);
		gtmevents.productShare(prodId,prodName,socialName);
	});
	
	//colour card request
	$('#colourcardrequest').click(function(e) {
		e.preventDefault();
		var url = Urls.colourcard;
		dialog.open({
			url: url,
			options: {
			dialogClass : 'colorcard-popup',
			width : 510,
			open: function () {
			validator.init();
			$('#request-for-paintcard').click(function(e) {
					e.preventDefault(); 
					var $form = $(this).closest('form');
					if (!$form.valid()) {
						return false;
					}
					var url = $form.attr('action');
					   var name = $("#dwfrm_paintcard_customer_name").val();
					   var email = $("#dwfrm_paintcard_customer_email").val();
					   var address1 = $("#dwfrm_paintcard_customer_address1").val();
					   var address2 = $("#dwfrm_paintcard_customer_address2").val();
					   var city = $("#dwfrm_paintcard_customer_city").val();
					   var state = $("#dwfrm_paintcard_customer_state").val();
					   var postal = $("#dwfrm_paintcard_customer_postal").val();
					   var subscribe = $("#dwfrm_paintcard_customer_subscribe").val();
					   url = util.appendParamToURL(url, 'name', name);
					   url = util.appendParamToURL(url, 'email', email);
					   url = util.appendParamToURL(url, 'address1', address1);
					   url = util.appendParamToURL(url, 'address2', address2);
					   url = util.appendParamToURL(url, 'city', city);
					   url = util.appendParamToURL(url, 'state', state);
					   url = util.appendParamToURL(url, 'postal', postal);
					   url = util.appendParamToURL(url, 'subscribe', subscribe);
					   var action = $form.find('#request-for-paintcard').attr('name');
						url = util.appendParamToURL(url, action, "x");
						url = util.appendParamToURL(url, 'format', 'ajax');
					   $.ajax({
						   type: 'GET',
						   dataType: 'json',
						   url:url,
							success: function (response) {
								dialog.close();
								redeye.colourCard('colourcard',$form);
								var data = "<div class='paintcard'>"+ Resources.COLOURCARDCONFIRMATION +"</div>";
								dialog.open({
									html:data,
									options: {
										dialogClass : 'paintcard custom-popup',
										width : 390
									},
									callback: function () {
									}
								});
						    },
						  });
			});
			}
			  }
		});
	});
	$(".fa-pinterest-p").click(function(e) {
        e.preventDefault();
        var media = $(".product-primary-image div.slick-current a").find("img").attr("src");
        var href = $(this).attr('href');
        href = util.appendParamToURL(href, 'media', media);
        window.open(href,'_blank');
	});
	$(".fa-facebook").click(function(e) {
        e.preventDefault();
        var picture = $(".product-primary-image div.slick-current a").find("img").attr("src");
        var href = $(this).attr('href');
        href = util.appendParamToURL(href, 'picture', picture);
        window.open(href,'_blank');
	});

};

//Paint or Roll Calculator
var calculator = {
	calculate : function () {
		if($(".roll-Calculator").length > 0){
			$(".roll-Calculator").off('click', '.calculator-form .calculate');
			$(".roll-Calculator").on('click', '.calculator-form .calculate', function (e) {
				e.preventDefault();
				var $form = $("#calculator-form");
				if (!$form.valid()) {
					return false;
				}
				var url = $form.attr('action');
				if($(".roll-Calculator #calculator-form input[id$=height]").length > 0){
					var height = $(".roll-Calculator #calculator-form input[id$=height]").val();
					url = util.appendParamToURL(url, 'height', height);
				}
				if($(".roll-Calculator #calculator-form input[id$=width]").length > 0){
					var width = $(".roll-Calculator #calculator-form input[id$=width]").val();
					url = util.appendParamToURL(url, 'width', width);
				}
				var isProductSet = ($("#isProductSet").val() == "true") ? true : false;
				
				if(isProductSet){
					if($(this).closest("form").find("#salesprice").length > 0){
						var price = $(this).closest("form").find("#salesprice").val();
						url = util.appendParamToURL(url, 'price', price);
					}else{
						if($(this).closest("form").find("#standardprice").length > 0){
							var price = $(this).closest("form").find("#standardprice").val();
							url = util.appendParamToURL(url, 'price', price);
						}
					}
					if($(this).closest("form").find("#pid").length > 0){
						var pid = $(this).closest("form").find("#pid").val();
						url = util.appendParamToURL(url, 'pid', pid);
					}
				}else{
					if($("#pdpMain #product-content .product-price #salesprice").length > 0){
						var price = $("#pdpMain #product-content .product-price #salesprice").val();
						url = util.appendParamToURL(url, 'price', price);
					}else{
						if($("#pdpMain #product-content .product-price #standardprice").length > 0){
							var price = $("#pdpMain #product-content .product-price #standardprice").val();
							url = util.appendParamToURL(url, 'price', price);
						}
					}
					if($("#pdpMain #product-content .pdpForm #pid").length > 0){
						var pid = $("#pdpMain #product-content .pdpForm #pid").val();
						url = util.appendParamToURL(url, 'pid', pid);
					}
				}
				
				var calculate = $form.find('.calculate').attr('name');
				url = util.appendParamToURL(url, calculate, "x");
				url = util.appendParamToURL(url, 'format', 'ajax');
				$.ajax({
					type: 'GET',
					dataType: 'html',
					url: url,
					success: function (response) {
						$('#calculator').html(response);
						calculator.saveDimensions();
						calculator.addToCart();
						calculator.addSavedProductsToCart();
						calculator.removeRoom();
						roll_calculator();
						calculator.validate();
					}
				});
			});
		}
	},
	saveDimensions : function () {
		if($(".roll-Calculator .calculator-form .save").length > 0){
			$(".roll-Calculator").off('click', '.calculator-form .save');
			$(".roll-Calculator").on('click', '.calculator-form .save', function (e) {
				e.preventDefault();
				var $form = $("#calculator-form");
				if (!$form.valid()) {
					return false;
				}
				var url = $form.attr('action');
				if($(".roll-Calculator #calculator-form #height").length > 0){
					var height = $(".roll-Calculator #calculator-form #height").val();
					url = util.appendParamToURL(url, 'height', height);
				}
				if($(".roll-Calculator #calculator-form #width").length > 0){
					var width = $(".roll-Calculator #calculator-form #width").val();
					url = util.appendParamToURL(url, 'width', width);
				}
				var isProductSet = ($("#isProductSet").val() == "true") ? true : false;
				
				if(isProductSet){
					if($(this).closest("form").find("#salesprice").length > 0){
						var price = $(this).closest("form").find("#salesprice").val();
						url = util.appendParamToURL(url, 'price', price);
					}else{
						if($(this).closest("form").find("#standardprice").length > 0){
							var price = $(this).closest("form").find("#standardprice").val();
							url = util.appendParamToURL(url, 'price', price);
						}
					}
					if($(this).closest("form").find("#pid").length > 0){
						var pid = $(this).closest("form").find("#pid").val();
						url = util.appendParamToURL(url, 'pid', pid);
					}
				}else{
					if($("#pdpMain #product-content .product-price #salesprice").length > 0){
						var price = $("#pdpMain #product-content .product-price #salesprice").val();
						url = util.appendParamToURL(url, 'price', price);
					}else{
						if($("#pdpMain #product-content .product-price #standardprice").length > 0){
							var price = $("#pdpMain #product-content .product-price #standardprice").val();
							url = util.appendParamToURL(url, 'price', price);
						}
					}
					if($("#pdpMain #product-content .pdpForm #pid").length > 0){
						var pid = $("#pdpMain #product-content .pdpForm #pid").val();
						url = util.appendParamToURL(url, 'pid', pid);
					}
				}
				var save = $form.find('.save').attr('name');
				url = util.appendParamToURL(url, save, "x");
				url = util.appendParamToURL(url, 'format', 'ajax');
				$.ajax({
					type: 'GET',
					dataType: 'html',
					url: url,
					success: function (response) {
						$('#calculator').html(response);
						calculator.saveRoom();
						calculator.addToCart();
						calculator.addSavedProductsToCart();
						calculator.removeRoom();
						roll_calculator();
						calculator.validate();
					}
				});
			});
		}
	},
	addToCart : function () {
		if($(".roll-Calculator .calculator-form .addtocart").length > 0){
			$(".roll-Calculator").off('click', '.calculator-form .addtocart');
			$(".roll-Calculator").on('click', '.calculator-form .addtocart', function (e) {
				e.preventDefault();
				var $form = $("#calculator-form");
				if (!$form.valid()) {
					return false;
				}
				var url = Urls.addProduct;
				if($(".roll-Calculator .calculator-form #count").length > 0){
					var quantity = $(".roll-Calculator .calculator-form #count").val();
					url = util.appendParamToURL(url, 'Quantity', quantity);
				}
				var isProductSet = ($("#isProductSet").val() == "true") ? true : false;
				
				if(isProductSet){
					if($(this).closest("form").find("#pid").length > 0){
						var pid = $(this).closest("form").find("#pid").val();
						url = util.appendParamToURL(url, 'pid', pid);
					}
				}else{
					if($("#pdpMain #product-content .pdpForm #pid").length > 0){
						var pid = $("#pdpMain #product-content .pdpForm #pid").val();
						url = util.appendParamToURL(url, 'pid', pid);
					}
				}
				var url = util.appendParamToURL(url, 'format', 'ajax');
				$.ajax({
					type: 'GET',
					dataType: 'html',
					url: url,
					success: function (response) {
						minicart.show(response);
						if($(".roll-Calculator .calculator-form .addtocart").length > 0){
							$(".roll-Calculator .calculator-form .addtocart").hide();
						}
						calculator.removeRoom();
						roll_calculator();
						calculator.validate();
					},
					failure: function () {
						window.alert(Resources.SERVER_ERROR);
					}
				});
			});
		}
	},
	saveRoom : function () {
		if($(".roll-Calculator .calculator-form .saveroom").length > 0){
			$(".roll-Calculator").off('click', '.calculator-form .saveroom');
			$(".roll-Calculator").on('click', '.calculator-form .saveroom', function (e) {
				e.preventDefault();
				var $form = $("#calculator-form");
				if (!$form.valid()) {
					return false;
				}
				var url = $form.attr('action');
				if($(".roll-Calculator #calculator-form #height").length > 0){
					var height = $(".roll-Calculator #calculator-form #height").val();
					url = util.appendParamToURL(url, 'height', height);
				}
				if($(".roll-Calculator #calculator-form #width").length > 0){
					var width = $(".roll-Calculator #calculator-form #width").val();
					url = util.appendParamToURL(url, 'width', width);
				}
				var isProductSet = ($("#isProductSet").val() == "true") ? true : false;
				
				if(isProductSet){
					if($(this).closest("form").find("#salesprice").length > 0){
						var price = $(this).closest("form").find("#salesprice").val();
						url = util.appendParamToURL(url, 'price', price);
					}else{
						if($(this).closest("form").find("#standardprice").length > 0){
							var price = $(this).closest("form").find("#standardprice").val();
							url = util.appendParamToURL(url, 'price', price);
						}
					}
					if($(this).closest("form").find("#pid").length > 0){
						var pid = $(this).closest("form").find("#pid").val();
						url = util.appendParamToURL(url, 'pid', pid);
					}
				}else{
					if($("#pdpMain #product-content .product-price #salesprice").length > 0){
						var price = $("#pdpMain #product-content .product-price #salesprice").val();
						url = util.appendParamToURL(url, 'price', price);
					}else{
						if($("#pdpMain #product-content .product-price #standardprice").length > 0){
							var price = $("#pdpMain #product-content .product-price #standardprice").val();
							url = util.appendParamToURL(url, 'price', price);
						}
					}
					if($("#pdpMain #product-content .pdpForm #pid").length > 0){
						var pid = $("#pdpMain #product-content .pdpForm #pid").val();
						url = util.appendParamToURL(url, 'pid', pid);
					}
				}
				if($(".roll-Calculator #calculator-form #dwfrm_calculator_roomname").length > 0){
					var name = $(".roll-Calculator #calculator-form #dwfrm_calculator_roomname").val();
					url = util.appendParamToURL(url, 'name', name);
				}
				
				var saveroom = $form.find('.saveroom').attr('name');
				url = util.appendParamToURL(url, saveroom, "x");
				url = util.appendParamToURL(url, 'format', 'ajax');
				$.ajax({
					type: 'GET',
					dataType: 'html',
					url: url,
					success: function (response) {
						$('#calculator').html(response);
						calculator.calculate();
						calculator.addToCart();
						calculator.addSavedProductsToCart();
						calculator.removeRoom();
						roll_calculator();
						calculator.validate();
					}
				});
			});
		}
	},
	addSavedProductsToCart : function(){
		if($(".roll-Calculator #calculator:visible .savedrooms .parent_room .add-to-cart").length > 0){
			$(".roll-Calculator").off('click', '#calculator:visible .savedrooms .parent_room .add-to-cart');
			$(".roll-Calculator").on('click', '#calculator:visible .savedrooms .parent_room .add-to-cart', function (e) {
				e.preventDefault();
				var url = Urls.addProduct;
				if($(this).closest(".parent_room").find("#count").length > 0){
					var quantity = $(this).closest(".parent_room").find("#count").val();
					url = util.appendParamToURL(url, 'Quantity', quantity);
				}
				var isProductSet = ($("#isProductSet").val() == "true") ? true : false;
				
				if(isProductSet){
					if($(this).closest("form").find("#pid").length > 0){
						var pid = $(this).closest("form").find("#pid").val();
						url = util.appendParamToURL(url, 'pid', pid);
					}
				}else{
					if($("#pdpMain #product-content .pdpForm #pid").length > 0){
						var pid = $("#pdpMain #product-content .pdpForm #pid").val();
						url = util.appendParamToURL(url, 'pid', pid);
					}
				}
				var url = util.appendParamToURL(url, 'format', 'ajax');
				$.ajax({
					type: 'GET',
					dataType: 'html',
					url: url,
					success: function (response) {
						minicart.show(response);
						if($(".roll-Calculator .calculator-form .addtocart").length > 0){
							$(".roll-Calculator .calculator-form .addtocart").hide();
						}
						calculator.removeRoom();
						roll_calculator();
						calculator.validate();
					},
					failure: function () {
						window.alert(Resources.SERVER_ERROR);
					}
				});
			});
		}
	},
	removeRoom : function () {
		if($(".roll-Calculator #calculator:visible .savedrooms .remove").length > 0){
			$(".roll-Calculator").off('click', '#calculator:visible .savedrooms .remove');
			$(".roll-Calculator").on('click', '#calculator:visible .savedrooms .remove', function (e) {
				e.preventDefault();
				var $form = $("#calculator-form");
				var url = $form.attr('action');
				var isProductSet = ($("#isProductSet").val() == "true") ? true : false;
				
				if(isProductSet){
					if($(this).closest("form").find("#salesprice").length > 0){
						var price = $(this).closest("form").find("#salesprice").val();
						url = util.appendParamToURL(url, 'price', price);
					}else{
						if($(this).closest("form").find("#standardprice").length > 0){
							var price = $(this).closest("form").find("#standardprice").val();
							url = util.appendParamToURL(url, 'price', price);
						}
					}
					if($(this).closest("form").find("#pid").length > 0){
						var pid = $(this).closest("form").find("#pid").val();
						url = util.appendParamToURL(url, 'pid', pid);
					}
				}else{
					if($("#pdpMain #product-content .product-price #salesprice").length > 0){
						var price = $("#pdpMain #product-content .product-price #salesprice").val();
						url = util.appendParamToURL(url, 'price', price);
					}else{
						if($("#pdpMain #product-content .product-price #standardprice").length > 0){
							var price = $("#pdpMain #product-content .product-price #standardprice").val();
							url = util.appendParamToURL(url, 'price', price);
						}
					}
					if($("#pdpMain #product-content .pdpForm #pid").length > 0){
						var pid = $("#pdpMain #product-content .pdpForm #pid").val();
						url = util.appendParamToURL(url, 'pid', pid);
					}
				}
				var removeRoom = $('#calculator:visible .savedrooms .remove').attr('name');
				url = util.appendParamToURL(url, removeRoom, "x");
				var index = $(this).attr('index');
				url = util.appendParamToURL(url, "index", index-1);
				url = util.appendParamToURL(url, 'format', 'ajax');
				$.ajax({
					type: 'GET',
					dataType: 'html',
					url: url,
					success: function (response) {
						$('#calculator .savedrooms').empty();
						$('#calculator .savedrooms').replaceWith(response);
						calculator.saveRoom();
						calculator.addToCart();
						calculator.addSavedProductsToCart();
						calculator.calculate();
						roll_calculator();
						calculator.validate();
					},
					failure: function () {
						window.alert(Resources.SERVER_ERROR);
					}
				});
			});
		}
	},
	validate : function () {
		$(".roll-Calculator #calculator-form input[id$=height],.roll-Calculator #calculator-form input[id$=width]").keyup(function(e) {
			e.preventDefault();
			var  text = $(this).val();
		    var regex=/[^\d.]/g;       
		    if(regex.test(text)){
		           $(this).val(text.replace(regex, ''));
		           return false;
		    }else{
		    	return true;
			}
		});
	}
};
 function addSampleToCart(pid){
	var url = Urls.addProduct;
	var pid = pid;
	url = util.appendParamToURL(url, 'pid', pid);
	url = util.appendParamToURL(url, 'format', 'ajax');
	$.ajax({
		type: 'GET',
		dataType: 'html',
		url: url,
		success: function (response) {
			minicart.show(response);
			if($(".pdp-main .pdpForm .orderasample").length > 0){
				$(".pdp-main .pdpForm .orderasample").addClass("added");
			}
			if($(".pdp-main .pdpForm .orderasample #input-select").length > 0){
				$(".pdp-main .pdpForm .orderasample #input-select").val('');
				//custom selectbox
				$('.pdp-main .pdpForm .orderasample #input-select').data('selectBox-selectBoxIt').destroy();
                $('.pdp-main .pdpForm .orderasample #input-select').selectBoxIt();
			}
		},
		failure: function () {
			window.alert(Resources.SERVER_ERROR);
		}
	});
}

 //custom-scroll
 function roll_calculator() {
	 if ($('.calculator .enscroll-track').length < 1 ) {
		 $('.calculator .savedrooms').enscroll({
			verticalTrackClass: 'track1',
			verticalHandleClass: 'handle1',
			drawScrollButtons: true,
			addPaddingToPane: false,
			scrollUpButtonClass: 'scroll-up1',
			scrollDownButtonClass: 'scroll-down1'
		});
	 }
	 else {
		 $('.calculator .enscroll-track').parent().remove();
		 $('.calculator .savedrooms').enscroll({
			verticalTrackClass: 'track1',
			verticalHandleClass: 'handle1',
			drawScrollButtons: true,
			addPaddingToPane: false,
			scrollUpButtonClass: 'scroll-up1',
			scrollDownButtonClass: 'scroll-down1'
		});
	 }
	 
	 // fix for IE 
	 if ($('.savedrooms .parent_room').length < 4 ) {
		 $('.calculator .enscroll-track').parent().remove();
		 $('.calculator .savedrooms').css('padding-right', '0');
	 }
	 
	 if ($('.calculator .enscroll-track').parent().is(':visible')) {
		 $('.calculator .savedrooms').css('padding-right', '35px');
	 }
	 else {
		 $('.calculator .savedrooms').css('padding-right', '0');
	 }
}
 
function checkQuantity(form) {
	var checkQuantityUrl = Urls.checkQuantity;
	checkQuantityUrl = util.appendParamToURL(checkQuantityUrl, 'pid', form.find("input[name='pid']").val());
	checkQuantityUrl = util.appendParamToURL(checkQuantityUrl, 'Quantity', form.find("input[name='Quantity']").val());
	return TPromise.resolve($.ajax({
		type: 'POST',
		async: false,//JIRA PREV-305:SG issue: No of items in cart value not displayed as expected.Added async:false.
		url: checkQuantityUrl
	}));
}

//HPWallArt pop-up box functionality
$('.button-fancy-large.customise-your-wall.prm-btn').on('click',function(e){
	e.preventDefault();
	var url = Urls.HPWallArt;
	url = util.appendParamToURL(url, 'format', 'ajax');
	dialog.open({
        url: url,
        options: {
            width: 487,
            dialogClass : 'hpwallart-popup'
        },
        callback: function() {
            util.selectbox();
            hpwallart.caluclatePrice();
            hpwallart.launchWindow();
            hpwallart.validations();
        }
    });
});		

$('.get-the-look-content .get-look-tile .getthelook-customise').on('click', function(e){
	e.preventDefault();
	var productid = $(this).closest('.get-look-tile').find("input[name=pid]").val();
	var url = Urls.getProductUrl;
	url = util.appendParamToURL(url, 'pid', productid);
	window.location.href = url;
});

var hpwallart = {
		caluclatePrice : function(){
			$('#dwfrm_hpwallart_Price').attr('disabled',true);
            $('#CalculatePriceForHP').on('click',function(e){
            	e.preventDefault();
            	var $form = $("#HPWallArtFormOne");
            	var url = $form.attr('action');
            	if($('#HPWallArtFormOne #dwfrm_hpwallart_Width').length > 0){
            		var width = $('#HPWallArtFormOne #dwfrm_hpwallart_Width').val();
					url = util.appendParamToURL(url, 'width', width);
            	}
            	
            	if($('#HPWallArtFormOne #dwfrm_hpwallart_Hight').length > 0){
					var hight = $('#HPWallArtFormOne #dwfrm_hpwallart_Hight').val();
					url = util.appendParamToURL(url, 'hight', hight);
				}
            	
				if($("#pdpMain #product-content .product-price #salesprice").length > 0){
					var price = $("#pdpMain #product-content .product-price #salesprice").val();
					url = util.appendParamToURL(url, 'price', price);
				}else{
					if($("#pdpMain #product-content .product-price #standardprice").length > 0){
						var price = $("#pdpMain #product-content .product-price #standardprice").val();
						url = util.appendParamToURL(url, 'price', price);
					}
				}
				if($("#pdpMain #product-content .pdpForm #pid").length > 0){
					var pid = $("#pdpMain #product-content .pdpForm #pid").val();
					url = util.appendParamToURL(url, 'pid', pid);
				}
				
				var calculate = $form.find('#CalculatePriceForHP').attr('name');
				
				url = util.appendParamToURL(url, calculate, "x");
				url = util.appendParamToURL(url, 'format', 'ajax');
				var hpprodprice = $('input[id=salesprice]').val();
				
				$.ajax({
					url: url,
					success: function (response) {
						//Converting area from Sq Cms to Sq Mts
						var area = (hight*width) * 0.0001;
						var totalprice = (area * hpprodprice).toFixed(2);
						$('#dwfrm_hpwallart_Price').val(totalprice);
					}
				});
				
            	});
		},
		
		launchWindow : function(){
			$('#launchWindowForHP').on('click',function(e){
            	e.preventDefault();
            	var hpPID = $("#pdpMain #product-content .pdpForm #pid").val();
            	if(util.GetCookie("HPProductID") == ""){
            		util.SetCookie("HPProductID", hpPID , 1);
            	}else{
            		var hpcookieval = util.GetCookie("HPProductID");
            		if(hpPID != hpcookieval){
            			//Delete existing cookie and create new cookie if PDP product-ID & cookie value(productID) doesn't match 
            			util.DeleteCookie("HPProductID");
            			util.SetCookie("HPProductID", hpPID , 1);
            		}
            	}
                var url = "https://designer.hpwallart.com/grahambrown?web_link=true&sku=WA_WC";
            	
            	if($('input[id=customerno]').val() != undefined && $('input[id=customerno]').val() != 'null'){
            		var customernumber = $('input[id=customerno]').val();
            		customernumber = "GB"+customernumber;
            		url = util.appendParamToURL(url, 'auth_token',customernumber);
            		url = util.appendParamToURL(url, 'user_logged_in','true');
            	}else{
            		url = util.appendParamToURL(url, 'user_logged_in','false');
            	}
         	
            	if($('#HPWallArtFormOne #dwfrm_hpwallart_Hight').length > 0){
					var height = $('#HPWallArtFormOne #dwfrm_hpwallart_Hight').val();
				}
            	if($('#HPWallArtFormOne #dwfrm_hpwallart_Width').length > 0){
            		var width = $('#HPWallArtFormOne #dwfrm_hpwallart_Width').val();
            	}
            	if(height != undefined && width != undefined){
            		url = util.appendParamToURL(url, 'size',width+"x"+height);
            	}
            	
            	url = util.appendParamToURL(url, 'content_context_token', hpPID);
            	
            	window.location = url;
            	
            });
		},
		
		validations : function(){
			$("#dwfrm_hpwallart_Width,#dwfrm_hpwallart_Hight").keyup(function(e) {
				e.preventDefault();
				var  text = $(this).val();
			    var regex=/[^\d.]/g;       
			    if(regex.test(text)){
			           $(this).val(text.replace(regex, ''));
			           return false;
			    }else{
			    	return true;
				}
			});
		}
};