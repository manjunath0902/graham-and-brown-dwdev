'use strict';
var dialog = require('../../dialog'),
	util = require('../../util');

var zoomMediaQuery = matchMedia('(min-width: 1024px)');

/**
 * @description Enables the zoom viewer on the product detail page
 * @param zmq {Media Query List}
 */
var loadZoom = function (zmq) {
	var $imgZoom = $('#pdpMain .main-image'),
		hiresUrl;
	
	if(util.isMobile()) {
		 $('.pinch_zoom').each(function () {
	           new RTP.PinchZoom($(this), {});
	      });
	}
	
};

//zoomMediaQuery.addListener(loadZoom);

/**
 * @description Sets the main image attributes and the href for the surrounding <a> tag
 * @param {Object} atts Object with url, alt, title and hires properties
 */
var setMainImage = function (atts) {
	$('#pdpMain .primary-image').attr({
		src: atts.url,
		alt: atts.alt,
		title: atts.title
	});
	if (!dialog.isActive() && !util.isMobile()) {
		$('#pdpMain .main-image').attr('href', atts.hires);
	}
	slickInit();
	util.selectbox();
	loadZoom();
};

var slickInit = function(){
	if($(window).width() < 767) {
		if($('.product-primary-image .mob-hide').length > 0) {
			$('.product-primary-image .mob-hide').remove();
		}
	}
	
	/*if($('.product-col-1 .product-primary-image').hasClass('slick-slider')){
		$('.product-col-1 .product-primary-image').slick('unslick');
	}*/
	

	
	$('.product-col-1 .product-primary-image').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  infinite: true,
	  arrows: true,
	  vertical: false,
	  asNavFor: '.product-col-1 #thumbnails ul'
	});
	
	
	/*if($('.product-col-1 #thumbnails ul').hasClass('slick-slider')){
		$('.product-col-1 #thumbnails ul').slick('unslick');
	}*/

	$('.product-col-1 #thumbnails ul').slick({
	  slidesToShow: 4,
      slidesToScroll: 1,
	  arrows: true,
	  infinite: true,
	  vertical: true,
	  focusOnSelect: true,
	  asNavFor: '.product-col-1 .product-primary-image'
	});
	
		
	/*only for 2 primary image start*/
	/*if($('.product-col-1 .product-primary-image.product-slider-twoimages').hasClass('slick-slider')){
		$('.product-col-1 .product-primary-image.product-slider-twoimages').slick('unslick');
	}
	$('.product-col-1 .product-primary-image.product-slider-twoimages').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  infinite: true,
	  arrows: true,
	  focusOnSelect: true,
	  asNavFor: '.product-col-1 #thumbnails ul',
		  responsive: [{
	          breakpoint: 1023,
	          settings: {
	        	  speed:100
	          }
	      }]
	});*/
	/*only for 2 primary image end*/
	$('.get-the-look-products').slick({
		  slidesToShow: 4,
		  slidesToScroll: 1,
		  infinite: true,
		  arrows: true
	});
	if($(window).width() < 768) {
		if($('.get-the-look-products').hasClass('slick-slider')){
			$('.get-the-look-products').slick('unslick');
		}
		
    	$('.get-the-look-products').slick({
    		responsive: [{
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                	slidesToScroll: 1,
                	dots: true
                    //dotsClass: 'custom_paging',
                    //customPaging: function (slider, i) {
                        //return  (i + 1) + '/' + slider.slideCount;
                    //},
          		  	//infinite: true,
          		  	//speed: 300
                }
            }]
    	});
    }
}
/**
 * @description Replaces the images in the image container, for eg. when a different color was clicked.
 */
var replaceImages = function () {
	var $newImages = $('#update-images'),
		$imageContainer = $('#pdpMain .product-image-container');
	if ($newImages.length === 0) { return; }

	$imageContainer.html($newImages.html());
	$newImages.remove();
	slickInit();
	util.selectbox();
	loadZoom();
};

/* @module image
 * @description this module handles the primary image viewer on PDP
 **/

/**
 * @description by default, this function sets up zoom and event handler for thumbnail click
 **/
module.exports = function () {
	if (dialog.isActive() || util.isMobile()) {
		$('#pdpMain .main-image').removeAttr('href');
	}
	slickInit();
	util.selectbox();
	loadZoom();
	// handle product thumbnail click event
	/*$('#pdpMain').on('click', '.productthumbnail', function () {
		// switch indicator
		$(this).closest('.product-thumbnails').find('.thumb.selected').removeClass('selected');
		$(this).closest('.thumb').addClass('selected');

		setMainImage($(this).data('lgimg'));
	});*/
};
module.exports.loadZoom = loadZoom;
module.exports.setMainImage = setMainImage;
module.exports.replaceImages = replaceImages;
