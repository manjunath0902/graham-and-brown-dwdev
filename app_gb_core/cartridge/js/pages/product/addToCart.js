'use strict';

var dialog = require('../../dialog'),
	minicart = require('../../minicart'),
	page = require('../../page'),
	util = require('../../util'),
	TPromise = require('promise'),
	_ = require('lodash');

/**
 * @description Make the AJAX request to add an item to cart
 * @param {Element} form The form element that contains the item quantity and ID data
 * @returns {Promise}
 */
var addItemToCart = function (form) {
	var $form = $(form),
		$qty = $form.find('input[name="Quantity"]');
	if ($qty.length === 0 || isNaN($qty.val()) || parseInt($qty.val(), 10) === 0) {
		$qty.val('1');
	}
	
	//prevail- Added for GTM integration
	
	var gtmevents= require('../../gtmevents');
	gtmevents.addToCart($form);
	
	
	return TPromise.resolve($.ajax({
		type: 'POST',
		async: false,//JIRA PREV-305:SG issue: No of items in cart value not displayed as expected.Added async:false.
		url: util.ajaxUrl(Urls.addProduct),
		data: $form.serialize()
	}));
};

/**
 * @description Handler to handle the add to cart event
 */
var addToCart = function(e) {
    e.preventDefault();
    var cuobj = $(this);
    var value =$(this).closest("form").find("#Quantity").val();
    var maxOrderQty = 50;
    var currentForm = cuobj.closest("form[id]");
    if(value.length == 0){
        $(this).closest("form").find("#Quantity").val(1);
        value = 1;
    }
    if (value > maxOrderQty) {
        currentForm.find('.add-to-cart').attr("disabled", true);
        $('#add-all-to-cart').attr("disabled", true);
        $('.span-messg').text(Resources.MAXORDERABLEQUANTITYERROR);
        var col2Height = ($('.product-col-2.product-detail').outerHeight() - 533);
		$('.pdp-main .socialsharing.social-links').css('padding-bottom',col2Height);
    } else {
        currentForm.find('.add-to-cart').attr("disabled", false);
        $('#add-all-to-cart').attr("disabled", false);
        $('.span-messg').text('');
    }
    var $form = $(this).closest('form');
    checkQuantity($form).then(function(resp){
    	if(resp.success){
		    if (value <= maxOrderQty) {
		        addItemToCart($form).then(function(response) {
		            var $uuid = $form.find('input[name="uuid"]');
		            if ($uuid.length > 0 && $uuid.val().length > 0) {
		                page.refresh();
		            } else {
		                // do not close quickview if adding individual item that is part of product set
		                // @TODO should notify the user some other way that the add action has completed successfully
		                if (!$(this).hasClass('sub-product-item')) {
		                    dialog.close();
		                }
		                minicart.show(response);
		            }
		        }.bind(this));
		    }
    	}else{
    		$form.find('.add-to-cart').attr("disabled", true);
	        $('#add-all-to-cart').attr("disabled", true);
	        $('.span-messg').text(Resources.MAXORDERABLEQUANTITYERROR);
	        var col2Height = ($('.product-col-2.product-detail').outerHeight() - 533);
			$('.pdp-main .socialsharing.social-links').css('padding-bottom',col2Height);
    	}
    });
};

/**
 * @description Handler to handle the add all items to cart event
 */
var addAllToCart = function (e) {
	e.preventDefault();
	var cuobj = $(this);
	var value =$('.product-set-details #Quantity').val(); 
	if(value.length == 0){
	   value = 1;
       $('.product-set-details #Quantity').val(1)
	}
	
	var $productForms = $('#product-set-list').find('form').toArray();
	TPromise.all(_.map($productForms, addItemToCart))
	.then(function (responses) {
	dialog.close();
	// show the final response only, which would include all the other items
	minicart.show(responses[responses.length - 1]);
	});
	};


/**
 * @description Checks for the product quantity in the basket
 */
var checkQuantity = function (form) {
	var checkQuantityUrl = Urls.checkQuantity;
	checkQuantityUrl = util.appendParamToURL(checkQuantityUrl, 'pid', form.find("input[name='pid']").val());
	checkQuantityUrl = util.appendParamToURL(checkQuantityUrl, 'Quantity', form.find("input[name='Quantity']").val());
	checkQuantityUrl = util.ajaxUrl(checkQuantityUrl);
	return TPromise.resolve($.ajax({
		type: 'POST',
		async: false,//JIRA PREV-305:SG issue: No of items in cart value not displayed as expected.Added async:false.
		url: checkQuantityUrl
	}));
};

/**
 * @function
 * @description Binds the click event to a given target for the add-to-cart handling
 */
module.exports = function () {
	$('.add-to-cart[disabled]').attr('title', $('.availability-msg').text());
	//Start JIRA PREV-454, PREV-469 : Application navigation not consistent when click of add to cart button of the Product set page
	$('#pdpMain, .pt_wish-list').on('click', '.add-to-cart', addToCart);
	$('.product-add-to-cart #add-all-to-cart').on('click', addAllToCart);
};
