'use strict';

var dialog = require('../../dialog'),
	productStoreInventory = require('../../storeinventory/product'),
	tooltip = require('../../tooltip'),
	util = require('../../util'),
	addToCart = require('./addToCart'),
	availability = require('./availability'),
	image = require('./image'),
	productSet = require('./productSet'),
	recommendations = require('./recommendations'),
	rating = require('../../rating'), //PREVAIL-Removed this from app.js and placed here to avoid issues during AJAX calls.
	variant = require('./variant');

/**
 * @description Initialize product detail page with reviews, recommendation and product navigation.
 */
function initializeDom() {
	tooltip.init();
}
/**
 * @description Initialize event handlers on product detail page
 */
function initializeEvents() {
	var $pdpMain = $('#pdpMain');

	addToCart();
	availability();
	variant();
	image();
	productSet();
	rating.init();//PREVAIL-Removed this from app.js and placed here to avoid issues during AJAX calls.
	if (SitePreferences.STORE_PICKUP) {
		productStoreInventory.init();
	}

	// Add to Wishlist and Add to Gift Registry links behaviors
	$pdpMain.on('click', '[data-action="wishlist"], [data-action="gift-registry"]', function () {
		var data = util.getQueryStringParams($('.pdpForm').serialize());
		if (data.cartAction) {
			delete data.cartAction;
		}
		var url = util.appendParamsToUrl(this.href, data);
		if(($(this).attr("data-action") == "wishlist")){
			var gtmevents= require('../../gtmevents');
			var id = $('.pdpForm').find(' input[name="pid"]').val();
			var name = $('.pdpForm').find(' input[name="name"]').val();
			gtmevents.addToWishlist(id,name);
		}
		this.setAttribute('href', url);
	});
	
	//custom scroll for PDP tab-content section
	if($(window).width() > 1024) {
		$('.tabs .tab').each(function() {
			$(this).find('.inner-tab-content').enscroll({
				verticalTrackClass: 'track1',
			    verticalHandleClass: 'handle1',
			    drawScrollButtons: true,
			    scrollUpButtonClass: 'scroll-up1',
			    scrollDownButtonClass: 'scroll-down1'
	       });
		});
	}

	//to aviod pdp product col2 overlapping in desktop
	$(window).load(function() {
		var col2Height = ($('.product-col-2.product-detail').outerHeight() - 533);
		$('.pdp-main .socialsharing.social-links').css('padding-bottom',col2Height);
	});

	// product options
	$pdpMain.on('change', '.product-options select', function () {
		/*Start JIRA PREV-66:Incorrect price displayed for the product set when options are selected for multiple individual products.
		Start JIRA PREV-67:Incorrect price displayed for the standard product when multiple options are selected.
		Added conditions for Product Set,Product Bundle and Normal Product*/
        var currentForm = $(this).closest("form");
        if (currentForm.hasClass("setProduct")) {
            var salesPrice = currentForm.find(".add-sub-product .price-sales");
            var selectedItem = $(this).children().filter(":selected").first();
            var combinedPrice = selectedItem.data("combined");
            salesPrice.text(combinedPrice);
            var setSalePrice = $("#pdpMain").find(".product-col-2.product-set").find(".product-add-to-cart").find(".product-price").find(".salesprice");
            var originalSetPrice = 0;
            $('.add-sub-product').find('.product-price').find('.price-sales').each(function(index) {
                var setlevelProductPrice = Number($(this).text().replace(/[^0-9\.]+/g, ""));
                originalSetPrice = originalSetPrice + setlevelProductPrice;
            });
            var currency = $('#currency').val();
            setSalePrice.text(currency + (parseFloat(originalSetPrice.toFixed(2))));
        } else {
            if (!($(this).closest('.product-set-item').hasClass('product-bundle-item')) || ($('.product-add-to-cart').find('form').hasClass('normalProduct'))) {
                var salesPrice = $pdpMain.find("div.product-add-to-cart .price-sales");
                var currency = $('#currency').val();
                if ($('.product-add-to-cart').find('form').find('.product-options').find('select option:selected').length > 1) {
                    var multiStandardProd = 0;
                    $('.product-add-to-cart').find('form').find('.product-options').find('select').each(function() {
                        multiStandardProd = multiStandardProd + Number(($(this).find('option:selected').data('setprodprice')).replace(/[^0-9\.]+/g, ""));
                    });
                    if ($('#product-content').find('.product-price').first().find('.price-sales').length > 0 &&
                        ($('#product-content').find('.product-price').first().find('.price-standard').length == 0)) {
                        var totalSalePrice = multiStandardProd + Number($('#product-content').find('.product-price').first().find('.price-sales').text().replace(/[^0-9\.]+/g, ""));
                    } else {
                        if ($('#product-content').find('.product-price').first().find('.price-standard').length > 0 &&
                            ($('#product-content').find('.product-price').first().find('.price-sales').length == 0)) {
                            var totalSalePrice = multiStandardProd + Number($('#product-content').find('.product-price').first().find('.price-standard').text().replace(/[^0-9\.]+/g, ""));
                        }
                    }

                    if (($('#product-content').find('.product-price').first().find('.price-standard').length > 0) &&
                        ($('#product-content').find('.product-price').first().find('.price-sales').length > 0)) {
                        var totalSalePrice = multiStandardProd + Number($('#product-content').find('.product-price').first().find('.price-sales').text().replace(/[^0-9\.]+/g, ""));
                    }

                    salesPrice.text(currency + (parseFloat(totalSalePrice.toFixed(2))));
                } else {
                    var selectedItem = $(this).children().filter(":selected").first();
                    var combinedPrice = selectedItem.data("combined");
                    salesPrice.text(combinedPrice);
                }

            }

        }
        if ($(this).closest('.product-set-item').hasClass('product-bundle-item')) {
            var bundleProduct = $(this).closest('.product-bundle-item');
            var bundleInitialPrice = $('.product-col-2').find('.product-price').first().find('span.price-sales').text();
            var numberPrice = Number(bundleInitialPrice.replace(/[^0-9\.]+/g, ""));
            var currency = $('#currency').val();
            var bundleLevelPriceForOption = 0;
            var price = '';
            $('.product-bundle-item').each(function() {
                if ($(this).find('.product-options').find('select option:selected').length > 1) {
                    var multiOptionsPrice = '';
                    var numMulti = 0;
                    $(this).find('.product-options').find('select').each(function() {
                        multiOptionsPrice = $(this).find('option:selected').data('setprodprice');
                        numMulti = numMulti + Number(multiOptionsPrice.replace(/[^0-9\.]+/g, ""));
                    });
                    price = currency + numMulti;
                } else {
                    price = $(this).find('.product-options').find('select option:selected').data('setprodprice');
                }

                var numPrice = 0;
                if (price != undefined) {
                    numPrice = Number(price.replace(/[^0-9\.]+/g, ""));
                }
                bundleLevelPriceForOption = bundleLevelPriceForOption + numPrice;
            });
            var totalBundlePrice = numberPrice + bundleLevelPriceForOption;
            var bundleLevelPriceTotal = $('.bundle').find('.product-add-to-cart').find('.product-price').find('.price-sales');
            bundleLevelPriceTotal.text(currency + (parseFloat(totalBundlePrice.toFixed(2))));
        }
        /*End JIRA PREV-66,PREV-67*/
	});

	// prevent default behavior of thumbnail link and add this Button
	$pdpMain.on('click', '.thumbnail-link, .unselectable a', function (e) {
		e.preventDefault();
	});

	//JIRA PREV-386 : On click of size chart link, the page displayed instead of overlay. Replaced $('.size-chart-link a').on('click', with $pdpMain.on('click', '.size-chart-link a',
	$pdpMain.on('click', '.size-chart-link a', function (e) {
		e.preventDefault();
		dialog.open({
			url: $(e.target).attr('href')
		});
	});
	//PDP zoom
	$("body").on('click','a.product-zoom', function(e){
		e.preventDefault();
		var pid = $("#pdp-zoom-prod").val();
		var url = Urls.PDPZoom;
		url = util.appendParamToURL(url, 'pid', pid);
		dialog.open({
			url : url,
			options : {
				dialogClass : 'PDP-product-zoom'
			},
			callback: function () {
				$('.ui-dialog .product-primary-image').slick({
				  slidesToShow: 1,
				  slidesToScroll: 1,
				  infinite: true,
				  arrows: true,
				  focusOnSelect: true,
				  asNavFor: '.ui-dialog #thumbnails ul',
				  initialSlide: parseInt($('#pdpMain .product-primary-image .slick-slide.slick-current:not(".slick-cloned")').attr('data-slick-index'))
				});
				$('.ui-dialog #thumbnails ul').slick({
				  asNavFor: '.ui-dialog .product-primary-image',
				  slidesToShow: 1,
				  slidesToScroll: 1,
				  arrows: false,
				  focusOnSelect: true,
				  initialSlide: parseInt($('#pdpMain .product-primary-image .slick-slide.slick-current:not(".slick-cloned")').attr('data-slick-index'))
				});
			}
		});
	});
	//PDP Video
	$(".thumb, .main-video, .slick-arrow").on('click', function(e){
		e.preventDefault();
		if($(this).hasClass('productVideos')) {
			$(".video-play-container").css("display", "block");
			$("#video-iframe").each(function(){
				$('.zoompad').addClass('zoom-tab');
				this.contentWindow.postMessage('{"event":"command","func":"' + 'playVideo' + '","args":""}', '*');
			});
		}
		else {
			$("#video-iframe").each(function(){
				$('.zoompad').removeClass('zoom-tab');
				this.contentWindow.postMessage('{"event":"command","func":"' + 'pauseVideo' + '","args":""}', '*');
			});
		}
		/*var pid = $("#pdp-zoom-prod").val();
		var url = Urls.PDPvideo;
		url = util.appendParamToURL(url, 'pid', pid);
		dialog.open({
			url : url,
			options : {
				dialogClass : 'PDP-product-video'
			},
			callback: function () {
				$(".ui-icon-closethick").on("click", function() {
					$('.video-container embed').remove();
				});
			}
		});*/
	});
	$(window).load(function(){
		var recomm = setInterval(function(){  recommend(); }, 5000);
		function recommend() {
			if($('.recommendations #carousel-recommendations ul').hasClass('slick-slider')) {
				clearInterval(recomm);
			}
			else {
				recommendations();
			}
		}
	});
}

var product = {
	initializeEvents: initializeEvents,
	init: function () {
		initializeDom();
		initializeEvents();
	}
};

module.exports = product;
