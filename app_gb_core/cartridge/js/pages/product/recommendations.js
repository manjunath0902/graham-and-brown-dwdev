'use strict';

/**
 * @description Creates product recommendation carousel using jQuery jcarousel plugin
 **/
module.exports = function () {

	$('.recommendations #carousel-recommendations ul').slick({
		  dots: false,
		  infinite: true,
		  speed: 300,
		  slidesToShow: 5,
		  slidesToScroll: 1,
		  responsive: [{
          breakpoint: 1023,
          settings: {
              slidesToShow: 3
          }
      }, {
          breakpoint: 768,
          settings: {
              slidesToShow: 1,
              dots: true
          }
      }]
	});	
};

