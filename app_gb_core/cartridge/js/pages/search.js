'use strict';

var compareWidget = require('../compare-widget'),
    productTile = require('../product-tile'),
    progress = require('../progress'),
    validator = require('../validator'),
    dialog = require('../dialog'),
    formPrepare = require('../pages/checkout/formPrepare'),
    util = require('../util');

function infiniteScroll() {
    // getting the hidden div, which is the placeholder for the next page
    var loadingPlaceHolder = $('.infinite-scroll-placeholder[data-loading-state="unloaded"]');
    // get url hidden in DOM
    var gridUrl = loadingPlaceHolder.attr('data-grid-url');

    if (loadingPlaceHolder.length === 1 && util.elementInViewport(loadingPlaceHolder.get(0), 250)) {
        // switch state to 'loading'
        // - switches state, so the above selector is only matching once
        // - shows loading indicator
        loadingPlaceHolder.attr('data-loading-state', 'loading');
        loadingPlaceHolder.addClass('infinite-scroll-loading');

        /**
         * named wrapper function, which can either be called, if cache is hit, or ajax repsonse is received
         */
        var fillEndlessScrollChunk = function(html) {
            loadingPlaceHolder.removeClass('infinite-scroll-loading');
            loadingPlaceHolder.attr('data-loading-state', 'loaded');
            $('div.search-result-content').append(html);
        };

        // old condition for caching was `'sessionStorage' in window && sessionStorage["scroll-cache_" + gridUrl]`
        // it was removed to temporarily address RAP-2649
        if (false) {
            // if we hit the cache
            fillEndlessScrollChunk(sessionStorage['scroll-cache_' + gridUrl]);
        } else {
            // else do query via ajax
            $.ajax({
                type: 'GET',
                dataType: 'html',
                url: gridUrl,
                success: function(response) {
                    // put response into cache
                    try {
                        sessionStorage['scroll-cache_' + gridUrl] = response;
                    } catch (e) {
                        // nothing to catch in case of out of memory of session storage
                        // it will fall back to load via ajax
                    }
                    // update UI
                    fillEndlessScrollChunk(response);
                    productTile.init();
                }
            });
        }
    }
}
/**
 * @private
 * @function
 * @description replaces breadcrumbs, lefthand nav and product listing with ajax and puts a loading indicator over the product listing
 */
function updateProductListing() {
    var hash = location.href.split('#')[1];
    if (hash === 'results-content' || hash === 'results-products') {
        return;
    }
    var refineUrl;

    if (hash.length > 0) {
        refineUrl = window.location.pathname + '?' + hash;
    } else {
        return;
    }
    progress.show($('.search-result-content'));
    $('#main').load(util.appendParamToURL(refineUrl, 'format', 'ajax'), function() {
        compareWidget.init();
        productTile.init();
        util.selectbox();
        refineBy();
        util.fancyScroll();
        refinementload();
        //code for mobile view in case of refinments showing
        var reminementExpand = false;
        $("#secondary").find("div.refinement").each(function() {
            var refinement = $(this);
            if ($(this).find("ul").length > 0 && $(this).find("ul li.selected").length > 0) {
                $(this).find("ul").css("display", "block");
                reminementExpand = true;
            }
        });
        if (reminementExpand) {
            //$("#secondary").css("display","block");
        }
        //end
        progress.hide();
        $('.last-visited .search-result-items, .featured-products .search-result-items').slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 5,
            slidesToScroll: 1,
            responsive: [{
                breakpoint: 1023,
                settings: {
                    slidesToShow: 3
                }
            }, {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    dots: true
                }
            }]
        });
        $(window).on("load", function() {
            if ($("#wrapper").hasClass("pt_cart")) {
                $('.last-visited .grid-tile').syncHeight();
                $('.featured-products .grid-tile').syncHeight();
            }
        });
        $('html, body').scrollTop(100);
    });
}
//this is for showing and hiding the clearall refinements (GB-571)
$(document).ajaxComplete(function() {
	refinementload();
    if ($('.clear-refinement').length >= 1) {
        $('.clearall').show();
    } else {
        $('.clearall').hide();
    }
    if($(window).width() >= 1024) {
    	$('#secondary .refinement').find('ul').addClass('refinement-hide');
    	$('.refinement.category-refinement').insertBefore('.bottom-apply');
	    $(".new .refinement").each(function(){
	        var $curObj = $(this);
	        if( $curObj.find("> ul li.selected").length > 0 ) {
	          $curObj.find("h3.toggle").trigger("click");
	        }
	    });
    }
});
/**
 * @private
 * @function
 * @description Initializes events for the following elements:<br/>
 * <p>refinement blocks</p>
 * <p>updating grid: refinements, pagination, breadcrumb</p>
 * <p>item click</p>
 * <p>sorting changes</p>
 */
function initializeEvents() {
    var $main = $('#main');
    refinementload();
    // compare checked
    $main.on('click', 'input[type="checkbox"].compare-check', function() {
        var cb = $(this);
        var tile = cb.closest('.product-tile');

        var func = this.checked ? compareWidget.addProduct : compareWidget.removeProduct;
        var itemImg = tile.find('.product-image a img').first();
        func({
            itemid: tile.data('itemid'),
            uuid: tile[0].id,
            img: itemImg,
            cb: cb
        });

    });

    //error page contact-us popup
    $('#error_contact').on('click', function(e) {
        e.preventDefault();
        var url = Urls.errorContactus;
        dialog.open({
            url: url,
            options: {
                dialogClass: 'colorcard-popup',
                width: 510,

            },
            callback: function() {
                util.selectbox();
                validator.init();
                contactUsEvents();
                //For cancel button in contactus page
                $('#cancelBtn').on('click', function(e) {
                    e.preventDefault();
                    var prevurl = $("[name='prevurl']").val();
                    window.location.href = prevurl;

                })

            }
        });

    });
    
    $(document).on('click', '.refine-by-mobile', function() {
    	$(this).toggleClass('active');
    });
    
    // handle toggle refinement blocks
    $main.on('click', '.refinement h3', function() {
        $(this).toggleClass('expanded');
        $(this).parent().siblings('ul').slideToggle();
        $(this).find('span').toggleClass('left-arrow');
    });
    

    //this is for clearall refinements (GB-571)
    $(document).on("click", ".clear-refinement", function(e) {
        e.preventDefault();
        var previousUrl = window.location.href;
        var peaces = previousUrl.split("prefn");
        var removeRef = $(this).closest('.refinement').attr('class').split(" ")[1];
        var clearurl = "";
        for (var peace = 1; peace < peaces.length; peace++) {
            var peaceStr = peaces[peace];
            if (peaceStr.indexOf(removeRef) > -1) {
                clearurl += peaces[peace].substring(2,peaces[peace].length);
            } else {
                clearurl += "prefn" + peaces[peace].substring(0, peaces[peace].length);
            }
        }
        var priceSelected = $(".pricefilter li").hasClass("selected");
        var productType = $('.refinements .ProductGroup li').hasClass('selected');
        var brandSelected = $('.refinements .brand li').hasClass('selected');
        var pattern = $('.refinements .DesignStyle li').hasClass('selected');
        var applicationMethod = $('.refinements .application li').hasClass('selected');
        var matchType = $('.refinements .DesignMatch li').hasClass('selected');
        if (priceSelected) {
            var clearurl = window.location.hash;
            var pmin = $(".pricefilter li.selected a").attr("pmin");
            var pmax = $(".pricefilter li.selected a").attr("pmax");
            clearurl = clearurl.replace("&pmin=" + pmin, "");
            clearurl = clearurl.replace("&pmax=" + pmax, "");
        }

        if (peaces.length == 2) {

            if (priceSelected || (productType != undefined && productType) || (brandSelected != undefined && brandSelected) || (pattern != undefined && pattern) || (applicationMethod != undefined && applicationMethod) || (matchType != undefined && matchType)) {
                window.location.hash = clearurl;
            } else {
                window.location.hash = window.location.search.replace('?', '') + "&refinement=clearall";
            }

        } else {
            window.location.hash = clearurl;
        }

    });

    //this is for clearall refinements (GB-571)
    $(document).on("click", ".clearall", function(e) {
        e.preventDefault();
        window.location.hash = window.location.search.replace('?', '') + "&refinement=clearall";
    });

    function commonRefinement(obj) {

        if ($(obj).parent().hasClass('unselectable')) {
            return;
        }
        var catparent = $(obj).parents('.category-refinement');
        var folderparent = $(obj).parents('.folder-refinement');

        //if the anchor tag is uunderneath a div with the class names & , prevent the double encoding of the url
        //else handle the encoding for the url
        if ((catparent != undefined && catparent.length > 0) || folderparent.length > 0) {
            return true;
        } else {
            var query = util.getQueryString(obj.context.href);
            if (query != undefined && query.length > 1) {
                window.location.hash = query;
            } else {
                window.location.href = obj.context.href;
            }

            //GTM
            var gtmevents = require('../gtmevents');
            gtmevents.categoryRefinement();

            return false;
        }
    }
    $main.on('click', '.apply', function(e) {
        if ($(window).width() < 1024) {
            var allParams = "";
            var allP = "";
            //#prefn1=colourgroup&prefv1=Multiprefn2=brand&prefv2=G&B Kidsprefn3=ProductGroup&prefv3=Children's Roomprefn4=SubBrand&prefv4=ZIZAZOU"
            var searchItems = 0;
            var pricecount = 0;
            var searchDiff = "";
            var qryStr = "&";
            var pmin = 0;
            var pmax = 0;
            var checkedStatus = false;
            var checkitem = '';
            $("li.selected a").each(function(i) {
                var searchItem = $(this).attr('refid0');
                var pricemin = $(this).attr('pmin');
                var pricemax = $(this).attr('pmax');
                checkedStatus = $(this).find("i").hasClass('fa-check-square-o');

                if (searchDiff != searchItem && searchItem != 'price' && searchItem != undefined && checkedStatus) {
                    searchDiff = searchItem;
                    searchItems++;
                    if (checkedStatus) {
                        if (searchItems != 1) {
                            qryStr += "&prefn" + searchItems + "=" + searchItem + "&prefv" + searchItems + "=";
                        } else {
                            qryStr += "prefn" + searchItems + "=" + searchItem + "&prefv" + searchItems + "=";
                        }
                    }
                }
                if (searchItem != 'price') {
                    var searchVal = $(this).attr('refid1');
                    var searchValue = $(this).attr('href');
                    if (searchVal != undefined && checkedStatus) {
                        //var filval = searchValue.substring(searchValue.indexOf("prefv1") + 7, searchValue.length);
                        //searchVal = searchVal.replace(new RegExp(" ", 'g'), "");
                    	if(checkitem != '' && searchItem == checkitem){
                    		searchVal = encodeURIComponent( "|" + searchVal );
                    	}else{
                    		searchVal = encodeURIComponent( searchVal );
                    	}
                        checkitem = searchItem;
                        qryStr += searchVal + ",";
                    }
                }
                qryStr = (qryStr.length - 1) == qryStr.indexOf(',') ? qryStr.substr(0, qryStr.length - 1) : qryStr;
            });

            var priceCheck = false;
            $("li.selected a").each(function(i) {
                var searchItem = $(this).attr('refid0');
                var pricemin = $(this).attr('pmin');
                var pricemax = $(this).attr('pmax');
                checkedStatus = $(this).find("i").hasClass('fa-check-square-o');
                if (searchItem == "price" && checkedStatus) {
                    priceCheck = true;
                    if (pricecount == 0) {
                        pmin = pricemin;
                        pmax = pricemax;
                    } else {
                        pmax = pricemax;
                    }
                    pricecount++;
                }
            });
            if (searchItems == 0) {
                qryStr = "&refinement=clearall&format=ajax";
            }
            if (priceCheck) {
                qryStr += "&pmin=" + pmin + "&pmax=" + pmax;
            } else {
                qryStr += "";
            }
            window.location.hash = window.location.search.replace('?', '') + qryStr;
            //GTM
            var gtmevents = require('../gtmevents');
            gtmevents.categoryRefinement();
        } else {
            commonRefinement($(this));
        }
	    
    });

    $main.on('click', 'div.refinements a', function(e) {    	
		e.preventDefault();
		if ($(this).hasClass('redirect-link')) {
			window.location = $(this).attr('href');
		} else {

	        if ($(window).width() < 1024) {
	            if ($(this).closest('ul').hasClass('pricefilter') && !$(this).closest('li').hasClass('selected')) {
	                $(this).closest('ul').find('li').removeClass("selected");
	                $(this).closest('ul').find('li i').removeClass('fa-check-square-o');
	                $(this).closest('ul').find('li i').addClass('fa-square-o');
	            }
	            $(this).closest('li').addClass("selected");
	            $(this).find('i').toggleClass('fa-check-square-o');
	            $(this).find('i').toggleClass('fa-square-o');
	        } else {
	            commonRefinement($(this));
	        }
		}
    });

    $main.on('click', '.pagination a, .breadcrumb-refinement-value a', function(e) {
        if ($(window).width() < 1024) {
            if ($(this).parent().hasClass('unselectable')) {
                return;
            }
            var catparent = $(this).parents('.category-refinement');
            var folderparent = $(this).parents('.folder-refinement');

            //if the anchor tag is uunderneath a div with the class names & , prevent the double encoding of the url
            //else handle the encoding for the url
            if ((catparent != undefined && catparent.length > 0) || folderparent.length > 0) {
                return true;
            } else {
                var query = util.getQueryString(this.href);
                if (query != undefined && query.length > 1) {
                    window.location.hash = query;
                } else {
                    window.location.href = this.href;
                }

                //GTM
                var gtmevents = require('../gtmevents');
                gtmevents.categoryRefinement();

                return false;
            }
        } else {
            commonRefinement($(this));
        }
    });


    // handle events item click. append params.
    $main.on('click', '.product-tile a:not("#quickviewbutton")', function() {
        var a = $(this);
        // get current page refinement values
        var wl = window.location;

        var qsParams = (wl.search.length > 1) ? util.getQueryStringParams(wl.search.substr(1)) : {};
        var hashParams = (wl.hash.length > 1) ? util.getQueryStringParams(wl.hash.substr(1)) : {};

        // merge hash params with querystring params
        var params = $.extend(hashParams, qsParams);
        if (!params.start) {
            params.start = 0;
        }
        // get the index of the selected item and save as start parameter
        var tile = a.closest('.product-tile');
        var idx = tile.data('idx') ? +tile.data('idx') : 0;

        /*Start JIRA PREV-50 : Next and Previous links will not be displayed on PDP if user navigate from Quick View.Added cgid to hash*/
        if (!params.cgid && tile.data("cgid") != null && tile.data("cgid") != "") {
            params.cgid = tile.data("cgid");
        }
        /*End JIRA PREV-50*/

        // convert params.start to integer and add index
        params.start = (+params.start) + (idx + 1);
        // set the hash and allow normal action to continue
        a[0].hash = $.param(params);
    });

    // handle sorting change
    $main.on('change', '.sort-by select', function() {
            var refineUrl = $(this).find('option:selected').val();
            var queryString = util.getQueryString(refineUrl);

            //GTM
            var gtmevents = require('../gtmevents');
            var selectedvalue = $('select#grid-sort-header option:selected').text();
            gtmevents.categoryPageSort(selectedvalue);

            window.location.hash = queryString;
            return false;
        })
        .on('change', '.items-per-page select', function() {
            var refineUrl = $(this).find('option:selected').val();
            var queryString = util.getQueryString(refineUrl);
            if (refineUrl === 'INFINITE_SCROLL') {
                $('html').addClass('infinite-scroll').removeClass('disable-infinite-scroll');
            } else {
                $('html').addClass('disable-infinite-scroll').removeClass('infinite-scroll');
                window.location.hash = queryString;
            }
            return false;
        });

    // handle hash change
    window.onhashchange = updateProductListing;

    refineBy();

    $(".category-banner-container .expand").click(function() {
        $(this).next().slideToggle();
        $(this).toggleClass("active");
    });
    $(".subcategories-mobile").click(function() {
        $(".subCat-mobile").slideToggle();
    });
    $('#home-recommendations').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 5,
        slidesToScroll: 5,
        responsive: [{
            breakpoint: 1023,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3
            }
        }, {
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true,
                dotsClass: 'custom_paging',
                customPaging: function(slider, i) {
                    return (i + 1) + '/' + slider.slideCount;
                },
            }
        }]
    });
    $(".search-result-bookmarks a#go-article").click(function() {
        $('html,body').animate({
            scrollTop: $(".search-results-content").offset().top - 200
        }, 'slow');
    });
}

//send button in contactus
function contactUsEvents() {
    if ($('#Contactus').length > 0) {
        formPrepare.init({
            continueSelector: '[name$="contactus_send"]',
            formSelector: '[id$="Contactus"]'
        });
    }

}


//refine by toggle for tab & mobile 
function refineBy() {
    $(".refine-by-mobile").click(function() {
        if ($(window).width() < 1024) {
            $("#secondary").slideToggle();
        }
    });

    $(".sort-by-mobile").click(function() {
        if ($(window).width() < 1024) {
            $(".sort-by, .items-per-page").slideToggle();
        }
    });

    $(window).resize(function() {
        if ($(window).width() > 1023) {
            $("#secondary, .sort-by, .items-per-page").show();
        }
    });
}

function refinementload() {
	if($(window).width() >= 1024) {
		$(document).on('click', '#secondary.new .refinement h3', function() {
		  $(this).toggleClass('active');
		  var currentcat = $(this).closest('.refinement').attr('class').split(' ')[1];
		  var addElement = false;
		  if(currentcat==="")
		  {
		    currentcat = $(this).closest('.refinement').find('ul').attr('class').split(' ')[1];
		  }
		  $(this).closest('.refinement').find('ul').addClass(currentcat);
		  var clickedElement = $(this);
		  if($('ul.horizontalSelector').length>0)
		  {
		    if(!clickedElement.hasClass('active'))
		    {
		      $('ul.horizontalSelector.'+currentcat).remove();
		    }else if($('ul.horizontalSelector.'+currentcat).length===0)
		    {
		      clickedElement.closest('.refinement').find('ul').clone().addClass('horizontalSelector').appendTo("#secondary");
		    }
		  }else
		  {
		    clickedElement.closest('.refinement').find('ul').clone().addClass('horizontalSelector').appendTo("#secondary");
		  }
		  $(this).closest('.refinement').find('ul').hide();
		});
	}
}

exports.init = function() {
    compareWidget.init();
    if (SitePreferences.LISTING_INFINITE_SCROLL) {
        $(window).on('scroll', infiniteScroll);
    }
    //changes for GBSS-9
    if(window.location.hash.length > 0){
        updateProductListing();
    }
    productTile.init();
    initializeEvents();
};