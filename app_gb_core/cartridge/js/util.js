'use strict';

var _ = require('lodash');

var util = {
	/**
	 * @function
	 * @description appends the parameter with the given name and value to the given url and returns the changed url
	 * @param {String} url the url to which the parameter will be added
	 * @param {String} name the name of the parameter
	 * @param {String} value the value of the parameter
	 */
	appendParamToURL: function (url, name, value) {
		// quit if the param already exists
		if (url.indexOf(name + '=') !== -1) {
			return url;
		}
		var separator = url.indexOf('?') !== -1 ? '&' : '?';
		return url + separator + name + '=' + encodeURIComponent(value);
	},
	/**
	 * @function
	 * @description appends the parameters to the given url and returns the changed url
	 * @param {String} url the url to which the parameters will be added
	 * @param {Object} params
	 */
	appendParamsToUrl: function (url, params) {
		var _url = url;
		_.each(params, function (value, name) {
			_url = this.appendParamToURL(_url, name, value);
		}.bind(this));
		return _url;
	},

	/**
	 * @function
	 * @description remove the parameter and its value from the given url and returns the changed url
	 * @param {String} url the url from which the parameter will be removed
	 * @param {String} name the name of parameter that will be removed from url
	 */
	removeParamFromURL: function (url, name) {
		if (url.indexOf('?') === -1 || url.indexOf(name + '=') === -1) {
			return url;
		}
		var hash;
		var params;
		var domain = url.split('?')[0];
		var paramUrl = url.split('?')[1];
		var newParams = [];
		// if there is a hash at the end, store the hash
		if (paramUrl.indexOf('#') > -1) {
			hash = paramUrl.split('#')[1] || '';
			paramUrl = paramUrl.split('#')[0];
		}
		params = paramUrl.split('&');
		for (var i = 0; i < params.length; i++) {
			// put back param to newParams array if it is not the one to be removed
			if (params[i].split('=')[0] !== name) {
				newParams.push(params[i]);
			}
		}
		return domain + '?' + newParams.join('&') + (hash ? '#' + hash : '');
	},

	/**
	 * @function
	 * @description extract the query string from URL
	 * @param {String} url the url to extra query string from
	 **/
	getQueryString: function (url) {
		var qs;
		if (!_.isString(url)) { return; }
		var a = document.createElement('a');
		a.href = url;
		if (a.search) {
			qs = a.search.substr(1); // remove the leading ?
		}
		return qs;
	},
	/**
	 * @function
	 * @description
	 * @param {String}
	 * @param {String}
	 */
	elementInViewport: function (el, offsetToTop) {
		var top = el.offsetTop,
			left = el.offsetLeft,
			width = el.offsetWidth,
			height = el.offsetHeight;

		while (el.offsetParent) {
			el = el.offsetParent;
			top += el.offsetTop;
			left += el.offsetLeft;
		}

		if (typeof(offsetToTop) !== 'undefined') {
			top -= offsetToTop;
		}

		if (window.pageXOffset !== null) {
			return (
				top < (window.pageYOffset + window.innerHeight) &&
				left < (window.pageXOffset + window.innerWidth) &&
				(top + height) > window.pageYOffset &&
				(left + width) > window.pageXOffset
			);
		}

		if (document.compatMode === 'CSS1Compat') {
			return (
				top < (window.document.documentElement.scrollTop + window.document.documentElement.clientHeight) &&
				left < (window.document.documentElement.scrollLeft + window.document.documentElement.clientWidth) &&
				(top + height) > window.document.documentElement.scrollTop &&
				(left + width) > window.document.documentElement.scrollLeft
			);
		}
	},

	/**
	 * @function
	 * @description Appends the parameter 'format=ajax' to a given path
	 * @param {String} path the relative path
	 */
	ajaxUrl: function (path) {
		return this.appendParamToURL(path, 'format', 'ajax');
	},

	/**
	 * @function
	 * @description
	 * @param {String} url
	 */
	toAbsoluteUrl: function (url) {
		if (url.indexOf('http') !== 0 && url.charAt(0) !== '/') {
			url = '/' + url;
		}
		return url;
	},
	/**
	 * @function
	 * @description Loads css dynamically from given urls
	 * @param {Array} urls Array of urls from which css will be dynamically loaded.
	 */
	loadDynamicCss: function (urls) {
		var i, len = urls.length;
		for (i = 0; i < len; i++) {
			this.loadedCssFiles.push(this.loadCssFile(urls[i]));
		}
	},

	/**
	 * @function
	 * @description Loads css file dynamically from given url
	 * @param {String} url The url from which css file will be dynamically loaded.
	 */
	loadCssFile: function (url) {
		return $('<link/>').appendTo($('head')).attr({
			type: 'text/css',
			rel: 'stylesheet'
		}).attr('href', url); // for i.e. <9, href must be added after link has been appended to head
	},
	// array to keep track of the dynamically loaded CSS files
	loadedCssFiles: [],

	/**
	 * @function
	 * @description Removes all css files which were dynamically loaded
	 */
	clearDynamicCss: function () {
		var i = this.loadedCssFiles.length;
		while (0 > i--) {
			$(this.loadedCssFiles[i]).remove();
		}
		this.loadedCssFiles = [];
	},
	/**
	 * @function
	 * @description Extracts all parameters from a given query string into an object
	 * @param {String} qs The query string from which the parameters will be extracted
	 */
	getQueryStringParams: function (qs) {
		if (!qs || qs.length === 0) { return {}; }
		var params = {},
			unescapedQS = decodeURIComponent(qs);
		// Use the String::replace method to iterate over each
		// name-value pair in the string.
		unescapedQS.replace(new RegExp('([^?=&]+)(=([^&]*))?', 'g'),
			function ($0, $1, $2, $3) {
				params[$1] = $3;
			}
		);
		return params;
	},

	fillAddressFields: function (address, $form) {
		$form.find('[id$="_address2"]').val("");
		for (var field in address) {
			if (field === 'ID' || field === 'UUID' || field === 'key') {
				continue;
			}
			if(address[field] != null){
				// if the key in address object ends with 'Code', remove that suffix
				// keys that ends with 'Code' are postalCode, stateCode and countryCode
				$form.find('[name$="' + field.replace('Code', '') + '"]').val(address[field]);
				// update the state fields
				if (field === 'countryCode') {
					$form.find('[name$="country"]').trigger('change');
					// retrigger state selection after country has changed
					// this results in duplication of the state code, but is a necessary evil
					// for now because sometimes countryCode comes after stateCode
					$form.find('[name$="state"]').val(address.stateCode).trigger('change');
				} 
			}
		}
	},
	/**
	 * @function
	 * @description Updates the number of the remaining character
	 * based on the character limit in a text area
	 */
	limitCharacters: function () {
		$('form').find('textarea[data-character-limit]').each(function () {
			var characterLimit = $(this).data('character-limit');
			var charCountHtml = String.format(Resources.CHAR_LIMIT_MSG,
				'<span class="char-remain-count">' + characterLimit + '</span>',
				'<span class="char-allowed-count">' + characterLimit + '</span>');
			var charCountContainer = $(this).next('div.char-count');
			if (charCountContainer.length === 0) {
				charCountContainer = $('<div class="char-count"/>').insertAfter($(this));
			}
			charCountContainer.html(charCountHtml);
			// trigger the keydown event so that any existing character data is calculated
			$(this).change();
		});
	},
	/**
	 * @function
	 * @description Binds the onclick-event to a delete button on a given container,
	 * which opens a confirmation box with a given message
	 * @param {String} container The name of element to which the function will be bind
	 * @param {String} message The message the will be shown upon a click
	 */
	setDeleteConfirmation: function (container, message) {
		$(container).on('click', '.delete', function () {
			return window.confirm(message);
		});
	},
	/**
	 * @function
	 * @description Scrolls a browser window to a given x point
	 * @param {String} The x coordinate
	 */
	scrollBrowser: function (xLocation) {
		$('html, body').animate({scrollTop: xLocation}, 500);
	},

	isMobile: function () {
		var mobileAgentHash = ['mobile', 'tablet', 'phone', 'ipad', 'ipod', 'android', 'blackberry', 'windows ce', 'opera mini', 'palm'];
		var	idx = 0;
		var isMobile = false;
		var userAgent = (navigator.userAgent).toLowerCase();

		while (mobileAgentHash[idx] && !isMobile) {
			isMobile = (userAgent.indexOf(mobileAgentHash[idx]) >= 0);
			idx++;
		}
		return isMobile;
	},
	selectbox : function(){
		$('#main .input-select, .ui-dialog .input-select').each(function(){
			$(this).selectBoxIt();
		});
	},
	countrySelectbox : function(){
		$('#HeaderChangeFlag .input-select').each(function(){
			$(this).selectBoxIt();
		});
	},
	miniSummaryArrow : function(){
	    $('.mini-cart-product').eq(0).find('.mini-cart-toggle').addClass('caret-down');
		$('.mini-cart-product').not(':first').addClass('collapsed')
			.find('.mini-cart-toggle').addClass('caret-right');

		$('.mini-cart-toggle').on('click', function () {
			$(this).toggleClass('caret-down caret-right');
			$(this).closest('.mini-cart-product').toggleClass('collapsed');
		});
	},
	fancyScroll : function(){
	// for search result page
		$(".pt_product-search-result #search-result-items li.grid-tile:nth-child(6n+4)").addClass("section");
	
		//FancyScroll
		(function initFancyScroll($container) {
		    var 
				$scroller = $(window),
				topOffset = 60,
				$sections = $('.section', $container);
	
		    function throttle(fn, context) {
		        var lastInvocation = 0, threshold = 10;
		        return function () {
		            var now = new Date().getTime();
		            if (now - lastInvocation > threshold) {
		                return fn.apply(context, arguments);
		            }
		        }
		    }
	
		    function scrollTop() {
		        return $scroller.scrollTop();
		    }
	
		    function isAboveTop() {
		        return $(this)[0].offsetTop < $scroller.scrollTop();
		    }
	
		    function isBelowTop() {
		        return $(this)[0].offsetTop > $scroller.scrollTop();
		    }
	
		    function prevSection() {
		        return $sections.filter(isAboveTop).last();
		    }
	
		    function nextSection() {
		        return $sections.filter(isBelowTop).first();
		    }
	
		    function currentSectionNumber() {
		        var i, scrollTop = $scroller.scrollTop() + 10;
		        for (i = 0; $sections[i] && $($sections[i]).position().top <= scrollTop; i++) { }
		        return i;
		    }
	
		    function scrollTo($target) {
		        if ($target.length === 0) { return; }
		        if ($target[0].offsetTop != 0) { $('html,body').animate({ scrollTop: $target[0].offsetTop }, 500); }
		        if ($target != undefined && $target != null && $target.prevObject != undefined) {
		        	if ($target.prevObject.length === 1) { $('html,body').animate({ scrollTop: $target[0].offsetTop }, 500); }
		        }
		        //$('html,body').animate({ scrollTop: $target[0].offsetTop }, 500);
		        //console.debug('scrollTo: ' + $target.position().top);
		    }
	
		    function updatePageNumber() {
		        if ($(document).outerHeight() - $(document).scrollTop() <= $scroller.outerHeight()) {
		            $('.page-number').text($sections.length);
		        } else {
		            $('.page-number').text(currentSectionNumber());
		        }
		    }
	
		    function init() {
		        $('.up').click(function () {
		            scrollTo(prevSection());
		        });
		        $('.down').click(function () {
		            scrollTo(nextSection());
		        });
	
		        $('.total-pages').text($sections.length);
		        $scroller.scroll(throttle(updatePageNumber));
		        $scroller.trigger('scroll');
	
		        if ($('.up').length > 0 && $('#navpanel').is(':visible')) {
		            $(document).keydown(function (e) {
		                if (e.keyCode == 38) {
		                    scrollTo(prevSection());
		                    e.preventDefault();
		                }
		            });
		            $(document).keydown(function (e) {
		                if (e.keyCode == 40) {
		                    scrollTo(nextSection());
		                    e.preventDefault();
		                }
		            });
		        }
		    }
		    init();
		} ($('#main')));
	},
	//getcookie
	GetCookie : function (cname) {
	    var name = cname + "=";
	    var ca = document.cookie.split(';');
	    for(var i=0; i<ca.length; i++) {
	        var c = ca[i];
	        if (c.charAt(0)==' ') c = c.substring(1);
	        if (c.indexOf(name) == 0){
	        	return c.substring(name.length, c.length);
	        }
	    }
	    return "";
	},

	//setcookie	
	SetCookie : function (name,value,days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
		document.cookie = name+"="+value+expires+"; path=/";
		return '';
	},
	
	DeleteCookie : function (name){
		var expires = "Thu, 01 Jan 1970 00:00:00 UTC";
		document.cookie = name+"=; expires="+expires+"; path=/";
		return "";
	},
	
	GetDateTime : function getDateTime(){
		var dateobj = new Date();
		var cookieval = '';
		cookieval = dateobj.getFullYear().toString();
		((dateobj.getMonth()+1).toString().length == 1) ? (cookieval += "0"+(dateobj.getMonth()+1)) : cookieval += dateobj.getMonth()+1,
		(dateobj.getDate().toString().length == 1) ? (cookieval += "0"+dateobj.getDate()) :  cookieval += dateobj.getDate(),
		(dateobj.getHours().toString().length == 1) ? (cookieval += "0"+dateobj.getHours()) :  cookieval += dateobj.getHours(),
		(dateobj.getMinutes().toString().length == 1) ? (cookieval += "0"+dateobj.getMinutes()) :  cookieval += dateobj.getMinutes()
		return cookieval;
	}
	
};

module.exports = util;
