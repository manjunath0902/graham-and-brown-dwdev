'use strict';

function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function runBrowseraTest(){
    
    var hover_selector = decodeURI(getUrlVars()['hover']);
    if(hover_selector != ''){
    	   hover_selector = hover_selector.replace(' eq ','=');
           if( $(hover_selector).length>0){
                  $(hover_selector).trigger('mouseenter');
           }
    }
    var click_selector = decodeURI(getUrlVars()['click']);
    if(click_selector != ''){
           click_selector = click_selector.replace(' eq ','=');
           if( $(click_selector).length>0){
                  $(click_selector).trigger('click');
           }
    }
}

exports.init = function(){
	runBrowseraTest();
};