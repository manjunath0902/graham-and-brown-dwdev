'use strict';

var _ = require('lodash'),
    util = require('./util');
//var dataLayer = [];
//Add To Cart Event
var addProduct = function(jsondata) {
    var productinfo = jsondata;
    var productjsonObj = JSON && JSON.parse(productinfo) || $.parseJSON(productinfo);
    var param = null;
    $.each(productjsonObj.ecommerce.add.products, function(e) {
        param = [{
            "id": this.id,
            "name": this.name,
            "price": this.price,
            "brand": this.brand,
            "category": this.category,
            'quantity': this.quantity,
            "variant": this.variant,
            'dimension6': this.dimension6,
            'group': this.group,
            'coupon': this.coupon,
            'metric3': this.metric3
        }];
    });
    dataLayer.push({
        'event': productjsonObj.event,
        'ecommerce': {
            'currencyCode': productjsonObj.ecommerce.currencyCode,
            'add': {
                'products': param
            }
        }
    });
};

//Checkout Steps
var checkoutProduct = function(jsondata) {
    var productinfo = jsondata;
    var productjsonObj = JSON && JSON.parse(productinfo) || $.parseJSON(productinfo);
    var param = [] ;
    $.each(productjsonObj.Products, function(e) {
        param.push({
            "id": this.id,
            "name": this.name,
            "price": this.price,
            "brand": this.brand,
            "category": this.category,
            'quantity': this.quantity,
            "variant": this.variant,
            'dimension6': this.dimension6
        });
    });
    dataLayer.push({
        'event': productjsonObj.event,
        'ecommerce': {
            'currencyCode': productjsonObj.currency,
            'checkout': {
                'actionField': {
                    'step': 1
                },
                'products': param
            }
        }
    });
};

//Event tracking for category sorting
var categoryPageSort = function(sortOption) {
    dataLayer.push({
        'event': 'categoryPageSort',
        'pageSort': sortOption,
    });
};

var categoryRefinement = function(){
	  var refType = "";
	  var refValue = "";
	  var refLocation = "";
	  if($(".refinements.new").length > 0){
		  refLocation = "top menu (A)";
	  }else{
		  refLocation = "side menu (B)";
	  }
	  $('.refinement').not('.category-refinement').each(function() {
			var ci = $(this);
			var ref = $(ci).find('.toggle').text();
			if(ref.length != 0){
				refType = (refType.length == 0) ? ref : refType+'/'+ref;
			}
				
	  });
	  $('.refinement ul li.selected').each(function() {
			var ci = $(this);
			var refV = $.trim($(ci).find('a').text());
			refValue= refValue +"/"+ refV;
	  });
	  
     dataLayer.push({
    	 'event': 'categoryPageRefinement',
    	 'refinementLocation' : refLocation,
    	 'refinementType': refType.replace(/(?:\r\n|\r|\n)/g, ''),
    	 'refinementValue' : refValue.replace(/(?:\r\n|\r|\n)/g, '')
     });
 };
 
var productShare = function(prodID,prodName,socialName){
	dataLayer.push({
		'event': socialName,
		'product_shared': prodName,
		'product_id_shared': prodID
	});
};

//Event tracking for adding product to wishlist
var addToWishlist = function(productID, productName) {
    dataLayer.push({
        'event': 'addToWishlist',
        'wishlistId': productID,
        'wishlistName': productName
    });
};

//Home Page main Carousel
var homeCarousel = function() {
    var count = 1;
    var slot = [];
    $('#homepage-slider ul li').each(function() {
        var ci = $(this);
        slot.push({
            'id': 'home-main',
            'creative': $(ci).find('a img').attr('src'),
            'position': 'slot' + count
        });
        count = count + 1;
    });

    dataLayer.push({
        'ecommerce': {
            'promoView': {
                'promotions': slot
            }
        }
    });
    
    $('#homepage-slider ul li').on("click", function() {
    	dataLayer.push({
	        'event': 'promotionClick',
	        'ecommerce': {
	        	'promoClick': {
        	      'promotions': [{
        	          'id': 'home-main',
        	          'name': '',
        	          'creative': $(this).find('a img').attr('src'),
        	          'position': 'slot'+$(this).index()
        	       }]
	        	}
	        }
	    });
    });
};

var homeRecommendation = function(){
	  var count=1;
	  var rec= {};
	  $('#vertical-carousel ul li').each(function() {
			rec[count] = {
				 'name': $(this).find('.product-tile .name-link').text(), 
     	         'id': $(this).find('.product-tile').attr('data-itemid'),
     	         'price': $(this).find('.product-tile .product-sales-price').text(),
     	         'brand': '',
     	         'category': '',
     	         'list': 'Homepage Recommendation Zone',
     	         'position':count
				}
				count = count+1;
	  });
	  
	  dataLayer.push({
			  'impressions': [rec]
	  });
};

//Coupon events
var applyCoupon = function(couponCode) {
    dataLayer.push({
        'event': 'promoCodeApplied',
        'promocode': couponCode,
    });
};

//Event tracking for signin and signup
var accountSignup = function() {
    dataLayer.push({
        'event': 'myAccountStart',
    });
};

//Event tracking for signin and signup
var login = function() {
    dataLayer.push({
        'event': 'myAccountSignIn',
    });
};
var signUp = function() {
    dataLayer.push({
        'event': 'myAccountComplete',
    });
};

//facebook
var facebookShare = function() {
    dataLayer.push({
        'event': 'facebookShare',
    });
};

//linkedIn
var linkedInShare = function() {
    dataLayer.push({
        'event': 'linkedInShare'
    });
};

//twitter
var twitterShare = function() {
    dataLayer.push({
        'event': 'twitterShare'
    });
};

//pinterest
var pinterestShare = function() {
    dataLayer.push({
        'event': 'pinterestFooter'
    });
};

//Footer email subscription
var footerEmailSignup = function() {
    dataLayer.push({
        'event': 'emailFooter',
    });
};

//Event tracking for AddProduct to Cart
var addToCart = function($form) {
    var pid = $form.find("input[name='pid']").val();
    var $qty = $form.find('input[name="Quantity"]');
    if ($qty.length === 0 || isNaN($qty.val()) || parseInt($qty.val(), 10) === 0) {
        $qty.val('1');
    }
    var param = {
        "pid": pid,
        'cartEvent': 'addtocart',
        'quantity': $qty.val()
    };
    $.ajax({
        type: 'GET',
        url: util.appendParamsToUrl(Urls.getGtmData, param),
        success: function(response) {
            try {
                var response = response;
                addProduct(response);
            } catch (e) {
            	//Do Nothing
            }

        }
    });

};

//Remove from cart
var removeProduct = function(pid, qty) {
    var param = {
        "pid": pid,
        'cartEvent': 'removeFromCart',
        'quantity': qty,
        'format':'ajax'
    };
    $.ajax({
        type: 'GET',
        url: util.appendParamsToUrl(Urls.getGtmData, param),
        success: function(response) {
            try {
                var response = response;
                addProduct(response);
            } catch (e) {
            	//Do Nothing
            }

        }
    });

};

//Checkout
var checkout = function(pid, qty) {
    var param = {
        'checkoutEvent': 'checkout'
    };
    $.ajax({
        type: 'GET',
        url: util.appendParamsToUrl(Urls.getGtmData, param),
        success: function(response) {
            try {
                var response = response;
                checkoutProduct(response);
            } catch (e) {
            	//Do Nothing
            }
        }
    });
};

var gtmevents = {
    init: function() {
        gtm_init();
    },
    removeProduct : removeProduct,
    addToCart : addToCart,
    footerEmailSignup : footerEmailSignup, 
    pinterestShare : pinterestShare,
    twitterShare : twitterShare,
    linkedInShare : linkedInShare,
    facebookShare : facebookShare,
    login : login,
    accountSignup : accountSignup,
    checkout : checkout,
    applyCoupon : applyCoupon,
    homeCarousel : homeCarousel,
    addToWishlist : addToWishlist,
    categoryPageSort : categoryPageSort,
    checkoutProduct : checkoutProduct,
    addProduct : addProduct,
    categoryRefinement : categoryRefinement,
    productShare : productShare,
    homeRecommendation : homeRecommendation,
    signUp :  signUp
    
};

var gtm_init = function() {
	
	var loginLink=$('.user-links a')[0];
	var registerLink=$('.user-links a')[1];
	
	//Linkedin 
    $('.footer-container .fa-linkedin-square').on('click', function(e) {
        linkedInShare();
    });
    
    //facebook
    $('.footer-container .fa-facebook-square').on('click', function(e) {
        facebookShare();
    });
    
    //twitter
    $('.footer-container .fa-twitter-square').on('click', function(e) {
        twitterShare();
    });
    
    //twitter
    $('#email-alert-signup button').on('click', function(e) {
    	footerEmailSignup();
    });
    //Home Carousel
    if ($('.pt_storefront').length > 0) {
    //   homeCarousel();
    //   homeRecommendation();
    }
	$(loginLink).on('click', function (e) {
		login();
	});
	$(registerLink).on('click', function (e) {
		accountSignup();
	});
	$('#RegistrationForm button').bind("click", function(e) {
		signUp();
    });
	$('.button-text[value="Remove"]').bind("click", function(e) {
        var productId = $(this).parents(".item-quantity-details").prevAll(".item-details").find(".sku span.value").text();
        var quantity = $(this).parents('.item-quantity-details').prev().children("input").val();
        removeProduct(productId,quantity);
    });
	$('.add-to-wishlist').bind("click", function(e) {
        var productId = $(this).parents(".item-quantity-details").prevAll(".item-details").find(".sku span.value").text();
        var name = $(this).parents(".item-quantity-details").prevAll(".item-details").find(".name a").text();
        addToWishlist(productId,name);
    });
	$('.cart-coupon-code #add-coupon').bind("click", function(e) {
        var couponCode = $('.cart-coupon-code input').val();
        applyCoupon(couponCode);
    });
	$('.button-fancy-large[value="Checkout"]').bind("click", function(e) {
        checkout();
    });

	
// Commenting for GB-618 and 627
//    $('#main').on('click', '.product-tile a:not("#quickviewbutton")', function () {
//    	dataLayer.push({
//	        'event': 'productClick',
//	        'productClicked': $(this).closest('.product-tile').attr('data-cgid'),
//	        'ecommerce': {
//	        	'click': {
//	        		 'actionField' : {
//	        			'list': $(this).closest('.product-tile').attr('data-cgid')
//	        		  },
//	        	     'products': [{
//	        	         'name': $(this).closest('.product-tile').find(' .name-link').text(), 
//	        	         'id': $(this).closest('.product-tile').attr('data-itemid'),
//	        	         'price': $(this).closest('.product-tile').find('.product-sales-price').text(),
//	        	         'brand': '',
//	        	         'category': $(this).closest('.product-tile').attr('data-cgid'),
//	        	         'position': $(this).parent().index()
//	        	      }]
//	        	}
//	        }
//	    });
//    });
};

module.exports = gtmevents;