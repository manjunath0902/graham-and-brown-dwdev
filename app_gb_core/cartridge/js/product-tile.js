'use strict';
var util = require('./util'),
	imagesLoaded = require('imagesloaded'),
	minicart = require('./minicart'),
	rating = require('./rating'); //PREVAIL-Added to handle ratings display in-case of ajax calls.
	
function gridViewToggle() {
	$('.toggle-grid').on('click', function () {
		$('.search-result-content').toggleClass('wide-tiles');
		$(this).toggleClass('wide');
	});
}

/**
 * @private
 * @function
 * @description Initializes events on the product-tile for the following elements:
 * - swatches
 * - thumbnails
 */
function initializeEvents() {
	gridViewToggle();
	rating.init(); //PREVAIL-Added to handle ratings display in-case of ajax calls.
	$('.swatch-list').on('mouseleave', function () {
		// Restore current thumb image
		var $tile = $(this).closest('.product-tile'),
			$thumb = $tile.find('.product-image .thumb-link img').eq(0),
			data = $thumb.data('current');

		$thumb.attr({
			src: data.src,
			alt: data.alt,
			title: data.title
		});
	});
	$('.swatch-list .swatch').on('click', function (e) {
		e.preventDefault();
		if ($(this).hasClass('selected')) { return; }

		var $tile = $(this).closest('.product-tile');
		$(this).closest('.swatch-list').find('.swatch.selected').removeClass('selected');
		$(this).addClass('selected');
		$tile.find('.thumb-link').attr('href', $(this).attr('href'));
		$tile.find('name-link').attr('href', $(this).attr('href'));

		var data = $(this).children('img').filter(':first').data('thumb');
		var $thumb = $tile.find('.product-image .thumb-link img').eq(0);
		var currentAttrs = {
			src: data.src,
			alt: data.alt,
			title: data.title
		};
		$thumb.attr(currentAttrs);
		$thumb.data('current', currentAttrs);
		
		/*Start JIRA PREV-466 : Product images are not updating in the compare section when the color swatches are changed in PLP.*/
		var pid = $(this).closest('.product-tile').attr( "data-itemid" );
		$( ".compare-items-panel .compare-item" ).each(function() {
			var compareid = $(this).attr( "data-itemid" );
			if(pid == compareid){
				var $compare = $(this).find('.compare-item-image').eq(0);
				$compare.attr(currentAttrs);
				$compare.data('current', currentAttrs);
			}
		});
		/*End JIRA PREV-466*/
	}).on('mouseenter', function () {
		// get current thumb details
		var $tile = $(this).closest('.product-tile'),
			$thumb = $tile.find('.product-image .thumb-link img').eq(0),
			data = $(this).children('img').filter(':first').data('thumb'),
			current = $thumb.data('current');

		// If this is the first time, then record the current img
		if (!current) {
			$thumb.data('current', {
				src: $thumb[0].src,
				alt: $thumb[0].alt,
				title: $thumb[0].title
			});
		}

		// Set the tile image to the values provided on the swatch data attributes
		$thumb.attr({
			src: data.src,
			alt: data.alt,
			title: data.title
		});
	});
	
	//save for later functionality
	$(document).on("click",".grid-tile .product-tile .saveforlater button",function(e){
		e.preventDefault();
		var $this = $(this);
		var url = Urls.addToWishlist;
		var pid = $(this).closest(".product-tile").find("input[name=pid]").val();
		url = util.appendParamToURL(url, 'pid', pid);
		url = util.appendParamToURL(url, 'format', 'ajax');
		$.ajax({
			type: 'GET',
			dataType: 'json',
			url: url,
			success: function (response) {
				if(response.success){
					if($this.closest(".product-tile").find(".saveforlater button").length > 0){
						$this.closest(".product-tile").find(".saveforlater button").addClass("added");
					}
				}else{
					if($this.closest(".product-tile").find(".saveforlater .errormsg").length > 0){
						$this.closest(".product-tile").find(".saveforlater .errormsg").show();
						$this.closest(".product-tile").find(".saveforlater .errormsg .close-error-msg").on('click', function(){
							$this.closest(".product-tile").find(".saveforlater .errormsg").hide();
						});
						$(document).mouseup(function (e){
						    var container = $(".saveforlater .errormsg");
						    if (!container.is(e.target) && container.has(e.target).length === 0) {
						        container.hide();
						    }
						});
					}
				}
			}
		});
	});
	//changes made for GB-581  start 
	$(document).on("click",".product-tile .orderasample-inner button",function(e){
		e.preventDefault();
		var pid = $(this).attr("pid");
		if(pid != '' && pid != null){
			addSampleToCart(pid,$(this));
		}
	});
	//order a sample product functionality when 1 or more products r there
	$(document).on("change",".product-tile  .orderasample-inner #input-select",function(e){
		e.preventDefault();
		var pid = $(this).val();
		if(pid != '' && pid != null){
			addSampleToCart(pid,$(this));
		}
	});	
	
}

function addSampleToCart(pid,$this){
	var url = Urls.addProduct;
	var pid = pid;
	url = util.appendParamToURL(url, 'pid', pid);
	url = util.appendParamToURL(url, 'format', 'ajax');
	$.ajax({
		type: 'GET',
		dataType: 'html',
		url: url,
		success: function (response) {
			minicart.show(response);
			if($(".product-tile .orderasample").length > 0){
				$this.closest(".product-tile .orderasample").addClass("added");
			}
			if($this.closest(".product-tile  .orderasample-inner #input-select").length > 0){
				$this.closest(".product-tile  .orderasample-inner #input-select").val('');
				//custom selectbox
				$this.closest('.product-tile  .orderasample-inner #input-select').data('selectBox-selectBoxIt').destroy();
				$this.closest('.product-tile  .orderasample-inner #input-select').selectBoxIt();
			}
		},
		failure: function () {
			window.alert(Resources.SERVER_ERROR);
		}
	});
	//end
}

exports.init = function () {
	var $tiles = $('.tiles-container .product-tile');
	if ($tiles.length === 0) { return; }
	imagesLoaded('.tiles-container').on('done', function () {
		$tiles.syncHeight()
			.each(function (idx) {
				$(this).data('idx', idx);
			});
	});
	initializeEvents();
};
