'use strict';

var ajax = require('./ajax'),
	progress = require('./progress'),
	checkout =  require('./pages/checkout'),
	util = require('./util'),
	tooltip = require('./tooltip'),
	validator = require('./validator'),
	redeye = require('./redeyetracking'),
	formPrepare = require('./pages/checkout/formPrepare'),
	shipping = require('./pages/checkout/shipping'),
	dialog = require('./dialog');

var $cache = {},
	isSPCLogin = false,
	isSPCShipping = false,
	isSPCMultiShipping = false,
	isSPCShipToShip = false,
	isSPCBilling = false;

function initializeCache() {
    $cache = {
    	main: $('#main'),
        primary: $('#primary'),
        secondary: $('#secondary')
    };
}

function initializeDom() {
    isSPCLogin = $('.spc-login').length > 0;
    isSPCShipping = $('.spc-shipping').length > 0;
    isSPCMultiShipping = $('.spc-multi-shipping').length > 0;
    isSPCShipToShip = $('.clickandcollect') > 0;
    isSPCBilling = $('.spc-billing').length > 0;
}

function initializeEvents() {
	
	if (isSPCLogin) {
        loadSPCLogin();

    } else if (isSPCShipping) {
    	
        loadSPCShipping();

    } else if (isSPCMultiShipping) {
        loadSPCMultiShipping();

    } else if (isSPCBilling) {
        loadSPCBilling();

    }
    
    $('.pt_checkout').ajaxError(function() {
    	window.location.href = Urls.cartShow;
    });
    
	$("a.tooltip").click(function(e){
		e.preventDefault();
	});
    
    $('.privacy-policy').on('click', function (e) {
		e.preventDefault();
		dialog.open({
			url: $(e.target).attr('href'),
			options: {
				height: 600
			}
		});
	});
    
    $cache.primary.off('click', '.checkout-tab-head[data-href]');
    $cache.primary.on("click", '.checkout-tab-head[data-href]', function(e) {
        e.preventDefault();
        progress.show($cache.primary);
        ajax.load({
            url: $(this).data('href'),
            type: 'POST',
            callback: function(data) {
            	if(handleBasketError(data)){
	                $cache.primary.html(data);
	                validator.init();
	                initSPC();
	                if ($cache.primary.find('.spc-login').length == 0) {
	                    tooltip.init();
	                    util.limitCharacters();
	                    util.selectbox();
	                    checkout.init();
	                }
	                progress.hide();
	                updateAnchor();
	                //Code Changes for the GB-752
	                if($('#dwfrm_singleshipping_shippingAddress_addressFields_countrySelectBoxIt').text().toUpperCase()=="NORWAY"){
	                	$('.personalidentificationnumber').closest('.form-row').show();
	                }else{
	                	$('.personalidentificationnumber').closest('.form-row').hide();
	                }
	                
	                if(SitePreferences.ShippingMethod_ClickAndCollect.trim() == $(".minishipments-method .shippingMethodID").val().trim()){
	                	retainStoreDetails(data);
	                }else if( $(".minishipments-method .shippingMethodName").text().trim() != "" ){
	                	$('.delivery-tab-text').trigger("click");
	                }
            	}
            }
        });
    });
    
    $cache.primary.on("click", '.checkout-tab-head.open[data-refreshurl]', function(e) {
        e.preventDefault();
        progress.show($cache.primary);
        ajax.load({
            url: $(this).data('refreshurl'),
            type: 'POST',
            callback: function(data) {
            	if(handleBasketError(data)){
	                $cache.primary.html(data);
	                validator.init();
	                initSPC();
	                if ($cache.primary.find('.spc-login').length == 0) {
	                    tooltip.init();
	                    util.limitCharacters();
	                    util.selectbox();
	                    checkout.init();
	                }
	                progress.hide();
	                updateSummary();
	                //Code Changes for the GB-752
	                if($('#dwfrm_singleshipping_shippingAddress_addressFields_countrySelectBoxIt').text().toUpperCase()=="NORWAY"){
	                	$('.personalidentificationnumber').closest('.form-row').show();
	                }else{
	                	$('.personalidentificationnumber').closest('.form-row').hide();
	                }
	                
	                if(SitePreferences.ShippingMethod_ClickAndCollect.trim() == $(".minishipments-method .shippingMethodID").val().trim()){
	                	retainStoreDetails(data);
	                }else if( $(".minishipments-method .shippingMethodName").text().trim() != "" ){
	                	$('.delivery-tab-text').trigger("click");
	                }
            	}
            }
        });
    });

    $cache.secondary.on("click", '.order-component-block a, .order-totals-table .order-shipping a', function(e) {
        e.preventDefault();
        progress.show($cache.primary);
        ajax.load({
            url: $(this).attr('href'),
            type: 'POST',
            callback: function(data) {
            	if(handleBasketError(data)){
	                $cache.primary.html(data);
	                validator.init();
	                initSPC();
	                tooltip.init();
	                util.limitCharacters();
	                util.selectbox();
	                checkout.init();
	                progress.hide();
	                updateAnchor();
	                //Code Changes for the GB-752
	                if($('#dwfrm_singleshipping_shippingAddress_addressFields_countrySelectBoxIt').text().toUpperCase()=="NORWAY"){
	                	$('.personalidentificationnumber').closest('.form-row').show();
	                }else{
	                	$('.personalidentificationnumber').closest('.form-row').hide();
	                }
	                
	                if(SitePreferences.ShippingMethod_ClickAndCollect.trim() == $(".minishipments-method .shippingMethodID").val().trim()){
	                	retainStoreDetails(data);
	                }else if( $(".minishipments-method .shippingMethodName").text().trim() != "" ){
	                	$('.delivery-tab-text').trigger("click");
	                }
            	}
            }
        });
    });
    $cache.primary.on('click', '.delivery-tab-text.cc-js', function(e) {
    	$(this).closest('.checkout-tab').find('.cc-js .clickandcollect-text').removeClass('act');
		$(this).closest('.checkout-tab').find('.clickandcollect').removeClass('active1');
		$(this).closest('.checkout-tab').find('.delivery-details').addClass('active1');
		$(this).closest('.checkout-tab').find('.delivery-text').addClass('act');
		$(this).closest('.checkout-tab').find('.clickandcollect-text').removeClass('act');
		
		//Dialog box functionality
		var currentSite = $("#wrapper .country-select").find("select[id$='_destination'] option[selected=selected]").val();
		 var currentCountryCode = $('.checkout-shipping #dwfrm_singleshipping_shippingAddress_addressFields_country').val();
		 var countryCode;
		 if($('.checkout-shipping #dwfrm_singleshipping_shippingAddress_addressFields_country').val() == "GB"){
			 countryCode = "UK";
		 }else{
			 countryCode = $('.checkout-shipping #dwfrm_singleshipping_shippingAddress_addressFields_country').val();
		 }
		 if((countryCode != undefined) && (currentSite != getSite(countryCode))){
			 if(currentSite == "UK"){
				 currentCountryCode = "GB";
				 showDeliveryCountryChangePopup(countryCode,"GB");
			 }else if(currentSite == "ROW"){
				 currentCountryCode = "";
				 showDeliveryCountryChangePopup(countryCode,"");
			 }else{
				 currentCountryCode = currentSite;
				 showDeliveryCountryChangePopup(countryCode,currentSite);
			 }
		 }
    }); 
    $cache.primary.on('click', '.click-and-collect-tab-text', function(e) {
		$(this).closest('.checkout-tab').find('.cc-js .delivery-text').removeClass('act');
		$(this).closest('.checkout-tab').find('.delivery-details').removeClass('active1');
		$(this).closest('.checkout-tab').find('.clickandcollect').addClass('active1');
		$(this).closest('.checkout-tab').find('.clickandcollect-text').addClass('act');
		$(this).closest('.checkout-tab').find('.delivery-tab-text').removeClass('act');
});

    $cache.primary.on('submit', 'form.address', function(e) {
        if($(this).find('button[class$=-btn]').length > 0){
        	e.preventDefault();
        	$(this).find('button[class$=-btn]').click();
        }
    });
}
//Start GB-687
$(window).on("load", function() {
	if($('input[id=checklogincustomer]').val() != undefined && $('input[id=checklogincustomer]').val() == 'true'){
		if($('input[id=checknewlogin]').val() != undefined  && $('input[id=checknewlogin]').val() == 'true'){
			if(dataLayer != undefined){
				dataLayer.push({
					'event' : 'CustomerStateInCheckout',
					'ecommerce' : {
							'CustomerState' : 'New' 
							}
				});
			}
		}else{
			if(dataLayer != undefined){
				dataLayer.push({
					'event' : 'CustomerStateInCheckout',
					'ecommerce' : {
							'CustomerState' : 'Returned' 
							}
				});
			}
		}
		
	}
	//Start GB-689
	if($('input[id=sitename]').val() == 'GrahamBrownUS'){
		$('input[id^=dwfrm_login_addtolist]').prop('checked', 'checked');
	}
	//End GB-689
});
//End GB-687
function loadSPCLogin() {
    updateSummary();
    $cache.primary.find('.spc-login').trigger("ready");
    
    $cache.primary.off().on('click', '.spc-guest-btn:not(:disabled)', function(e) {
        e.preventDefault();
        var form = $(this).closest('form');
        $('<input type="hidden" name="' + $(this).attr('name') + '" value="true" />').appendTo(form);
        form.validate();
        if (!form.valid()) {
        	//this is for Checkout - Highlighting missing fields GB-570
        	if(form.find("input.error").length > 0){
        		form.find("input.error").first().focus();
        	}
            return false;
        }
        if($('input[id^=dwfrm_login_addtolist]').is(':checked')){
        	redeye.emailSignUp(form.find("input.email").val());
        }
        progress.show($cache.primary);
        ajax.load({
            url: util.ajaxUrl(form.attr('action')),
            data: form.serialize(),
            type: 'POST',
            callback: function(data) {
            	if(handleBasketError(data)){
	                $cache.primary.html(data);
	                validator.init();
	                initSPC();
	                if ($cache.primary.find('.spc-login').length == 0) {
	                    tooltip.init();
	                    util.limitCharacters();
	                    util.selectbox();
	                    checkout.init();
	                }
	                progress.hide();
	                updateAnchor();
	                //Code Changes for the GB-752
	            	if($('#dwfrm_singleshipping_shippingAddress_addressFields_countrySelectBoxIt').text().toUpperCase()=="NORWAY"){
	            	    $('.personalidentificationnumber').closest('.form-row').show();
	            	  }else{
	            		  $('.personalidentificationnumber').closest('.form-row').hide();
	            	  }
	            	if(SitePreferences.ShippingMethod_ClickAndCollect.trim() == $(".minishipments-method .shippingMethodID").val() != undefined ? $(".minishipments-method .shippingMethodID").val() : 'Not Avail'){
	                	retainStoreDetails(data);
	                }else if( $(".minishipments-method .shippingMethodName").text().trim() != "" ){
	                	$('.delivery-tab-text').trigger("click");
	                }
            	}
            	//Start GB-687
            	 if($('input[id=checklogincustomer]').val() != null && $('input[id=checklogincustomer]').val() == 'true'){
            		if($('input[id=checknewlogin]').val() != undefined  && $('input[id=checknewlogin]').val() == 'true'){
         	    		if(dataLayer != undefined){
         					dataLayer.push({
         						'event' : 'CustomerStateInCheckout',
         						'ecommerce' : {
         								'CustomerState' : 'New' 
         								}
         					});
         				}           			
            		}else{
         	    		if(dataLayer != undefined){
         					dataLayer.push({
         						'event' : 'CustomerStateInCheckout',
         						'ecommerce' : {
         								'CustomerState' : 'Returned' 
         								}
         					});
         				}
            		}

     	    	}
     	    	//End GB-687
            }
        });
        //Start GB-687
        var emailid = $('input[id^=dwfrm_login_username]').val();
        var cookieemail = util.GetCookie(emailid);
		if(cookieemail == ''){
			util.SetCookie(emailid, emailid ,365);
			if(dataLayer != undefined){
				dataLayer.push({
					'event' : 'CustomerStateInCheckout',
					'ecommerce' : {
							'CustomerState' : 'New' 
							}
				});
			}
		}else{
			if(dataLayer != undefined){
				dataLayer.push({
					'event' : 'CustomerStateInCheckout',
					'ecommerce' : {
							'CustomerState' : 'Returned' 
							}
				});
			}
		}
		
		//End GB-687
    });
    $cache.primary.on('click', '.checkoutlogin .col-1 .account-login', function(e) {
    	$cache.primary.find(".checkoutlogin .col-1").hide();
    	$cache.primary.find(".checkoutlogin .col-2").show();
    });
    $cache.primary.on('click', '.checkoutlogin .col-2 .guest-checkout', function(e) {
    	$cache.primary.find(".checkoutlogin .col-2").hide();
    	$cache.primary.find(".checkoutlogin .col-1").show();
    });
    //this is for Checkout - Highlighting missing fields GB-570 
    /*formPrepare.init({
		continueSelector: '[name$="dwfrm_login_unregistered"]',
		formSelector:'[id$="login_unregistered"]'
	});*/
	$('#password-reset').on('click', function (e) {
		e.preventDefault();
		dialog.open({
			url: $(e.target).attr('href'),
			options: {
				//Start JIRA PREV-334 : Title is missing for the Forgot password overlay.
				dialogClass : 'password-reset-popup custom-popup',
				width : 390,
				open: function () {
					validator.init();
					var $requestPasswordForm = $('[name$="_requestpassword"]'),
						$submit = $requestPasswordForm.find('[name$="_requestpassword_send"]');
					$($submit).on('click', function (e) {
						if (!$requestPasswordForm.valid()) {
							return;
						}
						e.preventDefault();
						dialog.submit($submit.attr('name'));
					});
				}
			}
		});
	});
	$cache.primary.on("click", ".spc-login-btn:not(:disabled)", function(e) {
        e.preventDefault();
        var form = $(this).closest('form');
        $('<input type="hidden" name="' + $(this).attr('name') + '" value="true" />').appendTo(form);
        form.validate();
        if (!form.valid()) {
            return false;
        }
        redeye.emailPassed(form.find("input.email").val());
        if(typeof __s2tQ !==undefined)
        {
        	__s2tQ.push(['storeData',{'Email':form.find("input.email").val()}]);
        }
        
        progress.show($cache.primary);
        ajax.load({
            url: util.ajaxUrl(form.attr('action')),
            data: form.serialize(),
            type: 'POST',
            callback: function(data) {
            	if($(data).find('#main #primary .reset_pwsd').length > 0){
            		window.location.href = Urls.resetPassword;
            		return;
            	}
            	if(handleBasketError(data)){
	                $cache.primary.html(data);
	                if(jQuery(data).find(".checkoutlogin").length > 0){
	                	$cache.primary.find(".checkoutlogin .col-1").hide();
	                	$cache.primary.find(".checkoutlogin .col-2").show();
	                }
	                validator.init();
	                initSPC();
	                if ($cache.primary.find('.spc-login').length == 0) {
	                    tooltip.init();
	                    util.limitCharacters();
	                    util.selectbox();
	                    checkout.init();
	                    updateCustomerInfo();
	                }
	                progress.hide();
	                updateAnchor();
            	}
            	//Start GB-687
            	 if($('input[id=logincheckout]').val() != null && $('input[id=logincheckout]').val() == 'true'){
            		 if(dataLayer != undefined){
       					dataLayer.push({
       						'event' : 'CustomerStateInCheckout',
       						'ecommerce' : {
       								'CustomerState' : 'Returned' 
       								}
       					});
       				}
            	}
            	//End GB-687
            }
        });
    });
}

function loadSPCShipping() {
	if($('#dwfrm_singleshipping_shippingAddress_addressFields_countrySelectBoxIt').text().toUpperCase()=="NORWAY"){
	    $('.personalidentificationnumber').closest('.form-row').show();
	  }else{
		  $('.personalidentificationnumber').closest('.form-row').hide();
	  }
    $cache.primary.on('click', '.spc-shipping-btn:not(:disabled)', function(e) {
        e.preventDefault();
        var stateVal = $("#dwfrm_singleshipping_shippingAddress_addressFields_states_stateSelectBoxItText").text();
        var countryVal = $('input[id=siteID]').val();
        var warningMsg = $('#warningMsg').val();
        var form = $(this).closest('form');
        $('<input type="hidden" name="' + $(this).attr('name') + '" value="true" />').appendTo(form);
        form.validate();
        if (!form.valid()) {
        	//this is for Checkout - Highlighting missing fields GB-570 
        	if($('.errorcheck').length == 0){
        		$('<span class="errorcheck">'+Resources.ERROR_MESSAGE+'</span>').insertBefore('.spc-shipping-btn');
        	} 
        	if(form.find("input.error").length > 0){
        		form.find("input.error").first().focus();
        	}
        	if($("#dwfrm_singleshipping_shippingAddress").find("select").hasClass('state') && $(".input-select.state").val() == ''){
    			$(".input-select.state").focus();
    		}
            return false;
        }
        
        if( warningMsg == 'true' && stateVal.toUpperCase() == 'CALIFORNIA' && countryVal == 'GrahamBrownUS' && sessionStorage.firstVisit != 'true'){
        	var data = "<div class='payment-alert-us'><span class='warning-headding'>"+ Resources.WARNING +"</span> <span>"+ Resources.NOTIFICATIONTEXT + "<a class='stores-toggle collapsed' style='white-space:pre;'target = '_blank' href='https://www.p65warnings.ca.gov/'>" + Resources.WARNINGSITE + "</a> </span></div>";
        	var homeleavingsitedialog = dialog.create({
        		html:data,
        		options: {
        			dialogClass : 'payment-alert-us custom-popup',
        			width : 390,
        			position: {
        				my: 'center',
        				at: 'center',
        				of: 'body',
        				collision: 'flipfit'
        			}
        		},
        	});
        	$('body').css('overflow','hidden');
        	if($(".payment-alert-us").length > 0){
        		$(".ui-button-icon-primary.ui-icon.ui-icon-closethick").click(function(e){
    				e.preventDefault();
    				dialog.close();
    				sessionStorage.setItem('firstVisit', 'true');
    				$('body').css('overflow','auto');
    			});
            	$('.payment-alert-us #dialog-container').empty();
        	}
            homeleavingsitedialog.append(data).dialog('open'); 
        	return;
        }
        
        
        if(form.find("#useshippingyes").prop('checked')){
        	copyShippindAddrToBillingAddr();
        }
        progress.show($cache.primary);
        ajax.load({
            url: util.ajaxUrl(form.attr('action')),
            data: form.serialize(),
            type: 'POST',
            callback: function(data) {
            	if(handleBasketError(data)){
	                $cache.primary.html(data);
	                updateSummary();
	                validator.init();
	                initSPC();
	                tooltip.init();
	                util.limitCharacters();
	                util.selectbox();
	                checkout.init();
	                progress.hide();
	                if ($('.payment-method-options').length > 0) {
	                	$('html, body').animate({scrollTop:$('.payment-method-options').position().top - 65 }, '300');
	                }
	                if ($cache.primary.find('.spc-shipping').length == 0) {
	                	updateAnchor();
	                }else{
	                	updateAnchor(true);
	                }
            	}
            }
        });
    });
    

    //click and collect triggering to payment page
    
    $cache.primary.on('click', '.spc-shipping .clickandcollect .spc-shipping-shiptoship-btn', function(e){
    	e.preventDefault();
    	var form = $(this).closest('form');
    	$('<input type="hidden" name="' + $(this).attr('name') + '" value="true" />').appendTo(form);
    	form.validate();
    	if (!form.valid()) {
    		if(form.find("input.error").length > 0){
        		form.find("input.error").first().focus();
        	}
    		return false;
        }
    	
    	if($('input[name="store"]:checked').length > 0){
    		$(".spcshipping").find("span").remove();
    	}else{
    		if($(".errorcheck").length == 0){
    			$('<span class="errorcheck">'+Resources.ERROR_MESSAGE+'</span>').insertBefore('.spc-shipping-shiptoship-btn');
    		}
    		return false;
    	}
    	
    	
    	progress.show($cache.primary);
    	
    	ajax.load({

            url: util.ajaxUrl(form.attr('action')),
            data: form.serialize(),
            type: 'POST',
            callback: function(data) {
            	if(handleBasketError(data)){
	                $cache.primary.html(data);
	                updateSummary();
	                validator.init();
	                initSPC();
	                tooltip.init();
	                util.limitCharacters();
	                util.selectbox();
	                checkout.init();
	                progress.hide();
	                if ($('.payment-method-options').length > 0) {
	                	$('html, body').animate({scrollTop:$('.payment-method-options').position().top - 65 }, '300');
	                }
	                if ($cache.primary.find('.spc-shipping').length == 0) {
	                	updateAnchor();
	                }else{
	                	updateAnchor(true);
	                }
            	}
            }
    		
    	});
    });
    
    //end
    
    
    $cache.primary.on('click', '.shiptomultiplebutton:not(:disabled)', function(e) {
        e.preventDefault();
        var form = $(this).closest('form');
        var url = util.appendParamToURL(form.attr('action'), $(this).attr('name'), true);
        url = util.appendParamToURL(url, form.find("input[name$=_securekey]").attr("name"), form.find("input[name$=_securekey]").attr("value"));
        progress.show($cache.primary);
        ajax.load({
            url: url,
            type: 'POST',
            callback: function(data) {
            	if(handleBasketError(data)){
	                $cache.primary.html(data);
	                updateSummary();
	                validator.init();
	                initSPC();
	                tooltip.init();
	                util.limitCharacters();
	                util.selectbox();
	                checkout.init();
	                progress.hide();
	                updateAnchor();
            	}
            }
        });
    });
    
    $cache.primary.off('click', '.checkout-shipping .coupon-head');
    $cache.primary.on('click', '.checkout-shipping .coupon-head', function(e) {
    	$cache.primary.find(".checkout-shipping .coupon-head").toggleClass('active');
    	$cache.primary.find(".checkout-shipping .billing-coupon-code").slideToggle(150);
    });
    
    $('#add-coupon').off('click');
    $('#add-coupon').on('click', function (e) {
		e.preventDefault();
		var $checkoutForm = $(".checkout-shipping"),
			$couponCode = $('input[name$="_couponCode"]'),
			$error = $checkoutForm.find('.coupon-error'),
			code = $couponCode.val();
		if (code.length === 0) {
			$error.html(Resources.COUPON_CODE_MISSING);
			return;
		}

		var url = util.appendParamsToUrl(Urls.addCoupon, {couponCode: code, format: 'ajax'});
		$.getJSON(url, function (data) {
			var fail = false;
			var msg = '';
			if (!data) {
				msg = Resources.BAD_RESPONSE;
				fail = true;
			} else if (!data.success) {
				msg = data.message.split('<').join('&lt;').split('>').join('&gt;');
				fail = true;
			}
			/*JIRA PREV-96 : Billing page: Not updating the order summary and not displaying the success message, when the user applied any coupon. 
			  Added below block to display the coupon code apply message.*/
			else if(data.success){
				msg = data.message;
				$error.html(msg);
			}
			
			if (fail) {
				$error.html(msg);
				return;
			}

			//basket check for displaying the payment section, if the adjusted total of the basket is 0 after applying the coupon
			//this will force a page refresh to display the coupon message based on a parameter message
			if (data.success) {
				//PREVAIL-Added '$(".spc-billing").length === 0' to handle SPC.
				updateSummary();
			}
		});
	});
    
    var currentSite = $("#wrapper .country-select").find("select[id$='_destination'] option[selected=selected]").val();
    if(currentSite != $('.checkout-shipping #dwfrm_singleshipping_shippingAddress_addressFields_country').val()){
    	updateStateOptionsshipping($('.spc-shipping .shippingForm'));
	}
    var useasshipping = $(".spc-shipping .useasshippingaddress input[name='useasshipping']").val();
    if (useasshipping !='false') {
    	jQuery(".spc-shipping .bilingForm").hide();
		jQuery("#useshippingyes").prop('checked', true);
		jQuery("#useshippingno").prop('checked', false);

	} else {
		jQuery(".spc-shipping .bilingForm").show();
		jQuery("#useshippingyes").prop('checked', false);
		jQuery("#useshippingno").prop('checked', true);
		initBilling();
		if(currentSite != $('.checkout-shipping #dwfrm_billing_billingAddress_addressFields_country').val()){
			updateStateOptionsbilling($('.spc-shipping .bilingForm'));
		}
	}
    
    jQuery("#useshippingyes").change(function () {
		changed_hide();
	});
	
	
	jQuery("#useshippingno").change(function () {
        changed_show();
        initBilling();
    });
	
	var $formshipping = $('.spc-shipping .shippingForm');
    if($formshipping.find('select[id$="_country"]').length > 0 && $formshipping.find('select[id$="_country"]').val() == 'GB' && $formshipping.find('[name$="_state"][id$="_state"]').length > 0){
       $formshipping.find('[name$="_state"][id$="_state"]').closest(".form-row").hide();
       }
    if($("#wrapper .country-select").find("select[id$='_destination'] option[selected=selected]").val() == 'US' || $("#wrapper .country-select").find("select[id$='_destination'] option[selected=selected]").val() == 'CA'){
       $cache.primary.on('change', '.checkout-shipping #dwfrm_singleshipping_shippingAddress_addressFields_country' ,function () {
            updateStateOptionsshipping($formshipping);
      });
    }
   
  
       
     var $form = $('.spc-shipping .bilingForm');
     $('select[id$="_country"]', $form).on('change', function () {
         updateStateOptionsbilling($form);
     });
       
     function getSiteFieldType(value){
         switch(value){
            case "GB":
                   return "";
            case "FR":
                   return "INPUT";
            case "AU":
                    return "INPUT";
            case "NZ":
                	return "INPUT";                    
            case "CA":
                   return "SELECT";
            case "DE":
            return "INPUT";
            case "NL":
                   return "INPUT";
            case "US":
                   return "SELECT";
            default:
                   return "INPUT";
           }
       }
            
       function updateStateOptionsbilling (form) {
            var $form = $(form);
            if($('.checkout-shipping #dwfrm_billing_billingAddress_addressFields_country').val() != "GB"){
	            if($form.find('[name$="_state"][id$="_state"]').length > 0 && !$form.find('[name$="_state"][id$="_state"]').is(":visible")){
	                   $form.find('[name$="_state"][id$="_state"]').closest(".form-row").show();
	            }
	            $form.find('label[for$="_state"]').removeClass('error');
	            $form.find('input[name$="_state"]').removeClass('error');
	            $form.find('span[id$="_state-error"]').remove();
	            var $country = $form.find('select[id$="_country"]'),
	                site = $country.val(),
	                country = Countries[$country.val()];
	            var sitevalue = getSiteFieldType(site);
	            var isstate = $form.find(".form-row .field-wrapper .state");
	            if(isstate.length > 0)
	            {
	               var id = isstate.attr("id");
	               var name = isstate.attr("name");
	            }else{
	                 var id = "dwfrm_profile_address_states_state";
	               var name = "dwfrm_profile_address_states_state";
	            }
	            if ($country.length === 0 || !country) {
	                if($form.find('[name$="_state"][id$="_state"]').length > 0){
	                      $form.find('[name$="_state"][id$="_state"]').closest("div.form-row").find("label span").first().html("State / Province / Region");
	                      var fieldHTML = $('<input/>').attr({ type: 'text', class :'input-text state ', id:id, name:name });
	                      if($form.find('select[name$="_state"]').length){
	                            $form.find('select[name$="_state"]').replaceWith( fieldHTML);
	                      }else{
	                            $form.find('input[name$="_state"]').replaceWith( fieldHTML);
	                      }
	                   }
	                return;
	            }
	            if(sitevalue == "SELECT") 
	            { 
	                var  fieldHTML = $('<select/>').attr({ class :'input-select state valid', id:id, name:name});
	                if($form.find('select[name$="_state"]').length){
	                      $form.find('select[name$="_state"]').replaceWith( fieldHTML);
	                } else{
	                      $form.find('input[name$="_state"]').replaceWith( fieldHTML);
	                }
	            }else if(sitevalue == "INPUT"){  
	                var fieldHTML = $('<input/>').attr({ type: 'text', class :'input-text state ', id:id, name:name });
	                if($form.find('select[name$="_state"]').length){
	                      $form.find('select[name$="_state"]').replaceWith( fieldHTML);
	                      if($form.find('#stateBillValue').val() != undefined && $form.find('#stateBillValue').val() != ''){
	    	            	  $form.find('input[name$="_state"]').val($form.find('#stateBillValue').val());
	    	              }
	                }else{
	                     $form.find('input[name$="_state"]').replaceWith( fieldHTML);
	                     if($form.find('#stateBillValue').val() != undefined && $form.find('#stateBillValue').val() != ''){
	   	            	  $form.find('select[name$="_state"]').val($form.find('#stateBillValue').val());
	   	              }
	                }
	            }else{
	                   if($form.find('[name$="_state"][id$="_state"]').length > 0){
	                         $form.find('[name$="_state"][id$="_state"]').closest(".form-row").hide();
	                   }
	            }   
	            if(sitevalue == "SELECT") 
	            {
	              var arrHtml = [],
	              $stateField = $country.data('stateField') ? $country.data('stateField') : $form.find('select[name$="_state"]');
	    		  //$stateLabel = ($stateField.length > 0) ? $form.find('label[for="' + $stateField[0].id + '"] span').not('.required-indicator') : undefined;
	            }else if(sitevalue == "INPUT"){
	            	var arrHtml = [],
	                $stateField = $country.data('stateField') ? $country.data('stateField') : $form.find('input[name$="_state"]');
	                //$stateLabel = ($stateField.length > 0) ? $stateField.closest("div.form-row").find("label span").first() : undefined;
	            }
	            /*if($stateLabel){
	            	$stateLabel.html(country.regionLabel);
	            }else{
	               return;
	            }*/
	            if(sitevalue == "SELECT")      
	            {
	        	  var s;
	        	  arrHtml.push('<option value="">Select</option>');
	              for (s in country.regions) {
	                  arrHtml.push('<option value="' + s + '">' + country.regions[s] + '</option>');
	              }
	              $form.find('select[name$="_state"]').html(arrHtml.join(""));
	              if($form.find('#stateBillValue').val() != undefined && $form.find('#stateBillValue').val() != ''){
	            	  $form.find('select[name$="_state"]').val($form.find('#stateBillValue').val());
	              }
	           }
	            util.selectbox();
    	   }else{
    		   $form.find('[name$="_state"][id$="_state"]').closest(".form-row").hide();
    	   }
       }
	
	function changed_hide(){
		
		jQuery(".spc-shipping .bilingForm").hide();
		jQuery(".checkout-shipping").find("#dwfrm_billing_billingAddress_addressFields_firstName").removeClass("required");
		jQuery(".checkout-shipping").find("#dwfrm_billing_billingAddress_addressFields_lastName").removeClass("required");
		jQuery(".checkout-shipping").find("#dwfrm_billing_billingAddress_addressFields_address1").removeClass("required");
		jQuery(".checkout-shipping").find("#dwfrm_billing_billingAddress_addressFields_address2").removeClass("required");
		jQuery(".checkout-shipping").find("#dwfrm_billing_billingAddress_addressFields_city").removeClass("required");
		jQuery(".checkout-shipping").find("#dwfrm_billing_billingAddress_addressFields_country").removeClass("required");
		jQuery(".checkout-shipping").find("#dwfrm_billing_billingAddress_addressFields_postal").removeClass("required");
		jQuery(".checkout-shipping").find("#dwfrm_billing_billingAddress_addressFields_phone").removeClass("required");
		copyShippindAddrToBillingAddr();
	}
	

	function changed_show(){
        jQuery(".spc-shipping .bilingForm").show();
        if($('.spc-shipping .bilingForm select[id$="_country"]').length > 0 && $('.spc-shipping .bilingForm select[id$="_country"]').val() == 'GB' && $('.spc-shipping .bilingForm [name$="_state"][id$="_state"]').length > 0){
        	$('.spc-shipping .bilingForm [name$="_state"][id$="_state"]').closest(".form-row").find(".required-indicator").remove();
        	$('.spc-shipping .bilingForm [name$="_state"][id$="_state"]').closest(".form-row").hide();
        }else{
        	if($('.spc-shipping .bilingForm [name$="_state"][id$="_state"]').closest(".form-row").find(".required-indicator").length > 0){
        		$('.spc-shipping .bilingForm [name$="_state"][id$="_state"]').closest(".form-row").find(".required-indicator").remove();
        	}
        }
        $('.spc-shipping .bilingForm [name$="_state"][id$="_state"]').closest(".form-row").removeClass("required");
		/*jQuery(".checkout-shipping").find("#dwfrm_billing_billingAddress_addressFields_firstName").addClass("required");
		jQuery(".checkout-shipping").find("#dwfrm_billing_billingAddress_addressFields_lastName").addClass("required");
		jQuery(".checkout-shipping").find("#dwfrm_billing_billingAddress_addressFields_address1").addClass("required");
		jQuery(".checkout-shipping").find("#dwfrm_billing_billingAddress_addressFields_city").addClass("required");
		jQuery(".checkout-shipping").find("#dwfrm_billing_billingAddress_addressFields_country").addClass("required");
		//jQuery(".checkout-shipping").find("#dwfrm_billing_billingAddress_addressFields_postal").addClass("required");
		jQuery(".checkout-shipping").find("#dwfrm_billing_billingAddress_addressFields_phone").addClass("required");*/
		//removing class "postal" for removing the validation of postal code
		//jQuery(".checkout-shipping").find("#dwfrm_billing_billingAddress_addressFields_postal").removeClass("postal");
	}
	
	function copyShippindAddrToBillingAddr(){
		//Copy shipping address data to billing address
		jQuery("input[name=dwfrm_billing_billingAddress_addressFields_firstName]")
			.val(jQuery("input[name=dwfrm_singleshipping_shippingAddress_addressFields_firstName]").val());
		jQuery("input[name=dwfrm_billing_billingAddress_addressFields_lastName]")
			.val(jQuery("input[name=dwfrm_singleshipping_shippingAddress_addressFields_lastName]").val());
		jQuery("input[name=dwfrm_billing_billingAddress_addressFields_address1]")
			.val(jQuery("input[name=dwfrm_singleshipping_shippingAddress_addressFields_address1]").val());
		jQuery("input[name=dwfrm_billing_billingAddress_addressFields_address2]")
			.val(jQuery("input[name=dwfrm_singleshipping_shippingAddress_addressFields_address2]").val());
		jQuery("input[name=dwfrm_billing_billingAddress_addressFields_city]")
			.val(jQuery("input[name=dwfrm_singleshipping_shippingAddress_addressFields_city]").val());
		jQuery("input[name=dwfrm_billing_billingAddress_addressFields_postal]")
			.val(jQuery("input[name=dwfrm_singleshipping_shippingAddress_addressFields_postal]").val());
		if(jQuery("select[name=dwfrm_singleshipping_shippingAddress_addressFields_states_state]").length > 0){
			jQuery("select[name=dwfrm_billing_billingAddress_addressFields_states_state]")
			.val(jQuery("select[name=dwfrm_singleshipping_shippingAddress_addressFields_states_state]").val());
		}else if(jQuery("input[name=dwfrm_singleshipping_shippingAddress_addressFields_state]").length > 0){
			jQuery("input[name=dwfrm_billing_billingAddress_addressFields_state]")
			.val(jQuery("input[name=dwfrm_singleshipping_shippingAddress_addressFields_state]").val());
		}
		jQuery("select[name=dwfrm_billing_billingAddress_addressFields_country]")
			.val(jQuery("select[name=dwfrm_singleshipping_shippingAddress_addressFields_country]").val());
		jQuery("input[name=dwfrm_billing_billingAddress_addressFields_phone]")
			.val(jQuery("input[name=dwfrm_singleshipping_shippingAddress_addressFields_phone]").val());
	}
	
	
	$cache.primary.on('click', '.checkout-shipping .shippingForm #previous-addresses-link', function(e) {
		$(this).hide();
		//$cache.primary.find(".checkout-shipping .shippingForm #enter-address-manually").hide();
		$cache.primary.find(".checkout-shipping .shippingForm .PCA-address-search").hide();
		$cache.primary.find(".checkout-shipping .shippingForm #search-for-addresses").show();
		$cache.primary.find(".checkout-shipping .shippingForm .reg-address-list").show();
		//enebleFields();
	});
	
	$cache.primary.on('click', '.checkout-shipping .shippingForm #search-for-addresses', function(e) {
		$(this).hide();
		$cache.primary.find(".checkout-shipping .shippingForm .reg-address-list").hide();
		//$cache.primary.find(".checkout-shipping .shippingForm #enter-address-manually").show();
		$cache.primary.find(".checkout-shipping .shippingForm .PCA-address-search").show();
		$cache.primary.find('.checkout-shipping .shippingForm #previous-addresses-link').show();
		//disableFields();
	});
	
	$cache.primary.on('click', '.clickandcollect .click-and-collect-form #previous-addresses-link', function(e) {
        $(this).hide();
        $cache.primary.find(".clickandcollect .click-and-collect-form .PCA-address-search").hide();
        $cache.primary.find(".clickandcollect .click-and-collect-form #search-for-addresses").show();
        $cache.primary.find(".clickandcollect .click-and-collect-form .select-address").show();
        //enebleBillingFields();
	});
	
	$cache.primary.on('click', '.clickandcollect .click-and-collect-form #search-for-addresses', function(e) {
        $(this).hide();
        $cache.primary.find(".clickandcollect .click-and-collect-form .select-address").hide();
        $cache.primary.find(".clickandcollect .click-and-collect-form #previous-addresses-link").show();
        $cache.primary.find(".clickandcollect .click-and-collect-form .PCA-address-search").show();
        //enebleBillingFields();
	});
	

	function initBilling(){
		$cache.primary.on('click', '.checkout-shipping .bilingForm #previous-addresses-link', function(e) {
			$(this).hide();
			//$cache.primary.find(".checkout-shipping .bilingForm #enter-address-manually").hide();
			$cache.primary.find(".checkout-shipping .bilingForm .PCA-address-search").hide();
			$cache.primary.find(".checkout-shipping .bilingForm #search-for-addresses").show();
			$cache.primary.find(".checkout-shipping .bilingForm .select-address").show();
			//enebleBillingFields();
		});
		
		$cache.primary.on('click', '.checkout-shipping .bilingForm #search-for-addresses', function(e) {
			$(this).hide();
			$cache.primary.find(".checkout-shipping .bilingForm .select-address").hide();
			//$cache.primary.find(".checkout-shipping .bilingForm #enter-address-manually").show();
			$cache.primary.find(".checkout-shipping .bilingForm .PCA-address-search").show();
			$cache.primary.find('.checkout-shipping .bilingForm #previous-addresses-link').show();
			//disableBillingFields();
		});
		
	}
	
	 var currentSite = $("#wrapper .country-select").find("select[id$='_destination'] option[selected=selected]").val();
	 var currentCountryCode = $('.checkout-shipping #dwfrm_singleshipping_shippingAddress_addressFields_country').val();
	 var BasketValue = $(".BasketValue").val();
	 var countryCode;
	 if($('.checkout-shipping #dwfrm_singleshipping_shippingAddress_addressFields_country').val() == "GB"){
		 countryCode = "UK";
	 }else{
		 countryCode = $('.checkout-shipping #dwfrm_singleshipping_shippingAddress_addressFields_country').val();
	 }
	 if((countryCode != undefined) && (currentSite != getSite(countryCode))){
		 if(currentSite == "UK"){
			 currentCountryCode = "GB";
			 if(BasketValue != "true"){
				 showDeliveryCountryChangePopup(countryCode,"GB");
			 }
		 }else if(currentSite == "ROW"){
			 currentCountryCode = "";
			 showDeliveryCountryChangePopup(countryCode,"");
		 }else{
			 currentCountryCode = currentSite;
			 showDeliveryCountryChangePopup(countryCode,currentSite);
		 }
	 }
	 $cache.primary.on('change', '.checkout-shipping #dwfrm_singleshipping_shippingAddress_addressFields_country', function(e) {
		 e.preventDefault();
		 var countryCode;
		 var currentSite = $("#wrapper .country-select").find("select[id$='_destination'] option[selected=selected]").val();
		 if($('#dwfrm_singleshipping_shippingAddress_addressFields_countrySelectBoxIt').text().toUpperCase()=="NORWAY"){
		    $('.personalidentificationnumber').closest('.form-row').show();
		 }else{
		    $('.personalidentificationnumber').closest('.form-row').hide();
		 }
		 if($(this).val() == "GB"){
			 countryCode = "UK";
		 }else{
			 countryCode = $(this).val();
		 }
		 if(currentSite != getSite(countryCode)){
			 showDeliveryCountryChangePopup(countryCode,currentCountryCode);
		 }
	 });
	//this is for Checkout - Highlighting missing fields GB-570 
	 /*disables continue button until all the mandatory fields are entered
	 formPrepare.init({
		continueSelector: '[name$="shippingAddress_save"]',
		formSelector:'[id$="singleshipping_shippingAddress"]'
	});*/
	 
	 if(SitePreferences.PCAENABLED){
		 var presentSite = $("#wrapper .country-select").find("select[id$='_destination'] option[selected=selected]").val();
		 $cache.primary.on('keyup', '.checkout-shipping #dwfrm_singleshipping_postalcodeshipping,.click-and-collect-form #dwfrm_singleshipping_postalcodebilling', function(e) {
			 if($(this).val().length > 1){
				 var pcaSearchCountryCode = SitePreferences.PCASEARCHCOUNTRYCODE;
				 if(presentSite == 'US' && SitePreferences.PCASEARCHCOUNTRYCODE.split('|') != undefined){
					 pcaSearchCountryCode = SitePreferences.PCASEARCHCOUNTRYCODE.split('|')[0];
				 }else if(presentSite == 'CA' && SitePreferences.PCASEARCHCOUNTRYCODE.split('|') != undefined){
					 pcaSearchCountryCode = SitePreferences.PCASEARCHCOUNTRYCODE.split('|')[1];
				 }
				 getSuggestions(SitePreferences.PCAKEY, $(this).val(), pcaSearchCountryCode, $(this));
			 }
		 });
		 $cache.primary.on('keyup', '.checkout-shipping #dwfrm_singleshipping_postalcodebilling,.click-and-collect-form #dwfrm_singleshipping_postalcodebilling', function(e) {
			 if($(this).val().length > 1){
				 var pcaSearchCountryCode = SitePreferences.PCASEARCHCOUNTRYCODE;
				 if(presentSite == 'US' && SitePreferences.PCASEARCHCOUNTRYCODE.split('|') != undefined){
					 pcaSearchCountryCode = SitePreferences.PCASEARCHCOUNTRYCODE.split('|')[0];
				 }else if(presentSite == 'CA' && SitePreferences.PCASEARCHCOUNTRYCODE.split('|') != undefined){
					 pcaSearchCountryCode = SitePreferences.PCASEARCHCOUNTRYCODE.split('|')[1];
				 }
				 getSuggestions(SitePreferences.PCAKEY, $(this).val(), pcaSearchCountryCode, $(this));
			 }
		 });
		 var getSuggestions = function(Key, SearchTerm, Country, currentElem) {
			    $.getJSON(SitePreferences.PCARETRIEVESUGGESTIONURL+"?callback=?",
			    {
			        Key: Key,
			        SearchTerm: SearchTerm,
			        Country: Country,
			    },
			    function (data) {
			    	$(currentElem).parent().find('.suggestions').remove();
			    	if(data.Items.length > 0){
			    		var suggestion = '';
			    		$(data.Items).each(function(i, item){
			    			if(item['Next'] == "Retrieve"){
			    				suggestion = suggestion + "<li><a addrID="+ item['Id'] +">"+ item['Text'] + "</a></li>";
			    			}
			    		});
			    		if(suggestion != ''){
				    		suggestion = "<ul class='suggestions'>" + suggestion + "</ul>";
				    		$(currentElem).after(suggestion);
			    		}
			    	}else if(data.Items.length == 0){
			    		var suggestion = "<ul class='suggestions'>Sorry, there were no results</ul>";
			    		$(currentElem).after(suggestion);
			    	}
			    	if($(currentElem).parent().find('.suggestions').length > 0 && $(currentElem).parent().find('.suggestions').find('li').length > 0){
			    		$(currentElem).parent().find('.suggestions').find('li a').on('click', function(e){
			    			var $this = $(this);
			    			var parent = $(currentElem).closest('fieldset');
			    			var addrId = $this.attr("addrid");
			    			$.getJSON(SitePreferences.PCARETRIEVEADDRESSURL+"?Key="+ SitePreferences.PCAKEY + "&Id=" +addrId,
	    				    function(data){
	    				    	var response = data;
	    				    	if(response.Items.length > 0 && response.Items[0] != undefined){
	    				    		var addr = response.Items[0];
	    				    		if(addr['Line1'] != undefined && addr['Line1'] != ''){
	    				    			if(parent.find("input[name$='_address1']").length > 0){
	    				    				var company = (addr['Company'] != undefined && addr['Company'] != '')? addr['Company']+"," : '';
		    				    				parent.find("input[name$='_address1']").val(company+addr['Line1']);
	    				    			}
	    				    		}else{
	    				    			if(parent.find("input[name$='_address1']").length > 0){
	    				    				parent.find("input[name$='_address1']").val('');
	    				    			}
	    				    		}
	    				    		if(addr['Line2'] != undefined && addr['Line2'] != ''){
	    				    			if(parent.find("input[name$='_address2']").length > 0){
	    				    				parent.find("input[name$='_address2']").val(addr['Line2']);
	    				    			}
	    				    		}else{
	    				    			if(parent.find("input[name$='_address2']").length > 0){
	    				    				parent.find("input[name$='_address2']").val('');
	    				    			}
	    				    		}
	    				    		if(addr['Province'] != undefined && addr['Province'] != ''){
	    				    			if(parent.find("input[name$='_city']").length > 0){
	    				    				parent.find("input[name$='_city']").val(addr['Province']);
	    				    			}
	    				    		}else{
	    				    			if(parent.find("input[name$='_city']").length > 0){
	    				    				parent.find("input[name$='_city']").val('');
	    				    			}
	    				    		}
	    				    		if(addr['PostalCode'] != undefined && addr['PostalCode'] != ''){
	    				    			if(parent.find("input[name$='_postal']").length > 0){
	    				    				parent.find("input[name$='_postal']").val(addr['PostalCode']);
	    				    			}
	    				    		}else{
	    				    			if(parent.find("input[name$='_postal']").length > 0){
	    				    				parent.find("input[name$='_postal']").val('');
	    				    			}
	    				    		}
 	    				    		shipping.updateShippingMethodList();//added for GBSS-22
	    				    		if(addr['CountryIso2'] != undefined && addr['CountryIso2'] != ''){
	    				    			if(parent.find("input[name$='_country']").length > 0){
	    				    				parent.find("input[name$='_country']").val(addr['CountryIso2']);
	    				    			}
	    				    		}
	    				    		if(addr['City'] != undefined && addr['City'] != ''){
	    				    			if(parent.find("input[name$='_city']").length > 0){
	    				    				parent.find("input[name$='_city']").val(addr['City']);
	    				    			}
	    				    		}else{
	    				    			if(parent.find("input[name$='_city']").length > 0){
	    				    				parent.find("input[name$='_city']").val('');
	    				    			}
	    				    		}
	    				    		if(addr['ProvinceCode'] != undefined && addr['ProvinceCode'] != ''){
	    				    			if(parent.find("select[name$='_state']").length > 0){
	    				    				parent.find("select[name$='_state']").val(addr['ProvinceCode']);
	    				    				parent.find("select[name$='_state']").trigger('change');
	    				    			}
	    				    		}else{
	    				    			if(parent.find("select[name$='_state']").length > 0){
	    				    				parent.find("select[name$='_state']").val('');
	    				    			}
	    				    		}
	    				    	}
	    				    	$(currentElem).parent().find('.suggestions').hide();
	    				    });
			    		});
			    	}
			    });
		};
	 }
	 //GB-467
	$('#dwfrm_singleshipping_postalcodeshipping').on('keyup input',function (){ 
        var value = $('#dwfrm_singleshipping_postalcodeshipping').val(); 
        if(value != "") { 
            $('.pca-field-reset').show(); 
        }else{ 
                $('.pca-field-reset').hide(); 
                $('.suggestions').hide(); 
        } 
    }); 
    $('.pca-field-reset').on('click',function (){ 
        $('#dwfrm_singleshipping_postalcodeshipping').val(''); 
        $('.pca-field-reset').hide(); 
        $('.suggestions').hide();
        shipping.updateShippingMethodList(); //added for GBSS-22
    });
    
       
    //Selecting geoloction while clicking the "or use current location"
    
    $(".checkout-delivery-lookup-link.js-collect-lookup-geolocate").on("click",function(e){
    	e.preventDefault();
    	var lat ;
    	var lng ;
    	
    	if(navigator.geolocation != null){
    		navigator.geolocation.getCurrentPosition(

        		    // Success callback
        		    function(position) {

        		    	lat = position.coords.latitude;
        		    	lng= position.coords.longitude;
        		        
        		    	var url = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+lat+","+lng+"&key="+SitePreferences.GOOGLE_API_KEY;
        		    	$.ajax({
        					type: 'GET',
        					dataType: 'json',
        					url: url,
        					success: function (response) {
        						if (response.status == "OK") {
        				            if (response.results[0]) {
        				                for (var i = 0; i < response.results[0].address_components.length; i++) {
        				                    var types = response.results[0].address_components[i].types;
        				                    
        				                    for (var typeIdx = 0; typeIdx < types.length; typeIdx++) {
        				                        if (types[typeIdx] == 'postal_code') {
        				                            $("input[name=dwfrm_shiptoshop_postal]").val(response.results[0].address_components[i].short_name);
        				                        }
        				                    }
        				                }
        				            } else {
        				               //do nothing
        				            }
        				        }
        					}
        				});
        		    }
        		);
    	}
    	
    });
    
    //end 
    var currentSiteID = $('input[id=siteID]').val();
    if(currentSiteID == 'GrahamBrownUK' || currentSiteID == 'GrahamBrownNZ' || currentSiteID == 'GrahamBrownFR' || currentSiteID == 'GrahamBrownNL' || currentSiteID == 'GrahamBrownDE'){
    	$cache.primary.off('click','.candc-find').on('click', '.candc-find', function(e){
        	e.preventDefault();
        	if($('#dwfrm_shiptoshop_postal').hasClass('error')){
                $(".responce-data-isml").addClass("empty-result");
        	}else{
                $(".responce-data-isml").removeClass("empty-result");
        	}
        	var form = $(this).closest('form');
        	var address = $('input[id=dwfrm_shiptoshop_address1]').val(),
        		postcode = $('input[id=dwfrm_shiptoshop_postal]').val(),
        		region = $("#dwfrm_shiptoshop_stateSelectBoxItText").attr('data-val'),
        		mobile = $('input[id=dwfrm_shiptoshop_mobile]').val(),
        		fname = $('input[id=fName]').val(),
        		lname = $('input[id=lName]').val(),
        		emailid = $('input[id=emailID]').val();
        	
        	//Whitespaces issue fix for postcode
        	if(currentSiteID == 'GrahamBrownUK' ){
	        	if(postcode.indexOf(' ') >= 0){
	        		postcode = $('input[id=dwfrm_shiptoshop_postal]').val();
	        	}else{
	        		postcode = $('input[id=dwfrm_shiptoshop_postal]').val().replace(/^(.*)(\d)/, "$1 $2");
	        	}
        	}
        	
        	if(!form.valid()){
                return false;
        	}
        	
        	/*if(address == '' || postcode == '' || region == '' || phone == '' ){
        		if($('.errorcheck').length == 0){
        			$('<span class="errorcheck">'+Resources.ERROR_MESSAGE+'</span>').insertAfter('#dwfrm_shiptoshop_postal');
        		}
            	return false;
        	}else{
        		$("span.errorcheck").remove()
        	}*/
        	var url = util.appendParamsToUrl(Urls.shiptoshop, {
                 postcode : postcode,
                 firstname : fname,
                 lastname : lname,
                 email : emailid,
                 address : address,
                 region : region,
                 mobile : mobile,
                 format: "ajax"
             });
        	$.ajax({
            	url: url,
            	type: 'POST',
            	cache : false,
            	success: function(data) {
                	$(".responce-data-isml").html("");
                    $(".responce-data-isml").html(data);
                }
            });
        });    	
    }
}

function loadSPCMultiShipping() {
    $cache.primary.on('click', '.spc-multi-shipping-btn:not(:disabled)', function(e) {
        e.preventDefault();
        var form = $(this).closest('form');
        $('<input type="hidden" name="' + $(this).attr('name') + '" value="true" />').appendTo(form);
        form.validate();
        if (!form.valid()) {
            return false;
        }
        progress.show($cache.primary);
        ajax.load({
            url: util.ajaxUrl(form.attr('action')),
            data: form.serialize(),
            type: 'POST',
            callback: function(data) {
            	if(handleBasketError(data)){
	                $cache.primary.html(data);
	                updateSummary();
	                validator.init();
	                initSPC();
	                tooltip.init();
	                util.limitCharacters();
	                util.selectbox();
	                checkout.init();
	                progress.hide();
	                updateAnchor();
            	}
            }
        });
    });
     
    $cache.primary.on('click', '.shiptosinglebutton:not(:disabled)', function(e) {
        e.preventDefault();
        var form = $(this).closest('form');
         
        var url = util.appendParamToURL(form.attr('action'), $(this).attr('name'), true);
        url = util.appendParamToURL(url, form.find("input[name$=_securekey]").attr("name"), form.find("input[name$=_securekey]").attr("value"));
        progress.show($cache.primary);
        ajax.load({
            url: url,	   
            type: 'POST',
            callback: function(data) {
            	if(handleBasketError(data)){
	                $cache.primary.html(data);
	                updateSummary();
	                validator.init();
	                initSPC();
	                tooltip.init();
	                util.limitCharacters();
	                util.selectbox();
	                checkout.init();
	                progress.hide();
	                updateAnchor();
            	}
            }
        });
    });
    
    $cache.primary.on("click", '.item-shipping-address a', function(e) {
        e.preventDefault();
        progress.show($cache.primary);
        ajax.load({
            url: $(this).attr('href'),
            type: 'POST',
            callback: function(data) {
            	if(handleBasketError(data)){
	                $cache.primary.html(data);
	                validator.init();
	                initSPC();
	                tooltip.init();
	                util.limitCharacters();
	                util.selectbox();
	                checkout.init();
	                progress.hide();
	                updateAnchor();
            	}
            }
        });
    });
    
}

function loadSPCBilling() {
	
	$cache.ccContainer = $("#PaymentMethod_CREDIT_CARD");
	$cache.ccList = $("#creditCardList");
	$cache.ccOwner = $cache.ccContainer.find("input[name$='creditCard_owner']");
	$cache.ccType = $cache.ccContainer.find("select[name$='_type']");
	$cache.ccNum = $cache.ccContainer.find("input[name$='_number']");
	$cache.ccMonth = $cache.ccContainer.find("[name$='_month']");
	$cache.ccYear = $cache.ccContainer.find("[name$='_year']");
	$cache.ccCcv = $cache.ccContainer.find("input[name$='_cvn']");
	
	if($cache.ccType != null && $cache.ccNum != null){
		cardtype($cache.ccNum, $cache.ccType);
	}
	
	$cache.primary.on('click', '.spc-billing-btn:not(:disabled)', function(e) {
        if($('input[name$=_selectedPaymentMethodID]:checked').val() == 'PayPal'){
        	$(this).removeClass('spc-billing-btn');
        	return true;
        }
    	e.preventDefault();
        var form = $(this).closest('form');
        form.validate();
        if (!form.valid()) {
            return false;
        }
        progress.show($cache.primary);
        ajax.load({
            url: util.ajaxUrl(form.attr('action')),
            data: form.serialize(),
            type: 'POST',
            callback: function(data) {
            	if(handleBasketError(data)){
	                $cache.primary.html(data);
	                updateSummary();
	                if ($cache.primary.find('.spc-summary').length == 0) {
	                    tooltip.init();
	                    util.limitCharacters();
	                    util.selectbox();
	                    validator.init();
	                    checkout.init();
	                }
	                initSPC();
	                progress.hide();
	                if ($cache.primary.find('.spc-billing').length == 0) {
	                	updateAnchor();
	                }else{
	                	updateAnchor(true);
	                }
            	}
            }
        });
    });
    
	$cache.primary.on('click', '.redemption.giftcert a.remove', function(e){
    	 e.preventDefault();
    	 progress.show($cache.primary);
    	 var url = $(this).attr("href");
         ajax.load({
             url: url,
             data: {},
             type: 'POST',
             callback: function(data) {
            	 if(handleBasketError(data)){
	                 $cache.primary.html(data);
	                 updateSummary();
	                 validator.init();
	                 initSPC();
	                 tooltip.init();
	                 util.limitCharacters();
	                 util.selectbox();
	                 checkout.init();
	                 progress.hide();
            	 }
             }
         });
    });
	
	$('#dwfrm_billing_paymentMethods_creditCard_number').keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
		// changes made for GBSS-16
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190 ,229]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
             // Allow: Ctrl+C
            (e.keyCode == 67 && e.ctrlKey === true) ||
             // Allow: Ctrl+X
            (e.keyCode == 88 && e.ctrlKey === true) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
	//changes made for GBSS-16
	$('#dwfrm_billing_paymentMethods_creditCard_number').keyup(function (e) {
		var text =$("#dwfrm_billing_paymentMethods_creditCard_number")
		var regex = /[^\d]/g;
		var txt = $(text).val();      
		if(regex.test(txt)){
		   $(text).val(txt.replace(regex, ''));
		   return false;
		}else{
			return true;
		}
	});
	$(document).ready(function(){
	    $("#dwfrm_billing_paymentMethods_creditCard_number").on("keyup keypress keydown paste", function(){   
	      var a =  $("#dwfrm_billing_paymentMethods_creditCard_number").val().length;
	        if(a > 16){
	           $("#dwfrm_billing_paymentMethods_creditCard_number").val($("#dwfrm_billing_paymentMethods_creditCard_number").val().substr(0,16));
	        }
	    });
	});
	
	if($(".spc-billing .checkout-billing").find("input[name='Error']").length > 0 && $(".spc-billing .checkout-billing").find("input[name='Error']").val() == "true"){
		var data = "<div class='paymentfail'><h1>"+ Resources.PAYNENTERRORNOTIFICATIONTITLE +"</h1> <p>"+ Resources.PAYNENTERRORNOTIFICATIONTEXT +"</p><button type='buttont' class='prm-btn' id='paymentfailagree'>"+ Resources.PAYNENTERRORNOTIFICATIONBUTTON +"</button></div>";
		dialog.open({
			html:data,
			options: {
				dialogClass : 'payment-fail custom-popup',
				width : 390
			},
			callback: function () {
				$(".payment-fail .paymentfail #paymentfailagree").click(function(e){
					e.preventDefault();
					dialog.close();
				});
			}
		});
	}
	
	//GB-466
	$(".checkout-notice-link").click(function(e){
		e.preventDefault();
		var url = $(this).attr("href");
		dialog.open({
			url: url,
			options: {
				dialogClass : 'checkout_popup_links'
			}
		});
	});
}
//this is for Checkout - Highlighting missing fields GB-570 
$(document).on('click','.payformcheck', function(){
	var form = $(this).closest('form');
	if(!form.valid()){
		if(form.find("input.error").length > 0){
    		form.find("input.error").first().focus();
    	}
		return false;
	}
});
//Payment method validation
function cardtype  ($cardinput, $typeselector) {
	var $con = $($cardinput).not(':hidden').closest('form');
	if ($($cardinput).length > 0 && $con != undefined) {
		$con.find($cardinput).focus(function(){
			if($(this).val() != "" && $(this).hasClass('tooltipstered')){
				$(this).tooltipster('destroy');
			}
		}).blur(function () {
			var val = $.trim($(this).val());
			var regex = /^[a-zA-Z]+$/;
			var errorspan = $(this).closest('.form-row').find('span.error');
			var $error = false;

			if ($(this).closest('.form-row').find('.cardtypeimg').length == 0) {
				$(this).closest('.form-row').append('<span class="cardtypeimg"><span style="display:none;"></span></span>');
			}

			if (val.length > 0 && val.indexOf('*') == -1) {

				var cardTypeval = validatecardtype(val);

				if (cardTypeval != "Error") {
					$($typeselector).find('option[label="' + cardTypeval + '"]').prop('selected', true);
					$(this).closest('.form-row').find('.cardtypeimg > span').attr('data-cardlabel', cardTypeval).show();

					if ((cardTypeval == "MasterCard") || (cardTypeval == "Visa") || (cardTypeval == "Discover") || (cardTypeval == "Maestro")) {
						if (val.length < 16 && cardTypeval != "Visa") {
							$error = true;
						}
						if (val.length != 13 && val.length != 16 && cardTypeval == "Visa") {
							$error = true;
						}

					} else if (cardTypeval == "Discover") {
						if (val.length < 15) {
							$error = true;
						}
					} else {
						$error = false;
					}
				} else {
					$error = true;
					$($typeselector).find('option').eq(0).prop('selected', true);
					$(this).closest('.form-row').find('.cardtypeimg > span').removeAttr('data-cardlabel').hide();
				}

			}else if(val.indexOf('*') == -1){
				$error = true;
			}

			if ($con.find($cardinput).val().length < 4) {
				$(this).closest('.form-row').find('.cardtypeimg > span').hide();
			}
		});

		$con.find($cardinput).each(function(){
			var $val = $(this).val();
			if ($(this).closest('.form-row').find('.cardtypeimg').length == 0) {
				$(this).closest('.form-row').append('<span class="cardtypeimg"><span style="display:none;"></span></span>');
			}

			if($(this).closest('form').attr('data-cardType') != undefined){
				$(this).closest('.form-row').find('.cardtypeimg > span').attr('data-cardlabel', $(this).closest('form').attr('data-cardType')).show();
			}else if (validatecardtype($val) != "Error" && $(this).val() != "") {
				$(this).closest('.form-row').find('.cardtypeimg > span').attr('data-cardlabel', validatecardtype($val)).show();
			}

			if($val != '' && $(this).hasClass('creditcard_masked')){
				$(this).attr('cardval', $val);
				$(this).val('************'+ $val.substr($val.length - 4, 4));
				$(this).removeClass('creditcard_masked');
			}
		});

		$con.on('click','#applyBtn', function(e){
			var $type = "Error";
			if($con.find($cardinput).attr('readonly') == undefined){
				if($con.find($cardinput).val() != '' && validatecardtype($con.find($cardinput).val().indexOf('*') != -1 ? $con.find($cardinput).attr('cardval') : $con.find($cardinput).val()) == "Error"){
					e.preventDefault();
					showcartooltip($con.find($cardinput));
					return false;
				}else if($con.find($cardinput).val().indexOf('*') != -1 && validatecardtype($con.find($cardinput).attr('cardval')) != "Error"){
					$con.find($cardinput).val($con.find($cardinput).attr('cardval'));
				}
			}else{
				if($con.find($cardinput).val().indexOf('*') != -1){
					$con.find($cardinput).val($con.find($cardinput).attr('cardval'));
				}
			}

		});

		$con.on('click','.cancel-button', function(e){
			if($con.find($cardinput).hasClass('tooltipstered')){
				$con.find($cardinput).tooltipster('hide');
			}
		});
	}
}
$(window).on("load", function() {
	if($('input[id=siteid]').val() == "GrahamBrownUS"){
		$('input[id^=dwfrm_login_addtolist]').prop('checked', 'checked');
		$('#confsubscribe').trigger("click");
	}
});

//GB-680
$('#confsubscribe').click(function() {
	if($('input[id^=dwfrm_login_addtolist]').is(':checked')){
		redeye.emailSignUp($('input[id=customer-email]').val());
		if($('input[id=siteid]').val() != "GrahamBrownUS"){
			$('#confsubscribe-div').replaceWith("<h3>"+Resources.THANK_YOU_SUBSCRIBE+"</h3>");
		}
	}
});

function validatecardtype(val) {
	var cardregex = {
		mastercard: /^5[1-5][0-9]{2,14}$/,
		visa: /^4[0-9]{3,15}$/,
		amex: /^3[47]([0-9]{2,13})$/,
		discover: /^6(?:011[0-9]{0,12}|5[0-9]{2,14})$/,
		maestro: /^(?:5[0678]\d\d|6304|6390|67\d\d)\d{8,15}$/

	}
	var result = " ",
		carNo = val;

	// first check for MasterCard
	if (cardregex.mastercard.test(carNo)) {
		result = "MasterCard";
	}
	// then check for Visa
	else if (cardregex.visa.test(carNo)) {
		result = "Visa";
	}
	// then check for AmEx
	else if (cardregex.amex.test(carNo)) {
		result = "American Express";
	} else if (cardregex.discover.test(carNo)) {
		result = "Discover";
	} 
	 else if (cardregex.maestro.test(carNo)) {
		result = "Maestro";
	}
	else {
		result = "Error"
	}
	return result;
}

function updateSummary() {
    var url = Urls.summaryRefreshURL;
    var summary = $('#secondary.summary');
    summary.load(url, function() {
        summary.find('.checkout-mini-cart .minishipment .header a').hide();
        summary.find('.order-totals-table .order-shipping .label a').hide();
        util.miniSummaryArrow();
    });
}

function updateAnchor(noDefault) {
    var erroredElements = $cache.primary.find('.form-row.error:visible').filter(function(i, ele) {
        return $.trim($(ele).html()).length > 0
    });

    if (erroredElements.length > 0) {
        jQuery('html, body').animate({
            scrollTop: erroredElements.first().position().top
        }, 500);

    } else if($('.billing-error').length > 0){
    	jQuery('html, body').animate({
            scrollTop: $('.billing-error').position().top
        }, 500);
    }
    else if(!noDefault){
    	 if($('#navigation').length > 0){
			 jQuery('html, body').animate({
		            scrollTop: $('#navigation').position().top
	         }, 500);
		 }
    }
}

//while click on edit in shippingpage reating the click and collect data
function retainStoreDetails(data){
	$('.clickandcollect-text').trigger("click");
    $('.candc-find').trigger("click");
    setTimeout(function(){ 
    	var storeName = $(".mini-shipment.order-component-block .details .address .shopAddress").html().trim();
    	var $storesList = $(".suggested-stores.custmradio");
    	var storeSelection;
    	$storesList.each(function () {
    		if($(this).find(".checkout-delivery-locations-name").text().trim() == storeName){
    			$(this).find('input[type="radio"]').prop("checked",true);
    			$(this).closest(".suggested-stores.custmradio").addClass("selected");
    			$('.pickup-stores').animate({scrollTop: 0},0);
    			var storesOffset = $('.suggested-stores.selected').offset().top - $('.pickup-stores').offset().top;
	               $('.pickup-stores').animate({
	                     scrollTop: storesOffset
	                 },0);
    		}
		});
	}, 7000);
}
function handleBasketError(data) {
    if ($(data).find('.cart-empty').length > 0 || $(data).find("#header").length > 0) {
        window.location.href = Urls.cartShow;
        return false;
    }
    return true;
}

function updateCustomerInfo() {
	ajax.load({
        url: Urls.customerInfo,
        type: 'POST',
        async: false,
        callback: function(data) {
        	$('#navigation').find(".menu-utility-user").find(".user-info").remove();
        	$(data).insertAfter("#navigation .menu-utility-user li:first");
        }
    });
}

function initSPC(){
	initializeCache();
	initializeDom();
	initializeEvents();
}

function showDeliveryCountryChangePopup(countryCode,currentCountryCode){
	 var destination = countryCode;
	 var locale;
	 if(window.countries[countryCode] != undefined && window.countries[countryCode] != null){
		 locale = window.countries[countryCode].language[0].ID;
	 }else{
		 locale = window.countries['ROW'].language[0].ID;
		 destination = "ROW";
	 }
	 var data = "<div class='changeDeliveryCountry'><h1>"+ Resources.CHANGEDELIVERYCOUNTRYTITLE +"</h1> <span>"+ Resources.CHANGEDELIVERYCOUNTRYDESC +"</span><p>"+ Resources.CHANGEDELIVERYCOUNTRYCONFIRM +"</p><button type='buttont' class='sec-btn' id='changeNo'>"+ Resources.CHANGEDELIVERYCOUNTRYNO +"</button><button type='buttont' class='sec-btn' id='changeYes'>"+ Resources.CHANGEDELIVERYCOUNTRYYES +"</button></div>";
	 var $formshipping = $('.spc-shipping .shippingForm');
	 dialog.open({
		html:data,
		options: {
			dialogClass : 'change-delivery custom-popup info',
			width : 390,
            beforeClose: function () {
                $('.checkout-shipping #dwfrm_singleshipping_shippingAddress_addressFields_country').val(currentCountryCode);
                 if($("#wrapper .country-select").find("select[id$='_destination'] option[selected=selected]").val() == 'US' || $("#wrapper .country-select").find("select[id$='_destination'] option[selected=selected]").val() == 'CA'){
                	 updateStateOptionsshipping($formshipping);
                 }
                 $('select.country').data('selectBox-selectBoxIt').destroy();
                 $('select.country').selectBoxIt();
            }
		},
		callback: function () {
			$(".changeDeliveryCountry #changeNo").click(function(e){
				e.preventDefault();
				dialog.close();
				$('.checkout-shipping #dwfrm_singleshipping_shippingAddress_addressFields_country').val(currentCountryCode);
				
				if($("#wrapper .country-select").find("select[id$='_destination'] option[selected=selected]").val() == 'US' || $("#wrapper .country-select").find("select[id$='_destination'] option[selected=selected]").val() == 'CA'){
					updateStateOptionsshipping($formshipping);
                }
				
				$('select.country').data('selectBox-selectBoxIt').destroy();
				$('select.country').selectBoxIt();
			});
			$(".changeDeliveryCountry #changeYes").click(function(e){
				e.preventDefault();
				var url = Urls.changeSite;
				url = util.appendParamToURL(url, 'destination', destination);
				url = util.appendParamToURL(url, 'language', locale);
				url = util.appendParamToURL(url, 'format', 'ajax');
				$.ajax({
					type: 'GET',
					dataType: 'json',
					url: url,
					success: function (response) {
						if(response.success){
							dialog.close();
							window.location.href = response.redirectURL;
						}
					}
				});
			});
		}
	});
	//Code Changes for the GB-752
	if($('#dwfrm_singleshipping_shippingAddress_addressFields_countrySelectBoxIt').text().toUpperCase()=="NORWAY"){
	    $('.personalidentificationnumber').closest('.form-row').show();
	  }else{
		  $('.personalidentificationnumber').closest('.form-row').hide();
	  }
}

function updateStateOptionsshipping (formshipping) {
    var $form = $(formshipping);
    if($form.find('[name$="_state"][id$="_state"]').length > 0 && !$form.find('[name$="_state"][id$="_state"]').is(":visible")){
           $form.find('[name$="_state"][id$="_state"]').closest(".form-row").show();
    }
    $form.find('label[for$="_state"]').removeClass('error');
    $form.find('input[name$="_state"]').removeClass('error');
    $form.find('span[id$="_state-error"]').remove();
    var $country = $form.find('select[id$="_country"]'),
        site = $country.val(),
        country = Countries[$country.val()];
   if(site == 'CA' || site == 'US')
       {
       var arrHtml = [],
       $stateField = $country.data('stateField') ? $country.data('stateField') : $form.find('select[name$="_state"]');
       //$stateLabel = ($stateField.length > 0) ? $form.find('label[for="' + $stateField[0].id + '"] span').not('.required-indicator') : undefined;
           /*if ($stateLabel) {
                $stateLabel.html(country.regionLabel);
           } else {
                return;
           }*/
           var s;
           arrHtml.push('<option value="">Select</option>');
           for (s in country.regions) {
               arrHtml.push('<option value="' + s + '">' + country.regions[s] + '</option>');
            }
           $form.find('select[name$="_state"]').html(arrHtml.join(""));
           if($form.find('#stateShipValue').val() != undefined && $form.find('#stateShipValue').val() != ''){
         	  $form.find('select[name$="_state"]').val($form.find('#stateShipValue').val());
           }
           $form.find('label[for$="_state"]').removeClass('error');
           $form.find('select[name$="_state"]').removeClass('error');
           $form.find('span[id$="_state-error"]').remove();
           setTimeout(function(){ 
        	   $form.find('select[name$="_state"]').data('selectBox-selectBoxIt').destroy();
               $form.find('select[name$="_state"]').selectBoxIt();
   			}, 1000);
       }else{
          return;
       }
   }


function getSite(value){
	switch(value){
	    case "UK":
			return "UK";
		case "FR":
			return "FR";
		case "AU":
	    	return "AU";
		case "NZ":
	    	return "NZ";	    	
		case "CA":
			return "CA";
		case "DE":
	    	return "DE";
		case "NL":
			return "NL";
		case "US":
			return "US";
		default:
			return "ROW";
	}
}

exports.init = function () {
    initializeCache();
    initializeDom();
    initializeEvents();
};
