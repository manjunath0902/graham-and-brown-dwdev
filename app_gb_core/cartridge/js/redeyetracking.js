'use strict';

var ajax = require('./ajax'),
	util = require('./util');

var redeyetracking = {
		/**
		 * @function
		 * @description Function to trigger social share event in PDP
		 * @param {Object} params social share name, current product Id.
		 */
		socialShare: function (event,product) {
			var url = Urls.redEye;
			url = util.appendParamToURL(url, 'event', 'socialshare');
			if(event != undefined && event != null && event != ''){
				url = util.appendParamToURL(url, 'share', event);
			}
			if(product != undefined && product != null && product != ''){
				url = util.appendParamToURL(url, 'pid', product);
			}
			url = util.appendParamToURL(url, 'format', 'ajax');
			$.ajax({
				type: 'GET',
				dataType: 'html',
				url: url,
				success: function (response) {
					$("body").append(response);
				}
			});
		},
		colourCard: function (event,form){
			var url = Urls.redEye;
			url = util.appendParamToURL(url, 'event', event);
			url = util.appendParamToURL(url, 'format', 'ajax');
			var data = form.serialize();
			$.ajax({
				type: 'POST',
				url: url,
				data: data,
				dataType: 'html',
				success: function (response) {
					$("body").append(response);
				}
			});
		},
		emailPassed: function (email){
			var url = Urls.redEye;
			url = util.appendParamToURL(url, 'event', 'emailpassed');
			url = util.appendParamToURL(url, 'email', email);
			url = util.appendParamToURL(url, 'format', 'ajax');
			$.ajax({
				type: 'POST',
				url: url,
				dataType: 'html',
				success: function (response) {
					$("body").append(response);
				}
			});
		},
		emailSignUp: function (email){
			var url = Urls.redEye;
			url = util.appendParamToURL(url, 'event', 'emailsignup');
			url = util.appendParamToURL(url, 'email', email);
			url = util.appendParamToURL(url, 'format', 'ajax');
			$.ajax({
				type: 'POST',
				url: url,
				dataType: 'html',
				success: function (response) {
					$("body").append(response);
				}
			});
		}
};

module.exports = redeyetracking;
