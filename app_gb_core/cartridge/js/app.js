/**
 *    (c) 2009-2014 Demandware Inc.
 *    Subject to standard usage terms and conditions
 *    For all details and documentation:
 *    https://bitbucket.com/demandware/sitegenesis
 */

'use strict';

var countries = require('./countries'),
	cq = require('./cq'),
	dialog = require('./dialog'),
	minicart = require('./minicart'),
	page = require('./page'),
	searchplaceholder = require('./searchplaceholder'),
	searchsuggest = require('./searchsuggest'),
	searchsuggestbeta = require('./searchsuggest-beta'),
	tooltip = require('./tooltip'),
	util = require('./util'),
	validator = require('./validator');

// if jQuery has not been loaded, load from google cdn
if (!window.jQuery) {
	var s = document.createElement('script');
	s.setAttribute('src', 'https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js');
	s.setAttribute('type', 'text/javascript');
	document.getElementsByTagName('head')[0].appendChild(s);
}

require('./jquery-ext')();
require('./cookieprivacy')();


var SetMenuTimer = 150;
var MegaMenuTimer = {
    id: null,
    clear: function() {
        if (MegaMenuTimer.id) {
            window.clearTimeout(MegaMenuTimer.id); 
            delete MegaMenuTimer.id;
        }
    },
    start: function(duration, $curObj) {
        MegaMenuTimer.id = setTimeout(function() {
            if (!$curObj.hasClass('hovered-list')) {
                $curObj.siblings('li').removeClass('level-3-hovered');
                $curObj.addClass('level-3-hovered');
            }
        }, duration);
    }
};


function initializeEvents() {
	// Mega menu delay function
    $(document).off('mouseenter', '.menu-category .has-sub-menu').on('mouseenter', '.menu-category .has-sub-menu', function() {
    	var $curObj = $(this);
        if (window.matchMedia('(min-width: 1023px)').matches) {
            if (!$curObj.hasClass('hovered-list')) {
                $curObj.siblings('li').removeClass('hovered-list');
                $curObj.addClass('hovered-list');
                if( !$curObj.find('.level-2 .has-submenu').first().hasClass('level-3-hovered')) {
                	$curObj.find('.level-2 .has-submenu').siblings('li').removeClass('level-3-hovered');
                	$curObj.find('.level-2 .has-submenu').first().addClass('level-3-hovered');
                }
            }
        }
    }).off('mouseleave', '.menu-category .has-sub-menu').on('mouseleave', '.menu-category .has-sub-menu', function() {
    	var $curObj = $(this);
        if (window.matchMedia('(min-width: 1023px)').matches) {
        	$curObj.removeClass('hovered-list');
        	$curObj.find('.level-2 .has-submenu').first().removeClass('level-3-hovered');
        }
    });    
    if($(window).width() > 1023){
    	$(".top-banner .social-links-share").click(function(e){
    		$(".top-banner .social-links").toggle();
    	});
    }
    $(document).off('mouseenter', '.level-2 .has-submenu').on('mouseenter', '.level-2 .has-submenu', function() {
        if (window.matchMedia('(min-width: 1023px)').matches) {
            var $curObj = $(this);
            MegaMenuTimer.clear();
            MegaMenuTimer.start(SetMenuTimer, $curObj);
        }
    }).off('mouseleave', '.level-2 .has-submenu').on('mouseleave', '.level-2 .has-submenu', function() {
        if (window.matchMedia('(min-width: 1023px)').matches) {
            MegaMenuTimer.clear();
        }
    }); 
        
	var controlKeys = ['8', '13', '46', '45', '36', '35', '38', '37', '40', '39'];
	var slot = [];
	if (window.pageContext.type=="storefront" || window.pageContext.type=="search") {
		var promoId = "home-main";
		if(window.pageContext.type=="storefront")
			promoId = "home-main";
		else if(window.pageContext.type=="search")
			promoId = "globalslot";
		var homepageImp = null;
		if ($(window).width() > 1023) {
                homepageImp = $('#homepage-slider .banner1,.normal-btn-big:visible,.slot-below-usermenu.mobile .gtmeventsclick a img,.slot-below-usermenu.mobile .html-slot-container .top-promo,.html-slot-container .slot-banner-img,.category-banner-container.section .category-banner-cell.category-cell-1,.homepage-bottom-slots .section .banner5 .banner5-one a img,.homepage-bottom-slots .section .banner5 .banner5-two.hide-only-mobile a img,.homepage-bottom-slots .section .banner5 .banner5-three a img,#homepage-slider .banner4 div.show-only-desktop,.banner3 span.show-only-desktop:eq(1)');
	   	}else{
                homepageImp = $('#homepage-slider .banner1,.normal-btn-big:visible, .slot-below-usermenu.desktop .html-slot-container .top-promo,.slot-below-usermenu.mobile .gtmeventsclick a img, .homepage-bottom-slots .section .banner5 .banner5-two.show-only-mobile.slick-initialized.slick-slider a img,.html-slot-container .slot-banner-img,.category-banner-container.section .category-banner-cell.category-cell-1,.homepage-bottom-slots .section .banner2 ul li,.homepage-bottom-slots .section .banner2 .banner2-left .content-asset,.homepage-bottom-slots .section .banner4 .banner4-left');
	   	}
		$.each(homepageImp, function(index) {
			var url = '';
			if(window.pageContext.type == "storefront"){
				 url = $(this).find('a img').data('src') != undefined ? $(this).find('a img').data('src') : $(this).find('a img').attr('src') ;
                        url = ((url === ''||url == undefined) && $(this).data('src') != undefined) ? $(this).data('src') : $(this).attr('src')!=undefined && $(this).attr('src').length > 0 ? $(this).attr('src'): url;
				 url = url == undefined ? $(this).find('a').text() : url.lastIndexOf('?') > 0 ? url.split('?')[0] : url;
				 url = (url == undefined && $(this).find('a').text() && url.lastIndexOf('?') < 0 ) ? $(this).text() : url;
                        url = (url.length == 0) ? $(this).text().trim().replace('\n','') : url;
			}else{
                        url = $(this).is('img')?$(this).attr('src'):$(this).find('a img').attr('src');
			}
                 if($(this).is('img'))
                 {
                        $(this).length > 0 ? $(this).attr('index',index+1) : '';
                 }else
                 {
			$(this).find('a img').length > 0 ? $(this).find('a img').attr('index',index+1) : $(this).find('a').length > 0 ? $(this).find('a').attr('index',index+1) :$(this).attr('index',index+1);
                 }
                 
			var prodName = $(this).find('.name-link') != '' ? $(this).find('.name-link').text().trim() : $(this).find('a').text() != '' ? $(this).find('a').text().trim() : '';
			if(url != undefined){
				var count = url.lastIndexOf('/');
				var urlimg =url.slice(count+1);
				var imgjpj = urlimg.substr(urlimg.length-3,urlimg.length) == 'png' ? urlimg.replace(urlimg.substr(urlimg.length-3) ,'jpg') : urlimg;
				if(imgjpj == "lazy-placeholder.jpg" ||imgjpj == "blank_img.jpg" ||imgjpj == "blank_ampersand.jpg" ){
					// some unwanted images are loading that's why we are placing this.
				}else{
					var crea = urlimg.substr(urlimg.length-3,urlimg.length) == 'png' ? urlimg.replace(urlimg.substr(urlimg.length-3) ,'jpg') : urlimg;
                              crea =  crea.replace("?$staticlink$","");
					slot.push({
						 'id': $(this).find('#banner-ids').length > 0 ? $(this).find('#banner-ids').text() :$(this).closest('.banner2').find('#home-bottom-one').val() != null ? $(this).closest('.banner2').find('#home-bottom-one').val() : promoId,
					     'name': ($(this).find('a img').attr('gtmpname') != null) ? $(this).find('a img').attr('gtmpname') :($(this).attr('gtmpname') != null) ? $(this).attr('gtmpname'): ($(this).attr('gtmpname') != null) ? $(this).attr('gtmpname') : ($(this).find('a').attr('gtmpname') != null) ? $(this).find('a').attr('gtmpname') :  prodName ,
					     'creative': crea,
					     'position': index+1
				 });
				}	
			}
		});
		if(window.pageContext.type=="search"){
			var promoViewed = false;
			if($('.pt_plppage').length > 0){
				var plp = $('.col-lg-12.col-md-12.col-sm-12 .still-banner');
				$.each(plp, function(index) {
					var url = ($(this).find('img').attr('src') != null)?$(this).find('img').attr('src'):$('.sitewideBanner').parent().attr('href');
					var count = url.lastIndexOf('/');
					var prodName = $(this).find('.name-link') != '' ? $(this).find('.name-link').text().trim() : $(this).find('a').text() != '' ? $(this).find('a').text().trim() : '';
					var urlimg =url.slice(count+1);
					if(urlimg == "lazy-placeholder.jpg" ||urlimg == "blank_img.jpg" ||urlimg == "blank_ampersand.jpg" || urlimg == "shareyourwalls1.jpg" ){
						// some unwanted images are loading that's why we are placing this.
					}else{
						slot.push({
							 'id': $(this).find('#banner-ids').length > 0 ? $(this).find('#banner-ids').text() :'plpBodyContents',
						     'name': ($(this).find('img').attr('gtmpname') != null) ? $(this).find('img').attr('gtmpname') : prodName ,
						     'creative': $(this).find('a img').data('src') != undefined ? $(this).find('a img').data('src') : urlimg,
						     'position': index+1
					 });
					}	
				});
				$(".inner-content.right-text-title").find("a").each(function(index){
					$(this).find('span').length > 0 ? $(this).find('span').attr('index',1) : $(this).attr('index',1);
                    slot.push({
                            'id': $(this).closest('.inner-content').find('.category-tile h1').text(),
                            'name': $(this).find('span').length > 0 ? $(this).find('span').text() : $(this).text(),
                            'creative': $(this).find('span').length > 0 ? $(this).find('span').text() : $(this).text(),
                            'position': 1
                    });
                });
				
			}
		}
		if(window.pageContext.type != 'search'){
			dataLayer.push({
				'event' : 'promotions_loaded',
				'ecommerce' : {
					'promoView' : {
						'promotions' : slot 
						}}});
			//gtm reload for homepage GB-645
			var $homePageReload = $('#gtmReload');
			var url = Urls.gtmReload;
			//url = util.appendParamToURL(url, 'format', 'ajax');
			$homePageReload.load(url);
			//end
		}else{
                if($('.pt_plppage').length==0 && $('.clpdata').length == 0){
				if($(window).width() > 1023){
					dataLayer.push({
						'event' : 'promotions_loaded',
						'ecommerce' : {
							'promoView' : {
								'promotions' : slot 
								}}});
				}
				//gtm reload for clptwo GB-646
				var $clpTwo = $('#gtmReload');
				var url = Urls.gtmReload;
				//url = util.appendParamToURL(url, 'format', 'ajax');
				$clpTwo.load(url);
				//end
			}
			for(var i=0;i<dataLayer.length;i++){
				if(dataLayer[i].ecommerce!= null && !promoViewed){
					promoViewed = true;
					dataLayer[i].ecommerce.promoView = {
							promotions:slot
					}
				}
			}
		}
		
	}
	//Changes made for GB-620
	if(window.pageContext.type == "storefront"){
          $('#homepage-slider .banner1,.normal-btn-big:visible,.slot-below-usermenu.mobile .gtmeventsclick a,.slot-below-usermenu.mobile .html-slot-container .top-promo,.html-slot-container .slot-banner-img,.category-banner-container.section .category-banner-cell.category-cell-1,.homepage-bottom-slots .section .banner5 .banner5-one a img,.homepage-bottom-slots .section .banner5 .banner5-two.hide-only-mobile a img,.homepage-bottom-slots .section .banner5 .banner5-three a img,#homepage-slider .banner4 div.show-only-desktop,.banner3 span.show-only-desktop:eq(1)').on("click", function(e) {
			var url = '';
                        url = $(this).find('a img').data('src') != undefined ? $(this).find('a img').data('src') : $(this).find('a img').length>0 ? $(this).find('a img').attr('src'):$(this).find('img').attr('src');
				 url = (url == undefined && $(this).data('src') != undefined) ? $(this).data('src') : url;
				 url = url == undefined ? $(this).find('a').text() : url.lastIndexOf('?') > 0 ? url.split('?')[0] : url;
 
			var prodName = $(this).find('.name-link') != '' ? $(this).find('.name-link').text().trim() : $(this).find('a').text() != '' ? $(this).find('a').text().trim() : '';
			if(url != undefined){
				var count = url.lastIndexOf('/');
				var urlimg =url.slice(count+1);
				urlimg = urlimg == "" ? $(this).text().trim() : urlimg;
				var imgjpj = urlimg.substr(urlimg.length-3,urlimg.length) == 'png' ? urlimg.replace(urlimg.substr(urlimg.length-3) ,'jpg') : urlimg;
                        var position = $(this).find('a img').length > 0 ? $(this).find('a img').attr('index') : $(this).find('img').length>0 ? $(this).find('img').attr('index') : $(this).attr('index');
				position = position == undefined ? $(this).parent().parent().find('a img').attr('index') : position;
				if(imgjpj == "lazy-placeholder.jpg" ||imgjpj == "blank_img.jpg" ||imgjpj == "blank_ampersand.jpg"  ){
					// some unwanted images are loading that's why we are placing this.
				}else{
					slot.push({
						 'id': $(this).find('#banner-ids').length > 0 ? $(this).find('#banner-ids').text() :$(this).closest('.banner2').find('#home-bottom-one').val() != null ? $(this).closest('.banner2').find('#home-bottom-one').val() : "home-main",
					     'name': ($(this).find('a img').attr('gtmpname') != null) ? $(this).find('a img').attr('gtmpname') :($(this).attr('gtmpname') != null) ? $(this).attr('gtmpname'): ($(this).attr('gtmpname') != null) ? $(this).attr('gtmpname') : ($(this).find('a').attr('gtmpname') != null) ? $(this).find('a').attr('gtmpname') :  prodName ,
					     'creative': urlimg.substr(urlimg.length-3,urlimg.length) == 'png' ? urlimg.replace(urlimg.substr(urlimg.length-3) ,'jpg') : urlimg,
					     'position': position
				 });
				}	
			}
			
			if(window.pageContext.type=="storefront"){
				dataLayer.push({
					'event' : 'promotionClick',
					'ecommerce' : {
						'promoClick' : {
							'promotions' : slot
						}}});
			}
         });
	}
	//Changes made for GB-619
	var slot = [];
	var promoId = "globalslot";
	if(window.pageContext.type == "search"){
          $('#homepage-slider .banner1,.normal-btn-big:visible,.slot-below-usermenu.mobile .gtmeventsclick a,.html-slot-container .slot-banner-img,.slot-below-usermenu.mobile .html-slot-container .top-promo,.category-banner-container.section .category-banner-cell.category-cell-1,.homepage-bottom-slots .section .banner5 .banner5-one a img,.homepage-bottom-slots .section .banner5 .banner5-two.hide-only-mobile a img,.homepage-bottom-slots .section .banner5 .banner5-three a img,#homepage-slider .banner4 div.show-only-desktop,.banner3 span.show-only-desktop:eq(1)').on("click", function(e) {
                   var url = ($(this).find('a img').attr('src') != null)?$(this).find('a img').attr('src'):$(this).is('a')?$(this).find('img').attr('src'):$('.sitewideBanner').parent().attr('href');
			var count = url.lastIndexOf('/');
			var urlimg =url.slice(count+1);
                 var position = $(this).find('a img').length > 0 ? $(this).find('a img').attr('index') : $(this).find('img').length>0 ? $(this).find('img').attr('index') : $(this).attr('index');
			position = position == undefined ? $(this).parent().parent().find('a img').attr('index') : position;
			slot.push({
				'id': $(this).find('#banner-ids').length > 0 ? $(this).find('#banner-ids').text() :  promoId,
        	    'name': $(this).find('a img').attr('gtmpname') != null ? $(this).find('a img').attr('gtmpname') : "",
             'creative': urlimg.replace(urlimg.substr(urlimg.length-3) ,'jpg').replace('?$staticlijpg',''),
        	    'position': position
			});
			if(window.pageContext.type=="search"){
				dataLayer.push({
					'event' : 'promotionClick',
					'ecommerce' : {
						'promoClick' : {
							'promotions' : slot
						}}});
			}
	    }); 
			if($('.pt_plppage').length > 0){
				$('.col-lg-12.col-md-12.col-sm-12 .still-banner .inner-text a').on('click',function(){
					var url = ($(this).attr('href') != null)?$(this).attr('href'):$('.sitewideBanner').parent().attr('href');
						slot.push({
							 'id': $(this).closest('.inner-content').find('.category-tile h1').text() ,
						     'name': $(this).attr('gtmpname') != null ? $(this).attr('gtmpname') :$(this).find('span').length > 0 ? $(this).find('span').text() : $(this).text(),
						     'creative': $(this).text(),
						     'position': 1
					 });
						dataLayer.push({
							'event' : 'promotionClick',
							'ecommerce' : {
								'promoClick' : {
									'promotions' : slot
								}}});
				});
			}
	}
	////Changes made for GB-618 and GB-627
	var slot = [];
	var array = new Array();
	if(window.pageContext.type == "search"){
		$('body').on('click','.search-result-content .product-tile',function(){
			$( ".breadcrumb .breadcrumb-element" ).each(function( index ) {
				var arrayVals = $(this).text();
				array.push(arrayVals);
			});
			var breadCr = array.join('|')
			if($('#searchqueary').val() != "null"){
				var actionField={'list': 'Search Results'}
			}else if ($('#actionajax').val()=="ajax" && $('#searchqueary').val() == "null"){
				var actionField={'action':'click','list': breadCr+"- FILTERED"};
			}else{
				var actionField={'list': breadCr};
			}
			var variant = $('#searchqueary').val() != "null" ? $('#searchqueary').val() : '';
			slot.push({
				'name': $(this).find('.product-name').length > 0 ? $(this).find('.product-name').text().trim() : '' ,
				'id': $(this).attr('data-itemid').length > 0 ? $(this).attr('data-itemid') : '',
				'price':$(this).find('.product-sales-price').text() != "N/A" ? $(this).find('.product-sales-price').text().slice(1).trim() :'0.00', 
				'brand': $(this).attr('data-brand').length > 0 ? $(this).attr('data-brand') : '' ,
				'category': $(this).attr('data-primary-cgid').length > 0 ? $(this).attr('data-primary-cgid') : '',
				'variant': variant,
				'position': $(this).parent().attr('data-index') != undefined ? $(this).parent().attr('data-index').replace('.0','') : ""
			});
			dataLayer.push({
				'event' : 'productClick',
				'ecommerce' : {
					'click' : {
						'actionField': actionField,
							'products' : slot
					}}});
		});
	}
	////Changes made for GB-614
	var slot = [];
	var array = new Array();
	if(window.pageContext.type == "search" && $('.clpdata').length > 0){
		$('.product-listing .product-tile').on('click',function(){
			$( ".breadcrumb .breadcrumb-element" ).each(function( index ) {
				var arrayVals = $(this).text();
				array.push(arrayVals);
			});
			var breadCr = array.join('|');
			var actionField={'list': breadCr};
			slot.push({
				'name': $(this).find('.product-name').length > 0 ? $(this).find('.product-name').text().trim() : '' ,
				'id': $(this).attr('data-itemid').length > 0 ? $(this).attr('data-itemid') : '',
				'price':$(this).find('.product-sales-price').text() != "N/A" ? $(this).find('.product-sales-price').text().slice(1).trim() :'0.00', 
				'brand': $(this).attr('data-brand').length > 0 ? $(this).attr('data-brand') : '' ,
				'category': $(this).attr('data-primary-cgid').length > 0 ? $(this).attr('data-primary-cgid') : '',
				'variant': '',
				'position': $(this).parent().attr('data-prodcount') != undefined ? $(this).parent().attr('data-prodcount').replace('.0','') : ''
			});
			dataLayer.push({
				'event' : 'productClick',
				'ecommerce' : {
					'click' : {
						'actionField': actionField,
							'products' : slot
					}}});
		});
	}
	
	////Changes made for GB-623
	var slot = [];
	var array = new Array();
	if(window.pageContext.type == "product"){
		$('.product-tile').on('click',function(){
			slot.push({
				'name': $(this).find('.product-name').length > 0 ? $(this).find('.product-name').text().trim() : '' ,
				'id': $(this).attr('data-itemid').length > 0 ? $(this).attr('data-itemid') : '',
				'price':$(this).find('.product-sales-price').text() != "N/A" ? $(this).find('.product-sales-price').text().slice(1).trim() :'0.00', 
				'brand': $(this).attr('data-brand').length > 0 ? $(this).attr('data-brand') : '' ,
				'category': $(this).attr('data-primary-cgid').length > 0 ? $(this).attr('data-primary-cgid') : '',
				'variant': '',
				'position': $(this).closest('.grid-tile').attr('data-grid-tile-count') != undefined ? $(this).closest('.grid-tile').attr('data-grid-tile-count') : ''
			});
			var list = $(this).closest('.grid-tile').find("input[name=listName]").val();
			dataLayer.push({
				'event' : 'productClick',
				'ecommerce' : {
					'click' : {
						'actionField': {'list': list},
							'products' : slot
					}}});
		});
	}
	//GB-613 start
	window.onload = function() {
		var impressions;
		var youmaylikecount=1;
		for(var i=0;i<dataLayer.length;i++){
			if(dataLayer[i].ecommerce!= null){
				impressions = dataLayer[i].ecommerce.impressions;
			}
		}
		if(impressions != undefined){
			var youmaylike =  $('div.similar-products-content .recommendations #carousel-recommendations .slick-track .slick-slide').not('.slick-cloned');
			$.each(youmaylike, function(){
				impressions.push({
					'name': $(this).find('input[class="abtest-product-name"]').val(),
					'id':  $(this).find('input[class="abtest-pid"]').val(),
					'price':  $(this).find('input[class="abtest-price"]').val(),
					'brand':  $(this).find('input[class="abtest-brand"]').val(),
					'category':  $(this).find('input[class="abtest-category"]').val(),
					'variant': '',
					'list': 'You May Also Like',
					'position':	youmaylikecount++
				});
			});
		}
		var len = $('.product-col-1 #thumbnails ul li').length;
		if(len <= 4) {
			$('#thumbnails').addClass('tt');
		}
		if($('.bv-details-bar').length == 0)
		{
			$('#pdpMain').find('.product-col-2 #BVRRSummaryContainer .bv-action-bar').addClass('review-for-NZ');
		}
		var currentSite = $("#wrapper .country-select").find("select[id$='_destination'] option[selected=selected]").val();
		if(currentSite == "NL"){ 
			$('#pdpMain').find('.product-col-2 #BVRRSummaryContainer .bv-write-review-container').addClass('review-for-NL');
		}else if(currentSite == "FR"){ 
			$('#pdpMain').find('.product-col-2 #BVRRSummaryContainer .bv-write-review-container').addClass('review-for-FR');
		}else if(currentSite == "DE"){ 
			$('#pdpMain').find('.product-col-2 #BVRRSummaryContainer .bv-write-review-container').addClass('review-for-DE');
		}else if(currentSite == "CA"){ 
			$('#pdpMain').find('.product-col-2 #BVRRSummaryContainer .bv-write-review-container').addClass('review-for-CA');
		}	
	};
	//GB-613 end
	//PREVAIL-Added to handle form dialog boxes server side issues.
	$("body").on("click", ".dialogify, [data-dlg-options], [data-dlg-action]", require('./dialogify').setDialogify)
		.on('keydown', 'textarea[data-character-limit]', function (e) {
			var text = $.trim($(this).val()),
				charsLimit = $(this).data('character-limit'),
				charsUsed = text.length;

				if ((charsUsed >= charsLimit) && (controlKeys.indexOf(e.which.toString()) < 0)) {
					e.preventDefault();
				}
		})
		.on('change keyup mouseup', 'textarea[data-character-limit]', function () {
			var text = $.trim($(this).val()),
				charsLimit = $(this).data('character-limit'),
				charsUsed = text.length,
				charsRemain = charsLimit - charsUsed;

			if (charsRemain < 0) {
				$(this).val(text.slice(0, charsRemain));
				charsRemain = 0;
			}

			$(this).next('div.char-count').find('.char-remain-count').html(charsRemain);
		});

	/**
	 * initialize search suggestions, pending the value of the site preference(enhancedSearchSuggestions)
	 * this will either init the legacy(false) or the beta versions(true) of the the search suggest feature.
	 * */
	var $searchContainer = $('#navigation .header-search');
	if (SitePreferences.LISTING_SEARCHSUGGEST_LEGACY) {
		searchsuggest.init($searchContainer, Resources.SIMPLE_SEARCH);
	} else {
		searchsuggestbeta.init($searchContainer, Resources.SIMPLE_SEARCH);
	}

	// print handler
	$('.print-page').on('click', function () {
		window.print();
		return false;
	});

	// add show/hide navigation elements
	$('.secondary-navigation .toggle').click(function () {
		$(this).toggleClass('expanded').next('ul').toggle();
	});
	$(window).scroll(function() {
	    if ($(this).scrollTop()>50) 
	     {
	        $('.serviceMessages, .sitewideBanner, .slot-below-usermenu .html-slot-container').hide();
	     }
	    else
	     {
	      $('.serviceMessages, .sitewideBanner, .slot-below-usermenu .html-slot-container').show();
	     }
	});
	

	// add generic toggle functionality
	$('.toggle').next('.toggle-content').hide();
	$('.toggle').click(function (e) {
		e.preventDefault();//JIRA PREV-90 : When click on advance search from gift registry login page, focus is happening towards top of the page.
		$(this).toggleClass('expanded').next('.toggle-content').toggle();
	});

	// subscribe email box
	var $subscribeEmail = $('.subscribe-email');
	if ($subscribeEmail.length > 0)	{
		$subscribeEmail.focus(function () {
			var val = $(this.val());
			if (val.length > 0 && val !== Resources.SUBSCRIBE_EMAIL_DEFAULT) {
				return; // do not animate when contains non-default value
			}

			$(this).animate({color: '#999999'}, 500, 'linear', function () {
				$(this).val('').css('color', '#333333');
			});
		}).blur(function () {
			var val = $.trim($(this.val()));
			if (val.length > 0) {
				return; // do not animate when contains value
			}
			$(this).val(Resources.SUBSCRIBE_EMAIL_DEFAULT)
				.css('color', '#999999')
				.animate({color: '#333333'}, 500, 'linear');
		});
	}

	$('.privacy-policy').on('click', function (e) {
		e.preventDefault();
		dialog.open({
			url: $(e.target).attr('href'),
			options: {
				height: 600
			}
		});
	});

	// main menu toggle
	$('.menu-toggle').on('click', function () {
		$('#wrapper').toggleClass('menu-active');
	});
	$('.menu-category li .menu-item-toggle').on('click', function (e) {
		e.preventDefault();
		var $parentLi = $(e.target).closest('li');
		$parentLi.siblings('li').removeClass('active').find('.menu-item-toggle').removeClass('angle-down active').addClass('angle-right');
		$parentLi.toggleClass('active');
		$(e.target).toggleClass('angle-right angle-down active');
	});
	
	// Fix for tablet
	$('body').on('click', '.mobile_device .has-sub-menu.submenu_inactive', function(e) {
		e.preventDefault();
		$(this).removeClass('submenu_inactive');
		$(this).siblings().addClass('submenu_inactive');
	});
	
	$('.user-account').on('click', function (e) {
		e.preventDefault();
		$(this).parent('.user-info').toggleClass('active');
		$('.mini-cart-content').hide();
	});
	$('.user-panel').on('click', function (e) {
		$(this).toggleClass('active');
	});
	$(".header-search button[type='submit']").on('click', function () {
        $(".fix-head .header-search input").focus();
    });
	
	if ($(window).width() > 1024) {
		$(window).scroll(function() {  
		    var scroll = $(window).scrollTop();

		    if (scroll >= 201) {
		    	if (!$("#wrapper").hasClass("fix-head")) {
		    		$("#wrapper").addClass("fix-head");
		    		$("#header").fadeTo(0,0);
		    		$("#header").fadeTo(700,1);
		    	}
		    }
		    if (scroll <= 199) {
		        $("#wrapper").removeClass("fix-head");
		    }
		});
	}
		
	$('body').on('click','#navpanel .to-top', function(){
		$('html,body').animate({scrollTop: 0}, 500);
	});
	
	
	// fix for firefox issue in right side pagination
	$(window).scroll(function() {
	   if($(window).scrollTop() + $(window).height() > $(document).height() - 15) {
			var totalPage = $('.total-pages').text();
	       $('.page-number').text(totalPage);
	   }
	});
	$(window).scroll(function(){
		$(".backtoTop-button").addClass('backtoTop-button-show');
		if($(window).scrollTop() === 0) {
			$(".backtoTop-button").removeClass('backtoTop-button-show');
		}

		$(".backtoTop-button").click(function(){
				$(window).scrollTop(0);
		});
	});	
	
	$('body').on('click','.footer-left h3', function(){
		if ($(window).width() < 1024) {
			$(this).toggleClass("expand");
			$(this).next('.pipe').slideToggle();
		}
	});
	
	$(window).resize(function(){
		if ($(window).width() > 1023) {
			$(".footer-left h3").removeClass('expand');
			$(".footer-left .pipe").show();
		}
		$('.tiles-container .product-tile').css('height','auto');
		$('.tiles-container .product-tile').syncHeight();
	});
	
	//adding class for mobile device
	if(util.isMobile()) {
		 $('#wrapper').addClass('mobile_device');
	}
	
	//custom selectbox
	util.selectbox();
}
/**
 * @private
 * @function
 * @description Adds class ('js') to html for css targeting and loads js specific styles.
 */
function initializeDom() {
	// add class to html for css targeting
	$('html').addClass('js');
	if (SitePreferences.LISTING_INFINITE_SCROLL) {
		$('html').addClass('infinite-scroll');
	}
	// load js specific styles
	util.limitCharacters();
}

var pages = {
	account: require('./pages/account'),
	cart: require('./pages/cart'),
	checkout: require('./pages/checkout'),
	compare: require('./pages/compare'),
	product: require('./pages/product'),
	registry: require('./pages/registry'),
	search: require('./pages/search'),
	storefront: require('./pages/storefront'),
	wishlist: require('./pages/wishlist'),
	storelocator: require('./pages/storelocator')
};

var app = {
	init: function () {
		if (document.cookie.length === 0) {
			$('<div/>').addClass('browser-compatibility-alert').append($('<p/>').addClass('browser-error').html(Resources.COOKIES_DISABLED)).appendTo('#browser-check');
		}
		initializeDom();
		initializeEvents();

		// init specific global components
		countries.init();
		tooltip.init();
		minicart.init();
		validator.init();
		searchplaceholder.init();
		cq.init();
		// execute page specific initializations
		$.extend(page, window.pageContext);
		var ns = page.ns;
		if (ns && pages[ns] && pages[ns].init) {
			pages[ns].init();
		}
		
		//BISN for G&B
		require('./bisn').init();

		//SPC for G&B
		require('./spc').init();
		
		//GTM
		var gtmevents= require('./gtmevents');
		gtmevents.init();
		
		require('./browsera').init();
	}
};

// general extension functions
(function () {
	String.format = function () {
		var s = arguments[0];
		var i, len = arguments.length - 1;
		for (i = 0; i < len; i++) {
			var reg = new RegExp('\\{' + i + '\\}', 'gm');
			s = s.replace(reg, arguments[i + 1]);
		}
		return s;
	};
})();

// initialize app
$(document).ready(function () {
	app.init();
	
	
	$('button[name$="home-email"]').on('click',function(e) {
		e.preventDefault();
		emailsubscription($(this));
	});
	
	$('button[name$="content-email"]').on('click',function(e) {
		e.preventDefault();
		emailsubscription($(this));
	});
	
	function emailsubscription(currentname){
		var textemail = currentname.closest('#email-alert-signup').find('#email-alert-address').val();
		var txtemail=$.trim(textemail);
		var emailExp = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if(!txtemail.match(emailExp)){
			currentname.closest('form').find('.span-message').addClass('error').text(Resources.VALIDATE_EMAIL);
			return false;
		}
		else{
			currentname.closest('form').find('.span-message').addClass('error').text('');
			var url = Urls.emailUrl;
			url = util.appendParamToURL(url, 'email', txtemail);
		    dialog.open({
		    	url: url,
		    	options: {
					dialogClass : 'custom-popup',
					width : 390
		    	}
		    });
		  //SUB2 Tracking
			if(__s2tQ!=undefined || __s2tQ!="undefined" )
                __s2tQ.push(['storeData',{'Email' : txtemail , 'Optout1P' : '0' , 'Optout3P' : '1'}]);
		} 
	}
	
	//Added for email validation in header login section
	$('button[name$="account-email"]').on('click',function(e) {
		e.preventDefault();
		var txtemail =$('.user-panel form#mail-alert-signup').find('#mail-alert-address').val();
		var emailExp = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if(!txtemail.match(emailExp)){
			$('.spn-message').addClass('error').text(Resources.VALIDATE_EMAIL);
			return false;
		}
		else{
			$('.spn-message').addClass('error').text('');
			var url = Urls.emailUrl;
			url = util.appendParamToURL(url, 'email', txtemail);
			dialog.open({
				url: url,
				options: {
					dialogClass : 'custom-popup',
					width : 390
		    	}
		   });
			
			//SUB2 Tracking
			if(__s2tQ!=undefined || __s2tQ!="undefined" )
                __s2tQ.push(['storeData',{'Email' : txtemail , 'Optout1P' : '0' , 'Optout3P' : '1'}]);

		}    
	});

});

//Launch window popup

$(document).ready(function(){
	if(SitePreferences.LAUNCH_WINDOW_ENABLED){
		if(getCookie("launchwindow") && $("#header").find("select[id$='_destination']").val() != getCookie("launchwindow").toUpperCase()){
			setCookie("launchwindow", '', -1);
		}
		if(!getCookie("launchwindow") || getCookie("launchwindow") == 'true'){
			var url = Urls.launchWindow;
			dialog.open({
				url: url,
				options: {
					dialogClass: "launch-window",
				},
				callback: function () {
						var form = $("#LaunchWindowForm");
						if(form.find("select[id$='_destination']").val() != null && form.find("select[id$='_destination']").val() != ''){
							launchwindow.updateLanguageOptions(form.find("select[id$='_destination']"), form,false);
						}
						if(form.valid()){
							form.find("#submitBtn").removeAttr("disabled");
						}
						form.find("select[id$='_destination']").on('change',function(){
							launchwindow.updateLanguageOptions(form.find("select[id$='_destination']"), form, false);
						});
						form.find("select[id$='_destination'],select[id$='_language']").on('change',function(){
							if(form.valid()){
								form.find("#submitBtn").removeAttr("disabled");
							}else{
								form.find("#submitBtn").attr("disabled","disabled");
							}
						});
						form.find("#submitBtn").click(function(){
							if(form.valid()){
								launchwindow.changeURL(form);
							}
						});
				}
			});
		}else{
			var site = getCookie("launchwindow");
			if(window.location.href.replace(window.location.protocol+"//",'') == window.location.hostname || window.location.href.replace(window.location.protocol+"//",'') == window.location.hostname+"/"){
				window.location.href = "https://"+window.location.hostname+"/"+site;
			}
		}
	}
	$("#header").find("select[id$='_destination']").on('change',function(){
		launchwindow.updateLanguageOptions($("#header").find("select[id$='_destination']"), $("#header").find("#HeaderChangeFlag"),false);
		launchwindow.changeURL($(this).closest("form"));
	});
	$("#header").find("select[id$='_language']").on('change',function(){
		launchwindow.changeURL($(this).closest("form"));
	});
	if($("#header").find("select[id$='_destination'] option[selected=selected]").val() != null && $("#header").find("select[id$='_destination'] option[selected=selected]").val() != ''){
		launchwindow.updateLanguageOptions($("#header").find("select[id$='_destination'] option[selected=selected]"), $("#header").find("#HeaderChangeFlag"),true);
	}
	$('.last-visited .search-result-items, .featured-products .search-result-items').slick({
		  dots: false,
		  infinite: true,
		  speed: 300,
		  slidesToShow: 5,
		  slidesToScroll: 1,
		  responsive: [{
            breakpoint: 1023,
            settings: {
                slidesToShow: 3
            }
        }, {
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                dots: true
            }
        }]
	});
	$(window).on("load", function() {
		if ($("#wrapper").hasClass("pt_cart")) {
			$('.last-visited .grid-tile').syncHeight();
			$('.featured-products .grid-tile').syncHeight();
		}
	});
	$('.banner5-one, .banner5-two, .banner5-three').slick({
		infinite: true,
		speed: 300,
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: true,
		dotsClass: 'custom_paging',
		customPaging: function (slider, i) {
		    return  (i + 1) + '/' + slider.slideCount;
		}
	});
	$('.cat-land-row .product-listing .search-result-items').slick({
		  infinite: true,
		  speed: 300,
		  slidesToShow: 4,
		  slidesToScroll: 4,
		  dots: false,
		  responsive: [{
              breakpoint: 1700,
              settings: {
                  slidesToShow: 3,
                  slidesToScroll: 3
              }
          }, {
        	  breakpoint: 1280,
              settings: {
                  slidesToShow: 2,
                  slidesToScroll: 2
              }
          }, {
              breakpoint: 768,
              settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1,
                  dots: true,
                  dotsClass: 'custom_paging',
                  customPaging: function (slider, i) {
                      return  (i + 1) + '/' + slider.slideCount;
                  },
              }
          }]
	});
	util.countrySelectbox();
	//GB-716
	//Start GB-687
	$(window).on("load", function() {
		if($(window).width() >= 1024) {
			$('#secondary .refinement').find('ul').addClass('refinement-hide');
			$('.refinement.category-refinement').insertBefore('.bottom-apply');
		}
	    ACCORDION.init();
		if(window.pageContext.type != "checkout" && window.pageContext.type != "orderconfirmation" ){
			if($('input[id=logincustomer]').val() != undefined && $('input[id=logincustomer]').val() == 'true'){
				if($('input[id=newlogin]').val() != undefined  && $('input[id=newlogin]').val() == 'true'){
					if(dataLayer != undefined){
						dataLayer.push({
							'event' : 'CustomerState',
							'ecommerce' : {
									'CustomerState' : 'New' 
									}
						});
					}
				}else{
					if(dataLayer != undefined){
						dataLayer.push({
							'event' : 'CustomerState',
							'ecommerce' : {
									'CustomerState' : 'Returned' 
									}
						});
					}
				}			
			}else{
				if(getCookie('Graham&Brown') == ''){
					var cookievalue = parseInt(util.GetDateTime());
					setCookie('Graham&Brown',cookievalue,365);
					if(dataLayer != undefined){
						dataLayer.push({
							'event' : 'CustomerState',
							'ecommerce' : {
									'CustomerState' : 'New' 
									}
						});
					}
				}else{
					var cookietime = parseInt(getCookie('Graham&Brown'));
					var currenttime = parseInt(util.GetDateTime());
					var check = currenttime - cookietime;
					if(check <= SitePreferences.MaxTimePeriod){
						if(dataLayer != undefined){
							dataLayer.push({
								'event' : 'CustomerState',
								'ecommerce' : {
										'CustomerState' : 'NEW' 
										}
							});
						}
					}else{
						if(dataLayer != undefined){
							dataLayer.push({
								'event' : 'CustomerState',
								'ecommerce' : {
										'CustomerState' : 'Returned' 
										}
							});
						}
					}
				}
			}
		}
	});
	//End GB-687
});
//GB-703
var ACCORDION = {
		  full: null,
		  short: null,
		  init: function(){
		    
		    // define description
		    var description = $('.tabs .tab:first-child .inner-tab-content'),
		        firstParagraph = description.find('p, li').first(),
		        text = firstParagraph.text();
		    description[0]._inst = this;
		    // memorize full text
		    this.full = description.html();
		    if(description.find('p:empty').length > 0) {
		    	var firstParagraph = description.find('li').first();
		    	var	text = firstParagraph.text();
		    }

		    // memorize short text if there is need for
		    if(text.length > 350){
		      text = text.substring(0, 350) + '...';
		      firstParagraph.text(text);
		      this.short = firstParagraph[0].outerHTML + '<p class="description-read-more">'+ Resources.READ_MORE +'</p>';
		      
		    }else if(firstParagraph.next().length){
		      if(firstParagraph.next('li').length>0)
		      {
		    	  var newShortData='';
		    	  description.find('li').each(function(index){
		    		  if(index<3)
		    		  {
		    			  newShortData = newShortData + $(this)[0].outerHTML;  
		    		  }else if(index>=3)
		    		  {
		    			  return false;
		    		  }
		    	  });
		    	  this.short = newShortData + '<p class="description-read-more">'+ Resources.READ_MORE +'</p>';
		      }else
		      {
		    	  this.short = firstParagraph[0].outerHTML + '<p class="description-read-more">'+ Resources.READ_MORE +'</p>';  
		      }
		      
		    }else{
		      description.html(this.full + '<div style="font-size:0; height:9px; visibility:hidden;"></div>');
		    }
		    
		    // bind events
		    var labels =  $('.tabs .tab label');
		    for(var i = labels.length; i--;){
		      labels[i]._inst = this;
		    }
		    
		    $('.tabs .tab label').on('click', function(){
		    	if ($(this).closest('.tab').hasClass('checked')) {
		    		$(this).closest('.tab').removeClass('checked');
		    		$('.tabs .tab .tab-content').height(0);
		    	}
		    	else {
		    		$(this).closest('.tab').siblings('.tab').removeClass('checked');
		    		$(this).closest('.tab').addClass('checked');
		    		$('.tabs .tab .tab-content').height(0);
		    		this._inst.call(this);
		    	}
		    });
		    
		    // set short description
		    this.call( $('.tabs .tab:first-child label')[0] );
		  },
		  call: function(labelElement){
		    
		    var inner_tab = $(labelElement).parent().find('.inner-tab-content');
		    
		    // description tab only
		    if( $('.tabs .tab label').index(labelElement) == 0 && this.short){
		      
		      //set short text
		      inner_tab.html( this.short );
		      
		      //bind read-more event
		      $('.description-read-more').on('click', function(){
		        var inner_tab = $(this).parent();
		        inner_tab.html( inner_tab[0]._inst.full );
		        inner_tab.parent().height( inner_tab.height() );
		      });
		    }

		    // set tab adaptive height
		    $(labelElement).next().height( inner_tab.height() );
		  }
};
//getcookie
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

//setcookie
/************** Cookie Handling *****************/

function setCookie(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+""+expires+"; path=/";
	document.cookie = name+"="+value+expires+"; path=/";
}
//launch window
var launchwindow = {
	updateLanguageOptions: function (elem,form,keeplocale) {
		var site = window.countries[elem.val()],arrHtml = [];
		var lang;
		if(elem.val() == "ROW"){
			form.find(".shipmentnotification").html(Resources.DEFAULT_SHIPMENT_NOTIFICATION1+"<b>"+Resources.DEFAULT_SHIPMENT_NOTIFICATION2+"</b>"+Resources.DEFAULT_SHIPMENT_NOTIFICATION3);
		}else{
			form.find(".shipmentnotification").html("");
		}
		if(site != undefined && site != null){
			/*if(form.find(".currency").length > 0){
				form.find(".currency").html(site.currency);
			}else if(form.find("input[name='currency']").length > 0){
				form.find("select[id$='_language']").val(site.currency);
			}*/
			if(form.find(".selectedCountry").length > 0){
				form.find(".selectedCountry").addClass(elem.val());
			}
			var selected = form.find("select[id$='_language']").val();
			for (lang in site.language) {
				if(site.language[lang].ID == selected  && keeplocale){
					arrHtml.push('<option value="' + site.language[lang].ID + '" selected="selected">' + site.language[lang].value + "</option>");
				}else{
					arrHtml.push('<option value="' + site.language[lang].ID + '">' + site.language[lang].value + "</option>");
				}
			}
			form.find("select[id$='_language']").html(arrHtml.join(""));
		}
		//custom selectbox
		util.countrySelectbox();
	},
	changeURL: function (form) {
		var url = Urls.changeLocale;
		var dest = form.find("select[id$='_destination']").val();
		/*if(form.find("select[id$='_destination'] option[selected=selected]").val() != undefined && form.find("select[id$='_destination'] option[selected=selected]").val() != ''){
			dest = form.find("select[id$='_destination'] option[selected=selected]").val();
		}else{
			dest = form.find("select[id$='_destination']").val();
		} */
		var lang = form.find("select[id$='_language']").val();;
		/*if(form.find("select[id$='_language'] option[selected=selected]").val() != undefined && form.find("select[id$='_language'] option[selected=selected]").val() != ''){
			lang = form.find("select[id$='_language'] option[selected=selected]").val();
		}else{
			lang = form.find("select[id$='_language']").val();
		} */
		//var currency = "";
		url = util.appendParamToURL(url, 'destination', dest);
		url = util.appendParamToURL(url, 'language', lang);
		/*if(form.find("input[name='currency']").length > 0){
			currency = form.find("select[id$='_language']").val();
		}else if (form.find(".currency").length > 0) {
			currency = form.find(".currency").html();
		}
		url = util.appendParamToURL(url, 'currency', currency);*/
		url = util.appendParamToURL(url, 'format', 'ajax');
		$.ajax({
			type: 'GET',
			dataType: 'json',
			url: url,
			success: function (response) {
				if(response.success){
					setCookie("launchwindow", form.find("select[id$='_destination']").val().toLowerCase(), 60);
					dialog.close();
					window.location.href = response.redirectURL;
				}
			}
		});
	}
};
/*$(window).bind("pageshow", function(event) {
	if (event.originalEvent.persisted) {
	location.reload();
	}
	});*/
window.onpageshow = function(event) {
    if (event.persisted) {
        window.location.reload() 
    }
};
$(window).bind("pageshow", function(event) {
    if (event.originalEvent.persisted) {
      alert("From back / forward cache.");
    }
});
window.onpageshow = function(event) {
    if (event.persisted) {
        alert("From back / forward cache.");
    }
};
util.fancyScroll();

//Session timeout handling
$(document).ready(function () {
	var cookiecreation = new Date();
	var dateObj = new Date();
	dateObj.setTime(dateObj.getTime() + (1/24*24*30*60*1000));    
	document.cookie="sessionTime="+cookiecreation+"; expires="+dateObj.toUTCString()+"; path=/";
  	//(function($this) {
	window.setTimeout(
		function(){
			var currentDate = new Date();
			// var min = currentDate.getMinutes();
			// currentDate.setMinutes(min - 1);
			// var x =currentDate;
			var nameEQ = "sessionTime=";
			var ca = document.cookie.split(';');
			var final = null;
			for(var i=0;i < ca.length;i++) {
				var c = ca[i];
				while (c.charAt(0)==' ') c = c.substring(1,c.length);
				if (c.indexOf(nameEQ) == 0) { 
					final = c.substring(nameEQ.length,c.length);
		        }
			}					   
			var finalDate = new Date(final);
			var finalmin = finalDate.getMinutes();
			finalDate.setMinutes(finalmin + 28);			   
			   
			if(Date.parse(currentDate) >= Date.parse(finalDate)){
				window.setTimeout(function(){
					window.location.reload();						
				},60000);
			}
		}, 
	1680000);
});
//ipad touch color change
$('li a').on('touchstart', function(event){
	$("this").css("-webkit-tap-highlight-color","#63b0bb");
});

