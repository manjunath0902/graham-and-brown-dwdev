'use strict';

var naPhone = /^[0-9+ ]{6,20}$/;
var regex = {
	phone: {
		gb: naPhone,
		us: /^[0-9+ -]{6,20}$/,
		ca: /^[0-9+ -]{6,20}$/,
		nl: naPhone,
		de: naPhone,
		au: naPhone,
		nz: naPhone,
		fr: naPhone,
		row: naPhone
	},
	postal: {
		gb: /^[a-zA-Z]{1,2}[0-9][0-9A-Za-z]{0,1} {0,1}[0-9][A-Za-z]{2}$/,
		im: /^[a-zA-Z]{1,2}[0-9][0-9A-Za-z]{0,1} {0,1}[0-9][A-Za-z]{2}$/,
		je: /^[a-zA-Z]{1,2}[0-9][0-9A-Za-z]{0,1} {0,1}[0-9][A-Za-z]{2}$/,
		gg: /^[a-zA-Z]{1,2}[0-9][0-9A-Za-z]{0,1} {0,1}[0-9][A-Za-z]{2}$/,
		us: /^\d{5}(-\d{4})?$/,
		ca: /^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$/,
		nl: /^[1-9][0-9]{3}\s[a-zA-Z]{2}$/,
		de: /^[0-9]{4,5}$/,
		au: /^[0-9]{4}$/,
		nz: /^[0-9]{4}$/,
		fr: /^[0-9]{5}$/
	},
	notCC: /^(?!(([0-9 -]){13,19})).*$/
};
// global form validator settings
var settings = {
	errorClass: 'error',
	errorElement: 'span',
	onkeyup: false,
	onfocusout: function (element) {
		//if($(element).attr('id') != undefined && $(element).attr('id').split("_").length > 3 && $(element).attr('id').split("_").splice(0,4).join("_") == "dwfrm_billing_billingAddress_addressFields"){
		//	return true;
		//}
		if($(element).attr("type") != undefined){
			$(element).val($(element).val().trim());
		}
		if($(element).closest(".form-row").find("span.error").length > 0){
			$(element).closest(".form-row").find("span.error").hide();
		}
		if($(element).closest(".form-row").find("div.error-message").length > 0){
			$(element).closest(".form-row").find("div.error-message").hide();
		}
		if (!this.checkable(element)) {
			this.element(element);
			if(this.element(element)){
				$(element).closest(".form-row").find("label").removeClass("error");
	        }else{
	            $(element).closest(".form-row").find("label").addClass("error");
	        }
		}
	}
};
/**
 * @function
 * @description Validates a given phone number against the countries phone regex
 * @param {String} value The phone number which will be validated
 * @param {String} el The input field
 */
var validatePhone = function (value, el) {
    var country = $(el).closest('form').find('select.country');
    var country = $(el).closest('form').find('select.country');
    if( $(el).closest('form').hasClass("click-pos-form") && (country.length === 0 || country.val().length === 0) ){
    	country = $(".click-and-collect-form").find('select.country');
    }
    var rgx;
    if(regex.phone[country.val().toLowerCase()]){
           rgx = regex.phone[country.val().toLowerCase()];
    }else if(regex.phone['row']){
           rgx = regex.phone['row'];
    }else{
           return naPhone;
    }
    var isOptional = this.optional(el);
    var isValid = rgx.test($.trim(value));

    return isOptional || isValid;
};

/**
 * @function
 * @description Validates that a credit card owner is not a Credit card number
 * @param {String} value The owner field which will be validated
 * @param {String} el The input field
 */
var validateOwner = function (value) {
	var isValid = regex.notCC.test($.trim(value));
	return isValid;
};

/**
 * Add phone validation method to jQuery validation plugin.
 * Text fields must have 'phone' css class to be validated as phone
 */
$.validator.addMethod('phone', validatePhone, Resources.INVALID_PHONE);

$.validator.addMethod('mobile', validatePhone, Resources.INVALID_MOBILE);


/**
 * Add CCOwner validation method to jQuery validation plugin.
 * Text fields must have 'owner' css class to be validated as not a credit card
 */
$.validator.addMethod('owner', validateOwner, Resources.INVALID_OWNER);

/**
 * Add gift cert amount validation method to jQuery validation plugin.
 * Text fields must have 'gift-cert-amont' css class to be validated
 */
$.validator.addMethod('gift-cert-amount', function (value, el) {
	var isOptional = this.optional(el);
	var isValid = (!isNaN(value)) && (parseFloat(value) >= 5) && (parseFloat(value) <= 5000);
	return isOptional || isValid;
}, Resources.GIFT_CERT_AMOUNT_INVALID);

/**
 * Add positive number validation method to jQuery validation plugin.
 * Text fields must have 'positivenumber' css class to be validated as positivenumber
 */
$.validator.addMethod('positivenumber', function (value) {
	if ($.trim(value).length === 0) { return true; }
	return (!isNaN(value) && Number(value) >= 0);
}, ''); // '' should be replaced with error message if needed

/*Start JIRA PREV-77 : Zip code validation is not happening with respect to the State/Country.*/ 
function validateZip(value, el) {
	var country = $(el).closest("form").find("select.country");
	
	if(el.id == "dwfrm_shiptoshop_postal"){
		country = $(".click-and-collect-form").find("select.country");
	}
	//changes made for GBSS-86
	if(el.id == "dwfrm_billing_billingAddress_addressFields_postal"){
		country = $(el).closest('.bilingForm').find("select.country");
		if(country.val() == undefined || country.val() == null){
			country = $(el).closest('Form').find("select.country");
		}
	}
	if(country.length === 0 || country.val().length === 0 || !regex.postal[country.val().toLowerCase()]) {
		return true;
	}
	var isOptional = this.optional(el);
	var isValid = regex.postal[country.val().toLowerCase()].test($.trim(value));
	
	return isOptional || isValid;
}
$.validator.addMethod("postal", validateZip, Resources.INVALID_ZIP);
/*End JIRA PREV-77*/

//validation for first name
var validateFirstName = function (value, el) {
	var rgx = /[^A-Z a-z]/;
	var isValid = !rgx.test($.trim(value));
    return isValid;
};

//This is for shipping and billing form
$.validator.addMethod("firstName", validateFirstName, Resources.INVALID_FNAME);

//This is for registration and guest checkout user form
$.validator.addMethod("firstname", validateFirstName, Resources.INVALID_FNAME);


//validation for last name
var validateLastName = function (value, el) {
	var rgx = /[^A-Z a-z]/;
	var isValid = !rgx.test($.trim(value));
    return isValid;
};

//This is for shipping and billing form
$.validator.addMethod("lastName", validateLastName, Resources.INVALID_LNAME);
//This is for registration and guest checkout user form
$.validator.addMethod("lastname", validateLastName, Resources.INVALID_LNAME);



function validateExpirationDate(value, element) {
		if(element.form['dwfrm_billing_paymentMethods_creditCard_expiration_month'].value != ''){
			$(element.form).validate().element("#dwfrm_billing_paymentMethods_creditCard_expiration_month");
			$(element.form).find(".year span.error").hide();
		}
		return true;
}
function validateExpirationMonth(value, element) {
	if(element.form['dwfrm_billing_paymentMethods_creditCard_expiration_month'].value != '' && element.form['dwfrm_billing_paymentMethods_creditCard_expiration_year'].value != ''){
		var expiry = element.form['dwfrm_billing_paymentMethods_creditCard_expiration_year'].value + (element.form['dwfrm_billing_paymentMethods_creditCard_expiration_month'].value.length == 1 ? '0' + element.form['dwfrm_billing_paymentMethods_creditCard_expiration_month'].value : element.form['dwfrm_billing_paymentMethods_creditCard_expiration_month'].value),
	    date = new Date(),
	    month = date.getMonth() + 1,
	    month = month.toString();
	    if(month.length == 1){
	    	month = '0' + month;
	    }
	    var now = '' + date.getFullYear().toString() + month;
	    
	    var currentDate = Number(now);
	    var formData = Number(expiry);
	    
	    if(formData >= currentDate){
	    	return true;
	    }else{
	    	return false;
	    }
	}else{
		return true;
	}
}
$.validator.addMethod('year', validateExpirationDate,Resources.CREDITCARDEXPIRATIONERROR);

$.validator.addMethod('month', validateExpirationMonth,Resources.CREDITCARDEXPIRATIONERROR);
function validateCvnNo(value, element) {
	if(value != "***"){
		var rgx = /^[0-9]{1,4}$/;
		var isValid = rgx.test($.trim(value));
	    return isValid;
	}else{
		return true;
	}
}
$.validator.addMethod('cvn', validateCvnNo,Resources.CREDITCARDCVNERROR);

$.extend($.validator.messages, {
	required: Resources.VALIDATE_REQUIRED,
	remote: Resources.VALIDATE_REMOTE,
	email: Resources.VALIDATE_EMAIL,
	url: Resources.VALIDATE_URL,
	date: Resources.VALIDATE_DATE,
	dateISO: Resources.VALIDATE_DATEISO,
	number: Resources.VALIDATE_NUMBER,
	digits: Resources.VALIDATE_DIGITS,
	creditcard: Resources.VALIDATE_CREDITCARD,
	equalTo: Resources.VALIDATE_EQUALTO,
	maxlength: $.validator.format(Resources.VALIDATE_MAXLENGTH),
	minlength: $.validator.format(Resources.VALIDATE_MINLENGTH),
	rangelength: $.validator.format(Resources.VALIDATE_RANGELENGTH),
	range: $.validator.format(Resources.VALIDATE_RANGE),
	max: $.validator.format(Resources.VALIDATE_MAX),
	min: $.validator.format(Resources.VALIDATE_MIN)
});

var validator = {
	regex: regex,
	settings: settings,
	init: function () {
		var self = this;
		$('form:not(.suppress)').each(function () {
			$(this).validate(errorMessage(self.settings,$(this)));
		});
	},
	initForm: function (f) {
		$(f).validate(this.settings);
	}
};

function errorMessage(settings,form){
	settings.messages={};
	settings.rules={};
	form.find("[class^='input-'].required").each(function () {
		var key = $(this).attr("id");
		settings.messages[key]={};
		settings.rules[key]={};
		if($(this).is("[error]")){
			settings.messages[key]["required"]=$(this).attr("error");
			settings.rules[key]["required"]=true;
		}
		if($(this).is("[minlength]")){
			settings.rules[key]["minlength"]=parseInt($(this).attr("minlength"));
		}
		if($(this).is("[maxlength]")){
			settings.rules[key]["maxlength"]=parseInt($(this).attr("maxlength"));
		}
		if($(this).is("[minlengtherrormsg]")){
			settings.messages[key]["minlength"]=$(this).attr("minlengtherrormsg");
		}
		if($(this).is("[maxlengtherrormsg]")){
			settings.messages[key]["maxlength"]=$(this).attr("maxlengtherrormsg");
		}
	});
	return settings;
}

module.exports = validator;
