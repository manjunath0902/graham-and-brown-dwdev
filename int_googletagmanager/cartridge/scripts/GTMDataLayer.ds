/**
* Demandware Script File
* 
*   @input Product		   : dw.catalog.Product
*   @input RecentlyViewedProducts : dw.util.Collection
*   @input pid			   : String
*   @input CurrentRequest : dw.system.Request
*   @input Basket          : dw.order.LineItemCtnr  	
*   @input Order          : dw.order.Order
*   @input CurrentUser 	   : dw.customer.Customer
*   @input CheckoutStep    : String 
*	@input PageContextType : String
*	@input CurrentSession  : dw.system.Session The current session. 	
*   @output dataLayerObj   : Object()
*   
*/
importPackage( dw.system );
importPackage( dw.order );
importPackage( dw.catalog );
importPackage( dw.util );
importPackage( dw.content );
importPackage( dw.campaign );
importScript("app_gb_core:util/GetProductPrices.ds");
function execute( pdict : PipelineDictionary ) : Number
{

	var dataLayerStr : String = "";
	var dataLayerObj = new Object();
	var visitor_login_state = (pdict.CurrentUser.authenticated) ? "Registered" : "Guest";
	var customerEmail = empty(pdict.CurrentUser.profile) ? "" : pdict.CurrentUser.profile.email;
	var page_type = (pdict.CurrentHttpParameterMap.pagecontexttype == "storefront") ? "home" : pdict.PageContextType;
	var page_name = (pdict.CurrentHttpParameterMap.pagecontexttype == "storefront") ? "Homepage" : pdict.PageContextType;
	var isGuest = session.privacy.isGuest;
	
	if(!empty(session.privacy.isGuest) && session.privacy.isGuest && pdict.CheckoutStep == 2){
		dataLayerObj.event = "VirtualPageview";
		dataLayerObj.virtualPageURL = "/" + dw.system.Site.getCurrent().ID.substring(11).toLowerCase() + "/checkout/addressandshipping";
		dataLayerObj.virtualPageTitle = dw.system.Site.getCurrent().ID.substring(11) + " Checkout - Address Details and Shipping";
	}else{
		dataLayerObj.pageType = page_type;
		dataLayerObj.pageTitle = page_name;
		dataLayerObj.pageCategory =  page_type;
		dataLayerObj.visitorLoginState = visitor_login_state;
		dataLayerObj.customerEmail = customerEmail;
		dataLayerObj.userId = pdict.CurrentUser.ID;
		dataLayerObj.sessionId = pdict.CurrentSession.sessionID;
		if( pdict.CurrentRequest != null ){
			dataLayerObj.customerCountry = pdict.CurrentRequest.geolocation.countryCode;
			dataLayerObj.customerState = pdict.CurrentRequest.geolocation.regionCode;
		}else{
			dataLayerObj.customerCountry = '';
			dataLayerObj.customerState = '';
		}
	}	
 
 	if (page_type == "search") {
 		dataLayerObj.pageType="Category";
		dataLayerObj.pageTitle = pdict.CurrentHttpParameterMap.pagecgid.stringValue;
		dataLayerObj.pageCategory = pdict.CurrentHttpParameterMap.pagecgid.stringValue;
	}
	if(page_type=="quickview")
		{
		
		var ecommerceObj : Object = new Object();
		ecommerceObj.currencyCode = dw.system.Site.getCurrent().defaultCurrency;
		dataLayerObj.event = "VirtualPageview";
		//Capturing the product data 
		var products : Array = new Array();
		var prod : Object = new Object();
		var product : dw.catalog.Product = null;  //Master product
		var variantProduct : dw.catalog.Product = null //Variation Product
		if (pdict.Product.isMaster()) {
			product = pdict.Product;
			variantProduct = pdict.Product.getVariationModel().getDefaultVariant();
		} else {
			product = !empty(pdict.Product.getVariationModel().getMaster()) ? pdict.Product.getVariationModel().getMaster() : pdict.Product;
			variantProduct = pdict.Product;
		}
		
		prod.name = product.name;
		prod.id = product.ID;
		prod.price = !empty(variantProduct)?variantProduct.getPriceModel().getPrice().value:'';
		prod.brand = !empty(variantProduct.getBrand()) ? variantProduct.getBrand() : "";
		prod.category = product.primaryCategory.ID;
		if(!empty(variantProduct.custom.ProductGroup)){
			prod.group = variantProduct.custom.ProductGroup;
		}
		products.push(prod);
		var actionField : Object = new Object();
		actionField.list = "quickview";
		actionField.products =  products;				
		var detailObj = new Object();
		detailObj.actionField = actionField;
		ecommerceObj.detail = detailObj;
		dataLayerObj.ecommerce = ecommerceObj;
		}
	//Product Detail page
	if (page_type == "product") {
		dataLayerObj.productPageType = 'PDP';
		var ecommerceObj : Object = new Object();
		ecommerceObj.currencyCode = dw.system.Site.getCurrent().defaultCurrency;
				
		//Capturing the product data 
		var products : Array = new Array();
		var prod : Object = new Object();
		var product : dw.catalog.Product = null;  //Master product
		var variantProduct : dw.catalog.Product = null //Variation Product
		if (pdict.Product.isMaster()) {
			product = pdict.Product;
			variantProduct = pdict.Product.getVariationModel().getDefaultVariant();
		} else {
			product = !empty(pdict.Product.getVariationModel().getMaster()) ? pdict.Product.getVariationModel().getMaster() : pdict.Product;
			variantProduct = pdict.Product;
		}
		dataLayerObj.pageType="product";
		dataLayerObj.pageTitle = product.name;
		prod.name = product.name;
		prod.id = product.ID;
		prod.price = !empty(getPrices(variantProduct).salesPrice) ? getPrices(variantProduct).salesPrice.value.toFixed('2') : variantProduct.priceModel.getMinPrice().value.toFixed('2');
		prod.brand = !empty(variantProduct.getBrand()) ? variantProduct.getBrand() : "";
		prod.category = !empty(product.primaryCategory) ? product.primaryCategory.ID : "";
		prod.variant = !empty(product.getVariationModel().defaultVariant) ? product.getVariationModel().defaultVariant.getID():"";
		prod.quantity = 1;
		var promoname='';
		var promos = PromotionMgr.activeCustomerPromotions.getProductPromotions(pdict.Product).iterator();
		while(promos.hasNext()){
			var promotion = promos.next();
			if(promotion.getPromotionClass() == 'PRODUCT'){
				promoname += promotion.name+"|";
			}
		}
		prod.coupon = promoname.substring(0,promoname.length-1);
		var counter : Number = 6;			
		for each(var VA in variantProduct.variationModel.getProductVariationAttributes()) {
			var selectedVariationValue = variantProduct.variationModel.getSelectedValue(VA);				
			if (selectedVariationValue != null && VA.ID == "color") {
				prod.variant = selectedVariationValue.displayValue;					
			} else if (selectedVariationValue != null) {					
				prod["dimension"+counter] = selectedVariationValue.displayValue;
				counter++;
			}				
		}
		prod.metric3 = '0.00';
		var standardprice = variantProduct.priceModel.getMinPrice();
		var salesprice =  getPrices(variantProduct).salesPrice;
		if(!empty(standardprice) && !empty(salesprice)){
			prod.metric3 = standardprice.subtract(salesprice).value.toFixed('2');
		}
		if(!empty(variantProduct.custom.ProductGroup)){
			prod.group = variantProduct.custom.ProductGroup;
		}
		products.push(prod);
		dataLayerObj.pageCategory =  prod.category;
		//Capturing the product recommendation data		
		var impressions = new Array();
		var stylecount=1;
		//for each(var i=0 ; i < product.recommendations.size(); i++) {
			//var recProduct : dw.catalog.Product = product.recommendations.get(i).recommendedItem;
			//var recVariantProduct : dw.catalog.Product = null;
			//var impression : Object = new Object();
			//if (recProduct.isMaster()) {				
			//	recVariantProduct = recProduct.getVariationModel().getDefaultVariant();
			//} else {
			//	recProduct = !empty(recProduct.getVariationModel().getMaster()) ? recProduct.getVariationModel().getMaster() : recProduct;
			//	recVariantProduct = recProduct;
			//}
			//impression.name = recProduct.name;
			//impression.id = recProduct.ID;
			//impression.price = recVariantProduct.getPriceModel().getPrice().value;
			//impression.brand = !empty(recVariantProduct.getBrand()) ? recVariantProduct.getBrand() : "";
 			//impression.category = !empty(recProduct.primaryCategory)?recProduct.primaryCategory.ID:'';
			//impression.list = "PDP Recommendation Zone";
			//impression.position = x;
			//impression.variant ='';
			//impressions.push(impression);
			//x++;
		//}
		if ('getTheLook' in product.custom && product.custom.getTheLook) {
			for(var i=0; i < product.custom.getTheLookProducts.length; i++) {
				var impression : Object = new Object();
				var styleProductId = product.custom.getTheLookProducts[i];
				var styleProduct : dw.catalog.Product = dw.catalog.ProductMgr.getProduct(styleProductId);
				var styleVariantProduct : dw.catalog.Product = null;
				if (styleProduct.isMaster()) {				
					styleVariantProduct = styleProduct.getVariationModel().getDefaultVariant();
				} else {
					styleVariantProduct = styleProduct;
					styleProduct = !empty(styleProduct.getVariationModel().getMaster()) ? styleProduct.getVariationModel().getMaster() : styleProduct;
				}
				impression.name = styleProduct.name;
				impression.id = styleProduct.ID;
				impression.price = !empty(styleVariantProduct) && !empty(styleVariantProduct.getPriceModel()) ? styleVariantProduct.getPriceModel().getPrice().value.toFixed('2'):"";
				impression.brand = !empty(styleVariantProduct.getBrand()) ? styleVariantProduct.getBrand() : "";
	 			impression.category = !empty(styleProduct.primaryCategory)?styleProduct.primaryCategory.ID:'';
				impression.list = "Style it With";
				impression.position = stylecount;
				impression.variant ='';
				impressions.push(impression);
				stylecount++;
			}
		}
		var recentlycount = 1;
		if(!empty(pdict.RecentlyViewedProducts)) {
			for(var i=0; i < pdict.RecentlyViewedProducts.length; i++) {
				var recentVariantProduct;
				var recentProduct = pdict.RecentlyViewedProducts[i];
				if(recentProduct.isMaster()){
					recentVariantProduct = recentProduct.getVariationModel().getDefaultVariant();
				}else{
					recentVariantProduct = recentProduct;
					recentProduct = !empty(recentProduct.getVariationModel().getMaster()) ? recentProduct.getVariationModel().getMaster() : recentProduct;
				}
				if(recentProduct.online){
					var impression  = new Object();
					impression.name = recentProduct.name;
					impression.id = recentProduct.ID;
					impression.price = !empty(recentVariantProduct) && !empty(recentVariantProduct.getPriceModel()) ? recentVariantProduct.getPriceModel().getPrice().value.toFixed('2'):"";
					impression.brand = !empty(recentVariantProduct.getBrand()) ? recentVariantProduct.getBrand() : "";
		 			impression.category = !empty(recentProduct.primaryCategory)?recentProduct.primaryCategory.ID:'';
					impression.list = "Recently Viewed";
					impression.variant ='';
					impression.position = recentlycount;
					impressions.push(impression);
					recentlycount++;
				}
			}
		}
		//if(!empty(product.getOrderableRecommendations(1))){
		//	for(var i=0; i < product.getOrderableRecommendations(1).length; i++) {
		//		var impression : Object = new Object();
		//		var mayLikeVariantProduct;
		//		var mayLikeProduct = product.getOrderableRecommendations(1)[i].getRecommendedItem();
		//		if(mayLikeProduct.isMaster()){
		//			mayLikeVariantProduct = mayLikeProduct.getVariationModel().getDefaultVariant();
		//		}else{
		//			mayLikeVariantProduct = mayLikeProduct;
		//			mayLikeProduct = !empty(mayLikeProduct.getVariationModel().getMaster()) ? mayLikeProduct.getVariationModel().getMaster() : mayLikeProduct;
		//		}
		//		impression.name = mayLikeProduct.name;
		//		impression.id = mayLikeProduct.ID;
		//		impression.price = !empty(mayLikeVariantProduct.getPriceModel())? mayLikeVariantProduct.getPriceModel().getPrice().value.toFixed('2'):"";
		//		impression.brand = !empty(mayLikeVariantProduct.getBrand()) ? mayLikeVariantProduct.getBrand() : "";
	 	//		impression.category = !empty(mayLikeProduct.primaryCategory)?mayLikeProduct.primaryCategory.ID:'';
		//		impression.list = "You May Also Like";
		//		impression.variant ='';
		//		impression.position = x;
		//		impressions.push(impression);
		//		x++;
		//	}	
		//}
		var actionField : Object = new Object();
		var detailObj = new Object();
		detailObj.actionField = actionField;
		detailObj.products = products;
		ecommerceObj.detail = detailObj;
		ecommerceObj.impressions = impressions;
		dataLayerObj.ecommerce = ecommerceObj;
	}
	
	//Cart page
	if (page_type == "Cart" && pdict.Basket != null) {
		var ecommerceObj : Object = new Object();
		var checkout : Object = new Object();
		var actionField : Object = new Object();
		dataLayerObj.pageCategory = "checkout";
		dataLayerObj.pageType = "Bag";
		dataLayerObj.pageTitle = "shopping bag";
		dataLayerObj.event = "viewBag";
	
		var products : Array = new Array();
		for each  (var productLineItem in pdict.Basket.getProductLineItems()) {
			var prod : Object = new Object();			
			var product : dw.catalog.Product = null;  //Master product
			var variantProduct : dw.catalog.Product = null //Variation Product
			if (productLineItem.product.isMaster()) {
				product = productLineItem.product;
				variantProduct = productLineItem.product.getVariationModel().getDefaultVariant();
			} else {
				product = !empty(productLineItem.product.getVariationModel().getMaster()) ? productLineItem.product.getVariationModel().getMaster() : productLineItem.product;
				variantProduct = productLineItem.product;
			}
			prod.name = product.name;
			prod.id = product.ID;
			prod.price = !empty(getPrices(variantProduct).salesPrice) ? getPrices(variantProduct).salesPrice.value.toFixed('2') : variantProduct.priceModel.getMinPrice().value.toFixed('2');
			prod.brand = !empty(variantProduct.getBrand()) ? variantProduct.getBrand() : "";
 			prod.category = !empty(product.getPrimaryCategory())?product.getPrimaryCategory().ID:"";
 			prod.variant = !empty(product.getVariationModel().defaultVariant) ? product.getVariationModel().defaultVariant.getID():"";
			prod.quantity = productLineItem.quantity.value;  
			var promoname = '';
			var priceiter = productLineItem.getPriceAdjustments().iterator();
			while(priceiter.hasNext()){
				var promotion = priceiter.next().getPromotion();
				if(promotion.getPromotionClass() == 'PRODUCT'){
					promoname += promotion.name +"|";
				}
			}
			prod.coupon = promoname.substring(0,promoname.length-1);
			var totalprice = productLineItem.getPrice();
			var adjprice = !empty(productLineItem.getAdjustedPrice(false)) ? productLineItem.getAdjustedPrice(false):"";
			prod.metric3 = !empty(totalprice.subtract(adjprice).value) ? totalprice.subtract(adjprice).value.toFixed('2') : "";
			if(!empty(variantProduct.custom.ProductGroup)){
				prod.group = variantProduct.custom.ProductGroup;
			}
			var counter : Number = 6;			
			
			for each(var VA in variantProduct.variationModel.getProductVariationAttributes()) {
				var selectedVariationValue = variantProduct.variationModel.getSelectedValue(VA);				
				if (selectedVariationValue != null && VA.ID == "color") {
					prod.variant = selectedVariationValue.displayValue;					
				} else if (selectedVariationValue != null) {					
					prod["dimension"+counter] = selectedVariationValue.displayValue;
					counter++;
				}				
			}
			products.push(prod);			
		}
		actionField.step = parseInt(pdict.CheckoutStep);
		checkout.actionField = actionField;
		checkout.products = products;
		ecommerceObj.checkout= checkout;		
		//dataLayerObj.ecommerce = ecommerceObj;
	
	}
	if (pdict.CurrentHttpParameterMap.checkoutEvent =="checkout") {
		dataLayerObj.pageCategory = "checkout";
		dataLayerObj.event = "checkout";
		dataLayerObj.currency= dw.system.Site.getCurrent().defaultCurrency;
		var products : Array = new Array();
		for each  (var productLineItem in pdict.Basket.getProductLineItems()) {
			var prod : Object = new Object();			
			var product : dw.catalog.Product = null;  //Master product
			var variantProduct : dw.catalog.Product = null //Variation Product
			if (productLineItem.product.isMaster()) {
				product = productLineItem.product;
				variantProduct = productLineItem.product.getVariationModel().getDefaultVariant();
			} else {
				product = !empty(productLineItem.product.getVariationModel().getMaster()) ? productLineItem.product.getVariationModel().getMaster() : productLineItem.product;
				variantProduct = productLineItem.product;
			}
			prod.name = product.name;
			prod.id = product.ID;
			prod.price = !empty(getPrices(variantProduct).salesPrice) ? getPrices(variantProduct).salesPrice.value.toFixed('2') : variantProduct.priceModel.getMinPrice().value.toFixed('2');
			prod.brand = !empty(variantProduct.getBrand()) ? variantProduct.getBrand() : "";
			prod.category = product.getPrimaryCategory().ID;
			prod.variant = !empty(product.getVariationModel().defaultVariant) ? product.getVariationModel().defaultVariant.getID():"";
			prod.quantity = productLineItem.quantity.value;  
			var promoname = '';
			var priceiter = productLineItem.getPriceAdjustments().iterator();
			while(priceiter.hasNext()){
				var promotion = priceiter.next().getPromotion();
				if(promotion.getPromotionClass() == 'PRODUCT'){
					promoname += promotion.name +"|";
				}
			}
			prod.coupon = promoname.substring(0,promoname.length-1);
			var totalprice = productLineItem.getPrice();
			var adjprice = !empty(productLineItem.getAdjustedPrice(false)) ? productLineItem.getAdjustedPrice(false):"";
			prod.metric3 = !empty(totalprice.subtract(adjprice).value) ? totalprice.subtract(adjprice).value.toFixed('2') : "";
			if(!empty(variantProduct.custom.ProductGroup)){
				prod.group = variantProduct.custom.ProductGroup;
			}
			var counter : Number = 6;			
			
			for each(var VA in variantProduct.variationModel.getProductVariationAttributes()) {
				var selectedVariationValue = variantProduct.variationModel.getSelectedValue(VA);				
				if (selectedVariationValue != null && VA.ID == "color") {
					prod.variant = selectedVariationValue.displayValue;					
				} else if (selectedVariationValue != null) {					
					prod["dimension"+counter] = selectedVariationValue.displayValue;
					counter++;
				}				
			}
			products.push(prod);			
		}
		dataLayerObj.Products = products;
	}
	if(pdict.CurrentHttpParameterMap.cartEvent =="addtocart" || pdict.CurrentHttpParameterMap.cartEvent =="removeFromCart"){
		if(pdict.CurrentHttpParameterMap.cartEvent =="removeFromCart"){
			dataLayerObj.event = "removeFromCart";
		}else{
			dataLayerObj.event = "addToCart";
		}
		
		var ecommerceObj : Object = new Object();
		ecommerceObj.currencyCode = dw.system.Site.getCurrent().defaultCurrency;		
		//Capturing the product data 
		var products : Array = new Array();
		var prod : Object = new Object();
		var product : dw.catalog.Product = null;  //Master product
		var variantProduct : dw.catalog.Product = null; //Variation Product
 		if (!empty(pdict.Product) && pdict.Product.isMaster()) {
			product = pdict.Product;
			variantProduct = pdict.Product.getVariationModel().getDefaultVariant();
		} else {
			product = !empty(pdict.Product.getVariationModel().getMaster()) ? pdict.Product.getVariationModel().getMaster() : pdict.Product;
			variantProduct = pdict.Product;
		}		
		for each(var VA in variantProduct.variationModel.getProductVariationAttributes()) {
			var selectedVariationValue = variantProduct.variationModel.getSelectedValue(VA);				
			if (selectedVariationValue != null && VA.ID == "color") {
				prod.variant = selectedVariationValue.displayValue;					
			} else if (selectedVariationValue != null && (VA.ID == "size" || VA.ID == "finish")) {					
				prod.dimension6 = selectedVariationValue.displayValue;
			}else if(selectedVariationValue != null && VA.ID == "brand"){
				prod.dimension7 = selectedVariationValue.displayValue;
			}				
		}
		
		prod.name = product.name;
		prod.id = product.ID;
		prod.price = !empty(getPrices(variantProduct).salesPrice) ? getPrices(variantProduct).salesPrice.value.toFixed('2') : variantProduct.priceModel.getMinPrice().value.toFixed('2');
		prod.brand = !empty(variantProduct.getBrand()) ? variantProduct.getBrand() : "";
		prod.category = !empty(product.primaryCategory)?product.primaryCategory.ID:"";
		prod.quantity =pdict.CurrentHttpParameterMap.quantity.value;
		prod.variant = !empty(product.getVariationModel().defaultVariant) ? product.getVariationModel().defaultVariant.getID() : "";
		if(!empty(variantProduct.custom.ProductGroup)){
			prod.group = variantProduct.custom.ProductGroup;
		}
		
		var promoname='';
		var promos = PromotionMgr.activeCustomerPromotions.getProductPromotions(variantProduct).iterator();
		while(promos.hasNext()){
			var promotion = promos.next();
			if(promotion.getPromotionClass() == 'PRODUCT'){
				promoname += promotion.name+"|";
			}
		}
		prod.coupon = promoname.substring(0,promoname.length-1);
		prod.metric3 = '0.00';
		var standardprice = variantProduct.priceModel.getMinPrice();
		var salesprice =  getPrices(variantProduct).salesPrice;
		if(!empty(standardprice) && !empty(salesprice)){
			prod.metric3 = standardprice.subtract(salesprice).value.toFixed('2');
		}
		
		products.push(prod);
		var add = new Object();	
		add.products=products;
		ecommerceObj.add =add;
		dataLayerObj.ecommerce = ecommerceObj;

	}
	//checkoutstep : 2 ==> Intermediate Login page, checkoutstep : 3 ==> Shipping page, checkoutstep : 4 ==> Billing page
	//As per the request for GTM tickets checkoutsteps are changed checkoutstep : 1 ==> Intermediate Login page, checkoutstep : 2 ==> Shipping page, checkoutstep : 3 ==> Billing page
	if (!empty(pdict.CheckoutStep) && (pdict.CheckoutStep == 1 || pdict.CheckoutStep == 2 || pdict.CheckoutStep == 3 || pdict.CheckoutStep == 5) && pdict.Basket != null) {
		if(!(!empty(session.privacy.isGuest) && session.privacy.isGuest && pdict.CheckoutStep == 2) ){
		dataLayerObj.pageType = "checkout";
		dataLayerObj.pageCategory = "checkout";
		}
		if (pdict.CheckoutStep == 1) {
			dataLayerObj.pageTitle = "checkout options";
		} else if (pdict.CheckoutStep == 2) {
			if(!(!empty(session.privacy.isGuest) && session.privacy.isGuest && pdict.CheckoutStep == 2) ){
				dataLayerObj.pageTitle = "shipping address";
 			}
		} else if (pdict.CheckoutStep == 3) {
			dataLayerObj.pageTitle = "payment and billing";
			dataLayerObj.event = "VirtualPageview";
			dataLayerObj.virtualPageURL = "/" + dw.system.Site.getCurrent().ID.substring(11).toLowerCase() + "/checkout/payment";
			dataLayerObj.virtualPageTitle = dw.system.Site.getCurrent().ID.substring(11) + " Checkout - Secure Payment";
		} else if (pdict.CheckoutStep == 5) {
				dataLayerObj.pageTitle = "order review";
				dataLayerObj.email = pdict.Basket.customerEmail;
		}
		
		var products : Array = new Array();
		for each  (productLineItem in pdict.Basket.getProductLineItems()) {
			var prod : Object = new Object();			
			var product : dw.catalog.Product = null;  //Master product
			var variantProduct : dw.catalog.Product = null //Variation Product
			if (productLineItem.product.isMaster()) {
				product = productLineItem.product;
				variantProduct = productLineItem.product.getVariationModel().getDefaultVariant();
			} else {
				product = !empty(productLineItem.product.getVariationModel().getMaster()) ? productLineItem.product.getVariationModel().getMaster() : productLineItem.product;
				variantProduct = productLineItem.product;
			}
			prod.name = product.name;
			prod.id = product.ID;
			prod.price = !empty(getPrices(variantProduct).salesPrice) ? getPrices(variantProduct).salesPrice.value.toFixed('2') : variantProduct.priceModel.getMinPrice().value.toFixed('2');
			prod.brand = !empty(variantProduct.getBrand()) ? variantProduct.getBrand() : "";
			prod.category = !empty(product.getPrimaryCategory())?product.getPrimaryCategory().ID:"";
			prod.variant = !empty(product.getVariationModel().defaultVariant) ? product.getVariationModel().defaultVariant.getID():"";
			prod.quantity = productLineItem.quantity.value;
			var promoname = '';
			var priceiter = productLineItem.getPriceAdjustments().iterator();
			while(priceiter.hasNext()){
				var promotion = priceiter.next().getPromotion();
				if(promotion.getPromotionClass() == 'PRODUCT'){
					promoname += promotion.name +"|";
				}
			}
			prod.coupon = promoname.substring(0,promoname.length-1);
			var totalprice = productLineItem.getPrice();
			var adjprice = !empty(productLineItem.getAdjustedPrice(false)) ? productLineItem.getAdjustedPrice(false):"";
			prod.metric3 = !empty(totalprice.subtract(adjprice).value) ? totalprice.subtract(adjprice).value.toFixed('2') : "";
			if(!empty(variantProduct.custom.ProductGroup)){
				prod.group = variantProduct.custom.ProductGroup;
			}  
			var counter : Number = 6;			
			
			for each(var VA in variantProduct.variationModel.getProductVariationAttributes()) {
				var selectedVariationValue = variantProduct.variationModel.getSelectedValue(VA);				
				if (selectedVariationValue != null && VA.ID == "color") {
					prod.variant = selectedVariationValue.displayValue;					
				} else if (selectedVariationValue != null) {					
					prod["dimension"+counter] = selectedVariationValue.displayValue;
					counter++;
				}
			}
			products.push(prod);			
		}
		
		var checkout : Object = new Object();
		var actionField : Object = new Object();
		actionField.step = parseInt(pdict.CheckoutStep);
		actionField.action = "checkout";
		checkout.actionField = actionField;
		checkout.products = products;
		
		
		var ecommerceObj : Object = new Object();
		ecommerceObj.currencyCode = pdict.Basket.getCurrencyCode();
		ecommerceObj.checkout= checkout;		
		dataLayerObj.ecommerce = ecommerceObj;
	}
	
	//checkoutstep : 6 ==> order confirmation page
	var order = pdict.Order;
	if (pdict.CurrentHttpParameterMap.pagecontexttype == 'orderconfirmation' && !empty(order) && empty(order.custom.gtmEventTriggered)) {
		dataLayerObj.pageType = "checkout";
		dataLayerObj.pageCategory = "checkout";
		dataLayerObj.pageTitle = "order confirmation";
		dataLayerObj.email = pdict.Basket.customerEmail;
		dataLayerObj.event = "transaction";
		
		
		var products : Array = new Array();
		for each  (productLineItem in pdict.Basket.getProductLineItems()) {
			var prod : Object = new Object();			
			var product : dw.catalog.Product = null;  //Master product
			var variantProduct : dw.catalog.Product = null //Variation Product
			if (productLineItem.product.isMaster()) {
				product = productLineItem.product;
				variantProduct = productLineItem.product.getVariationModel().getDefaultVariant();
			} else {
				product = !empty(productLineItem.product.getVariationModel().getMaster()) ? productLineItem.product.getVariationModel().getMaster() : productLineItem.product;
				variantProduct = productLineItem.product;
			}
			prod.name = product.name;
			prod.id = product.ID;
			prod.price = !empty(getPrices(variantProduct).salesPrice) ? getPrices(variantProduct).salesPrice.value.toFixed('2') : variantProduct.priceModel.getMinPrice().value.toFixed('2');
			prod.brand = !empty(variantProduct.getBrand()) ? variantProduct.getBrand() : "";
 			prod.category = !empty(product.getPrimaryCategory())? product.getPrimaryCategory().ID:"";
 			prod.variant = !empty(product.getVariationModel().defaultVariant)? product.getVariationModel().defaultVariant.getID():"";
			prod.quantity = productLineItem.quantity.value; 
			var promoname = '';
			var priceiter = productLineItem.getPriceAdjustments().iterator();
			while(priceiter.hasNext()){
				var promotion = priceiter.next().getPromotion();
				if(promotion.getPromotionClass() == 'PRODUCT'){
					promoname += promotion.name +"|";
				}
			}
			prod.coupon = promoname.substring(0,promoname.length-1);
			var totalprice = productLineItem.getPrice();
			var adjprice = !empty(productLineItem.getAdjustedPrice(false)) ? productLineItem.getAdjustedPrice(false):"";
			prod.metric3 = !empty(totalprice.subtract(adjprice).value) ? totalprice.subtract(adjprice).value.toFixed('2') : "";
			if(!empty(variantProduct.custom.ProductGroup)){
				prod.group = variantProduct.custom.ProductGroup;
			}
			var counter : Number = 6;			
			
			for each(var VA in variantProduct.variationModel.getProductVariationAttributes()) {
				var selectedVariationValue = variantProduct.variationModel.getSelectedValue(VA);				
				if (selectedVariationValue != null && VA.ID == "color") {
					prod.variant = selectedVariationValue.displayValue;					
				} else if (selectedVariationValue != null) {					
					prod["dimension"+counter] = selectedVariationValue.displayValue;
					counter++;
				}				
			}
			products.push(prod);			
		}
		
		var purchase : Object = new Object();
		var actionField : Object = new Object();		
		actionField.id = pdict.Basket.orderNo;
		actionField.affiliation = '';
		var orderRevenue = pdict.Basket.getAdjustedMerchandizeTotalPrice();
		actionField.revenue = orderRevenue.value.toFixed('2');
		actionField.tax = pdict.Basket.totalTax.value.toFixed('2');
		actionField.shipping = pdict.Basket.shippingTotalGrossPrice.value.toFixed('2');
		if(!empty(pdict.Basket.getCouponLineItems())){
			actionField.awincg = 'VOUCHER';
		}else{
			actionField.awincg = 'DEFAULT';
		}
		var merchTotalExclOrderDiscounts : dw.value.Money = pdict.Basket.getAdjustedMerchandizeTotalPrice(false);
		var merchTotalInclOrderDiscounts : dw.value.Money = pdict.Basket.getAdjustedMerchandizeTotalPrice(true);
		var orderDiscount : dw.value.Money = merchTotalExclOrderDiscounts.subtract( merchTotalInclOrderDiscounts );
		actionField.metric2 = orderDiscount.value.toFixed('2');
		//GB-529 - Adding coupons - Start
		//var coupons : Array = new Array();
		//var coupon : Object = new Object();
		//if(!empty(pdict.Basket.getCouponLineItems())){
		//	for each(var couponApplied : CouponLineItem in pdict.Basket.getCouponLineItems()){
 		//		coupon.ID = couponApplied.couponCode;
		//		coupons.push(coupon);
		//	}
		//}
		//GB-529 - Adding coupons - End
		
		//Start GBSS-53
		var couponcodes="";
		if(!empty(pdict.Basket.getCouponLineItems())){
			for each(var couponApplied : CouponLineItem in pdict.Basket.getCouponLineItems()){
				couponcodes += couponApplied.couponCode +"|";
			}
		}
		actionField.couponcode = couponcodes.substring(0,couponcodes.length-1);
		//End GBSS-53
		var promonames ="";
		var basketpriceadj = pdict.Basket.getPriceAdjustments().iterator();
		while(basketpriceadj.hasNext()){
			var promotion = basketpriceadj.next().getPromotion();
			if(promotion.getPromotionClass() == 'ORDER'){
				promonames += promotion.name +"|";
			}
		}
		promonames = promonames.substring(0,promonames.length-1);
		actionField.coupon = promonames;
		purchase.actionField = actionField;		
		purchase.products = products;
		//purchase.coupons = coupon;
		var ecommerceObj : Object = new Object();
		ecommerceObj.currencyCode = pdict.Basket.getCurrencyCode();
		ecommerceObj.purchase= purchase;		
		dataLayerObj.ecommerce = ecommerceObj;
		order.custom.gtmEventTriggered = true;
	}
	pdict.dataLayerObj = dataLayerObj;
	
   return PIPELET_NEXT;
}
