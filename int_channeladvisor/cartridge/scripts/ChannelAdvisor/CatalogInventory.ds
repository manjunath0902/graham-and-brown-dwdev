/**

*

*	Demandware ChannelAdvisor Catalog Extract File v15.1.0

*

*	Based on SiteGenesis version 2.10.0.2

*	Created By: Precision Design Studios

*	Last Modified On: 1/15/2015 

*	Last Modified By: Mike Wolff, ChannelAdvisor	

*

*/

importPackage( dw.system );

importPackage( dw.util );

importPackage( dw.catalog );

importPackage( dw.customer );

importPackage( dw.io );

importPackage( dw.net );

importPackage( dw.object );

importPackage( dw.campaign );

importPackage( dw.web );


/**
 * Iterates through all site products, creates an XML representation of the price and quantity data, and FTP's that data based on the settings in the custom preferences.
 *
 * @param PipelineDictionary pdict
 * @return Number  The next Pipelet to fire
 */
function execute( pdict : PipelineDictionary ) : Number {
	
	var caStringWriter : StringWriter = new StringWriter();
	
	var productCounter : Number = 0;
	
	var writeCounter : Number = 0;
	
	(new dw.io.File(dw.io.File.IMPEX +'/src/channeladvisor/')).mkdirs();
	
	caStringWriter.write('<?xml version="1.0" encoding="iso-8859-1"?>\n');
	
	caStringWriter.write('<Offers xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://ssc.channeladvisor.com/files/cageneric.xsd">\n');
	
	caStringWriter.flush();
	
	if (!writeToFile(caStringWriter.toString(), false))	{
		
		return PIPELET_ERROR;
		
	}
	
	caStringWriter.close();
	
	var prods : SeekableIterator = ProductMgr.queryAllSiteProducts();
	
	var counter : Number = 0;
	
	
	while(prods.hasNext())
	{
		var product : Product = prods.next();
		
		var isMaster : Boolean = product.isMaster();
		
		var isVariant : Boolean = product.isVariant();
		
		if ( product.isOnline() && product.isSearchable() && !product.isProductSet() && ( isMaster || (!isMaster && !isVariant) ) ) {
			
			if (isMaster) {

				for(var x : Number = 0; x < product.variationModel.variants.size(); x++) {
					
					var CurrentOffer : XML = buildProductNode(product.variationModel.variants[x]);
					
					productCounter++;
					
					var xmlString : String = CurrentOffer.toXMLString();
 					
 					if (!writeToFile(xmlString, true))	{
		
						return PIPELET_ERROR;
		
					}
 					
				}	
				
			} else if (!isVariant) {

				var CurrentOffer : XML = buildProductNode(product);
				
	 			productCounter++;
	 			
				var xmlString : String = CurrentOffer.toXMLString();
 				
 				if (!writeToFile(xmlString, true))	{
		
					return PIPELET_ERROR;
		
				}
 				
			}
				
		} else if (!isVariant){
			//we don't send product sets or products that aren't online or searchable
		}
		
	}
	
	prods.close(); //close the seekable iterator
	
	Logger.info("Total products in CA Inventory feed: "+ counter);
	
	if (!writeToFile('</Offers>\n', true))	{
		
		return PIPELET_ERROR;
		
	}
	
	var ftp : FTPClient = new FTPClient(); 

	ftp.setTimeout(60*1000*30); 
	
	if(ftp.connect(dw.system.Site.getCurrent().preferences.custom.CA_FTP_Hostname , dw.system.Site.getCurrent().preferences.custom.CA_FTP_Username, dw.system.Site.getCurrent().preferences.custom.CA_FTP_Password)) {
		
		Logger.info('Connected to Channel Advisor FTP');
		
		//XML files have to be uploaded into the (Inventory->Transform subfolder to be processed) Filedump folder.
		ftp.cd('Filedump');
		
		//Override the base settings in CA such that only updates are processed using this file, no new inventory is created
		var targetFileName : String = 'UpdateOnly_Inventory.xml';

		var file : File = new dw.io.File(dw.io.File.IMPEX + '/src/channeladvisor/UpdateOnly_Inventory.xml');
	
		if (!file.exists()) {
			
			return PIPELET_ERROR;
			
		} else {
			
			ftp.putBinary(targetFileName, file);
			
		}
		
		ftp.disconnect();
		
	}
	
    return PIPELET_NEXT;
    
}


/**
 * Given a product,  extract the data and create the XML representation of that product.
 *
 * @param Product product  The content to be written to the file
 * @return XML the full product XML representation
 */
function buildProductNode(product : Product) : XML
{
	var Offer : XML = <Offer></Offer>;
	
	Offer.Model = !empty(product.ID) ? product.ID : '' ; //Model is a required field, so this should never be empty

	var pb : String = ''; 

	if(('listPriceDefault' in dw.system.Site.current.preferences.custom) && !empty(dw.system.Site.current.preferences.custom.listPriceDefault)){
		
		pb = dw.system.Site.current.preferences.custom.listPriceDefault;
		
	} else	{
		
	    pb = 'list-prices';
	    
	}
	
	Offer.RegularPrice = !empty(product.priceModel.getPriceBookPrice(pb).toNumberString()) && product.priceModel.getPriceBookPrice(pb).toNumberString() != 'N/A' ? product.priceModel.getPriceBookPrice(pb).toNumberString() : ''; 
	
	Offer.CurrentPrice = !empty(product.getPriceModel().getPrice().toNumberString()) && product.getPriceModel().getPrice().toNumberString() != 'N/A' ? product.getPriceModel().getPrice().toNumberString() : '';
	
	/*
	 *   START - New code to add promotional pricing to the feed. 
	 */

	var promos : Collection = dw.campaign.PromotionMgr.getActivePromotions().getProductPromotions(product);
	
	var myPromoPrice = '';

	for(var a in promos){
		
		var promo : Promotion = promos[a];
		
		var promoIsForEveryone = false;
		
		if(promo.isBasedOnCustomerGroups()){
			
			var customerGroups = promo.getCustomerGroups();
			
			for(var b in customerGroups){
				
				if(customerGroups[b].ID == 'Unregistered' || customerGroups[b].ID == 'Everyone'){
					
						promoIsForEveryone = true;
						
				}	
				
			}
			
		} else {
			
			promoIsForEveryone = true;
			
		}		
		
		if(promo.getPromotionClass() != null && promo.getPromotionClass().equals(dw.campaign.Promotion.PROMOTION_CLASS_PRODUCT) && promoIsForEveryone && !promo.basedOnCoupons && !promo.basedOnSourceCodes){
			
			var tempPrice = 0;
			
			if(product.optionProduct){
			
				tempPrice = promo.getPromotionalPrice(product, product.getOptionModel());
			
			} else {
			
				tempPrice = promo.getPromotionalPrice(product);
		
			}
		
			if(tempPrice > 0){
		
				myPromoPrice = tempPrice;
		
			}
		
		}

	}

	if(myPromoPrice !== ''){
		
		myPromoPrice = myPromoPrice.toNumberString();	
		
	}

	Offer.PromoPrice = myPromoPrice;

	/*
	 *   END - New code to add promotional pricing to the feed. 
	 */
	
	Offer.Quantity = product.availabilityModel.inventoryRecord != null ? StringUtils.stringToXml(product.availabilityModel.inventoryRecord.ATS.value) : StringUtils.stringToXml(0);
	
	return Offer;

}


/**
 * Writes the content to an xml file (Inventory.xml) in the Impex /src/channeladvisor/ folder.
 *
 * @param String content  The content to be written to the file
 * @param Boolean isAppend  Should the string be appended, or replace existing content
 * @return Boolean returns true if the write succeeded, false if it failed
 */
function writeToFile(content : String, isAppend : Boolean) : Boolean {
	
	try	{		
		
		var file : File = new dw.io.File(dw.io.File.IMPEX + '/src/channeladvisor/UpdateOnly_Inventory.xml');
		
		if(!file.exists()) {
			
			if(!file.createNewFile()) {
				
				Logger.error("File "+file.name+" could not be created!");
				
				return false;
				
			}
			
		} 
		
		var out : FileWriter = new FileWriter(file, isAppend);
		
		out.write(content);
		
		out.flush();
		
		out.close();
		
	} catch(e) {
		
		Logger.error("An error occured while exporting channel advisor XML Inventory {0}.", e);
		
		return false;
		
	}	
	
	return true;
	
}