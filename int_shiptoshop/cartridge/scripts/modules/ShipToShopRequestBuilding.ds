/**
* Demandware Script File
* 
* Building request for pickupoptions API call
*
* For example:
*
*   @input Basket : dw.order.Basket
*   @input CurrentHttpParameterMap : dw.web.HttpParameterMap
*   @output responsedata : Object
*
*/
var System = require("dw/system");
var Util = require("dw/util");
var svc =  require("dw/svc"); 
//var shipToShopServiceInstance = require('int_shiptoshop/cartridge/scripts/ShipToShopServicesInit.ds');

function execute( args : PipelineDictionary ) : Number
{
	try{
		
		var params = new Object();
		var CurrentHttpParameterMap = args.CurrentHttpParameterMap;
		var basket = args.Basket;
		var reqobj = new Object();
		var siteID = System.Site.getCurrent().getID(); 
		var pickupdistance = System.Site.getCurrent().getCustomPreferenceValue("distanceForPickupOptions"),
			unitfordistance = System.Site.getCurrent().getCustomPreferenceValue("unitForDistance"),
			maxresults = System.Site.getCurrent().getCustomPreferenceValue("maxResultsToFetch"),
			pickupendpoint = System.Site.getCurrent().getCustomPreferenceValue("pickupOptionsEndPointUrl");
		
		var distance = new Object();
		distance.Unit = unitfordistance;
		distance.Value = new Util.Decimal(pickupdistance).valueOf();
		reqobj.Distance =  distance;

		reqobj.MaxResults = new Util.BigInteger(maxresults).valueOf();
		
		var packagesarray = new Array();
		
		for each(var pli in basket.getAllProductLineItems()){
			
			var productweight, productwidth, productlength, productheight;
			var product = pli.getProduct();
			productweight = new Util.Decimal(product.custom.weight).valueOf();
			//if(productweight == 0 || productweight == ''){
			//	throw Error("Productweight is less than zero or null");
			//}
			productwidth = new Util.Decimal(product.custom.width).valueOf();
			productlength = new Util.Decimal(product.custom.length).valueOf();
			productheight = new Util.Decimal(product.custom.height).valueOf();
			
			var weight = new Object();
			weight.Value = productweight;
			weight.Unit = "Kg";
			
			var dimensions = new Object();
			dimensions.Unit = "Cm";
			dimensions.Width = productwidth;
			dimensions.Length = productlength;
			dimensions.Height = productheight;
			
			var value = new Object();
			var amount =  new Util.Decimal(pli.getAdjustedPrice()).valueOf();
			value.Amount = amount;
			var currency = new Object();
			currency.IsoCode = basket.getCurrencyCode();
			value.Currency = currency;
			
			
			var items = new Array();
			
			var itemDetails = new Object();
				itemDetails.Sku = pli.productID;
				itemDetails.Model = "Item-"+pli.productID;
				itemDetails.Description = pli.lineItemText;
				
				var IsoCode  = new Object();
				switch(siteID) {
				    case "GrahamBrownUK":
				       IsoCode.TwoLetterCode = "GB";
				       break;
				    case "GrahamBrownFR":
				       IsoCode.TwoLetterCode = "FR";
				       break;
				   	case "GrahamBrownNL":
				       IsoCode.TwoLetterCode = "NL";
				       break;
				    case "GrahamBrownDE":
				       IsoCode.TwoLetterCode = "DE";
				       break;
				}
				
				var countryOfOrigin = new Object();
				countryOfOrigin.IsoCode = IsoCode;
				
				
				itemDetails.CountryOfOrigin  = countryOfOrigin;
				itemDetails.HarmonisationCode = "ITEMREH-"+pli.productID;
				itemDetails.Weight = weight;
				itemDetails.Dimensions = dimensions;
				itemDetails.Value = value;
				itemDetails.ItemReferenceProvidedByCustomer = "ITEMREF-"+pli.productID;
				
				var mdata = new Array();
				var mObj = new Object();
					mObj.KeyValue ="Picker";
					mObj.StringValue = CurrentHttpParameterMap.firstname.stringValue + CurrentHttpParameterMap.lastname.stringValue; 
					mdata.push(mObj);
					
				itemDetails.MetaData = mdata;
				itemDetails.Quantity = pli.quantityValue;
				itemDetails.PackageReferenceProvidedByCustomer = "MYPACK-"+pli.productID;
				items.push(itemDetails);
			
			var date = new Date();
			var time = date.getFullYear() + date.getMonth()+date.getUTCHours()+date.getUTCMinutes()+date.getUTCSeconds()+date.getUTCMilliseconds();
			
			var matadata = new Array();
			var matadataObj = new Object();
				matadataObj.KeyValue = "WMS-REF";
				matadataObj.IntValue = pli.productID.toString().replace('-','')+time;
			matadata.push(matadataObj);

			
			var packages = new Object();
			packages.Items = items;
			packages.Weight = weight;
			packages.Dimensions = dimensions;
			packages.Description = product.getName();
			packages.Value = value;
			packages.MetaData = matadata;
			packages.PackageSizeReference = time+"-"+CurrentHttpParameterMap.firstname.stringValue.toUpperCase()+CurrentHttpParameterMap.lastname.stringValue.toUpperCase()
			
			packagesarray.push(packages);
		}
		
		reqobj.Packages = packagesarray;
		
		var addressarray = new Array();
		var originadd = new Object();
		originadd.AddressType = "Origin";
		originadd.ShippingLocationReference = System.Site.getCurrent().getCustomPreferenceValue("shippingLocationReference");
		addressarray.push(originadd);
		
		var destinationadd = new Object();
		destinationadd.AddressType = "Destination";
		var contact = new Object();
		contact.FirstName = CurrentHttpParameterMap.firstname.value;
		contact.LastName = CurrentHttpParameterMap.lastname.value;
		contact.Mobile = CurrentHttpParameterMap.mobile.value;
		contact.Telephone = CurrentHttpParameterMap.mobile.value;
		contact.Email = CurrentHttpParameterMap.email.value;
		destinationadd.Contact = contact;
		destinationadd.AddressLine1 = CurrentHttpParameterMap.address.value;
		destinationadd.Region = CurrentHttpParameterMap.region.value;
		destinationadd.Postcode = CurrentHttpParameterMap.postcode.value;
		
		var country = new Object();
		var countryname = new Object();
		
		switch(siteID) {
		    case "GrahamBrownUK":
		       countryname.name = "United Kingdom";
		       countryname.twolettercode = "GB";
		       break;
		    case "GrahamBrownFR":
		       countryname.name = "France";
		       countryname.twolettercode = "FR";
		       break;
		   	case "GrahamBrownNL":
		       countryname.name = "Netherlands";
		       countryname.twolettercode = "NL";
		       break;
		    case "GrahamBrownDE":
		       countryname.name = "Germany";
		       countryname.twolettercode = "DE";
		       break;
		}
		
		country.Name = countryname.name;
		var countrycode = new Object();
		countrycode.TwoLetterCode = countryname.twolettercode;
		country.IsoCode = countrycode;
		
		destinationadd.Country = country;
		
		addressarray.push(destinationadd);
		
		reqobj.Addresses = addressarray;
		
		params.reqobj = reqobj;
		
		params.url = pickupendpoint;
		
		var shipToShopService = svc.LocalServiceRegistry.createService("sortedpro.http.shiptoshop.post", { 

		     createRequest: function(svc:Service, params) {
		     	
		         	//Set HTTP Method
					svc = svc.setRequestMethod("POST");
			
					//Set content type
					svc = svc.addHeader("Content-Type", "application/json");
					
					//Set accept type
					svc = svc.addHeader("Accept", "application/json");
					
					//Set subscription-key
					svc = svc.addHeader("ocp-apim-subscription-key", dw.system.Site.getCurrent().getCustomPreferenceValue("clickAndCollectApiKey"));
					
					svc = svc.addHeader("electio-api-version", "1.1");
			
					//Set encoding
					svc = svc.setEncoding("UTF-8");
			
					//Set caching
					svc = svc.setCachingTTL(0);
					
					//Set URL
					svc = svc.setURL(params.url);
					return JSON.stringify(params.reqobj);
		     },
		     
		     parseResponse : function(svc:Service, response) {
				return response;
		     },
		     
		     filterLogMessage: function(msg:String) {
        		return msg;
    		 }
		 });
		
		
		var result : Result = shipToShopService.call(params);
		
		var httpClient : HTTPClient = result.object;
		
		if (httpClient.statusCode == 200) {
			var response = httpClient.text;
			
			args.responsedata = response.toString();  
			
			return PIPELET_NEXT;
		} else {
			Logger.error('Error in the PickupOptions API Call ' + httpClient.errorText);
			return PIPELET_ERROR;
		}	
		
	}catch(e){
		System.Logger.error("Error in ShipToShopRequestBuilding.ds :" + e.message);
	}
	
   return PIPELET_NEXT;
}
