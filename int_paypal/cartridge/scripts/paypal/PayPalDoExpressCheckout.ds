/**********************************************************************************************************************************************************************************************
*
* TITLE:  
*
*	PayPalDoExpressCheckout.ds
*
* DESCRIPTION:
*
*	Sets up a request object and sends it over to PayPal for verification. A response object is then returned and the code handles the result so the user can be directed to the proper page
*
* 	We have set billing and shipping informations and have checked all, so we can submit
* 	the sale. This script calls Paypal with DoExpressCheckoutRequest to start the payment
* 	transaction.
*
* INPUT PARAMETERS:
*
*	@input Order : dw.order.Order
* 	@input OrderNo : String The created OrderNo.
* 	@input PaymentProcessor : dw.order.PaymentProcessor The needed paymentprocessor.
*	@input ButtonSource : String Flow - PayPal payment source
* 	@input GiroRedirectURL : String, redirect url to PayPal
*
* OUPUT PARAMETERS:
*
* 	@output Error : Object The Error object
* 	@output Location : String The redirect location.
* 	@output PaypalError : String If a Paypal call fails or doesn't return Success
* 	@output WSResponse : Object
* 	@output ReturnCode : Number The returned token from Paypal
* 	@output GiroRedirect : String Giro redirect from PayPal
* 	@output GiroLocation : String The redirect location.
* 	@output BAID : String The returned token from Paypal Billing Agreement
* 
**********************************************************************************************************************************************************************************************/

importPackage( dw.system );
importPackage( dw.util );
importPackage( dw.order );
importPackage( dw.rpc );
importPackage( dw.svc );

importScript("int_paypal:cart/Credentials.ds");

function execute(pdict : PipelineDictionary) {

	try{
		
		if( pdict.Order == null )
		{
			Logger.error("order is null : script file DoExpressCheckout.ds");
			return PIPELET_ERROR;
		}
		
		var PAYPAL_PAYMENT : String = "PayPal";
		var order : Order = pdict.Order;
		var paymentProcessor = pdict.PaymentProcessor;
		var paymentInstruments : Collection = order.paymentInstruments;
		var paymentInstrument : OrderPaymentInstrument = order.getPaymentInstruments('PayPal').iterator().next();
		
		//Get webrefrence object
		var webReference : WebReference = webreferences.PayPalSvcDev;	
		if (dw.system.System.getInstanceType() == dw.system.System.PRODUCTION_SYSTEM) {
			webReference = webreferences.PayPalSvc;
		}
		
		var paypalExpress : WebReference = webReference;
		
		//Get the service object
		var svc : SOAPService = ServiceRegistry.get("paypal.soap.payment.doExpressCheckoutPayment");
		
		//Prepare Params Object
		var params : Object = {
							    "order": pdict.Order,
							    "paymentProcessor": pdict.PaymentProcessor,
							    "paymentInstruments": paymentInstruments,
							    "paymentInstrument": paymentInstrument,
							    "buttonSource" : pdict.ButtonSource
							};
							
		//Execute the service
		var result : Result = svc.call(params);
		
		//Check for service availability. If service is not available go to cart page and display error message.
		if(result.status == Result.SERVICE_UNAVAILABLE){
			throw new Error(dw.web.Resource.msg('confirm.error.technical', 'checkout', null))
		}
		
		var response  : Object = result.object;
	
		Logger.debug("after response call");
 		pdict.WSResponse = response;
		Logger.debug('Ack  -- ' + response.getAck().toString() );
		
		if( response.getAck() != null && ( response.getAck().equals(paypalExpress.AckCodeType.Success) || response.getAck().equals(paypalExpress.AckCodeType.SuccessWithWarning))) 
		{
			var details = response.getDoExpressCheckoutPaymentResponseDetails();
			var mytoken : String = details.getToken();
			var paymentInfo = details.getPaymentInfo()[0];
			var transactionID = paymentInfo.getTransactionID();
			pdict.ReturnCode = 1; 
			pdict.GiroRedirect = details.redirectRequired;
			
			if (pdict.GiroRedirectURL != null)
			{
				pdict.GiroLocation = pdict.GiroRedirectURL.toString() + mytoken;
			}
			
			pdict.BAID = details.billingAgreementID;
			
			Logger.debug( 'Writing Transaction ID to DB - ' + transactionID);
			paymentInstrument.paymentTransaction.transactionID = transactionID;
			paymentInstrument.paymentTransaction.amount = new dw.value.Money(response.doExpressCheckoutPaymentResponseDetails.paymentInfo[0].grossAmount._value, order.getCurrencyCode());
			paymentInstrument.getPaymentTransaction().setPaymentProcessor( paymentProcessor );
			paymentInstrument.custom.paypalTransactionID = transactionID;
			pdict.Order.setPaymentStatus(Order.PAYMENT_STATUS_PAID);
				
			Logger.error('Token: ' + response.doExpressCheckoutPaymentResponseDetails.token );
			
		} 
		else 
		{
			
			Logger.error('Corelation ID  --' + response.correlationID );
			Logger.error('Error Message Short Message  -- ' + response.getErrors()[0].getShortMessage() );
			Logger.error('Error Message Long Message  -- ' + response.getErrors()[0].getLongMessage() );
			Logger.error('Error Message ErrorCode  -- ' + response.getErrors()[0].getErrorCode() );
			
			pdict.ReturnCode = new Number(response.getErrors()[0].getErrorCode().toString());
			pdict.PaypalError = response.getErrors()[0].getLongMessage() + " (" + response.getErrors()[0].getErrorCode() + ")" + "-(" + response.correlationID + ")";
			
			return PIPELET_ERROR;
		}
		
	}catch(e){
		//uncomment var error for error debugging
		//var error = e;
		
		if(e instanceof Fault)
		{
			Logger.error("Actor " + e.faultActor+" caused fault '"+e.faultCode +"' ("+e.faultString+" - "+e.faultDetail+")");
		}
		else
		{
			Logger.error(e.toString());
		}
		pdict.Error = e;
		pdict.PaypalError = e.toString();
		return PIPELET_ERROR;
	}	
   	return PIPELET_NEXT;
}