'use strict';

/**
 * Determines a product based on the given ID.
 *
 * @module controllers/Monetate
 */

var ISML = require('dw/template/ISML');
var monetateObject = require('~/cartridge/scripts/monetate/libMonetate');

function getProductID () {
	if (request.httpHeaders['x-is-requestid'].indexOf('-0-00') !== -1){
		throw new Error('Guard(s) \'include\' did not match the incoming request.');
	}
	var pid = request.httpParameterMap.pid.stringValue;
	var updateProductVariationSelectionsResult,
		mProduct,
		type;
	if (pid) {
		mProduct = require('dw/catalog/ProductMgr').getProduct(pid);
		type = request.httpParameterMap.type.stringValue || "lp";
	}
	if (mProduct && mProduct.variant && !monetateObject.getMonetateVariationInSite()){
		mProduct = mProduct.variationModel.master
	}
	ISML.renderTemplate('monetate/monetategetproduct', {MProduct: mProduct, type: type});
	
}

exports.GetProductID = getProductID;
exports.GetProductID.public = true;
