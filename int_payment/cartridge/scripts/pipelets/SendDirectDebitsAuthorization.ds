/**
*	Sends Authorization to payment through DirectLink for Direct Debits AT, DE and NL 
*
*   @input Order : dw.order.Order Order
*   @input OrderNo : String Order number
*	@input PaymentForms : Object
*   @input Locale : String Locale
*	@input UseAliasCheckbox : Boolean
*	@input Customer : dw.customer.Customer
*   @output Status : String Status of Payment
*   @output StatusDescription : String Status description of Payment
*   @output PaymentMethod : String Payment Method
*   @output PayID : String Transaction ID of Payment
*   @output Brand : String Brand of Payment
*   @output PaymentMethodIDOut : String
*   @output ErrorString : String
*
*/
importPackage(dw.util);
importPackage(dw.net);
importPackage(dw.order);
importPackage(dw.customer);
importPackage(dw.system);
importPackage(dw.web);
importPackage(dw.crypto);

importScript("int_payment:lib/libPayment.ds");

function execute( args : PipelineDictionary ) : Number {
	var log : Logger = Logger.getLogger("payment");

	var forms = args.PaymentForms;
	var paymentType : String = forms.selectedPaymentMethodID.htmlValue;

	var mapForms = {
		'DIRECT_DEBITS_DE' : forms.directdebitsde,
		'DIRECT_DEBITS_NL' : forms.directdebitsnl,
		'DIRECT_DEBITS_AT' : forms.directdebitsat
	};

	var form = mapForms[paymentType];
	var paymentMethodId = paymentType;

	var order : Order = args.Order;
	var locale : String = args.Locale;
	var paymentMgr = null;

	if (empty(order)) {
		log.error('SendDirectDebitsAuthorization.ds: Missing required input parameter(s) Order.');
		return PIPELET_ERROR;
	}

	var resultObj = null;
	var status = "";
	var PaymentMethod = "";
	var payID = "";
	var brand = "";

	var start : Date = new Date();
	var enableTwoSteps : Boolean = Site.getCurrent().getCustomPreferenceValue("enableTwoStepsAuthorizationDirectLink") == true;
	var enableNewSEPAVersion : Boolean = Site.getCurrent().getCustomPreferenceValue("enableNewSEPAVersion");
	var operation : String = (paymentType == 'DIRECT_DEBITS_NL') ? 'VEN' : enableTwoSteps ? 'RES' : 'VEN';

	try {
		paymentObj = new PaymentCtnr(order, args.OrderNo, null, paymentMethodId, null, locale);

		status = paymentObj.getStatus();

		var data = {};
		if (paymentType == 'DIRECT_DEBITS_DE') {
			data = {
				iban: form.iban.htmlValue,
				konto: form.konto.htmlValue,
				blz: form.blz.htmlValue
			};
		} else if (paymentType == 'DIRECT_DEBITS_NL') {
			data = {
				iban: form.iban.htmlValue
			};
			// if SEPA enabled, display BIC optional field for NL 
			if (enableNewSEPAVersion) {
				var transactionDate = order.creationDate;
				var getTDYear = transactionDate.getFullYear().toString();
				var getTDMonth = (transactionDate.getMonth() + 1) < 10 ? '0' + (transactionDate.getMonth() + 1).toString() : (transactionDate.getMonth() + 1).toString();
				var getTDDate = transactionDate.getDate() < 10 ? '0' + transactionDate.getDate().toString() : transactionDate.getDate().toString();
				
				if (form.bic.htmlValue) {
					data.bic = form.bic.htmlValue;
				}
				data.transactionDate = getTDYear + getTDMonth + getTDDate;
			}
		} else if (paymentType == 'DIRECT_DEBITS_AT') {
			data = {
				konto: form.konto.htmlValue,
				blz: form.blz.htmlValue
			};
		}
		
		var enabledAliasGateway = dw.system.Site.getCurrent().getCustomPreferenceValue('enableDirectLinkAliasGateway') ;
		
		// make the payment request and save the response to 'resultObj' 
		if(args.UseAliasCheckbox && enabledAliasGateway){
			resultObj = paymentObj.sendDirectLinkDirectDebitsNewOrderRequest(operation, locale, data, null, null, true, args.Customer);
		}else{
			resultObj = paymentObj.sendDirectLinkDirectDebitsNewOrderRequest(operation, locale, data);
		}

	} catch (error) {
		var end : Date = new Date();
		var diff = (end.getTime() - start.getTime()) * 0.001;

		// Error responses:
		// status=0, at least one of the payment data fields is invalid or missing:
		if(error.status == "0" ){
			log.error("SendDirectDebitsAuthorization.ds: Direct Link Authorization Request invalid or incomplete; OrderNo: {0}", args.OrderNo);
			args.ErrorString=Resource.msg('billing.paymentstatus0', locale + '/checkout', null);
		
		// status=2, the authorisation has been declined by the financial institution
		} else if ( error.status == "2" ) {
			log.error("SendDirectDebitsAuthorization.ds: Direct Link Authorization Request Authorisation refused; OrderNo: {0}", args.OrderNo);
			args.ErrorString=Resource.msg('billing.paymentstatus2', locale + '/checkout', null);
		
		// status=52, a technical problem arose during the authorisation/payment process, giving an unpredictable result
		} else if ( error.status == "52" ) {
			order.getCustom()["payment_authNotKnown"] = true;
			var getAttr = order.getCustom()["payment_authNotKnown"];
			
			log.error("SendDirectDebitsAuthorization.ds: Direct Link Authorisation Request Authorisation not known; OrderNo: {0}", args.OrderNo);
			args.ErrorString=Resource.msg('billing.paymentstatus52', locale + '/checkout', null);
		
		// status=92, payment uncertain
		} else if ( error.status == "92" ) {
			log.error("SendDirectDebitsAuthorization.ds: Direct Link Authorisation Request Authorisation payment uncertain; OrderNo: {0}", args.OrderNo);
			args.ErrorString=Resource.msg('billing.paymentstatus92', locale + '/checkout', null);
		}

		return PIPELET_ERROR;
	}
	
	var useFraudDetection = dw.system.Site.getCurrent().getCustomPreferenceValue('enableFraudDetection');

	if ( !empty(resultObj) ) {
		// get resultObj data, store them to output 
		status = resultObj.hasOwnProperty("status") ? resultObj.status : status;
		PaymentMethod = resultObj.hasOwnProperty("pm") ? resultObj.pm : '';
		payID = resultObj.hasOwnProperty("payid") ? resultObj.payid : '';
		brand = resultObj.hasOwnProperty("brand") ? resultObj.brand : '';
		
		if (!empty(useFraudDetection) && useFraudDetection) {
			order.custom.payment_ip = resultObj.hasOwnProperty("ip") ? resultObj.ip : '';
			order.custom.payment_aavCheck = resultObj.hasOwnProperty("aavcheck") ? resultObj.aavcheck : '';
			order.custom.payment_cvcCheck = resultObj.hasOwnProperty("cvccheck") ? resultObj.cvccheck : '';
			order.custom.payment_scoring = resultObj.hasOwnProperty("scoring") ? resultObj.scoring : '';
			order.custom.payment_scoCategory = resultObj.hasOwnProperty("sco_category") ? resultObj.sco_category : '';	
		}
		
		if(!empty(brand)){
			PaymentMethod+=" - "+brand;
		}

		// set used payment method to ensure used payment method ID could be returned
		if (!empty(PaymentMethod)) paymentObj.setPaymentMethod(PaymentMethod);
	}
	
	order.custom.payment_PAYID = payID;

	args.Status = status;
	args.StatusDescription = getPaymentStatusDescription(status);
	args.PaymentMethod = PaymentMethod;
	args.PayID = payID;
	args.Brand = brand;
	args.PaymentMethodIDOut = paymentMethodId;

	// status=5, the authorisation has been accepted
	if (status == "5") {
		// Update the order for background step 2 reservation request
		order.custom.payment_isAuthorized = true;
		order.custom.payment_needCapture = true;
		order.custom.payment_authorizationStatus = 'AUTHORISED';
		order.custom.payment_captureAttempts = 0;
		order.custom.payment_data = JSON.stringify({
			paymentMethodID: paymentMethodId,
			locale: locale
		});
	} else {
		order.custom.payment_isAuthorized = false;
		order.custom.payment_needCapture = false;
		order.custom.payment_captureAttempts = 0;

		if(status != "9"){
			// status=46, waiting for identification.
			if ( status == "46" ) {
				if (enableTwoSteps) {
					order.custom.payment_data = JSON.stringify({
						paymentMethodID: PaymentMethodId,
						locale: locale
					});
				}
			// status=51, the authorisation will be processed offline.
			} else if ( status == "51" ) {
				order.getCustom()["payment_authWaiting"] = true;
				
				log.error("SendDirectDebitsAuthorization.ds: Direct Link Authorisation Request Authorisation waiting; OrderNo: {0}", args.OrderNo);
				args.ErrorString=Resource.msg('billing.paymentstatus51', locale + '/checkout', null);
			} else {
				log.error("SendDirectDebitsAuthorization.ds: Direct Link Authorization Request Authorisation refused; OrderNo: {0}", args.OrderNo);
				args.ErrorString=Resource.msg('billing.paymentstatus2', locale + '/checkout', null);
				return PIPELET_ERROR;
			}
		}
		// if status=9, the payment has been accepted.		
		
		if (status != "51") {
			order.setPaymentStatus(dw.order.Order.PAYMENT_STATUS_PAID);
		}
		
	}
	
	return PIPELET_NEXT;
}
