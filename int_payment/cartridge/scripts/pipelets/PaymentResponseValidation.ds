/**
*	Validates Payment response and checks if SHAOut and parameters are valid.
*
*   @input ParamMap : dw.web.HttpParameterMap Payment request's answer parameters from CurrentHttpParameterMap
*   @input OrderNo : String Order Number
*/
importPackage( dw.system );
importPackage( dw.util );
importPackage( dw.crypto );

importScript("int_payment:lib/libPayment.ds");	

function execute( args : PipelineDictionary ) : Number
{
	var log : Logger = Logger.getLogger("payment");
	
	var paramSortedMap : SortedMap,
		dwParamMap : Map,
		paramKeySet : Set,
		paramName : String,
		paramValue : String,
		paramString : String = '',
		paramIterator : Iterator,
		SHASign : String,
		digest : dw.crypto.MessageDigest;
		
	var paramMap = args.ParamMap;
	var SHAOut = Site.getCurrent().getCustomPreferenceValue("shaPasword");
	var hashMethod = Site.getCurrent().getCustomPreferenceValue("shaMethod");
	
	if ( empty(paramMap)) {
		log.error('PaymentResponseValidation.ds: Missing required input parameter(s).');
		return PIPELET_ERROR;
	}
	
	if ( paramMap.getParameterCount() <= 0 ) {
		log.error('PaymentResponseValidation.ds: Missing Payment HTTP parameter(s).');
		return PIPELET_ERROR;
	}
	
	paramSortedMap = new SortedMap();
	dwParamMap = new HashMap();
	paramIterator = paramMap.getParameterNames().iterator();
	digest = new dw.crypto.MessageDigest(hashMethod);
	while ( paramIterator.hasNext() ) {
		paramName = paramIterator.next().toString();
		if ( paramName == 'SHASIGN' ) {
			SHASign = paramMap[paramName].value;
			
		} else if(paramName.indexOf('dw_') == 0) { 
			dwParamMap.put(paramName, paramMap[paramName].value);
		} else {
			paramSortedMap.put(paramName.toUpperCase(), paramMap[paramName].value);
		}
	}
	
	if ( empty(SHASign) ) {
		log.error('PaymentResponseValidation.ds: Payment hash is missing.');
		return PIPELET_ERROR;
	}
	
	paramKeySet = paramSortedMap.keySet();
	paramIterator = paramKeySet.iterator();
	var paramValue = '';
	while ( paramIterator.hasNext() ) {
		paramName = paramIterator.next().toString();
		paramValue = paramSortedMap.get(paramName);
		if (!empty(paramValue)) {
			paramString += paramName + '=' + paramValue.toString() + SHAOut;
		}
	}
	if ( digest.digest(paramString).toUpperCase() !== SHASign ) {
		log.error('PaymentResponseValidation.ds: Payment returned parameters do not match the returned hash.');
		return PIPELET_ERROR;
	}
	
	return PIPELET_NEXT;
}
