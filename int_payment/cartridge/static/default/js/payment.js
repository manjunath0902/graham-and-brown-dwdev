(function($){
$(document).ready(function() {
	
	$(".aliasGateway").children().click(function() {
		 $(".payment-method-options.hide-payment-method").show();
		 $(".payment-method.hide-credit-card div,span").show();
		 $(".direct-debits.hide-direct-debits").show();
		 $(".payment-method-display, div.cvn-tip .tooltip-content, div.cvn-tip .dw-object-rinclude").hide();
	});
	
	$(".input-radio").click(function(e) {
		var allowAlias = $(e.target).attr("data-alias-gateway-available");
		if(allowAlias == "false"){
			$(".aliasGateway").hide();
		}else{
			$(".aliasGateway").show();
	
		}
	});
	
	$("#dwfrm_billing_paymentMethods_creditCard_type").change(function(e) {
		var creditCardType = $(e.target).find(":selected").val();
		var allowAliasCard = $('#' + creditCardType).attr("data-alias-card-available");
		if(allowAliasCard == "false"){
			$(".aliasGateway").hide();
		}else{
			$(".aliasGateway").show();
		}
	});
	
	$("#creditCardList").change(function() {
		setTimeout('$("#dwfrm_billing_paymentMethods_creditCard_type").trigger("change");',300);
	});
});
})(jQuery);